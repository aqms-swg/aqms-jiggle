package org.trinet.jasi.magmethods.TN;

import java.util.*;

import org.trinet.jasi.*;
import org.trinet.jasi.magmethods.*;
import org.trinet.util.MathTN;

public class CuspSoCalMlMagMethod extends MlMagMethod {

    public CuspSoCalMlMagMethod() {
        magSubScript = MagTypeIdIF.ML;
        methodName = "CuspSoCalMl";
        setMinDistance(30.0);
        setMaxDistance(600.0);
        setRequireCorrection(true);
    }
    // punt here for now
    public void configure(int src, String location, String section) {
        setProperties(location); // location like a filename url
    }
    // assume we don't need channel gains for the pre-digital era data 
    protected boolean hasRequiredChannelData(MagnitudeAssocJasiReadingIF jr) {
        Channel jrChan = jr.getChannelObj();
        java.util.Date date = jr.getDateTime(); // reading observation time
        if ( ! jrChan.hasLatLonZ() ) {
          // missing, if doLookUp reloads from source, but why repeated queries ad nauseum ?
          // also see implementation of JasiReading.setChannelObj(Channel) - aww
          if (doLookUp) jr.setChannelObjData(jrChan.lookUp(jrChan, date));
        }
        double dist = jrChan.getHorizontalDistance(); // slant ok to use for test -aww
        if ( (dist == Channel.NULL_DIST) || (dist == 0.0) || Double.isNaN(dist) ) {
          jrChan.calcDistance(jr.getAssociatedSolution()); // changed input arg type aww 06/11/2004
        }
        if (debug) {
          System.out.println(methodName+" DEBUG: hasRequiredChannelData() chan:"+jrChan+
          " for date:"+date+" latLonZ? "+jrChan.hasLatLonZ() + " dist: " + jrChan.getHorizontalDistance() + " az: " + jrChan.getAzimuth());
        }
        return (jrChan.hasLatLonZ()) ? true : false ; // isValid(jr);
    }

    /** Formula from Carl Johnson CUSP code claiming adaption from Richter Elmentary Seismology.
     *  Input distance value is epicentral distance to station in kilometers.
     *  */
    public double distanceCorrection(double km) {
        double a0 = Math.exp(-0.0051*km);
        a0 = (0.3070*a0)*Math.pow(km,-1.1317);
        a0 = - MathTN.log10(a0);
        return a0;
    }

    public int getSummaryMagValueStatType() { return MEDIAN_MAG_VALUE; }

} 
