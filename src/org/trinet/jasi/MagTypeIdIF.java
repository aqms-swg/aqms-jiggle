package org.trinet.jasi;
public interface MagTypeIdIF {
  public static final String ANY = "any";
  public static final String NULL = "null";
  public static final String [] TYPES =
      {"b","c","d","e","h","l","lr","lg","n","s","un","w"};

  public static final String MB  =  "b";
  public static final String MC  =  "c";
  public static final String MD  =  "d";
  public static final String ME  =  "e";
  public static final String MH  =  "h";
  public static final String ML  =  "l";
  public static final String MLR  =  "lr";
  public static final String MLG = "lg";
  public static final String MN  =  "n";
  public static final String MS  =  "s";
  public static final String MUN =  "un";
  public static final String MW  =  "w";

  public static final String [] TYPES_DESC = {
      "body-wave",
      "coda amplitude",
      "coda duration",
      "energy",
      "hand-entered",
      "local",
      "local reduced",
      "bodywave Lg",
      "no magnitude",
      "surface-wave",
      "unknown",
      "moment"
  };
}
