package org.trinet.jiggle.common.type;

import java.util.List;

public class AssocEvent {
    private long masterEventId;
    private long assocEventId;
    private long commentId;

    static final public long DEFAULT_ID = -1;
    public AssocEvent() {
        this.masterEventId = DEFAULT_ID;
        this.assocEventId = DEFAULT_ID;
        this.commentId = DEFAULT_ID;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (int) (this.masterEventId ^ (this.masterEventId >>> 32));
        hash = 89 * hash + (int) (this.assocEventId ^ (this.assocEventId >>> 32));
        hash = 89 * hash + (int) (this.commentId ^ (this.commentId >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AssocEvent other = (AssocEvent) obj;
        if (this.masterEventId != other.masterEventId) {
            return false;
        }
        if (this.assocEventId != other.assocEventId) {
            return false;
        }
        return this.commentId == other.commentId;
    }

    /**
     * @return the eventId
     */
    public long getMasterEventId() {
        return masterEventId;
    }

    /**
     * @param masterEventId the eventId to set
     */
    public void setMasterEventId(long masterEventId) {
        this.masterEventId = masterEventId;
    }

    /**
     * @return the assocEventId
     */
    public long getAssocEventId() {
        return assocEventId;
    }

    /**
     * @param assocEventId the assocEventId to set
     */
    public void setAssocEventId(long assocEventId) {
        this.assocEventId = assocEventId;
    }
    
    public static String getEventAssociationList(List<AssocEvent> assocEvents) {
        String assocString = "";
        for (int idx = 0; idx < assocEvents.size(); idx++) {
            // comma separated list of event IDs
            if (idx > 0) { 
                assocString += ", " + assocEvents.get(idx).getAssocEventId();
            } else {
                assocString += assocEvents.get(idx).getAssocEventId();
            }
        }
        return assocString;
    }    

    /**
     * @return the commentId
     */
    public long getCommentId() {
        return commentId;
    }

    /**
     * @param commentId the commentId to set
     */
    public void setCommentId(long commentId) {
        this.commentId = commentId;
    }
}
