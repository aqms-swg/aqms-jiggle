// *** PACKAGE NAME *** \                                     
package org.trinet.jasi.EW.SQL;                                
                                                               
                                                               
// *** IMPORTS *** \                                          
import java.util.*;                                            
import java.sql.Connection;                                    
import java.sql.SQLException;                                  
import org.trinet.jasi.EW.EWSolution;                                  
                                                               
                                                               
/**                                    
 * <!-- Class Description>             
 *                                     
 *                                     
 * Created:  2002/06/12                        
 *                                     
 * @author   ew_oci2java Tool / DK                        
 * @version  0.99                        
 **/                                   
                                       
public class GetPreferredInfoStatement extends EWSQLStatement 
{                                      
                                       
  // *** CLASS ATTRIBUTES *** \       
                                       
  // *** CONSTRUCTORS *** \           
  public GetPreferredInfoStatement()                          
  {                                    
    sSQLStatement = "Begin Get_Preferred_Summary_Info(OUT_RetCode => :OUT_RetCode, IN_idEvent => :IN_idEvent,  OUT_idOrigin => :OUT_idOrigin, OUT_idMag => :OUT_idMag,  OUT_idMech => :OUT_idMech); End;";
    bIsQuery = false;                
    init();                            
  }                                    
                                       
                                       
  public GetPreferredInfoStatement(Connection IN_conn)        
  {                                    
    this();                            
    SetConnection(IN_conn);            
  }                                    
                                       
                                       
  // *** CLASS METHODS *** \          
                                       
  protected boolean SetInputParams(Object obj)    
  {                                  
                                     
    EWSolution objEWSolution = (EWSolution)obj;              
                                     
    try                              
    {                                
      cs.setLong(2, objEWSolution.idEvent);    
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetPreferredInfoStatement:SetInputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end GetPreferredInfoStatement::SetInputParams()    
                                     
                                     
  protected boolean RegisterOutputParams()    
  {                                  
    try                              
    {                                
      cs.registerOutParameter(1, java.sql.Types.INTEGER); 
      cs.registerOutParameter(3, java.sql.Types.BIGINT); 
      cs.registerOutParameter(4, java.sql.Types.BIGINT); 
      cs.registerOutParameter(5, java.sql.Types.BIGINT); 
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetPreferredInfoStatement:RegisterOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
    return(true);                    
  }  // end GetPreferredInfoStatement:RegisterOutputParams()
                                     
                                     
  protected boolean RetrieveOutputParams(Object obj)
  {                                  
    EWSolution objEWSolution = (EWSolution)obj;             
                                     
    try                              
    {                                
      objEWSolution.Retcode=cs.getInt(1);     
      objEWSolution.idOrigin=cs.getLong(3);     
      objEWSolution.idMag=cs.getLong(4);     
      objEWSolution.idMech=cs.getLong(5);     
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetPreferredInfoStatement:RetrieveOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end GetPreferredInfoStatement::RetrieveOutputParams()
                                     
                                     
} // end class GetPreferredInfoStatement                      
                                       
                                       
//             <EOF>                   
