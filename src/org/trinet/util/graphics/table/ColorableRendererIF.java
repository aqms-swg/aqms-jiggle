package org.trinet.util.graphics.table;
public interface ColorableRendererIF {
  public void setBackground(java.awt.Color c);
  public void setForeground(java.awt.Color c);
  public void setDefaultForeground(java.awt.Color c);
  public void setDefaultBackground(java.awt.Color c);
}
