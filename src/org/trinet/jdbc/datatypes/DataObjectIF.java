package org.trinet.jdbc.datatypes;
public interface DataObjectIF extends DataNumber, Comparable { 
/** Method compares DataObject values and return true if they are equivalent. */
    public abstract boolean equalsValue(Object object) ;
/** Method returns the SQL string equivalent to the DataObject value. */
    public abstract String toStringSQL() ;
/** Method sets interval value to the equivalent to the value type. */
    public abstract void setValue(Object object) throws ClassCastException, NumberFormatException ;
/** Method returns true if the value is valid, e.g. depending on constraints. */
    public abstract boolean isValid();
}
