package org.trinet.apps;

import java.util.*;

import org.trinet.jasi.*;
import org.trinet.util.*;
import org.trinet.jdbc.datasources.AbstractSQLDataSource;
import test.common.DatabaseTests;

public final class ChannelListCacheWriter {
    public static final void main(String args[]) {

      if (args.length < 7) {
        System.out.println("Args: [user] [pwd] [host.domain] [dbName] [driver] [subprotocol] [port] [ [date(yyyy-MM-dd)][cacheFilename] [groupname or 'all'] ['src' (match channelsrc too) ['llz' (don't load gain,corr)]");
        System.exit(0);
      }

      String user   = args[0];
      String passwd = args[1];
      String host   = args[2];
      String dbname = args[3];
      String driver = args[4];
      String subprotocol = args[5];
      String port = args[6];

      System.out.println("ChannelListCacheWriter: making connection... ");
      DataSource ds = TestDataSource.create(host, dbname,
              user, passwd, subprotocol, port, driver);
      System.out.println("ChannelListCacheWriter dataSource : " + ds.describeConnection());

      java.util.Date date = (args.length > 7) ? EpochTime.stringToDate(args[7] +" 00:00:00.000") : new java.util.Date();

      String groupName = null;
      if (args.length > 9 && ! args[9].equalsIgnoreCase("all")) {
          groupName = args[9];
      }
      System.out.println("ChannelListCacheWriter: creating "+ ((groupName == null) ? "all channels" : groupName) +
              " list for date: " + EpochTime.toString(date));

      //Test level matching:
      String suffix = "ByLocation";
      if (args.length > 10 && args[10].equalsIgnoreCase("src")) {
        System.out.println("ChannelListCacheWriter: channelmap match by net,sta,seedchan,location,channel,channelsrc");
        JasiChannelDbReader.defaultMatchMode = JasiChannelDbReader.CHANNELSRC;
        suffix = "ByChannelSrc";
      }
      else {
        System.out.println("ChannelListCacheWriter: channelmap match by net,sta,seedchan,location");
        JasiChannelDbReader.defaultMatchMode = JasiChannelDbReader.LOCATION;
      }

      if (args.length > 11 && args[11].equals("llz")) ChannelList.readOnlyChannelLatLonZ();

      ChannelList cl  = (groupName != null) ? ChannelList.readListByName(groupName, date) : ChannelList.readList(date);
      System.out.println("ChannelListCacheWriter: creating lookup map...");
      cl.createLookupMap();
      //
      // object serialization out
      //
      String cacheFile = (args.length > 8) ? args[8] : "channelList.cache";
      cacheFile += suffix;
      cl.setCacheFilename(cacheFile);
      System.out.println("ChannelListCacheWriter : writing to " +cacheFile);
      boolean retVal = cl.writeToCache();
      System.out.println( "ChannelListCacheWriter: Write cache success = " +retVal+ " size = "+cl.size());

    } // end of main
}
