package org.trinet.jasi.TN;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.util.*;

/**
 * This is a QC model that answers a yes/no question. Is this solution good
 * enough to finalize or does it need futher work.
 *
 * This should probably be a dbase stored procedure.
 *
 * @author Doug Given
 * @version */

public class QualityControlModelTN extends QualityControlModel {

    /** Description of what's wrong with the Solution. */
    static String hintString;

    Solution sol;

    /* This should all be parameterized in the dbase or a properties file. */
    /** RMS must be less then this value. */
    double rmsThresh = 0.4;
    double gapThresh = 180.0;
    double depthThresh = 20.0;

    // block use of constructor
    QualityControlModelTN() {}

    /** Return 'true' if the event is "good" according to the quality model */
    public boolean isGood(Solution sol) {
        this.sol = sol;
        hintString = "";
        return rmsOK() && phaseCountOK() && magRmsOK();
    }

    /** Return a string describing what's wrong with the event. */
    public String getHintString (Solution sol) {
        isGood(sol);
        return hintString;
    }

    boolean rmsOK() {
        if (sol.rms.doubleValue() < rmsThresh) {
            return true;
        } else {
            hintString += "RMS too high: = "+sol.rms.doubleValue()+"\n";
            return false;
        }
    }

    boolean phaseCountOK() {
        long phaseThresh =(long) ( 50.0 * sol.magnitude.value.longValue() - 75.0);
        if (sol.usedReadings.doubleValue() >= phaseThresh) {
            return true;
        } else {
            hintString += "Too few phases for mag, needs "+phaseThresh+"\n";
            return false;
        }
    }

    boolean gapOK () {
        if (sol.gap.doubleValue() > gapThresh) {
            return true;
        } else {
            hintString += "Gap too large: = "+sol.gap.doubleValue()+"\n";
            return false;
        }
    }

    boolean depthOK () {
        if (sol.depth.doubleValue() > depthThresh) {
            return true;
        } else {
            hintString += "Depth too large: = "+sol.depth.doubleValue()+"\n";
            return false;
        }
    }

    boolean magRmsOK () {
        return true;
    }
/*
    // Main for testing: % CatalogView [hours-back]  (default = 1)
    public static void main (String args[]) {
        System.out.println ("Starting");
        System.out.println ("Making connection...");
        DataSource ds = TestDataSource.create();

        // get by time
        double hoursBack = 24;
        double now = new DateTime().getTrueSeconds();
        double then = now - 3600.*hoursBack;

        TimeSpan ts = new TimeSpan(then, now);

        System.out.println ("Get entries by timespan: \n"+ ts.toString());

        //Solution sol[] = Solution.getByTime(dstart, dend);
        Solution sol[] = (Solution[])Solution.create().getValidByTime(then, now).toArray(new Solution[0]);
        System.out.println ("Found "+sol.length+" events.");

        //QualityControlModel qc = QualityControlModel.getInstance();
        QualityControlModel qc = QualityControlModel.create();

        for (int i = 0; i < sol.length; i++) {
            System.out.println (sol[i].toSummaryString());
            System.out.println (qc.getHintString(sol[i]));
        }

    }
*/
} // QualityControlModelTN
