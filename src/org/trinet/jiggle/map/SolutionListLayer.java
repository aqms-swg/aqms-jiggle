// TODO: A Popup legend for interval color mapping
package org.trinet.jiggle.map;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.*;

import javax.swing.Action;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

import com.bbn.openmap.Environment;
import com.bbn.openmap.I18n;
import com.bbn.openmap.MapBean;
import com.bbn.openmap.MapHandler;
import com.bbn.openmap.event.*;
import com.bbn.openmap.layer.*;
import com.bbn.openmap.omGraphics.*;
import com.bbn.openmap.proj.Projection;
import com.bbn.openmap.util.*;

import org.trinet.jasi.*;
import org.trinet.jiggle.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.LatLonZ;

/**
* SolutionListLayer displays data from a SolutionList.
* Application must set SolutionList to display it.
* @see MasterViewLayer
* @see CatalogLayer
*/
public class SolutionListLayer extends JiggleGraphicHandlerLayer implements
        MapMouseListener, ListDataStateListener, PropertyChangeListener {

    public static final float DEFAULT_RADIUS_SCALAR = 2.f;
    public static final double DEFAULT_DEPTH_INTERVAL = 5.; // km
    public static final double DEFAULT_TIME_INTERVAL = 1.; // 1 day
    public static final String DEFAULT_TIME_INTERVAL_UNITS = "DAY";
    public static final float DEFAULT_VIEW_NONE_SCALE =  10000000.f;
    public static final boolean DEFAULT_MAP_RECENTER = true;

    protected static final String SaveCmd = "saveProps"; 
    protected static final String LegendCmd = "makeLegend"; 

    public static final String VIEW_NONE_SCALE_PROPERTY = "viewNoneScale";
    public static final String RADIUS_SCALAR_PROPERTY = "radiusScalar";
    public static final String COLOR_BY_PROPERTY = "colorBy";
    public static final String DEPTH_INTERVAL_PROPERTY = "depthInterval";
    public static final String TIME_INTERVAL_PROPERTY = "timeInterval";
    public static final String TIME_INTERVAL_UNITS_PROPERTY = "timeIntervalUnits";
    // public static final String INTERVAL_COLORS_PROPERTY = "intervalColors"; // deprecated -aww 2008/10/27
    public static final String DEFAULT_FILL_COLOR_PROPERTY = "defaultFillColor";
    public static final String RECENTER_BOUNDS_PROPERTY = "recenterBounds";

    private double timeIntervalStart = 0.; // min time of all solution datetime true seconds in list

    private static final Format fmt = new Format("%02d");

    /** The layer GUI. */
    protected JPanel gui = null;

    protected Jiggle jiggle = null;

    protected JFrame legend = null;

    protected SolutionList solList = null; // new GenericSolutionList();

    protected OMGraphic lastSelectedGraphic = null; // mouse selected
    protected OMGraphic lastSelectedSolGraphic = null; // user list selected

    protected boolean showingInfoLine = false;

    protected boolean recenterBounds = DEFAULT_MAP_RECENTER; // default to recenter

    protected float viewNoneScale = DEFAULT_VIEW_NONE_SCALE;

    protected double depthInterval = DEFAULT_DEPTH_INTERVAL; // km
    protected double timeInterval = DEFAULT_TIME_INTERVAL; // seconds
    protected String timeIntervalUnits = DEFAULT_TIME_INTERVAL_UNITS; // days

    protected float radiusScaleFactor = DEFAULT_RADIUS_SCALAR;

    protected String colorBy = ColorIntervalTypePropertyEditor.UNIFORM;

    protected static Color selectColor = Color.yellow;
    protected static Color defaultFillColor = Color.red;
    protected static String defaultFillColorString = Integer.toHexString(defaultFillColor.getRGB());

    protected static Color [] DEFAULT_FILL_COLORS = {defaultFillColor};
    protected Color [] intervalFillColors = DEFAULT_FILL_COLORS;
    protected HashMap etypeFillColorMap = new HashMap();
    protected HashMap gtypeFillColorMap = new HashMap();

    //from QWClient points for arrow polygon to mark selected quake symbol:
    private static final int [] arrowMarkerPts =
        new int [] { 0,0, 10,24, 10,12, 22,14, 0,0 };

    //from QWClient arrow polygon to mark selected quake symbol:
    private final OMPoly arrowMarker =
        new OMPoly(0,0, arrowMarkerPts, OMPoly.COORDMODE_ORIGIN);

    {
        arrowMarker.setLinePaint(Color.black);  //set color for arrow
        arrowMarker.setFillPaint(
           (this instanceof MasterViewLayer) ? Color.pink : Color.white
        );  //marker for select
    }

    /**
     * Construct an SolutionListLayer.
     */
    public SolutionListLayer() {
        setProjectionChangePolicy(new com.bbn.openmap.layer.policy.ListResetPCPolicy(this));
        //setMouseModeIDsForEvents(new String[] { SelectMouseMode.modeID });
    }

    //
    //NOTE: mapBean = (MapBean) SwingUtilities.getAncestorOfClass(Class.forName("com.bbn.openmap.MapBean"), this);
    //                (MapBean) this.getParent(); // usually
    //

    public void setSelectedSolution(Solution sol) {

        OMGraphicList omgList = getList();
        if (omgList == null) return;
        if (sol == null) {
            omgList.deselect();
            lastSelectedSolGraphic = null;
            arrowMarker.setVisible(false);
        }
        else {
          if (lastSelectedSolGraphic != null) lastSelectedSolGraphic.deselect();
          OMGraphic g = OMUtils.getOMGraphicWithAppObject(omgList, sol);
          if (g != null) {
            g = resetGraphicLocationFor(sol);
            g.select();
            showArrowAt(g);
            lastSelectedSolGraphic = g;
            repaint();
          }
          else {
              arrowMarker.setVisible(false); // input solution not in list
          }
        }
    }
    
    protected void showArrowAt(OMGraphic g) {
        OMGraphicList omgList = getList();
        if (omgList == null || g == null) {
          arrowMarker.setVisible(false);
          return;
        }
        double lat = 0.;
        double lon = 0.;
        if (g instanceof OMPoint) {
          OMPoint o = (OMPoint) g;
          lat = o.getLat();
          lon = o.getLon();

        }
        else if (g instanceof OMPoly) {
          OMPoly o = (OMPoly) g;
          lat = o.getLat();
          lon = o.getLon();
        }
        else if (g instanceof OMCircle) {
          OMCircle o = (OMCircle) g;
          lat = o.getLatLon().getLatitude();
          lon = o.getCenter().getX();
        }
        else {
            System.err.println(getClass().getName() +
                    " ERROR unknown graphic marker type for earthquake: " +
                    g.getClass().getName());
            return;
        }
        arrowMarker.setLocation(lat, lon, OMGraphic.DECIMAL_DEGREES, arrowMarkerPts);
        arrowMarker.generate(getProjection());
        omgList.moveIndexedToTop(omgList.indexOf(arrowMarker));
        arrowMarker.setVisible(true);
        //
        // Recenter map only if a new MasterView or its selected Solution changed
        //
        //if (recenterBounds) {
          if (lastSelectedSolGraphic == null) recenterWhenOutOfBounds(lat, lon, recenterBounds);
          else if (lastSelectedSolGraphic.getAppObject() != g.getAppObject()) recenterWhenOutOfBounds(lat, lon, recenterBounds);
        //}
    
    }

    private void recenterWhenOutOfBounds(double lat, double lon, boolean byBounds) {
        if (lat == lon && lat == LatLonZ.NullValue) return; // don't do it for the unknowns
        Projection p = getProjection();
        LatLonPoint ulp = LatLonPoint.valueOf(p.getUpperLeft());
        LatLonPoint lrp = LatLonPoint.valueOf(p.getLowerRight());
        //System.out.println("Projection corner upperLeft: " + ulp.toString() +  " lowerRight: " + lrp.toString());
        boolean outOfBounds = byBounds || 
            (lat > ulp.getLatitude() || lat < lrp.getLatitude() || lon < ulp.getLongitude() || lon > lrp.getLongitude());
        //System.out.println("selection outOfBounds: " + outOfBounds);
        //((Proj) p).setCenter(lat,lon);
        if (outOfBounds) {
            MapHandler mh = (MapHandler) getBeanContext();
            if (mh != null) {
               JiggleMapBean mapBean = (JiggleMapBean) mh.get("org.trinet.jiggle.JiggleMapBean");
               if (mapBean != null) {
                   mapBean.setCenter(lat, lon);
               }
            }
        }
    }

    public void setSolutionList(SolutionList inList) {
        //System.out.println(getClass().getName() + " DEBUG setSolutionList is null? : " + (inList == null));
        if (solList != null) { // assume time sorted list
            solList.removeListDataStateListener(this);
        }
        solList = inList;
        if (solList != null) {
          setTimeIntervalStart(solList); // added -aww 2008/02/13
          solList.addListDataStateListener(this);
        }

        OMGraphicList omgList = getList();
        if (omgList != null) {
          omgList.clear(); // clear old graphics list
          setList(null);    
          repaint();
        }

        if (this.solList != null) {
          //System.out.println(getClass().getName() + " DEBUG setSolutionList size : " + solList.size());
          doPrepare(); // build layer graphics in background thread
        }
    }

    private void setTimeIntervalStart(SolutionList inList) { // added -aww 2008/02/13
        timeIntervalStart = new DateTime().getTrueSeconds(); // currrent time
        if (inList.size() > 0) {
           TimeSpan ts = inList.getTimeSpan(); // min,max datetime
           if (ts.isValid()) timeIntervalStart = ts.getEnd(); // use max datetime
        }
    }

    @Override
    public void prepare(OMGraphicList omgList) {
      super.prepare(omgList);
      if (solList != null) setSelectedSolution((Solution) solList.getSelected());
    }

    @Override
    protected OMGraphicList generateGraphics(OMGraphicList omgList) {
        Projection p = getProjection(); // null, if projection not yet defined 
        if (p == null || p.getScale() > viewNoneScale)  return omgList;

        //System.out.println(getClass().getName() + " DEBUG solList is null : " + (solList == null));
        if (solList != null) {
          //System.out.println(getClass().getName() +  " DEBUG generateGraphics solList size: " + solList.size());
          Solution [] s = (Solution []) solList.toArray(new Solution [solList.size()]);
          Arrays.sort(s, new Comparator() {
                  public int compare(Object o1, Object o2) {

                      Magnitude mag1= ((Solution) o1).getPreferredMagnitude();
                      double v1 = (mag1 == null) ? 0. : mag1.getMagValue();
                      Magnitude mag2 = ((Solution) o2).getPreferredMagnitude();
                      double v2 = (mag2 == null) ? 0. : mag2.getMagValue();
                      if (Double.isNaN(v1) && Double.isNaN(v2) ) return 0;
                      if (Double.isNaN(v1) && !Double.isNaN(v2) ) return -1;
                      if (!Double.isNaN(v1) && Double.isNaN(v2) ) return 1;

                      if (v1 == v2) return 0;
                      return (v1 < v2) ? -1 : 1;
                  }
                  public boolean equals(Object obj) {
                      return (obj == this);
                  }
              }
          );
          OMGraphic g = null;
          for (int i = 0; i < s.length; i++) {
            //System.out.println(getClass().getName() +  " DEBUG generateGraphics count: " + i);
            g = makeOMGraphic((Solution) s[i]);
            if (g != null) omgList.add(g);
          }
          omgList.add(arrowMarker);
      }
      return omgList;
    }

    protected OMGraphic makeOMGraphic(Solution sol) {
        if (sol == null) return null;
        //System.out.println(getClass().getName() +  " DEBUG makeOMGraphic " + sol.toSummaryString());
        float scale = getProjection().getScale();

        double magValue = 0.;
        if ( sol.magnitude != null && ! sol.magnitude.hasNullValue() )
            magValue = sol.magnitude.getMagValue();

        // Don't show small events at large scales
        if (magValue < 3.0 && scale > 10000000.f) return null;

        // minimum pixel radius size for rounded to zero magnitude 
        int blip = 0; 
        if (scale < 2000000.f) blip = 2;
        else if (scale < 5000000.f) blip = 1;

        // don't scale radius size at large scales
        if (scale > 5000000.f) radiusScaleFactor = 1.f; 

        if (magValue < 0.5) magValue = 0.5;  // aww 07/05/2007
        int circle_r = blip + (int) Math.round(radiusScaleFactor*magValue);
        if (circle_r < 1) circle_r = 1; // allow a minimum pixel minimum size

        // XY-Circle at LatLonPoint
        OMPoint g = new OMPoint(sol.lat.doubleValue(), sol.lon.doubleValue(), circle_r);
        g.setOval(true);
        g.setSelectPaint(selectColor);

        /*
        if (solList != null) {
          Solution selection  =  (Solution) solList.getSelected();
          if (selection == sol) {
            g.select();
            lastSelectedSolGraphic = g;
          }
        }
        */

        g.setFillPaint(getFillColor(sol));
        g.setAppObject(sol); //map to Solution
        return g;
    }

    private Color getFillColor(Solution sol) {
        //System.out.println("getFillColor:" +colorBy);
        if (colorBy.equalsIgnoreCase(ColorIntervalTypePropertyEditor.UNIFORM)) {
            return defaultFillColor;
        }

        if (colorBy.equalsIgnoreCase(ColorIntervalTypePropertyEditor.TYPE)) { // aww added TYPE coloring 2007/08/22
            if (etypeFillColorMap.size() == 0) {
               return defaultFillColor;
            }
            else {
                String etype = sol.getEventTypeString();
                // Commented out kludge for old coloring scheme, switch mapped color to gtype color when etype earthquake - aww 2015/10/09
                //if (etype.equals("earthquake")) {
                //   //Color c = (Color) etypeFillColorMap.get(etype);
                //   return (gtypeFillColorMap.size() == 0) ? (Color) etypeFillColorMap.get(etype) : (Color) gtypeFillColorMap.get(sol.getOriginGTypeString());
                //}
                //else {
                   return (Color) etypeFillColorMap.get(etype);
                //}
            }
        }

        if (colorBy.equalsIgnoreCase(ColorIntervalTypePropertyEditor.GTYPE)) { // aww added GTYPE coloring 2015/04/28
            if (gtypeFillColorMap.size() == 0) {
                 return defaultFillColor;
            }
            else {
                String gtype = sol.getOriginGTypeString();
                Color c = null;
                if (gtype.length() == 0) {
                    // To have it not fill any color you would set c = null instead just set clear color to any RGB like 000000
                    c = (gtypeFillColorMap.get("clear") != null ) ? null : (Color) gtypeFillColorMap.get("null");
                }
                else {
                    c= (Color) gtypeFillColorMap.get(gtype);
                }
                return c;
            }
        }

        // Fill colors set by property named "intervalColors"
        Color fillColor = intervalFillColors[0]; // default is to use first array element
        if (intervalFillColors.length == 1) return fillColor;

        double val = -1.; // forces color array index 0 if UNIFORM color mode
        double interval = 0.;
        double scalar = 1.;
        if (colorBy.equalsIgnoreCase(ColorIntervalTypePropertyEditor.DEPTH)) {
            //val = sol.getDepth(); // should introduce a new layer property to be able to choose model or geoid depth -aww 2015/10/09
            val = sol.getModelDepth(); // -aww 2015/10/09
            interval = depthInterval; // in km (default 5 km)
        }
        else if (colorBy.equalsIgnoreCase(ColorIntervalTypePropertyEditor.TIME)) {
            val = Math.abs(sol.getTime() - timeIntervalStart) ; // now use list max time, instead of current time -aww 2008/02/09
            interval = timeInterval; // elapsed time default is 1 day 86400 secs
            scalar = getTimeIntervalScalar(timeIntervalUnits); 
        }
        fillColor = intervalFillColors[intervalFillColors.length-1]; // set color to LAST array value, the upper bound
        for (int i=0; i < intervalFillColors.length; i++) {
               if ( val < (i+1)*interval*scalar ) {
                 fillColor = intervalFillColors[i]; // value less than interval bound
                 break;
               }
        }
        //System.out.println("getFillColor returns: " +fillColor.toString());

        return fillColor;
    }

    private double getTimeIntervalScalar(String timeIntervalUnits) {
        double scalar = 86400.;
        if ( timeIntervalUnits == null || timeIntervalUnits.equalsIgnoreCase("DAY") ) {
            return scalar;
        }
        else if ( timeIntervalUnits.equalsIgnoreCase("MONTH") ) {
            return scalar *= 30.;
        }
        else if ( timeIntervalUnits.equalsIgnoreCase("YEAR") ) {
            return scalar *= 365.;
        }
        else if ( timeIntervalUnits.equalsIgnoreCase("WEEK") ) {
            return scalar *= 7.;
        }
        else if ( timeIntervalUnits.equalsIgnoreCase("DECADE") ) {
            return scalar *= 3650.;
        }
        else if (  timeIntervalUnits.equalsIgnoreCase("HOUR") ) {
            return scalar /= 24.;
        }
        System.err.println("SolutionListLayer Error Unknown units value for property timeIntervalUnits: " + 
                timeIntervalUnits);
        return 0.;
    }

    @Override
    public void findAndInit(Object someObj) {
        if (someObj instanceof JiggleMapBean) {
            JiggleMapBean mapBean = (JiggleMapBean) someObj;
            mapBean.addPropertyChangeListener(this);
            this.jiggle = mapBean.jiggle;
        }
        super.findAndInit(someObj);
    }

    @Override
    public void findAndUndo(Object obj) {
        if (obj instanceof MapBean) {
            JiggleMapBean mapBean = (JiggleMapBean) obj;
            mapBean.removePropertyChangeListener(this);
            setSolutionList(null);
            this.jiggle = null;
        }
        super.findAndUndo(obj);
    }

    /**
     * Get the earthquake Solution data.
     */
    public SolutionList getSolutionList() {
        return solList;
    }

    /** The user interface palette for layer. */
    protected JPanel palette = null;

    /**
     * Gets the gui controls associated with the layer.
     * @return Component
     */

    /** Creates the interface palette. */
    @Override
    public java.awt.Component getGUI() {
        if (palette == null) {
            if (com.bbn.openmap.util.Debug.debugging("solution"))
                System.out.println(getClass().getName() + " : creating GUI Palette.");

            palette = PaletteHelper.createPaletteJPanel(i18n.get(SolutionListLayer.class,
                    "layerPanel", "Solution Layer Options"));

            palette.setLayout(new GridLayout(0,1));

            ActionListener al = new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                  SolutionListLayer.this.actionPerformed(e);
                  String command = e.getActionCommand();
                  if (command == RedrawCmd) {
                      getList().clear(); // clear old graphics list
                      setList(null);    
                      if (isVisible()) SolutionListLayer.this.doPrepare();
                  }
                  else if (command == LegendCmd) {
                      makeLegend();
                  }
                  else if (command == SaveCmd) {
                    File file = new File(jiggle.getProperties().getUserFilePath()+
                            GenericPropertyList.FILE_SEP+getPropertyPrefix()+"Layer.props");
                    JFileChooser jfc = new JFileChooser(file);
                    //jfc.setFileSelectionMode(JFileChooser.FILES_ONLY); // forces save to be inside jiggle user dir -aww 2008/06/24
                    jfc.setSelectedFile(file);
                    if (jfc.showSaveDialog(jiggle) == JFileChooser.APPROVE_OPTION) {
                      file = jfc.getSelectedFile();
                      String pathname = file.getAbsolutePath();
                      GenericPropertyList gpl = (GenericPropertyList) getProperties(new GenericPropertyList());
                      gpl.setUserPropertiesFileName(pathname);
                      System.out.println("Saving layer " + getName() + " properties to file: " + pathname);
                      gpl.saveProperties("# Map layer " + getName() + " properties ##");
                    }
                  }
                }
            };

            JButton jb = new JButton("Color Legend");
            jb.addActionListener(al);
            jb.setActionCommand(LegendCmd);
            palette.add(jb);


            jb = new JButton(i18n.get(SolutionListLayer.class, "setProperties", "Set Preferences"));
            jb.setActionCommand(DisplayPropertiesCmd);
            jb.addActionListener(al);
            palette.add(jb);

            jb = new JButton(i18n.get(SolutionListLayer.class, "saveProperties", "Save Preferences"));
            jb.setActionCommand(SaveCmd);
            jb.addActionListener(al);
            palette.add(jb);
            
            jb = new JButton(i18n.get(SolutionListLayer.class, "redraw", "Redraw Solution Layer"));
            jb.setActionCommand(RedrawCmd);
            jb.addActionListener(al);
            palette.add(jb);

        }

        return palette;
    }

    public void makeLegend() {

        if (legend != null) {
            legend.setVisible(false);
            legend.dispose();
            legend=null;
        }

        JPanel jp = new JPanel(new BorderLayout());

        JLabel jlabel =  null;
        double interval = 0.;
        if (colorBy.equalsIgnoreCase(ColorIntervalTypePropertyEditor.DEPTH)) {
            jlabel = new JLabel("DEPTH KM", JLabel.CENTER);
            interval = depthInterval;
        }
        else if (colorBy.equalsIgnoreCase(ColorIntervalTypePropertyEditor.TIME)) {
            jlabel = new JLabel("TIME " + timeIntervalUnits, JLabel.CENTER);
            interval = timeInterval;
        }
        else if (colorBy.equalsIgnoreCase(ColorIntervalTypePropertyEditor.TYPE)) { // aww added TYPE coloring 2007/08/22
            jlabel = new JLabel("EVENT TYPE", JLabel.CENTER);
        }
        else if (colorBy.equalsIgnoreCase(ColorIntervalTypePropertyEditor.GTYPE)) {
            jlabel = new JLabel("ORIGIN GTYPE", JLabel.CENTER);
        }
        else {
            jlabel = new JLabel("UNIFORM", JLabel.CENTER);
        }
        jp.add(jlabel,BorderLayout.NORTH);

        Box vbox = Box.createVerticalBox();
        Box hbox = null;
        String val = " ";
        JLabel clabel = null;
        Dimension dd = new Dimension(10,10);
        if (interval > 0.) { // Create colored interval labels (depth or time)
          for (int i = 0; i < intervalFillColors.length; i++) {
            hbox = Box.createHorizontalBox();
            clabel = new JLabel();
            clabel.setMaximumSize(dd);
            clabel.setOpaque(true);
            clabel.setBackground(intervalFillColors[i]);
            clabel.setBorder(BorderFactory.createLineBorder(Color.black));
            hbox.add(clabel);
            val = Concatenate.format(new StringBuffer(8), (i+1)*interval, 2, 2, 0).toString(); 
            jlabel = new JLabel(" <"+val);
            hbox.add(jlabel);
            hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
            vbox.add(hbox);
          }
        }
        else if (jlabel.getText().equals("UNIFORM")) {
            hbox = Box.createHorizontalBox();
            clabel = new JLabel();
            clabel.setMaximumSize(dd);
            clabel.setOpaque(true);
            clabel.setBackground(defaultFillColor);
            clabel.setBorder(BorderFactory.createLineBorder(Color.black));
            hbox.add(clabel);
            jlabel = new JLabel(" "+"uniform");
            hbox.add(jlabel);
            hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
            vbox.add(hbox);
        }
        else if (jlabel.getText().startsWith("EVENT")) {
          // Write out color map
          Set keys = etypeFillColorMap.keySet();
          if (keys.size() > 0) {
            String [] keyArray = (String []) keys.toArray(new String[keys.size()]);
            Arrays.sort(keyArray);
            for (int idx = 0; idx < keyArray.length; idx++) {
              hbox = Box.createHorizontalBox();
              clabel = new JLabel();
              clabel.setMaximumSize(dd);
              clabel.setOpaque(true);
              clabel.setBackground((Color)etypeFillColorMap.get(keyArray[idx]));
              clabel.setBorder(BorderFactory.createLineBorder(Color.black));
              hbox.add(clabel);
              jlabel = new JLabel(" "+keyArray[idx]);
              hbox.add(jlabel);
              hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
              vbox.add(hbox);
            }
          }
        }
        else { // Origin gtype
          Set keys = gtypeFillColorMap.keySet();
          if (keys.size() > 0) {
            String [] keyArray = (String []) keys.toArray(new String[keys.size()]);
            Arrays.sort(keyArray);
            for (int idx = 0; idx < keyArray.length; idx++) {
              hbox = Box.createHorizontalBox();
              clabel = new JLabel();
              clabel.setMaximumSize(dd);
              clabel.setOpaque(true);
              clabel.setBackground((Color)gtypeFillColorMap.get(keyArray[idx]));
              clabel.setBorder(BorderFactory.createLineBorder(Color.black));
              hbox.add(clabel);
              jlabel = new JLabel(" "+keyArray[idx]);
              hbox.add(jlabel);
              hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
              vbox.add(hbox);
            }
          }
        }
        //vbox.add(Box.createVerticalGlue());
        jp.add(vbox, BorderLayout.CENTER);
        legend = new JFrame("Legend");
        legend.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        legend.getContentPane().add(jp); 
        legend.setLocation(getLocationOnScreen());
        legend.pack();
        dd = legend.getSize();
        legend.setSize(dd.width+30,dd.height);
        legend.setVisible(true);
    }

    /**
     * Returns the MapMouseListener object that handles the mouse
     * events.
     * 
     * @return the MapMouseListener for the layer, or null if none
     */
    @Override
    public MapMouseListener getMapMouseListener() {
        return this;
    }

    //----------------------------------------------------------------
    // MapMouseListener interface methods
    //----------------------------------------------------------------

    /**
     * Return a list of the modes that are interesting to the
     * MapMouseListener. The source MouseEvents will only get sent to
     * the MapMouseListener if the mode is set to one that the
     * listener is interested in. Layers interested in receiving
     * events should register for receiving events in "select" mode:
     * <code>
     * <pre>
     * return new String[] { SelectMouseMode.modeID };
     * </pre>
     * <code>
     * @return String[] of modeID's
     * @see com.bbn.openmap.event.NavMouseMode#modeID
     * @see com.bbn.openmap.event.SelectMouseMode#modeID
     * @see com.bbn.openmap.event.NullMouseMode#modeID
     */
    @Override
    public String[] getMouseModeServiceList() {
        return new String[] { com.bbn.openmap.event.SelectMouseMode.modeID };
        //return getMouseModeIDsForEvents(); // ?
    }

    /**
     * Invoked when a mouse button has been pressed on a component.
     * 
     * @param e MouseEvent
     * @return true if the listener was able to process the event.
     */
    @Override
    public boolean mousePressed(MouseEvent e) {
        int clickCount = e.getClickCount();
        int mask = e.getModifiers();
        boolean status = false;
        //Example  ( (mask == (MouseEvent.BUTTON1_MASK | InputEvent.SHIFT_MASK) ) )
        if ( mask == MouseEvent.BUTTON1_MASK && clickCount >= 2)  {
          //System.out.println(getClass().getName() + " DEBUG DOUBLE click" );
          OMGraphicList omgList = getList();
          if (omgList != null && solList != null) {
            //System.out.println(getClass().getName() + " DEBUG DOUBLE click on solution");
            OMGraphic obj = omgList.findClosest(e.getX(), e.getY(), 2);
            if (obj != null) {
                Object o = obj.getAppObject();
                if (o != null) jiggle.loadSolution(((Solution) o).id.longValue(), jiggle.isNetworkModeLAN()); // false=>WAN, don't check for existance ?
                status = true;
            }
          }
        }
        else if (e.isPopupTrigger()) {               
            //System.out.println(getClass().getName() + " DEBUG MOUSE EVENT popup trigger");
        }
        else {
          //System.out.println(getClass().getName() + " DEBUG SINGLE left click" );
          OMGraphicList omgList = getList();
          if (omgList != null && solList != null) {
            //OMGraphic obj = omgList.findClosest(e.getX(), e.getY(), 4);
            OMGraphic obj = omgList.selectClosest(e.getX(), e.getY(), 2);
            if (obj != null) {
                lastSelectedGraphic = obj;
                // obj.setSelected(true); // OVERRIDE in subclass MasterView list differs from CatalogPanel list ?
                Object o = obj.getAppObject();
                if (o != null) {
                    Solution sol = (Solution) o;
                    solList.setSelected(sol); // use Jiggle for MasterView selection, possibly CatalogPanel here?
                    String dataStr = sol.toSummaryString();
                    fireRequestInfoLine(dataStr);
                    //fireRequestMessage(dataStr);
                    fireRequestToolTip(dataStr);
                    showingInfoLine = true;
                    //setSelectedSolution(sol); // repaint, but LDSEListener solList.setSelected does this call?
                    repaint();
                    status = true;
                }
            }
          }
        }
        //System.out.println(getClass().getName() + " DEBUG MOUSE EVENT return status: " + status);
        return status;
    }

    /**
     * Invoked when a mouse button has been released on a component.
     * 
     * @param e MouseEvent
     * @return true if the listener was able to process the event.
     */
    @Override
    public boolean mouseReleased(MouseEvent e) {
        if ( e.isPopupTrigger() ) {               
            //System.out.println(getClass().getName() + " DEBUG MOUSE RELEASED EVENT popup trigger");
            OMGraphicList omgList = getList();
            if (omgList != null) {
              OMGraphic obj = omgList.findClosest(e.getX(), e.getY(), 2);
              if (obj != null) {
                JPopupMenu jpm = new JPopupMenu();
                Object o = obj.getAppObject();
                if (o != null) {
                  final Solution s = (Solution) o;
                  AbstractAction a =
                    new AbstractAction() {
                        public void actionPerformed(ActionEvent e) {
                            jiggle.loadSolution(s.id.longValue(), jiggle.isNetworkModeLAN());  // false=>WAN, don't check for existance ?
                        }
                  };
                  a.putValue(Action.NAME, "Load Event " + s.getId());
                  jpm.add( new JMenuItem(a) );
                }
                jpm.add(new JMenuItem("Cancel"));
                jpm.show((java.awt.Component) e.getSource(), e.getX(), e.getY());

                return true;
              }
            }
        }
        return false;
    }

    /**
     * Invoked when the mouse has been clicked on a component. The
     * listener will receive this event if it successfully processed
     * <code>mousePressed()</code>, or if no other listener
     * processes the event. If the listener successfully processes
     * mouseClicked(), then it will receive the next mouseClicked()
     * notifications that have a click count greater than one.
     * 
     * @param e MouseEvent
     * @return true if the listener was able to process the event.
     */
    @Override
    public boolean mouseClicked(MouseEvent e) {
        //if (e.isPopupTrigger()) {}
        //System.out.println(getClass().getName() + " DEBUG MOUSE CLICKED EVENT popup trigger");
        return false;
    }

    /**
     * Invoked when the mouse enters a component.
     * 
     * @param e MouseEvent
     */
    @Override
    public void mouseEntered(MouseEvent e) { }

    /**
     * Invoked when the mouse exits a component.
     * 
     * @param e MouseEvent
     */
    @Override
    public void mouseExited(MouseEvent e) { }

    /**
     * Invoked when a mouse button is pressed on a component and then
     * dragged. The listener will receive these events if it
     * successfully processes mousePressed(), or if no other listener
     * processes the event.
     * 
     * @param e MouseEvent
     * @return true if the listener was able to process the event.
     */
    @Override
    public boolean mouseDragged(MouseEvent e) {
        return false;
    }

    /**
     * Invoked when the mouse button has been moved on a component
     * (with no buttons down).
     * 
     * @param e MouseEvent
     * @return true if the listener was able to process the event.
     */
    @Override
    public boolean mouseMoved(MouseEvent e) {
        // clean up display
        if (showingInfoLine) {
            showingInfoLine = false;
            fireRequestInfoLine("");
        }
        if (lastSelectedGraphic != null && lastSelectedGraphic.distance(e.getX(),e.getY()) > 10) {
          fireHideToolTip();
          getList().deselect();
          lastSelectedGraphic = null;
          repaint();
        }
        return false;
    }

    /**
     * Handle a mouse cursor moving without the button being pressed.
     * This event is intended to tell the listener that there was a
     * mouse movement, but that the event was consumed by another
     * layer. This will allow a mouse listener to clean up actions
     * that might have happened because of another motion event
     * response.
     */
    @Override
    public void mouseMoved() {}

    //----------------------------------------------------------------
    // PropertyConsumer Interface
    //----------------------------------------------------------------

    /**
     * Set the properties of the SolutionListLayer.
     * 
     * @param prefix String
     * @param props Properties
     */
    @Override
    public void setProperties(String aPrefix, Properties props) {
        super.setProperties(aPrefix, props);

        String prefix = PropUtils.getScopedPropertyPrefix(aPrefix);

        recenterBounds =
            PropUtils.booleanFromProperties(props, prefix + RECENTER_BOUNDS_PROPERTY, DEFAULT_MAP_RECENTER);

        viewNoneScale =
            PropUtils.floatFromProperties(props, prefix + VIEW_NONE_SCALE_PROPERTY, DEFAULT_VIEW_NONE_SCALE);

        colorBy = props.getProperty(prefix + COLOR_BY_PROPERTY, ColorIntervalTypePropertyEditor.UNIFORM);

        defaultFillColor =
            (Color) PropUtils.parseColorFromProperties(props, prefix + DEFAULT_FILL_COLOR_PROPERTY, defaultFillColorString);
        DEFAULT_FILL_COLORS = new Color [] { defaultFillColor };

        depthInterval =
            PropUtils.doubleFromProperties(props, prefix + DEPTH_INTERVAL_PROPERTY, DEFAULT_DEPTH_INTERVAL);
        timeInterval =
            PropUtils.doubleFromProperties(props, prefix + TIME_INTERVAL_PROPERTY, DEFAULT_TIME_INTERVAL);
        timeIntervalUnits =
            props.getProperty(prefix + TIME_INTERVAL_UNITS_PROPERTY, DEFAULT_TIME_INTERVAL_UNITS);
        radiusScaleFactor =
            PropUtils.floatFromProperties(props, prefix + RADIUS_SCALAR_PROPERTY, DEFAULT_RADIUS_SCALAR);

        etypeFillColorMap = getEventTypeColorsFromProperties(prefix, props);
        gtypeFillColorMap = getGTypeColorsFromProperties(prefix, props);

        //String [] colorStrings = PropUtils.stringArrayFromProperties(props, prefix + INTERVAL_COLORS_PROPERTY, " ");
        //intervalFillColors = getIntervalColorsFromStrings(colorStrings);
        intervalFillColors = getIntervalColorsFromProperties(prefix, props); // replaces above -aww 2008/10/27
    
    }

    private HashMap getEventTypeColorsFromProperties(String scopedPrefix, Properties props) {
        String prefix = scopedPrefix + "color.etype.";
        String [] jasiTypes = EventTypeMap3.getJasiTypeArray(); 
        HashMap aMap = new HashMap(jasiTypes.length);
        if (jasiTypes.length > 0) {
          // Get etypes and set color map, if no color property use default color
          for (int idx = 0; idx < jasiTypes.length; idx++) {
            aMap.put(jasiTypes[idx],
                    PropUtils.parseColorFromProperties(props, prefix + jasiTypes[idx], Integer.toHexString(defaultFillColor.getRGB())));
          }
        }
        //System.out.println("ColorMap:\n"+aMap.toString());
        return aMap;
    }

    private HashMap getGTypeColorsFromProperties(String scopedPrefix, Properties props) {
        String prefix = scopedPrefix + "color.gtype.";
        String [] jasiTypes = GTypeMap.getJasiTypeArray(); 
        HashMap aMap = new HashMap(jasiTypes.length);
        if (jasiTypes.length > 0) {
          // Get gtypes and set color map, if no color property use default color
          for (int idx = 0; idx < jasiTypes.length; idx++) {
            aMap.put(jasiTypes[idx],
                    PropUtils.parseColorFromProperties(props, prefix + jasiTypes[idx], Integer.toHexString(defaultFillColor.getRGB())));
          }
        }
        aMap.put("clear",
            PropUtils.parseColorFromProperties(props, prefix + "clear", Integer.toHexString(defaultFillColor.getRGB())));
        //System.out.println("ColorMap:\n"+aMap.toString());
        return aMap;
    }

    private Color [] getIntervalColorsFromProperties(String scopedPrefix, Properties props) {
        String prefix = scopedPrefix + "color.interval.";
        int idx = 1;
        ArrayList aList = new ArrayList();
        Color [] fillColors = DEFAULT_FILL_COLORS;
        String colorValue = props.getProperty(prefix + fmt.form(idx)); 
        Color c = null;
        while (colorValue != null && colorValue.length() > 0) {
          c = ColorFactory.parseColor(colorValue, false);
          if (c == null) {
            System.err.println(getName() + " Layer Error: unable to parse interval " + idx + " color string value: " + colorValue);
            break;
          }
          aList.add(c);
          idx++;
          colorValue = props.getProperty(prefix + fmt.form(idx)); 
        }
        if (aList.size() > 0) {
            fillColors = (Color []) aList.toArray(new Color[aList.size()]);
        }
        return fillColors;
    }

    private Color [] getIntervalColorsFromStrings(String [] colorStrings) {

        if (colorStrings == null) return DEFAULT_FILL_COLORS;
        int maxColors = colorStrings.length;
        if (maxColors < 0 ) return DEFAULT_FILL_COLORS;

        //System.out.println("SolutionListLayer: number of interval colors: " + maxColors);
        Color [] fillColors = new Color[maxColors];
        for (int i=0; i < maxColors; i++) {
            fillColors[i] = ColorFactory.parseColor(colorStrings[i], false);
        }

        return fillColors;
    }

    /**
     * Get the associated properties object.
     */
    @Override
    public Properties getProperties(Properties props) {
        return getProperties(propertyPrefix, props);
    }

    /**
     * Get the associated properties object. This method creates a
     * Properties object if necessary and fills it with the relevant
     * data for this layer. Relevant properties for SolutionListLayer
     * are the db URL to retrieve earthquake data from, and the
     * interval in milliseconds (see class description.)
     */
    public Properties getProperties(String aPrefix, Properties props) {
        props = super.getProperties(props);

        String prefix = PropUtils.getScopedPropertyPrefix(aPrefix);

        props.put(prefix + RECENTER_BOUNDS_PROPERTY, String.valueOf(recenterBounds));
        props.put(prefix + VIEW_NONE_SCALE_PROPERTY, String.valueOf(viewNoneScale));
        props.put(prefix + RADIUS_SCALAR_PROPERTY, String.valueOf(radiusScaleFactor));
        props.put(prefix + COLOR_BY_PROPERTY, colorBy);
        props.put(prefix + DEPTH_INTERVAL_PROPERTY, String.valueOf(depthInterval));
        props.put(prefix + TIME_INTERVAL_PROPERTY, String.valueOf(timeInterval));
        props.put(prefix + TIME_INTERVAL_UNITS_PROPERTY, timeIntervalUnits);

        String colorString = (defaultFillColor == null) ?
            defaultFillColorString : Integer.toHexString(defaultFillColor.getRGB());
        props.put(prefix + DEFAULT_FILL_COLOR_PROPERTY, colorString);

        // Write out etype color map
        Set keys = etypeFillColorMap.keySet();
        if (keys.size() > 0) {
          String [] keyArray = (String []) keys.toArray(new String[keys.size()]);
          Arrays.sort(keyArray);
          for (int idx = 0; idx < keyArray.length; idx++) {
              props.put(prefix+"color.etype."+ keyArray[idx], Integer.toHexString(((Color)etypeFillColorMap.get(keyArray[idx])).getRGB()) );
          }
        }

        // Write out gtype color map
        keys = gtypeFillColorMap.keySet();
        if (keys.size() > 0) {
          String [] keyArray = (String []) keys.toArray(new String[keys.size()]);
          Arrays.sort(keyArray);
          for (int idx = 0; idx < keyArray.length; idx++) {
              props.put(prefix+"color.gtype."+ keyArray[idx], Integer.toHexString(((Color)gtypeFillColorMap.get(keyArray[idx])).getRGB()) );
          }
        }

        //props.put(prefix + INTERVAL_COLORS_PROPERTY, toIntervalColorPropertyValueString(intervalFillColors));
        for (int i = 0; i < intervalFillColors.length; i++) { // replaces above -aww 2008/10/27
          props.put(prefix+"color.interval."+fmt.form(i+1), ColorFactory.getHexColorString(intervalFillColors[i]));
        }

        return props;
    }

    /*
    private static String toIntervalColorPropertyValueString(Color [] colors) {
        StringBuffer sb = new StringBuffer(128);
        for (int i = 0; i < colors.length; i++) {
          sb.append(ColorFactory.getHexColorString(colors[i])).append(" ");
        }
        return sb.toString();
    }
    */

    /**
     * Supplies the propertiesInfo object associated with this
     * SolutionListLayer object. Contains the human readable
     * describtions of the properties and the
     * <code>initPropertiesProperty</code> (see Inspector class.)
     */
    @Override
    public Properties getPropertyInfo(Properties info) {
        StringBuffer sb = new StringBuffer(2048);
        sb.append(RECENTER_BOUNDS_PROPERTY).append(" ");
        sb.append(VIEW_NONE_SCALE_PROPERTY).append(" ");
        sb.append(RADIUS_SCALAR_PROPERTY).append(" ");
        sb.append(DEPTH_INTERVAL_PROPERTY).append(" ");
        sb.append(TIME_INTERVAL_PROPERTY).append(" ");
        sb.append(TIME_INTERVAL_UNITS_PROPERTY).append(" ");

        sb.append(COLOR_BY_PROPERTY).append(" ");
        sb.append(DEFAULT_FILL_COLOR_PROPERTY).append(" ");

        Set keys = etypeFillColorMap.keySet();
        String [] keyArray = new String[0];
        if (keys.size() > 0) {
          keyArray = (String []) keys.toArray(new String[keys.size()]);
          Arrays.sort(keyArray);
          for (int idx = 0; idx < keyArray.length; idx++) {
              sb.append("color.etype."+ keyArray[idx]).append(" ");
          }
        }

        keys = gtypeFillColorMap.keySet();
        keyArray = new String[0];
        if (keys.size() > 0) {
          keyArray = (String []) keys.toArray(new String[keys.size()]);
          Arrays.sort(keyArray);
          for (int idx = 0; idx < keyArray.length; idx++) {
              sb.append("color.gtype."+ keyArray[idx]).append(" ");
          }
        }

        //sb.append(INTERVAL_COLORS_PROPERTY).append(" ");
        for (int idx = 0; idx < intervalFillColors.length; idx++) { // replaces above -aww 2008/10/27
          sb.append("color.interval."+fmt.form(idx+1)).append(" ");
        }

        info = super.getPropertyInfo(info);
        info.put(initPropertiesProperty, sb.toString());

        info.put(RECENTER_BOUNDS_PROPERTY, "True recenters map to location of event choosen from Catalog list.");
        info.put(VIEW_NONE_SCALE_PROPERTY, "Max zoom scale showing any Catalog Solution.");
        info.put(RADIUS_SCALAR_PROPERTY, "Scale symbol radius by magnitude * factor");
        info.put(DEPTH_INTERVAL_PROPERTY, "Delta depth interval for color change");
        info.put(TIME_INTERVAL_PROPERTY, "Delta time interval for color change");
        info.put(TIME_INTERVAL_UNITS_PROPERTY, "Time interval units (DAY, WEEK, MONTH, YEAR)");
        info.put(COLOR_BY_PROPERTY, "Color fill event graphics by depth interval or time interval, or uniformly (\"depth\",\"time\",\"uniform\")");
        info.put(COLOR_BY_PROPERTY + ScopedEditorProperty, "org.trinet.jiggle.map.ColorIntervalTypePropertyEditor");

        I18n i18n = Environment.getI18n();
        String str = i18n.get(SolutionListLayer.class, DEFAULT_FILL_COLOR_PROPERTY, i18n.TOOLTIP,
                "Fill Color string value used when coloring uniformly (not by interval or type)");
        info.put(DEFAULT_FILL_COLOR_PROPERTY, str);
        str = i18n.get(SolutionListLayer.class, DEFAULT_FILL_COLOR_PROPERTY, "Uniform event color");
        info.put(DEFAULT_FILL_COLOR_PROPERTY + LabelEditorProperty, str);
        info.put(DEFAULT_FILL_COLOR_PROPERTY + ScopedEditorProperty,
                "com.bbn.openmap.util.propertyEditor.ColorPropertyEditor");

        String prop = null;
        if (keyArray.length > 0) {
          for (int idx = 0; idx < keyArray.length; idx++) {
            prop = "color.etype."+ keyArray[idx];
            str = i18n.get(SolutionListLayer.class, prop, i18n.TOOLTIP,
                "Fill Color string value used for event type " + keyArray[idx]);
            info.put(prop, str);
            str = i18n.get(SolutionListLayer.class, prop, "Event type " + keyArray[idx]);
            info.put(prop + LabelEditorProperty, str);
            info.put(prop + ScopedEditorProperty, "com.bbn.openmap.util.propertyEditor.ColorPropertyEditor");
          }
        }

        prop = null;
        if (keyArray.length > 0) {
          for (int idx = 0; idx < keyArray.length; idx++) {
            prop = "color.gtype."+ keyArray[idx];
            str = i18n.get(SolutionListLayer.class, prop, i18n.TOOLTIP,
                "Fill Color string value used for origin gtype " + keyArray[idx]);
            info.put(prop, str);
            str = i18n.get(SolutionListLayer.class, prop, "Origin gtype " + keyArray[idx]);
            info.put(prop + LabelEditorProperty, str);
            info.put(prop + ScopedEditorProperty, "com.bbn.openmap.util.propertyEditor.ColorPropertyEditor");
          }
        }

        //info.put(INTERVAL_COLORS_PROPERTY, "Color String values for range interval colors (from min to max range)");
        if (intervalFillColors.length > 0) { // replaces above -aww 2008/10/27
          String ivalStr = null;
          for (int idx = 0; idx < intervalFillColors.length; idx++) {
            ivalStr = fmt.form(idx+1);
            prop = "color.interval."+ ivalStr;
            str = i18n.get(SolutionListLayer.class, prop, i18n.TOOLTIP,
                "Fill Color string value used for interval " + ivalStr);
            info.put(prop, str);
            str = i18n.get(SolutionListLayer.class, prop, "Time/depth interval " + ivalStr);
            info.put(prop + LabelEditorProperty, str);
            info.put(prop + ScopedEditorProperty, "com.bbn.openmap.util.propertyEditor.ColorPropertyEditor");
          }
        }

        return info;
    }

    //ListDataStateListener interface methods
    @Override
    public void intervalAdded(ListDataStateEvent e) {
        if (e.getSource() == solList) processSolutionListLDSE(e); // ??
    }

    @Override
    public void intervalRemoved(ListDataStateEvent e) {
        if (e.getSource() == solList) processSolutionListLDSE(e); // ??
    }

    @Override
    public void contentsChanged(ListDataStateEvent e) {
        if (e.getSource() == solList) processSolutionListLDSE(e); // ??
    }

    @Override
    public void orderChanged(ListDataStateEvent e) {
        // no-op, order doesn't matter yet
    }

    @Override
    public void stateChanged(ListDataStateEvent e) {
        if (e.getSource() == solList) {
          if (e.getStateChange().getStateName().equals(StateChange.SELECTED) )
             setSelectedSolution((Solution) e.getStateChange().getValue()); // color/visibility update of graphics
          else processSolutionListLDSE(e); // ??
        }
    }

    private void processSolutionListLDSE(ListDataStateEvent e) {

        if (solList == null || solList.isEmpty()) return;

        // Lazy approach, do all events the same way
        SolutionList src = (SolutionList) e.getSource();

        setTimeIntervalStart(src); // added -aww 2008/02/13 

        int first  = e.getIndex0();
        int last   = e.getIndex1();
        StateChange sc = e.getStateChange();
        Object data = sc.getValue();
        boolean removeOnly = (e.getType() == ListDataStateEvent.INTERVAL_REMOVED)  || 
                             (sc.getStateName() == StateChange.DELETED); 
        /*
        System.out.println(
          "DEBUG " + getClass().getName() + " processChannelListLDSE for: " 
          + src.getClass().getName()
          + " type: " + sc.getStateName() + " from " + first + " to " + last
          + " value is : " + ((data == null) ? "null" : data.getClass().getName())
          + " removeOnly: " + removeOnly
        );
        */

        // Check for valid StateChange data element, else we don't know what changed,
        // so for this weird case we have to recreate a new list of Solution graphics
        if (data == null) {
          getList().clear(); // clear old graphics list
          setList(null);    
        }
        else if (first > -1 && last >= first) { // valid indices
          if (e.getType() == ListDataStateEvent.STATE_CHANGED) {
            //For "stateChanged" case, the StateChange data is value of the new state, 
            // not the list element(s) changed, so have to use indices of the source
            // list to lookup element (hopefully source hasn't been resorted).
            for (int idx=first; idx <= last; idx++) {
              updateGraphicListForSolution( (Solution) src.get(idx), removeOnly );
            }
          }
          // Data either single element or array of such
          else if (data instanceof Solution) {
              updateGraphicListForSolution( (Solution) data, removeOnly );
          }
          else { // assume event data object is an array of changed list elements
              Object [] d = (Object []) data;
              for (int idx=0; idx < d.length; idx++) {
                updateGraphicListForSolution( (Solution) d[idx], removeOnly );
              }
          }
        }

        OMGraphicList omgList = getList();
        // If we have a list refresh the graphics, otherwise rebuild anew
        if (omgList != null) {
          omgList.project(getProjection());
          repaint();
        }
        else {
          doPrepare();  // use thread to construct anew
        }
    }

    private void updateGraphicListForSolution(Solution sol, boolean removeOnly) {
        if (sol == null) return;
        OMGraphicList omgList = getList();
        if (omgList == null) {

            return;
        }
        OMGraphic g = OMUtils.getOMGraphicWithAppObject(omgList, sol);
        if (g != null) omgList.remove(g);
        if (removeOnly) return;
        g = makeOMGraphic(sol);
        if (g != null) omgList.add(g);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getSource() instanceof MapBean) {
          String propName = evt.getPropertyName();
          if (propName.equals(EngineIF.SOLVED)) {
              // get graphics list and find graphic with appObject matching Solution
              Object obj = evt.getNewValue();
              Solution sol = null;
              if (obj instanceof Solution) {
                  sol = (Solution)obj;
              }
              else if (obj instanceof Magnitude) {
                  sol = ((Magnitude)obj).getAssociatedSolution();
              }
              resetGraphicLocationFor(sol);
          }
        }
    }

    protected OMGraphic resetGraphicLocationFor(Solution sol) {
        OMPoint p = null;
        if (sol != null) {
           OMGraphicList omgList = getList();
           p = (OMPoint) OMUtils.getOMGraphicWithAppObject(omgList, sol);
           if (p != null) {
             p.setLat(sol.lat.doubleValue());
             p.setLon(sol.lon.doubleValue());
             if (getProjection() != null) {
               omgList.project(getProjection());
               // move arrow to new location if any
               if (lastSelectedSolGraphic != null &&
                       lastSelectedSolGraphic.getAppObject() == sol) showArrowAt(p);
               repaint();
             }
           }
        }
        return p;
    }

    @Override
    public List getItemsForMapMenu(MapMouseEvent mme) {
        //System.out.println(getClass().getName() + " DEBUG getItemsForMapMenu" );
        List aList = new ArrayList();
        JMenuItem jmi = new JMenuItem("Recenter");
        final MapMouseEvent evt = mme;
        jmi.addActionListener( new ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                LatLonPoint llp = LatLonPoint.valueOf(getProjection().inverse(evt.getX(), evt.getY()));
                MapHandler mh = (MapHandler) getBeanContext();
                if (mh != null) {
                    JiggleMapBean mapBean =
                            (JiggleMapBean) mh.get("org.trinet.jiggle.JiggleMapBean");
                    if (mapBean != null) mapBean.setCenter(llp);
                }
            }
        });
        aList.add(jmi);
        //
        jmi = new JMenuItem("Zoom +");
        jmi.addActionListener( new ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                MapHandler mh = (MapHandler) getBeanContext();
                if (mh != null) {
                    JiggleMapBean mapBean =
                            (JiggleMapBean) mh.get("org.trinet.jiggle.JiggleMapBean");
                    if (mapBean != null) mapBean.zoom(new ZoomEvent(this, ZoomEvent.RELATIVE, 0.5f));
                }
            }
        });
        aList.add(jmi);

        jmi = new JMenuItem("Zoom -");
        jmi.addActionListener( new ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                MapHandler mh = (MapHandler) getBeanContext();
                if (mh != null) {
                    JiggleMapBean mapBean =
                            (JiggleMapBean) mh.get("org.trinet.jiggle.JiggleMapBean");
                    if (mapBean != null) mapBean.zoom(new ZoomEvent(this, ZoomEvent.RELATIVE, 2.0f));
                }
            }
        });
        aList.add(jmi);

        // no-op return option
        aList.add(new JMenuItem("Cancel"));

        return aList;
    }

    @Override
    public List getItemsForOMGraphicMenu(OMGraphic omg) {
        //System.out.println(getClass().getName() + " DEBUG getItemsForGraphicMenu" );
        return null;
    }

}
