package org.trinet.util;
import java.beans.*;

public abstract class AbstractGenericEngine extends Object implements EngineIF {

    protected boolean debug = false;
    protected boolean verbose  = false;
  
    protected GenericPropertyList props;
  
    protected String statusString = "";
    protected String resultsString = "";
    protected boolean solveSuccess  = false;

  
    private PropertyChangeSupport propChangeSupport = null;
  
    protected String engineName; 
    /** Valid initialization and can process data.*/
    protected boolean engineIsValid = false; // do we need this? - aww
  
    protected AbstractGenericEngine() {
        propChangeSupport = new PropertyChangeSupport(this);
    }
  
    public void setDebug(boolean tf) {
        debug = tf;
        if (debug) verbose = true;
    }
    public void setVerbose(boolean tf) {
        verbose = tf;
    }
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propChangeSupport.addPropertyChangeListener(listener);
    }
    public void addPropertyChangeListener(String propName, PropertyChangeListener listener) {
        propChangeSupport.addPropertyChangeListener(propName, listener);
    }
    public void firePropertyChange(PropertyChangeEvent evt) {
        propChangeSupport.firePropertyChange(evt);
    }
    public void firePropertyChange(String propName, boolean oldValue, boolean newValue) {
        propChangeSupport.firePropertyChange(propName, oldValue, newValue);
    }
    public void firePropertyChange(String propName, int oldValue, int newValue) {
        propChangeSupport.firePropertyChange(propName, oldValue, newValue);
    }
    public void firePropertyChange(String propName, Object oldValue, Object newValue) {
        propChangeSupport.firePropertyChange(propName, oldValue, newValue);
    }
     /* jdk 1.4 only
    public PropertyChangeListener[] getPropertyChangeListeners() {
        return propChangeSupport.getPropertyChangeListeners();
    }
    public PropertyChangeListener[] getPropertyChangeListeners(String propName) {
        return propChangeSupport.getPropertyChangeListeners(propName);
    }
    */
    public boolean hasListeners(String propName) {
        return propChangeSupport.hasListeners(propName);
    }
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propChangeSupport.removePropertyChangeListener(listener);
    }
    public void removePropertyChangeListener(String propName, PropertyChangeListener listener) {
        propChangeSupport.removePropertyChangeListener(propName, listener);
    }
  
    public void initializeEngine() {
        setDefaultProperties();
        initIO();
    }

    protected void initIO() {
      if (props.isSpecified("debug")) debug = props.getBoolean("debug");
      else props.setProperty("debug", debug);

      if (props.isSpecified("verbose")) verbose = props.getBoolean("verbose");
      else props.setProperty("verbose", verbose);

      if (debug) verbose = true;

      if (debug) { // changed from "verbose" test -aww 04/06/2006
         System.out.println(((engineName != null) ? engineName : getClass().getName())+
                         " property file: "+props.getUserPropertiesFileName());
         props.dumpProperties();
      }
    }

  
//
// found in DK/DG revision, below methods are declared in ConfigurationSourceIF 
// do we want to keep this duality, configure(...)/setProperties(...) ?  -aww
    public void configure() {
        initializeEngine();
    }
    public void configure(int source, String location, String section) {
        if (source == ConfigurationSourceIF.CONFIG_SRC_FILE) setProperties(location);
    }

// end of ConfigurationSourceIF methods
//
    public GenericPropertyList getProperties() {
        return props;
    }

    public void setProperties(GenericPropertyList props) {
        this.props = props;
        if (props == null) props = new GenericPropertyList(); 
        initializeEngine();
    }

    /** Path qualified filename to user property file. */
    public boolean setProperties(String propFileName) {
        boolean status = loadProperties(propFileName);
        initializeEngine();
        return status;
    }

    public void setDefaultProperties() {
        if (props == null) props = new GenericPropertyList(); 
        debug = false;
        verbose  = false;
    }

    protected boolean loadProperties(String propFileName) {
        props =  new GenericPropertyList(); // empty list
        //Force user property filename to input
        props.setUserPropertiesFileName(propFileName); // aww 07/22/2004
        return props.readPropertiesFile(propFileName);
    }

    abstract public boolean solve(Object data);
  
    /** Returns false by default, subclass should implement desired functionality. */
    public boolean success() {
        return solveSuccess;
    }
  
    public String getResultsString() {
        return resultsString;
    }
    public void setResultsString(String results) {
        resultsString = results;
    }
  
    public String getStatusString() {
        return statusString;
    }
    public void setStatusString(String status) {
        statusString = status;
    }
  
    public void reset() {
        resultsString = "";
        statusString = "";
        solveSuccess = false;
    }
  
    public boolean isEngineValid() {
        return engineIsValid;
    }
    public void setEngineName(String name) {
        engineName = name;
    }
    public String getEngineName() {
        return engineName;
    }
}
