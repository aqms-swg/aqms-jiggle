package org.trinet.util.graphics.table;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;

public class SaveJDBCAdapterToASCII implements ActionListener {
     JDialog about = null;
     boolean canceled = false;

     public void save (JDBCAdapter adapter, Frame f) {
         FileDialog fd = new FileDialog (f, "Save content to which file", FileDialog.SAVE);
         fd.setVisible(true);
         if (fd.getFile () == null)
             return;
         JRadioButton tabDelimited = null;
         JRadioButton commaDelimited = null;
         JCheckBox includeHeaderRow = null;
         if (about == null) {
             about = new JDialog(f, "Table to ASCII file", true);
            
             tabDelimited = new JRadioButton ("Tab delimited");
             tabDelimited.setSelected (true);
             commaDelimited = new JRadioButton ("Comma delimited");
             JRadioButton spaceDelimited = new JRadioButton ("Space delimited");
             ButtonGroup group = new ButtonGroup ();
             group.add (tabDelimited);
             group.add (commaDelimited);
             group.add (spaceDelimited);
             includeHeaderRow = new JCheckBox ("Include header row");
             includeHeaderRow.setSelected (true);
             JButton OKButton = new JButton ("Save");
             OKButton.setActionCommand ("Save");
             OKButton.addActionListener (this);
             JButton cancelButton = new JButton ("Cancel");
             cancelButton.setActionCommand ("Cancel");
             cancelButton.addActionListener (this);
             JPanel buttonPanel = new JPanel ();
             buttonPanel.setLayout (new FlowLayout (FlowLayout.CENTER, 5, 5));
             buttonPanel.add (OKButton);
             buttonPanel.add (cancelButton);
             JPanel radioButtons = new JPanel ();
             radioButtons.setLayout (new GridLayout (5,1,5,5));
             radioButtons.add (new JLabel ("File format"));
             radioButtons.add (tabDelimited);
             radioButtons.add (commaDelimited);
             radioButtons.add (spaceDelimited);
             radioButtons.add (includeHeaderRow);

             Container panel = about.getContentPane();
             panel.setLayout(new BorderLayout(5, 5));
             panel.add(radioButtons, BorderLayout.CENTER);
             panel.add(buttonPanel, BorderLayout.SOUTH);
        }
        
        // NOTE: these lines center the about dialog in the
        // current window. Some older Swing versions have
        // a bug in getLocationOnScreen() and they may not
        // make this behave properly.
        about.pack();
        about.setVisible(true);
        if (canceled)
            return;
//        String delimiter = tabDelimited.isSelected()?"\t":",";
	String delimiter = " ";
	if (tabDelimited.isSelected()) delimiter = "\t";
	if (commaDelimited.isSelected()) delimiter = ",";
        try {
            BufferedWriter bw = new BufferedWriter (new FileWriter (fd.getDirectory()+fd.getFile()));
            if (includeHeaderRow.isSelected()) {
                if (adapter.getColumnCount()>0) {
                    bw.write (adapter.getColumnName(0));
                    for (int i=1; i<adapter.getColumnCount(); i++)
                        bw.write (delimiter+adapter.getColumnName(i));
                    bw.newLine ();
                }
            }
            if (adapter.getColumnCount()>0) {
                for (int i=0; i<adapter.getRowCount(); i++) {
                    bw.write (adapter.dbRepresentation (0, adapter.getValueAt (i, 0)));
                    for (int ii=1; ii<adapter.getColumnCount(); ii++)
                        bw.write (delimiter+adapter.dbRepresentation (ii, adapter.getValueAt (i, ii)));
                    bw.newLine ();
                }
            }
            bw.close ();
        } catch (IOException ioe) {
        }
//        Debug.println("Saved table to file : "+fd.getDirectory()+fd.getFile() );
    }

    public void actionPerformed (ActionEvent ae) {
        if (ae.getActionCommand().equals("cancel"))
            canceled = true;
        about.setVisible (false);
    }
 
    /*
    static public void main(String args[]) {
        SaveJDBCAdapterToASCII save = new SaveJDBCAdapterToASCII ();
        save.save (null, new JFrame());
    }
    */
}
