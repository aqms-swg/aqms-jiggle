package test.org.trinet.jasi;

import org.junit.jupiter.api.Test;
import org.trinet.jasi.Units;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class UnitsTests {

    @Test
    public void verify_get_string_returns_unknown_string_with_unknown_int() {
        assertEquals("unknown", Units.getString(Units.UNKNOWN));
    }

    @Test
    public void verify_get_string_returns_counts_string_with_counts_int() {
        assertEquals("counts", Units.getString(Units.COUNTS));
    }

    @Test
    public void verify_get_string_returns_s_string_with_seconds_int() {
        assertEquals("s", Units.getString(Units.SECONDS));
    }

    @Test
    public void verify_get_string_returns_m_string_with_mn_int() {
        assertEquals("m", Units.getString(Units.M));
    }

    @Test
    public void verify_get_string_returns_cm_string_with_cm_int() {
        assertEquals("cm", Units.getString(Units.CM));
    }

    @Test
    public void verify_get_string_returns_mm_string_with_mm_int() {
        assertEquals("mm", Units.getString(Units.MM));
    }

    @Test
    public void verify_get_string_returns_microns_string_with_microns_int() {
        assertEquals("mc", Units.getString(Units.MICRONS));
    }

    @Test
    public void verify_get_string_returns_nm_string_with_nm_int() {
        assertEquals("nm", Units.getString(Units.NM));
    }

    @Test
    public void verify_get_string_returns_ms_string_with_ms_int() {
        assertEquals("ms", Units.getString(Units.MS));
    }

    @Test
    public void verify_get_string_returns_cms_string_with_cms_int() {
        assertEquals("cms", Units.getString(Units.CMS));
    }

    @Test
    public void verify_get_string_returns_mms_string_with_mms_int() {
        assertEquals("mms", Units.getString(Units.MMS));
    }

    @Test
    public void verify_get_string_returns_mss_string_with_mss_int() {
        assertEquals("mss", Units.getString(Units.MSS));
    }

    @Test
    public void verify_get_string_returns_cmss_string_with_cmss_int() {
        assertEquals("cmss", Units.getString(Units.CMSS));
    }

    @Test
    public void verify_get_string_returns_mmss_string_with_mmss_int() {
        assertEquals("mmss", Units.getString(Units.MMSS));
    }

    @Test
    public void verify_get_string_returns_e_string_with_ergs_int() {
        assertEquals("e", Units.getString(Units.ERGS));
    }

    @Test
    public void verify_get_string_returns_iovs_string_with_iovs_int() {
        assertEquals("iovs", Units.getString(Units.IOVS));
    }

    @Test
    public void verify_get_string_returns_spa_string_with_spa_int() {
        assertEquals("spa", Units.getString(Units.SPA));
    }

    @Test
    public void verify_get_string_returns_countscmsec2_string_with_cntmss_int() {
        assertEquals("counts/(cm/sec2)", Units.getString(Units.CntCMSS));
    }

    @Test
    public void verify_get_string_returns_countcmsec_string_with_cntcms_int() {
        assertEquals("counts/(cm/sec)", Units.getString(Units.CntCMS));
    }

    @Test
    public void verify_get_string_returns_countms_string_with_cntms_int() {
        assertEquals("COUNTS/M/S", Units.getString(Units.CntMS));
    }

    @Test
    public void verify_get_string_returns_countms2_string_with_cntmss_int() {
        assertEquals("COUNTS/M/S**2", Units.getString(Units.CntMSS));
    }

    @Test
    public void verify_get_string_returns_dums_string_with_dums_int() {
        assertEquals("DU/M/S", Units.getString(Units.DuMS));
    }

    @Test
    public void verify_get_string_returns_dums2_string_with_dumss_int() {
        assertEquals("DU/M/S**2", Units.getString(Units.DuMSS));
    }

    @Test
    public void verify_get_string_returns_volts_string_with_volts_int() {
        assertEquals("volts", Units.getString(Units.Volts));
    }

    @Test
    public void verify_get_string_returns_mvolts_string_with_microvolts_int() {
        assertEquals("mVolts", Units.getString(Units.MicroVolts));
    }

    @Test
    public void verify_get_string_returns_g_string_with_g_int() {
        assertEquals("g", Units.getString(Units.G));
    }

    @Test
    public void verify_get_string_returns_dycm_string_with_dycm_int() {
        assertEquals("dycm", Units.getString(Units.DYCM));
    }

    @Test
    public void verify_get_string_returns_dug_string_with_dug_int() {
        assertEquals("DU/G", Units.getString(Units.DUG));
    }

    @Test
    public void verify_get_int_returns_unknown_int_with_unknown_string() {
        assertEquals(Units.UNKNOWN, Units.getInt("unknown"));
    }

    @Test
    public void verify_get_int_returns_unknown_int_with_unknown_all_caps_string() {
        assertEquals(Units.UNKNOWN, Units.getInt("UNKNOWN"));
    }

    @Test
    public void verify_get_int_returns_seconds_int_with_s_string() {
        assertEquals(Units.SECONDS, Units.getInt("s"));
    }

    @Test
    public void verify_get_int_returns_m_int_with_m_string() {
        assertEquals(Units.M, Units.getInt("m"));
    }

    @Test
    public void verify_get_int_returns_cm_int_with_cm_string() {
        assertEquals(Units.CM, Units.getInt("cm"));
    }

    @Test
    public void verify_get_int_returns_mm_int_with_mm_string() {
        assertEquals(Units.MM, Units.getInt("mm"));
    }

    @Test
    public void verify_get_int_returns_mc_int_with_mc_string() {
        assertEquals(Units.MICRONS, Units.getInt("mc"));
    }

    @Test
    public void verify_get_int_returns_nm_int_with_nm_string() {
        assertEquals(Units.NM, Units.getInt("nm"));
    }

    @Test
    public void verify_get_int_returns_ms_int_with_ms_string() {
        assertEquals(Units.MS, Units.getInt("ms"));
    }

    @Test
    public void verify_get_int_returns_cms_int_with_cms_string() {
        assertEquals(Units.CMS, Units.getInt("cms"));
    }

    @Test
    public void verify_get_int_returns_mms_int_with_mms_string() {
        assertEquals(Units.MMS, Units.getInt("mms"));
    }

    @Test
    public void verify_get_int_returns_mss_int_with_mss_string() {
        assertEquals(Units.MSS, Units.getInt("mss"));
    }

    @Test
    public void verify_get_int_returns_cmss_int_with_cmss_string() {
        assertEquals(Units.CMSS, Units.getInt("cmss"));
    }

    @Test
    public void verify_get_int_returns_mmss_int_with_mmss_string() {
        assertEquals(Units.MMSS, Units.getInt("mmss"));
    }

    @Test
    public void verify_get_int_returns_e_int_with_e_string() {
        assertEquals(Units.ERGS, Units.getInt("e"));
    }

    @Test
    public void verify_get_int_returns_iovs_int_with_iovs_string() {
        assertEquals(Units.IOVS, Units.getInt("iovs"));
    }

    @Test
    public void verify_get_int_returns_spa_int_with_spa_string() {
        assertEquals(Units.SPA, Units.getInt("spa"));
    }

    @Test
    public void verify_get_int_returns_cntmss_int_with_countscmsec2_string() {
        assertEquals(Units.CntCMSS, Units.getInt("counts/(cm/sec2)"));
    }

    @Test
    public void verify_get_int_returns_cntcms_int_with_countscmsec_string() {
        assertEquals(Units.CntCMS, Units.getInt("counts/(cm/sec)"));
    }

    @Test
    public void verify_get_int_returns_cntms_int_with_COUNTSMS_string() {
        assertEquals(Units.CntMS, Units.getInt("COUNTS/M/S"));
    }

    @Test
    public void verify_get_int_returns_cntmss_int_with_COUNTSMS2_string() {
        assertEquals(Units.CntMSS, Units.getInt("COUNTS/M/S**2"));
    }

    @Test
    public void verify_get_int_returns_dums_int_with_DUMS_string() {
        assertEquals(Units.DuMS, Units.getInt("DU/M/S"));
    }

    @Test
    public void verify_get_int_returns_dumss_int_with_DUMS2_string() {
        assertEquals(Units.DuMSS, Units.getInt("DU/M/S**2"));
    }

    @Test
    public void verify_get_int_returns_volts_int_with_volts_string() {
        assertEquals(Units.Volts, Units.getInt("volts"));
    }

    @Test
    public void verify_get_int_returns_mvolts_int_with_mVolts_string() {
        assertEquals(Units.MicroVolts, Units.getInt("mVolts"));
    }

    @Test
    public void verify_get_int_returns_g_int_with_g_string() {
        assertEquals(Units.G, Units.getInt("g"));
    }

    @Test
    public void verify_get_int_returns_dycm_int_with_dycm_string() {
        assertEquals(Units.DYCM, Units.getInt("dycm"));
    }

    @Test
    public void verify_get_int_returns_dug_int_with_DUG_string() {
        assertEquals(Units.DUG, Units.getInt("DU/G"));
    }

    @Test
    public void verify_is_legal_returns_true_with_value_of_1() {
        assertTrue(Units.isLegal(1));
    }

    @Test
    public void verify_is_legal_returns_false_with_value_of_0() {
        assertFalse(Units.isLegal(0));
    }

    @Test
    public void verify_is_legal_returns_false_with_negative_value() {
        assertFalse(Units.isLegal(-1));
    }

    @Test
    public void verify_is_legal_returns_false_with_large_positive_value() {
        assertFalse(Units.isLegal(10000));
    }

    @Test
    public void verify_is_legal_returns_true_with_unknown_string() {
        assertTrue(Units.isLegal("unknown"));
    }

    @Test
    public void verify_is_legal_returns_true_with_unknown_all_caps_string() {
        assertTrue(Units.isLegal("UNKNOWN"));
    }

    @Test
    public void verify_is_legal_returns_true_with_s_string() {
        assertTrue(Units.isLegal("s"));
    }

    @Test
    public void verify_is_legal_returns_true_with_m_string() {
        assertTrue(Units.isLegal("m"));
    }

    @Test
    public void verify_is_legal_returns_true_with_cm_string() {
        assertTrue(Units.isLegal("cm"));
    }

    @Test
    public void verify_is_legal_returns_true_with_mm_string() {
        assertTrue(Units.isLegal("mm"));
    }

    @Test
    public void verify_is_legal_returns_true_with_mc_string() {
        assertTrue(Units.isLegal("mc"));
    }

    @Test
    public void verify_is_legal_returns_true_with_nm_string() {
        assertTrue(Units.isLegal("nm"));
    }

    @Test
    public void verify_is_legal_returns_true_with_ms_string() {
        assertTrue(Units.isLegal("ms"));
    }

    @Test
    public void verify_is_legal_returns_true_with_cms_string() {
        assertTrue(Units.isLegal("cms"));
    }

    @Test
    public void verify_is_legal_returns_true_with_mss_string() {
        assertTrue(Units.isLegal("mss"));
    }

    @Test
    public void verify_is_legal_returns_true_with_cmss_string() {
        assertTrue(Units.isLegal("cmss"));
    }

    @Test
    public void verify_is_legal_returns_true_with_mmss_string() {
        assertTrue(Units.isLegal("mmss"));
    }

    @Test
    public void verify_is_legal_returns_true_with_e_string() {
        assertTrue(Units.isLegal("e"));
    }

    @Test
    public void verify_is_legal_returns_true_with_iovs_string() {
        assertTrue(Units.isLegal("iovs"));
    }

    @Test
    public void verify_is_legal_returns_true_with_spa_string() {
        assertTrue(Units.isLegal("spa"));
    }

    @Test
    public void verify_is_legal_returns_true_with_countscmsec2_string() {
        assertTrue(Units.isLegal("counts/(cm/sec2)"));
    }

    @Test
    public void verify_is_legal_returns_true_with_countscmsec_string() {
        assertTrue(Units.isLegal("counts/(cm/sec)"));
    }

    @Test
    public void verify_is_legal_returns_true_with_COUNTSMS_string() {
        assertTrue(Units.isLegal("COUNTS/M/S"));
    }

    @Test
    public void verify_is_legal_returns_true_with_COUNTSMS2_string() {
        assertTrue(Units.isLegal("COUNTS/M/S**2"));
    }

    @Test
    public void verify_is_legal_returns_true_with_DUMS_string() {
        assertTrue(Units.isLegal("DU/M/S"));
    }

    @Test
    public void verify_is_legal_returns_true_with_DUMS2_string() {
        assertTrue(Units.isLegal("DU/M/S**2"));
    }

    @Test
    public void verify_is_legal_returns_true_with_volts_string() {
        assertTrue(Units.isLegal("volts"));
    }

    @Test
    public void verify_is_legal_returns_true_with_mVolts_string() {
        assertTrue(Units.isLegal("mVolts"));
    }

    @Test
    public void verify_is_legal_returns_true_with_g_string() {
        assertTrue(Units.isLegal("g"));
    }

    @Test
    public void verify_is_legal_returns_true_with_dycm_string() {
        assertTrue(Units.isLegal("dycm"));
    }

    @Test
    public void verify_is_legal_returns_true_with_DUG_string() {
        assertTrue(Units.isLegal("DU/G"));
    }

    @Test
    public void verify_is_legal_returns_true_with_special_case_countcms_string() {
        assertTrue(Units.isLegal("counts/cm/s"));
    }

    @Test
    public void verify_is_legal_returns_true_with_special_case_countscmsec_string() {
        assertTrue(Units.isLegal("counts/cm/sec"));
    }

    @Test
    public void verify_is_legal_returns_true_with_special_case_countscmsec2_string() {
        assertTrue(Units.isLegal("counts/cm/sec^2"));
    }

    @Test
    public void verify_is_legal_returns_true_with_special_case_COUNTSCMS2_string() {
        assertTrue(Units.isLegal("COUNTS/CM/S**2"));
    }

    @Test
    public void verify_is_legal_returns_false_with_illegal_string() {
        assertFalse(Units.isLegal("illegal"));
    }

    @Test
    public void verify_is_velocity_returns_true_with_cms_unit() {
        assertTrue(Units.isVelocity(Units.CMS));
    }

    @Test
    public void verify_is_velocity_returns_true_with_cntcms_unit() {
        assertTrue(Units.isVelocity(Units.CntCMS));
    }

    @Test
    public void verify_is_velocity_returns_true_with_ms_unit() {
        assertTrue(Units.isVelocity(Units.MS));
    }

    @Test
    public void verify_is_velocity_returns_true_with_mms_unit() {
        assertTrue(Units.isVelocity(Units.MMS));
    }

    @Test
    public void verify_is_velocity_returns_true_with_dums_unit() {
        assertTrue(Units.isVelocity(Units.DuMS));
    }

    @Test
    public void verify_is_velocity_returns_true_with_cntms_unit() {
        assertTrue(Units.isVelocity(Units.CntMS));
    }

    @Test
    public void verify_is_velocity_returns_false_with_non_velocity_unit() {
        assertFalse(Units.isVelocity(Units.CMSS));
    }

    @Test
    public void verify_is_counts_returns_true_with_counts_units() {
        assertTrue(Units.isCounts(Units.COUNTS));
    }

    @Test
    public void verify_is_counts_returns_false_with_non_counts_units() {
        assertFalse(Units.isCounts(Units.CMS));
    }

    @Test
    public void verify_is_acceleration_returns_true_with_cmss_units() {
        assertTrue(Units.isAcceleration(Units.CMSS));
    }

    @Test
    public void verify_is_acceleration_returns_true_with_cntcmss_units() {
        assertTrue(Units.isAcceleration(Units.CntCMSS));
    }

    @Test
    public void verify_is_acceleration_returns_true_with_mss_units() {
        assertTrue(Units.isAcceleration(Units.MSS));
    }

    @Test
    public void verify_is_acceleration_returns_true_with_mmss_units() {
        assertTrue(Units.isAcceleration(Units.MMSS));
    }

    @Test
    public void verify_is_acceleration_returns_true_with_dumss_units() {
        assertTrue(Units.isAcceleration(Units.DuMSS));
    }

    @Test
    public void verify_is_acceleration_returns_true_with_cntmss_units() {
        assertTrue(Units.isAcceleration(Units.CntMSS));
    }

    @Test
    public void verify_is_acceleration_returns_true_with_g_units() {
        assertTrue(Units.isAcceleration(Units.G));
    }

    @Test
    public void verify_is_displacement_returns_true_with_cm_units() {
        assertTrue(Units.isDisplacement(Units.CM));
    }

    @Test
    public void verify_is_displacement_returns_true_with_mm_units() {
        assertTrue(Units.isDisplacement(Units.MM));
    }

    @Test
    public void verify_is_displacement_returns_true_with_m_units() {
        assertTrue(Units.isDisplacement(Units.M));
    }

    @Test
    public void verify_is_displacement_returns_true_with_microns_units() {
        assertTrue(Units.isDisplacement(Units.MICRONS));
    }

    @Test
    public void verify_is_displacement_returns_true_with_nm_units() {
        assertTrue(Units.isDisplacement(Units.NM));
    }

    @Test
    public void verify_is_displacement_returns_false_with_non_displacement_units() {
        assertFalse(Units.isDisplacement(Units.CntMSS));
    }

    @Test
    public void verify_convert_returns_nan_with_incompatible_units_from_list_1_and_list_2() {
        assertEquals(Double.NaN, Units.convert(0, Units.M, Units.CMS));
    }

    @Test
    public void verify_convert_returns_nan_with_incompatible_units_from_list_1_and_list_3() {
        assertEquals(Double.NaN, Units.convert(0, Units.M, Units.CMSS));
    }

    @Test
    public void verify_convert_returns_nan_with_incompatible_units_from_list_2_and_list_3() {
        assertEquals(Double.NaN, Units.convert(0, Units.CMS, Units.MSS));
    }

    @Test
    public void verify_convert_from_common_returns_correct_value_with_m_units() {
        double value = 100.0;
        assertEquals(value / 100.0, Units.convertFromCommon(value, Units.M));
    }

    @Test
    public void verify_convert_from_common_returns_correct_value_with_ms_units() {
        double value = 100.0;
        assertEquals(value / 100.0, Units.convertFromCommon(value, Units.MS));
    }

    @Test
    public void verify_convert_from_common_returns_correct_value_with_cm_units() {
        double value = 100.0;
        assertEquals(value / 100.0, Units.convertFromCommon(value, Units.MSS));
    }

    @Test
    public void verify_convert_from_common_returns_correct_value_with_cms_units() {
        double value = 100.0;
        assertEquals(value, Units.convertFromCommon(value, Units.CM));
    }

    @Test
    public void verify_convert_from_common_returns_correct_value_with_cmss_units() {
        double value = 100.0;
        assertEquals(value, Units.convertFromCommon(value, Units.CMS));
    }

    @Test
    public void verify_convert_from_common_returns_correct_value_with_mm_units() {
        double value = 100.0;
        assertEquals(value * 10.0, Units.convertFromCommon(value, Units.MM));
    }

    @Test
    public void verify_convert_from_common_returns_correct_value_with_mms_units() {
        double value = 100.0;
        assertEquals(value * 10.0, Units.convertFromCommon(value, Units.MMS));
    }

    @Test
    public void verify_convert_from_common_returns_correct_value_with_mmss_units() {
        double value = 100.0;
        assertEquals(value * 10.0, Units.convertFromCommon(value, Units.MMSS));
    }

    @Test
    public void verify_convert_from_common_returns_correct_value_with_microns_units() {
        double value = 100.0;
        assertEquals(value * 10000.0, Units.convertFromCommon(value, Units.MICRONS));
    }

    @Test
    public void verify_convert_from_common_returns_correct_value_with_nm_units() {
        double value = 100.0;
        assertEquals(value * 10000000.0, Units.convertFromCommon(value, Units.NM));
    }

    @Test
    public void verify_convert_from_common_returns_nan_value_with_illegal_units() {
        double value = 100.0;
        assertEquals(Double.NaN, Units.convertFromCommon(value, Units.COUNTS));
    }

    @Test
    public void verify_convert_to_cgs_returns_correct_value_with_m_units() {
        double value = 100.0;
        assertEquals(value * 100.0, Units.convertToCGS(value, Units.M));
    }

    @Test
    public void verify_convert_to_cgs_returns_correct_value_with_ms_units() {
        double value = 100.0;
        assertEquals(value * 100.0, Units.convertToCGS(value, Units.MS));
    }

    @Test
    public void verify_convert_to_cgs_returns_correct_value_with_cm_units() {
        double value = 100.0;
        assertEquals(value * 100.0, Units.convertToCGS(value, Units.MSS));
    }

    @Test
    public void verify_convert_to_cgs_returns_correct_value_with_cms_units() {
        double value = 100.0;
        assertEquals(value, Units.convertToCGS(value, Units.CM));
    }

    @Test
    public void verify_convert_to_cgs_returns_correct_value_with_cmss_units() {
        double value = 100.0;
        assertEquals(value, Units.convertToCGS(value, Units.CMS));
    }

    @Test
    public void verify_convert_to_cgs_returns_correct_value_with_mm_units() {
        double value = 100.0;
        assertEquals(value / 10.0, Units.convertToCGS(value, Units.MM));
    }

    @Test
    public void verify_convert_to_cgs_returns_correct_value_with_mms_units() {
        double value = 100.0;
        assertEquals(value / 10.0, Units.convertToCGS(value, Units.MMS));
    }

    @Test
    public void verify_convert_to_cgs_returns_correct_value_with_mmss_units() {
        double value = 100.0;
        assertEquals(value / 10.0, Units.convertToCGS(value, Units.MMSS));
    }

    @Test
    public void verify_convert_to_cgs_returns_correct_value_with_microns_units() {
        double value = 100.0;
        assertEquals(value / 10000.0, Units.convertToCGS(value, Units.MICRONS));
    }

    @Test
    public void verify_convert_to_cgs_returns_correct_value_with_nm_units() {
        double value = 100.0;
        assertEquals(value / 10000000.0, Units.convertToCGS(value, Units.NM));
    }

    @Test
    public void verify_convert_to_cgs_returns_nan_value_with_illegal_units() {
        double value = 100.0;
        assertEquals(Double.NaN, Units.convertToCGS(value, Units.COUNTS));
    }

    @Test
    public void verify_getCGSunit_returns_correct_value_with_m_units() {
        assertEquals(Units.CM, Units.getCGSunit(Units.M));
    }


    @Test
    public void verify_getCGSunit_returns_correct_value_with_cm_units() {
        assertEquals(Units.CM, Units.getCGSunit(Units.CM));
    }

    @Test
    public void verify_getCGSunit_returns_correct_value_with_mm_units() {
        assertEquals(Units.CM, Units.getCGSunit(Units.MM));
    }

    @Test
    public void verify_getCGSunit_returns_correct_value_with_microns_units() {
        assertEquals(Units.CM, Units.getCGSunit(Units.MICRONS));
    }

    @Test
    public void verify_getCGSunit_returns_correct_value_with_nm_units() {
        assertEquals(Units.CM, Units.getCGSunit(Units.NM));
    }

    @Test
    public void verify_getCGSunit_returns_correct_value_with_ms_units() {
        assertEquals(Units.CMS, Units.getCGSunit(Units.MS));
    }

    @Test
    public void verify_getCGSunit_returns_correct_value_with_cms_units() {
        assertEquals(Units.CMS, Units.getCGSunit(Units.CMS));
    }

    @Test
    public void verify_getCGSunit_returns_correct_value_with_mms_units() {
        assertEquals(Units.CMS, Units.getCGSunit(Units.MMS));
    }

    @Test
    public void verify_getCGSunit_returns_correct_value_with_mss_units() {
        assertEquals(Units.CMSS, Units.getCGSunit(Units.MSS));
    }

    @Test
    public void verify_getCGSunit_returns_correct_value_with_cmss_units() {
        assertEquals(Units.CMSS, Units.getCGSunit(Units.CMSS));
    }

    @Test
    public void verify_getCGSunit_returns_correct_value_with_mmss_units() {
        assertEquals(Units.CMSS, Units.getCGSunit(Units.MMSS));
    }
}
