package org.trinet.jasi.coda;
import org.trinet.jasi.*;
import org.trinet.util.*;
public interface CodaGeneratorIF {

    public static final int NOCAL_MD = 1; // subtype id for method parms
    public static final int SOCAL_MD = 2;
    public static final int NOCAL_MC = 3;
    public static final int SOCAL_MC = 4;

    String getAlgorithm() ;
    void setAlgorithm(String name) ;
    boolean calcCoda( double secsPerSample, double secsAfterPWaveAtStartingOffset,
                             float[] timeSeriesAmps, int startingIdxOffset, int timeSeriesSamples,
                             double responseNormalizedCodaCutoffAmp, double responseClippingAmp,
                             double lta) ; 
    Coda setCodaResults(Coda results) ;
    double getQFix() ;
    void setQFix(double qFix) ;
    void setResetOnClipping(boolean value) ;
    boolean isResetOnClipping() ;
    int getMinGoodWindows() ;
    void setMinGoodWindows(int numberOfWindows) ;
    int getMaxGoodWindows() ;
    double getMaxCodaDurationSecs() ;
    void setMaxGoodWindows(int maxCount) ;
    double getWindowSize() ;
    void setWindowSize(double windowSize) ;
    void setCodaStartSignalToNoiseRatio(double snrToStart) ;
    double getCodaStartSignalToNoiseRatio() ;
    void setCodaCutoffSignalToNoiseRatio(double snrToEnd) ;
    double getCodaCutoffSignalToNoiseRatio() ;
    void setPassThruNoiseDecayAmpRatio(double snrToEnd) ;
    double getPassThruNoiseDecayAmpRatio() ;
    double getBiasLTASecs();
    void setBiasLTASecs(double secs);
    int getOverlapWindowMax();
    boolean getOverlapStartingWindows();

    String inputToString();
    String outputToString();
    String outputHeaderString();
    String getExitStatusString();

    void setFilter(FilterIF filter);
    void setFilter(Channel ch);
    boolean hasFilter();
    boolean isFilterEnabled();
    void enableFilter();
    void disableFilter();
    String describeFilter();
}
