package org.trinet.jdbc ;
import org.trinet.jdbc.datatypes.DataObject;
import org.trinet.util.Utils;

/** Data members Contain the default null values for the declared data types.
*
* Methods compare input values to null defaults to test for null equivalence.
*/

public class NullValueDb {
/**  = Integer.MAX_VALUE */
    public static final int NULL_INT = Integer.MAX_VALUE;
/**  = Long.MAX_VALUE */
    public static final long NULL_LONG = Long.MAX_VALUE;
/**  = Double.NaN */
    public static final double NULL_DOUBLE = Double.NaN;
/**  = Float.NaN */
    public static final float NULL_FLOAT = Float.NaN;
/**  = "NULL" */
    public static final String NULL_STRING = "NULL"; 	// to flag for using "NULL";
/**  = java.sql.Time(0) */
    public static final java.sql.Time NULL_TIME = new java.sql.Time(0l);
/**  = java.sql.Timestamp(0) */
    public static final java.sql.Timestamp NULL_TIMESTAMP = new java.sql.Timestamp(0l); 
/**  = java.sql.Date(0) */
    public static final java.sql.Date NULL_DATE = new java.sql.Date(0l); 
/**  = null */
    public static final Object NULL_OBJECT = null;
    
/** Returns true if number == NULL_INT. */
    public static final boolean isNull(int number) {
	return (number == NULL_INT) ;
    }
    
/** Returns true if number == NULL_LONG. */
    public static final boolean isNull(long number) {
	return (number == NULL_LONG) ;
    }
    
/** Returns true if number == NULL_FLOAT.
 * https://www.baeldung.com/java-not-a-number
 * */
    public static final boolean isNull(float number) {
	return (number != number) ;
    }
    
/** Returns true if number == NULL_DOUBLE. */
    public static final boolean isNull(double number) {
	return (number != number) ;
    }

/** Returns true if number.intValue() == NULL_INT. */
    public static final boolean isNull(Integer number) {
	return (number.intValue() == NULL_INT) ;
    }
    
/** Returns true if number.longValue() == NULL_LONG. */
    public static final boolean isNull(Long number) {
	return (number.longValue() == NULL_LONG) ;
    }
    
/** Returns true if number.isNaN() == true. */
    public static final boolean isNull(Float number) {
	return number.isNaN() ;
    }
    
/** Returns true if number.isNaN() == true. */
    public static final boolean isNull(Double number) {
	return number.isNaN() ;
    }

/** Returns true if value.equals(NULL_TIME). */
    public static final boolean isNull(java.sql.Time value) {
	return value.equals(NULL_TIME) ;
    }
    
/** Returns true if value.equals(NULL_TIMESTAMP). */
    public static final boolean isNull(java.sql.Timestamp value) {
	return value.equals(NULL_TIMESTAMP) ;
    }
    
    public static final boolean isNull(java.sql.Date value) {
	return value.equals(NULL_DATE) ;
    }
    
/** Returns true if (str==null || str.equalsIgnoreCase(NULL_STRING) || str.equals("NaN")).*/
    public static final boolean isNull(String str) {
	return (str == null || str.equalsIgnoreCase(NULL_STRING) || str.equals("NaN")) ;
    }
    
/** Returns true if Object is null. */
    public static final boolean isNull(Object obj) {
	return (obj == null ||
               (obj instanceof DataObject && ((DataObject)obj).isNull()));
    }

    /** Returns true if (isNull(String) || Utils.isBlank(String)). */
    public static final boolean isBlank(String str) {
        return (isNull(str) || Utils.isBlank(str)) ;
    }

/** Returns true if (isNull(String) || String.isEmpty(String)). */
    public static final boolean isEmpty(String str) {
        return (isNull(str) || str.isEmpty()) ;
    }
}
