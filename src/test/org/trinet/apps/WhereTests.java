package test.org.trinet.apps;

import org.junit.jupiter.api.Test;
import org.trinet.apps.ArcImport;
import org.trinet.apps.Where;
import test.common.DatabaseTests;

import static org.junit.jupiter.api.Assertions.fail;

public class WhereTests extends DatabaseTests {

    @Test
    public void verify_where_main_function_runs() throws Exception {
        try {
            Where.main(new String[]{postgreSQLContainer.getHost(), getDbName(),
                    getDbUsername(), getDbPassword(), getSubProtocol(), getPortNumber(), getDatabaseDriver()});
        } catch (Exception e) {
            fail("Where main function failed to run successfully");
        }
    }
}