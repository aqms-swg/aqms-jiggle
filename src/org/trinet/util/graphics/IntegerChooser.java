package org.trinet.util.graphics;

/**
 * Extention of JComboBox for choosing an integer from a range. JComboBoxes handle lists of
 * Objects and are generic. This is specific to a list of Integer objects.
 */
import javax.swing.*;
import javax.swing.plaf.basic.*;
import java.awt.*;

import java.awt.event.*;
import org.trinet.util.graphics.text.*;


public class IntegerChooser extends NumberChooser {

       int selected;

/*public IntegerChooser () {

    setEditable(editable);
    addListeners();

}
*/
/**
 * Create a chooser that displays integers with the given range.
 * The variable 'selected' will change dynamically with the value of the chooser box so
 * that no special getValue method needs to be called by the caller. By default the box
 * is editable.
 * Values in ComboBox list are Strings. Values outside the given range are not tested for.
 */
public IntegerChooser(int min, int max, int interval, int sel)
{

    selected = sel;

    // make a vector of selection values
    // !!! THIS IS SLOW !!!!
    for (int i = min; i <= max; i = i + interval)
    {
    addItem( String.valueOf(i).intern() );      // add the selection to the box list
//  addItem( ""+i );        // add the selection to the box list
    }
    int nchar = String.valueOf(max).length();

    setSelectedItem( String.valueOf(sel).intern() );
 
    // make the width of the thing match the max value
    int wid = getFont().getSize();
    int hig = this.getHeight();
    setSize (wid * nchar, hig);

      setEditable(editable);
      addListeners();

}


/** Added action listeners */
private void addListeners() {
// the action listerner automatically updates 'selected' when some uses the pull-down
// part of the chooser OR hits <CR> after typing in the text area.
    addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
//              System.out.println ("action performed.");
              NumberChooser nc = (NumberChooser) e.getSource();
              selected = (int) nc.getSelectedValue();
         }
         });

// This does the format check when the box gets edited
    ComboBoxEditor editor = getEditor();
    editor.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
             getSelectedValue();
             /*
             String str = (String) getSelectedItem();
             try {
               selected = Integer.parseInt(str);
             } catch (NumberFormatException evt) {
               System.out.println (" % Bad number format in IntegerChooser: " + "\""+  str + "\"" );
    // fall back to previously selected value
               setSelectedItem( String.valueOf(selected) );
             }
             */
          }
      });

}
/**
 * Return the currently selected value. If there's a format error the previously
 * selected value is returned.
 */
public double getSelectedValue()
{
    String str = (String) getSelectedItem();
    if (str == null) return selected;
    try {
     selected = Integer.parseInt(str);
    }
    catch (NumberFormatException e)
    {
    System.out.println (" % Bad number format in IntegerChooser: " + "\""+  str + "\"" );
    // fall back to previously selected value
        setSelectedItem( String.valueOf(selected) );
    return selected;
    }

    return selected;
}

/*
    public static void main(String s[]) {

        JFrame frame = new JFrame("Main");
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });

        final IntegerChooser chooser = new IntegerChooser(-2, 20, 1, 3);

//        frame.getContentPane().add("North", numberChooser);

        JPanel panel = new JPanel(new BorderLayout());
        panel.add (chooser, BorderLayout.CENTER);
        JButton okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {

           public void actionPerformed(ActionEvent e) {
               System.out.println ("value = "+(int)chooser.getSelectedValue());
           }
        });

        panel.add(okButton, BorderLayout.SOUTH);
        frame.getContentPane().add(panel);


        frame.pack();
        frame.setVisible(true);


    }
*/
} // end of class

