package org.trinet.util.graphics.table;
import java.awt.Font;
import org.trinet.jdbc.datatypes.DataObject;
import org.trinet.util.LeapSeconds;

public class DateTimeRenderer extends ColorableTextCellRenderer {

    public DateTimeRenderer() { super(); }
    public DateTimeRenderer(int fontSize) { super(fontSize); }
    public DateTimeRenderer(Font font) { super(font); }

    public void setValue(Object value) {
        if (value == null) {
            super.setValue("");
        }
        else if (value instanceof Number) {
            super.setValue(LeapSeconds.trueToString(((Number)value).doubleValue(),"yyyy-MM-dd HH:mm:ss.fff").substring(0)); // for UTC leap secs -aww 2008/02/07
        }
        else if (value instanceof DataObject) {
            super.setValue(LeapSeconds.trueToString(((DataObject)value).doubleValue(),"yyyy-MM-dd HH:mm:ss.fff").substring(0)); // for UTC leap secs -aww 2008/02/07
        }
        else {
            super.setValue(value);
        }
    }
}
