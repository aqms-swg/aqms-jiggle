package org.trinet.util.locationengines;
import java.beans.*;
import org.trinet.jasi.*;
import org.trinet.util.*;
import org.trinet.jasi.engines.HypoMagDelegatePropertyList;

/**
 * Base case which can be used by subclass implementations to provide an
 * location engine service on a remote server.
 */
public abstract class AbstractLocationEngineService extends AbstractLocationEngine implements LocationEngineServiceIF {

    /** Server timeout interval, default = 5000 (5 sec). Without this socket
        reads would block forever if server didn't respond. */
    protected int serverTimeoutMillis = 5000;
    protected int port = 0;
    protected String serverAddress = null;
    protected String serviceName = null;

    /** Protected, use factory method to create concrete subclass instance. */
    protected AbstractLocationEngineService() { }

    public static LocationEngineIF create(String sClassName) {

        LocationEngineIF locEng = null;
        try {
          locEng = (LocationEngineIF) Class.forName(sClassName).newInstance();
        }
        catch (ClassNotFoundException ex) {
          ex.printStackTrace();
        }
        catch (InstantiationException ex) {
          ex.printStackTrace();
        }
        catch (IllegalAccessException ex) {
          ex.printStackTrace();
        }

        return locEng;
    }

    /** Set properties, does not make a serivce connection. */
    public void initializeEngine() {
      super.initializeEngine();
      boolean haveService = false;
      if (props instanceof HypoMagDelegatePropertyList) {
         HypoMagDelegatePropertyList hmdProps = (HypoMagDelegatePropertyList) props;
         LocationServerGroup lsg = hmdProps.getLocationServerGroup();
         if (lsg != null) {
            LocationServiceDescIF s = lsg.getSelectedService();
            if ( s != null) {
              serviceName = s.getServiceName();
              serverAddress = s.getIPAddress();
              port          = s.getPort();
              serverTimeoutMillis = s.getServerTimeoutMillis(); 
              haveService = true;
            }
         }
      }
      if (! haveService) {
        serviceName = "default";
        serverAddress = props.getProperty("locationEngineAddress");
        port          = props.getInt("locationEnginePort");
        // if undefined timeout value - use default
        if ( props.isSpecified("locationEngineTimeoutMillis") ) {
          int value = props.getInt("locationEngineTimeoutMillis");
          if (value > -1) serverTimeoutMillis = value; 
        }
      }
    }


    public void setServiceName(String name)  {
        serviceName = name;
    }
    public String getServiceName()  {
        return serviceName;
    }

    /*
    public String getHostName() {
      if (hostName != null) return hostName;
      if (serverAddress == null) return null;
      int idx = serverAddress.indexOf(".");
    // to be robust could implement reverse DNS lookup of a numeric ip?
      if (idx > 0) {
        hostName = serverAddress.substring(0,idx);
      }
      else { // punt here
        hostName = serverAddress;
      }
      return hostName;
    }
    public void setHostName(String name) {
      hostName = name;
    }
    */


    public String getIPAddress() {
      return serverAddress;
    }
    public void setIPAddress(String serverIPAddress) {
      serverAddress = serverIPAddress;
    }

    public int getPort() {
      return port;
    }
    public void setPort(int port) {
      this.port = port;
    }

    public String describeConnection() {
      StringBuffer sb = new StringBuffer(96);
      sb.append(serviceName).append(".").append(engineName).append("@").append(serverAddress).append(":").append(port);
      return sb.toString();
    }

    public String toString() {
      StringBuffer sb = new StringBuffer(96);
      sb.append(describeConnection());
      sb.append( " timeoutMillis: ").append(serverTimeoutMillis);
      return sb.toString();
    }

    /**
     * ServerIPAddress has the form: "XXX.XXX.XX.154" or "splat.gps.caltech.edu"
     */
    /** Sets the remote server location and makes a connection.
     * If data are changed, it should reestablish a service connection.
     * when invoked.
     * */
    public void setServer(String serverIPAddress, int serverPort) {
       // has URL or port changed?
       if (!serverIPAddress.equals(serverAddress) ||
           port != serverPort ) {

         disconnect();          // close old socket
         serverAddress = serverIPAddress;
         port = serverPort;

         if (connect()) {       // open new one
           statusString = "New connection: url= "+serverIPAddress+ " port= "+port;
         } else {
           statusString = "Failed connection: url"+serverIPAddress+ " port="+port;
         }
       }
    }

    public void setServerTimeoutMillis(int millis) {
      serverTimeoutMillis = millis;
    }

    public int getServerTimeoutMillis() {
      return serverTimeoutMillis;
    }

    /** Set the remote server location via properties.
     * If changed, should reestablish the service connection.
     * */
    public void setServiceByProperties(String propFileName) {
        setProperties(propFileName);
    }

    public boolean setProperties(String propFileName) {
        // method override added here as insurance to force disconnect of socket 
        disconnect(); // insurance
        return super.setProperties(propFileName);
    }

    /** Default does a no-op, since client may have no command control,
     * may be automatic by server. Returns false.
     * */
    public boolean disconnect() { return false; }

    /** Default does a no-op, since client may have no command control,
     * may be automatic by server. Returns false.
     * */
    public boolean connect() { return false; }

    /** Returns true if initialized and connected, otherwise false. */
    public boolean hasLocationService() {
        return isEngineValid();
    }

}   // end of AbstractLocationEngineService class

