// Migrated common "reading" method function to abstracted reader class
// TODO: Extend JasiDbReader to JasiReadingDbReader to include any getBy...
// code common to PhaseTN,AmplitudeTN, and CodaTN etc.
//
package org.trinet.jasi.TN;

import java.util.*;
import java.sql.*;

import org.trinet.jasi.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

/**
 * PhaseTN.java
 * schema-specific implementation
 */

/*

  There are two types of phases (distingushed by 'fromDbase' = true/false):
        1) those that were read from the dbase
                - these would never be deleted, updated, or inserted
                - you would never delete or update an AssocArO
                - you may create a new AssocArO associating them with an new origin

        2) those created by the application
                - there's nothing in the dbase to update or delete
          - they must be inserted in the dbase to save them
                - you MUST create a new AssocArO for them if they are to be meaningful
 */

public class PhaseTN extends Phase implements DbOriginAssocIF, DbReadableJasiObjectIF {

    protected static boolean debug = false;

    //private JasiDbReader jasiDataReader =  new JasiDbReader(this); // if state dependent, not static
    //Can be static so long as parseData() does not depend on state (like joinType in AmplitudeTN)
    //private static JasiDbReader jasiDataReader = new JasiDbReader();
    private static JasiDbReader jasiDataReader =  new JasiDbReader("org.trinet.jasi.TN.PhaseTN");

    /*
    static {
      //jasiDataReader.setFactoryClassName(getClass().getName());
      setDebug(JasiDatabasePropertyList.debugTN);
      if (JasiDatabasePropertyList.debugSQL) jasiDataReader.setDebug(true);
    }
    */

    // The standard start to most SQL queries
    protected static String sqlPrefix =
        //"SELECT a.ARID,a.DATETIME,a.STA,a.NET,a.AUTH,a.SUBSOURCE,a.CHANNEL,a.CHANNELSRC,a.SEEDCHAN," +
        "SELECT a.ARID,TRUETIME.getEpoch(a.DATETIME,'UTC'),a.STA,a.NET,a.AUTH,a.SUBSOURCE,a.CHANNEL,a.CHANNELSRC,a.SEEDCHAN," +
        "a.LOCATION,a.IPHASE,a.QUAL,a.FM,a.DELTIM,a.QUALITY,a.RFLAG," + // a.LDDATE," + removed lddate
        "o.IMPORTANCE,o.DELTA,o.SEAZ,o.IN_WGT,o.WGT,o.TIMERES,o.EMA,o.SDELAY" +  // removed orid
        " FROM Arrival a, AssocArO o WHERE (a.arid=o.arid)";
    private static final String psBySolutionString = sqlPrefix + " AND (o.orid=?) ORDER BY a.datetime";
    private static final String psByTimeString = sqlPrefix + " AND a.DATETIME BETWEEN TRUETIME.putEpoch(?,'UTC') AND TRUETIME.putEpoch(?,'UTC')";
    private static final String psByAridString = sqlPrefix + " AND o.arid=?";

    /*
    protected static String sqlPrefix =
        "Select "+ Arrival.QUALIFIED_COLUMN_NAMES+","+ AssocArO.QUALIFIED_COLUMN_NAMES+
        " from Arrival, AssocArO where (Arrival.arid = AssocArO.arid) ";
    private static final String psBySolutionString = sqlPrefix + " and (AssocArO.orid=?) order by Arrival.datetime";
    private static final String psByTimeString = sqlPrefix + " and Arrival.datetime between TRUETIME.putEpoch(?,'UTC') and TRUETIME.putEpoch(?,'UTC')";
    private static final String psByAridString = sqlPrefix + " and AssocArO.arid=?";
    */

    //The associated origin identifier in the data source. 
    //protected DataLong orid           = new DataLong();
    /** The arrival identifier in the data source. */
    protected DataLong arid           = new DataLong();

    /** True if data was orginally read from the data source.
     * Used to know if a commit requires 'insert' or an 'update' */
    protected boolean fromDbase = false;

    /**
     * Default Constructor
    */
    public PhaseTN() {
        if (jasiDataReader != null) jasiDataReader.setDebug(JasiDatabasePropertyList.debugSQL);
        setDebug(JasiDatabasePropertyList.debugTN);
    }

    public static void setDebug(boolean tf) {
        debug = tf;
        if (debug && jasiDataReader != null) jasiDataReader.setDebug(true);
    }

    public String toAssocIdString() {
        StringBuffer sb = new StringBuffer(super.toAssocIdString());
        sb.append(" orid: ").append(getOridValue());
        return sb.toString();
    }

    /** Copy the input Phase's data into this Phase. */
    public boolean replace(Phase ph) {
        // ***** CHECK *****
        // if setValue(obj) method of DataXXX sets null false 
        // for input true == NullValueDb.isNull(obj)?
        //setChannelObj( (Channel) ph.getChannelObj().clone() );
        setChannelObj(ph.getChannelObj()); // aww 04/05/2005 change to use jr wrapper
        description = ph.description;
        processingState = ph.processingState;        // rflag
        sol = ph.sol ;
        datetime.setValue(ph.datetime.doubleValue()) ;

        // could implement in Channel a DistAzElev object to copy?
        setDistance(ph.getDistance());
        setHorizontalDistance(ph.getHorizontalDistance()); //aww
        setAzimuth(ph.getAzimuth());

        /* Use assignment below to be consistent with update state?
        emergenceAngle.setValue(ph.emergenceAngle.doubleValue()) ;
        weightIn.setValue(ph.weightIn.doubleValue()) ;
        weightOut.setValue(ph.weightOut.doubleValue()) ;
        residual.setValue(ph.residual.doubleValue()) ;
        delay.setValue()ph.delay.doubleValue()) ;
        */
        emergenceAngle = ph.emergenceAngle;
        weightIn = ph.weightIn ;
        weightOut = ph.weightOut ;
        residual = ph.residual ;
        delay = ph.delay ; // aww added model tt delay 
        importance = ph.importance ; // aww added importance 2008/07/14
        deltaTime = ph.deltaTime ; // aww added deltaTime 2010/01/24

        return true;
    }

    /** Commit changes or delete to underlying DataSource. The action taken will
     *  depend on the state of the Phase. It may or may not be originally
     *  from the data source, it may be deleted, unassociated, modified, or
     *  unchanged.  To write to the DataSource, the flag
     *  DataSource.isWriteBackEnabled() must be true. This must be set with
     *  DataSource.setWriteBackEnabled(true) BEFORE data is read.
    */
    public boolean commit() {

        if (!DataSource.isWriteBackEnabled()) return false;

        //!! Not right below - what if this class is used by an auto program?
        //  processingState.setValue("H"); // set 'rflag' value "H" for human checked

        if (debug) {
            System.out.println("DEBUG PhaseTN commit() fromDB:"+fromDbase+" "+toNeatString());
            System.out.println(toString());
        }

        // Reading is deleted or not associated! So don't write new rows.
        // Note we NEVER actually delete a row, just don't write it out
        // nor make an association if it came from the database.
        if (isDeleted() || ! isAssignedToSol()) return true;

        boolean status = false; // commit status

        if (fromDbase) {  // "existing" (came from the dbase)
          if (getNeedsCommit() || hasChanged()) { // force new row (sequence number) and association
              if (debug) System.out.println("(changed) INSERT and assoc");
              status = dbaseInsert() && dbaseAssociate(false);
          } else { // if no changes, just make new association
              if (debug) System.out.println("(unchanged) assoc only");
              status = dbaseAssociate(true);
              if (status) setUpdate(false); // mark as up-to-date
          }
        } else {        // not from dbase <INSERT>
            if (debug) System.out.println("(new) INSERT and assoc");
            status = dbaseInsert() && dbaseAssociate(false);
        }
        setNeedsCommit(! status); // hopefully false?
        return status;
    }

    // since protected can access across package boundaries by Solution and Magnitude
    public boolean isFromDataSource() {
       return fromDbase;
    }

    /**
     * NEVER delete an arrival or its association with another origin.  If
     * you don't want to save it just don't associate it with a new origin.
     */
    protected boolean dbaseDelete() {
        return true;
    }

    /**
     * Make AssocArO row for this Arrival
     */
    protected boolean dbaseAssociate(boolean updateExisting) {
        return dbaseAssociateWithOrigin(updateExisting);
    }

    /**
     * Make AssocArO row for this Arrival
     */
    protected boolean dbaseAssociateWithOrigin(boolean updateExisting) {
        if ( ! isAssignedToSol() ) return false;
        // Check that 'orid' of associated event has been set
        if ( ((SolutionTN)sol).getOridValue() == 0l ) {
          if (debug) System.out.println("DEBUG PhaseTN Error associated Solution has 0 orid "+
                          getChannelObj().getChannelId().toString());
          return false;
        }
        if (getArid() == 0l) { // no arid!
          if (debug) System.out.println("DEBUG PhaseTN Error 0 arid "+
                          getChannelObj().getChannelId().toString());
          return false;
        }

        // force row check unless a boolean input arg for doing row check is implemented 
        return DataTableRowUtil.rowToDb(DataSource.getConnection(), toAssocArORow(), updateExisting);
    }

       
    /**
     * Insert a new Arrival row in the database, it also creates an AssocArO row
     * Note that if this method is invoked outside of a Solution commit(),
     * its currently assigned Solution id value is used for the key id.
     */
    protected boolean dbaseInsert() {
        StnChlTableRow aRow = toTableRow();
        aRow.setProcessing(DataTableRowStates.INSERT);
        // write row
        boolean status = (aRow.insertRow(DataSource.getConnection()) > 0);
        if (status) {
          //Note: Solution commit() invokes reading's commit() which writes new assoc - aww 09/03
          //status = dbaseAssociate(); // (removed see above note) commit() invokes it.
          setUpdate(false);  // make as up-to-date (fromDbase=true) now
        }
        return status;
    }

    public Object getOriginIdentifier() {
        //return orid;
        return (sol == null) ? null : sol.getPreferredOriginId();
    }
    public long getOridValue() {
        //return (orid.isValidNumber()) ? orid.longValue() : 0l;
        if (sol == null) return 0l;
        DataLong orid = (DataLong) sol.getPreferredOriginId();
        return  (orid.isValidNumber()) ? orid.longValue() : 0l;
    }
    //public void setOrid(long id) { orid.setValue(id);}

    /**
    * Return the 'arid' of the row (if it came from the database). If it did not
    * come from the data base 0 is returned.
    */
    public long getArid() {
        return (arid.isValidNumber()) ? arid.longValue() : 0l;
    }
    public void setArid(long id) {
        arid.setValue(id);
    }

    public Object getIdentifier() {
        return arid;
    }
    public void setIdentifier(Object obj) {
        arid.setValue((DataLong) obj);
    }

    /**
    * Returns true if this instance was not initialized from database data.
    * or hasChangedAttributes() == true.
    */
    public boolean hasChanged() {
        return (!fromDbase || hasChangedAttributes());
    }
    /** Returns true if certain data member values were modified after initialization.
    * (description and datetime)
    */
    public boolean hasChangedAttributes() {
//        System.out.println("DEBUG PhaseTN: " + getChannelObj().toDelimitedSeedNameString() + " processingState.isUpdate() : " + processingState.isUpdate() );
        return (
            description.hasChanged() || // should handle "quality" changes
            //chanName.isUpdate() ||
            datetime.isUpdate() ||
            processingState.isUpdate()
            // Note HypoFormat resets ema,azimuth,weightIn, don't know if values changed
            // or only a "reset" of the update flags, note that IN_WGT is saved in AssocArO:
            // || emergenceAngle.isUpdate(); // it should be mirrored in AssocArO table -aww 
            // || azimuth.isUpdate() // this ESaz is the same as SEAZ in AssocArO table -aww
            // || weightIn.isUpdate() // location might depend on changed input weight -aww
        );
    }

    /**
     * Set the isUpdate() flag for all data dbase members the given boolean value.
     * */
    protected void setUpdate(boolean tf) {
        fromDbase = !tf;
        description.remember(description);
        datetime.setUpdate(tf);
        processingState.setUpdate(tf);
    }

    protected String makeTypeClause(String[] typeList) {
        if (typeList == null || typeList.length == 0) return "";
        StringBuffer sb = new StringBuffer();
        sb.append(" and (");
        for (int i = 0; i < typeList.length; i++) {
            if (i > 0) sb.append(" or ");
            sb.append("(a.iphase like '").append(typeList[i]).append("')");
        }
        sb.append(")");
        return sb.toString();
    }

    /*
    private static void associateList(Solution sol,  List phases) {
        if (sol.getPhaseList().isEmpty()) {
            sol.fastAssociateAll(phases); // does not filter input elements
        }
        else {
            sol.associateAll(phases); // filters input for dupes or nulls
        }
    }
    */

    /**
     * Extract from DataSource ALL Phases associated with this Solution.
     */
    // Don't call getBySolution(long) because it has the expensive step of looking up
    // the prefor which is already known to the Solution.
    public Collection getBySolution(Solution aSol) {

        if (aSol == null) return null;

        long inOrid = ((SolutionTN)aSol).getOridValue(); // input arg value
        if (inOrid <= 0l) return null; // return is 0 if not valid (null), so don't query - aww 02/07/2005

        if (DataSource.isNull() || DataSource.isClosed()) {
          System.err.println("PhaseTN.getBySolution(aSol) DataSource null or connection is closed");
          return null;
        }

        ArrayList dbList = null;
        PreparedStatement psBySolution = null;
        try {
          Connection conn = DataSource.getConnection();
          psBySolution = conn.prepareStatement(psBySolutionString);
          // Below was ((SolutionTN)aSol).prefor.longValue(), changed to be consistent with AmpTN,CodaTN.
          // Alternate origins may have different data than the prefor, thus for an input SolutionTN,
          // you need to load the data associated with its Origin id value which may or may not by the
          // same data as associated with the event preferred origin id. -aww
          psBySolution.setLong(1, inOrid);
          dbList = (ArrayList) ((JasiDbReader)getDataReader()).getBySQL(conn, psBySolution);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            JasiDatabasePropertyList.debugSQL(psBySolutionString, psBySolution, debug);
            Utils.closeQuietly(psBySolution);
        }

        if (dbList == null) return null; // no data
        int dbCount  = dbList.size();
        //System.out.println("getBySolution: phases found ="+dbCount);
        if (dbCount < 1) return dbList; // no data

        // option to associate elements of list with the input argument
        // otherwise returned list elements are orphaned from context
        //if (assoc) associateList(aSol, dbList);
        aSol.associate(dbList);

        return dbList;
    }

    // ! Overhead here creating new solution from db query !
    public Collection getBySolution(long id) {
        return getBySolution(Solution.create().getById(id));
    }

/** Get a list of phases associated with this solution from the datasource.
     * @param aSol - the solution
     * @param typeList - list of phase types to get
     * @param assoc - if true, associate the phases with the solution
     * @return - collection of phases = Solution.phaselist. Null if solution is invalid.
     */
    public Collection getBySolution(Solution aSol, String [] typeList, boolean assoc) {
        long inOrid = ((SolutionTN)aSol).getOridValue(); // input arg value
        if (inOrid <= 0l) return null; // return is 0 if not valid (null), so don't query - aww 02/07/2005

        StringBuffer sb = new StringBuffer(1024);
        sb.append(sqlPrefix);
        // Below was ((SolutionTN)aSol).prefor.longValue(), changed to be consistent with AmpTN,CodaTN.
        // Alternate origins may have different data than the prefor, thus for an input SolutionTN,
        // you need to load the data associated with its Origin id value which may or may not by the
        // same data as associated with the event preferred origin id. -aww
        sb.append(" and (o.orid = ?)");
        sb.append(makeTypeClause(typeList));
        sb.append(" order by a.datetime ");
        ArrayList dbList = null; // (ArrayList) ((JasiDbReader)getDataReader()).getBySQL(sb.toString());
        PreparedStatement psBySolution = null;
        final String sql = sb.toString();
        try {
          Connection conn = DataSource.getConnection();
          psBySolution = conn.prepareStatement(sql);
          // Below was ((SolutionTN)aSol).prefor.longValue(), changed to be consistent with AmpTN,CodaTN.
          // Alternate origins may have different data than the prefor, thus for an input SolutionTN,
          // you need to load the data associated with its Origin id value which may or may not by the
          // same data as associated with the event preferred origin id. -aww
          psBySolution.setLong(1, inOrid);
          dbList = (ArrayList) ((JasiDbReader)getDataReader()).getBySQL(conn, psBySolution);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            JasiDatabasePropertyList.debugSQL(sql, psBySolution, debug);
            Utils.closeQuietly(psBySolution);
        }

        if (dbList == null) return null; // no data
        int dbCount  = dbList.size();
        //System.out.println("getBySolution: phases found ="+dbCount);
        if (dbCount < 1) return dbList; // no data

        // option to associate elements of list with the input argument
        // otherwise returned list elements are orphaned from context
        //if (assoc) associateList(aSol, dbList);
        if (assoc) aSol.associate(dbList);

        return dbList;
    }    

    public Collection getBySolution(Solution aSol, String [] typeList) {
        return getBySolution(aSol, typeList, true);
    }

    /**
     * Returns a list of Phases associated in the default DataSource with a
     * Solution whose identifier id matches the input id number that have
     * phase type qualifiers matching those specified in the input typeList.
     * Returns null if there is no Solution exists with such identifier.
     * Returns all types if typeList is null or empty.
     */
    public Collection getBySolution(long id, String [] typeList) {
        // must first  retreive the Solution, then look up prefor for this id
        Solution aSol = Solution.create().getById(id);
        // check for no such solution in DataSource
        return (aSol == null) ? null : getBySolution(aSol, typeList, true);
    }

    /**
     * Extract from DataSource all Phases for this time window. The SolutionList
     * is used to match associated phases with solutions in the list. If a
     * Phase is associated with a Solution not in the list that Solution will be
     * added to the list.<p>
     * If you pass in an empty SolutionList, it will be populated with the Solutions
     * that match the Phases found in the time window.
    public static Collection getByTime(double start, double end, GenericSolutionList sl) {
        ArrayList phList = (ArrayList) Phase.create().getByTime(start, end);
        if (phList == null || phList.isEmpty())  return phList; // added "null" test and return -aww 02/08/2005
        // Match up with associated solutions. Get from dbase is necessary.
        Solution aSol = null;
        PhaseTN ph = null;
        for (int i = 0; i < phList.size(); i++) {
            ph = (PhaseTN) phList.get(i); //pull phase from collection
            // is it in the solution list?
            aSol = sl.getByOriginId(ph.orid); // lookup sol by ORID of phase
            if (aSol != null) {           // a match
                ph.associate(aSol);
            } else {                      // no match, look in dbase for Solution
                aSol = new SolutionTN().getByPreferredOrid(ph.orid.longValue());
                if (aSol != null) {
                    ph.associate(aSol);
                    sl.addByTime(aSol);   // add to the Solution list
                }
            }
        }
        return phList;
    }
    public static Collection getByTime(TimeSpan ts, GenericSolutionList sl) {
        return getByTime(ts.getStart(),  ts.getEnd(), sl);
    }
    */


    /** Input times are true epoch with leap seconds.*/
    public Collection getByTime(double start, double end) {
        if (DataSource.isNull() || DataSource.isClosed()) {
          System.err.println("PhaseTN.getByTime(start,end) DataSource null or connection is closed");
          return null;
        }
        ArrayList dbList = null;
        PreparedStatement psByTime = null;
        try {
          Connection conn = DataSource.getConnection();
          psByTime = conn.prepareStatement(psByTimeString);
          psByTime.setDouble(1, start);
          psByTime.setDouble(2, end);
          dbList = (ArrayList) ((JasiDbReader)getDataReader()).getBySQL(conn, psByTime);
          psByTime.close();
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            JasiDatabasePropertyList.debugSQL(psByTimeString, psByTime, debug);
            Utils.closeQuietly(psByTime);
        }
        return dbList;
    }

    /**
     * Return a Phase for the given Arid in the NCDC schema. Does not get assoc info.
     */
    protected Phase getByArid(long arid) {
        if (DataSource.isNull() || DataSource.isClosed()) {
          System.err.println("PhaseTN.getByArid(arid) DataSource null or connection is closed");
          return null;
        }
        ArrayList dbList = null;
        PreparedStatement psByArid = null;
        try {
          Connection conn = DataSource.getConnection();
          psByArid = conn.prepareStatement(psByAridString);
          psByArid.setLong(1, arid);
          dbList = (ArrayList) ((JasiDbReader)getDataReader()).getBySQL(conn, psByArid);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
          JasiDatabasePropertyList.debugSQL(psByAridString, psByArid, debug);
          Utils.closeQuietly(psByArid);
        }
        return (dbList == null || dbList.isEmpty()) ? null : (Phase) dbList.get(0);
    }

    public JasiCommitableIF getById(Object id) {
        if (id instanceof Number)
            return  getById(((Number)id).longValue());
        else if (id instanceof DataNumber)
            return getById(((DataNumber)id).longValue());
        else return null;
    }

    public JasiCommitableIF getById(long id) {
        return getByArid(id);
    }

    public JasiObject parseData(Object rsdb) {
        return parseResultSetDb((ResultSetDb)rsdb);
    }
    public JasiObject parseResultSetDb(ResultSetDb rsdb) {
        return parseResultSet(rsdb);
    }
    /**Access to the JasiDbReader to allow generic SQL queries. */
    public JasiDataReaderIF getDataReader() {
        return jasiDataReader;
    }

    // A matching row key exists in datasource but its actually data may differ from instance
    public boolean existsInDataSource() {
        JasiDbReader jdr = (JasiDbReader) getDataReader();
        //int count = jdr.getCountBySQL(DataSource.getConnection(), "SELECT 1 FROM arrival WHERE arid = " + arid.longValue());
        int count = jdr.getCountBySQL(DataSource.getConnection(), "ARRIVAL", "ARID", arid.toString());
        return (count > 0);
    }

    /**
     * Parse resultset row data into Phase
     */
    protected static Phase parseResultSet(ResultSetDb rsdb) {
        // SELECT a.ARID,a.DATETIME,a.STA,a.NET,a.AUTH,a.SUBSOURCE,a.CHANNEL,a.CHANNELSRC,a.SEEDCHAN
        // ,a.LOCATION,a.IPHASE,a.QUAL,a.FM,a.DELTIM,a.QUALITY,a.RFLAG,a.LLDATE" 
        // ,o.IMPORTANCE,o.DELTA,o.SEAZ,o.IN_WGT,o.WGT,o.TIMERES,o.EMA,o.SDELAY 
        // FROM Arrival, AssocArO o WHERE (a.arid=o.arid)

        PhaseTN ph = new PhaseTN();
        int offset = 1;
        try {
            //arid
            ph.arid = rsdb.getDataLong(offset++); // Arrival.ARID

            //datetime
            ph.datetime = rsdb.getDataDouble(offset++); // Arrival.DATETIME

            //sta
            ph.chan.setSta(rsdb.getResultSet().getString(offset++));

            //net
            ph.chan.setNet(rsdb.getResultSet().getString(offset++));

            //auth
            ph.authority = rsdb.getDataString(offset++); // Arrival.AUTH

            //subsource
            ph.source = rsdb.getDataString(offset++); //Arrival.SUBSOURCE

            //channel
            ph.chan.setChannel(rsdb.getResultSet().getString(offset++));

            //channelsrc
            ph.chan.setChannelsrc(rsdb.getResultSet().getString(offset++));

            //seedchan
            ph.chan.setSeedchan(rsdb.getResultSet().getString(offset++));

            //location
            ph.chan.setLocation(rsdb.getResultSet().getString(offset++));

            //iphase
            String phaseType = PhaseDescription.DEFAULT_TYPE;
            DataString dataStr = rsdb.getDataString(offset++); //Arrival.IPHASE
            if (dataStr.isValid()) phaseType = dataStr.toString();

            //qual
            String ie = PhaseDescription.DEFAULT_TYPE;
            dataStr = (DataString) rsdb.getDataString(offset++); // Arrival.QUAL
            if (dataStr.isValid()) ie = dataStr.toString();

            //fm
            String fm = PhaseDescription.DEFAULT_FM;  // length is 2-chars 
            dataStr = (DataString) rsdb.getDataString(offset++); // Arrival.FM
            if (dataStr.isValid()) fm = dataStr.toString();

            //deltim
            ph.deltaTime = rsdb.getDataDouble(offset++); // Arrival.DELTIM

            //quality
            double decimalWt = 0.; // bug fix: used to be hypoinv integral wt 4, 03/04 aww
            DataDouble dd = rsdb.getDataDouble(offset++); // Arrival.QUALITY
            if (dd.isValid()) decimalWt = dd.doubleValue();

            //rflag
            ph.processingState = rsdb.getDataString(offset++); // Arrival.RFLAG

            //lddate removed
            //ph.timeStamp = rsdb.getDataDate(offset++); // Arrival.LDDATE

            //orid removed
            //ph.orid = rsdb.getDataLong(offset++); // AssocArO.ORID

            //importance
            ph.importance = rsdb.getDataDouble(offset++); // AssocArO.IMPORTANCE

            //delta
            dd = rsdb.getDataDouble(offset++); // AssocArO.DELTA
            if (dd.isValid()) ph.setHorizontalDistance(dd.doubleValue());

            //seaz
            dd = rsdb.getDataDouble(offset++); // AssocArO.SEAZ
            if (dd.isValid()) ph.setAzimuth(dd.doubleValue());

            //in_wgt
            ph.weightIn = rsdb.getDataDouble(offset++); // AssocArO.IN_WGT
            if (ph.weightIn.isValid()) ph.reject = (ph.weightIn.doubleValue() == 0.);

            //wgt
            ph.weightOut = rsdb.getDataDouble(offset++); // AssocArO.WGT

            //timeres
            ph.residual = rsdb.getDataDouble(offset++); // AssocArO.TIMERES

            //ema
            ph.emergenceAngle = rsdb.getDataDouble(offset++); // AssocArO.EMA

            //sdelay
            ph.delay = rsdb.getDataDouble(offset++); // AssocArO.SDELAY
            //if (! ph.delay.isValidNumber()) ph.delay = rsdb.getDataDouble(?); // AssocArO.SCORR could be added?

            ph.description = new PhaseDescription(phaseType, ie, fm, decimalWt);
            if (!ph.deltaTime.isValid()) ph.qualityToDeltaTime(); // reset deltaTime to quality mapping -aww 2011/04/13

            ph.setUpdate(false); // set fromDbase flag true
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        return (Phase) ph;
    }

    /*
    protected static Phase parseResultSet(ResultSetDb rsdb) {
        Arrival arr    = new Arrival();        // jdbc object
        AssocArO assoc = new AssocArO();
        int offset = 0;
        // get jdbc objects from the result set
        arr = (Arrival) arr.parseOneRow(rsdb, offset);
        offset += Arrival.MAX_FIELDS;
        assoc = (AssocArO) assoc.parseOneRow(rsdb, offset);
        // Allow changes to this DataTableRow. Need to set this because we are
        // not using our own rather than org.trinet.jdbc parsing methods
        if (DataSource.isWriteBackEnabled()) {
            arr.setMutable(true).setMutableAllValues(true);
            assoc.setMutable(true).setMutableAllValues(true);
        }
        // build up the Phase from the jdbc objects
        PhaseTN ph = new PhaseTN();
        ph.parseArrival(arr);
        ph.parseAssocArO(assoc);
        // remember that we got this from the dbase
        ph.setUpdate(false);
        return (Phase) ph;
    }

    protected void parseArrival(Arrival arr) {
        //
        // defaults
        //
        String phaseType   = PhaseDescription.DEFAULT_TYPE;
        String ie          = PhaseDescription.DEFAULT_TYPE;
        String fm          = PhaseDescription.DEFAULT_FM;  // aww length is 2-chars 

        double decimalWt   = 0.; // bug fix: used to be hypoinv integral wt 4, 03/04 aww
        //
        // use setChannel to synch with master channel list
        //
        Channel cha = ChannelTN.parseChannelFromDataTableRow(arr, DataTableRowUtil.AUTHSUBSRC);
        // call setChannel to synch with master channel list
        //setChannelObj(cha); // aww 04/05/2005
        this.chan = cha; // aww 04/05/2005
        arid          = (DataLong) arr.getDataObject(Arrival.ARID);
        datetime      = (DataDouble) arr.getDataObject(Arrival.DATETIME);
        //Kludge for NCEDC leap corrected data, to convert to nominal time -aww
        //DataSource.dbTimeBaseToNominal(datetime); // removed 01/24/2008 for leap secs -aww

        processingState = (DataString) arr.getDataObject(Arrival.RFLAG);
        authority    = (DataString) arr.getDataObject(Arrival.AUTH);
        source       = (DataString) arr.getDataObject(Arrival.SUBSOURCE);
        // Create phaseDescription based on stuff in dbase
        DataString dataStr  = (DataString) arr.getDataObject(Arrival.IPHASE);
        if (dataStr.isValid()) phaseType  = dataStr.toString();
        dataStr  = (DataString) arr.getDataObject(Arrival.QUAL);
        if (dataStr.isValid()) ie  = dataStr.toString();
        dataStr  = (DataString) arr.getDataObject(Arrival.FM);
        if (dataStr.isValid()) fm  = dataStr.toString();

        DataDouble dd = (DataDouble) arr.getDataObject(Arrival.QUALITY);
        if (dd.isValid()) decimalWt  = dd.doubleValue();
        description = new PhaseDescription(phaseType, ie, fm, decimalWt);

        deltaTime = (DataDouble) arr.getDataObject(Arrival.DELTIM); // added 2010/01/24 -aww

        // save Arrival.LDDATE in timeStamp? -aww
        this.timeStamp = (DataDate) arr.getDataObject(Arrival.LDDATE); // date of first commit?

        // Measured Values below are not populated by SCEDC as of 6/2004.
        // ema and azimuth should only be obtained from AssocArO row - aww 04/14/2006
        //emergenceAngle = (DataDouble) arr.getDataObject(Arrival.EMA); // only the prefor or null value?
        // Azimuth below is probably coupled to the ema above:
        //DataDouble az = (DataDouble) arr.getDataObject(Arrival.AZIMUTH); // only the prefor or null value?
        //if (az.isValidNumber()) setAzimuth(az.doubleValue());

        // Save the arrivalRow for future use (commit)
        //arrivalRow = arr;
        return;
    }
    */

    /**
     * Stuff contents from this Phase object into a new Arrival (TableRow) object. Gets a
     * new 'arid' from "arseq" in the dbase.
     * @See: org.trinet.jdbc.Arrival().
     */
    protected StnChlTableRow toTableRow() {
        // Always make a new row, get unique key #
        // long newId = SeqIds.getNextSeq(DataSource.getConnection(), "ARSEQ");
        long newId = SeqIds.getNextSeq(DataSource.getConnection(), Arrival.SEQUENCE_NAME);
        arid.setValue(newId);

        Arrival arrivalRow = new Arrival(arid.longValue());        // set ID for arrival row
        //if (debug) System.out.println("DEBUG PhaseTN toTableRow new id: " + arrivalRow.toString());

        // Stuff contents of channel name.
        setRowChannelId(arrivalRow, getChannelObj());
        // set flag to enable processing
        arrivalRow.setUpdate(true);

        //Kludge for NCEDC leap corrected data, to convert to leap time -aww
        //DataDouble dt = (DataDouble) datetime.clone();
        //DataSource.nominalToDbTimeBase(dt);
        //arrivalRow.setValue(Arrival.DATETIME, dt);
        // end of leap patch aww // removed 2008/01/24 for leap secs -aww
        arrivalRow.setValue(Arrival.DATETIME, datetime);

        // the following should not be null -aww
        arrivalRow.setValue(Arrival.AUTH, getClosestAuthorityString());

        if (processingState.isValid())
          arrivalRow.setValue(Arrival.RFLAG, processingState);
        if (source.isValid())
          arrivalRow.setValue(Arrival.SUBSOURCE, source);
        // phase description.
        //if (description.hasChanged()) {
            arrivalRow.setValue(Arrival.IPHASE, description.iphase);
            arrivalRow.setValue(Arrival.QUAL, description.getQual());
            arrivalRow.setValue(Arrival.QUALITY, description.getQuality());
            arrivalRow.setValue(Arrival.FM, description.fm);
            if (deltaTime.isValid()) arrivalRow.setValue(Arrival.DELTIM, deltaTime); // added 2010/01/24 -aww
        //}
        // Measured fields below not currently used by SCEDC as of 6/2004
        // values are those "predicted" by Hypoinverse and belong in AssocArO - aww 04/18/2006
        /*
        // azimuth (ESaz) and ema values in arrival, what values go here?
        // they should be null, unless some algo other than Hypoinverse "measures" them - aww
        x = getAzimuth();
        if ( ! (x == Channel.NULL_AZIMUTH || Double.isNaN(x)) )
            arrivalRow.setValue(Arrival.AZIMUTH, x);
        if (emergenceAngle.isValidNumber()) arrivalRow.setValue(Arrival.EMA, emergenceAngle);
        */

        if (debug) {
            System.out.println("DEBUG PhaseTN toTableRow(): "+arrivalRow.toString());
        }

        return arrivalRow;
    }

  /** Put the channel name attributes in the Arrival DataTableRow */
    protected void setRowChannelId(StnChlTableRow aRow, Channel chan) {
        //DataTableRowUtil.setRowAuthChannelId(aRow, chan); // generic, but below is quicker
        ChannelName cn = chan.getChannelName();
        // by default all ChannelName fields are "" rather then null strings: see ChannelName
        aRow.setValue(Arrival.STA, cn.getSta()); // this attribute can't be null
        String str = cn.getNet();
        if (str.length() > 0) aRow.setValue(Arrival.NET, str);
        str = cn.getAuth();
        if (str.length() > 0) aRow.setValue(Arrival.AUTH, str);
        str = cn.getSubsource();
        if (str.length() > 0) aRow.setValue(Arrival.SUBSOURCE, str);
        str = cn.getChannel();
        if (str.length() > 0) aRow.setValue(Arrival.CHANNEL, str);
        str = cn.getChannelsrc();
        if (str.length() > 0) aRow.setValue(Arrival.CHANNELSRC, str);
        str = cn.getSeedchan();
        if (str.length() > 0) aRow.setValue(Arrival.SEEDCHAN, str);
        str = cn.getLocation();
        if (str.length() > 0) aRow.setValue(Arrival.LOCATION, str);
    }


    /*
    protected void parseAssocArO(AssocArO assoc) {
        orid          = (DataLong)   assoc.getDataObject(AssocArO.ORID);
        DataDouble dd = (DataDouble) assoc.getDataObject(AssocArO.DELTA);
        //if (dd.isValid()) setDistance(dd.doubleValue()); // not slant it should be horizontal aww 06/11/2004
        if (dd.isValid()) setHorizontalDistance(dd.doubleValue()); // aww 06/11/2004
        dd            = (DataDouble) assoc.getDataObject(AssocArO.SEAZ);
        if (dd.isValid()) setAzimuth(dd.doubleValue());
        emergenceAngle = (DataDouble) assoc.getDataObject(AssocArO.EMA); // now in AssocArO schema table 04/14/2006 -aww
        weightIn      = (DataDouble) assoc.getDataObject(AssocArO.IN_WGT);
        weightOut     = (DataDouble) assoc.getDataObject(AssocArO.WGT);
        residual      = (DataDouble) assoc.getDataObject(AssocArO.TIMERES);
        delay         = (DataDouble) assoc.getDataObject(AssocArO.SDELAY); // added tt model delay - aww
        if (! delay.isValidNumber()) delay = (DataDouble) assoc.getDataObject(AssocArO.SCORR); //perhaps it's stored here ? aww
        importance    = (DataDouble) assoc.getDataObject(AssocArO.IMPORTANCE); // added importance - aww 2008/07/14

        // DataBoolean db = (DataBoolean) assoc.getDataObject(AssocArO.IN_WGT_USE);
        // if (db.isValid()) reject = ! db.booleanValue(); // aww 06/22/2006 not implemented yet

        if (weightIn.isValid()) reject = (weightIn.doubleValue() == 0.); // 01/23/2007 -aww

        //Save the assocRow for future writeBack
        //assocRow = assoc;
    }
    */

    /**
     * Stuff contents of this Phase/association into an AssocArO (TableRow) object.
     */
    private AssocArO toAssocArORow() {

        AssocArO assocRow = new AssocArO(); // keys are orid and arid
        assocRow.setUpdate(true); // set flag to enable processing

        // For assoc row using auth and subsource from reading is confusing
        // if the assoc row is supposed to refer to its "creator" then should 
        // use auth and subsource of the associated origin - aww 01/11/2005
        SolutionTN solTN = (SolutionTN) sol;
        long assocOrid = solTN.getOridValue();
        //setOrid(assocOrid);

        // Constrained "NOT NULL" in the schema, hypothetically "" or NaN could be a legit value string 
        assocRow.setValue(AssocArO.ORID, assocOrid);
        assocRow.setValue(AssocArO.ARID, arid);
        //String str = solTN.getAuthority(); // removed -aww 2011/08/17
        String str = getClosestAuthorityString(); // added -aww 2011/08/17
        // force null check here
        if (!NullValueDb.isNull(str)) assocRow.setValue(AssocArO.AUTH, str);

        // ALLOWED TO BE NULL
        //if (source.isValid()) assocRow.setValue(AssocArO.SUBSOURCE, source); // aww 01/11/2005 removed
        str = solTN.getSource();
        if (!NullValueDb.isNull(str)) assocRow.setValue(AssocArO.SUBSOURCE, str);

        //double x = getDistance(); // don't save slant distance - aww 06/11/2004
        double x = getHorizontalDistance(); // aww 06/11/2004
        if ( ! (x == Channel.NULL_DIST || Double.isNaN(x)) )
            assocRow.setValue(AssocArO.DELTA, x);
        x = getAzimuth();
        if ( ! (x == Channel.NULL_AZIMUTH || Double.isNaN(x)) )
            assocRow.setValue(AssocArO.SEAZ, x);
        if (emergenceAngle.isValidNumber())  assocRow.setValue(AssocArO.EMA, emergenceAngle); // schema change - aww 04/18/2006 
        if (weightIn.isValid())   assocRow.setValue(AssocArO.IN_WGT,  weightIn);
        if (weightOut.isValid())  assocRow.setValue(AssocArO.WGT,     weightOut);
        if (residual.isValid())   assocRow.setValue(AssocArO.TIMERES, residual);
        if (delay.isValid())      assocRow.setValue(AssocArO.SDELAY, delay); // added tt model delay - aww
        if (importance.isValid()) assocRow.setValue(AssocArO.IMPORTANCE, importance); // added importance - aww 2008/07/14
        // phase description. Note: this is redundant with Arrival.IPHASE
        assocRow.setValue(AssocArO.IPHASE,  description.iphase);
        //assocRow.setValue(AssocArO.IN_WGT_USE,  reject); // aww 06/22/2006 not implemented yet

        if (processingState.isValid()) assocRow.setValue(AssocArO.RFLAG, processingState);

        if (debug) {
            System.out.println("DEBUG PhaseTN toAssocArORow(): "+assocRow.toString());
        }

        return assocRow;
    }

    public boolean copySolutionDependentData( Phase newPhase) {
        if (isLikeChanType(newPhase)) { // no association check
            /*
            //AWW
            // NOTE: setValue(obj) default behavior was changed
            //to propagate the isNull() state of input DataObject.
            //but it still sets dataObj.valueUpdate true, though value 
            //may not have changed, value was null, and still is null.
            //This behavior may change in future implementation,
            //the question is when setting new value == old value
            //is valueUpdate=true required to get the correct behavior
            //for the DataTableRow object processing for database SQL ?
            //Note that valueUpdate flag is used to signal "hasChanged" 
            //for some subclasses of JasiObjects.
            //For now "updateValue" means only that setValue was invoked,
            //it does not inform whether the value changed from its old value.
            //DataObjects could have code added to implement Change or
            //PropertyChange events, but this would increase the 
            //the object footprint a bit and require listener management.
            //AWW
            residual.setValue(newPhase.residual);
            weightIn.setValue(newPhase.weightIn);
            weightOut.setValue(newPhase.weightOut);
            emergenceAngle.setValue(newPhase.emergenceAngle);
            */

            residual  = newPhase.residual;
            delay     = newPhase.delay; //  added tt model delay - aww
            importance = newPhase.importance; //  added importance - aww 2008/07/14
            weightIn  = newPhase.weightIn; // not setValue(), see hasChangedAttributes()
            weightOut = newPhase.weightOut;
            emergenceAngle = newPhase.emergenceAngle;

            setDistance(newPhase.getDistance());
            setHorizontalDistance(newPhase.getHorizontalDistance()); // aww
            setAzimuth(newPhase.getAzimuth());

            return true;
        } else {
            return false;
        }
    }

    public Object clone() {
      PhaseTN phTN        = (PhaseTN) super.clone();
      //phTN.orid           = (DataLong) orid.clone();
      phTN.arid           = (DataLong) arid.clone();
      return phTN;
    }

} // PhaseTN
