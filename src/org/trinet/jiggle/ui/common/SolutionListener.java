package org.trinet.jiggle.ui.common;

import org.trinet.jiggle.common.type.SolutionEvent;
import java.util.EventListener;

public interface SolutionListener extends EventListener {
    void solutionChanged(SolutionEvent evt);
}
