//
// SHOULD MOVE ChannelDataMap hash map lookup similar code in ChannelList subclass up to this class - aww
//
package org.trinet.jasi;

import java.util.*;
import java.io.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;

/** An ActiveArrayList of Channelable objects. Allows calculation of distance from
* a Solution or LatLonZ and sorting of the list by distance/component. */
public class ChannelableList extends ActiveArrayList implements ChannelableListIF {

    /**
     * Master list to check against.
     * If not null, an input channelable not contained in master list
     * will not be added to this list instance by its add() method.
     * @see #add(Object)
     * */
    protected ChannelableList masterList = null;

    public ChannelableList() { }

    public ChannelableList(int initCapacity) {
      super(initCapacity);
    }

    public ChannelableList(Collection<?> col) {
      super(col == null ? 0: col.size());
      if ( col == null || col.isEmpty() ) {
          return;
      }
      // confirm input Collection has Channelable objects
      Channelable ch;
      final Iterator<?> it = col.iterator();
      while (it.hasNext()) {
          ch = (Channelable) it.next(); // throw exception if not Channelable
          add(ch, false);
      }
    }

    /**
     * Set a master list.
     * Only channels that appear in this master list can be added to this list.
     * */
    public void setMasterList(ChannelableList list) {
      // What if it's not the same concrete subtype as this list? -aww
      masterList = list;
    }
    public  ChannelableList getMasterList() {
      return masterList;
    }
    /** Returns a ChannelList containing channels that are in the same group
     * as the input channel. 
     * "In the same group" means matching net, sta, location and the first two
     * characters of seedchan and having an active date range that contains the input date.
     * If the input date is null, it uses the current date.
     * Returns empty ChannelList if no match is found.
     * */
    // DDG 4/13/07
    public ChannelableList findChannelGroup(Channelable ch, java.util.Date date) {

    	if (ch == null) return null;
        
    	Channelable chan = ch.getChannelObj(); 
        if (chan == null) return null;
        
        ChannelList list = new ChannelList();
        
        if (date == null) date = new java.util.Date();
        
        Channelable chnl = null;
        
        // shlep thru the list
        for (int i = 0; i < size(); i++) {
        
          chnl = (Channelable) get(i);
          if (chan.sameTriaxialAs(chnl)) { 
        	  list.add(chnl);
        	  // ineffecient but can't count on the list's sort order 
          }
        }

        return list;
    }    
    /**
     * Adds an object to the list.
     * Will not add object if its already in the list or it is not a Channelable object.
     * The test is based on the equals() method of the Channelable object's class.
     * If a MasterList is defined (not null) the input instance must also have an
     * equivalent in the MasterList to be added to this list instance.
     * Returns 'true' only if the object was added.
     */
    public boolean add(Object obj) {
      // What about overrides of the other super class public methods:
      // like add(Object,boolean), set(int,Object), add(int, Object) ?
      // JDK1.5 generic list template checking in lieu of instanceof?
      // if (! (obj instanceof Channelable)) return false; // why is this check here? -aww
      // Should master class be forced to same concrete subtype as this list to contain instance? -aww
      // if no master list, or the master list has an element with matching channelId add to this list
      return ( matchesNameInMasterList((Channelable)obj) ) ? super.add(obj) : false;
    }
    public boolean add(Object obj, boolean notify) {
      // What about overrides of the other super class public methods:
      // like add(Object,boolean), set(int,Object), add(int, Object) ?
      // JDK1.5 generic list template checking in lieu of instanceof?
      // if (! (obj instanceof Channelable)) return false; // why is this check here? -aww
      // Should master class be forced to same concrete subtype as this list to contain instance? -aww
      // if no master list, or the master list has an element with matching channelId add to this list
      return ( matchesNameInMasterList((Channelable)obj) ) ? super.add(obj, notify) : false;
    }

    /**
    * Insert an object at this index. Will not add object if its null or already in the list.
    * Overrides ArrayList.add() to inforce no nulls or duplicates rule.
    * Notifies listeners if object was added.
    */
    public void add(int index, Object obj)  {
      if ( matchesNameInMasterList((Channelable)obj) ) super.add(index, obj);
    }

    /** Returns 'true' if this channel is allowed by being in the MasterList or
     * the master list is empty. */
    public boolean matchesNameInMasterList(Channelable ch) {
//      if (masterList == null || masterList.isEmpty()) return false;
      if (masterList == null || masterList.isEmpty()) return true;
      Channelable masterCh = null;
      int count = masterList.size();
      for (int idx = 0; idx < count; idx++) {
        masterCh = (Channelable) masterList.get(idx);
        //Removed below since for Triaxial set clone the 'location' may not be that of actual input
        //Re-added 03/01/2006 -aww
        if (masterCh.equalsChannelId(ch)) return true;
        //if (masterCh.sameSeedChanAs(ch)) return true; // break out removed 03/01/2006 -aww
      }
      return false;
    }

    /** Adds input to list without the MasterList membership check.
     * @see #add(Object)
     * */
    public boolean addWithoutCheck(Object obj) {
      return super.add((Channelable) obj); // cast to check type -aww 03/04/2005
    }

    /**
     * Return this list (collection) as an array of Channelable objects.
     * Returns a 0 length array if this ChannelableList is empty.
     */
    public Channelable[] getChannelableArray() {
      return (Channelable[]) toArray(new Channelable[size()]);
    }

    /** Return the first Channelable in the list that matches the
     * ChannelId of the input Channelable. Returns null if no match. */
    public Channelable getByChannel(Channelable chan) {
      return getByChannel(this, chan);
    }

    public static Channelable getByChannel(ChannelableListIF chList, Channelable chan) {
      if (chan == null) return null; // short-circuit
      Channelable ch = null;
      int count = chList.size();
      for (int i = 0; i<count; i++) {
        ch = (Channelable) chList.get(i);
        if ( ch.equalsChannelId(chan) ) return ch;
      }
      return null;
    }

    /**
     * Calculate the horizontal distance and azimuth from location in the argument
     * for each object in the list.
     * If the LatLonZ of the object is null, distance is set to Channel.NULL_DIST.*/
    public void calcDistances(GeoidalLatLonZ loc) { // aww 06/11/2004
      int count = size();
      for ( int i=0; i < count; i++) {
         ((Channelable)get(i)).calcDistance(loc); // does azimuth, too
      }
    }

    /** Given a list of Channelable's (which have azimuth values) find the
    * maximum azmuthal gap. Returns the gap in degrees. */
    public double getMaximumAzimuthalGap() {
      return getMaximumAzimuthalGap(this);
    }
    public double getMaximumAzimuthalGap(ChannelableListIF chList) {
      double maxgap = 360.0;
      int count = chList.size();
      if (count < 2) return maxgap;  // only one point
      double az[]   = new double[count];
      for (int i = 0; i < count; i++) {
        az[i] = ((Channelable)chList.get(i)).getAzimuth();
      }
      // sort in ascending order, first is smallest
      Arrays.sort(az, 0, count);
      double diff = 0.;
      // start with gap from last to first azimuth
      // this angle would span the 0 position in the circle
      maxgap = (az[0] + 360.0) - az[count-1];
      if (Double.isNaN(maxgap)) maxgap = 360.0;
      // go clockwise around the circle
      for ( int i=0; i < count-2; i++) {
        diff = az[i+1] - az[i];
        if (diff > maxgap && ! Double.isNaN(diff)) maxgap = diff;
      }
      return maxgap;
    }

    /**
     * Calculate distances and sort the list by distance, and for components
     * at the same distance, by net, sta, and component name.
     * @see #sortByStation()
     * @see #distanceOnlySort(GeoidalLatLonZ)
     */
    public void distanceSort(GeoidalLatLonZ latlonz) {
       calcDistances(latlonz);
       // aww ActiveArrayList avoids sort error by removing Collection.sort below:
       this.sort(new ChannelDistanceSorter());  // ActiveArrayList has special sort
    }

    public void distanceSort() {
       this.sort(new ChannelDistanceSorter());  // ActiveArrayList has special sort
    }

    /**
     * Sort the ChannelableList by distance from the input location.  <p>
     * Does not order components at same distance by name or type.<br>
    */
    public void distanceOnlySort(GeoidalLatLonZ latlonz) {
      calcDistances(latlonz);
      // aww ActiveArrayList avoids sort error by removing Collection.sort below:
      this.sort(new DistanceSorter()); // Comparator in gazetteer package
    }

    /**
     * Sort the list by name alphabetically by network, station, and component.
     * The component sort order follows the ComponentSorter() convention. <p>
     * NO distance sort.
     * @see #distanceSort(GeoidalLatLonZ)
     */
    public void sortByStation() {
      // aww ActiveArrayList avoids sort error by removing Collection.sort below:
      this.sort(new ChannelNameSorter()); // ActiveArrayList has special sort don't use Collections.sort()
    }

    public void sortByStation2() {
      // aww ActiveArrayList avoids sort error by removing Collection.sort below:
      this.sort(new ChannelNameSorter2()); // ActiveArrayList has special sort don't use Collections.sort()
    }

   /** Make sure all element members of this list have LatLonZ info
    * valid for input date (channel active for date).
    * If missing tries to look up a info in the DataSource.
    * Returns 'true' if ALL the Channels have a LatLonZ.
    * Input null date to use current system time.
    * */
    public boolean insureLatLonZinfo(java.util.Date date) {
      return insureLatLonZinfo(this, date);
    }
    /** Makes sure all in list has Lat/Lon/Z info (if available).<p>
    * Returns 'false' if any channels were missing in DataSource.*/
    public static boolean insureLatLonZinfo(ChannelableListIF chList, java.util.Date date) {

      //if (latLonZinfoComplete(chList)) return true;  // bad idea - prevents copy of response info
    	
        // Maybe the model doesn't need response info or the passed chList already
    	// has it!
    	// This, too, is a bad idea - it forces a very slooow reread of every channel
    	// from the dbase!
      return chList.matchChannelsWithDataSource(date);

//      if (status)  { // some may still not have LatLonZ, so need to check
//        return !missingLatLonZinfo(chList);
//      }
//      return status;
    }
    /** Return false if any in the list is missing Lat/Lon/Z info, 'true' if complete. */
    public static boolean latLonZinfoComplete(ChannelableListIF chList) {
      for ( int i=0; i < chList.size(); i++) {
        if ( ((Channelable) chList.get(i)).getChannelObj().getLatLonZ().isNull() ) {
          return false; // missing data, bail on the 1st instance
        }
      }
      return true;  // made it thru the list
    }
   /** Make sure all element members of this list have LatLonZ info
    * valid for current system time at invocation.
    * If missing tries to look up a info in the DataSource.
    * Returns 'true' if ALL the Channels have a LatLonZ.
    * */
    /*
    public boolean insureLatLonZinfo() {
       return insureLatLonZinfo(this, (java.util.Date) null) ;
    }

    public boolean insureLatLonZinfo() {
      boolean status = true;
      Channel chan = null;
      int count = size();
      for ( int i=0; i < count; i++) {
        chan = ((Channelable) get(i)).getChannelObj();
        if (chan.getLatLonZ().isNull())  {
          chan.setChannelObjData(chan.lookUp(chan));
          status &= chan.hasLatLonZ();
        }
      }
      return status;
    }
    */

    /**
     * Trim (remove) elements with distance greater then 'dist'.
     * Returns true if any element was removed.
     */
    public boolean trim(double dist) {
      boolean trimmed = false;
      // Most efficient to do in reverse, eliminates repacking array
      int count = size();
      for ( int i = count-1; i >= 0; i--) {
        //if ( ((Channelable)get(i)).getDistance() > dist ) { // dont' use slant, use epicentral aww 06/11/2004
        if ( ((Channelable)get(i)).getHorizontalDistance() > dist ) { // aww 06/11/2004
          remove(i);                      // each remove fires an event
          trimmed = true;
        }
      }
      return trimmed;
    }

    /**
     * Trim this List to 'count' elements by removing
     * the list elements whose indexes >= count.
     * Returns true if any element was removed.
     */
    public boolean trim(int count) {
      if (count < 0) return false;
      int size = size();
      if (count >= size ) return false; // nothing to trim
      // fromIndex, inclusive and toIndex, exclusive:
      //   0,1,2 indices for size=3:
      //   trim(0) removes all elements
      //   trim(1) removes 2nd&3rd
      //   trim(2) removes 3rd element
      removeRange(Math.min(size, count), size);
      return true;
    }

    /** Return the first Channelable object in the list with a full name
     * <b>begining with</b> the given string. Search is NOT case sensitive.
     * Assumes string is a full Net-Sta-Chan-Loc description delimited by
     * space, \t,"."/", or ",". For example: the input string "CI.AB" would match
     * "CI.ABC.HHL.0". So, would "CI ABC", "ci.ab" or "CI/ABC/H".
     * Returns null if no match. */

    public Channelable getChannelableStartingWith(String str) {
      return getChannelableStartingWith(this, str);
    }
    public static Channelable getChannelableStartingWith(ChannelableListIF chList, String str) {
      int count = chList.size();
      if (count == 0) return null;

      // parse the channel string then build a dot delimited comparison string
      StringTokenizer stok = new StringTokenizer(str," .,\t/");
      StringBuffer searchNameBuffer = new StringBuffer(24);

      while (stok.hasMoreElements()) {
        searchNameBuffer.append(stok.nextToken());
        if (stok.hasMoreElements()) searchNameBuffer.append(".");
      }

      String searchName = searchNameBuffer.toString().toUpperCase();

      Channelable ch = null;
      for (int i = 0; i<count; i++) {
        ch = (Channelable) chList.get(i);
        if (ch.getChannelObj().toDelimitedSeedNameString(".").toUpperCase().startsWith(searchName) )
          return ch;   // match!
      }
      return null;
    }

    /** Return the index in the List of the object that matches the
     * given ChannelId.
     * Returns -1 if no match.
     * */
    public int getIndexOf(Channelable chan) {
        return getIndexOf(this, chan);
    }
    public static int getIndexOf(ChannelableListIF chList, Channelable chan) {
      int count = chList.size();
      for (int i = 0; i<count; i++) {
        if ( ((Channelable)chList.get(i)).equalsChannelId(chan) ) return i;
      }
      return -1;
    }
    public boolean hasChannelId(Channelable chan) {
        return (getIndexOf(this, chan) >= 0);
    }

    public int getChannelCount() {
      return size();
    }

    /** Given a list of Channelable objects, return a count of how many unique
    * stations are represented in the list. A "station" is a unique "net", "sta".
    * A station may have multiple channels. */

    /** Return a count of how many unique stations are present in the list.
     * A "station" is a unique "net" and "sta".
    * A station may have multiple channels.
    * This list does not have to be pre-sorted and will not be sorted by this call. <p>
    * @see Channelable.sameStationAs()
    * */

    public int getNetworkCount() {
      return getNetworkCount(this);
    }
    public int getStationCount() {
      return getStationCount(this);
    }
    public int getSeedChanCount() {
      return getSeedChanCount(this);
    }
    public int getTriaxialCount() {
      return getTriaxialCount(this);
    }

    public static int getNetworkCount(ChannelableListIF chList) {
      int size = chList.size();
      if (size == 0) return 0;
      // avoid double cast for now just make an array
      Channelable ch[] = chList.getChannelableArray();
      // for each entry, scan forward in list & only count if no repeats ahead
      int count = size;
      for (int i = 0; i<size-1; i++) {
        for (int k = i+1; k<size; k++) {
          if ( ch[i].sameNetworkAs(ch[k]) ) {
            count--;
            break;
          }
        }
      }
      return count;
    }
    public static int getSeedChanCount(ChannelableListIF chList) {
      int size = chList.size();
      if (size == 0) return 0;
      // avoid double cast for now just make an array
      Channelable ch[] = chList.getChannelableArray();
      // for each entry, scan forward in list & only count if no repeats ahead
      int count = size;
      for (int i = 0; i<size-1; i++) {
        for (int k = i+1; k<size; k++) {
          //if ( ch[i].sameSeedChanAs(ch[k]) ) { // removed 03/01/2006 -aww
          if ( ch[i].equalsChannelId(ch[k]) ) { // added to use loc code 03/01/2006 -aww
            count--;
            break;
          }
        }
      }
      return count;
    }
    public static int getStationCount(ChannelableListIF chList) {
      int size = chList.size();
      if (size == 0) return 0;
      // avoid double cast for now just make an array
      Channelable ch[] = chList.getChannelableArray();
      // for each entry, scan forward in list & only count if no repeats ahead
      int count = size;
      for (int i = 0; i<size-1; i++) {
        for (int k = i+1; k<size; k++) {
          if ( ch[i].sameStationAs(ch[k]) ) {
            count--;
            break;
          }
        }
      }
      return count;
    }
    /** Return a count of how many unique triaxial instruments are present in the list.
     * A "triaxial" is a unique "net", "sta", and the first two characters of the
     * SEED channel (e.g. "HH_" or "EL_").
     * The list does not have to be pre-sorted
     * and will not be sorted as a result of this call.<p>
     * @see Channel.equalsName()
     * */
    public static int getTriaxialCount(ChannelableListIF chList) {
      int size = chList.size();
      if (size == 0) return 0;
      Channelable ch[] = chList.getChannelableArray();
      int count = size;

      // for each entry, scan forward in list & only count if no repeats ahead
      for (int i = 0; i<size-1; i++) {
        for (int k = i+1; k<size; k++) {
          if ( ch[i].sameTriaxialAs(ch[k]) ) {
            count--;
            break;
          }
        }
      }
      return count;
    }

    public ChannelableListIF cullByChannelNameAttribute(String [] attributeValues,
                    int attributeId, int includeFlag) {
        return cullByChannelNameAttribute(new StringList(attributeValues), attributeId, includeFlag);
    }

    public ChannelableListIF cullByChannelNameAttribute(StringList attributeValueList,
                    int attributeId, int includeFlag) {
      return cullByChannelNameAttribute(this, attributeValueList, attributeId, includeFlag);
    }
    public static ChannelableListIF cullByChannelNameAttribute(ChannelableListIF chList,
                    String [] attributeValues, int attributeId, int includeFlag) {
      return cullByChannelNameAttribute(chList, new StringList(attributeValues), attributeId, includeFlag);
    }
    public static ChannelableListIF cullByChannelNameAttribute(ChannelableListIF chList,
                        StringList attributeValueList, int attributeId, int includeFlag) {

      ChannelableList newList = new ChannelableList();
      if (attributeValueList.size() == 0) return newList;
      int count = chList.size();
      String nameAttribute = null;
      Channelable ch = null;
      DbStringMatcher matcher = new DbStringMatcher(attributeValueList.toArray());
      for (int i = 0; i<count; i++) {
        ch = (Channelable) chList.get(i);
        nameAttribute = ch.getChannelObj().getChannelNameAttribute(attributeId);
        if (includeFlag == INCLUDE) {
          //if (attributeValueList.contains(nameAttribute)) newList.add(ch); // 2008/01/24 -aww
          if (matcher.matches(nameAttribute)) newList.add(ch); // allows db wildcard 2008/01/24 -aww
        } else if (includeFlag == EXCLUDE) {
          //if (! attributeValueList.contains(nameAttribute)) newList.add(ch); // 2008/01/24 -aww
          if (! matcher.matches(nameAttribute)) newList.add(ch); // allows db wildcard 2008/01/24 -aww
        }
        else {
          System.err.println("Invalid cullByChannelNameAttribute flag value: " + includeFlag);
        }
      }
      return newList;
    }

    /** Return a new ChannelableList that is a subset of this list and contains
     * only those Channelable objects that were ACTIVE on the input true UTC seconds time.
     */
    public ChannelableListIF cullByTime(double trueSecs) {
      return cullByTime(this, trueSecs);
    }

    public static ChannelableListIF cullByTime(ChannelableListIF chList, double time) {
      ChannelableList newList = new ChannelableList();
      int count = chList.size();
      Channelable ch = null;
      for (int i = 0; i<count; i++) {
        ch = (Channelable) chList.get(i);
        if ( ch.getChannelObj().wasActive(time) ) newList.add(ch); //see ChannelDataIF.isValidFor(date);
      }
      return newList;
    }

    /**
     * Replaces the Channel references of all readings in the list with
     * the equivalent Channel match from the specified input ChannelList.
     * This is used to replace partial Channel descriptions with possibly
     * more complete descriptions such as lat/lon/z, response, corrections,
     * etc.
     * @return count of Channels matched with input list
     */
    // To enable below method requires a "Channelable" interface change and
    // implementation in dependent classes, but time dependent lookUp is an
    // issue since Channelables don't all necessarily have time attribute data.
    // So in method we use a "null" or new Date() for currently active Channel
    // return "int" count of matched - aww
    public int matchChannelsWithList(ChannelList channelList) {
        return matchChannelsWithList(channelList, (java.util.Date) null);
    }

    public int matchChannelsWithList(ChannelList channelList, java.util.Date aDate) {
        int mySize = size();
        int matchCount = 0;
        for (int i = 0; i<mySize; i++) {
          if (((Channelable) get(i)).setChannelObjFromList(channelList, aDate)) matchCount++;
        }
        return matchCount;
    }

    /**
    * Looks up Channel data values for each Channel of this ChannelableList using
    * the static MasterChannelList list data. Channel must be currently active.<p>
    * Returns 'false' if any channels were missing in DataSource.
    * @see #matchChannelsWithDataSource(ChannelableListIF, java.util.Date)
    */
    public boolean matchChannelsWithDataSource() {
      return matchChannelsWithDataSource ((java.util.Date) null);
    }
    /**
    * Looks up Channel data values for each Channel of this ChannelableList using
    * the static MasterChannelList list data. Channel must have been active on
    * the input date.<p>
    * Returns 'false' if any channels were missing in DataSource.
    * @see #matchChannelsWithDataSource(ChannelableListIF, java.util.Date)
    */
    public boolean matchChannelsWithDataSource(java.util.Date date) {
      return MasterChannelList.matchChannelsWithDataSource(this, date);
    }

    /** Return a new ChannelableList that is a subset of this list and
     * contains only those Channelable objects that have a non-NULL LatLonZ.
     */
    public ChannelableList cullNullLatLonZ() {
      ChannelableList newList = new ChannelableList();
      int count = size();
      Channelable ch = null;
      for (int i = 0; i<count; i++) {
        ch = (Channelable) get(i);
        if (ch.getChannelObj().hasLatLonZ()) newList.add(ch);
      }
      return newList;
    }

    /** Return a line-feed delimited String representation of this list's elements. */
    public String toString() {
      int count = size();
      StringBuffer sb = new StringBuffer((count+1)*256);
      if (count == 0) {
        sb.append("No channelables found in list.");
      } else {
        for (int i = 0; i < count; i++) {
          sb.append(get(i).toString()).append("\n");
        }
      }
      return sb.toString();
    }

    /** Write the line-feed delimited String representation of this list's elements to standard output. */
    public void dump() {
        System.out.println(toString());
    }

} // end of ChannelableList class
