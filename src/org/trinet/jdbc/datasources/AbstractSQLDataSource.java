package org.trinet.jdbc.datasources;

import org.trinet.jiggle.database.JiggleConnection;

import java.sql.Connection;

/**
 * Define the default data source for all JASI classes.  This data source will
 * be used in calls and in other methods when no data source is given.
 * */
public abstract class AbstractSQLDataSource implements SQLDataSourceIF  {

    // example defaults could be declared in concrete instance resulting
    // from Abstract..DataSource create(...) then accessed via interface
    // lookup methods of type "getDefaultXXX()"
    public static String  DEFAULT_DS_SUBPROTOCOL = "oracle:thin";
    public static String  DEFAULT_DS_DRIVER   = "oracle.jdbc.driver.OracleDriver";
    public static String  DEFAULT_DS_PORT     = "1521";
    public static String  POSTGRESQL_DS_SUBPROTOCOL = "postgresql";
    public static String  POSTGRESQL_DS_DRIVER = "org.postgreSQL.Driver";
    public static String  POSTGRESQL_DS_PORT     = "5432";
    //public static String  DEFAULT_DS_DOMAIN   = "gps.caltech.edu";
    //public static String  DEFAULT_DS_DBNAME     = "databasear";
 
    public static final int UNKNOWN = 0;
    public static final int DBASE   = 1;
    public static final int FILE    = 2;

    /** Value that tells if we are using a "DATABASE" or a "FILE" */
    protected int source = UNKNOWN;

    /** The static SQL data connection */
    protected Connection conn = null;
    protected JiggleConnection jiggleConnection = null; // database specific logics

    // filename - if working with a file rather then a database
    protected String filename = "";
    protected String serviceName = "";

    /** The DbaseConnectionDescription object of the current connection.
     *  @See: DbaseConnectionDescription*/
    protected DbaseConnectionDescription desc = new DbaseConnectionDescription();

    /** Flag that controls ability to write back to the data source. This flag
     *  just controls if a call to commit() will attempt to write to the data
     *  source. It can be used to make applications READ-ONLY. By default it is
     *  'true'.
     *  */
    /* Note that this is NOT the same as the Connection.isReadOnly()
     * condition. It also no longer sets org.trinet.jdbc classes to make table
     * row locks (using SelectForUpdate). Such locks caused unexpected and
     * apparently unrelated tables to become locked until commit() or rollback.
     * Now this flag just controls if a call to commit() will attempt to write.*/
    //static boolean writeBackEnabled = false;
    protected boolean writeBackEnabled = true;

    /** Constructor protected, use static createDataSource() factory method to create a concrete instance.
     * @see #createDataSource()
     * */
    protected AbstractSQLDataSource() {
        serviceName = getClass().getName();
    }

    /** Real Construction Method */
    public static SQLDataSourceIF createDataSource(String sClassName) {
        SQLDataSourceIF  newDataSource = null;
        try {
          newDataSource =
           (SQLDataSourceIF) Class.forName(sClassName).newInstance();
        }
        catch (ClassNotFoundException ex) {
          ex.printStackTrace();
        }
        catch (InstantiationException ex) {
          ex.printStackTrace();
        }
        catch (IllegalAccessException ex) {
          ex.printStackTrace();
        }
        return newDataSource;
    }
    /**
     * Configure using values specified in the input property file.
     * @see  DbaseConnectionDescription.readFromPropertyfile() */
    public void configure(String propertyFileName)   {
      setServiceByProperties(propertyFileName);
      if (desc.isValid()) configure(desc);
      else throw new IllegalArgumentException("Database description invalid, check input property file: "+propertyFileName);
    }
 

    /** Configure using an existing jdbc connection to a database. Data
     * will be accessed READONLY. That is writeBackEnabled = false */
    public void configure(Connection conn)
    {
        configure(conn, false);
    }

    /** Configure using a jdbc connection. If 'allowWriteBack' is
        true, caller will be allowed to update, delete, and insert data. The
        default is 'false'. */
    public void configure(Connection conn, boolean allowWriteBack)
    {
        set(conn);
        setWriteBackEnabled(allowWriteBack);
    }
    /**
     * Configure by opening a jdbc connection. Data will be accessed
     * READONLY. That is writeBackEnabled = false.
     * Parameter 'dbaseURL' is a verbose JDBC style URL of the form:<p>
     * <subprotocol>:<subname>:@<IP-address>:<port>:<dbasename> </p>
     * Ex: "jdbc:oracle:thin:@serverma.gps.caltech.edu:1521:databasema"
     * */
    public void configure(String dbaseURL,
                       String driver,
                       String username,
                       String passwd)   {
        configure( dbaseURL, driver, username, passwd, false);
    }
    /**
     * Configure by opening a jdbc connection. Data will be accessed READONLY
     * if 'allowWriteBack' is false, caller will not be allowd to update,
     * delete, and insert data.
     * Parameter 'dbaseURL' is a verbose JDBC style URL of the form:<p>
     * <subprotocol>:<subname>:@<IP-address>:<port>:<dbasename> </p>
     * Ex: "jdbc:oracle:thin:@serverma.gps.caltech.edu:1521:databasema"
     * */
    public void configure(String dbaseURL,
                       String driver,
                       String username,
                       String passwd,
                       boolean allowWriteBack)   {
        set( dbaseURL, driver, username, passwd);
        setWriteBackEnabled(allowWriteBack);
    }

    /** Configure a jdbc connection attributes are described by the
    *  input DbaseConnectionDescription.
    *  @see DbaseConnectionDescription
    *  */
    public void configure(DbaseConnectionDescription desc) {
        configure(desc.getURL(), desc.getDriver(),
             desc.getUserName(), desc.getPassword(), true);
    }

    /** Return the connection of this source. */
     public Connection getConnection() {
        return conn;
    }
    /** Returns true if the connection is not open. */
     public abstract boolean isClosed() ;
     //return (conn == null) ? true : conn.isClosed()

    /** Set flag true/false to allow/disallow writing back to the data
      * source. In some implementations setting this flag may lock data records
      * that are read until SQLDataSourceIF.commit() is called.
      * The actual locking behavior depends on the schema implementation.
      * */
    public void setWriteBackEnabled(boolean tf) {
        writeBackEnabled = tf;
        // this makes subsequent uses of ExecuteSQL methods do select-for-update
        // which allows writeback later. Rows are LOCKED until a commit is done.
        // ExecuteSQL.setSelectForUpdate(tf);
    }

    /** Return true if you can write results back to the data source */
    public boolean isWriteBackEnabled() {
        return writeBackEnabled;
    }

    /** Set/reset the DbaseConnectionDescription.
     *  Establishes a new connection to data source using the description's
     *  data attributes. Closes any existing instance connection.
     *  @see: DbaseConnectionDescription
     *  */
    public boolean set(DbaseConnectionDescription descript) {
      // only act if its a change
      if (! desc.equals(descript) || isClosed() ) { // or is closed - aww 07/12/2006
        if (! isClosed() ) close(); // close existing static connection - aww 01/11/2007
        desc = descript;
        source = DBASE;
        //System.out.println("DEBUG " + getClass().getName() + " setting db descript and getNewConnect()...");
        conn = getNewConnect();
        return (conn != null);
      }
      return true;   // no change
    }

    public DbaseConnectionDescription getDbaseConnectionDescription() {
      return desc;
    }

    /** Return database driver name. */
    public String getDriver() {
        return desc.getDriver();
    }
    /** Return database host name. */
    public String getHostName() {
        return desc.getHostName();
    }
    /** Return database host IP address like we.cannot.edu . */
    public String getIPAddress() {
        return desc.getIPAddress();
    }
    /** Return the connection's username. */
    public String getUserName() {
           return desc.getUserName();
    }

    /** Return the port number. */
    public int getPort() {
        int value = -1;
        try {
          value = Integer.parseInt(desc.getPort());
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return value;
    }

    /** Return the database name. */
    public String getDbaseName() {
        return desc.getDbaseName();
    }

    public String getServiceName() {
      return serviceName;
    }
    public void setServiceName(String name) {
      serviceName = name;
    }

    // public interface "setter" methods
    public void setPort(int port) {
        desc.setPort(port);
    }
    public void setDbaseName(String dbaseName) {
        desc.setDbaseName(dbaseName);
    }
    public void setUserName(String userName) {
        desc.setUserName(userName);
    }
    public void setHostName(String hostName) {
        desc.setHostName(hostName);
    }
    public void setIPAddress(String ipAddr) {
        desc.setIPAddress(ipAddr);
    }

    /** Return a String describing the data attributes of this instance. */
    public String toString() {
        if (source == DBASE) {
            return "DataSource is a database:"+
                " URL = "+desc.getURL()+"\n"+
                " domain = "+desc.getDomain()+
                " host = "+desc.getHostName()+
                " port = "+desc.getPort()+
                "\n driver = "+desc.getDriver()+
                " username = "+ desc.getUserName()+
                " writable= "+writeBackEnabled;
        } else if (source == FILE) {
            return "DataSource is file = "+filename+
                "\n writable= "+writeBackEnabled;
        }
        return "DataSource is unknown.";
    }

    abstract public void setServerTimeoutMillis(int millis) ;

    public void setServer(String ipAddress, int port) {
        //if (desc == null) return;
        desc.setIPAddress(ipAddress);
        desc.setPort(port);
    }
    public void setServiceByProperties(String propFileName) {
        desc.readFromPropertyFile(propFileName);
    }

    /** Abbreviated description of connection source.*/
    public String toDumpString() {
        if (source == DBASE) {
            return getClass().getName() + " URL= "+desc.getURL()+ " user= "+ desc.getUserName();
        } else if (source == FILE) {
            return "DataSource file:"+filename;
        }
        return "DataSource is unknown.";
    }
    /** Expanded description of connection source.*/
    public String describeConnection() {
        return
          " URL = "+ desc.getURL()+ "\n"+
          " dbase = " + desc.getDbaseName() + "\n"+
          " driver = "+ desc.getDriver()+ "\n"+
          " username = "+ desc.getUserName();
    }

    /** Set/reset the data attributes of this instance to match the metadata of
     * the input, assumes the input connection already open (i.e. connected).
     * */
    public abstract boolean set(Connection connection);


    /** Set/reset the connection using the input attributes to describe
     * the default connection.
     * */
    public abstract void set(String url,
                       String driver,
                       String username,
                       String passwd);

    /** Returns a new Connection to the current source.  Invoked by 
     * methods creating the default connection. Can be invoked to return
     * additional connections to the source described by instance metadata. */
    public abstract Connection getNewConnect();

    public abstract String getConnectionStatus();

    /** Return true if you cannot write results back to the data source */
    public abstract boolean isReadOnly();

    /**
     * Commit data modification transactions via the connection to the source.
     * Any changes made to the data should not be stored unless committed over
     * the active connection.
     * */
    public abstract void commit(Connection connection);

    /**
     * Commits any data modification transactions made via the connection
     * the data source.
     * If this is not called changes will not take effect. */
    public void commit() {
        commit(conn);
    }

    /**
     * Rollbacks data modifications.
     * Set data to their state in the data source when last committed.
     */
    public abstract void rollback();

    /** Close the connection */
    public abstract void close();

    /** Return true if connection is internal to database (used by java classes loaded inside database server) */
    public abstract boolean onServer();

    public JiggleConnection getJiggleConnection() {
        return jiggleConnection;
    }
} // AbstractSQLDataSource
