package test.common;

import java.nio.file.FileSystems;

public class UnitTests {

    protected String getTestPropFilePath() {
        if (isOnLocalMachine()) {
            return FileSystems.getDefault().getPath(new String("./"))
                    .toAbsolutePath().getParent().toString() + "\\testprop\\";
        } else {
            // For gitlab pipeline
            // TODO make it work for people running locally on non windows machines
            // TODO https://gitlab.com/aqms-swg/aqms-jiggle/-/issues/240
            return FileSystems.getDefault().getPath(new String("./"))
                    .toAbsolutePath().getParent().toString() + "/../testprop/";
        }
    }

    public static boolean isOnLocalMachine() {
        return System.getProperty("os.name").contains("Windows");
    }

    protected static String getTestDataFilePath() {
        if (isOnLocalMachine()) {
            return FileSystems.getDefault().getPath(new String("./"))
                    .toAbsolutePath().getParent().toString() + "\\testdata\\";
        } else {
            return FileSystems.getDefault().getPath(new String("./"))
                    .toAbsolutePath().getParent().toString() + "/../testdata/";
        }

    }
}
