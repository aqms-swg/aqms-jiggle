package org.trinet.util.graphics;
import java.io.*;
import javax.swing.*;
import org.trinet.jasi.DataSource;
import org.trinet.jasi.EventSelectionProperties;
import org.trinet.jdbc.datasources.DbaseConnectionDescription;
import org.trinet.util.GenericPropertyList;
import org.trinet.util.graphics.EventSelectionDialog;

public class EventSelectionPropertyEditor {

    private static EventSelectionDialog eventSelectionDialog = null;

    private EventSelectionPropertyEditor() { }

    public static final void main(String [] args) {

        if (args.length < 1) {
            System.err.println("Syntax Error: [propfilename] [javaSystemPropPath].");
            doExit(0);
        }

        String filename = args[0];

        String pathname = null;
        if (args.length > 1) {
            pathname = System.getProperty(args[1]);
            if (pathname != null) pathname = pathname + EventSelectionProperties.FILE_SEP + filename;
        }
        if (pathname == null) pathname = filename;

        EventSelectionProperties evtProps = new EventSelectionProperties();
        boolean status = evtProps.setUserPropertiesFileName(pathname);

        File file = new File(evtProps.getUserPropertiesFileName());
        JFileChooser jfc = new JFileChooser(file);
        jfc.setSelectedFile(file);

        String str = null;

        if (jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {

            file = jfc.getSelectedFile();
            pathname = file.getAbsolutePath();
            filename = file.getName();

            if (! file.exists() ) {
              str = pathname + " file D.N.E. !"; 
              System.err.println(str);
              JOptionPane.showMessageDialog(null, str, "Load Event Selection Properties", JOptionPane.ERROR_MESSAGE);
              doExit(0);
            }
            evtProps.setUserPropertiesFileName(pathname);
        }
        else {
            System.out.println("INFO: Event selection properties file not changed");
            doExit(0);
        }

        status = evtProps.reset();
        if (! status) {
          str = getPropertyFileStatusFor(evtProps, null);
          System.err.println(str);
          JOptionPane.showMessageDialog(null, str, "Event Selection Properties", JOptionPane.PLAIN_MESSAGE);
          doExit(0);
        }

        DbaseConnectionDescription dcd = new DbaseConnectionDescription();
        dcd.parseFromProperties(evtProps);
        System.err.println("Connection URL :\n" + dcd.getURL());
        if (dcd.isValid()) {
            DataSource ds = new DataSource();
            ds.set(dcd);
        }
        else {
          str = dcd.getURL();
          System.err.println("Invalid connection URL :\n" + str);
          JOptionPane.showMessageDialog(null, str, "Invalid Database Connection Properties", JOptionPane.PLAIN_MESSAGE);
          doExit(0);
        }

        showEditor(evtProps);

        doExit(0);
    }

    private static void processPropertyEditorDialog() {
        if (eventSelectionDialog.getButtonStatus() == JOptionPane.OK_OPTION) {
            EventSelectionProperties newEventProps = eventSelectionDialog.getEventSelectionProperties();
            if (checkEventProperties(newEventProps)) {
               System.out.println("EventSelectionProperties after editing:\n"+newEventProps.listToString());
               if (! newEventProps.saveProperties()) {
                  System.err.println("Error saving edited event selection properties to file.");
               }
            }
            else showEditor(newEventProps);
        }
        else {
            System.out.println("INFO: Event selection properties not edited");
        }
    }

    public static final void showEditor(EventSelectionProperties eventProps) {
        if (eventSelectionDialog != null) eventSelectionDialog.dispose(); 
        eventSelectionDialog = new EventSelectionDialog(null, "Event Selection", true, eventProps);
        eventSelectionDialog.setVisible(true); // show the dialog
        processPropertyEditorDialog();
    }

    private static final String getPropertyFileStatusFor(GenericPropertyList gpl, String msg) {
          StringBuffer sb = new StringBuffer(256);
          sb.append("Input properties file status:\n");
          sb.append(" Default: ").append(gpl.getDefaultPropertiesFileName());
          sb.append(" --> exists: " ).append(gpl.hasDefaultPropertiesFile()).append("\n");
          sb.append(" User   : ").append(gpl.getUserPropertiesFileName());
          sb.append(" --> exists: ").append(gpl.hasUserPropertiesFile()).append("\n");
          if (msg != null) {
            sb.append(msg);
          }
          return sb.toString();
    }

    private static final boolean checkEventProperties(EventSelectionProperties eventProps) {
        // Check event selection properties has specified a valid time span
        if (! eventProps.hasValidTimeSpan()) {
            JOptionPane.showMessageDialog( null,
               "WARNING: Event selection properties specifies an invalid time span!\n"+
               "You must reset you event selection time properties!",
               "Invalid Catalog Time Span", JOptionPane.PLAIN_MESSAGE);
            return false;
        }
    
        // Check total days in TimeSpan requested
        int days = (int) (eventProps.getTimeSpan().getDuration()/ 86400. + 0.5);
        if (days > 31) {
            int yn = JOptionPane.showConfirmDialog(null,
               "WARNING: event selection time range spans: "+days+" days.\n"+
               "Do you want to reset the event time range properties?",
               "Large Catalog Time Span", JOptionPane.YES_NO_OPTION
            );
            if (yn == JOptionPane.YES_OPTION) return false;
        }
        return true;
    }

    private static final void doExit(int status) {
        if (eventSelectionDialog != null) eventSelectionDialog.dispose(); 
        System.exit(status);
    }
}
