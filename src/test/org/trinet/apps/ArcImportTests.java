package test.org.trinet.apps;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.trinet.apps.ArcImport;
import test.common.DatabaseTests;
import test.testqueries.common.TestQueries;

import static org.junit.jupiter.api.Assertions.*;

public class ArcImportTests extends DatabaseTests {

    @BeforeAll()
    public static void addDataToDatabase() throws Exception {
        TestQueries testQueries = new TestQueries(postgreSQLContainer.getJdbcUrl());
        int rows = testQueries.populateDataForArcImportTest();
        assertTrue(rows > 0);
    }

    @Test
    public void verify_arc_import_main_function_runs() throws Exception {
        try {
            ArcImport arcImport = new ArcImport(postgreSQLContainer);
            arcImport.importArc(getTestPropFilePath() + "testcontainers_z72109735_9999.arcout");
        } catch (Exception e) {
            fail("ARC Import main function failed to run successfully");
        }
    }
}