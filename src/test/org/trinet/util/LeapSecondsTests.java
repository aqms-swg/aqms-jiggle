package test.org.trinet.util;

import org.junit.jupiter.api.Test;
import org.trinet.util.LeapSeconds;
import test.common.DatabaseTests;
import test.common.UnitTests;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LeapSecondsTests extends DatabaseTests {

    @Test
    public void verify_nominal_to_true_returns_correct_value() {
        assertEquals(1.26230403E8, LeapSeconds.nominalToTrue(126230400));
    }

    @Test
    public void verify_true_to_nominal_returns_correct_value() {
        assertEquals(1.26230398E8, LeapSeconds.trueToNominal(126230400));
    }

    @Test
    public void verify_get_leap_secs_at_nominal_returns_correct_value() {
        assertEquals(3.0, LeapSeconds.getLeapSecsAtNominal(126230400));
    }

    @Test
    public void verify_get_leap_secs_at_true_returns_correct_value() {
        assertEquals(2.0, LeapSeconds.getLeapSecsAtTrue(126230400));
    }

    @Test
    public void verify_get_leap_secs_at_true_with_illegal_value_returns_correct_value() {
        assertEquals(0.0, LeapSeconds.getLeapSecsAtTrue(-1));
    }

    @Test
    public void verify_seconds_lept_at_returns_correct_value() {
        assertEquals(1.0, LeapSeconds.secondsLeptAt(126230402));
    }

    @Test
    public void verify_true_to_string_returns_correct_value() {
        assertEquals("1973-12-31 23:59:58.000", LeapSeconds.trueToString(126230400));
    }

    @Test
    public void verify_true_to_string_with_custom_pattern_returns_correct_value() {
        assertEquals("197312312359", LeapSeconds.trueToString(126230400, "yyyyMMddHHmm"));
    }

    @Test
    public void verify_true_to_PST_returns_correct_value() {
        assertEquals("1973-12-31 15:59:58.000", LeapSeconds.trueToPST(126230400));
    }

    @Test
    public void verify_string_to_true_returns_correct_value() {
        assertEquals(126230400, LeapSeconds.stringToTrue("1973-12-31 23:59:58.000"));
    }
}
