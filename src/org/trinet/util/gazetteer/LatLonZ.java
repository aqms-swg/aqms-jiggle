package org.trinet.util.gazetteer;

import java.math.*;
import java.text.*;
import java.util.*;
import java.io.Serializable;

/**
 * Geographical reference point latitude, longitude, elevation/depth (z)
 * Northern and eastern hemispheres are positive.  Southern and western
 * hemispheres are negative.  Depths are negative, elevations are positive; z
 * units can be specified by setZUnits(...)  else the units default to
 * kilometers.  Distances are returned in units set by the GeoidalConvert class.<p>
 * Because lat, lon and z are stored a type double an uninitialized LatLonZ object
 * will return lat=0.0, lon=0.0, z=0.0 .
 */
// Made serializable - DDG 7/21/2000

public class LatLonZ implements Geoidal, Cloneable, Serializable {

// These all default to NaN
//    double latitude  = Double.NaN;
//    double longitude = Double.NaN;
//    double z         = Double.NaN;
// NOTE:  Double.NaN==Double.NaN has the value false! So it is difficult to use as a default value.
//
// TODO: either make data members DataDouble to discriminate true 0. from unset values
//       or make NullValue a number that is out of range of possible numbers,
//       0 may be a legitimate value for depth. Need to feret out the setting of 
//       of literal 0. values for lat,lon,z in the code base.
//

    public static final double NullValue = 0.0;

    private double latitude  = NullValue;
    private double longitude = NullValue;
    private double z         = NullValue;
    // Note to fix z at "zero" depth lat,lon,z can't all be set NullValue
    // to overcome this flaw set z to a small nonzero number like -.01
    //

    private GeoidalUnits zUnits = GeoidalUnits.KILOMETERS;

/**
 * Null constructor. Because lat, lon and z are stored a type double
 * an uninitialized LatLonZ object will return lat=0.0, lon=0.0, z=0.0
 */
    public LatLonZ() { }

/**
 * Contruct from existing LatLonZ. Basically a copy of the original.
 */
    public LatLonZ(LatLonZ llz) {
        copy(llz);
    }

/** 
 * Latitude and longitude in decimal degrees 
 */
    public LatLonZ(double lat, double lon, double zkm) {
        set(lat, lon, zkm);
    }

/** 
 * Latitude and longitude in interger degrees and decimal minutes
 */
    public LatLonZ(int lat, double latm, int lon, double lonm, double zkm) {
        set(lat, latm, lon, lonm, zkm);
    }

/** Get depth/elevation units (default == GeoidalUnits.KILOMETERS).
*/
    public GeoidalUnits getZUnits() {
        return zUnits;
    }

/** Set depth/elevation units (default == GeoidalUnits.KILOMETERS).
*/
    public void setZUnits(GeoidalUnits geoidalUnits) {
        if (geoidalUnits == GeoidalUnits.KILOMETERS) {
            zUnits = geoidalUnits;
        }
        else if (geoidalUnits == GeoidalUnits.MILES) {
            zUnits = geoidalUnits;
        }
        else if (geoidalUnits == GeoidalUnits.FEET) {
            zUnits = geoidalUnits;
        }
        else throw new IllegalArgumentException("setZUnits input type must be KILOMETERS, MILES, or FEET");
    }

/** Returns z value in equivalent kilometers */
    protected double zToKm() {
        return GeoidalConvert.toKm(z, zUnits);
    }

/**
 * Copy LatLonZ to this one by setting member data to input's values.
 */
    public void copy(LatLonZ llz) {
        set(llz.getLat(), llz.getLon(), llz.getZ());
        this.zUnits = llz.getZUnits();
    }

/* 
 * Latitude and longitude in decimal degrees 
    public void set(LatLonZ latlonz) {
        copy(latlonz);
    }
 */

    /**
     * Latitude and longitude in decimal degrees
     */
    public void set(double lat, double lon, double zkm) {
      setLat(lat);
      setLon(lon);
      setZ(zkm);
    }

/** 
 * Latitude and longitude in interger degrees and decimal minutes.
 * Sign is preserved only in the integer degrees; all minutes assume the same sign
 * as the degrees value. Sign and units of the elevation/depth (z) are preserved.
 */
    public void set(int lat, double latm, int lon, double lonm, double z) {
//      if (lat >= 0) setLat((double) lat + Math.abs(latm)/60.0);
//      else setLat((double) lat - Math.abs(latm)/60.0);
//
//      if (lon >= 0) setLon((double) lon + Math.abs(lonm)/60.0);
//      else setLon( (double) lon - Math.abs(lonm)/60.0);
//
//      setZ(z);

        set (degMinToDec(lat, latm), degMinToDec(lon, lonm), z);

    }
    /** Given coords in degrees/minutes return the equivalent in decimal degrees.*/
    public static double degMinToDec (double deg, double min) {
        if (deg >= 0) {
            return deg + Math.abs(min)/60.0 ;
        } else {
            return deg - Math.abs(min)/60.0;
        }
    }

    public void setLat(double lat) {
        latitude = (Double.isNaN(lat)) ? NullValue : lat;
    }
    public void setLat(int deg, double min) {
        setLat(degMinToDec(deg, min));
    }
    public void setLon(double lon) {
        longitude = (Double.isNaN(lon)) ? NullValue : lon;
    }
    public void setLon(int deg, double min) {
        setLon(degMinToDec(deg, min));
    }
    /** Set value of z and the units used.
     @See: GeoidalUnits */
    public void setZ(double z, GeoidalUnits zunits) {
        this.z = (Double.isNaN(z)) ? NullValue : z;
        setZUnits(zunits);
    }
    /** Set value of z. Assumes units are GeoidalUnits.KILOMETERS*/
    public void setZ(double km) {
        setZ(km, GeoidalUnits.KILOMETERS);
    }

    public LatLonZ getLatLonZ() {
        return this;
    }

/** Implementation of Geoidal interface method */
    public Geoidal getGeoidal() {
        return this;
    }

/** Implementation of Geoidal interface method */
    public double getLat() {
        return latitude;
    }

/** Return degrees part of latitude */
    public int getLatDeg() {
        return (int) latitude;
    }

/** return minutes part of latitude. This is always positive */
    public double getLatMin() {

       return 60.0 * (Math.abs(getLat()) - (double) Math.abs(getLatDeg()));
    }

/** Implementation of Geoidal interface method */
    public double getLon() {
        return longitude;
    }
/** Return degrees part of latitude */
    public int getLonDeg() {
        return (int) longitude;
    }

/** return minutes part of latitude. This is always positive */
    public double getLonMin() {

       return 60.0 * (Math.abs(getLon()) - (double) Math.abs(getLonDeg()));
    }
/** Implementation of Geoidal interface method */
    public double getZ() {
        return z;
    }

/**
 * Calculate the distance km from input location. 
 * Includes elevation/depth (z) in the distance calculation. 
 */
    public double distanceFrom(LatLonZ llz) {
        return GeoidalConvert.distanceKmBetween(this, llz);
    }

/**
 * Calculate the km horizontal distance from this location. 
 * Does NOT include elevation/depth (z) in the distance calculation. This is only accurate
 * over smaller distances because it calculates the length of a chord on the surface of a sphere.
 */
    public double horizontalDistanceFrom(LatLonZ llz) {
        return GeoidalConvert.horizontalDistanceKmBetween(this, llz);
    }

    /** Return the azimuth from this point to the argument.*/
    public double azimuthTo(LatLonZ llz) {
        return GeoidalConvert.azimuthDegreesTo(this, llz);
    }
    /** Return the azimuth from the argument to this point.*/
    public double azimuthFrom(LatLonZ llz) {
        return GeoidalConvert.azimuthDegreesTo(llz,this);
    }

    /** Returns true if LatLonZ's have equal values*/
    public boolean equals(Object object) {
        if (object == null) return false;
        try {
            LatLonZ llz =  (LatLonZ) object;
            if (
              latitude == llz.latitude &&
              longitude == llz.longitude &&
              z == llz.z &&
              zUnits == llz.zUnits
            ) return true;
        }
        catch (ClassCastException ex) {
        }
        return false;
    }

    /** Returns 'true' if the values of latitude, longitude and z are all are 0.0
     * which is the default if they have not been set. */

    public boolean isNull() {
      return 
          (latitude == NullValue &&
            longitude == NullValue &&
            z == NullValue);
    }

    /** Return 'true' if this is a valid LatLonZ. That is, the values of latitude,
     * longitude and z are not all 0.0 which is the default if they have not been set.*/
    public boolean hasLatLonZ() {
      return !isNull();
    }
    
    /** Parse a LatLonZ from the given String using the given delimiter.
    * If there are not enough tokens to define a LatLonZ "missing" values are
    * left in their initial state. */
    public static LatLonZ parse(String str, String delim) {
       return parse(new StringTokenizer(str, delim));
    }

    /** Parse a LatLonZ from the given StringTokenizer.
    * If there are not enough tokens to define a LatLonZ "missing" values are
    * left in their initial state. */
    public static LatLonZ parse(StringTokenizer strTok) {

       LatLonZ llz = new LatLonZ();
       try {
        try { llz.setLat( Double.parseDouble(strTok.nextToken()) ); }
        catch (NumberFormatException ex) {}

        try { llz.setLon( Double.parseDouble(strTok.nextToken()) ); }
        catch (NumberFormatException ex) {}

        try { llz.setZ( Double.parseDouble(strTok.nextToken()) ); }
        catch (NumberFormatException ex) {}

//            llz.setLat(Double.valueOf(strTok.nextToken()).doubleValue());
//            llz.setLon(Double.valueOf(strTok.nextToken()).doubleValue());
//            llz.setZ(Double.valueOf(strTok.nextToken()).doubleValue());

       } catch (NoSuchElementException ex) {}

       return llz;
    }

/** Returns String of latitude, longitude, and z fields delimited by blanks. */
    public String toString() {
        return toString(" ");
    }

/** Returns String of latitude, longitude, and z fields delimited by the
* given string. If any field is unknown it will appear as 0.0, except NaN. */
    public String toString(String delim) {
        return toString(delim, true);
    }

    public String toLatLonString() {
        return toString(" ", false);
    }

    public String toLatLonString(String delim) {
        return toString(delim, false);
    }

    private final String toString(String delim, boolean includeZ) {
        DecimalFormat df = new DecimalFormat ( "###0.000000" );
        StringBuffer sb = new StringBuffer(48);

        if (Double.isNaN(latitude)) sb.append("NaN");
        else sb.append(df.format(latitude));
        sb.append(delim);

        if (Double.isNaN(longitude)) sb.append("NaN");
        else sb.append(df.format(longitude));

        if (includeZ) {
          sb.append(delim);
          if (Double.isNaN(z)) sb.append("NaN");
          else sb.append(df.format(z));
        }

        return sb.toString();
    }

    public Object clone() {
       LatLonZ latLonZ = null;
       try {
          latLonZ = (LatLonZ) super.clone();
       }
       catch (CloneNotSupportedException ex)  {
          ex.printStackTrace();
       }
       return latLonZ;
    }

    //
    public int hashCode() {
        return Double.valueOf(latitude+longitude+z).hashCode();
    }
    //
}
