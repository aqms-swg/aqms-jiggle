package org.trinet.util;

/**
 * Class Utils contains various static utility methods.
 */
public class Utils {
  /**
   * Close the resource quietly ignoring any errors.
   * 
   * @param resource the resource or null if none.
   */
  public static void closeQuietly(AutoCloseable resource) {
    try {
      if (resource != null) {
        resource.close();
      }
    } catch (Exception ex) {
    }
  }

  /**
   * Close the resources quietly ignoring any errors.
   * 
   * @param resources the resources or null if none.
   */
  public static void closeQuietly(AutoCloseable... resources) {
    if (resources != null) {
      for (AutoCloseable resource : resources) {
        closeQuietly(resource);
      }
    }
  }

  /**
   * Returns {@code true} if the string is null, empty or contains only
   * {@link Character#isWhitespace(char) white space} characters, otherwise
   * {@code false}.
   * 
   * @param s the string.
   * @return {@code true} if the string is null, empty or contains only
   *         {@link Character#isWhitespace(char) white space} characters,
   *         otherwise {@code false}
   *
   * @see Character#isWhitespace(char)
   */
  public static boolean isBlank(String s) {
      if (s != null) {
          char c;
          final int length = s.length();
          for (int index = 0; index < length; index++) {
              c = s.charAt(index);
              if (!Character.isWhitespace(c)) {
                  return false;
              }
          }
      }
      return true;
  }
}
