package org.trinet.storedprocs.association;

import java.sql.*;
import java.util.*;

import org.trinet.jasi.*;
import org.trinet.storedprocs.DbaseConnection;
import org.trinet.util.*;
import org.trinet.util.gazetteer.LatLonZ;  /// LatLonZ

 /** Find and return an event that is likely to be associated with this point in time and
  *  location. This is used to find events associated with a peak amplitude. If you want to use
  * it for associating other times, say phase arrivals or coda terminations, override this
  * method. */
public class PointAssociator {

  //static boolean debug = true;
  static boolean debug = false;

  /** Event selection properties for candidate selection. */
  static EventSelectionProperties evtProps = new EventSelectionProperties();

  /** Coda-based energy time window
   * @See: org.trinet.util.EnergyWindow*/
  private static EnergyWindow ew = new EnergyWindow(); //static context
  {
    ew.setTravelTimeModel(TravelTime.getInstance());  // default traveltime model
  }
  /** Default values:
   PrePtime = 1.0;
   MinWindow = 20.0;
   MaxWindow = 120.0;
   */

  protected static Connection conn = null;

  //protected double magAssocThreshold = 0.;
 
  public PointAssociator() {

  }
  /** set event selection properties for candidate selection. */
  public static void setEventSelectionProperties(EventSelectionProperties props) {
      evtProps = props;
  }
  /** Get event selection properties for candidate selection. */
  public static EventSelectionProperties getEventSelectionProperties() {
      return evtProps;
  }

  /** Set the travel-time model used to calculate the energy window. */
   public static void setTravelTimeModel(TravelTime model) {
      ew.setTravelTimeModel(model);
   }

   /** Return the travel-time model used to calculate the energy window. */
   public static TravelTime getTravelTimeModel() {
      return ew.getTravelTimeModel();
   }

  /** Set the window seconds that will be used to find candidate events. Candidate will have
   * an origin time up to this value BEFORE the time passed to getAssocEvent().
   */
  public static void setCandidateTimeWindowSize(double seconds) {
      ew.setMaxWindow(seconds);
  }

  /** Return the window seconds that will be used to search for candidate events.
   * Candidate will have an origin time up to this value BEFORE the time passed to getAssocEvent().
   */
  public static double getCandidateTimeWindowSize() {
      return ew.getMaxWindow();
  }
  /** Return the best-fit associated Solution from the SolutionList for seismic energy at
   * this point in space/time.
   * That is, the event for which the given time is likely to be a peak amplitude at the given lat/lon/z .
   * Returns null if no solution meets the fit criteria. If more than one Solution fits only
   * the BEST fitting Solution is returned.  The BEST fit is the one closest to the expected
   * S-wave arrival time. */
  public static Solution getAssocEvent(LatLonZ llz, double ampTime, SolutionList solList) {

    // nothing, bail
    if (solList.isEmpty() || llz.isNull()) return null;

    // find best
    Solution sol;
    Solution bestSol = null;

    double distance = 0.0;
    double mag = 0.0;
    double biggestMag = -999.9;
    TimeSpan codaWindow = null;
    double ot;

    // scan candidates for best fit
    // "Best fit" really means: the event with an S-wave closest to the given time at this lat/lon
    for (int i = 0; i < solList.size(); i++ ) {

       sol = solList.getSolution(i);    // = (Solution) solList.get(i);
       
       if (debug) System.err.print("PointAssociator solList evid ="+ sol.id.longValue());
       if (sol.getLatLonZ().isNull()) continue;   // no loc., skip it

       ot = sol.getTime();
       distance = llz.horizontalDistanceFrom(sol.getLatLonZ());  // point-to-origin dist

       // dist may be NaN!
       if (Double.isNaN(distance) || Double.isNaN(ot)) continue; // insufficient info about sol

       if (sol.getPreferredMagnitude().hasNullValue()) {
         mag = -1.0;
         codaWindow = ew.getEnergyTimeSpan(ot, distance);        // use S-P technique
         if (debug) System.err.print(", window by S-P dist: " + distance + ", " + codaWindow);
       } else {
         mag = sol.getPreferredMagnitude().getMagValue();
         codaWindow = ew.getEnergyTimeSpan(ot, distance, mag);     // use coda technique
         if (debug) System.err.print(", window by mag: " + mag + ", " + codaWindow);
       }

       if (codaWindow.contains(ampTime)) {
         // Amp is within coda window, select the one with the largest magnitude to decide among candidates
         if (mag > biggestMag ) {
           // if (debug) System.err.println("PointAssociator: amp associates with EVID ="+ sol.id.longValue());
           bestSol = sol;
           biggestMag = mag;
           if (debug) System.out.println(", +assoc deltaT= "+Math.round(codaWindow.getStart()-ampTime)+", ampTime:"+LeapSeconds.trueToString(ampTime));
         }
       } else {
           if (debug) System.out.println(", -assoc deltaT= "+Math.round(codaWindow.getStart()-ampTime)+", ampTime:"+LeapSeconds.trueToString(ampTime));
       }

    }
    /* Instead just set in the event selection properties the minimum of the acceptable magnitude range for candidate list
    if (biggestMag < magAssocThreshhold) {
        System.out.println("PointAssociator skipped assoc of amptime: " +LeapSeconds.trueToString(ampTime)+
            " with event " + sol.id.longValue() + " mag: " + biggestMag + " < " + magAssocThreshhold + " acceptance threshhold");
        bestSol = null;
    }
    */
    return bestSol;
  }

  /** Return the best-fit associated Solution for this point in space/time.
   *  Returns null if no match. If more than one candidate event is found only
   *  the ID of the BEST fit is returned. That is, the event for which the given
   *  time is likely to be a peak amplitude at the given lat/lon/z . */
  public static Solution getAssocEvent(double lat, double lon, double z, double time) {

      return getAssocEvent(lat, lon, z, time, getCandidateSolutionList(time));
  }

  /** Return the best-fit associated Solution for this point in space/time.
   *  Returns null if no match. If more than one candidate event is found only
   *  the ID of the BEST fit is returned. That is, the event for which the given
   *  time is likely to be a peak amplitude at the given LatLonZ . */
  public static Solution getAssocEvent(LatLonZ llz, double time) {

      return getAssocEvent(llz, time, getCandidateSolutionList(time));
  }

  /** Return the best-fit associated Solution for this point in space/time.
   *  Returns null if no match. If more than one candidate event is found only
   *  the ID of the BEST fit is returned. That is, the event for which the given
   *  time is likely to be a peak amplitude at the given LatLonZ . */
  public static Solution getAssocEvent(double lat, double lon, double z,
                                       double time, SolutionList solList) {

      return getAssocEvent(new LatLonZ(lat, lon, z), time, solList);
  }

/** Return a SolutionList of candidate events for the target time.
 * Returns an empty SolutionList if no candidates are found.
 * Candidates are those events that may have produced significant seismic energy within the target time.
 * Therefore, they will have origin times BEFORE the target time.
 * @see: setCandidateTimeWindowSize()*/
  public static SolutionList getCandidateSolutionList(double targetTime) {
    return getCandidateSolutionList(targetTime, targetTime);
  }
  /** Return a SolutionList of candidate events for target times within 'timeSpan'.
   * Returns an empty SolutionList if no candidates are found.
   * Candidates are those events that may have produced significant seismic energy within the target time.
   * Therefore, they will have origin times BEFORE the target time.
   * @see: setCandidateTimeWindowSize()*/
   public static SolutionList getCandidateSolutionList(TimeSpan targetSpan) {
     return getCandidateSolutionList(targetSpan.getStart(), targetSpan.getEnd());
   }
/** Return a SolutionList of candidate events for targe times within the given range.
 * Returns an empty SolutionList if no candidates are found.
 * Candidates are those events that may have produced significant seismic energy within the target time.
 * Therefore, they will have origin times BEFORE the target time.<p>
 * <pre>
         Candidate window size      target timerange
    |----------------------------|====================|
                   Full OT search window
    |:::::::::::::::::::::::::::::::::::::::::::::::::|
</pre>
 * @see: setCandidateTimeWindowSize()*/
   public static SolutionList getCandidateSolutionList(double targetStart, double targetEnd) {

     // define the search window, event origin time must be BEFORE target time
     if (debug) {
         System.out.println("DEBUG PointAssociator getCandidateSolutionList targetStart,End:" + LeapSeconds.trueToString(targetStart) +
                 "-> " + LeapSeconds.trueToString(targetEnd));
         System.out.println("DEBUG PointAssociator getCandidateSolutionList timeWindow size:" + getCandidateTimeWindowSize());
     }
     double OTstart = targetStart - getCandidateTimeWindowSize();
     double OTend   = targetEnd;  // OT can't be after target time

     // get candidate events before 'searchTimeWindow'
     SolutionList solList = new SolutionList();

//     solList.fetchValidByTime(OTstart, OTend);  // get candidates
     getEventSelectionProperties().setTimeSpan(OTstart, OTend);
     if (conn == null) conn = DbaseConnection.getConnection(); // if not defined, create a new one from DataSource
     solList.fetchByProperties(conn, getEventSelectionProperties());
     if (debug) {
       System.out.println("DEBUG PointAssociator getCandidateSolutionList eventSelectionProperties:\n");
       getEventSelectionProperties().dumpProperties();
       System.out.println(solList.getNeatHeader());
       System.out.println(solList.dumpToString());
     }

     return solList;
  }
/** Return a SolutionList of candidate events. Returns an empty SolutionList if
no candidates. */
  public static SolutionList getCandidateSolutionList(JasiReadingList list) {
    if (list.isEmpty()) return new SolutionList();
    // Get earliest and latest amps in list - ASSUMES ITS TIME SORTED
    double t0 = ((Amplitude)list.get(0)).getTime(); // 2008/02/08 -aww
    double t1 = ((Amplitude)list.get(list.size()-1)).getTime(); // 2008/02/08 -aww

    // make a candidate solution list
    // Math.max() & Math.min() insure the start/end are in the right order whether
    // the list is sorted descending or ascending.
    return getCandidateSolutionList(Math.min(t0, t1), Math.max(t0, t1));
  }
  /** Set up behavior parameters and establish dbase connection.
   *
   */
//  private static void setup() {
//    DbaseConnection.getConnection();
//
//    // setup behavior
//    searchTimeWindow = 121.0; // secs
//  }

/*
  public static void main(String[] args) {

    //long id = 56998981;
    long evid = 12475624;

    DbaseConnection.getConnection();

    List aList = (List) Amplitude.create().getBySolution(evid);
    Amplitude amps[] = (Amplitude []) aList.toArray(new Amplitude[aList.size()]);
    Channel chan = Channel.create();
    for (int i = 0; i< amps.length; i++) {
       System.out.println(amps[i].toString());

       Channel ch = chan.lookUp(amps[i].getChannelObj());
       LatLonZ llz = ch.getLatLonZ();
       PointAssociator.setCandidateTimeWindowSize(60.0);
       Solution sol = PointAssociator.getAssocEvent(llz.getLat(), llz.getLon(), llz.getZ(),
       amps[i].getTime());
       System.out.println("Result 1: evid = "+ sol.toString());

    }
  }
*/
}
