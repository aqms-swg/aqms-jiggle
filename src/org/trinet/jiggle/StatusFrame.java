package org.trinet.jiggle;

import java.awt.*;
import javax.swing.*;
import org.trinet.util.graphics.*;
/**
 * A popup frame used to show the status of an operation. Doesn't really work
 * well for showing dynamic progress because the threading isn't handled right.
 */
public class StatusFrame extends JFrame {

     public static final int BEEP_OFF      = 0;
     public static final int BEEP_ON_CLOSE = 1;
     public static final int BEEP_ON_OPEN  = 2;
     public static final int BEEP_ON       = 3;

     protected int beep = BEEP_OFF; // make no sound on open/close

     private JLabel textLabel = new JLabel();

     private JProgressBar progressBar = new JProgressBar(0, 100); // make 0-100%
     private JLabel graphicLabel = new JLabel();
     private JPanel mainPanel = new JPanel();

     // default
     private ImageIcon defIcon = new ImageIcon(IconImage.getImage("gearsAnim.gif"));
    //     ImageIcon defIcon = null;

     public StatusFrame() {
          try  {
               jbInit();
          }
          catch(Exception e) {
               e.printStackTrace();
          }
     }

     public StatusFrame(String title, String text) {
       this();
       set(title, text);
     }

     public StatusFrame(String title, String text, Icon icon) {
         this();
         set(title, text, icon);
     }


     public void set(String title, String text, Icon icon) {
        setTitle(title);
        setText(text);
        setIcon(icon);
        setProgress(0);

        pack();
     }
     public void set(String title, String text) {
        setTitle(title);
        setText(text);
        setIcon(defIcon);
        setProgress(0);

        pack();
     }
/** Set the text in the text panel. */
     public void setText(String text) {
       textLabel.setText(text);
       pack();
     }

     public void setDefaultIcon() {
         setIcon(defIcon);
     }

     public void setIcon(Icon icon) {

         if (icon != null) graphicLabel.setIcon(icon);

//        graphicLabel = new JLabel(icon);
        pack();
     }

     public void setProgress(int percent) {
        progressBar.setValue(percent);
        progressBar.setString(percent+"%");
     }
     public void setProgressIndeterminate(boolean tf) {
        //jdk1.4 progressBar.setIndeterminate(tf); // aww 02/24/2005 
     }

    public void setProgressStringPainted(boolean tf) {
        progressBar.setStringPainted(tf);
    }

     public void setProgress(double percent) {
        setProgress( (int) percent);
     }

// Create the object (built using JBuilder)
     private void jbInit() throws Exception {

          textLabel.setText("status text here");
          mainPanel.setLayout(new BorderLayout());
          mainPanel.setMinimumSize(new Dimension(200, 150));
          mainPanel.setPreferredSize(new Dimension(200, 150));

          graphicLabel.setHorizontalAlignment(SwingConstants.CENTER);
//          graphicLabel.setIcon(new ImageIcon(new java.net.URL("file:///e:/TPP/images/tsunami.gif")));
          mainPanel.add(graphicLabel, BorderLayout.CENTER);
          mainPanel.add(textLabel, BorderLayout.NORTH);
          mainPanel.add(progressBar, BorderLayout.SOUTH);
          progressBar.setStringPainted(true);

          this.getContentPane().add(mainPanel);

          centerDialog();
     }

/**
 * Center the dialog on the screen
 */
    protected void centerDialog() {
        Dimension screenSize = this.getToolkit().getScreenSize();
        Dimension size = this.getSize();
        screenSize.height = screenSize.height/2;
        screenSize.width = screenSize.width/2;
        size.height = size.height/2;
        size.width = size.width/2;
        int y = screenSize.height - size.height;
        int x = screenSize.width - size.width;
        this.setLocation(x,y);
    }

// These don't work well. The stuff that runs after this is called seems to
// crowd this off the Event Dispatch queue and the dialog doesn't showup until the other
// stuff is done!
/** Puts the frame creation job on the GUI event queue and returns. */
    public void pop(String title, String message, boolean indeterminate) {
        setProgressIndeterminate(indeterminate);
        setProgressStringPainted(! indeterminate);
        set(title, message);
        setVisible(true);
        if (beep == BEEP_ON || beep == BEEP_ON_OPEN) getToolkit().beep(); // make noise
    }
    public void unpop() {
        setProgressIndeterminate(false);
        setProgressStringPainted(true);
        setVisible(false);
        if (beep == BEEP_ON || beep == BEEP_ON_CLOSE) getToolkit().beep(); // make noise
    }

    public void setBeep(int state) {
      beep = state;
    }

/*
public static class Tester {
     public static void main(String[] args) {
    //      ImageIcon defIcon = new ImageIcon("images/swing-64.gif");

        ImageIcon defIcon = new ImageIcon("images/gearsAnim.gif");

     //   JLabel pic = new JLabel(defIcon);
     //   jpanel1.add(pic);
     
          StatusFrame statusFrame =
      //          new StatusFrame("Test frame", "Something's happening", defIcon);
          new StatusFrame("Test frame", "Something's happening");
  //        statusFrame.pack();
          statusFrame.setVisible(true);

          statusFrame.setProgress(43);

     }
  } // end of Tester
*/
}
