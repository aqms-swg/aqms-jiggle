//
//TODO: Refactor to super Class the methods generic to abstract getReadingList type
//
package org.trinet.util.graphics.table;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import org.trinet.jasi.*;
import org.trinet.util.*;
import org.trinet.util.graphics.*;
public class JasiAmpListPanel extends AbstractJasiMagnitudeAssocPanel {
    public JasiAmpListPanel() {this(null,false);}
    public JasiAmpListPanel(JTextArea textArea, boolean updateDB) {
        super(textArea,updateDB);
        initPanel();
    }
    protected void initPanel() { 
        //if (tableColumnOrderByName == null) this.tableColumnOrderByName = JasiAmpListTableConstants.columnNames;
        dataTable = new JasiAmpListTable(new JasiAmpListTableModel());
        super.initPanel();
    }
    protected void setList(Solution sol) {
        aMag = null;  // avoids appending values to prior mag in update of solution
        aSol = sol;
        setList(sol.getAmpList());
    }
    protected void setList(Magnitude mag) {
        aMag = mag;
        // need like method isAmpMag() or like for amplitude dependence
        if (aMag.isAmpMag()) setList(aMag.getReadingList());
        else
          System.err.println(panelName +
               " Error setting model AmpList from input not an AmpMag (perhaps CodaMag)"); 
    }
    /*
    protected int updateList() {
      if (! confirmUpdate()) return 0;
      return synchInputList();
    }
    */

    /* Could replace the method below by generic abstraction in superclass
    // Need application USER OPTION to select add or replacement strategy here:
    protected int synchInputList() {
      Debug.println("DEBUG updating panel list for " + panelName);
      if (! isListUpdateNeeded()) return 0;
      AmpList aList = (AmpList) getList();
      if (inputList == aList) { // identical list object no-op
        setListUpdateNeeded(false);
        return 0;
      }
      boolean added = false;
      for (int idx=0; idx < aList.size(); idx++){
        Amplitude amp = (Amplitude) aList.get(idx);
        // always update, but amp may not be related to prefmag:
        // test trial of  "replacement" in lieu of "add" 02/03 aww: 
        added = aSol.getAmpList().add(amp);
        // "set" state code below redundant with list listeners:
        if (added) aSol.setNeedsCommit(true);
        if (aMag != null && aMag.isAmpMag()) {
          added = aMag.getAmpList().add(amp);
          if (added) aMag.setStale(true);
        }
        //JasiCommitableIF jcOut=(JasiCommitableIF)aSol.getListFor(inputList).addOrReplace(jcIn);
        //JasiCommitableIF jcOut=(JasiCommitableIF)aSol.getListFor(inputList).addOrReplaceWithSameTime(jcIn);
        // Without list listerners the above replacement methods 
        // return jcOut == jcIn if jcIn already in list, not replaced 
        // way to test for add would be to find its index then do
        // the list "set" here.
      }
      setListUpdateNeeded(false);
      return aList.size();
    }
   */

    protected boolean insertRow() { 
        Amplitude newAmp = Amplitude.create();
        getModel().initRowValues(newAmp);
        return insertRow(newAmp);
    }
    protected boolean canParseInput() {
      boolean status = super.canParseInput();
      if (!status) return status;
      // changed logic to allow Sol input and generic amp mag - aww 02/08/2005
      return (aMag != null && aMag.isAmpMag()) || aSol != null;
    }
    protected void enableInsertButtons() {
      // changed logic to allow Sol input and generic amp mag - aww 02/08/2005
      if ( (aMag != null && aMag.isAmpMag()) || aSol != null ) {;
        super.enableInsertButtons();
      }
      else {
        super.disableInsertButtons();
      }
    }
}
