package org.trinet.util.graphics;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import org.trinet.util.*;
public class NumberRangeChooser extends JPanel {

    NumberChooser c1;
    NumberChooser c2;

    public NumberRangeChooser() { }
    /** Create a NumberRangeChooser with the specified labels for start and end.
     * It is just two NumberRangeChoosers.
     * @see: NumberRangeChooser
     */
    public NumberRangeChooser(String label,
                               double min1, double max1, double interval1, double sel1,
                               double min2, double max2, double interval2, double sel2) {

        c1 = new NumberChooser(min1, max1, interval1, sel1);
        c2 = new NumberChooser(min2, max2, interval2, sel2);

        setLayout(new FlowLayout());
        add(new Label(label));
        add(c1);
        add(c2);

    }

    /** Return a NumberRange representing the values selected by the chooser.
    * @see NumberRange()
    */
    public NumberRange getNumberRange () {
        return new NumberRange( Double.valueOf(getMinValue()), Double.valueOf(getMaxValue()) );
    }

    /** Return the minimum value selected by the chooser. */
    public double getMinValue () {
        return c1.getSelectedValue();
    }
    /** Return the maximum value selected by the chooser. */
    public double getMaxValue () {
        return c2.getSelectedValue();
    }

/*
    public static void main(String[] args) {
        JFrame frame = new JFrame("NumberRangeChooser");

        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });

        final NumberRangeChooser chooser = new NumberRangeChooser("Mag Range",
                           0.0, 9.0, 0.5, 0.0,
                           0.0, 9.0, 0.5, 4.0);
        frame.getContentPane().add("Center", chooser);

        JButton button = new JButton("Show");
          button.addActionListener(new java.awt.event.ActionListener() {

               public void actionPerformed(ActionEvent e) {
                    System.out.println (chooser.getNumberRange().toString());
               }
          });

        frame.getContentPane().add("South", button);

        frame.pack();
        frame.setVisible(true);

    }
*/
}
