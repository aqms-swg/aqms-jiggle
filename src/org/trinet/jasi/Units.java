package org.trinet.jasi;

import java.util.*;
import org.trinet.util.*;
/**
 * Used to contain enumeration of recognized units types for seismic signals.
 * Also supports logic to determine if one unit type can be converted to another
 * and a static method to do such conversion. <p>
 *
 * Possible unit types:<p>
<pre>
 0  unknown                     // * not in NCDEC schema
 1  counts
 2  s                           // seconds
 3  m
 4  cm
 5  mm
 6  mc
 7  nm
 8  mss
 9  cms
10  mms
11  mss
12  cmss
13  mmss
14  e                          // ergs
15  iovs                       // integral of velocity squared
16  spa                        // spectral peak amplitude
17  counts/(cm/sec2)
18  counts/(cm/sec)
19  COUNTS/M/S
20  COUNTS/M/S**2
21  DU/M/S
22  DU/M/S**2
23  volts
24  mVolts
25  g
26  dycm
27  DU/G
</pre>
* Note that the variants "counts/cm/sec" (used by BK?) and "counts/cm/s" will be accepted as strings but will always
* be interpreted as "counts/(cm/sec)".
 */
// DDG 3/28/03 -- added units conversion logic so amp readings could be converted easily.
// DDG 4/1/03 -- changed all str values to match the NCEDC schema 'units' table.
//                 Not sure why it was done otherwise originally.
// DDG 7/28/04 -- added 'volts' & 'microvolts' - they are not in NCEDC schema
// DDG 11/16/04 -- added hack to accomodate variants on counts/cm/sec: all resolve to "counts/(cm/sec)"
//                 BK's use of "counts/cm/s"

public class Units  {

    static int i = 0;
    public static final int UNKNOWN        = i++;
    public static final int COUNTS         = i++;
    public static final int SECONDS        = i++;

    public static final int M              = i++;
    public static final int CM             = i++;
    public static final int MM             = i++;
    public static final int MICRONS        = i++;
    public static final int NM             = i++;  // nanometers

    public static final int MS             = i++;  // m/sec
    public static final int CMS            = i++;
    public static final int MMS            = i++;

    public static final int MSS            = i++;  // m/sec2
    public static final int CMSS           = i++;
    public static final int MMSS           = i++;

    public static final int ERGS           = i++;
    public static final int IOVS           = i++;  // intergral of vel. squared
    public static final int SPA            = i++;  // spectral peak amp - NOTE: for amps GroMoPacket maps this to velocity 

    public static final int CntCMSS        = i++;  // CI - counts/(cm/sec^2)
    public static final int CntCMS         = i++;  // CI - counts/(cm/sec)

    public static final int CntMS          = i++;  // SIS
    public static final int CntMSS         = i++;  // SIS
    public static final int DuMS           = i++;  // BK - DigitalUnits/Meter/Sec
    public static final int DuMSS          = i++;  // BK - DigitalUnits/Meter/Sec^2

    public static final int Volts          = i++;  //
    public static final int MicroVolts     = i++;  //

    public static final int G              = i++; // BAP gravity units -added aww 2009/06/20
    public static final int DYCM           = i++;
    public static final int DUG            = i++;

// used for conversions
    private static final int CGSunits[] = {CM, CMS, CMSS};

// lists of compatible units that can be converted one to another.
    private static final int comp1[] = {M, CM, MM, MICRONS, NM};
    private static final int comp2[] = {MS, CMS, MMS};
    private static final int comp3[] = {MSS, CMSS, MMSS};
    private static final int comp4[] = {CntCMS, CntMS, DuMS};
    private static final int comp5[] = {CntCMSS, CntMSS, DuMSS};
    //private static final int comp6[] = {DYCM, NM};

/* Map of units descriptions that are legal in the NCEDC schema.
    See NCDN parametric schema doc, column 'units' */
    StringList sl = new StringList();
    private static String str[] = {
      "unknown",        // * not in NCDC schema
      "counts",         // counts
      "s",              // seconds

      "m",
      "cm",
      "mm",
      "mc",              // microns
      "nm",              // nanometers

      "ms",
      "cms",
      "mms",

      "mss",
      "cmss",
      "mmss",

      "e",                // ergs
      "iovs",             // integral of velocity squared
      "spa",              // spectral peak amplitude

      "counts/(cm/sec2)", // * gain_units in NCDC schema (added 6/10/2003)
      "counts/(cm/sec)",  // * gain_units in NCDC schema (added 6/10/2003)

      "COUNTS/M/S",       // SIS
      "COUNTS/M/S**2",    // SIS
      "DU/M/S",           // BK - DigitalUnits/Meter/Sec
      "DU/M/S**2",        // BK - DigitalUnits/Meter/Sec^2

      "volts",
      "mVolts",           // microvolts

      "g",                // BAP gravities acceleration -added aww 2009/06/20
      "dycm",             // dyne-cm moment
      "DU/G",             // Gravity units -added 2015/01/26
     };

    /** Return a string describing the amplitude type. Returns the string "unknown"
        if the type is not legal. */
    public static String getString(int type) {
        if (isLegal(type)) return str[type];
        return str[0];
    }

    /** Return an 'int' matching the enumeration value given a short string
    describing the amplitude type.  Returns 0 (=UNKNOWN) if no match was found. */
    public static int getInt(String type) {

      // Special cases HACK - dbase should enforce constraint!
      if (type.equalsIgnoreCase("counts/cm/s") || type.equalsIgnoreCase("counts/cm/sec")) return Units.CntCMS;
      if (type.equalsIgnoreCase("counts/cm/sec^2") || type.equalsIgnoreCase("COUNTS/CM/S**2")) return Units.CntCMSS;

        for (int i = 0; i < str.length; i++) {
            if (type.equalsIgnoreCase(str[i])) return i;
        }
        return 0;
    }

    /** Dump string and int of all unit types - for troubleshooting. */
    private static void dumpAll() {

      // Special case HACK
        for (int i = 0; i < str.length; i++) {
            System.out.println (i + "  "+ str[i]);
        }
    }

    /** Return true if value is within legal range of enumeration list. Note that a
     * value of 0 (=UNKNOWN) is legal.*/
    public static boolean isLegal(int type) {
        return (type > 0 && type < str.length);
    }

    /** Return true if the string is a legal units string. Is NOT case sensitive.
    * Note that a value of "UNKNOWN" is legal.*/
    public static boolean isLegal(String type) {
     if (type == null) return false;
     for (int i = 0; i < str.length; i++) {
       if (type.equalsIgnoreCase(str[i])) return true;
     }
     // Special case HACK
     if (type.equalsIgnoreCase("counts/cm/s") || type.equalsIgnoreCase("counts/cm/sec") ||
         type.equalsIgnoreCase("counts/cm/sec^2") || type.equalsIgnoreCase("COUNTS/CM/S**2")
     ) return true;

     return false;
    }

    /** Returns true if the given Units is a velocity. */
    public static boolean isVelocity(int units) {
        return (
          (units == Units.CMS) ||
          (units == Units.CntCMS) ||
          (units == Units.MS) ||
          (units == Units.MMS) ||
          (units == Units.DuMS) ||
          (units == Units.CntMS) 
        );
    }
    public static boolean isCounts(int units) {
        return (units == Units.COUNTS);
    } 

    /** Returns true if the given Units is an acceleratioin. */
    public static boolean isAcceleration(int units) {
        return (
          (units == Units.CMSS) ||
          (units == Units.CntCMSS) ||
          (units == Units.MSS) ||
          (units == Units.MMSS) ||
          (units == Units.DuMSS) ||
          (units == Units.CntMSS) ||
          (units == Units.G) || //  BAP added -aww 2009/06/20
          (units == Units.DUG)  // added -aww 2015/01/26
        );
    }
    /** Returns true if the given Units is a displacement. */
    public static boolean isDisplacement(int units) {
        return (
           (units == Units.CM) ||
           (units == Units.MM) ||
           (units == Units.M) ||
           (units == Units.MICRONS) ||
           (units == Units.NM) 
        );

    }

    /** Convert from the 'fromUnits' to the 'toUnits'. Only works compatible units
     types that can be converted by shifting the decimal point.
     Returns NaN if the units are illegal or incompatible.

     convertFromCommon unit tests cover this function */
    public static double convert (double value, int fromUnits, int toUnits) {
      if (!areCompatible(fromUnits, toUnits)) return Double.NaN;
      return convertFromCommon(convertToCGS(value, fromUnits), toUnits);
    }

    /** Convert these units to standard CGS units if there is an equivalent for 'fromUnits'.
     * CGS units are CM, CMS, & CMS2. */
    public static double convertToCGS (double value, int fromUnits) {

      if (getCGSunit(fromUnits) == 0) return Double.NaN;

      if (fromUnits == M || fromUnits == MS || fromUnits == MSS) {
          return value * 100.0;
      } else if ( fromUnits == CM || fromUnits == CMS || fromUnits == CMSS) {
          return value;
      } else if ( fromUnits == MM || fromUnits == MMS || fromUnits == MMSS) {
          return value / 10.0;
      } else if ( fromUnits == MICRONS) { // one millionth (10-6m)
          return value / 10000.0;
      } else if ( fromUnits == NM) {      // one billionth (10-9m)
          return value / 10000000.0;
      }

      return Double.NaN;

    }
    /** Convert these units from the standard units of cm/xxx . */
    public static double convertFromCommon (double value, int toUnits) {

      if (getCGSunit(toUnits) == 0) return Double.NaN;

      if (toUnits == M || toUnits == MS || toUnits == MSS) {
         return value / 100.0;
      } else if ( toUnits == CM || toUnits == CMS || toUnits == CMSS) {
         return value;
      } else if ( toUnits == MM || toUnits == MMS || toUnits == MMSS) {
         return value * 10.0;
      } else if (toUnits == MICRONS) { // one millionth (10-6m)
         return value * 10000.0;
      } else if (toUnits == NM) { // nanometer (10-9m)
         return value * 10000000.0;
      }
      return Double.NaN;
    }
    /** Returns true if the units passed as args can be simply converted one to
    *  the other by shifting the decimal place. For example: CM and MIRCONS are compatible
    * while CMS and MM are not. */
    private static boolean areCompatible (int unit1, int unit2) {
         if (isLegal(unit1) && isLegal(unit2)){
           if (isInList(unit1, comp1) && isInList(unit2, comp1)) return true;
           if (isInList(unit1, comp2) && isInList(unit2, comp2)) return true;
           if (isInList(unit1, comp3) && isInList(unit2, comp3)) return true;
         }
         return false;
    }
    /** Return the CGS equivalent unit type for the given type.
     Possibilities are: CM, CMS, CMS2. Returns UNKNOWN (=0) if there is no
     equivalent. */
    public static int getCGSunit (int unit) {
      if (isInList(unit, comp1)) return CGSunits[0];
      if (isInList(unit, comp2)) return CGSunits[1];
      if (isInList(unit, comp3)) return CGSunits[2];
      return 0;
    }

    public static double getCountsCGS(double value, int unit) {
           if (unit == comp4[0] || unit == comp5[0]) return value;
           if (unit == comp4[1] || unit == comp5[1]) return value/100.;
           if (unit == comp4[2] || unit == comp5[2]) return value/100.;
           return Double.NaN;
    }
    /** Return true if value is in array list. */
    private static boolean isInList (int value, int[] list) {
          for (int i = 0; i<list.length; i++) {
            if (value == list[i]) return true;
          }
          return false;
    }

/*
//  public static final class Tester {
    public static void main(String[] args) {

      dumpAll();

      System.out.println ("?? 23 legal? "+Units.isLegal(23));
      System.out.println (" Units.CntCMS = "+ Units.getString(Units.CntCMS));
      System.out.println (" counts/cm/s = "+ Units.getInt("counts/cm/s"));
      System.out.println (" counts/cm/sec = "+ Units.getInt("counts/cm/sec"));
      System.out.println (" counts/(cm/sec) = "+ Units.getInt("counts/(cm/sec)"));
      System.out.println (" counts/(cm/s) = "+ Units.getInt("counts/(cm/s)"));

      // double translation
      System.out.println (" counts/cm/s = "+ Units.getString(Units.getInt("counts/cm/s")));
      System.out.println (" counts/cm/sec = "+ Units.getString(Units.getInt("counts/cm/sec")));
      System.out.println (" counts/(cm/sec) = "+ Units.getString(Units.getInt("counts/(cm/sec)")));
      System.out.println (" counts/(cm/s) = "+ Units.getString(Units.getInt("counts/(cm/s)")));

      double val = 1234.5678;
      System.out.println ("CM, NM  "+convert(val, CM, NM));
      System.out.println ("CM,  M  "+convert(val, CM, M));
      System.out.println ("CM, MICRONS  "+convert(val, CM, MICRONS));
      System.out.println ("CMS, MS  "+convert(val, CMS, MS));
      System.out.println ("MMSS, MS  "+convert(val, MMSS, MS));
      System.out.println ("CMSS, CM  "+convert(val, CMSS, CM));
    }
//  }
*/
}
