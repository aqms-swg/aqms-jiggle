package org.trinet.util.velocitymodel;
public class SoCalVelocityModel extends UniformFlatLayerVelocityModel {
    public static final double [] DEPTHS     = {0., 4., 10., 15., 25.} ;
    public static final double [] VELOCITIES = {4., 6., 6.2, 6.4, 7.9} ;
    public static final double DEFAULT_PS_RATIO = 1.78;
    public SoCalVelocityModel()  {
        super("SoCal", DEPTHS, VELOCITIES, DEFAULT_PS_RATIO);
    }
}
