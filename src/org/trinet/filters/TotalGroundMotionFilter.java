package org.trinet.filters;

import java.util.*;
import org.trinet.jasi.*;
import org.trinet.util.*;

/**
 * Recursive integration filter for all ground motion types in one pass.
 * Only intended for use on 20, 50, 80, and 100 sps data.<p>
 *
 * Implimentation of the technique
 * developed by Hiroo Kanamori described in "Continuous Monitoring of Ground-Motion
 * Parameters", BSSA, V89, pp. 311-316, Feb. 1999 as implemented in Hiroo Kanimori's
 * FORTRAN subroutine recres6().<p>
 *
 * The displacement record is Butterworth filtered to remove long period signal.
 * This is necessary to get the same result as we see from the RT implementation of
 * this algorithm, AmpGen. <p>
 *
 * This was done to test if this was faster then calling individual ground motion
 * filters one after another. It turns out is IS NOT.
 *
 * @see: AccelFilter()
 * @see: VelocityFilter()
 * @see: DisplacementFilter()
 * @see: RSAFilter()
 */

// DDG - commented out all Wood-Anderson stuff - to restore look for "//x//"
public class TotalGroundMotionFilter extends AbstractWaveformFilter {

    // constants
    double  g_wa,  g_03,  g_10, g_30;
    double c1_wa, c1_03, c1_10, c1_30;
    double c2_wa, c2_03, c2_10, c2_30;
    double omega_03, omega_10, omega_30;
    double gf;

    // Hiroo's magic numbers from equation (1') for "recusive differentiator" from velocity to acceleration
    final double c0 = -0.7721187782;
    final double c1 = -0.3788779578;
    final double c2 =  1.1509967359;

    final double dd0 =  0.102101057;
    final double dd1 = -0.8593897;

    // timeseries arrays
    //x// float wa[];
    float d[];
    float v[];
    float a[];
    float sp03[];
    float sp10[];
    float sp30[];

    // waveform wrappers
    //x// Waveform wfWa = null;
    Waveform wfa = null;
    Waveform wfv = null;
    Waveform wfd = null;
    Waveform wfsp03 = null;
    Waveform wfsp10 = null;
    Waveform wfsp30 = null;

    //ArrayList timeSeriesList = new ArrayList(7);
    ArrayList wfList = new ArrayList(7);

    // Must filter Displacement to remove weird long-period signal
    GeneralButterworthFilter butt = new GeneralButterworthFilter();

    public TotalGroundMotionFilter() { }

    /** Return true if the units are legal for this filter. */
    public boolean unitsAreLegal(int units) {
        return (units == Units.COUNTS );      // raw counts only
    }

    /** Returns true if the given sample rate is legal for this filter.
     * Override this if your filter is valid for other than 20, 40, 50, 80, 100, or 200 sps.*/
    protected boolean sampleRateIsLegal(double rate) {
        return (Math.round(rate)  ==  100l || Math.round(rate)  == 40l || Math.round(rate)  ==  80l ||
                Math.round(rate)  ==  200l || Math.round(rate)  == 250l || Math.round(rate)  == 500l ||
                Math.round(rate)  ==  20l || Math.round(rate)  == 50l 
               );
    }

    // Must define because this is abstract in parent, but the individual resulting
    // waveform's unit types are different and are set during the filtering process
    protected void setUnits(Waveform wf) {
        // intentional noop
    }

    /** Returns just the input alias. */
    public Waveform filter(Waveform wf) {

        // does some setup
        if (setWaveform(wf) == null) return null;  // failed tests

        // Create the new waveform wrappers that contain results from processing below
        makeNewWaveforms();

        // Filter each segment, otherwise time gaps would reak havoc.
        WFSegment seg[] = wfIn.getArray();
        if (seg != null) {
            for (int i=0; i < seg.length; i++) {
               filterSegment(seg[i]);
               //x// wfWa.getSegmentList().add(makeNewSegment(wa));
               wfa.getSegmentList().add( makeNewSegment(a) );
               wfv.getSegmentList().add( makeNewSegment(v) );
               wfd.getSegmentList().add( makeNewSegment(d) );
               wfsp03.getSegmentList().add( makeNewSegment(sp03) );
               wfsp10.getSegmentList().add( makeNewSegment(sp10) );
               wfsp30.getSegmentList().add( makeNewSegment(sp30) );
            }
        }

        // Recalculate bias, max, min, etc. for thw Waveform level (rather then segs)
        for (int i=0; i<wfList.size(); i++) {
            Waveform wfx = (Waveform) wfList.get(i);
            wfx.scanForBias();   // scan segs for bias
            wfx.scanForAmps();   // scan segs for max/min
        }

        return wf; // wf.copy(); // copy of original
    }

    // Make a new segment with this time series and the attributes of the original waveform. Scan it for bias, max/min sample. //
    private WFSegment makeNewSegment(float[] timeSeries) {
        WFSegment newSeg = new WFSegment();
        newSeg.copyHeader(seg);  // internal seg was set in call to filterSegment
        newSeg.setTimeSeries(timeSeries); // scans for bias, max, min
        //newSeg.scanTimeSeries();   // bias, max, min
        return newSeg;
    }

    public WFSegment filterSegment(WFSegment seg) {

        float ts[] = seg.getTimeSeries();

        if (ts.length < 3) return seg;    // not enough to filter

        //x// wa   = new float[ts.length];
        d    = new float[ts.length];
        v    = new float[ts.length];
        a    = new float[ts.length];
        sp03 = new float[ts.length];
        sp10 = new float[ts.length];
        sp30 = new float[ts.length];

        setWFSegment(seg);
        // Remove bias: note 'scanIt' is 'false' because we don't yet want to recalc bias, max/min, etc.
        seg.setTimeSeries( demean(ts), false );

        // set all the constants
        setConstants();

        double dt = seg.getSampleInterval();
        double dtgf = dt*gf;
        //double b0gwa = g_wa * (2.0 / (1 + qHighPass));

        double b0 = 2.0 / (1.0 + qHighPass); // reciprocol of (1 + q) / 2
        double devisor = b0 * gf;  // moosh these together of efficiency
        double dt2b0 = dt/(2.0*b0);

        //  <VELOCITY>
        if ( seg.getChannelObj().isVelocity() ) {

          // optimization - maybe the compiler would handle this...
          // double multwa = (g_wa/gf) * dt;
          double mult03 = (g_03/gf) * dt;
          double mult10 = (g_10/gf) * dt;
          double mult30 = (g_30/gf) * dt;
          float delta;

          for (int i = 2; i < ts.length -3; i++) {

            delta = ts[i]-ts[i-1];  //diff used by all filters

            a[i] =(float) (  (c2*ts[i] + c1*ts[i-1] + c0*ts[i-2]) /
                              dtgf + (dd1*a[i-1])+(dd0*a[i-2])   );

            //x// wa[i]   = (float) (( delta * multwa + (c1_wa*2.0 *   wa[i-1] -   wa[i-2] )) / c2_wa);
            sp03[i] = (float) (( delta * mult03 + (c1_03*2.0 * sp03[i-1] - sp03[i-2] )) / c2_03);
            sp10[i] = (float) (( delta * mult10 + (c1_10*2.0 * sp10[i-1] - sp10[i-2] )) / c2_10);
            sp30[i] = (float) (( delta * mult30 + (c1_30*2.0 * sp30[i-1] - sp30[i-2] )) / c2_30);
            v[i]    = (float) ( delta/devisor + qHighPass * v[i-1] );
            d[i]    = (float) ((v[i]+v[i-1])*dt2b0 + qHighPass*d[i-1]);

          }

        // <ACCELERATION>
        } else if ( seg.getChannelObj().isAcceleration() ) {

          // optimization - maybe the compiler would handle this...
          // double multwa = g_wa * dt * dt;
          double mult03 = g_03 * dt * dt;
          double mult10 = g_10 * dt * dt;
          double mult30 = g_30 * dt * dt;

          for (int i = 2; i < ts.length -3; i++) {

            a[i]    = (float) ( (ts[i]-ts[i-1]) / (b0*gf) + qHighPass * a[i-1] );
            v[i]    = (float) ( (a[i]+a[i-1]) * dt2b0 + qHighPass * v[i-1] );

            //x// wa[i]   = (float) (( a[i] * multwa + (c1_wa*2.0 *   wa[i-1] -   wa[i-2] )) / c2_wa);
            sp03[i] = (float) (( a[i] * mult03 + (c1_03*2.0 * sp03[i-1] - sp03[i-2] )) / c2_03);
            sp10[i] = (float) (( a[i] * mult10 + (c1_10*2.0 * sp10[i-1] - sp10[i-2] )) / c2_10);
            sp30[i] = (float) (( a[i] * mult30 + (c1_30*2.0 * sp30[i-1] - sp30[i-2] )) / c2_30);

            d[i]    = (float) ((v[i]+v[i-1])*dt2b0 + qHighPass*d[i-1]);

          }

          // filter the bastard again to remove weird long-period signal
          butt.setSampleRate(1./seg.getSampleInterval()); // added 2012/06/26 -aww
          d = butt.filter(d);

        } else {
          System.err.println("TotalGroundMotionFilter illegal input waveform, check gain units for: "+ seg.getChannelObj().toDelimitedSeedNameString());
        }

        // Convert peak spectral displacement to peak spectral accelerations
        // multiplier scalar is omega**2 for spectral period set in setConstants() method
        for (int i = 0; i < ts.length -1; i++) {
          sp03[i] *= omega_03;
          sp10[i] *= omega_10;
          sp30[i] *= omega_30;
        }
        return null;  // ??
    }

    /**  Create New waveform objects for the filtered versions.  */
    private void makeNewWaveforms() {
        clear();
        // filtername = amptype: used to create Amplitudes later
        //x// wfWa   = makeNewWaveform(AmpType.WA);
        wfa    = makeNewWaveform(AmpType.PGA);
        wfv    = makeNewWaveform(AmpType.PGV);
        wfd    = makeNewWaveform(AmpType.PGD);
        wfsp03 = makeNewWaveform(AmpType.SP03);
        wfsp10 = makeNewWaveform(AmpType.SP10);
        wfsp30 = makeNewWaveform(AmpType.SP30);
    }

    private void clear() {

        wfList.clear();

        if (wfa != null) wfa.unloadTimeSeries();
        if (wfv != null) wfv.unloadTimeSeries();
        if (wfd != null) wfd.unloadTimeSeries();
        if (wfsp03 != null) wfsp03.unloadTimeSeries();
        if (wfsp10 != null) wfsp10.unloadTimeSeries();
        if (wfsp30 != null) wfsp30.unloadTimeSeries();


        d    = null;
        v    = null;
        a    = null;
        sp03 = null;
        sp10 = null;
        sp30 = null;
    }

    /** Create a single waveform of this type. Copy header info from input (original raw) waveform. */
    private Waveform makeNewWaveform(int ampType) {

        Waveform wf = AbstractWaveform.createDefaultWaveform();
        // copy all the header attributes of original
        // This includes wfid, filename, encoding, etc. which will not really be valid
        // for the copy.
        wf.copyHeader(wfIn);

        // change a few things
        wf.setAmpUnits(AmpType.getDefaultUnits(ampType));

        // filtername = amptype: used to create Amplitudes later
        wf.setFilterName(AmpType.getString(ampType));

        // add it to the array list
        wfList.add(wf);

        return wf;
    }

    /** Return an amplist containing the peaks of all the ground motion types resulting from the filter operation. <p>
     * The resulting Amp has no Auth or Source. */
    public AmpList getPeaks() {
        AmpList amplist = AmpList.create();
        Amplitude amp = null;
        for (int i=0; i<wfList.size();i++) {
            Waveform wfx = (Waveform) wfList.get(i);
            amp = wfx.getPeakAmplitude();      // this returns amptype = 'UNKNOWN'.
            amp.setType(wfx.getFilterName());  // filtername = amptype
            amplist.add(amp, false);
        }
        return amplist;
    }

    /**
     * Set constant values that depend on sample rate, input signal type (vel or accel)
     * and output signal type (e.g. SP03)
    */
    private boolean setConstants() {

        gf   = getGainFactor();  // true for all

        // WA
        WAFilter wafilter = new WAFilter((int)getSampleRate());
        wafilter.setConstants(seg);
        c1_wa = wafilter.c1;
        c2_wa = wafilter.c2;
        g_wa  =  wafilter.g0;   // WA gain factor, used to be gwa

        // SPxx
        RSAFilter rsafilter = new RSAFilter(FilterTypes.SP03, 0);
        rsafilter.setConstants(seg);
        c1_03 = rsafilter.c1;
        c2_03 = rsafilter.c2;
        g_03  = rsafilter.g0; // used to be gwa
        omega_03 = rsafilter.getOmega(AmpType.getPeriod(AmpType.SP03));
        omega_03 *= omega_03; // square it to go from spectral displacement to acceleration -aww 20100113

        rsafilter = new RSAFilter(FilterTypes.SP10, 0);
        rsafilter.setConstants(seg);
        c1_10 = rsafilter.c1;
        c2_10 = rsafilter.c2;
        g_10  = rsafilter.g0; // used to be gwa
        omega_10 = rsafilter.getOmega(AmpType.getPeriod(AmpType.SP10));
        omega_10 *= omega_10; // square it to go from spectral displacement to acceleration -aww 20100113

        rsafilter = new RSAFilter(FilterTypes.SP30, 0);
        rsafilter.setConstants(seg);
        c1_30 = rsafilter.c1;
        c2_30 = rsafilter.c2;
        g_30  = rsafilter.g0; // used to be gwa
        omega_30 = rsafilter.getOmega(AmpType.getPeriod(AmpType.SP30));
        omega_30 *= omega_30; // square it to go from spectral displacement to acceleration -aww 20100113

        return true;
    }

    /**
    * Return the Waveform of the given type. Returns null if invalid type.
    * Should only be called after filter().
    */
    public Waveform getFilteredWaveform(int ampType) {
        //x// if (ampType == AmpType.WA) return wfWa;
        if (ampType == AmpType.PGA) return wfa;
        if (ampType == AmpType.PGV) return wfv;
        if (ampType == AmpType.PGD) return wfd;
        if (ampType == AmpType.SP03) return wfsp03;
        if (ampType == AmpType.SP10) return wfsp10;
        if (ampType == AmpType.SP30) return wfsp30;
        return null;
    }
}
