package org.trinet.util.graphics.table;
import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import org.trinet.jasi.*;

/** Renderer to color table cells of deleted Solutions
*/
public class CatalogDeletedRowCellRenderer extends DefaultTableCellRenderer {
    Color defaultBackgroundColor = Color.white;
    Color deletedRowColor = Color.pink;
    AbstractTableModel tableModel;

    public CatalogDeletedRowCellRenderer (AbstractTableModel tableModel) {
        super();
        this.tableModel = tableModel;
    }

    public CatalogDeletedRowCellRenderer (AbstractTableModel tableModel, Color defaultBackgroundColor, Color deletedRowColor) {
        super();
        this.tableModel = tableModel;
        setDefaultBackground(defaultBackgroundColor);
        setDeletedRowColor(deletedRowColor);
    }

    public void setDefaultBackground(Color defaultBackgroundColor) {
        this.defaultBackgroundColor = defaultBackgroundColor;
    }

    public void setDeletedRowColor(Color deletedRowColor) {
        this.deletedRowColor = deletedRowColor;
    }

    public Component getTableCellRendererComponent(JTable jtable, Object value, boolean isSelected, boolean hasFocus,
                 int row, int col) {
        Component comp =  super.getTableCellRendererComponent(jtable, value, isSelected, hasFocus, row, col);

        if (!isSelected) {
                Solution sol = (Solution) ((CatalogTableModel) tableModel).getList().get(row);
                if ( sol.isDeleted() || ! sol.isValid() ) {
                        comp.setBackground(deletedRowColor);
                }
                else    comp.setBackground(defaultBackgroundColor);
        }
        return comp;
    }
}
