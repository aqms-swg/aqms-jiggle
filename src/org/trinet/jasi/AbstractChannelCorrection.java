package org.trinet.jasi;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

/**
* Abstract type for a generic correction for a station channel.
*/
// Should it extend CommitableChannelData, AbstractChannelData, or SourceAssocJasiObject ?
public abstract class AbstractChannelCorrection extends CommitableChannelData
             implements ChannelCorrectionDataIF {

    protected boolean verbose = true;        // for debugging print i/o

    protected static final String UNKNOWN_STRING_TYPE = "?";
    protected DataDouble corr       = new DataDouble();
    protected DataString corrType   = new DataString(UNKNOWN_STRING_TYPE);
    protected DataString corrFlag   = new DataString(UNKNOWN_STRING_TYPE);
    // below member not needed here if inherited from SourceAssocJasiObject
    // protected DataString authority  = new DataString(UNKNOWN_STRING_TYPE);

    /** A default value to use for this channel type if no value is retrieved from a datasource. */ 
    protected DataDouble defaultCorrection  = new DataDouble(0.);


    protected AbstractChannelCorrection() {
        this(null, null);
    }

    protected AbstractChannelCorrection(ChannelIdIF id) {
        this(id, null);
    }
    protected AbstractChannelCorrection(ChannelIdIF id, DateRange dateRange) {
        super(id, dateRange);
    }
    protected AbstractChannelCorrection(ChannelIdIF id, DateRange dateRange, double value, String corrType) {
        this(id, dateRange, value, corrType, null, null);
    }
    protected AbstractChannelCorrection(ChannelIdIF id, DateRange dateRange, double value,
                    String corrType, String corrFlag) {
        this (id, dateRange, value, corrType, corrFlag, null);
    }
    protected AbstractChannelCorrection(ChannelIdIF id, DateRange dateRange, double value,
                    String corrType, String corrFlag, String authority) {
        super(id, dateRange);
        this.corr.setValue(corr);
        if (corrType != null) this.corrType.setValue(corrType);
        if (corrFlag != null) this.corrFlag.setValue(corrFlag);
        if (authority != null) this.authority.setValue(authority);
    }
    public void setCorr(DataNumber corr) {
        this.corr.setValue(corr);
    }
    public void setCorrValue(double value) {
        corr.setValue(value);
    }
    public void setCorrType(String type) {
        corrType.setValue(type);
    }
    public void setCorrStatus(String status) {
        corrFlag.setValue(status);
    }

    public DataNumber getCorr() { return corr; }
    public String getCorrType() { return corrType.toString(); }
    public String getCorrStatus() { return corrFlag.toString(); }
    //below is implemented in super class no override needed here
    //public String getAuthority() { return authority.toString(); }

    public DataNumber getDefaultCorrection() {
        return defaultCorrection;
    }

    protected void set(double value, String corrType, String corrFlag) {
        set(value, corrType, corrFlag, null) ;
    }
    protected void set(double value, String corrType, String corrFlag, String authority) {
      corr.setValue(value);
      if (! NullValueDb.isEmpty(corrType)) this.corrType.setValue(corrType);
      else this.corrType.setValue(UNKNOWN_STRING_TYPE);
      if (! NullValueDb.isEmpty(corrFlag)) this.corrFlag.setValue(corrFlag);
      else this.corrFlag.setValue(UNKNOWN_STRING_TYPE);
      if (! NullValueDb.isEmpty(authority)) this.authority.setValue(authority);
      else this.corrFlag.setValue(UNKNOWN_STRING_TYPE);
    }


/**
* Assigns the data member values of the input instance to this instance
* Returns false if input is null or input is not an  instance of AbstractChannelCorrection.
*/
    public boolean copy(ChannelDataIF correction) {
        return (getClass().isInstance(correction)) ? copy((AbstractChannelCorrection) correction) : false;
    }

    public boolean copy(AbstractChannelCorrection correction) {
        if (correction == null)  return false;
        if (correction == this) return true;
        this.channelId = (ChannelIdIF) correction.channelId.clone();
        this.dateRange.setLimits(correction.dateRange);
        this.corr.setValue(correction.corr);
        this.corrType.setValue(correction.corrType);
        this.corrFlag.setValue(correction.corrFlag);
        // members inherited use setValue or clone():
        this.authority.setValue(correction.authority);
        this.source.setValue(correction.source);
        this.processingState.setValue(correction.processingState);
        this.defaultCorrection.setValue(correction.defaultCorrection);
        return true;
    }

// override super methods:
    public String toString() {
        StringBuffer sb = new StringBuffer(132);
        sb.append(super.toString());
        sb.append(" ").append(corr.toString());
        sb.append(" ").append(corrType.toString());
        sb.append(" ").append(corrFlag.toString());
        //sb.append(" ").append(authority.toString());
        sb.append(" ").append(defaultCorrection.toString());
        return sb.toString();
    }

    public boolean equals(Object object) {
        if (object == null || ! super.equals(object)) return false;
        AbstractChannelCorrection correction = (AbstractChannelCorrection) object;
        return (
                 corr.equals(correction.corr) &&
                 corrType.equals(correction.corrType) &&
                 corrFlag.equals(correction.corrFlag)
                 && defaultCorrection.equals(correction.defaultCorrection)  // aww  03/04
                 // no check of authority, source, processingState
               );
    }

    public Object clone() {
        AbstractChannelCorrection correction = (AbstractChannelCorrection) super.clone();
        correction.corr = (DataDouble) this.corr.clone();
        correction.corrType = (DataString) this.corrType.clone();
        correction.corrFlag = (DataString) this.corrFlag.clone();
        correction.defaultCorrection = (DataDouble) this.defaultCorrection.clone();
        //correction.authority = (DataString) this.authority.clone();
        return correction;
    }

    /*
    public int compareTo(Object obj) {
        if (obj == null ||  obj.getClass() != getClass() )
                 throw new ClassCastException("compareTo(object) argument wrong class type, required: "
                                this.getClass().getName() + " input: " + obj.getClass().getName());
        return corr.compareTo( ((ChannelCorrectionDataIF) obj).getCorr());
    }
    */

    public String getNeatHeader() {
      return "Channel     DateRange  Auth  Src  Pflg  Type  Flag  Value    Default";
    }
    public String toNeatString() {
      Format df1 = new Format("%10.2f");
      StringBuffer sb = new StringBuffer(80);
      sb.append(super.toString()).append("  ");
      sb.append(corrType).append("  ");
      sb.append(corrFlag).append("  ");
      //sb.append(authority).append("  "); // super has source, processingState?
      sb.append(df1.form(corr.doubleValue())).append(" ");
      sb.append(df1.form(defaultCorrection.doubleValue())).append(" ");
      return sb.toString();
    }

    public Object getTypeQualifier() { return corrType.toString(); }
}
