# Running the tests locally
## Windows set up
1. Ensure that you have docker desktop installed 
   1. Instructions can be found: https://www.docker.com/products/docker-desktop/
      1. Will need to add the linux subsystem to windows in order to get Docker running
         * https://docs.docker.com/desktop/features/wsl/
2. Have docker desktop running in the background when running the tests
3. The images for the testdb image can be found: https://gitlab.com/aqms-swg/aqms-jiggle/container_registry/
   1. It is the parchdbtest-docker one
4. IDE 
   1. Intellij
      1. Ensure all the required jars under lib are added to the modules section in the project set up
   2. Eclipse
      1. Coming soon!

## Mac Set up
Coming Soon!

# Documentation for test progress
https://gitlab.com/aqms-swg/aqms-jiggle/-/wikis/Jiggle-Unit-Testing