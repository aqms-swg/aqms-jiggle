package org.trinet.apps;

import org.trinet.jasi.*;
import org.trinet.jdbc.datasources.AbstractSQLDataSource;
import org.trinet.util.gazetteer.LatLonZ;
import org.trinet.util.Format;

/**
 * Dump a list of channels sorted by distance from a given lat/lon.<p>
 * <pre>
 * Syntax: stadist number-of-stas lat min lon min<p>
 * OR<p>
 * Syntax: stadist number-of-stas lat lon <p>
 * <\pre><p>
 */
public class StaDist {

    static double lat, latm, lon, lonm;
    static int chanKnt;
    private static final StringBuilder result = new StringBuilder();

    public StaDist() {

    }

    /**
     * Main
     */
    public static void main(String args[]) {

        String syntax = "stadist <number-of-stas> <lat> <min> <lon> <min> [cacheFileName] (optional) [host] [dbasename] [username] [password] [port]  \n" +
                "Any value of <number-of-stas> <= 0 means 'all'.";

        double dlat = 0.0;
        double dlon = 0.0;
        String channelCacheFile = "";

        if (args.length < 3) {
            System.out.println(syntax);
            System.exit(0);
        } else if (args.length >= 5) {

            chanKnt = Integer.valueOf(args[0]).intValue();
            lat = Double.valueOf(args[1]).doubleValue();
            latm = Double.valueOf(args[2]).doubleValue();
            lon = Double.valueOf(args[3]).doubleValue();
            lonm = Double.valueOf(args[4]).doubleValue();
            dlat = lat + latm / 60.0;
            dlon = -(Math.abs(lon) + lonm / 60.0);
            if ( args.length > 6 ) {
                channelCacheFile = args[5];
            }
        } else if (args.length == 3) {
            chanKnt = Integer.valueOf(args[0]).intValue();
            dlat = Double.valueOf(args[1]).doubleValue();
            dlon = Double.valueOf(args[2]).doubleValue();
        }

        System.out.println("Making connection... ");
        ChannelList list;
        if (args.length > 6) { // Testing
            TestDataSource.create(args[6], args[7],
                    args[8], args[9], AbstractSQLDataSource.POSTGRESQL_DS_SUBPROTOCOL, args[10], AbstractSQLDataSource.POSTGRESQL_DS_DRIVER);      // make connection
            list = ChannelList.smartLoad(null, null, channelCacheFile);
        } else if (args.length == 6) {
            TestDataSource.create();      // make connection
            list = ChannelList.smartLoad(null, null, channelCacheFile);
        } else {
            TestDataSource.create();      // make connection
            list = ChannelList.smartLoad();
        }


        LatLonZ latlonz = new LatLonZ(dlat, dlon, 0.0);
        list.distanceSort(latlonz);
        if (chanKnt <= 0) chanKnt = Integer.MAX_VALUE;

        dumpResult(list);

    } // end of main


    public static void dumpResult(ChannelList list) {

        final Format fmt = new Format("%8.1f");
        int knt = 0;

        Channel[] ch = list.getArray();
        Channel lastChan = null;

        if (ch.length == 0) {
            System.out.println("No channels found.");
        } else {
//       list.dump();
            for (int i = 0; i < ch.length; i++) {
                if (!ch[i].equalsNameIgnoreCase(lastChan))    // filter out dups?
                {
                    result.append(ch[i].getChannelName().toNeatString()).append("  ").append(fmt.form(ch[i].getHorizontalDistance())).append(  // dont' use slant distance ? - aww  06/11/2004
                            "  ").append(ch[i].getLatLonZ().toString()).append("\n");
                    lastChan = ch[i];
                    if (knt++ >= chanKnt) break;
                }
            }
            System.out.print(result.toString());
        }
    }

    public static String getResult() {
        return result.toString();
    }
}