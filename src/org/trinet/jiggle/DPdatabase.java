package org.trinet.jiggle;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import org.trinet.util.graphics.text.JTextClipboardPopupMenu;
import org.trinet.jdbc.datasources.AbstractSQLDataSource;

/**
  * This dialog allows user to choose Jiggle preferences
  */
public class DPdatabase extends JPanel {

    JiggleProperties newProps = null; // new properties as changed by this dialog
  
    JTextField driverField = null;

    JTextField dbaseDomainField = null;
    JTextField dbasePortField = null;

    JTextField dbaseTNSField = null;
    JTextField dbaseHostField = null;
    JTextField dbaseNameField = null;

    JTextField userNameField = null;
    JPasswordField passwordField = null;   // don't echo password text

    //JTextField jasiTypeField = null; // removed - aww 2008/06/16
    JCheckBox loginDialogCheckBox = null; // added -aww 2008/06/16

    public DPdatabase(JiggleProperties props) {
        newProps = props;

        try  {
             initGraphics();
        }
        catch(Exception ex) {
             ex.printStackTrace();
        }
    }

    private static void addComponent(JPanel p, GridBagConstraints c, Component comp) {
        c.anchor = GridBagConstraints.WEST;
        c.fill = GridBagConstraints.HORIZONTAL;
        p.add(comp, c);
    }

    private static void addComponents(JPanel p, GridBagConstraints c, String labelText, Component comp) {
        c.gridy++;
        if (labelText != null) {
            addLabel(p, c, labelText);
        }
        c.gridwidth = GridBagConstraints.REMAINDER;
        addComponent(p, c, comp);
    }
    
    private static void addLabel(JPanel p, GridBagConstraints c, String labelText) {
        c.anchor = GridBagConstraints.EAST;
        c.fill = GridBagConstraints.NONE;
        c.gridwidth = 1;
        c.gridy++;
        p.add(new JLabel(labelText, JLabel.RIGHT), c);
    }
    
    private static JRadioButton addRadioButton(JPanel p, GridBagConstraints c, String text, boolean selected, ActionListener al, ButtonGroup bg) {
        JRadioButton jrb = new JRadioButton(text); 
        jrb.setSelected(selected);
        if (al != null) {
            jrb.addActionListener(al);
        }
        if (bg != null) {
            bg.add(jrb);
        }
        addComponent(p, c, jrb);
        return jrb;
    }

    /**
    * Creates the dbase panel which will contain all the fields for the connection information.
    */
    private void initGraphics() throws Exception {
        JPanel dataPanel = new JPanel(new GridBagLayout());
        dataPanel.setBorder( new TitledBorder("Dbase connection info") );
        // oci or thin ?
        ButtonGroup bg = new ButtonGroup();
        ActionListener al = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                String str = evt.getActionCommand();
                newProps.setProperty("dbaseSubprotocol", str);
                if (str.contains("oracle")) {
                   resetFieldsByProtocol(! str.equals("oracle:oci"));
                }
            }
        };

        boolean isOracle = newProps.getProperty("dbaseSubprotocol", 
                AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL).contains("oracle");
        boolean isThin = newProps.getProperty("dbaseSubprotocol",
                AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL ).equals(AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL);
        GridBagConstraints c = new GridBagConstraints();
        c.gridy = -1;
        addLabel(dataPanel, c, "Subprotocol ");
        addRadioButton(dataPanel, c, AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL, isOracle && isThin, al, bg);
        addRadioButton(dataPanel, c, "oracle:oci", isOracle && !isThin, al, bg);
        addRadioButton(dataPanel, c, "postgresql", !isOracle, al, bg);

        dbaseTNSField = new JTextField(newProps.getProperty("dbaseTNSname",""));
        new JTextClipboardPopupMenu(dbaseTNSField);
        dbaseTNSField.setToolTipText("Use for a TNSNAMES.ora alias or its connection definition (host, domain, dbname, port fields must be blank)"); 
        dbaseTNSField.getDocument().addDocumentListener(new MiscDocListener("dbaseTNSname", dbaseTNSField));
        addComponents(dataPanel, c, "TNS alias ", dbaseTNSField);

        dbaseHostField = new JTextField(newProps.getProperty("dbaseHost",""));
        new JTextClipboardPopupMenu(dbaseHostField);
        dbaseHostField.setToolTipText("Name of computer hosting database"); 
        dbaseHostField.getDocument().addDocumentListener(new MiscDocListener("dbaseHost", dbaseHostField));
        addComponents(dataPanel, c, "Database host ", dbaseHostField);

        dbaseDomainField = new JTextField(newProps.getProperty("dbaseDomain",""));
        new JTextClipboardPopupMenu(dbaseDomainField);
        dbaseDomainField.getDocument().addDocumentListener(new MiscDocListener("dbaseDomain", dbaseDomainField));
        dbaseDomainField.setToolTipText("Host computer ip domain, like \"gps.caltech.edu\""); 
        addComponents(dataPanel, c, "Database domain ", dbaseDomainField);

        dbaseNameField = new JTextField(newProps.getProperty("dbaseName",""));
        new JTextClipboardPopupMenu(dbaseNameField);
        dbaseNameField.getDocument().addDocumentListener(new MiscDocListener("dbaseName", dbaseNameField));
        addComponents(dataPanel, c, "Database name ", dbaseNameField);

        dbasePortField = new JTextField(newProps.getProperty("dbasePort",""));
        new JTextClipboardPopupMenu(dbasePortField);
        dbasePortField.getDocument().addDocumentListener(new MiscDocListener("dbasePort", dbasePortField));
        addComponents(dataPanel, c, "Database Port # ", dbasePortField);

        userNameField = new JTextField(newProps.getProperty("dbaseUser",""));
        new JTextClipboardPopupMenu(userNameField);
        userNameField.getDocument().addDocumentListener(new MiscDocListener("dbaseUser", userNameField));
        addComponents(dataPanel, c, "Username ", userNameField);

        passwordField = new JPasswordField(newProps.getProperty("dbasePasswd",""));  // don't echo password text
        new JTextClipboardPopupMenu(passwordField);
        passwordField.getDocument().addDocumentListener(new MiscPasswordDocListener("dbasePasswd", passwordField));
        addComponents(dataPanel, c, "Password ", passwordField);

        driverField = new JTextField(newProps.getProperty("dbaseDriver", org.trinet.jdbc.datasources.AbstractSQLDataSource.DEFAULT_DS_DRIVER));
        new JTextClipboardPopupMenu(driverField);
        driverField.getDocument().addDocumentListener(new MiscDocListener("dbaseDriver", driverField));
        addComponents(dataPanel, c, "Driver ", driverField);

        if ( isOracle ) {
            resetFieldsByProtocol(isThin);
        }

        loginDialogCheckBox = new JCheckBox("Use login dialog on startup");
        loginDialogCheckBox.setSelected(newProps.getBoolean("useLoginDialog"));
        /*
        loginDialogCheckBox.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                JCheckBox jcb = (JCheckBox) evt.getSource();
                if (jcb.isSelected()) {
                   dbaseHostField.setText("");
                   dbaseNameField.setText("");
                   userNameField.setText("");
                   passwordField.setText("");
                }
            }
        });
        */
        addComponents(dataPanel, c, null, Box.createVerticalStrut(10));
        loginDialogCheckBox.addItemListener(new BooleanPropertyCheckBoxListener("useLoginDialog"));
        loginDialogCheckBox.setToolTipText("Check to use connection dialog (clears username, password, host, dbname property fields for security");
        loginDialogCheckBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        addComponents(dataPanel, c, null, loginDialogCheckBox);

        this.add(dataPanel);
    }

    private void resetFieldsByProtocol(boolean isThin) {

        //if (isThin) dbaseTNSField.setText("");
        //dbaseTNSField.setEnabled(! isThin);
        //dbaseTNSField.setBackground(isThin ? Color.gray : Color.white);

        if (!isThin) dbaseDomainField.setText("");
        dbaseDomainField.setEnabled(isThin);
        dbaseDomainField.setBackground(isThin ? Color.white: Color.gray);

        if (!isThin) dbaseHostField.setText("");
        dbaseHostField.setEnabled(isThin);
        dbaseHostField.setBackground(isThin ? Color.white : Color.gray);

        if (!isThin) dbaseNameField.setText("");
        dbaseNameField.setEnabled(isThin);
        dbaseNameField.setBackground(isThin ? Color.white : Color.gray);

        if (!isThin) dbasePortField.setText("");
        dbasePortField.setEnabled(isThin);
        dbasePortField.setBackground(isThin ? Color.white : Color.gray);
    }

    private class MiscDocListener implements DocumentListener {
        private JTextField jtf = null;
        private String prop = null;

        public MiscDocListener(String prop, JTextField jtf) {
            this.jtf = jtf;
            this.prop = prop;
        }

        public void insertUpdate(DocumentEvent e) {
            setValues(e);
        }
        public void removeUpdate(DocumentEvent e) {
            setValues(e);
        }
        public void changedUpdate(DocumentEvent e) {
            // we won't ever get this with a PlainDocument
        }

        private void setValues(DocumentEvent e) {
            // no sanity checking on numbers !
            newProps.setProperty(prop, jtf.getText().trim());
        }
    }

    private class MiscPasswordDocListener implements DocumentListener {
        private JPasswordField jtf = null;
        private String prop = null;

        public MiscPasswordDocListener(String prop, JPasswordField jtf) {
            this.jtf = jtf;
            this.prop = prop;
        }

        public void insertUpdate(DocumentEvent e) {
            setValues(e);
        }
        public void removeUpdate(DocumentEvent e) {
            setValues(e);
        }
        public void changedUpdate(DocumentEvent e) {
            // we won't ever get this with a PlainDocument
        }

        private void setValues(DocumentEvent e) {
            newProps.setProperty(prop, new String(jtf.getPassword()).trim());
        }
    }

    private class BooleanPropertyCheckBoxListener implements ItemListener {
        private String prop = null;

        public BooleanPropertyCheckBoxListener(String prop) {
            this.prop = prop;
        }

        public void itemStateChanged(ItemEvent e) {
            boolean isSelected = (e.getStateChange() == ItemEvent.SELECTED);
            newProps.setProperty(prop, isSelected);
            if (isSelected) {
                   dbaseHostField.setText("");
                   dbaseNameField.setText("");
                   userNameField.setText("");
                   passwordField.setText("");
            }
        }
    }
}
