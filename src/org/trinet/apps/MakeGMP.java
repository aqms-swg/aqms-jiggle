package org.trinet.apps;

import org.trinet.formats.*;
import org.trinet.jasi.*;
import org.trinet.jiggle.JiggleProperties;
import org.trinet.jiggle.common.JiggleSingleton;

/**
 * Create CISN ground motion packet file for the given event. Writes to stdout
 * so redirect to a file when you call it.<p>
 * <p>
 * Uses the default data source (dbase);
 *
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: USGS</p>
 *
 * @author Doug Given
 * @version 1.0
 */

public class MakeGMP extends GroMoPacket {

    static String megasStr;

    public MakeGMP() {
    }

    public static void main(String[] args) {

        long evid = 13951892;
        String host = null; // default.

        if (args.length < 1) // no args
        {
            System.out.println("Usage: java MakeGMP <evid> [<dbasehost>] [dbasename] [username] [password] [subprotocol] [port] [jigglePropLocation])");

            //System.out.println ("Using evid "+ evid+" as a test...");
            //      System.exit(0);
        } else {

            Integer val = Integer.valueOf(args[0]); // convert arg String
            evid = (int) val.intValue();

            if (args.length > 1) {  // optional host name
                host = args[1];
            }
        }
        // Connect to dbase
        if (host == null) {
            TestDataSource.create();
        } else {
            if (args.length > 2) {
                JiggleSingleton.getInstance().setJiggleProp(new JiggleProperties(args[7]));
                TestDataSource.create(host, args[2], args[3], args[4], args[5], args[6]);
            } else {
                JiggleSingleton.getInstance().setJiggleProp(new JiggleProperties());
                TestDataSource.create(host);
            }
        }

        // don't send NC, BK, NP or CE data! Probably a better way to do this.
        String netList[] = {"CI", "AZ", "DW"};
        // Get amps for this event (those associated via AssocAmO)
        Amplitude amp = Amplitude.create();
        AmpList amplist = AmpList.create();
        amplist.fastAddAll(amp.getBySolutionNets(evid, netList));

        // format
        megasStr = GroMoPacket.format(evid, amplist);
        // megasStr = GroMoPacket.formatXY(evid, amplist);

        // print
        System.out.println(megasStr);
    }

    public static String getMegasStr() {
      return megasStr;
    }
}
