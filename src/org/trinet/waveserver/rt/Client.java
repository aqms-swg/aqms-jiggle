package org.trinet.waveserver.rt;
/** Implementation of the Client class of the TRINET WaveClient API.
* Principal role is as convenience container the wave server host address and port number.
* which are used to generate a key in a hashtable associating TCPConnClient connection objects
* in the WaveClient class.
* @see TCPConn
* @see TCPConnClient
* @see WaveClient
*/
public class Client implements Cloneable, Comparable, java.io.Serializable{

/** String IP address of the remote server host */
    String host;

/** Connection port number of the remote server host */
    int port;

/** Constructor, sets host and port data  members to specified input values.
* @exception java.lang.NullPointerException input host String null or empty
* @exception java.lang.IllegalArgumentException input port  < 0
*/
    public Client(String host, int port) {
       if (host == null || host.length() == 0)
           throw new NullPointerException("Client constructor null or empty host string address");
       if (port < 0) throw new IllegalArgumentException("Client constructor negative port number.");
       this.host = host;
       this.port = port;
    }

    public int hashCode() {
        return host.toLowerCase().hashCode() + port;
    }

/** A deep copy. */
    public Object clone() {
        Client client = null;
        try {
            client = (Client) super.clone();
        }
        catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
        }
        return client;
    }

/** Returns true only if the input object is an instance of this class and its host
* host (case-insensitive) and port number member values are equivalent to this object's.
*/
    public boolean equals(Object object) {
        if (this == object) return true;
        if ( object == null || getClass() != object.getClass() ) return false;
        Client client = (Client) object;
        return ( host.equalsIgnoreCase(client.host) && port == client.port) ? true: false;
    }

/** Case insensitive comparison of this object's host String and port number values to the input object's values.
* For relative comparisons the host strings are compared first, then the port numbers.
* @return <pre>
* -1 this object's member values are less than the input object's values.
*  0 this object's member values are equivalent to the input object's  values.
*  1 this object's member values are greater than the input object's values.
* </pre>
* @exception java.lang.ClassCastException input is not an object instance of this class.
*/
    public int compareTo(Object object) {
        Client client = (Client) object;
        int retVal = host.compareToIgnoreCase(client.host);
        if (retVal != 0) return retVal;
        if (port == client.port) return 0;
        return  (port < client.port) ? -1  : 1;
    }

/** Returns value of host and port data members separated by a colon. */
    public String toString() {
        StringBuffer sb = new StringBuffer(80);
        sb.append(host);
        sb.append(":");
        sb.append(String.valueOf(port));
        return sb.toString();
    }

/** Convenience wrapper of System.out.println(toString()).*/
    public void print() {
        System.out.println(toString());
    }
}
