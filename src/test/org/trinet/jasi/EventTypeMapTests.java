package test.org.trinet.jasi;

import org.junit.jupiter.api.Test;
import org.trinet.jasi.EventTypeMap;
import test.common.UnitTests;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Only covers methods that are actively used/relevant
 */
public class EventTypeMapTests extends UnitTests {

    @Test
    public void verify_is_valid_returns_false_with_minus_one() {
        assertFalse(EventTypeMap.isValid(-1));
    }

    @Test
    public void verify_is_valid_returns_false_with_large_number() {
        assertFalse(EventTypeMap.isValid(10000));
    }

    @Test
    public void verify_is_valid_returns_true_with_number_in_range() {
        assertTrue(EventTypeMap.isValid(5));
    }

    @Test
    public void verify_get_0_returns_correct_string() {
        assertEquals("local", EventTypeMap.get(0));
    }

    @Test
    public void verify_get_1_returns_correct_string() {
        assertEquals("regional", EventTypeMap.get(1));
    }

    @Test
    public void verify_get_2_returns_correct_string() {
        assertEquals("teleseism", EventTypeMap.get(2));
    }

    @Test
    public void verify_get_3_returns_correct_string() {
        assertEquals("quarry", EventTypeMap.get(3));
    }

    @Test
    public void verify_get_4_returns_correct_string() {
        assertEquals("explosion", EventTypeMap.get(4));
    }

    @Test
    public void verify_get_5_returns_correct_string() {
        assertEquals("sonic", EventTypeMap.get(5));
    }

    @Test
    public void verify_get_6_returns_correct_string() {
        assertEquals("longperiod", EventTypeMap.get(6));
    }

    @Test
    public void verify_get_7_returns_correct_string() {
        assertEquals("tremor", EventTypeMap.get(7));
    }

    @Test
    public void verify_get_8_returns_correct_string() {
        assertEquals("trigger", EventTypeMap.get(8));
    }

    @Test
    public void verify_get_9_returns_correct_string() {
        assertEquals("nuclear", EventTypeMap.get(9));
    }

    @Test
    public void verify_get_10_returns_correct_string() {
        assertEquals("other", EventTypeMap.get(10));
    }

    @Test
    public void verify_get_11_returns_correct_string() {
        assertEquals("unknown", EventTypeMap.get(11));
    }

    @Test
    public void verify_get_minus_one_returns_correct_string() {
        assertEquals("unknown", EventTypeMap.get(-1));
    }
}

