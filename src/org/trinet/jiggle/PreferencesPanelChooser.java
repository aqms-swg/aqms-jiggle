package org.trinet.jiggle;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;

import org.trinet.util.StringList;
import org.trinet.util.graphics.IconImage;

public class PreferencesPanelChooser extends PreferencesPanelUtils {
    private static final long serialVersionUID = 1L;

    private List allItems = null;
    private final String borderTitle;
    private final String choicesText;
    private List chooseItems = null;
    private JList chooseList = null;
    private final String[] colNames;
    protected boolean colorChanged = false;
    private JiggleProperties newProps; // new properties as changed by this dialog
    private final String propKey;
    private List rejectItems = null;
    private final Map<String, String> toolTipMap;
    protected boolean typeChanged = false;

    /**
     * Create the preferences panel chooser.
     * 
     * @param props       the properties.
     * @param names       the names.
     * @param toolTipMap  the tool tip map.
     * @param propKey     the property key.
     * @param borderTitle the border title.
     * @param choicesText the text for the choices.
     */
    public PreferencesPanelChooser(JiggleProperties props, String[] colNames, Map<String, String> toolTipMap,
            String propKey, String borderTitle, String choicesText) {
        this.newProps = props;
        this.colNames = colNames;
        this.toolTipMap = toolTipMap;
        this.propKey = propKey;
        this.borderTitle = borderTitle;
        this.choicesText = choicesText;
        setAll(new StringList(colNames).getList());
        List list = newProps.getStringList(propKey).getList();
        if (list == null || list.isEmpty()) {
            list = new StringList(colNames).getList();
        } else {
            String s, name;
            // replace property value with name as needed
            for (int index = 0; index < list.size(); index++) {
                s = list.get(index).toString();
                name = getName(s);
                if (s != name) {
                    list.set(index, name);
                }
            }
        }
        setChoosen(list);
        try {
            initGraphics();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the default property value if no options are selected.
     * 
     * @return the default property value or null if none.
     */
    protected String getDefaultPropertyValue() {
        return "";
    }

    /**
     * Get the name for the property value.
     * 
     * @param propValue the property value.
     * @return the name.
     */
    protected String getName(String propValue) {
        return propValue;
    }

    /**
     * Get the property value.
     * 
     * @param name the name.
     * @return the property value.
     */
    protected String getPropValue(String name) {
        return name;
    }

    /**
     * Initialize the graphics.
     */
    private void initGraphics() {
        Box hbox = Box.createHorizontalBox();
        hbox.setBorder(BorderFactory.createTitledBorder(borderTitle));
        DefaultListModel dlm = new DefaultListModel();
        for (Object element : rejectItems)
            dlm.addElement(element);
        final JList rejectList = new JList(dlm);
        PreferencesListCellRenderer ttr = new PreferencesListCellRenderer(toolTipMap);
        rejectList.setCellRenderer(ttr);
        rejectList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        hbox.add(createScrollComponent(rejectList, "Available to add"));

        Box vbox = Box.createVerticalBox();
        JButton jb = makeButton(">", "Add selected available column(s) to " + choicesText, "e.gif");
        final ActionListener al = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                String cmd = evt.getActionCommand();
                if (cmd.equals(">")) {
                    List selected = rejectList.getSelectedValuesList();
                    DefaultListModel cmodel = (DefaultListModel) chooseList.getModel();
                    DefaultListModel rmodel = (DefaultListModel) rejectList.getModel();
                    int selectIdx = chooseList.getSelectedIndex();
                    if (selectIdx < 0)
                        selectIdx = cmodel.size() - 1;
                    for (Object selection : selected) {
                        cmodel.add(++selectIdx, selection);
                        rmodel.removeElement(selection);
                    }
                } else if (cmd.equals("<")) {
                    List selected = chooseList.getSelectedValuesList();
                    DefaultListModel cmodel = (DefaultListModel) chooseList.getModel();
                    DefaultListModel rmodel = (DefaultListModel) rejectList.getModel();
                    ArrayList rlist = new ArrayList(Arrays.asList(rmodel.toArray()));
                    rlist.addAll(selected);
                    Collections.sort(rlist);
                    rmodel = new DefaultListModel();
                    for (Object element : rlist) {
                        rmodel.addElement(element);
                    }
                    for (Object selection : selected) {
                        cmodel.removeElement(selection);
                    }
                    rejectList.setModel(rmodel);
                }
            }
        };
        jb.addActionListener(al);
        vbox.add(jb);
        jb = makeButton("<", "Remove selected " + choicesText + " column(s)", "w.gif");
        jb.addActionListener(al);
        vbox.add(jb);
        hbox.add(vbox);

        dlm = new DefaultListModel();
        for (int idx = 0; idx < chooseItems.size(); idx++)
            dlm.addElement(chooseItems.get(idx));
        chooseList = new JList(dlm);
        chooseList.setCellRenderer(ttr);
        chooseList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        hbox.add(createScrollComponent(chooseList, choicesText));

        vbox = Box.createVerticalBox();
        jb = makeButton("-", "Move selected " + choicesText + " column to left", "n.gif");
        final ActionListener al2 = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                String cmd = evt.getActionCommand();
                int selectIdx = chooseList.getSelectedIndex();
                Object selected = (selectIdx >= 0) ? chooseList.getModel().getElementAt(selectIdx) : null;
                if (selected == null)
                    return;

                DefaultListModel cmodel = (DefaultListModel) chooseList.getModel();

                if (cmd.equals("+")) {
                    if (selectIdx + 1 < cmodel.size()) {
                        cmodel.removeElementAt(selectIdx);
                        cmodel.insertElementAt(selected, selectIdx + 1);
                        chooseList.setSelectedIndex(selectIdx + 1);
                    }
                } else if (cmd.equals("-")) {
                    if (selectIdx - 1 >= 0) {
                        cmodel.removeElementAt(selectIdx);
                        cmodel.insertElementAt(selected, selectIdx - 1);
                        chooseList.setSelectedIndex(selectIdx - 1);
                    }
                }
            }
        };
        jb.addActionListener(al2);
        vbox.add(jb);
        jb = makeButton("+", "Move selected " + choicesText + " column to right", "s.gif");
        jb.addActionListener(al2);
        vbox.add(jb);
        hbox.add(vbox);
        this.add(hbox);
    }

    /**
     * Make the button.
     * 
     * @param labelStr the label string.
     * @param tooltip  the tool tip.
     * @param imageStr the image filename.
     * @return the button.
     */
    private JButton makeButton(String labelStr, String tooltip, String imageStr) {
        Image image = IconImage.getImage(imageStr);
        JButton btn = null;
        if (image == null) { // handle situation when .gif file can't be found
            btn = new JButton(labelStr);
        } else {
            btn = new JButton(new ImageIcon(image));
        }
        btn.setActionCommand(labelStr);
        btn.setToolTipText(tooltip);
        return btn;
    }

    /**
     * Set the list for all items.
     * 
     * @param all the list for all items.
     */
    public void setAll(List all) {
        allItems = all;
    }

    /**
     * Set the list for chosen and rejected items.
     * 
     * @param choose the list of chosen items.
     */
    public void setChoosen(List choose) {
        ArrayList aList = new ArrayList(allItems);
        aList.removeAll(choose);
        rejectItems = aList;
        aList = new ArrayList(choose);
        aList.removeAll(rejectItems);
        chooseItems = aList;
    }

    /**
     * Set the list for chosen and rejected items.
     * 
     * @param reject the list of rejected items.
     */
    public void setRejected(List reject) {
        ArrayList aList = new ArrayList(allItems);
        aList.removeAll(reject);
        chooseItems = aList;
        aList = new ArrayList(allItems);
        aList.removeAll(chooseItems);
        rejectItems = aList;
    }

    /**
     * Update the properties.
     */
    public void updateProperties() {
        String s;
        final ListModel listModel = chooseList.getModel();
        final int size = listModel.getSize();
        final String choices;
        if (size == 0) {
            choices = getDefaultPropertyValue();
            if (choices == null) {
                newProps.remove(propKey);
                return;
            }
        } else {
            StringBuilder sb = new StringBuilder(size * 3 - 1);
            for (int index = 0; index < size; index++) {
                // convert name to property value as needed
                s = getPropValue(listModel.getElementAt(index).toString());
                if (sb.length() != 0) {
                    sb.append(' ');
                }
                sb.append(s);
            }
            choices = sb.toString();
        }
        String oldChoices = newProps.getProperty(propKey, "");
        boolean b = !choices.equals(oldChoices);
        typeChanged = b;
        if (b) {
            newProps.setProperty(propKey, choices);
        }
    }
}
