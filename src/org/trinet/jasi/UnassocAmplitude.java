package org.trinet.jasi;

import java.sql.*;
import java.util.Collection;
import org.trinet.jdbc.datatypes.DataLong;
import org.trinet.util.Concatenate;
import org.trinet.util.Format;
import org.trinet.util.LeapSeconds;

// Note UnassocAmplitudeTN is NOT a direct concrete subclass of this class 
// rather it is a concrete subclass of AmplitudeTN (thus Amplitude)
// This class is only for factory static methods for creating concrete subtype
public abstract class UnassocAmplitude extends Amplitude {

    /**
    * Factory Method: This is how you instantiate a JasiObject. You do
    * NOT use a constructor. This is so that this "factory" method can create
    * the type of object that is appropriate for your site's data source.
    */

    protected DataLong fileid = new DataLong();

   // Overrides:    abstract public JasiObject create(int schemaType);
   // Removed "final" from Amplitude.create() so this could override.

    /**
    * Instantiate an object of this type. You do
    * NOT use a constructor. This "factory" method creates various
    * concrete implementations. Creates a Solution of the DEFAULT type.
    * @See: JasiObject
    */
    public static Amplitude create() {
      return create(DEFAULT);
    }

    /**
    * Instantiate an object of this type. You do
    * NOT use a constructor. This "factory" method creates various
    * concrete implementations. The argument is an integer implementation type.
    * @See: JasiObject
    */
    public static Amplitude create(int schemaType) {
       return create(JasiFactory.suffix[schemaType]);
    }

    /**
    * Instantiate an object of this type. You do
    * NOT use a constructor. This "factory" method creates various
    * concrete implementations. The argument is as 2-char implementation suffix.
    * @See: JasiObject
    */
    public static Amplitude create(String suffix) {
        // below used to cast to a UnassocAmplitude which throws cast exception -aww 03/10/2005
      return (Amplitude) JasiObject.newInstance("org.trinet.jasi.UnassocAmplitude", suffix);
    }

    public DataLong getFileid() {
        return fileid;
    }

    public void setFileid(long id) {
        //System.out.println("Gmp2db channel : " + getChannelObj().toDelimitedSeedNameString() + " fileid: "+ id);
        fileid.setValue(id);
    }

    public long getFileidValue() {
        return  (fileid.isValidNumber()) ? fileid.longValue() : 0l;
    }

    /**
     * Return a fixed format header to match output from toNeatString(). Has the form:
     * @see: toNeatString()
    */
    public static String getNeatStringHeader() {
      StringBuffer sb = new StringBuffer(300);
      sb.append(ChannelName.getNeatStringHeader());
      sb.append("     Dist  Az     Datetime     Value Type   Units        Per Qual   Source OS WindowStart            Dur F      Fileid");
      return sb.toString();
    }

    abstract public Collection getByTimeNet(double start, double end, String net);
    abstract public Collection getByTimeSubsource(double start, double end, String subsource);

    public String toNeatString() {

        Format df = new Format("%9.4f"); // CORE Java Format class
        Format df0 = new Format("%3d");
        Format df1 = new Format("%8.2f");
        Format df3 = new Format("%4.2f");
        Format df4 = new Format("%6.2f");
        Format df5 = new Format("%13d");
        Format dfs = new Format("%-6s");

        String chl = "xxx";
        try {
          chl = getChannelObj().getChannelName().toNeatString();
        }
        catch (NullPointerException ex) {
          System.out.println("UnAssocAmplitude.toNeatString() " + ex.toString());
        }

        StringBuffer sb = new StringBuffer(128);
        sb.append(chl).append(" ");
        sb.append(df1.form(getHorizontalDistance())).append(" ");
        sb.append(df0.form(Math.round(getAzimuth()))).append(" ");
        sb.append(getDateTime().toString().substring(11)).append(" ");        
        sb.append(df.form(value.doubleValue())).append(" ");
        sb.append(dfs.form(AmpType.getString(type))).append(" ");
        sb.append(dfs.form(Units.getString(units))).append(" ");
        sb.append(df4.form(period.doubleValue())).append(" ");
        sb.append(df3.form(quality.doubleValue())).append(" ");
        Concatenate.rightJustify(sb, getSource(), 8).append(" ");
        Concatenate.rightJustify(sb, ((isClipped()) ? "F" : "T"), 2).append(" ");
        sb.append(LeapSeconds.trueToString(windowStart.doubleValue())).append(" ");
        sb.append(df4.form(windowDuration.doubleValue())).append(" ");
        sb.append(getProcessingStateString());
        sb.append(df5.form(fileid.longValue())).append(" ");
        return sb.toString();
    }


}
