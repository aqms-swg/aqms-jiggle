package org.trinet.jasi.TN;

import java.sql.*;
import java.util.*;

import org.trinet.jasi.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

/**
* Specific ChannelClipAmpData type originating from database table.
*/
public class ChannelClipAmpDataTN extends ChannelClipAmpData implements DbReadableJasiChannelObjectIF {

    protected boolean fromDbase = false; 

    protected final static String TN_TABLE_NAME = "JASI_AMPPARMS_VIEW"; // 03/30/2006 aww

    protected final static String SQL_SELECT_PREFIX =
      // note maxAmp (cnts?) not defined in table so using "clip" value twice -aww 11/10/2004
      //"SELECT net,sta,seedchan,location,ondate,offdate,maxcnts,ampclip FROM " +TN_TABLE_NAME;
      "SELECT net,sta,seedchan,location,ondate,offdate,clip,clip FROM " +TN_TABLE_NAME;

    static protected JasiChannelDbReader jasiDataReader =  new JasiChannelDbReader();
    {
      jasiDataReader.setFactoryClassName(getClass().getName());
      jasiDataReader.setDebug(JasiDatabasePropertyList.debugSQL);
    }

    public ChannelClipAmpDataTN() { }

    public ChannelClipAmpDataTN(ChannelIdIF id) {
        super(id);
    }
    public ChannelClipAmpDataTN(ChannelIdIF id, DateRange dateRange) {
        super(id, dateRange);
    }
    public ChannelClipAmpDataTN(ChannelIdIF id, DateRange dateRange, UnitsAmp maxAmp, UnitsAmp clipAmp ) {
        super(id, dateRange, maxAmp, clipAmp);
    }
    public ChannelClipAmpDataTN(ChannelIdIF id, DateRange dateRange, double maxAmpValue, int maxAmpUnits,
                    double clipAmpValue, int clipAmpUnits) {
        super(id, dateRange, maxAmpValue, maxAmpUnits, clipAmpValue, clipAmpUnits);
    }

    public String toChannelSQLSelectPrefix() {
        return SQL_SELECT_PREFIX ;
    }
    public String toChannelSQLSelectPrefix(java.util.Date date) {
        return SQL_SELECT_PREFIX +
          " WHERE " + DataTableRowUtil.toDateConstraintSQLWhereClause(TN_TABLE_NAME, date) ;
    }

    /** Return count of all channels in data source at specified time. */
    public int getCount(java.util.Date date) {
      return jasiDataReader.getCountBySQL(DataSource.getConnection(), toChannelSQLSelectPrefix(date));
    }

    /**
     * Returns an instance valid for the specified input channel id and date.
     * If input date is null, returns most recently available data for the input channel identifier.
     * Returns null if no data are found satisfying input criteria.
    */
    public ChannelDataIF getByChannelId(ChannelIdIF id, java.util.Date date) {
        return (ChannelDataIF) jasiDataReader.getByChannelId(TN_TABLE_NAME, id, date);
    }

    public Collection getByChannelId(ChannelIdIF id, DateRange dr) {
        return jasiDataReader.getByChannelId(TN_TABLE_NAME, id, dr);
    }

    /**
    * Returns an instance whose data members values are parsed from the input ResultSetDb object.
    */
    private static ChannelClipAmpDataTN parseResultSet(ResultSetDb rsdb) {
        ResultSet rs = rsdb.getResultSet();
        if (rs == null) return null;
        ChannelClipAmpDataTN data = null;
        try {
            data = new ChannelClipAmpDataTN();
            int offset = 0;
            data.channelId =
                jasiDataReader.parseChannelIdKeyByOffset(data.channelId, offset, rs);
            offset = 4;
            if (data.dateRange == null) data.dateRange = new DateRange();
            // 2005/04/05 -removed aww
            //data.dateRange.setMin(rs.getTimestamp(++offset));
            //data.dateRange.setMax(rs.getTimestamp(++offset));
            // String to UTC Date because jdbc times are shifted to local tz millisecs (PST)
            String dstr = rs.getString(++offset);
            if (dstr != null)
              if (dstr.indexOf('.',dstr.length()-4) < 0) dstr += ".0";
              data.dateRange.setMin(EpochTime.stringToDate(dstr)); // -aww 2008/02/11 ok
            dstr = rs.getString(++offset);
            if (dstr != null)
              if (dstr.indexOf('.',dstr.length()-4) < 0) dstr += ".0";
              data.dateRange.setMax(EpochTime.stringToDate(dstr)); // -aww 2008/02/11 ok

            DataDouble dd = rsdb.getDataDouble(++offset);
            if ( !dd.isNull()) {
              data.maxAmp.setValue(dd);
              data.maxAmp.setUnits(Units.COUNTS);
            }
            dd = rsdb.getDataDouble(++offset);
            if ( !dd.isNull()) {
              data.clipAmp.setValue(dd);
              data.clipAmp.setUnits(Units.COUNTS);
            }
            data.fromDbase = true; // flag as db acquired
        }
        catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }
        return data;
    }

    /**
     * Returns a Collection of objects from the default DataSource regardless of date.
     * There may be multiple entries for each channel that represent changes through time.
     * Uses the default DataSource Connection.
    */
    public  Collection loadAll() {
        return loadAll((java.util.Date) null);
    }

    /*
     * Return Collection of objects that were valid on the input date.
     * Data is retrieved from the default DataSource Connection.
    */
    public Collection loadAll(java.util.Date date) {
        return loadAll(DataSource.getConnection(), date);
    }

    public Collection loadAll(java.util.Date date, String[] prefSeedchan) {
        return loadAll(date, null, prefSeedchan, null);
    }

    public Collection loadAll(java.util.Date date, String [] prefNet,
                    String [] prefSeedchan, String [] prefChannel) {
        return loadAll(date, prefNet, prefSeedchan, prefChannel, null);
    }
    public Collection loadAll(java.util.Date date, String [] prefNet,
                    String [] prefSeedchan, String [] prefChannel,
                    String [] prefLocations) {
        return jasiDataReader.loadAll(TN_TABLE_NAME, DataSource.getConnection(), date,
                        prefNet, prefSeedchan, prefChannel, prefLocations);
    }

    /**
    * Returns a Collection of objects created from data obtained from the specified connection
    * that are valid for the specified input date.
    * Note - individual lookups may be more time efficient, if one shot less then several hundred channels.
    */
    static public Collection loadAll(Connection conn, java.util.Date date) {
        return jasiDataReader.loadAll(conn, date);
    }

    // below 1000mv vs. 800mv => 655L cnts see TIMIT$DEV$CHAR; 360L old cusp value from MCA.for
    public static final double MAX_AMP_E_DEFAULT  = 2048.; // VCO USGS
    public static final double MAX_AMP_H_DEFAULT  = 8388608.;
    public static final double CLIP_AMP_E_DEFAULT = 1250.; // VCO USGS?
    public static final double CLIP_AMP_H_DEFAULT = 8388608.;

    public double getMaxAmpValue() {
      //return ( isMaxAmpNull() ) ? getDefaultMaxAmpValue() : maxAmp.doubleValue();
      return ( isMaxAmpNull() ) ? 0. : maxAmp.doubleValue(); // assume no default -aww 2009/03/10
    }
    public double getDefaultMaxAmpValue() {
        String type = channelId.getSeedchan().substring(0,1);
        if (type.equals("E")) return MAX_AMP_E_DEFAULT;
        else return MAX_AMP_H_DEFAULT;
    }

    public double getClipAmpValue() {
      //return ( isClipAmpNull() ) ? getDefaultClipAmpValue() : clipAmp.doubleValue();
      return ( isClipAmpNull() ) ? 0. : clipAmp.doubleValue(); // assume no default -aww 2009/03/10
    }
    public double getDefaultClipAmpValue() {
        String type = channelId.getSeedchan().substring(0,1);
        if (type.equals("E")) return CLIP_AMP_E_DEFAULT;
        else if (type.equals("H")) return CLIP_AMP_H_DEFAULT;
        else return MAX_AMP_H_DEFAULT;
    }


    public JasiObject parseData(Object rsdb) {
      return parseResultSetDb((ResultSetDb) rsdb);
    }
    public JasiObject parseResultSetDb(ResultSetDb rsdb) {
      return parseResultSet((ResultSetDb) rsdb);
    }
    /**Access to the JasiDbReader to allow generic SQL queries. */
    public JasiDataReaderIF getDataReader() {
        return jasiDataReader;
    }

} // end of class ChannelClipAmpDataTN

