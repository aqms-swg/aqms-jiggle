package org.trinet.jasi.magmethods.TN;

import org.trinet.util.*;
import org.trinet.jasi.magmethods.*;

public class LeeMdMagMethod extends MdMagMethod {

    public LeeMdMagMethod() {
        super();
        methodName = "LeeMd";
    }

    // punt here for now
    public void configure(int src, String location, String section) {
        setProperties(location); // location like a filename url
    }

    //  return (-0.87 + 2.0*log10(tau) + 0.0035*coda.getHorizontalDistance() + magCorr.doubleValue());
    protected double calcChannelMagValue(double tau, double distKm, double vertKm, double chlMagCorr) {
        return (-0.87 + 2.0*MathTN.log10(tau) + 0.0035*distKm + chlMagCorr);
    }

    // NoCal uses weighted median
    public int getSummaryMagValueStatType() { return WT_MEDIAN_MAG_VALUE; } // MEDIAN_MAG_VALUE ?

}
/*
Eaton (1992).  MD(f-p) = -0.81 + 2.22*log (f-p) + 0.0011*D + Stacor + G + distCorr
where distCorr:
  + 0.005*(D - 40) if D < 40 km
  + 0.0006*(D - 350) if D > 350 km
  + 0.014*(Z - 10) if Z > 10 km

The supported form of the lapse-time expression is:
  MT(tau) = DMA0 + DMA1*log(tau) + DMA2*log_(tau) + DMLIN*tau + DMZ*Z + DMGN*G + STACOR + Component correction
where tau is the lapse time (P travel time + coda duration f-p), Z is the (positive) depth.
STACOR is the duration magnitude correction for the station, G is the gain correction.
Default coefficients are: DMA0=-1.312, DMA1=2.329, DMA2=0, DMLIN=0.00197, DMZ=0, and DMGN=1.
*/
