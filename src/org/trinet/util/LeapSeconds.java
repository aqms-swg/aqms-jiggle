package org.trinet.util;

import java.util.Calendar;

import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Connection;
import org.trinet.jasi.DataSource;
//import org.trinet.jdbc.datasources.DbaseConnectionDescription;
//import org.trinet.jdbc.datasources.AbstractSQLDataSource;

/**
 * <p>Handle conversion of epoch times to and from bases that include leap seconds. </p>
 *
 * In the NCEDC vernacular on which this is based "true" means with leap-seconds included
 * and "nominal" means without leap-seconds.
 *
 * Which you use doesn't matter so long as all epoch times use the same system or base.
 * However, if you mix them you will get discrepencies of up to 23 seconds.<p>
 *
 * <pre>
 *        00:59|01:00
 *     --+--+--|--+--+--+--+--  "nominal" time (no leap second)
 *     57 58 59  0  1  2  3  4
 *
 *           00:60|01:00
 *     --+--+--+##|--+--+--+--  "true" time (leap second)
 *     57 58 59 60  0  1  2  3
 *
 * </pre>
 *
 * True to nominal: subtract leap seconds<br>
 * Nominal to true: add leap seconds<br>
 * See: http://www.leapsecond.com/java/gpsclock.htm
 <tt>
            Dump of the Leap_Seconds Table

 S_NOMINAL  E_NOMINAL     S_TRUE     E_TRUE   LS_COUNT
---------- ---------- ---------- ---------- ----------
-6.214E+10   78796799 -6.214E+10   78796799          0
  78796800   94694399   78796801   94694400          1
  94694400  126230399   94694402  126230401          2
 126230400  157766399  126230403  157766402          3
 157766400  189302399  157766404  189302403          4
 189302400  220924799  189302405  220924804          5
 220924800  252460799  220924806  252460805          6
 252460800  283996799  252460807  283996806          7
 283996800  315532799  283996808  315532807          8
 315532800  362793599  315532809  362793608          9
 362793600  394329599  362793610  394329609         10
 394329600  425865599  394329611  425865610         11
 425865600  489023999  425865612  489024011         12
 489024000  567993599  489024013  567993612         13
 567993600  631151999  567993614  631152013         14
 631152000  662687999  631152015  662688014         15
 662688000  709948799  662688016  709948815         16
 709948800  741484799  709948817  741484816         17
 741484800  773020799  741484818  773020817         18
 773020800  820454399  773020819  820454418         19
 820454400  867715199  820454420  867715219         20
 867715200  915148799  867715221  915148820         21
 915148800 1136073599  915148822 1136073621         22
1136073600 1230767999 1136073623 1230768022         23
1230768000 1341100799 1230768024 1341100823         24
1341100800 1435708799 1341100825 1435708824         25
1435708800 32503680000 1435708826 32503680026       26

</tt>
*/
public class LeapSeconds {
//
// NOTE: The static data arrays below are default fallbacks, db loading from LEAP_SECONDS takes precedence.
// Can we ferret code calling LeapSeconds w/o db connection and remove it?
// For code that does make a db connection, after making the connection, should it always call loadArraysFromDataSource?
//
// Ending epoch second of time span with a given leap second contained //
  protected static long defNominalSecEnd[] = {
          78796799,
          94694399,
         126230399,
         157766399,
         189302399,
         220924799,
         252460799,
         283996799,
         315532799,
         362793599,
         394329599,
         425865599,
         489023999,
         567993599,
         631151999,
         662687999,
         709948799,
         741484799,
         773020799,
         820454399,
         867715199,
         915148799,
        1136073599,
        1230767999,
        1341100799, 
        1435708799,
        Long.MAX_VALUE 
  };

// Ending true second of time span with a given leap second contained //
  protected static long defTrueSecEnd[] = {
          78796799,
          94694400,
         126230401,
         157766402,
         189302403,
         220924804,
         252460805,
         283996806,
         315532807,
         362793608,
         394329609,
         425865610,
         489024011,
         567993612,
         631152013,
         662688014,
         709948815,
         741484816,
         773020817,
         820454418,
         867715219,
         915148820,
        1136073621,
        1230768022,
        1341100823,
        1435708824,
        Long.MAX_VALUE
  };

// Starting "nominal" epoch second of time span with a given leap second contained //
  protected static long defNominalSecStart[] = {
     Long.MIN_VALUE,
     78796800,
     94694400,
     126230400,
     157766400,
     189302400,
     220924800,
     252460800,
     283996800,
     315532800,
     362793600,
     394329600,
     425865600,
     489024000,
     567993600,
     631152000,
     662688000,
     709948800,
     741484800,
     773020800,
     820454400,
     867715200,
     915148800,
     1136073600,
     1230768000,
     1341100800,
     1435708800
  };

// Starting "true" epoch second of time span with a given leap second contained //
  protected static long defTrueSecStart[] = {
    Long.MIN_VALUE,
    78796801,
    94694402,
    126230403,
    157766404,
    189302405,
    220924806,
    252460807,
    283996808,
    315532809,
    362793610,
    394329611,
    425865612,
    489024013,
    567993614,
    631152015,
    662688016,
    709948817,
    741484818,
    773020819,
    820454420,
    867715221,
    915148822,
   1136073623,
   1230768024,
   1341100825,
   1435708826
  };

// NOTE: double leapseconds are POSSIBLE.
    protected static int defLeapSeconds[] = {
      0,
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      19,
      20,
      21,
      22,
      23,
      24,
      25,
      26
    };

  //
  // Referenced data arrays DEFAULTED to empty arrays
  //
  protected static long nominalSecStart[] = new long[0];
  protected static long nominalSecEnd[] = new long[0];
  protected static long trueSecStart[] = new long[0];
  protected static long trueSecEnd[] = new long[0];
  protected static int leapSeconds[] = new int[0];

  // NOTE: If requiring a DataSource to init arrays, you can't use code that references this class without making db connection first!
  // or by calling the loadArraysFromDataSource static method after you setup a db connection in that code, eg. DateTime objects to string
  // methods call LeapSeconds.
  static {
      if (DataSource.isNull()) { // assume serverside
          System.err.println("DEBUG: LeapSeconds calling DataSource.createDefaultDataSource().createInternalDbServerConnection()");
          DataSource.createDefaultDataSource().createInternalDbServerConnection();
      }
      if ( !DataSource.isNull() && DataSource.isClosed()) { // reset DataSource connection
          System.err.println("DEBUG: LeapSeconds DataSource.isClosed()");
          if (DataSource.onServer()) {
              //System.err.println("DEBUG: LeapSeconds DataSource.onServer()");
              DataSource.createInternalDbServerConnection();
          }
          else {
              System.err.println("DEBUG: LeapSeconds DataSource.getNewConnect()");
              DataSource.set(DataSource.getNewConnect()); // new one
          }
      }
      if (DataSource.isNull() || DataSource.isClosed()) {
          System.err.println("WARNING! LeapSeconds initializing from static default arrays of leap seconds #" + defLeapSeconds[defLeapSeconds.length-1]);
          nominalSecStart = defNominalSecStart;
          nominalSecEnd = defNominalSecEnd;
          trueSecStart = defTrueSecStart;
          trueSecEnd = defTrueSecEnd;
          leapSeconds = defLeapSeconds;
      }
      //else System.err.println("DEBUG: LeapSeconds loadArraysFromDataSource...");

      // initializes data arrays from database if available
      loadArraysFromDataSource();

      // Debug printing, to see in sqlplus when installed on server set serveroutpout on size #maxchars and call dbms_java.set_output(#maxchars)
      //for (int idx = 0; idx< leapSeconds.length; idx++) {
      //    System.out.println(idx + " " + leapSeconds[idx] + " " + trueSecStart[idx]);
      //}
  }
  //

  // Call this method after making db connection
  public static boolean loadArraysFromDataSource() {
    boolean status = false;
    if (! DataSource.isNull() && ! DataSource.isClosed()) {
      System.out.println("Loading leap second array from datasource LEAP_SECONDS table.");
      Statement sm = null;
      ResultSet rs = null;
      try {
        sm = DataSource.getConnection().createStatement();
        rs = sm.executeQuery( "SELECT S_NOMINAL, E_NOMINAL, S_TRUE, E_TRUE, LS_COUNT FROM LEAP_SECONDS" );
          //System.out.println("DEBUG: After executing");
        ArrayList snom =  new ArrayList(32);
        ArrayList enom =  new ArrayList(32);
        ArrayList stru =  new ArrayList(32);
        ArrayList etru =  new ArrayList(32);
        ArrayList lsec =  new ArrayList(32);
        if (rs != null) {
          int count = 0;
          while ( rs.next() ) {
            snom.add(Long.valueOf(rs.getLong(1)));
            enom.add(Long.valueOf(rs.getLong(2)));
            stru.add(Long.valueOf(rs.getLong(3)));
            etru.add(Long.valueOf(rs.getLong(4)));
            lsec.add(Integer.valueOf(rs.getInt(5)));
            count++;
          }
          nominalSecStart = new long [count];
          nominalSecEnd = new long [count];
          trueSecStart = new long [count];
          trueSecEnd = new long [count];
          leapSeconds = new int [count];

          for (int idx = 0; idx < count; idx++) {
            nominalSecStart[idx] = ((Long) snom.get(idx)).longValue();
            nominalSecEnd[idx] = ((Long) enom.get(idx)).longValue();
            trueSecStart[idx] = ((Long) stru.get(idx)).longValue();
            trueSecEnd[idx] = ((Long) etru.get(idx)).longValue();
            leapSeconds[idx] = ((Integer) lsec.get(idx)).intValue();
          }
          //System.out.println("Debug: " + nominalSecStart[count-1] + leapSeconds[count-1]);
          status = true;

        }
      }
      catch (SQLException ex) {
        System.err.println(ex);
        ex.printStackTrace();
        System.err.println("WARNING! LeapSeconds initializing from static default arrays of leap seconds #" + defLeapSeconds[defLeapSeconds.length-1]);
        nominalSecStart = defNominalSecStart;
        nominalSecEnd = defNominalSecEnd;
        trueSecStart = defTrueSecStart;
        trueSecEnd = defTrueSecEnd;
        leapSeconds = defLeapSeconds;
      }
      finally {
          try {
            if (rs != null) rs.close();
            if (sm != null) sm.close();
          }
          catch (SQLException ex) {}
      }
    }
    return status;
  }
  //

  public static double nominalToTrue(double nominalEpochTime) {
    return nominalEpochTime + getLeapSecsAtNominal(nominalEpochTime);
  }
  public static double trueToNominal(double trueEpochTime) {
    return trueEpochTime - getLeapSecsAtTrue(trueEpochTime);
  }

  /** Given an epoch time in the nominal system (no leap seconds) return
   * the number of leap seconds to add to get "true". */
  public static int getLeapSecsAtNominal(double nominalEpochTime) {
    // look thru the list backwards because current times are more common and
    // this way you need only  compare to the nominalSecStart value (not the end).
    // Note we don't examine the earliest time window but just fall through to 0 leapseconds.
    for (int i = nominalSecStart.length-1; i >= 0; i--) {
      if ( nominalEpochTime >= nominalSecStart[i]) {
        return leapSeconds[i];
      }
    }
    return -1;
  }

  /** Given an epoch time in the true system return
   * the number of leap seconds to subtract to get "nominal". I.e. this is
   * the number of leap seconds "included" in this "true" time. */
  public static int getLeapSecsAtTrue(double trueEpochTime) {
    // look thru the list backwards because current times are more common and
    // this way you need only compare to the nominalSecStart value (not the end).
    // Note we don't examine the earliest time window but just fall through to 0 leapseconds.
    int leap = -1;
    double tt = Math.floor(trueEpochTime);
    for (int i = trueSecStart.length-1; i >= 0; i--) {
      if (tt > trueSecEnd[i]) break;
      if ( tt >= trueSecStart[i] && tt <= trueSecEnd[i]) {
        leap = leapSeconds[i];
        break;
      }
    }
    if (leap >= 0) return leap; // true is not a leap second

    // Test for one leap second revert nominal to :59
    tt--;;
    leap = -1;
    for (int i = trueSecStart.length-1; i >= 0; i--) {
        if (tt > trueSecEnd[i]) break;
        if ( tt >= trueSecStart[i] && tt <= trueSecEnd[i] ) {
            leap = leapSeconds[i] + 1;
            break;
        }
    }
    if (leap >= 0)  return leap;

    // Test for two leap seconds revert nominal to :59
    tt--;
    leap = -1;
    for (int i = trueSecStart.length-1; i >= 0; i--) {
        if (tt > trueSecEnd[i]) break;
        if ( tt >= trueSecStart[i] && tt <= trueSecEnd[i] ) {
            leap = leapSeconds[i] + 2;
            break;
        }
    }
    return leap;

  }

  /** Given a true epoch time (leap seconds added) return
   * the number of leap seconds occurring just at that time. 
   * Returns 0, unless the input value is a leap second, then 
   * return value is 1 for first leap second and 2 only if its
   * a second leap second in a 2-second leap.
   */
  public static int secondsLeptAt(double trueEpochTime) {
    // look thru the list backwards because current times are more common and
    // this way you need only compare to the nominalSecStart value (not the end).
    // Note we don't examine the earliest time window but just fall through to 0 leapseconds.
    int leap = -1;
    double tt = Math.floor(trueEpochTime);
    for (int i = trueSecStart.length-1; i >= 0; i--) {
      if (tt > trueSecEnd[i]) break;
      if ( tt >= trueSecStart[i] && tt <= trueSecEnd[i]) {
        leap = 0;
        break;
      }
    }
    if (leap >= 0) return leap; // true is not a leap second

    // Test for one leap second revert nominal to :59
    tt--;;
    leap = -1;
    for (int i = trueSecStart.length-1; i >= 0; i--) {
        if (tt > trueSecEnd[i]) break;
        if ( tt >= trueSecStart[i] && tt <= trueSecEnd[i] ) {
            leap = 1;
            break;
        }
    }
    if (leap >= 0)  return leap;

    // Test for two leap seconds revert nominal to :59
    tt--;
    leap = -1;
    for (int i = trueSecStart.length-1; i >= 0; i--) {
        if (tt > trueSecEnd[i]) break;
        if ( tt >= trueSecStart[i] && tt <= trueSecEnd[i] ) {
            leap = 2;
            break;
        }
    }
    return leap;

  }

  public static final String trueToString(double trueEpochTime) {
      //return trueToString(trueEpochTime, EpochTime.DEFAULT_FORMAT);
      return trueToString(trueEpochTime, EpochTime.FFF_NO_ZONE_FORMAT, "UTC");
  }

  public static final String trueToString(double trueEpochTime, String pattern) {
    return trueToString(trueEpochTime, pattern, "UTC");
  }

  public static final String trueToString(double trueEpochTime, String pattern, String zone) {
        StringBuffer tStr = null;
        double tt = Math.floor(trueEpochTime);
        int lsecs = -1;
        for (int ii = trueSecEnd.length-1; ii >= 0; ii--) {
            if (trueSecEnd[ii] < tt) break;
            if (trueSecEnd[ii] >= tt && trueSecStart[ii] <= tt) {
                lsecs = leapSeconds[ii];
                break;
            }
        }

        if (lsecs >= 0) { // input not a leap second
           tStr = new StringBuffer(EpochTime.epochToString(trueEpochTime - lsecs, pattern, zone));
        }
        else { // input has one or more leap seconds
          int idx = pattern.indexOf("ss");  // >= 0 when pattern has seconds
          lsecs = -1;
          tt--;
          for (int ii =trueSecEnd.length-1; ii >= 0; ii--) {
            if (trueSecEnd[ii] < tt) break;
            if (trueSecEnd[ii] >= tt && trueSecStart[ii] <= tt) {
                lsecs = leapSeconds[ii];  // closest leap
                break;
            }
          }
          if (lsecs >= 0) { // input with just one leap second
            tStr = new StringBuffer(EpochTime.epochToString(trueEpochTime - lsecs - 1, pattern, zone));
            if (idx >= 0 && tStr.substring(idx, idx+2).equals("59")) tStr.replace(idx, idx+2, "60"); // only when pattern has seconds
          }
          else { // input with two leap seconds
            lsecs = -1;
            tt--;
            for (int ii =trueSecEnd.length-1; ii >= 0; ii--) {
              if (trueSecEnd[ii] < tt) break;
              if (trueSecEnd[ii] >= tt && trueSecStart[ii] <= tt) {
                lsecs = leapSeconds[ii];  // closest leap
                break;
              }
            }
            tStr = new StringBuffer(EpochTime.epochToString(trueEpochTime - lsecs - 2, pattern, zone));
            if (idx >= 0 && tStr.substring(idx, idx+2).equals("59")) tStr.replace(idx, idx+2, "61"); // only when pattern has seconds
          } 
        }
        return tStr.toString(); 
  }

  public static String trueToPST(double dateTime, String pattern ) {
    return trueToString(dateTime, pattern, "PST");
  }

  public static String trueToPST(double dateTime) {
    return trueToString(dateTime, EpochTime.FFF_NO_ZONE_FORMAT, "PST");
  }

  public static String trueToDefaultZone(double dateTime) {
    return trueToDefaultZone(dateTime, EpochTime.FFF_NO_ZONE_FORMAT);
  }

  public static String trueToDefaultZone(double dateTime, String pattern) {
    return trueToString(dateTime, pattern, Calendar.getInstance().getTimeZone().getID());
  }

  public static final double stringToTrue(String dateStr) {
      return stringToTrue(dateStr, EpochTime.DEFAULT_FORMAT);
  }
  public static final double stringToTrue(String dateStr, String pattern) {
      StringBuffer tStr = new StringBuffer(dateStr);
      int idx = pattern.indexOf("ss");
      int leap = 0;
      if (idx >= 0 ) { // only when input has possible leap seconds in format
          if (tStr.substring(idx, idx+2).equals("60")) {
              tStr.replace(idx, idx+2, "59"); // also done by stringToEpoch call below
              leap = 1; // input with one leap second
          }
          else if (tStr.substring(idx, idx+2).equals("61")) {
              tStr.replace(idx, idx+2, "59"); // also done by stringToEpoch call below
              leap = 2; // input with two leap seconds
          }
      }
      return (nominalToTrue(EpochTime.stringToEpoch(tStr.toString(), pattern)) + leap);
  }

  public static java.util.Date trueToDate(double trueEpochTime) {
    return EpochTime.epochToDate(trueToNominal(trueEpochTime));
  }

  public static double dateToTrue(java.util.Date date) {
    return nominalToTrue(EpochTime.dateToEpoch(date));
  }

  /* 
  public static void main(String[] args) {

    //
    //String dbhost   = args[0];
    //String dbdomain = args[1];
    //String dbname   = args[2];
    //String dbuser   = args[3];
    //String dbpass   = args[4];

    // Create a DataSource connection to input db here
    //DbaseConnectionDescription dcd = new DbaseConnectionDescription(dbhost, dbdomain, dbname, AbstractSQLDataSource.DEFAULT_DS_DRIVER, dbuser, dbpass);
    //DataSource ds = new DataSource();
    //ds.set(dcd);

    //for (int idx = 0; idx< LeapSeconds.leapSeconds.length; idx++) {
    //      System.out.println(idx + " " + LeapSeconds.leapSeconds[idx] + " " + LeapSeconds.trueSecStart[idx]);
    //}
    //LeapSeconds.loadArraysFromDataSource(); // do this after creating DataSource
    //for (int idx = 0; idx< LeapSeconds.leapSeconds.length; idx++) {
    //      System.out.println(idx + " " + LeapSeconds.leapSeconds[idx] + " " + LeapSeconds.trueSecStart[idx]);
    //}
    //

    // 915148820 true  and 915148799 nominal at Dec 31, 1998 23:59:59 
    long start = 915148818;
    double tt = 0;
    double tt2 = 0;
    String dStr = null;
    for (long i = start; i < start + 6; i++) {
      dStr = LeapSeconds.trueToString((double)i);
      System.out.print(i + " =true: " + dStr);
      System.out.print(" :true= " + LeapSeconds.stringToTrue(dStr));
      tt2 = EpochTime.stringToEpoch(dStr);
      tt = LeapSeconds.trueToNominal((double)i);
      System.out.println(" stringToEpoch tt2= " + tt2 + " trueToNominal tt= " + tt);
      System.out.println("tt2 - tt = " + (tt2-tt));
      System.out.println("epoch2Str(trueToNominal) =       " + EpochTime.epochToString(tt));
      System.out.println("epoch2Str(str2epoch(true2str)) = " + EpochTime.epochToString(tt2) + "\n");
      
    }
    //
    // Test nominal (epoch) to leap seconds and back again.
    //
    Format df = new Format("%12.3f");
    double nominal = 1072102160.123;
    double leap = LeapSeconds.nominalToTrue(nominal);
    System.out.println("\nmominalToTrue(double) test should have 22.0 leap seconds:");
    System.out.println("nominalToTrue("+df.form(nominal)+") "+df.form(leap));
    System.out.println("trueToNominal("+df.form(leap)+") "+df.form(LeapSeconds.trueToNominal(leap)));
    nominal = 10.123;
    leap = LeapSeconds.nominalToTrue(nominal);
    System.out.println("\nRelative time seconds nominal=10.123 should be leap= 10.123");
    System.out.println("nominalToTrue("+df.form(nominal)+") "+df.form(leap));

      int yr   = 1997;
      int jday = 181;
      int hr   = 23;
      int mn   = 59;
      int sec  = 60;
      int frac = 1507;

      // DateTime class keeps precision to nanoseconds (0.000001),
      // native Java time only good to milliseconds (0.001)
      frac = Math.round((float)frac/10.f);
      StringBuffer sb = new StringBuffer(32);
      Format d4 = new Format("%04d");
      Format d3 = new Format("%03d");
      Format d2 = new Format("%02d");
      sb.append(d4.form(yr)).append(" ");
      sb.append(d3.form(jday)).append(" ");
      sb.append(d2.form(hr)).append(":");
      sb.append(d2.form(mn)).append(":");
      sb.append(d2.form(sec)).append(".");
      sb.append(d3.form(frac));
      //Concatenate.format(sb,(long)yr,4,4).append(" ");
      //Concatenate.format(sb,(long)jday,3,3).append(" ");
      //Concatenate.format(sb,(long)hr,2,2).append(":");
      //Concatenate.format(sb,(long)mn,2,2).append(":");
      //Concatenate.format(sb,(long)sec,2,2).append(".");
      //Concatenate.format(sb,(long)frac,3,3);
      tt = LeapSeconds.stringToTrue(sb.toString(), "yyyy DDD HH:mm:ss.SSS");
      System.out.println("stringToTrue: " + sb.toString() + " = " + tt);
      System.out.println("stringToTrue: " + "1997 181 23:59:60.151 = " + tt);
      System.out.println("trueToString: " + LeapSeconds.trueToString(tt));

      System.out.println("trueLeapString: " + LeapSeconds.trueToString(867715220.151));
      System.out.println("trueLeapString: " + LeapSeconds.trueToString(867715220.1519, "yyyy DDD HH:mm:ss.SSS"));
      System.out.println("trueLeapString: " + LeapSeconds.trueToString(867715220.1519, "yyyy DDD HH:mm:ss.fff"));

      System.out.println("trueLeapString 2009/01/01: " + LeapSeconds.trueToString(1230768022.12345, "yyyy DDD HH:mm:ss.SSS"));
      System.out.println("trueLeapString 2009/01/01: " + LeapSeconds.trueToString(1230768022.12345, "yyyy DDD HH:mm:ss.fff"));
      System.out.println("trueLeapString 2009/01/01: " + LeapSeconds.stringToTrue("2009 01 00:00:00.000", "yyyy DDD HH:mm:ss.SSS"));
      tt = LeapSeconds.stringToTrue("0000/00/00 00:00:00.000", "yyyy/MM/dd HH:mm:ss.SSS");
      System.out.println("stringToTrue 0000/00/00 00:00:00.000 = " + tt);
  }
  */

}
