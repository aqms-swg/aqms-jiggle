package test.org.trinet.jasi;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.trinet.jasi.*;
import test.common.DatabaseTests;
import test.testqueries.org.trinet.jasi.ChannelGroupTestsQueries;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChannelGrouperTests extends DatabaseTests {

    public static final int ChannelGrouperTestsEventId = 39533071;
    public static final int ChannelGrouperTestsOrId = 105451981;
    public static final int ChannelGrouperTestsAmp1Id = 904917477;
    public static final int ChannelGrouperTestsAmp2Id = 904917478;
    public static final int ChannelGrouperTestsAmp3Id = 904917479;
    public static final int ChannelGrouperTestsPrefMag = 108867285;
    public static final int ChannelGrouperTestsPrefMec = 3426229;

    @BeforeAll()
    public static void addDataToDatabase() throws Exception {
            ChannelGroupTestsQueries testQueries = new ChannelGroupTestsQueries(postgreSQLContainer.getJdbcUrl());

            // Amplitude 1
            int rows = testQueries.populateEventDataForChannelGrouperTests();
            assertTrue(rows > 0);
            rows = testQueries.populateAmp1DataForChannelGrouperTests();
            assertTrue(rows > 0);
            rows = testQueries.populateAssoCamoAmp1DataForChannelGrouperTests();
            assertTrue(rows > 0);
            rows = testQueries.populateOriginDataForChannelGroupTests();
            assertTrue(rows > 0);

            // Amplitude 2
            rows = testQueries.populateAmp2DataForChannelGrouperTests();
            assertTrue(rows > 0);
            rows = testQueries.populateAssoCamoAmp2DataForChannelGrouperTests();
            assertTrue(rows > 0);

            // Amplitude 3
            rows = testQueries.populateAmp3DataForChannelGrouperTests();
            assertTrue(rows > 0);
            rows = testQueries.populateAssoCamoAmp3DataForChannelGrouperTests();
            assertTrue(rows > 0);
    }

    @Test
    public void verify_is_same_group_function_returns_true_if_the_two_channel_label_objects_belong_in_the_same_group() {
            TestDataSource.create(DatabaseTests.getHost(), DatabaseTests.getDbName(),
                    DatabaseTests.getDbUsername(), DatabaseTests.getDbPassword(), DatabaseTests.getSubProtocol(), DatabaseTests.getPortNumber(), DatabaseTests.getDatabaseDriver());
            AmpList amplist = new AmpList(Amplitude.create().getBySolution(ChannelGrouperTestsEventId));
            ChannelGrouper grouper = new ChannelGrouper(amplist);
            ChannelableListIF list = grouper.getNext();
            Channelable ch1 = (Channelable) list.get(0);
            Channelable ch2 = (Channelable) list.get(1);

            assertTrue(grouper.inSameGroup(ch1, ch2));
    }

    @Test
    public void verify_is_same_group_function_returns_false_if_the_two_channel_label_objects_do_not_belong_in_the_same_group() {
            TestDataSource.create(DatabaseTests.getHost(), DatabaseTests.getDbName(),
                    DatabaseTests.getDbUsername(), DatabaseTests.getDbPassword(), DatabaseTests.getSubProtocol(), DatabaseTests.getPortNumber(), DatabaseTests.getDatabaseDriver());
            AmpList amplist = new AmpList(Amplitude.create().getBySolution(ChannelGrouperTestsEventId));
            ChannelGrouper grouper = new ChannelGrouper(amplist);
            Channelable ch1 = (Channelable) grouper.getNext().get(0);
            Channelable ch2 = (Channelable) grouper.getNext().get(0);

            assertFalse(grouper.inSameGroup(ch1, ch2));
    }
}