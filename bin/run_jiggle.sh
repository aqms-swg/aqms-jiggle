#!/bin/bash

# run_jiggle.sh
# This script runs Jiggle, using input parameters in properties files in the prop/ directory
# This script lives in the bin/ directory

if [ "$#" -ne 2 ]
then
   echo "Usage: ./run_jiggle.sh <java_version> <prop_file_path_name>"
   echo "Possible values of <java_version>: 6, 7, 8"
   echo "<prop_file_path_name> must include path"
   exit 1
fi

# Java version from command line
JAVA_VER=$1
echo "JAVA_VER: 1."${JAVA_VER}

case "${JAVA_VER}" in
   6)
      echo "6"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.6.0_65`
      ;;
   7 )
      echo "7"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.7.0_80`
      ;;
   8 )
      echo "8"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.8`
#      export JAVA_HOME=`/usr/libexec/java_home -v 1.8.0_202`
      ;;
   11 )
      echo "Using JDK 11"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.11`
      ;;
   * )
      echo "8"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.8`
#      export JAVA_HOME=`/usr/libexec/java_home -v 1.8.0_202`
      ;;
esac

java -version

# Properties file path from command line
PROP_FILE=$2
echo "PROP_FILE: "${PROP_FILE}

# jiggle.jar file name
JAR_FILE="jiggle_p"${JAVA_VER}"g.jar"
echo "JAR_FILE: "${JAR_FILE}

# Need to use entire absolute path (not relative path) for JIGGLE_USER_HOMEDIR
java -Xms1000M -Xmx2560M -DJIGGLE_HOME=./ -DJIGGLE_USER_HOMEDIR=../prop -jar ${JAR_FILE} ${PROP_FILE} # Built from command line scripts
