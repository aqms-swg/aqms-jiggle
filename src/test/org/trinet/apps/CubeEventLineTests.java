package test.org.trinet.apps;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.trinet.apps.CubeEventLine;
import test.common.DatabaseTests;
import test.testqueries.common.TestQueries;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CubeEventLineTests extends DatabaseTests {

    public static final int CubeEventLineTestEventId = 60945919;

    @BeforeAll
    public static void populateEventDataForCubeEventLineTest() throws Exception {
        TestQueries testQueries = new TestQueries(postgreSQLContainer.getJdbcUrl());
        int rows = testQueries.populateEventDataForCubeEventLineTest();
        assertTrue(rows > 0);
    }

    @Test
    public void verify_cube_event_main_function_with_event_format_parameter_prints_correctly() throws Exception {
        CubeEventLine.main(new String[]{DatabaseTests.getDbUsername(), DatabaseTests.getDbPassword(), DatabaseTests.getHost(),
                DatabaseTests.getDbName(), String.valueOf(CubeEventLineTestEventId), "E", DatabaseTests.getPortNumber(), getTestPropFilePath() + "jiggle.props"});
        assertEquals("E 60945919CI2197001010000-010            0       0        0                       0  Ny", CubeEventLine.getOutputString());
    }

    @Test
    public void verify_cube_event_main_function_with_email_format_parameter_prints_correctly() throws Exception {
        CubeEventLine.main(new String[]{DatabaseTests.getDbUsername(), DatabaseTests.getDbPassword(), DatabaseTests.getHost(),
                DatabaseTests.getDbName(), String.valueOf(CubeEventLineTestEventId), "eventmail", DatabaseTests.getPortNumber(), getTestPropFilePath() + "jiggle.props"});
        assertTrue(CubeEventLine.getOutputString().contains("CI 60945919\n"));
    }

    @Test
    public void verify_cube_event_main_function_with_cancelmail_format_parameter_prints_correctly() throws Exception {
        CubeEventLine.main(new String[]{DatabaseTests.getDbUsername(), DatabaseTests.getDbPassword(), DatabaseTests.getHost(),
                DatabaseTests.getDbName(), String.valueOf(CubeEventLineTestEventId), "cancelmail", DatabaseTests.getPortNumber(), getTestPropFilePath() + "jiggle.props"});
        assertTrue(CubeEventLine.getOutputString().contains("EVENT CANCELLED AFTER HUMAN REVIEW"));
    }

    @Test
    public void verify_cube_event_main_function_with_delete_format_parameter_prints_correctly() throws Exception {
        CubeEventLine.main(new String[]{DatabaseTests.getDbUsername(), DatabaseTests.getDbPassword(), DatabaseTests.getHost(),
                DatabaseTests.getDbName(), String.valueOf(CubeEventLineTestEventId), "DE", DatabaseTests.getPortNumber(), getTestPropFilePath() + "jiggle.props"});
        assertEquals("DE60945919CI2", CubeEventLine.getOutputString());
    }
}