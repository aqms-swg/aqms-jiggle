package org.trinet.jdbc.table;
import java.sql.Connection;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;

/** Constructor uses static data members defined by the TableRowCoda interface to initialize the base class with 
* the parameters necessary to describe the schema table named by the interface String parameter DB_TABLE_NAME. 
* The class implements several convenience methods to provides class specific static arguments to the argument
* list of the DataTableRow base class methods. Base class methods are used to set or modify the values and states
* of the contained DataObjects. Because the base class uses a JDBC connection class to access the database containing
* the table described by this object, a connection object must be instantiated before using any of the database enabled
* methods of this class.
* Object states refering to data update, nullness, and mutability are inhereited from the DataTableRow base class
* implementation of the DataState interface. These state conditions are used to control the methods access to the
* object and its contained DataObjects, which also implement the DataState interface.
* The default constructor sets the default states to: setUpdate(false), setNull(true), setMutable(true), and
* setProcessing(NONE).
*/

public class Coda extends StnChlTableRow implements TableRowCoda {

    public Coda () {
	super(DB_TABLE_NAME, SEQUENCE_NAME, MAX_FIELDS, KEY_COLUMNS, FIELD_NAMES, FIELD_NULLS, FIELD_CLASS_IDS);
    }
    
/** Copy constructor invokes the default constructor, then clones all the DataObject classes of the input argument.
* The newly instantiated object state values are set to those of the input object.
*/ 
    public Coda(Coda object) {
	this();
	for (int index = 0; index < MAX_FIELDS; index++) {
	    fields.set(index, ((DataObject) object.fields.get(index)).clone());
	}
	this.valueUpdate = object.valueUpdate;
	this.valueNull = object.valueNull;
	this.valueMutable = object.valueMutable;
    }

/** Constructor invokes default constructor, then sets the default Connection object to the handle of the input Connection argument.
* The newly instantiated object state values are those of the default constructor.
*/
    public Coda(Connection conn) {
	this();
        setConnection(conn);
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Coda(long coid) {
	this();
	fields.set(COID, new DataLong(coid));
	valueUpdate = true;
	valueNull = false;
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Coda(long coid, DataStnChl sc, String codaType, double dateTime, double tau, String iphase) {
	this(coid);
	setDataStnChl(sc);

	if (NullValueDb.isEmpty(codaType)) throw new IllegalArgumentException("Coda Parameter codaType is undefined.");
	fields.set(CODATYPE, new DataString(codaType));

	fields.set(DATETIME, new DataDouble(dateTime));

        if (tau <= 0.) throw new IllegalArgumentException("Coda Constructor Parameter tau input <= 0");
	fields.set(TAU, new DataDouble(tau));

	if (! NullValueDb.isEmpty(iphase)) fields.set(IPHASE, new DataString(iphase));
    }

    public Coda(long coid, DataStnChl sc, String codaType, double dateTime, double afix, double afree, double qfix,
		double qfree, double tau, int nsample, String iphase, String units, int [] time, int [] amp) {
	this(coid);
	setDataStnChl(sc);

	if (NullValueDb.isEmpty(codaType)) throw new IllegalArgumentException("Coda Parameter codaType is undefined.");
	fields.set(CODATYPE, new DataString(codaType));

	fields.set(DATETIME, new DataDouble(dateTime));

	fields.set(AFIX, new DataDouble(afix));
	fields.set(AFREE, new DataDouble(afree));
	fields.set(QFIX, new DataDouble(qfix));
	fields.set(QFREE, new DataDouble(qfree));

	if (tau > 0.) fields.set(TAU, new DataDouble(tau));

	if (! NullValueDb.isEmpty(iphase)) fields.set(IPHASE, new DataString(iphase));
	if (! NullValueDb.isEmpty(units))  fields.set(UNITS, new DataString(units));

        if (nsample > 0) {
            fields.set(NSAMPLE, new DataLong(nsample));
            setTimeAmpFields(nsample, time, amp);
        }
    }

    private void setTimeAmpFields(int nsample, int [] time, int [] amp) {
	int count = 0;
        int samples = time.length;
        samples = Math.min(nsample, samples);
        if (samples < 1) return;
        samples = Math.min(samples, 6);

	if (count < samples) {
	    fields.set(TIME1, new DataLong(time[count]));
	    fields.set(AMP1, new DataLong(amp[count]));
	    count++;
	}
	else return;
	if (count < nsample) {
	    fields.set(TIME2, new DataLong(time[count]));
	    fields.set(AMP2, new DataLong(amp[count]));
	    count++;
	}
	else return;
	if (count < nsample) {
	    fields.set(TIME3, new DataLong(time[count]));
	    fields.set(AMP3, new DataLong(amp[count]));
	    count++;
	}
	else return;
	if (count < nsample) {
	    fields.set(TIME4, new DataLong(time[count]));
	    fields.set(AMP4, new DataLong(amp[count]));
	    count++;
	}
	else return;
	if (count < nsample) {
	    fields.set(TIME5, new DataLong(time[count]));
	    fields.set(AMP5, new DataLong(amp[count]));
	    count++;
	}
	else return;
	if (count < nsample) {
	    fields.set(TIME6, new DataLong(time[count]));
	    fields.set(AMP6, new DataLong(amp[count]));
	    count++;
	}	
    }

/** Initializes string members used in base class method argument lists
*/
    private void initialize() {
	keyColumn = FIELD_NAMES[KEY_COLUMNS[0]];
	assocTableOrigin = "ASSOCCOO";
	assocTableNetmag = "ASSOCCOM";
    }
}
