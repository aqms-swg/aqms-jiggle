// column_name data_type type_name column_size ... isnullable
package org.trinet.jdbc;
import java.sql.*;
import java.util.*;

/** Class hold data members describing table column attributes stored in data members. */
public class ColumnData {
/** Number of number of table column attributes */
    public static final int MAX_FIELDS = 9;
/** Name of table containing column */
    public String tableName;
/** Name of table column */
    public String columnName;
/** SQL data type code of the column */
    public int dataType;
/** SQL data type name of the column */
    public String typeName;
/** Width of the column data field */
    public int columnSize;
/** Decimal fraction digits in column data field */
    public int decimalDigits;
/** Default String value of the column data, if NULL */
    public String columnDef;
/** Column position in a table row */
    public int ordinalPosition;
/** Is column data nullable? (SET value == NULL) */
    public boolean isNullable;

/** Default constructor empty */
    public ColumnData () {}

/** Returns String array whose elements are the String values of the data attributes. 
*/
    public String [] toStringArray() {
	String [] rval = new String[9];
	rval[0] = StringSQL.NullToEmpty(tableName);
	rval[1] = StringSQL.NullToEmpty(columnName);
	rval[2] = String.valueOf(dataType);
	rval[3] = StringSQL.NullToEmpty(typeName);
	rval[4] = String.valueOf(columnSize);
	rval[5] = String.valueOf(decimalDigits);
	rval[6] = StringSQL.NullToEmpty(columnDef);
	rval[7] = String.valueOf(ordinalPosition);
	rval[8] = String.valueOf(isNullable);
	return rval;
    }
	
/** Returns a String containing "label: value" pairs of the column attributes. */
    public String toString() {
	return "tableName: " + tableName + " columnName: " + columnName +
		" dataType: " + dataType + " typeName: " + typeName + 
		" columnSize: " + columnSize + " decimalDigits: " + decimalDigits +
		" columnDef: " + columnDef + " ordinalPosition: " + ordinalPosition +
		" isNullable: " + isNullable ;
    }

/** Returns a ColumnData array containing the column attributes for specified input schema and table.
* Input schema string can be null, null matches all schemas, and is case-sensitive, (e.g. uppercase user name string).
* Requires non-null Connection and tableName.
* Returns null, if input java.sql.Connection is null, input tableName is null, or an SQLException occurs.
*/
    public static ColumnData [] getColumnData(Connection conn, String schema, String tableName) {
	if (conn == null) {
	    System.out.println("ColumnData getColumnData: input connection null.");
	    return null;
	}
	if (tableName == null) {
	    System.out.println("ColumnData getColumnData: input tableName null.");
	    return null;
	}

        DatabaseMetaData dbMD;
        ResultSet rs = null;
        ColumnData cd;
        String yesno;
        ArrayList cols = new ArrayList();
	String catalog = null;
	boolean exFlag = false;

        try {
            dbMD = conn.getMetaData();
            rs = dbMD.getColumns(catalog, schema, tableName, null);
            while (rs.next()){
                try {
                    cd = new ColumnData();
                    cd.tableName = rs.getString("TABLE_NAME");
                    cd.columnName = rs.getString("COLUMN_NAME");
                    cd.dataType = rs.getInt("DATA_TYPE");
                    cd.typeName = rs.getString("TYPE_NAME");
                    cd.columnSize = rs.getInt("COLUMN_SIZE");
                    cd.decimalDigits = rs.getInt("DECIMAL_DIGITS");
                    cd.columnDef = rs.getString("COLUMN_DEF");
                    cd.ordinalPosition = rs.getInt("ORDINAL_POSITION");
                    yesno = rs.getString("IS_NULLABLE");
                    if ( yesno.equals("YES") )
                        cd.isNullable = true;
                    else
                        cd.isNullable = false;
                    cols.add(cd);
//		    System.out.println("ColumnData.toString(): \n" + cd.toString());
                }
                catch (SQLException ex) {
                    System.err.println("ColumnData: getColumnNames rs.getXXX SQLException");
                    SQLExceptionHandler.prtSQLException(ex);
		    exFlag = true;
		    break;
                }
            }
        }
        catch (SQLException ex) {
            System.err.println("ColumnData: getColumnNames dbMD.getColumns SQLException");
            SQLExceptionHandler.prtSQLException(ex);
	    exFlag = true;
        }
        finally {
            try {
                rs.close();
            }
            catch (SQLException ex) {
                System.err.println("ColumnData: getColumnNames rs.close SQLException");
                SQLExceptionHandler.prtSQLException(ex);
            }
        }
	if (exFlag) return null;
        ColumnData [] data = new ColumnData[cols.size()];
        cols.toArray((Object []) data);
        
        return data;
    }
 
/** Returns a String array containing the names of the table key columns in key sequence order.
* Input schema string can be null, null matches all schemas, and is case-sensitive, (e.g. uppercase user name string).
* Requires non-null Connection and tableName.
* Returns null, if input java.sql.Connection is null, input tableName is null, or an SQLException occurs.
*/
    public static String [] getPrimaryKeys(Connection conn, String schema, String tableName) {
	if (conn == null) {
	    System.out.println("ColumnData getPrimaryKeys: input connection null.");
	    return null;
	}
	if (tableName == null) {
	    System.out.println("ColumnData getPrimaryKeys: input tableName null.");
	    return null;
	}

        ArrayList kn = new ArrayList();
        int [] ks = new int[16];
        int ncomp=0;
        DatabaseMetaData dbMD = null;
        ResultSet rs = null;
	String catalog = null;
	boolean exFlag = false;

        try {
            dbMD = conn.getMetaData();
            rs = dbMD.getPrimaryKeys(catalog, schema, tableName);
            while (rs.next()) {
                try {
                    kn.add(rs.getString("COLUMN_NAME"));
                    ks[ncomp] = rs.getInt("KEY_SEQ") - 1;
                    ncomp++;
                }
                catch (SQLException ex) {
                    System.err.println("ColumnData: getPrimaryKeys rs.getResults SQLException");
                    SQLExceptionHandler.prtSQLException(ex);
		    exFlag = true;
		    break;
                }
            }
        }
        catch (SQLException ex) {
            System.err.println("ColumnData: dbMD.getPrimaryKeys SQLException");
            SQLExceptionHandler.prtSQLException(ex);
	    exFlag = true;
        }
        finally {
            try {
                rs.close();
            }
            catch (SQLException ex) {
                System.err.println("ColumnData: getPrimaryKeys rs.close SQLException");
                SQLExceptionHandler.prtSQLException(ex);
            }
        }
	if (exFlag) return null;
        String [] keyNames = new String[kn.size()];
        int j;
        for (int i = 0; i < ncomp; i++) {
            j = ks[i];
            keyNames[i] = (String) kn.get(j);   
        }
        return keyNames;
    }

}
