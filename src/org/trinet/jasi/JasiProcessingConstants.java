package org.trinet.jasi;
public interface JasiProcessingConstants {
// Constant values used to identify respective processing state of members in lists, access methods, or models.
    public static final int STATE_UNKNOWN      = 0;
    public static final int STATE_AUTO         = 1;
    public static final int STATE_HUMAN        = 2;
    public static final int STATE_INTERMEDIATE = 3;
    public static final int STATE_FINAL        = 4;
    public static final int STATE_CANCELLED    = 5;

    public static final String STATE_UNKNOWN_TAG      = "?";
    public static final String STATE_AUTO_TAG         = "A";
    public static final String STATE_HUMAN_TAG        = "H";
    public static final String STATE_INTERMEDIATE_TAG = "I";
    public static final String STATE_FINAL_TAG        = "F";
    public static final String STATE_CANCELLED_TAG    = "C";

    public static final String STATE_UNKNOWN_LABEL      = "unknown";
    public static final String STATE_AUTO_LABEL         = "automatic";
    public static final String STATE_HUMAN_LABEL        = "human";
    public static final String STATE_INTERMEDIATE_LABEL = "intermediate";
    public static final String STATE_FINAL_LABEL        = "final";
    public static final String STATE_CANCELLED_LABEL        = "cancelled";

}
