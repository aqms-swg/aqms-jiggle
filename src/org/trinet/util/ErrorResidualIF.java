package org.trinet.util;
public interface ErrorResidualIF {
    public double getResidual();
}
