package org.trinet.jiggle.map;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.Properties;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

import com.bbn.openmap.dataAccess.shape.DbfTableModel;
import com.bbn.openmap.layer.shape.ShapeLayer;
import com.bbn.openmap.omGraphics.OMGraphic;
import com.bbn.openmap.omGraphics.OMGraphicList;
import com.bbn.openmap.omGraphics.DrawingAttributes;
import com.bbn.openmap.plugin.esri.EsriLayer;
import com.bbn.openmap.util.Debug;
import com.bbn.openmap.util.PropUtils;

import java.awt.Paint;
import java.util.Iterator;
import com.bbn.openmap.dataAccess.shape.EsriGraphicList;

/**
 */
public class CaNvFaultLayer extends EsriLayer {

    int nameCol = -1;
    int ageCol = -1;
  
    public CaNvFaultLayer() { }
  
    @Override
    public void setProperties(String prefix, Properties properties) {
  
      super.setProperties(prefix, properties);
      
      prefix = PropUtils.getScopedPropertyPrefix(prefix);
  
      final DbfTableModel model = getModel();
      if (model != null)  {
        final int columnCount = model.getColumnCount();
        String columnName = null;
        for (int column = 0; column < columnCount; column++) {
          columnName = model.getColumnName(column);
          if (columnName.equals("FAULT")) nameCol = column;
          if (columnName.equals("AGE")) ageCol = column;
        }
      }
      setColors(getEsriGraphicList());
      //System.out.println("DEBUG CaNvFaultLayer list size:" + getEsriGraphicList().size());
      //System.out.println("DEBUG CaNvFaultLayer nameCol:" + nameCol);
      //System.out.println("DEBUG CaNvFaultLayer ageCol:" + ageCol);
    }

    @Override
    public String getInfoText(OMGraphic omg) {
      if (nameCol < 0) return null;
      int index = getEsriGraphicList().indexOf(omg);
      if (index >= 0) {
        StringBuffer sb = new StringBuffer(128);
        sb.append( (String) getModel().getValueAt(index,nameCol));
        sb.append(" ");
        sb.append( (String) getModel().getValueAt(index,ageCol));
        return sb.toString();
      }
      /*
      Object appObj = omg.getAppObject();
      if (appObj instanceof ArrayList) {
        ArrayList row = (ArrayList) appObj;
        StringBuffer sb = new StringBuffer(128);
        if (nameCol >= 0 && row.size() > nameCol)
            sb.append((String)row.get(nameCol));
        if (ageCol >= 0 && row.size() > ageCol) {
            sb.append(" ");
            sb.append((String)row.get(ageCol));
        }
        return sb.toString();
      }
      */
      return null;
    }

    @Override
    public String getToolTipTextFor(OMGraphic omg) {
      if (nameCol < 0) return null;
      int index = getEsriGraphicList().indexOf(omg);
      if (index >= 0) return (String) getModel().getValueAt(index,nameCol);
      /*
      Object appObj = omg.getAppObject();
      if (appObj instanceof ArrayList) {
        ArrayList row = (ArrayList) appObj;
        if (nameCol >= 0 && row.size() > nameCol)
            return (String) row.get(nameCol);
      }
      */
      return null;
    }

    private void setColor(EsriGraphicList list) {
        Object appObj = list.getAppObject();
        Iterator it = list.iterator();
        Object element = null;
        while (it.hasNext()) {
          element = it.next();
          if (element instanceof OMGraphic) {
            OMGraphic omg = (OMGraphic) element;
            omg.setAppObject(appObj);
            setColor(omg);
          }
          else if (element instanceof EsriGraphicList) { // list in a list
             EsriGraphicList omg = (EsriGraphicList) element;
             omg.setAppObject(appObj); // set 2nd list data to primary
             setColor((EsriGraphicList) element); // propagate to 2nd list elements
          }
        }
    }
  
    private void setColor(OMGraphic omg) {
        //get the application object which should be the record of dbf file (1..N)
        omg.setLinePaint(drawingAttributes.getLinePaint());
    }
  
    /**
     * Sets the attributes and colors for the layer graphics.
    */
    private void setColors(EsriGraphicList listObj) {
        if (listObj == null) return;
        Iterator it = listObj.iterator();
        while (it.hasNext()) {
          final Object element = it.next();
          if (element instanceof EsriGraphicList) {
            setColor((EsriGraphicList)element);
          }
          else if (element instanceof OMGraphic) {
            setColor((OMGraphic) element);
          }
        }
    }
    protected transient JPanel guiPaletteBox;
    protected final static String ReloadCmd = "ReloadLayer";
    @Override
    public Component getGUI() {
        if (guiPaletteBox == null) {
            guiPaletteBox = new JPanel();
            guiPaletteBox.setLayout(new BoxLayout(guiPaletteBox, BoxLayout.Y_AXIS));
            guiPaletteBox.setAlignmentX(Component.LEFT_ALIGNMENT);

            JPanel jp = new JPanel();
            if (drawingAttributes != null) jp.add(drawingAttributes.getGUI());
            guiPaletteBox.add(jp);
            jp = new JPanel();
            JButton redraw = new JButton(i18n.get(ShapeLayer.class, "redrawLayerButton", "Redraw Layer"));
            redraw.setActionCommand(RedrawCmd);
            redraw.addActionListener(this);
            jp.add(redraw);
            JButton reload = new JButton("Reload Layer");
            reload.setActionCommand(ReloadCmd);
            reload.addActionListener(this);
            jp.add(reload);
            guiPaletteBox.add(jp);
        }
        return guiPaletteBox;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        String cmd = e.getActionCommand();
        if (cmd == RedrawCmd) {
            setColors(getEsriGraphicList());
        }
        else if (cmd == ReloadCmd) {
            if (getList() != null) {
                getList().clear(); // clear old graphics list just in case something tries to repaint it
                setList(null); // so new Shape list will be created by getList() override
            }
        }
        if (isVisible()) doPrepare();
    }

    @SuppressWarnings("deprecation")
    @Override
    public OMGraphicList getList() {
        if (super.getList() == null) {
            try {
                setList(
                    EsriGraphicList.getEsriGraphicList(
                        PropUtils.getResourceOrFileOrURL(shp),
                        PropUtils.getResourceOrFileOrURL(shx),
                        drawingAttributes,
                        getModel()
                    )
                );
            } catch (Exception exception) {
                Debug.error("QFaultLayer(" + getName()
                        + ") exception reading Shape files:\n "
                        + exception.getMessage());
            }
        }
        return super.getList();
    }
  
}
