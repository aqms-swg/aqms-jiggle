package org.trinet.apps;

import org.trinet.jasi.*;
import org.trinet.formats.*;
import org.trinet.jdbc.datasources.AbstractSQLDataSource;
import org.trinet.jiggle.JiggleProperties;
import org.trinet.jiggle.common.JiggleSingleton;
/*
Produce Cube "E " formated string for only seismic events.
Fixed format by column.
If CUBE "E " fields contain null values, 'white' space is inserted.
*/

public class CubeEventLine {

    static String outputString;

    public CubeEventLine() {
    }

    public static void main(String[] args) {

        if (args.length < 5) {
            System.out.println("Args: [user] [pwd] [host.domain] [dbName] [evid] <E, DE, cancelmail, or eventmail) default = E> [port] [jigglePropLocation] (optional)");
            System.exit(0);
        }

        String user = args[0];
        String passwd = args[1];
        String host = args[2];        // default.
        String dbname = args[3];

        long evid = Long.parseLong(args[4]);

        String url;
        String driver;
        if (args.length > 6) { // Testing
            JiggleSingleton.getInstance().setJiggleProp(new JiggleProperties(args[7]));
            url = "jdbc:" + AbstractSQLDataSource.POSTGRESQL_DS_SUBPROTOCOL + "://" + host + ":" + args[6] + "/" + dbname;
            driver = AbstractSQLDataSource.DEFAULT_DS_DRIVER;
        } else {
            JiggleSingleton.getInstance().setJiggleProp(new JiggleProperties());
            url = "jdbc:" + AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL + ":@" + host + ":" + AbstractSQLDataSource.DEFAULT_DS_PORT + ":" + dbname;
            driver = AbstractSQLDataSource.DEFAULT_DS_DRIVER;
        }


        System.out.println("CubeEventLine: making connection... ");
        DataSource ds = new DataSource(url, driver, user, passwd);    // make connection
        System.out.println("CubeEventLine dataSource : " + ds.describeConnection());

        Solution sol = Solution.create().getById(evid);

        if (sol == null) {
            System.out.println("No event found with evid = " + evid);
        } else {
            // Prints out cube format string.
            if (args.length > 5) {
                if (args[5].equalsIgnoreCase("eventmail")) {
                    outputString = CubeFormat.toEmailString(sol, null, ds.getConnection());
                    System.out.println(outputString); //null = version
                } else if (args[5].equalsIgnoreCase("cancelmail")) {
                    outputString = CubeFormat.toCancelEmailString(sol, null, ds.getConnection());
                    System.out.println(CubeFormat.toCancelEmailString(sol, null, ds.getConnection())); //null = version
                } else if (args[5].equalsIgnoreCase("E")) {
                    outputString = CubeFormat.toEventString(sol);
                    System.out.println(outputString);
                } else if (args[5].equalsIgnoreCase("DE")) {
                    outputString = CubeFormat.toDeleteString(sol);
                    System.out.println(outputString);
                } else {
                    System.out.println("Unknown output type input option='" + args[5] + "', value must be 'E', 'DE', 'cancelmail', or 'eventmail'");
                }
            } else {
                System.out.println(CubeFormat.toEventString(sol));
            }
        }
    } // end of main

    public static String getOutputString() {
        return outputString;
    }

} // End of class

