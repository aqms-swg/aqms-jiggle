package org.trinet.waveserver.rt;
public class UnknownTrinetDataTypeException extends RuntimeException {
    UnknownTrinetDataTypeException() {
        super();
    }
    UnknownTrinetDataTypeException(String message) {
        super(message);
    }
}
