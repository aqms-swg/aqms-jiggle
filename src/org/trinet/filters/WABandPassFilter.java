package org.trinet.filters;
import org.trinet.jasi.*;
import org.trinet.util.*;

public class WABandPassFilter extends WAFilter {

    public WABandPassFilter() {
        this(FilterTypes.WOOD_ANDERSON_BP, 0);
    }
  
    public WABandPassFilter(int sampleRate) {
        this(FilterTypes.WOOD_ANDERSON_BP, sampleRate);
    }
  
    public WABandPassFilter(int type, int sampleRate) {
        setApplyTaper(WAFilter.DEFAULT_APPLY_TAPER);
        setSampleRate(sampleRate);
        setDescription(WAFilter.FILTER_NAME+"_BP_.5_9.99_6");
        this.type = FilterTypes.WOOD_ANDERSON_BP; // input type not relevant only one type 
    }
  
    public FilterIF getFilter(String type) {
        return new WABandPassFilter();
    }
  
    public FilterIF getFilter(int sampleRate) {
        WABandPassFilter f = (WABandPassFilter) this.clone(); // uses current type settings
        f.setSampleRate(sampleRate);
        return f; 
    }
  
    public void setSampleRate(int sampleRate) {
        super.setSampleRate(sampleRate);
        if (sampleRate != 0) {
          if (noiseFilter == null) noiseFilter = ButterworthFilterSMC.createBandpass(sampleRate, 0.5, 9.99, 6, true); // subtracted 0.01 from hi freq to allow 20sps data
          else noiseFilter.setSampleRate(sampleRate);
        }
    }

    public FilterIF getFilter(int type, int sampleRate) {
        return new WABandPassFilter(type, sampleRate);
    }
  
    public Waveform filter(Waveform wf) {
        if (super.setWaveform(wf) == null) return null;
        if (noiseFilter == null) {
            noiseFilter = ButterworthFilterSMC.createBandpass((int)wf.getSampleRate(), 0.5, 9.99, 6, true); // subtracted 0.01 from hi freq to allow 20sps data
            noiseFilter.copyInputWaveform(false);
        }
        // Note for jiggle need to copy the input wf else original is changed (ie. in wfview in masterview -aww 02/28/2007)
        return super.filter(noiseFilter.filter(super.getWaveform()));
    }

} // end of class
