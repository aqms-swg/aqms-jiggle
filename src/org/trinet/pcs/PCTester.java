package org.trinet.pcs;
import java.io.*;
import org.trinet.jasi.*;

public class PCTester {
/*
  public static void main(String args []) {
    if (args.length < 4) {
        System.out.println("SYNTAX: dbhostname dbname user password [true=delete (T)] [printSQL (F)]");
        System.exit(0);
    }
    System.out.println("Test of PCS datasource on " + args[0]);
    DataSource ds = TestDataSource.create(args[0], args[1], args[2], args[3]);
    System.out.println("DataSource: " + ds.toDumpString());

    // By default "delete postings" and don't print out the row SQL text
    boolean deletePostings = (args.length < 5 || args[4].equalsIgnoreCase("true"));
    System.out.println("DeletePostings: " + deletePostings);
    boolean printSQL = (args.length > 5 && args[5].equalsIgnoreCase("true"));
    System.out.println("DebugSQL: " + printSQL);
    ProcessControl.debug = printSQL;
    //
    // Create Transition and State rows for method tests
    //
    TransitionRow tr = new TransitionRow();
    tr.debug = printSQL;
    tr.groupOld = "A";   // NOT NULL
    tr.sourceOld = "B";  // NOT NULL
    tr.stateOld = "C";   // NOT NULL
    tr.resultOld = 1;    // NOT NULL?
    tr.groupNew = "A";   // NOT NULL
    tr.sourceNew = "B";  // NOT NULL
    tr.stateNew = "D";   // NOT NULL
    tr.rankNew = 10;
    tr.resultNew = 0;
    tr.auth = "CI";
    tr.subsource = "test";

    StateRow sr = new StateRow();
    sr.debug = printSQL;
    sr.id = 1l;
    sr.controlGroup = "A";
    sr.sourceTable = "B";
    sr.state = "C";
    sr.rank = 5;
    sr.result = 0;
    sr.secondaryId = 0l;
    sr.secondarySourceTable = "DUMMY";
    sr.auth = "CI";
    sr.subsource = "test";

    PCTester.rowTester(sr, tr);

    //
    // Transitions via ProcessControl method test
    //
    System.out.println("...Inserting state row...");
    sr.insert(); // insert stateRow
    System.out.println("... Inserting test row transition via ProcessControl...");
    tr.insert(); // insert its transition
    System.out.println("...Resulting id 1 A,B,C to 1 should go to D.");
    ProcessControl.putResult("A", "B", 1l, "C", 1); // result should transition to "D"
    System.out.println("...ProcessControl.putState(A,B,C,7,10,1), should go to D.");
    ProcessControl.putState("A", "B", 7, "C", 10, 1); // result should transition to "D"
    System.out.println("...ProcessControl.putState(A,B,C,8,10,2), should NOT go to D.");
    ProcessControl.putState("A", "B", 8, "C", 10, 2); // result should not transition to "D"
    //
    StateRow [] s = sr.get( "A", "B", "C");
    assert( s != null && s.length == 1 ) : "ProcessControl.putState with result wrong cnt: " + s.length;
    sr.dump(s); // should be 8 in "D" state

    //ProcessControl.doTransitions("A","B","C"); // do transitions for state
    //System.out.println("...Transition StateRow... A B C should now be: A B D 10");

    // Should now only have the "D" StateRow, "C" should be gone.
    s = sr.get( "A", "B", "D") ;
    sr.dump(s); // should be 1 and 7 ids in "D" state
    assert( s != null && s.length == 2) : " Transitions should delete 2/ create 2 rows";
    assert( s[0].state.equals("D")) : "row state should be D state";
    assert( tr.delete() == 1) : "should delete the C to D transition row";

    s[0].delete(); // now delete new "D" row

    s = sr.get("A","B","D"); // confirm "D" was deleted
    assert (s == null || s.length == 1 ) : "sr.get should not return 2 D state rows";

    s = sr.get("A","B","C"); // transition above should have left one "C" state
    assert (s == null || s.length == 1 ) : "sr.get should return 1 C state";

    System.out.println("...Reinsert the original row...");
    sr.insert(); // reinsert  the original row
    System.out.println("...DUPLICATION test for insert of the original row, should be no-op, no update...");
    sr.insert(); // DUPLICATION insert  the original row for no-op test
    // RECYCLE TEST
    System.out.println("...test ProcessControl.putResult(A, B, 1l, C, 1)...");
    ProcessControl.putResult("A", "B", 1l, "C", 1); // ABC.1.1
    // TEST no-op update

    // insert another row with different id,result for same state description
    System.out.println("...Reinsert the row with id 2...");
    StateRow sr2 = new StateRow(sr);
    sr2.id = 2l;
    sr2.result = 1;
    sr2.insert(); // ABC.2.1

    // insert 3rd row with different id,result for same state description
    System.out.println("...Reinsert the row for with id 3...");
    sr2.id = 3l;
    sr2.result = 3;
    sr2.insert(); // ABC.3.3

    // Now test recycle for a result = 2, should be 1 row id 8
    System.out.println("...Recycle A B C 2 to Recycle Test recycle 150");
    s = sr.get( "A", "B", "C") ;
    sr.dump(s); // should be id 8 in "C" state
    int result = ProcessControl.recycle("A","B","C", 2, "Recycle", "Test", "recycle", 150);
    assert (result == 1) : "recycle result must be 1 such row";


    // test recycle with a result = 1, should be 2 rows id 1 and 2
    System.out.println("...Recycle A B C 1 to Recycle Test recycle 150");
    result = ProcessControl.recycle("A", "B", "C", 1, "Recycle", "Test", "recycle", 150);
    assert (result == 2) : "recycle result must be 2 rows";

    // test recycle removed recycle state rows, should be 2 rows id 1 and 2
    System.out.println("...Recycle Recycle Test recycle 0 to NULL");
    result = ProcessControl.recycle("Recycle", "Test", "recycle", 0, null, null, null, 0);
    assert (result == 3) : "recycle result must be 3";
    assert (sr.delete("Recycle") == 0) : "sr.delete should not find any recycle state";
    s = sr.get( "A");
    sr.dump(s); // should be id 3 and 7, two rows  in "A" group
    assert (sr.delete("A") == 2) : "sr.delete should deleted two rows in 'A' group (ids 3 and 7)";
    //
    // test other ProcessControl methods
    //
    final String grp = "TestProc";
    final String tbl = "Tester";
    final String st = "test";
    final long id = 123;
    final int rank = 10;

    boolean status =  ProcessControl.hasPosting(grp, tbl, st, id) ;
    assert (status == false) : "hasPosting("+grp+","+tbl+","+st+","+id+") must be false";

    // Return -1 = id not posted; 0 = old result; 1 = ready;  >1 = rank blocked.
    result = ProcessControl.runStatusOf(grp, tbl, st, id) ;
    assert (result==-1) : "runStatusOf: result must be -1";

    System.out.println("...Now posting id 123 for TestProc Tester test 100");
    ProcessControl.putState(grp, tbl, id, st, rank);
    StateRow.dump(StateRow.get(grp, tbl, st));

    System.out.println("...Testing ProcessControl.hasPosting ...");
    status = ProcessControl.hasPosting(grp, tbl, st, id) ;
    assert (status == true) : "hasPosting(grp, tbl, st, id) must be true";
    System.out.println("...Testing ProcessControl.runStatusOf ...");
    result = ProcessControl.runStatusOf(grp, tbl, st, id) ;
    assert (result == 1) : "runStatusOf: ready, result must be 1";
    System.out.println("...Testing ProcessControl.isReady ...");
    status = ProcessControl.isReady(grp, tbl, st, id) ;
    assert (status == true) : "isReady(grp, tbl, st, id) must be true";
    System.out.println("...Testing ProcessControl.hasResult ...");
    status = ProcessControl.hasResult(grp, tbl, st, id) ;
    assert (status == false) : "hasResult(grp, tbl, st, id) must be false";
    System.out.println("...Testing ProcessControl.isRankBlocked ...");
    status = ProcessControl.isRankBlocked(grp, tbl, st, id) ;
    assert (status == false) : "isRankBlocked(grp, tbl, st, id) must be false";
    System.out.println("...Testing ProcessControl.getResultFor ...");
    Integer ires= ProcessControl.getResultFor(grp, tbl, st, id) ;
    assert (ires != null && ires.intValue() == 0) : "getResultFor(grp, tbl, st, id) 0 = "
            + ((ires == null) ? "NULL" : ires.toString());

    System.out.println("...Testing ProcessControl.putResult for id 123 to 1");
    ProcessControl.putResult(grp, tbl, id, st, 1);

    System.out.println("...Testing ProcessControl.runStatusOf ...");
    result = ProcessControl.runStatusOf(grp, tbl, st, id) ;
    assert (result==0) : "runStatusOf: old, result must be 0";

    System.out.println("...Testing ProcessControl.has xxx Result ...");
    status = ProcessControl.hasResult(grp, tbl, st, id) ;
    assert (status == true) : "hasResult(grp, tbl, st, id) must be true";
    status = ProcessControl.hasNegativeResult(grp, tbl, st, id) ;
    assert (status == false) : "hasNegativeResult(grp, tbl, st, id) must be false";
    status = ProcessControl.hasPositiveResult(grp, tbl, st, id) ;
    assert (status == true) : "hasPositiveResult(grp, tbl, st, id) must be true";

    System.out.println("...Testing ProcessControl.getResultFor ...");
    ires= ProcessControl.getResultFor(grp, tbl, st, id) ;
    assert (ires != null && ires.intValue() == 1) : "getResultFor(grp, tbl, st, id) 1 = "
            + ((ires == null) ? "NULL" : ires.toString());

    ires= ProcessControl.getResultFor("JUNK", "junk", "junk", 123l) ;
    assert (ires == null) : "getResultFor('JUNK', tbl, st, id) must be NULL";

    // Rank block test, reinsert test row for different state ranks
    System.out.println("...Posting id 123 for TestProc Tester test 100");
    ProcessControl.putState(grp, tbl, id, st, rank);
    System.out.println("...Testing ProcessControl.isRankBlocked ...");
    status = ProcessControl.isRankBlocked(grp, tbl, st, id) ;
    assert (status == false) : "isRankBlocked(grp, tbl, 'test', id) false";

    System.out.println("...Posting id 123 for TestProc Tester BLOCKER 1000");
    ProcessControl.putState(grp, tbl, id, "BLOCKER", 1000);
    System.out.println("...Testing ProcessControl.isRankBlocked ...");
    status = ProcessControl.isRankBlocked(grp, tbl, "BLOCKER", id) ;
    assert (status == false) : "isRankBlocked(grp, tbl, 'BLOCKER', id) false";
    status = ProcessControl.isRankBlocked(grp, tbl, st, id) ;
    assert (status == true) : "isRankBlocked(grp, tbl, 'test', id) true";

    System.out.println("...Posting id 123 for TestProc Tester BLOCKER_HIGHER 10000");
    ProcessControl.putState(grp, tbl, id, "BLOCKER_HIGHER", 10000);
    System.out.println("...Testing ProcessControl.isRankBlocked ...");
    result = ProcessControl.runStatusOf(grp, tbl, "test", id) ;
    assert (2 == result) : "runStatusOf: ranked blocked must be result 2";

    System.out.println("...Dump of all state postings for id 123: ");
    s = StateRow.get(123l);
    StateRow.dump(s);

    // Post from file test
    System.out.println("...Writing the test posting rows to file:C:\\TEMP\\testpost.txt");
    BufferedWriter fw =  null;
    File file = null;
    try {
      file = new File("C:\\TEMP\\testpost.txt");
      if (! file.exists()) {
        fw =  new BufferedWriter(new FileWriter(file));
        fw.write("1 cat 1 TestProc Tester");
        fw.newLine();
        fw.write("2 dog 10 TestProc Tester");
        fw.newLine();
        fw.write("3 mouse 100 TestProc Tester");
        fw.newLine();
        fw.write("4 pig 1000 TestProc Tester");
        fw.newLine();
      }
    }
    catch (IOException ex) {
        ex.printStackTrace();
    }
    finally {
      try {
        if (fw != null) fw.close();
      }
      catch (IOException ex) {
      }
    }
    System.out.println("...Testing ProcessControl.postFromFile "+file.toString());
    result = ProcessControl.postFromFile(file.toString());
    s = StateRow.get(grp,tbl);
    StateRow.dump(s);
    //
    System.out.println("\n...Testing of StateRow.getNegativeResults() ... " );
    ProcessControl.putResult(grp, tbl, id, "test", -1);
    ProcessControl.putResult(grp, tbl, id, "BLOCKER", -2);
    ProcessControl.putResult(grp, tbl, id, "BLOCKER_HIGHER", -3);
    s = StateRow.getNegativeResults();
    StateRow.dump(s);
    //
    if (deletePostings) {
        System.out.println("...Testing deletion of all postings for : " + grp + " " + tbl);
        StateRow.delete(grp, tbl);
    }
    //
    System.out.println("\n...Inserting PCS_STATE rows 5,6,7 A.B.C.10.0) ... " );
    // First add some rows to state table
    ProcessControl.putState("A", "B", 5, "C", 10);
    ProcessControl.putState("A", "B", 6, "C", 10);
    ProcessControl.putState("A", "B", 7, "C", 10);

    // Get next id for state, should be none since lag 10 secs
    long evid = ProcessControl.getNextId("A", "B", "C", 10);
    System.out.println(" Test lag of 10 secs for ProcessControl.getNextId(A, B, C, 10) == 0  => " + evid);
    assert( evid == 0) : "ProcessControl.getNextId(A, B, C, 10) must be 0 !"; 

    s = sr.get( "A", "B", "C");
    StateRow.dump(s);
    // Result them
    System.out.println("\n...Testing of ProcessControl.putResultAll(A,B,C,2) ... " );
    ProcessControl.putResultAll("A","B","C", 2);
    s = sr.get( "A", "B", "C");
    StateRow.dump(s);
    assert( s != null && s.length == 3 ) : "PC.putResultAll A,B,C rows must be 3";
    // Now test transitions
    tr.insert(); // insert its transition
    ProcessControl.putResultAll("A","B","C", 1); // invokes ProcessControl.doTransitions("A","B","C");
    s = sr.get( "A", "B", "C");
    StateRow.dump(s);
    assert( s == null ) : "PC.putResultAll A,B,C rows must be 0";
    s = sr.get( "A", "B", "D");
    StateRow.dump(s);
    assert( s != null && s.length == 3 ) : "PC.putResultAll A,B,D rows must be 3";
    tr.delete(); // delete its transition

    if (deletePostings) {
        System.out.println("...Testing deletion of all postings for : " + grp + " " + tbl);
        StateRow.delete("A", "B");
    }
    //

  }

  protected static final void rowTester(StateRow sr, TransitionRow tr) {

    System.out.println("...Dumping Test TransitionRow...");
    String tst = tr.toOutputString();
    System.out.println(tst);

    System.out.println("...Inserting TransitionRow...");
    assert (tr.insert() == 1) : "  insert failed";

    System.out.println("...Updating TransitionRow...");
    assert (tr.insert() == 1) : "  update failed";

    System.out.println("...Get TransitionRow matching G,T,S and result ...");
    TransitionRow[] t = tr.get("A", "B", "C", 1);
    assert (t != null && t.length == 1 && tst.equals(t[0].toOutputString())) : "tr.get returned incorrect row array";

    System.out.println("...Get TransitionRow By Group, Table, and State ...");
    t = tr.get("A", "B", "C");
    assert (t != null && t.length == 1 && tst.equals(t[0].toOutputString())) : "tr.get returned incorrect row array";

    System.out.println("...Get TransitionRow By Group and Table ...");
    t = tr.get("A", "B");
    assert (t != null && t.length == 1 && tst.equals(t[0].toOutputString())) : "tr.get returned incorrect row array";

    System.out.println("...Get TransitionRow By Group...");
    t = tr.get("A");
    assert (t != null && t.length == 1 && tst.equals(t[0].toOutputString())) : "tr.get returned incorrect row array";

    System.out.println("...Get ALL TransitionRows AND DUMP ...");
    t = tr.get();
    tr.dump(t);

    // Now test StateRow methods

    tst = sr.toOutputString();
    System.out.println("...Test StateRow: " + tst);

    System.out.println("...Inserting StateRow...");
    assert (sr.insert() == 1) : "  insert failed";

    System.out.println("...Updating StateRow...");
    assert (sr.insert() == 1) : "  update failed";

    System.out.println("...Get StateRow ALL ...");
    StateRow[] s = sr.get();
    sr.dump(s);
    int count = 0;
    String tst2 = "";
    System.out.println("...Get rows for test id 1 ...");
    if (s != null) {
      for (int i = 0; i < s.length; i++) {
          if (s[i].id == 1l) {
              count++;
              tst2 = s[i].toOutputString();
          }
      }
    }
    assert (count == 1 && tst.equals(tst2)) : "sr.get returned incorrect row array";

    System.out.println("...Get StateRow KEY ...");
    s = sr.get( "A",  "B",  "C", 1l, true) ;
    assert (s != null && s.length == 1 && tst.equals(s[0].toOutputString())) : "sr.get returned incorrect row array";

    System.out.println("...Get StateRow GRP ...");
    s = sr.get( "A") ;
    assert (s != null && s.length == 1 && tst.equals(s[0].toOutputString())) : "sr.get returned incorrect row array";

    System.out.println("...Get StateRow GRP,TBL ...");
    s = sr.get( "A",  "B") ;
    assert (s != null && s.length == 1 && tst.equals(s[0].toOutputString())) : "sr.get returned incorrect row array";

    System.out.println("...Get StateRow GRP,TBL,ST ...");
    s = sr.get( "A",  "B",  "C") ;
    assert (s != null && s.length == 1 && tst.equals(s[0].toOutputString())) : "sr.get returned incorrect row array";

    System.out.println("...Get StateRow GRP,TBL,ID ...");
    s = sr.get( "A",  "B", 1l) ;
    assert (s != null && s.length == 1 && tst.equals(s[0].toOutputString())) : "sr.get returned incorrect row array";

    StateRow sr2 = new StateRow(sr);
    sr2.state = "C2";
    sr2.insert();
    System.out.println("...Get StateRow ID ...");
    s = StateRow.get(1l);
    sr.dump(s); // should be 2 rows: "C" and "C2" state
    assert (s != null && s.length == 2) : "sr.get(id) should return 2 rows for id";

    System.out.println("...Get NEXT StateRow GRP,TBL,ST ...");
    sr2 = sr.getNext( "A",  "B",  "C");
    System.out.println("sr2 = " + ((sr2 ==null) ? "null" : sr2.toOutputString()));
    assert (sr2 != null && tst.equals(sr2.toOutputString())) : "sr.getNext returned incorrect row array";

    System.out.println("...Get NEXT id GRP,TBL,ST ...");
    assert (sr.getNextId( "A",  "B",  "C")  == 1l) : "Should have returned id of 1";

    System.out.println("...Get negative results ...");
    s = sr.getNegativeResults() ;
    assert (s == null || s.length == 0) : "sr.getNegativeResults returned incorrect row array";

    System.out.println("...Get non-null results ...");
    s = sr.getNonNullResults(true) ;
    System.out.println("...All non-null non 0 results for all states: " + ((s == null) ? 0 : s.length));

    s = sr.getNonNullResults( "A",  "B", true) ;
    System.out.println("...A,B non-null results=0 : " + ((s == null) ? 0 : s.length));
    assert (s == null || s.length == 0) : "sr.getNonNullResults returned incorrect row array length:" + s.length;

    System.out.println("...Test StateRow.canProcess(GRP,TBL,ST,1) == true, it's posted");
    assert(sr.canProcess( "A",  "B",  "C", 1l) == true) : "Should have returned true";

    System.out.println("...Test StateRow.canProcess(GRP,TBL,ST,88) == false, no such posting");
    assert(sr.canProcess( "A",  "B",  "C", 88l) == false) : "Should have returned false";

    // Same id, different state, higher rank
    sr2 = new StateRow(sr);
    sr2.id = 1l;
    sr2.state = "C3";
    sr2.rank = 500;
    sr2.insert();

    System.out.println("...Test StateRow.canProcess(GRP,TBL,ST,1) == false, it's RANK BLOCKED");
    assert(sr.canProcess( "A",  "B",  "C", 1l) == false) : "Should have returned false";

    System.out.println("...Delete StateRow...");
    sr.delete( "A",  "B", 1l) ;
    sr.delete( "A",  "B") ;
    sr.delete( "A") ;
    sr.delete() ;
    s = sr.get( "A");
    assert (s == null || s.length == 0 ) : "sr.delete delete, should result in no row array";

    System.out.println("...Delete TransitionRow...");
    tr.delete("A", "B", "C");
    System.out.println("...Delete TransitionRow...");
    tr.delete("A", "B");
    System.out.println("...Delete TransitionRow...");
    tr.delete("A");
    System.out.println("...Delete TransitionRow...");
    tr.delete();
    t = tr.get( "A");
    assert (t == null || t.length == 0 ) : "tr.get delete, should result in no row array";
  }
  */
}
