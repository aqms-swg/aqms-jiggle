package org.trinet.jasi.EW;

import java.sql.*;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jasi.TN.AmplitudeTN;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

//
// For now convenience to compile just extend TN class
// still need to implement abstract Amplitude methods here -aww
//
public class AmplitudeEW extends AmplitudeTN
{

    public AmplitudeEW()
    {
    }
    public Collection getByTime(Connection conn, double start, double stop)
    {
      /**@todo: implement this org.trinet.jasi.Amplitude abstract method*/
      return AmpList.create();
    }
    public Collection getByMagnitude(long parm1, String[] parm2)
    {
      /**@todo: implement this org.trinet.jasi.Amplitude abstract method*/
      return AmpList.create();
    }
    public Collection getBySolution(Solution sol)
    {
      /**@todo: implement this org.trinet.jasi.Amplitude abstract method*/
      return AmpList.create();
    }
    public Collection getBySolutionNets (long id, String[] netList) {
      /**@todo: implement this org.trinet.jasi.Amplitude abstract method*/
      return AmpList.create();
    }
    public Collection getByMagnitude(Magnitude mag)
    {
      /**@todo: implement this org.trinet.jasi.Amplitude abstract method*/
      return AmpList.create();
    }
    public boolean commit()
      {
        /**@todo: implement this org.trinet.jasi.JasiReading abstract method*/
        return(false);
    }
    public Collection getByTime(double start, double stop)
    {
      /**@todo: implement this org.trinet.jasi.Amplitude abstract method*/
      return AmpList.create();
    }
    public JasiCommitableIF getById(long id)
    {
      /**@todo: implement this org.trinet.jasi.Amplitude abstract method*/
      return(null);
    }
    public Collection getByMagnitude(long magid)
    {
      /**@todo: implement this org.trinet.jasi.Amplitude abstract method*/
      return AmpList.create();
    }
    public Collection getByMagnitude(Magnitude parm1, String[] parm2)
    {
      return AmpList.create();
      /**@todo: implement this org.trinet.jasi.Amplitude abstract method*/
    }
    public Collection getBySolution(long parm1, String[] parm2)
    {
      /**@todo: implement this org.trinet.jasi.Amplitude abstract method*/
      return AmpList.create();
    }
    public Collection getBySolution(long id)
    {
      /**@todo: implement this org.trinet.jasi.Amplitude abstract method*/
      return AmpList.create();
    }
}
//
// end of AmplitudeEW
//
