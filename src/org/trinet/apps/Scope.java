package org.trinet.apps;

import org.trinet.jiggle.*;
import java.awt.*;
import java.io.File;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;

import org.trinet.jasi.*;
import org.trinet.util.*;
import org.trinet.formats.*;
import org.trinet.jdbc.datasources.*;

/**
 * GUI to allow viewing of current data for a set of channels.
 *
 * Usage: Scope <seconds> "<nt.sta.chan nt.sta.chan ...>"
 */

public class Scope extends JFrame implements WaveformDisplayIF {

    static final String versionNumber  = Version.buildTag; // "2004.02.07";  -- replaced aww

    private TimeSpan ts = null;
    private double oldDur = 0.;
    private MasterView mv = null; // new MasterView();
    private WFScroller wfScroller = null; // new WFScroller();
    private ZoomPanel zpanel = null; // new ZoomPanel();
    private JSplitPane waveSplit = null; // new JSplitPane();
    private ChannelFinderTextField chanFinder = null;

    private ChannelableList chanList = new ChannelableList();

    private double duration = 120.;

    private int panelsInView = 10;
    private int secsInView = 60;

    private boolean absMode = false;
    private double absStartTime = 0.;
    private double absEndTime = 0.;

    public static boolean debug = true;
    public static Scope scope;
    private static JiggleProperties props;
    private static String propertyFileName = "scope.props";

    public Scope() { }

    public WFScroller getWFScroller() {
        return wfScroller;
    }

    public ZoomPanel getZoomPanel() {
        return zpanel;
    }

    public SolutionWfEditorPropertyList getWfPanelProperties() {
        return (SolutionWfEditorPropertyList) props;
    }

    public static void main(String args[]) {
        //System.out.println("SCOPE: args count: " + args.length);

        // create an instance else everything must be 'static'
        scope = new Scope();

        if (args.length == 1 && (args[0].equalsIgnoreCase("help") || args[0].equalsIgnoreCase("?")) )  {
          System.out.println("Usage: Scope <spanSeconds> \"<nt.sta.chan nt.sta.chan ...>\" [startTime endTime]");
          System.out.println("Usage: Scope <spanSeconds> @filename [startTime endTime]");
          System.out.println("Usage: optional start and end times are of form yyyy-MM-dd HH:mm:ss.SSS"); 
          System.out.println("Usage: if absent, window end is the current time and window start is endTime-spanSeconds"); 
          System.exit(0);
        }

        // must do this to establish dbase connection before reading in channel list
        if (! scope.setupProperties() ) {
          System.out.println("Problem with properties.");
          scope.stop();
        }

        // 1st arg is duration of data capture
        if (args.length > 0 && (!scope.parseDuration(args[0]) || scope.duration <= 0.) ) {
            System.out.println("Input duration must be > 0.");
            scope.stop();
        }
        System.out.println("SCOPE: duration: " + scope.duration);

        if (args.length > 1) {
          System.out.println("SCOPE: channels: " + args[1]);
          // 2nd arg parse the sta list and make a ChannelList
          String chanFilename = null;
          if (args[1].startsWith("@")) { // Read from a file
            if (args[1].equals("@?")) {
                chanFilename = JOptionPane.showInputDialog(null, "Enter name of station channel file", "Scope Channels", JOptionPane.PLAIN_MESSAGE);
            }
            else chanFilename = args[1].substring(1);

            scope.makeChannelList(chanFilename);
            int cnt = (scope.chanList == null) ? 0 : scope.chanList.size();
            System.out.println("Read " + cnt + " channels from " + chanFilename);
            if (cnt == 0) scope.stop();
            // read from command string
          } else {
              if (!scope.parseChannelList(args[1])) {
                  scope.stop();
              }
          }
        }
        String dtime = null;
        if (args.length > 2)  {
            // time span                   "yyyy-MM-dd HH:mm:ss.SSS"
            dtime = args[2];
            if ( dtime.matches("^.* \\d\\d:\\d\\d$") ) dtime = dtime + ":00.000";
            else if ( dtime.matches("^.* \\d\\d:\\d\\d:\\d\\d$") ) dtime = dtime + ".000";
            System.out.println("SCOPE: start time: " + dtime);
            scope.absStartTime = LeapSeconds.stringToTrue(dtime);
            //System.out.println("SCOPE: start time seconds: " + scope.absStartTime);
            if (args.length > 3) {
                dtime = args[3];
                if ( dtime.matches("^.* \\d\\d:\\d\\d$") ) dtime = dtime + ":00.000";
                else if ( dtime.matches("^.* \\d\\d:\\d\\d:\\d\\d$") ) dtime = dtime + ".000";
                System.out.println("SCOPE: end   time: " + dtime);
                scope.absEndTime   = LeapSeconds.stringToTrue(dtime);
                //System.out.println("SCOPE: end   time seconds: " + scope.absEndTime);
                if (scope.absStartTime >= scope.absEndTime) {
                    System.err.println("Error: Input start absolute time after end time");
                    scope.stop();
                }
            }
            else scope.absEndTime = scope.absStartTime + scope.duration;
            scope.absMode = true;
        }
        System.out.println("SCOPE: time span: " + new TimeSpan(scope.absStartTime,scope.absEndTime));

        scope.startup();
    }

    private boolean parseDuration(String dur) {
        boolean status = true;
        try {
            scope.duration = Double.parseDouble(dur);
        }
        catch (NumberFormatException ex) {
            status = false;
            System.out.println(ex.getMessage());
        }
        return status;
    }

    private boolean parseChannelList(String chanStr) {
        StringTokenizer strTok = new StringTokenizer(chanStr, " ,\t\n\r\f");
        Channel ch = null;
        ChannelName cn = null;
        boolean status = true;
        try {
            while (strTok.hasMoreElements()) {
                cn = ChannelName.fromDelimitedString(strTok.nextToken(), ".");
                ch = Channel.create();
                ch.setChannelName(cn);
                chanList.add(ch.getChannelObj());
                 if (debug) System.out.println (cn.toString());
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            status = false;
        }
        return status;
    }

    private boolean makeChannelList(String chanFilename) {
          File file = null;
          if (chanFilename == null) return false;
          try {
            file = new File(chanFilename);
            if (!file.isAbsolute() && !file.exists()) {
                chanFilename = props.getUserFilePath()+GenericPropertyList.FILE_SEP+chanFilename;
                file = new File(chanFilename);
            }
            System.out.println("Reading channels from file: "+file);
            scope.chanList = NSCLformat.readInFile(file);
          } catch (Exception ex) {
            System.err.println("Error on read of: "+file);
            System.err.println(ex.getMessage());
            return false;
          }
          return (scope.chanList != null);
    }

    private void startup() {

        final JTextField durField = new JTextField(6);
        durField.setText(String.valueOf(duration));
        durField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                parseDuration(durField.getText()); 
                refreshGUI(false);
            }
        });
        JLabel durLabel = new JLabel(" Secs ");

        JButton refreshButton = new JButton("REFRESH");
        refreshButton.setToolTipText("Reload new time window relative to current time");
        refreshButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                refreshGUI(true);
            }
        });

        JButton nextButton = new JButton("RELOAD");
        nextButton.setToolTipText("Reload time window with no change in end time");
        nextButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                refreshGUI(false);
            }
        });

        JButton chanButton = new JButton("CHANNEL");
        chanButton.setToolTipText("New string of SNCL names");
        chanButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String list = JOptionPane.showInputDialog(null, "Type a list of NET.STA.SEEDCHAN to add",
                            "Scope Load Channels", JOptionPane.PLAIN_MESSAGE);
                if (list != null && list.length() > 0 && parseChannelList(list)) {
                    refreshGUI(true);
                }
            }
        });

        JButton fileButton = new JButton("FILE");
        fileButton.setToolTipText("Enter name of file containing parseable list of channel names");
        fileButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String filename = JOptionPane.showInputDialog(null, "Type name station file to load (1 channel per line)",
                            "Scope Load File", JOptionPane.PLAIN_MESSAGE);
                if (filename != null && filename.length() > 0 && makeChannelList(filename)) {
                    refreshGUI(true);
                }
            }
        });
        
        JButton clearButton = new JButton("CLEAR");
        clearButton.setToolTipText("Clear current channel list of all channels");
        clearButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                chanList.clear();
                refreshGUI(true);
            }
        });

        // make a main frame
        setTitle("Scope v."+ versionNumber);

        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(
                new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        stop();
                    }
                }
        );

        chanFinder = new ChannelFinderTextField(new MasterView());

        //getContentPane().add(waveSplit, BorderLayout.CENTER);     // add splitPane to frame
        JPanel bottomPanel = new JPanel();
        bottomPanel.add(new JLabel("Find: "));
        bottomPanel.add(chanFinder);
        bottomPanel.add(durLabel);
        bottomPanel.add(durField);
        bottomPanel.add(refreshButton);
        if (!absMode) bottomPanel.add(nextButton);
        bottomPanel.add(chanButton);
        bottomPanel.add(fileButton);
        bottomPanel.add(clearButton);
        getContentPane().add(bottomPanel, BorderLayout.SOUTH );
        pack();
        setVisible(true);

        int height = 900;
        int width  = 900;
        setSize(width, height);   // must be done AFTER setVisible

        // get and display the data
        refreshGUI(true);
    }

    public void stop() {
        try {
            System.out.println("Disposing Scope frame ...");
            dispose(); // release window resources
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        if (! DataSource.isClosed()) DataSource.close(); // close existing connection, if open
        System.exit(0);
    }
 
    private boolean setupProperties() {
        // read props file to get waveserver
        //props = new JiggleProperties("scope.props");
        //props = new JiggleProperties("properties");       
        props = new JiggleProperties();
        props.setFilename(propertyFileName);
        boolean status = props.initialize("jiggle", propertyFileName, null);
        if ( ! status) {
          String str = getPropertyFileStatusFor(props, "Properties file error,  Enter 'YES' to continue.");
          System.err.println(str);
          int yn = JOptionPane.showConfirmDialog( Scope.this,
                         str, "Scope Properties Initialization",
                         JOptionPane.YES_NO_OPTION
                   );
           if (yn != JOptionPane.YES_OPTION) return false; // bail
        }

        System.out.println("Reading properties from "+props.getUserFilePath()+"/"+props.getFilename());

        if (props.getBoolean("verbose") || props.getBoolean("debug")) props.dumpProperties();
       
        System.out.println("Making connection...");
        //if (!  makeDataSourceConnection() ) return false;
        makeDataSourceConnection();

        WaveServerGroup waveClient = null;
        try {
          // Make a WaveClient
          waveClient = props.getWaveServerGroup();

          if (waveClient == null || waveClient.numberOfServers() <= 0) {
            System.err.println(
               "getDataFromWaveServer Error: no or bad waveservers in properties file. " );
            System.err.println("waveServerGroupList = " + props.getProperty("waveServerGroupList"));
            return false;
          }

          System.err.println("waveServerGroupList = " + props.getProperty("waveServerGroupList"));
          AbstractWaveform.setWaveDataSource(waveClient);

        } catch (Exception ex) {
          System.err.println(ex.toString());
          //ex.printStackTrace();
        }

       // load candidate channel list
       String channelGroupName = "RWP_BASE";
       Channel ch = Channel.create();
       ch.getNamedChannelList(channelGroupName);

       return true;
    }

    private String getPropertyFileStatusFor(GenericPropertyList gpl, String msg) {
          StringBuffer sb = new StringBuffer(256);
          sb.append("Input properties file status:\n");
          sb.append(" User   : ").append(gpl.getUserPropertiesFileName());
          sb.append(" --> exists: ").append(gpl.hasUserPropertiesFile()).append("\n");
          if (msg != null) {
            sb.append(msg);
          }
          return sb.toString();
    }

    /**
      * Make the actual connection to the data source. Returns 'true' on success.
      */
     private boolean makeDataSourceConnection() {
         // get current data source description from properties
         boolean statusOk = true;
         DbaseConnectionDescription dbDesc = props.getDbaseDescription();

         if (DataSource.isNull()) {

           new DataSource(dbDesc.getURL(), dbDesc.getDriver(), dbDesc.getUserName(), dbDesc.getPassword());
           statusOk = ! (DataSource.isNull() || DataSource.isClosed());
         }
         else {
           statusOk = DataSource.set(dbDesc);
         }


         if (! statusOk) {
           System.err.println("Database Connection failure to:\n URL    = " + dbDesc.getURL() +
                              "\n driver = " + dbDesc.getDriver() +
                              "\n user   = " + dbDesc.getUserName()
                              );
           String err = DataSource.getConnectionStatus();
           String msg = "DataSource could not be opened.\n"+
                  err + "\n"+
                  "Check parameters.\n"+
                  "Host = "+ DataSource.getHostName() + "\n"+
                  "Dbase = "+ DataSource.getDbaseName() + "\n"+
                  "Username = " + DataSource.getUserName()+ "\n"+
                  "Port #: "+ DataSource.getPort();
           //popInfoFrame("Dbase Connection Failure", msg);
           System.err.println( msg);
           return false;
         }

         props.setProperty("lastLoginDbURL", dbDesc.getURL());
         if (props.getProperty("writeBackEnabled") != null) {
           DataSource.setWriteBackEnabled(props.getBoolean("writeBackEnabled"));
         }

         if (DataSource.isReadOnly()) {
           String msg = "You have READ-ONLY access to this database\n"+
                        "URL = "+ DataSource.getHostName() + 
                        " Dbase = "+ DataSource.getDbaseName() +
                        " Username = " + DataSource.getUserName() +
                        " Port #: "+ DataSource.getPort();
           //popInfoFrame("Read-Only Warning", msg);
           System.err.println(msg);
           return false;
         }

         // set static attribute in waveform class about type of data source
         props.setupWaveDataSource();

         if (debug) {
           System.out.println(
               "DEBUG makeDataSourceConnection to:\n"+
               DataSource.toDumpString()
           );
         }

         return true;
     }

     /** Pop a information dialog via the GUI event queue and return. */
     private void popInfoFrame(String title, Object message) {
         final String tit = title;
         final Object msg = message;  // (message instanceof String) ? ((String)message) : message;

         SwingUtilities.invokeLater(
             new Runnable() {
                 public void run() {
                     JOptionPane.showMessageDialog(
                         null, msg, tit, JOptionPane.PLAIN_MESSAGE
                     );
                 }
             }
         );
    }
//
    private void refreshGUI(boolean newEndTime) {

      if (debug) System.out.println ("Creating GUI...");

      if (waveSplit != null) getContentPane().remove(waveSplit);  // remove old one

      // make new graphics from new masterView
      if (absMode) {
          mv = makeMasterView(chanList, new TimeSpan(absStartTime, absEndTime));             
      } else {
          mv = makeMasterView(chanList, duration, newEndTime);
      }
      if (mv == null) stop();

      waveSplit = makeWaveSplit();

      getContentPane().add(waveSplit, BorderLayout.CENTER);       // add splitPane to frame
      wfScroller.setSecondsInViewport(secsInView);
      setSelectedInZoom();
      validate();

        //debug
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("WFView count        = "+ mv.getWFViewCount());
        //System.out.println("masterWFPanelModel  = "+ mv.masterWFViewModel.countListeners());
        //System.out.println("masterWFWindowModel = "+ mv.masterWFWindowModel.countListeners());
        System.out.println("Requested time window is relative to client host clock time, so be sure clock is set correctly!");
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++");

    }

    /** */
//    JSplitPane makeWaveSplit(MasterView mv) {
    private JSplitPane makeWaveSplit() {

      boolean isActive = true;
      WFScroller.ENABLE_HIDE = true; // must set this first since its static
      wfScroller = new WFScroller(mv, panelsInView, isActive);
      wfScroller.setMinimumSize(new Dimension(300, 100) );
      wfScroller.getWFGroupPanel().setShowSegments(true);
      wfScroller.setShowRowHeader(true);

      // remember old zoom value to pass it along to the next instance
      String oldZoomValue = null;
      if (zpanel != null) oldZoomValue = zpanel.getZoomValue();
      zpanel = new ZoomPanel(mv);
      // set minsize or splitpane won't move
      zpanel.setMinimumSize(new Dimension(100, 100) );
      zpanel.setFilterEnabled(true);

      // make a split pane with the zpanel and wfScroller
      JSplitPane waveSplit =
          new JSplitPane(JSplitPane.VERTICAL_SPLIT,
          false,       // don't repaint until resizing is done
          zpanel,      // top component
          wfScroller); // bottom component
      waveSplit.setOneTouchExpandable(true);
      waveSplit.setDividerLocation(0.5);

//      chanFinder = new ChannelFinderTextField(mv);
      chanFinder.setMasterView(mv);
      chanFinder.setChannelableList(mv.wfvList);

      if (oldZoomValue != null) zpanel.setZoomValue(oldZoomValue);

      return waveSplit;

    }

//    void setSelectedInZoom(MasterView mv) {
    void setSelectedInZoom() {
          // Retain previously selected WFPanel if there is one,
    // if none default to the first WFPanel in the list
      WFView wfvSel = mv.masterWFViewModel.get();
      // none selected, use the 1st one in the scroller
      if (wfvSel == null && mv.getWFViewCount() > 0) {
        wfvSel = (WFView) mv.wfvList.get(0);
   }

    // Must reset selected WFPanel because PickingPanel and WFScroller are
    // new and must be notified (via listeners) of the selected WFPanel.  It might
    // be null if no data is loaded.
     if (wfvSel != null ) {
       wfScroller.groupPanel.getWFPanel(wfvSel).setSelected(true);
       mv.masterWFViewModel.set(wfvSel);
       mv.masterWFWindowModel.setFullView(wfvSel.getWaveform());
     }
    }
    /**
     * Returns a new MasterView containing wf's for the set of channels with a
     * time window starting now and going back 'dur' seconds
     */
  private MasterView makeMasterView(ChannelableList chanList, double dur, boolean newTime) {
       // cut some slack, particularly if latency is not 0
       if (newTime) { 
           double padding = props.getDouble("endTimeShiftSecs", 0.);
           DateTime dt = new DateTime();
           double endTime = dt.getTrueSeconds(); // for UTC time seconds -aww 2008/02/11
           endTime = endTime - padding;    // backoff pad secs from current time
           ts = new TimeSpan(endTime-dur, endTime);
           setTitle("Scope v."+ versionNumber + " " + dt);
       }
       else if (ts == null) {
           return null;
       }
       else if (dur != oldDur) {
           oldDur = dur;
           ts.setStart(ts.getEnd()-duration); 
       }

       System.out.println("MV time span: " + ts.toString() );
       return makeMasterView(chanList, ts);
  }
    /**
     * Returns a new MasterView containing wf's for the set of channels with a
     * time window starting at 'start' and ending at 'end' in epoch secs.
     */
  private MasterView makeMasterView(ChannelableList chanList, TimeSpan ts) {

      // make a new view list with current time window
       ArrayList viewList = new ArrayList(chanList.size());

       // make a list of channel time windows
       for (int i = 0 ; i< chanList.size(); i++) {
         //     viewList.add(new  TriggerChannelTimeWindow(
         viewList.add(new  ChannelTimeWindow((Channel)chanList.get(i), ts));
       }

      // define the Master View
      if (mv != null) mv.destroy();       // destroy previous, free listeners, etc.
      mv = new MasterView();
      //mv.defineByTriggerChannelTimeWindowList(viewList);
      mv.defineByChannelTimeWindowList(viewList);

//      mv.setLoadChannelData(true);
//      mv.loadChannelData();
      mv.setWaveformLoadMode(MasterView.LoadAllInBackground);

      mv.setOwner(this);

      return mv;
  }

} // EventViewer

