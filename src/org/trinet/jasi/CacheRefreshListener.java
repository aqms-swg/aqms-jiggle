package org.trinet.jasi;
import java.util.EventListener;
public interface CacheRefreshListener extends java.util.EventListener {
   public void channelCacheRefresh(CacheRefreshEvent evt);
}
