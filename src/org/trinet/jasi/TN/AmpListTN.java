package org.trinet.jasi.TN;
import java.sql.*;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jdbc.NullValueDb;
import org.trinet.jdbc.table.SeqIds;
import org.trinet.jdbc.table.TableRowAmp;

public class AmpListTN extends AmpList {

    private PreparedStatement psInsert = null;

    private static final String psInsertStr =
        "INSERT INTO AMP (AMPID,DATETIME,STA,NET,AUTH,SUBSOURCE,CHANNEL,CHANNELSRC,SEEDCHAN," + // 1-9
        "LOCATION,AMPLITUDE,AMPTYPE,UNITS,AMPMEAS,PER,SNR,QUALITY,RFLAG,CFLAG,WSTART,DURATION) VALUES" +  // 10-21
        " (?,TRUETIME.putEpoch(?,'UTC'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,TRUETIME.putEpoch(?,'UTC'),?)"; // 21 columns
      //" (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"; // 21 columns

    private PreparedStatement psInsertAssocO = null;
    private static final String psInsertAssocOStr =
        "INSERT INTO ASSOCAMO (ORID,AMPID,AUTH,SUBSOURCE,DELTA,SEAZ,RFLAG) VALUES (?,?,?,?,?,?,?)"; // 7 columns

    private CallableStatement csUpsertAssocO = null;
    private static final String csUpsertAssocOStr = "{ call EPREF.upsertAssocAmO(?,?,?,?,?,?,?) }"; // 7 columns

    private PreparedStatement psInsertAssocM = null;
    private static final String psInsertAssocMStr = 
        "INSERT INTO ASSOCAMM (MAGID,AMPID,AUTH,SUBSOURCE,WEIGHT,IN_WGT,MAG,MAGRES,MAGCORR,RFLAG) VALUES (?,?,?,?,?,?,?,?,?,?)"; // 10 columns

    private CallableStatement csUpsertAssocM = null;
    private static final String csUpsertAssocMStr = "{ call EPREF.upsertAssocAmM(?,?,?,?,?,?,?,?,?,?) }"; // 10 columns


    public static boolean debug = false;

    static {
        debug = JasiDatabasePropertyList.debugTN;
    }

    private static boolean isSkipped(AmplitudeTN ampTN) {
        // Reading is deleted or not associated so skip
        if (ampTN.isDeleted() || !ampTN.isAssignedToSol()) {
            return true;
        }
        return false;
    }

    /** No-op default constructor, empty list. */
    public AmpListTN() {}

    /** Create empty list with specified input capacity.*/
    public AmpListTN(int capacity) {
        super(capacity);
    }

    /** Create list and add input collection elements to list.*/
    public AmpListTN(Collection col) {
        super(col);
    }
    /*
    private static final String csSeqStr = "{ ? = call SEQUENCE.getSeqTable('ARSEQ', ?) }";
        CallableStatement csSeqIdStmt = DataSource.getConnection().prepareCall(csSeqStr); 
        csSeqIdStmt.registerOutParameter(1, java.sql.Types.ARRAY, "SEQ_TABLE");
        csSeqIdStmt.setInt(2, newList.size());
        int result = csSeqIdStmt.executeUpdate();
        java.sql.Array intArray = cs.SeqIdStmt.getArray(1);
        BigDecimal[] bigd = (BigDecimal[]) intArray.getArray();  // need to use bigd.longValue() for the ampid 
    */

    // private static final String psSeqStr = "Select column_value from table (sequence.getSeqTable('AMPSEQ',?))"; // Oracle only
    // private static final String psSeqStr = "Select * from sequence.getSeqTable('AMPSEQ',?)"; // Postgres only
     
    private PreparedStatement psSeqIdStmt = null;

    public boolean commit() {
        return commit(true);
    }

    public boolean commit(boolean newSeqAmpid) {

        debug = JasiDatabasePropertyList.debugTN;

        if (!DataSource.isWriteBackEnabled()) return false;
        boolean status = true;

        int mySize = this.size();
        if (mySize == 0)  return true; // nothing to commit, a no-op

        // Find new readings to be saved, but without a valid id
        AmplitudeTN ampTN = null;
        ArrayList newList = new ArrayList(mySize);
        for (int i = 0; i < mySize; i++) {
            ampTN = (AmplitudeTN) get(i);
            //System.out.println(ampTN); // dump for debug
            //if (ampTN.isDeleted() || ! (ampTN.isAssignedToSol() || ampTN.isAssignedToMag())
            //        || ampTN.getAmpid() != 0) continue; // skip, replaced by below -aww 2011/05/12
            if (ampTN.isDeleted() || ! (ampTN.isAssignedToSol() || ampTN.isAssignedToMag()) ||
                    (ampTN.getAmpid() !=0 && !ampTN.hasChanged()) ) continue; // skip, old id and a change=>new ampid - aww 2011/05/12
            newList.add(ampTN);
        }

        // Set the ids of new readings to be committed
        PreparedStatement psSeqIdStmt = null;
        if ( newSeqAmpid && newList.size() > 0) {
          ResultSet rs = null;
          try {
            if (psSeqIdStmt == null) {
                psSeqIdStmt = DataSource.getConnection().prepareStatement(DataSource.getJiggleConnection().getSequenceQuery(TableRowAmp.SEQUENCE_NAME));
            }
            psSeqIdStmt.setInt(1, newList.size());
            rs = psSeqIdStmt.executeQuery();
            int ii = 0;
            while (rs.next()) {
                ampTN = (AmplitudeTN)newList.get(ii++);
                ampTN.setAmpid( rs.getLong(1) );
            }
          }
          catch (SQLException ex) {
              status = false;
              ex.printStackTrace();
          }
          finally {
            try {
              if (rs != null) rs.close();
              if (psSeqIdStmt != null) psSeqIdStmt.close();
              psSeqIdStmt = null;
            }
            catch (SQLException ex) {
            }
          }
        }
        if (!status) return false; // error setting new ids

        if (debug) {
            StringBuffer sb = new StringBuffer(128);
            System.out.print("DEBUG AmpListTN commit(");
            System.out.print(newSeqAmpid);
            System.out.print("): fromDB=");
            System.out.print(ampTN.fromDbase);
            System.out.println();
            Amplitude.getNeatStringHeader(sb);
            System.out.print(sb);
            sb.setLength(0);
            for (int i = 0; i<mySize; i++) {
                ampTN = (AmplitudeTN) get(i);
                System.out.println();
                Amplitude.toNeatString(sb, ampTN);
                System.out.print(sb);
                sb.setLength(0);
                if (isSkipped(ampTN)) continue;
                if (!ampTN.windowStart.isValid()) {
                    System.out.print(" *** No windowStart ***");
                }
            }
            System.out.println();
        }
        
        // Batch queue insert or update
        try {
          for (int i = 0; i<mySize; i++) {
            ampTN = (AmplitudeTN) get(i);
            if (isSkipped(ampTN)) continue;
            if (!ampTN.windowStart.isValid()) continue;
              if (ampTN.fromDbase) {  // "existing" (came from the dbase)
                if (ampTN.getNeedsCommit() || ampTN.hasChanged()) { // insert new row and association
                    if (debug) System.out.println ("Amplitude (changed) INSERT and assoc");
                    status = dbaseInsert(ampTN) && dbaseAssociate(ampTN, false);
                } else { // if no changes, just make new association
                    if (debug) System.out.println ("Amplitude (unchanged) assoc only");
                    status = dbaseAssociate(ampTN, true);
                    if (status) ampTN.setUpdate(false); // mark as up-to-date
                }
              } else {        // not from dbase <INSERT>
                  if (debug) System.out.println ("Amplitude (new) INSERT and assoc");
                  status = dbaseInsert(ampTN) && dbaseAssociate(ampTN, false);
              }
              //ampTN.setNeedsCommit(! status); // hopefully false?
              if (!status) {
                System.out.println("AmpListTN commit() batched status:" + status + " " + ampTN.toString());
              }
          }

          // Done with queuing up, now execute DML
          status = batchToDb(true); // close and null statements
          if (debug) System.out.println("DEBUG AmpListTN batchToDb success " + status);

          if (status) { // assume success, reset commit status
            for (int i = 0; i<mySize; i++) {
              ampTN = (AmplitudeTN) get(i);
              ampTN.setNeedsCommit(! status); // hopefully ok?
            }
            // For now commit ampset only if the type is not null, and ampsetid is not yet valid
            if (mySize > 0 && !getAmpSetType().equals("") && getAmpSetId() == 0l ) {
                Solution sol = ampTN.sol;
                if (sol != null) commitAmpSet(sol.getId().longValue());
            }
            //
          }
        }
        catch (Exception ex) {
          ex.printStackTrace();
          return false;
        }
        return status;  // relies on commit throwing exception if unsuccessful 
    }

    private boolean dbaseAssociate(AmplitudeTN ampTN, boolean update) {
        return (update) ? upsertAssoc(ampTN) : insertAssoc(ampTN);
    }

    private boolean upsertAssoc(AmplitudeTN ampTN) {
        boolean status = upsertOridAssoc(ampTN);
        status |= upsertMagidAssoc(ampTN);
        return status;
    }

    private boolean upsertOridAssoc(AmplitudeTN ampTN) {
        boolean status = true;
        try {
            // Have valid orid ?
            SolutionTN solTN = (SolutionTN) ampTN.sol;
            if (solTN == null) return false;

            long assocOrid = solTN.getOridValue();
            if (assocOrid <= 0l) return false;

            if (csUpsertAssocO == null) {
                if (debug || JasiDatabasePropertyList.debugSQL) {
                    System.out.println("DEBUG AmpListTN upsertOridAssoc for orid:" + assocOrid);
                    System.out.println(csUpsertAssocOStr);
                }
                csUpsertAssocO = DataSource.getConnection().prepareCall(csUpsertAssocOStr);
                //csUpsertAssocO.registerOutParameter(1, Types.INTEGER);
            }

            int icol = 1;

            // Key can't be null value
            //orid
            csUpsertAssocO.setLong(icol++, assocOrid);
            //ampid
            csUpsertAssocO.setLong(icol++, ampTN.getAmpid());

            //auth
            //String str = solTN.getAuthority(); // changed to below -aww 2011/09/21
            String str = ampTN.getClosestAuthorityString(); // uses local net code if undefined 
            if (NullValueDb.isNull(str)) str = null;
            csUpsertAssocO.setString(icol++, str);

            //subsource
            str = solTN.getSource();
            if (NullValueDb.isNull(str)) str = null;
            csUpsertAssocO.setString(icol++, str);

            //delta
            double x = ampTN.getHorizontalDistance();
            if ( ! (x == Channel.NULL_DIST || Double.isNaN(x)) ) csUpsertAssocO.setDouble(icol++, x);
            else csUpsertAssocO.setNull(icol++, Types.DOUBLE);

            //seaz
            x = ampTN.getAzimuth();
            if ( ! (x == Channel.NULL_AZIMUTH || Double.isNaN(x)) ) csUpsertAssocO.setDouble(icol++, x);
            else csUpsertAssocO.setNull(icol++, Types.DOUBLE);

            // rflag
            if (ampTN.processingState.isValid()) csUpsertAssocO.setString(icol++, ampTN.processingState.toString());
            else csUpsertAssocO.setNull(icol++, Types.VARCHAR);

            csUpsertAssocO.addBatch();

        }
        catch (SQLException ex) {
            System.err.println(ampTN);  // in case we need to know more on error 
            status = false;
            ex.printStackTrace();
        }
        return status;
    }

    private boolean upsertMagidAssoc(AmplitudeTN ampTN) {
        boolean status = true;
        try {
            // Have valid magid ?
            MagnitudeTN magTN = (MagnitudeTN) ampTN.magnitude;
            if (magTN == null) return false;

            long assocMagid = magTN.getMagidValue();
            if (assocMagid <= 0l) return false;

            if (csUpsertAssocM == null) {
                if (debug || JasiDatabasePropertyList.debugSQL) {
                    System.out.println("DEBUG AmpListTN upsertMagidAssoc for magid:" + assocMagid);
                    System.out.println(csUpsertAssocMStr);
                }
                csUpsertAssocM = DataSource.getConnection().prepareCall(csUpsertAssocMStr);
                //csUpsertAssocM.registerOutParameter(1, Types.INTEGER);
            }

            int icol = 1;

            // Key can't be null value
            //magid
            csUpsertAssocM.setLong(icol++, assocMagid);

            //ampid
            csUpsertAssocM.setLong(icol++, ampTN.getAmpid());

            //auth
            //String str = magTN.getAuthority();  // changed to below -aww 2011/09/21
            String str = ampTN.getClosestAuthorityString();  // uses local net code if undefined 
            if (NullValueDb.isNull(str)) str = null;
            csUpsertAssocM.setString(icol++, str);

            //subsource
            str = magTN.getSource();
            if (NullValueDb.isNull(str)) str = null;
            csUpsertAssocM.setString(icol++, str);

            //wgt
            if (ampTN.channelMag.weight.isValid()) csUpsertAssocM.setDouble(icol++, ampTN.channelMag.weight.doubleValue());
            else csUpsertAssocM.setNull(icol++, Types.DOUBLE);

            //in_wgt
            if (ampTN.channelMag.inWgt.isValid()) csUpsertAssocM.setDouble(icol++, ampTN.channelMag.inWgt.doubleValue());
            else csUpsertAssocM.setNull(icol++, Types.DOUBLE);

            //mag
            if (ampTN.channelMag.value.isValid()) csUpsertAssocM.setDouble(icol++, ampTN.channelMag.value.doubleValue());
            else csUpsertAssocM.setNull(icol++, Types.DOUBLE);

            //magres
            if (ampTN.channelMag.residual.isValid()) csUpsertAssocM.setDouble(icol++, ampTN.channelMag.residual.doubleValue());
            else csUpsertAssocM.setNull(icol++, Types.DOUBLE);

            //magcorr
            if (ampTN.channelMag.correction.isValid()) csUpsertAssocM.setDouble(icol++, ampTN.channelMag.correction.doubleValue());
            else csUpsertAssocM.setNull(icol++, Types.DOUBLE);

            // rflag
            if (ampTN.processingState.isValid()) csUpsertAssocM.setString(icol++, ampTN.processingState.toString());
            else csUpsertAssocM.setNull(icol++, Types.VARCHAR);

            csUpsertAssocM.addBatch();
        }
        catch (SQLException ex) {
            System.err.println(ampTN);  // in case we need to know more on error 
            status = false;
            ex.printStackTrace();
        }
        return status;
    }

    private boolean insertAssoc(AmplitudeTN ampTN) {
        boolean status = insertOridAssoc(ampTN);
        status |= insertMagidAssoc(ampTN);
        return status;
    }

    private boolean insertOridAssoc(AmplitudeTN ampTN) {
        boolean status = true;
        try {
            // Have valid orid ?
            SolutionTN solTN = (SolutionTN) ampTN.sol;
            if (solTN == null) return false;

            long assocOrid = solTN.getOridValue();
            if (assocOrid <= 0l) return false;

            if (debug || JasiDatabasePropertyList.debugSQL) 
                System.out.println("DEBUG AmpListTN insertOridAssoc orid:" + assocOrid + " ampid:" + ampTN.ampid.longValue());
            if (psInsertAssocO == null) {
                if (debug || JasiDatabasePropertyList.debugSQL) {
                    System.out.println(psInsertAssocOStr);
                }
                psInsertAssocO = DataSource.getConnection().prepareStatement(psInsertAssocOStr);
            }

            int icol = 1;

            // Key can't be null
            //orid
            psInsertAssocO.setLong(icol++, assocOrid);

            //ampid
            psInsertAssocO.setLong(icol++, ampTN.getAmpid());

            //auth
            //String str = solTN.getAuthority(); // changed to below -aww 2011/09/21
            String str = ampTN.getClosestAuthorityString(); // uses local net code if undefined 
            if (NullValueDb.isNull(str)) str = null;
            psInsertAssocO.setString(icol++, str);

            //subsource
            str = solTN.getSource();
            if (NullValueDb.isNull(str)) str = null;
            psInsertAssocO.setString(icol++, str);

            //delta
            double x = ampTN.getHorizontalDistance();
            if ( ! (x == Channel.NULL_DIST || Double.isNaN(x)) ) psInsertAssocO.setDouble(icol++, x);
            else psInsertAssocO.setNull(icol++, Types.DOUBLE);

            //seaz
            x = ampTN.getAzimuth();
            if ( ! (x == Channel.NULL_AZIMUTH || Double.isNaN(x)) ) psInsertAssocO.setDouble(icol++, x);
            else psInsertAssocO.setNull(icol++, Types.DOUBLE);

            // rflag
            if (ampTN.processingState.isValid()) psInsertAssocO.setString(icol++, ampTN.processingState.toString());
            else psInsertAssocO.setNull(icol++, Types.VARCHAR);

            psInsertAssocO.addBatch();

        }
        catch (SQLException ex) {
            System.err.println(ampTN);  // in case we need to know more on error 
            status = false;
            ex.printStackTrace();
        }
        return status;
    }

    private boolean insertMagidAssoc(AmplitudeTN ampTN) {
        boolean status = true;
        try {
            // Have valid magid ?
            MagnitudeTN magTN = (MagnitudeTN) ampTN.magnitude;
            if (magTN == null) return false;

            long assocMagid = magTN.getMagidValue();
            if (assocMagid <= 0l) return false;

            if (debug || JasiDatabasePropertyList.debugSQL)
                System.out.println("DEBUG AmpListTN insertMagidAssoc magid:" + assocMagid + " ampid:" + ampTN.ampid.longValue());
            if (psInsertAssocM == null) {
                if (debug || JasiDatabasePropertyList.debugSQL) {
                    System.out.println(psInsertAssocMStr);
                }
                psInsertAssocM = DataSource.getConnection().prepareStatement(psInsertAssocMStr);
            }

            int icol = 1;

            // Key can't be null value
            //magid
            psInsertAssocM.setLong(icol++, assocMagid);

            //ampid
            psInsertAssocM.setLong(icol++, ampTN.getAmpid());

            //auth
            //String str = magTN.getAuthority();  // changed to below -aww 2011/09/21
            String str = ampTN.getClosestAuthorityString();  // uses local net code if undefined 
            if (NullValueDb.isNull(str)) str = null;
            psInsertAssocM.setString(icol++, str);

            //subsource
            str = magTN.getSource();
            if (NullValueDb.isNull(str)) str = null;
            psInsertAssocM.setString(icol++, str);

            //wgt
            if (ampTN.channelMag.weight.isValid()) psInsertAssocM.setDouble(icol++, ampTN.channelMag.weight.doubleValue());
            else psInsertAssocM.setNull(icol++, Types.DOUBLE);

            //in_wgt
            if (ampTN.channelMag.inWgt.isValid()) psInsertAssocM.setDouble(icol++, ampTN.channelMag.inWgt.doubleValue());
            else psInsertAssocM.setNull(icol++, Types.DOUBLE);

            //mag
            if (ampTN.channelMag.value.isValid()) psInsertAssocM.setDouble(icol++, ampTN.channelMag.value.doubleValue());
            else psInsertAssocM.setNull(icol++, Types.DOUBLE);

            //magres
            if (ampTN.channelMag.residual.isValid()) psInsertAssocM.setDouble(icol++, ampTN.channelMag.residual.doubleValue());
            else psInsertAssocM.setNull(icol++, Types.DOUBLE);

            //magcorr
            if (ampTN.channelMag.correction.isValid()) psInsertAssocM.setDouble(icol++, ampTN.channelMag.correction.doubleValue());
            else psInsertAssocM.setNull(icol++, Types.DOUBLE);

            // rflag
            if (ampTN.processingState.isValid()) psInsertAssocM.setString(icol++, ampTN.processingState.toString());
            else psInsertAssocM.setNull(icol++, Types.VARCHAR);

            psInsertAssocM.addBatch();
        }
        catch (SQLException ex) {
            System.err.println(ampTN);  // in case we need to know more on error 
            status = false;
            ex.printStackTrace();
        }
        return status;
    }

    private boolean dbaseInsert(AmplitudeTN ampTN) {
        boolean status = true;
        try {
            if (debug || JasiDatabasePropertyList.debugSQL)
                System.out.println("DEBUG AmpListTN dbaseInsert ampid:" + ampTN.ampid.longValue());

            // NOTE: 2nd argument below to return generatedKeys not meaningful for batch !
            //if (psInsert == null) psInsert = DataSource.getConnection().prepareStatement(psInsertStr, new String[] {"AMPID"});
            if (psInsert == null) {
                if (debug || JasiDatabasePropertyList.debugSQL) {
                    System.out.println(psInsertStr);
                }
                psInsert = DataSource.getConnection().prepareStatement(psInsertStr);
            }

            int icol = 1;

            ChannelName cn = ampTN.getChannelObj().getChannelName();
            // id was assumed to be set from sequence
            psInsert.setDouble(icol++, ampTN.ampid.longValue());

            if (ampTN.datetime.isValid()) psInsert.setDouble(icol++, ampTN.datetime.doubleValue());
            else psInsert.setNull(icol++, Types.NUMERIC);

            psInsert.setString(icol++, cn.getSta());
            psInsert.setString(icol++, cn.getNet());
            psInsert.setString(icol++, ampTN.getClosestAuthorityString()); // uses local net code if undefined 
            psInsert.setString(icol++, ampTN.source.toString());
            psInsert.setString(icol++, cn.getChannel());
            psInsert.setString(icol++, cn.getChannelsrc());
            psInsert.setString(icol++, cn.getSeedchan());
            psInsert.setString(icol++, cn.getLocation());

            // "rectify" - peak amp must be > 0.0
            psInsert.setDouble(icol++, Math.abs(ampTN.value.doubleValue()));

            psInsert.setString(icol++, AmpType.getString(ampTN.type));

            psInsert.setString(icol++, Units.getString(ampTN.units));

            // if zero-to-peak = 1 , if peak-to-peak = 0
            psInsert.setInt(icol++, ((ampTN.halfAmp) ? Amplitude.ZERO_TO_PEAK : Amplitude.PEAK_TO_PEAK));

            if (ampTN.period.isValid()) psInsert.setDouble(icol++, ampTN.period.doubleValue());
            else psInsert.setNull(icol++, Types.DOUBLE);

            if (ampTN.snr.isValid()) psInsert.setDouble(icol++, ampTN.snr.doubleValue());
            else psInsert.setNull(icol++, Types.DOUBLE);

            psInsert.setDouble(icol++, ampTN.getQuality());

            psInsert.setString(icol++,  ampTN.processingState.toString());

            int clipped = ampTN.getClipped();
            if (clipped == Amplitude.ON_SCALE)         psInsert.setString(icol++, "OS");
            else if (clipped == Amplitude.BELOW_NOISE) psInsert.setString(icol++, "BN");
            else if (clipped == Amplitude.CLIPPED)     psInsert.setString(icol++, "CL");

            if (ampTN.windowStart.isValid()) psInsert.setDouble(icol++, ampTN.windowStart.doubleValue());
            else psInsert.setNull(icol++, Types.NUMERIC);

            if (ampTN.windowDuration.isValid()) psInsert.setDouble(icol++, ampTN.windowDuration.doubleValue());
            else psInsert.setNull(icol++, Types.DOUBLE);

            psInsert.addBatch();
        }

        catch (SQLException ex) {
            System.err.println(ampTN);  // in case we need to know more on error 
            status = false;
            ex.printStackTrace();
        }
        if (status) ampTN.setUpdate(false); // mark as up-to-date -aww added 2010/09/13
        return status;
    }

    private boolean batchToDb(boolean close) {

        int cnt = batchToDb(psInsert, close);
        if (debug || JasiDatabasePropertyList.debugSQL) System.out.println("DEBUG AmpListTN batchToDb insertAmp status: " + cnt);
        boolean status = (cnt >= 0);

        if (status) {

            cnt = batchToDb(csUpsertAssocO, close);
            status &= (cnt >= 0);
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println("DEBUG AmpListTN batchToDb upsertAssocO status: " + cnt);

            cnt = batchToDb(psInsertAssocO, close);
            status &= (cnt >= 0);
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println("DEBUG AmpListTN batchToDb insertAssocO status: " + cnt);

            cnt = batchToDb(csUpsertAssocM, close);
            status &= (cnt >= 0);
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println("DEBUG AmpListTN batchToDb upsertAssocM status: " + cnt);

            cnt = batchToDb(psInsertAssocM, close);
            status &= (cnt >= 0);
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println("DEBUG AmpListTN batchToDb insertAssocM status: " + cnt);
        }

        if (close) {
            psInsert = null;
            psInsertAssocO = null;
            csUpsertAssocO = null;
            psInsertAssocM = null;
            csUpsertAssocM = null;
        }

        return status;
    }

    // updateCount value: Statement.SUCCESS_NO_INFO
    // updateCount value: Statement.EXECUTE_FAILED
    private int batchToDb(Statement stmt, boolean close) {
        if (stmt == null) return 0;  // no-op, assume none

        int [] updateCounts = null;
        int status = -1;

        try {
            updateCounts = stmt.executeBatch();
            status = updateCounts.length;
            for (int idx = 0; idx<updateCounts.length; idx++) {
                if (updateCounts[idx] == Statement.EXECUTE_FAILED) {
                    status = -idx; // return negative index, flagging error -aww 2010/09/15
                    System.err.println("ERROR AmplitudeTN executeBatchInsert failed at idx: " + idx);
                    break;
                }
            }
        }
/*
ORACLE BATCH IMPLEMENTATION
For a prepared statement batch, it is not possible to know which operation failed.
The array has one element for each operation in the batch, and each element has a value of -3.
According to the JDBC 2.0 specification, a value of -3 indicates that an operation did not complete successfully.
In this case, it was presumably just one operation that actually failed, but because the JDBC driver does not 
know which operation that was, it labels all the batched operations as failures. 
You should always perform a ROLLBACK operation in this situation.

For a generic statement batch or callable statement batch, the update counts array is only a partial array
containing the actual update counts up to the point of the error. The actual update counts can be provided
because Oracle JDBC cannot use true batching for generic and callable statements in the Oracle implementation
of standard update batching. 

For example, if there were 20 operations in the batch, the first 13 succeeded, and the 14th generated an exception,
then the update counts array will have 13 elements, containing actual update counts of the successful operations.
You can either commit or roll back the successful operations in this situation, as you prefer.
In your code, upon failed execution of a batch, you should be prepared to handle either -3's
or true update counts in the array elements when an exception occurs.
For a failed batch execution, you will have either a full array of -3's or a partial array of positive integers.
*/
        catch(BatchUpdateException b) {
            System.err.println("-----BatchUpdateException-----");
            System.err.println("SQLState:  " + b.getSQLState());
            System.err.println("Message:  " + b.getMessage());
            System.err.println("Vendor:  " + b.getErrorCode());
            System.err.print("Update counts:  ");
            updateCounts = b.getUpdateCounts();
            for (int i = 0; i < updateCounts.length; i++) {
                System.err.print(updateCounts[i] + "   ");
            }
            System.err.println("Cause: " + b.getNextException().getMessage());
        }
        catch(SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            try {
                if (close) stmt.close();
            }
            catch (SQLException ex2) {
                ex2.printStackTrace();
            }
        }
        return status;
    }

    // We need upsert EPREF package function calls for updates to associations to handle re-commits of same list !!!
    private static final String selectAmpSetIdStr =
        "select ampsetid from assocevampset where (evid=?) and (ampsettype=?) and (subsource=?) and (isvalid>=?) order by lddate desc";
    private static final String ampSetInsertStr =
        "insert into ampset (ampsetid, ampid) values (?, ?)";
    private static final String ampSetInsertAssocStr =
        "insert into assocevampset (ampsetid, evid, ampsettype, isvalid, subsource) values (?, ?, ?, ?, ?)";
    private static final String ampSetUpdateAssocStr =
        "update assocevampset set isvalid=? where (evid=?) and (ampsettype=?) and (ampsetid=?)";

    private PreparedStatement psAmpSetSelectIdStmt = null;
    private PreparedStatement psAmpSetInsertStmt = null;
    private PreparedStatement psAmpSetAssocInsertStmt = null;
    private PreparedStatement psAmpSetAssocUpdateStmt = null;

    private int commitAmpSet(long evid) {
        // As implemented here only allows a new set if the setid was not set valid already
        long setid = getAmpSetId();
        if (setid > 0l) {
            System.err.println("ERROR AmpList commitAmpSet("+evid+") already committed ampsetid= "+ setid);
            return 0;
        }
        // <0 is a new value, >0 is existing ampsetseq value
        setid = getValidAmpSetId(evid);
        int cnt = insertAmpSet(evid, Math.abs(setid)); // ok if cnt > 0
        if (cnt > 0) {
          if (setid < 0) { // setid < 0 => create new set assoc 
            if (!insertAmpSetAssoc(evid, Math.abs(setid))) {
                System.err.println("Unable is insert new assocevampset row for evid= " +evid+ " ampsetid= " + Math.abs(setid));
            }
            else setAmpSetId(Math.abs(setid)); // success, save value positive, current set == new
          }
          else setAmpSetId(Math.abs(setid)); // success, save value positive, current set == old
        }
        return cnt; // <= 0 error 
    }

    // <0 is a new value, >0 is existing ampsetseq value
    private long getValidAmpSetId(long evid) {
        ResultSet rs = null;
        long ampsetid = 0l;

        try {
            if (psAmpSetSelectIdStmt == null) psAmpSetSelectIdStmt = DataSource.getConnection().prepareStatement(selectAmpSetIdStr);
            psAmpSetSelectIdStmt.setLong(1, evid);
            psAmpSetSelectIdStmt.setString(2, getAmpSetType());
            psAmpSetSelectIdStmt.setString(3, EnvironmentInfo.getApplicationName());
            psAmpSetSelectIdStmt.setInt(4, 1); // =1 value flags a valid set
            rs = psAmpSetSelectIdStmt.executeQuery();
            while (rs.next()) {
                ampsetid = rs.getLong(1); // the old value is positive
                break;
            }
            if (ampsetid == 0l) {
                ampsetid = -SeqIds.getNextSeq(DataSource.getConnection(), "AMPSETSEQ"); // negate the new value
                if (debug || JasiDatabasePropertyList.debugSQL) 
                   System.out.println("DEBUG AmpListTN.getValidAmpSet("+evid+") called SeqIds for new AMPSETSEQ:" + ampsetid); 
            }
        }
        catch (SQLException ex) {
              ex.printStackTrace();
        }
        finally {
            try {
              if (rs != null) rs.close();
              if (psAmpSetSelectIdStmt != null) psAmpSetSelectIdStmt.close();
              psAmpSetSelectIdStmt = null;
            }
            catch (SQLException ex2) {
                ex2.printStackTrace();
            }
        }
        return ampsetid; // prior value is positive, new value is negative, and no value is 0
    }

    private int insertAmpSet(long evid, long ampsetid) {
        if (ampsetid == 0l) {
            System.err.println("ERROR: AmpList insertAmpSet("+evid+", 0) input ampsetid is 0");
            return 0;
        }

        int cnt = 0;
        try {
            if (psAmpSetInsertStmt == null) {
                if (debug || JasiDatabasePropertyList.debugSQL) {
                    System.out.println(ampSetInsertStr);
                }
                psAmpSetInsertStmt = DataSource.getConnection().prepareStatement(ampSetInsertStr); 
            }
            int mySize = size();
            AmplitudeTN ampTN= null;
            // Here we assume that every amp in this list has an ampid that exists in db 
            for (int idx=0; idx<mySize; idx++) {
                ampTN = (AmplitudeTN) get(idx);
                psAmpSetInsertStmt.setLong(1, ampsetid);
                psAmpSetInsertStmt.setLong(2, ampTN.getAmpid());
                psAmpSetInsertStmt.addBatch();
            }

            boolean close = false;
            cnt = batchToDb(psAmpSetInsertStmt, close); // close statement after commit
            if (debug || JasiDatabasePropertyList.debugSQL) {
                System.out.println("DEBUG AmpListTN insertAmpSet batchToDb status cnt: " + cnt);
            }
        }
        catch (SQLException ex) {
            cnt = 0;
            System.err.println("ERROR: AmpListTN insertAmpSet("+evid+","+ampsetid+") exception"); 
            ex.printStackTrace();
        }
        finally {
            try {
              if (psAmpSetInsertStmt != null) {
                  psAmpSetInsertStmt.close();
                  psAmpSetInsertStmt = null;
              }
            }
            catch (SQLException ex2) {
                ex2.printStackTrace();
            }
        }
        return cnt;
    }


    private boolean insertAmpSetAssoc(long evid, long ampsetid) {
        String ampSetType = getAmpSetType();
        if (ampSetType == null || ampSetType == "") {
            System.err.println("ERROR AmpListTN insertAmpSetAssoc NULL ampset type not allowed");
            return false;
        }
        if (ampsetid <= 0l || evid <= 0l) {
            System.err.println("ERROR AmpListTN insertAmpSetAssoc invalid association id, evid= "+evid+ " ampsetid= "+ ampsetid);
            return false;
        }
        boolean status = false;
        try {
            if (psAmpSetAssocInsertStmt == null) {
                if (debug || JasiDatabasePropertyList.debugSQL) {
                    System.out.println(ampSetInsertAssocStr);
                }
                psAmpSetAssocInsertStmt = DataSource.getConnection().prepareStatement(ampSetInsertAssocStr); 
            }
            psAmpSetAssocInsertStmt.setLong(1, ampsetid);
            psAmpSetAssocInsertStmt.setLong(2, evid);
            psAmpSetAssocInsertStmt.setString(3, ampSetType);
            psAmpSetAssocInsertStmt.setInt(4, 1); // flag valid = 1
            psAmpSetAssocInsertStmt.setString(5, EnvironmentInfo.getApplicationName());
            status = (psAmpSetAssocInsertStmt.executeUpdate() > 0);
        }
        catch (SQLException ex) {
            status = false;
            System.err.println("ERROR: AmpListTN insertAmpSetAssoc(" +evid+ "," + ampsetid + ") exception");
            ex.printStackTrace();
        }
        finally {
            try {
              if (psAmpSetAssocInsertStmt != null) psAmpSetAssocInsertStmt.close();
              psAmpSetAssocInsertStmt = null;
            }
            catch (SQLException ex2) {
                ex2.printStackTrace();
            }
        }
        return status;

    }

    public boolean invalidateAmpSet() {

        if (size() == 0) return false; // empty list

        Solution sol = ((AmplitudeTN) get(0)).sol;
        if (sol == null) return false;

        long setid = getAmpSetId(); 
        if (setid == 0l) return false;

        String type = getAmpSetType();
        if (type == null || type.equals("")) return false;

        return updateAmpSetValidity(sol.getId().longValue(), setid, type, 0);
    }

    public boolean validateAmpSet() {

        if (size() == 0) return false; // empty list

        Solution sol = ((AmplitudeTN) get(0)).sol;
        if (sol == null) return false;

        long setid = getAmpSetId(); 
        if (setid == 0l) return false;

        String type = getAmpSetType();
        if (type == null || type.equals("")) return false;

        return updateAmpSetValidity(sol.getId().longValue(), setid, type, 1);
    }

    /** Invalidate all ampsets of same type as this list having specified source like input string, or all sources, if input source is null.*/
    public boolean invalidateAmpSets(String source) {

        if (size() == 0) return false; // empty list

        Solution sol = ((AmplitudeTN) get(0)).sol;
        if (sol == null) return false;

        String type = getAmpSetType();
        if (type == null || type.equals("")) return false;

        return updateAllAmpSetValidity(sol.getId().longValue(), type, source, 0);
    }

    private boolean updateAllAmpSetValidity(long evid, String ampSetType, String subsource, int validFlag) {
        boolean status = false;
        try {
            String ampSetUpdateAssocStr2 = (subsource != null) ?
                "update assocevampset set isvalid=? where (evid=?) and (ampsettype=?) and (lower(subsource) like ?)" :
                 "update assocevampset set isvalid=? where (evid=?) and (ampsettype=?)";

            if (psAmpSetAssocUpdateStmt == null) {
                if (debug || JasiDatabasePropertyList.debugSQL) {
                    System.out.println(ampSetUpdateAssocStr2);
                }
                psAmpSetAssocUpdateStmt = DataSource.getConnection().prepareStatement(ampSetUpdateAssocStr2); 
            }
            psAmpSetAssocUpdateStmt.setInt(1, validFlag); // invalid flag value
            psAmpSetAssocUpdateStmt.setLong(2, evid);
            psAmpSetAssocUpdateStmt.setString(3, ampSetType);
            if (subsource != null) psAmpSetAssocUpdateStmt.setString(4, subsource);
            int cnt = psAmpSetAssocUpdateStmt.executeUpdate();
            status = (cnt > 0);
            if (debug || JasiDatabasePropertyList.debugSQL) {
                if (subsource != null) {
                    System.out.println("DEBUG AmpList.updateAllAmpSetValidity set isValid= " + validFlag +
                            " for " +cnt+ " ampset(s) of ampsettype= " +ampSetType+ " subsource= " +subsource+ " for evid= " + evid);
                 }
                 else {
                    System.out.println("DEBUG AmpList.updateAllAmpSetValidity set isValid= " + validFlag +
                            " for " +cnt+ " ampset(s) of ampsettype= " +ampSetType+ " for evid= " + evid);
                 }
            }
        }
        catch (SQLException ex) {
            status = false;
            // in case we need to know more on error 
            System.err.println("ERROR: AmpListTN updateAllAmpSetValidity("+evid+","+ampSetType+","+subsource+","+validFlag+")");
            ex.printStackTrace();
        }
        finally {
            try {
              if (psAmpSetAssocUpdateStmt != null) psAmpSetAssocUpdateStmt.close();
              psAmpSetAssocUpdateStmt = null;
            }
            catch (SQLException ex2) {
                ex2.printStackTrace();
            }
        }
        return status;
    }

    private boolean updateAmpSetValidity(long evid, long setid, String ampSetType, int validFlag) {
        boolean status = false;
        try {
            // "update assocevampset set isvalid=? where (evid=?) "; 
            // "and (ampsettype=?)"  "and (ampsetid=?)"
            if (psAmpSetAssocUpdateStmt == null) {
                if (debug || JasiDatabasePropertyList.debugSQL) {
                    System.out.println(ampSetUpdateAssocStr);
                }
                psAmpSetAssocUpdateStmt = DataSource.getConnection().prepareStatement(ampSetUpdateAssocStr); 
            }
            psAmpSetAssocUpdateStmt.setInt(1, validFlag);
            psAmpSetAssocUpdateStmt.setLong(2, evid);
            psAmpSetAssocUpdateStmt.setString(3, ampSetType);
            psAmpSetAssocUpdateStmt.setLong(4, setid);
            status = (psAmpSetAssocUpdateStmt.executeUpdate() > 0);
        }
        catch (SQLException ex) {
            status = false;
            System.err.println("ERROR: AmpListTN updateAmpSetValidity("+evid+","+setid+","+ampSetType+","+validFlag+")");
            // in case we need to know more on error 
            ex.printStackTrace();
        }
        finally {
            try {
              if (psAmpSetAssocUpdateStmt != null) psAmpSetAssocUpdateStmt.close();
              psAmpSetAssocUpdateStmt = null;
            }
            catch (SQLException ex2) {
                ex2.printStackTrace();
            }
        }
        return status;
    }

} // AmpListTN
