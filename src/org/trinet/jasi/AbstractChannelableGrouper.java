package org.trinet.jasi;

/**
 * Abstract base class that searches linearly down a sorted
 * ChannelableList returning sublist that contain only
 * Channelable objects elements that meet the grouping critera
 * as defined by the concrete subclass implementations.
 * @see isSameGroup(Channelable, Channelable)
 */

public abstract class AbstractChannelableGrouper {

  protected ChannelableListIF list;
  protected ChannelableListIF sublist;
  protected Channelable ch1;
  protected Channelable chx;

  protected int groupsReturned = 0;
  protected int index = 0;

  /**  */
  public AbstractChannelableGrouper() { }

  /** Sets the list to use. */
  public AbstractChannelableGrouper(ChannelableListIF list) {
    setList(list);
  }
  /** Set the list to use. */
  public void setList(ChannelableListIF list) {
    this.list = list;
    groupsReturned = 0;
    index = 0;
    // implement sorter in subclass, method can do other init tasks too.
    sortList(list);
  }

  public abstract void sortList(ChannelableListIF list);

  /** Return true if list contains more subgroups. */
  public boolean hasMoreGroups() {
    return index < list.size();
  }

  /**
  * Returns a ChannelableList that contains next subgroup.
  * Returns null if List was not initiated or no subgroups remain.
  * @see #isSameGroup(Channelable,Channelable)
  */
  public ChannelableListIF getNext()  {
    if (! hasMoreGroups() ||
        list == null || list.isEmpty()) return null;

    try {
      sublist = (ChannelableListIF) list.getClass().newInstance();
    }
    catch (Exception ex) {
      System.err.println(getClass().getName()+
          " - Bad ChannelableList class type: "+list.getClass().getName());
    }

    // get the first element of subgroup
    ch1 = (Channelable) list.get(index++);
    initSubgroup(ch1);

    while (index < list.size()) {
      chx = (Channelable) list.get(index);
      if (inSameGroup(ch1, chx)) {
        sublist.add(chx);
        index++;
      } else {
        break; // another subgroup, bail
      }
    }
    groupsReturned++;
    return sublist;
  }

  /** Concrete subclasses should override if necessary, to initialize
  * extra data needed by the subclass comparator method.
  * @see #isSameGroup(Channelable,Channelable)
  */
  protected void initSubgroup(Channelable ch) {
    sublist.add(ch); // save first member of subgroup
    // override can get more data it needs from the input
  }

  /** Return true if the two input Channelable object belong in the same group. */
  public abstract boolean inSameGroup (Channelable ch1, Channelable ch2) ;

  /** Return current number of groups returned. */
  public int getGroupsReturned() {
      return groupsReturned;
  }
}
