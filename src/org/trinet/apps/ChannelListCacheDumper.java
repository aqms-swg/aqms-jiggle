package org.trinet.apps;
import org.trinet.jasi.*;
import org.trinet.jdbc.datasources.AbstractSQLDataSource;

public class ChannelListCacheDumper extends Object {
    public static void main(String args[]){

      if (args.length < 9) {
        System.out.println("Args:[user passwd host.domain dbname driver subprotocol port isCacheFileInWorkingDir [cacheName default=channelList.cache, in work directory (user.dir)]");
        System.exit(0);
      }

      String user   = args[0];
      String passwd = args[1];
      String host   = args[2];
      String dbname = args[3];
      String driver = args[4];
      String subprotocol = args[5];
      String port = args[6];
      boolean isCacheFileInWorkingDir = Boolean.parseBoolean(args[7].toLowerCase());

      String cacheFile = (args.length > 8) ? args[8] : "channelList.cache";

      System.out.println("ChannelListCacheDumper: making db connection... ");
      DataSource ds = TestDataSource.create(host, dbname,
              user, passwd, subprotocol, port, driver);
      System.out.println("ChannelListCacheDumper dataSource : " + ds.describeConnection());

      StringBuffer sb = new StringBuffer(256);
      if (isCacheFileInWorkingDir) {
        sb.append(System.getProperty("user.dir", "."));
        sb.append(System.getProperty("file.separator"));
      }
      sb.append(cacheFile);

      System.out.println("ChannelListCacheDumper reading data from cache file : "+sb.toString());
      ChannelList cl = ChannelList.readFromCache(sb.toString());

      if (cl == null) {
        System.out.println("ChannelListCacheDumper: input channelList null");
      }
      else if (cl.size() == 0) {
        System.out.println("ChannelListCacheDumper: input channelList size = 0");
      }
      else {
        sb.append("Dump");
        System.out.println("ChannelListCacheDumper: writing data to listing: "+sb.toString());
        cl.writeToFile(sb.toString());
      }
    }
}
