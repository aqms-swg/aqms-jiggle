package org.trinet.jasi;
import java.util.*;
public interface SolutionAssociatedListIF extends JasiCommitableListIF, StripableDistanceResidualIF {
  public SolutionAssociatedListIF getAssociatedWith(Solution sol);
  public SolutionAssociatedListIF getAssociatedWith(Solution sol, boolean undeletedOnly);
  public SolutionAssociatedListIF getUnassociated();
  public SolutionAssociatedListIF getUnassociated(boolean undeletedOnly);
  public void associateAllWith(Solution sol);
  public void unassociateAllFrom(Solution sol);
  public void assignAllTo(Solution sol); // no list addition, cf. associate()
  public int deleteAll(Solution sol);
  public int removeAll(Solution sol);
  public SolutionAssociatedListIF getListByType(Solution sol, Object type);
  public void unassociateAll();
  public boolean commit(Solution sol);
  public int deleteUnassociated();
  public int removeUnassociated();
  public int eraseUnassociated();
  //methods below extend StripableDistanceResidualIF:
  public int stripByResidual(double val, Solution sol);
  public int stripByDistance(double val, Solution sol);
  public int stripByResidual(double val);
  public int stripByDistance(double val);
}
