package org.trinet.jasi;

import java.util.*;

/*
 * ActiveList.java
 *
 *
 * @author Doug Given
 * @version
 */

/**
 * Collection extended to handle MVC model. You can't extend both Collection and
 * Observable so this class provides model function by having a simple internal
 * Observable. Therefore, it will behave like a Collection but notify observers
 * when changes are made. Observers should have their update() method react to
 * two possible args; an object and a Collection object. If an object
 * is sent just that one object in thelist was changed. If a Collection is sent
 * unspecified changes were made to the list and the observer should refresh its
 * view of the list.  <p> NOTE: that if you operate on an object directly
 * you must call ActiveList.notifyObs(Object) for the observers to react to the
 * change. <p>
 *
 * Allows the "selection" of one object in the list.
 *
 * Had to extend a concrete class so chose ArrayList, that could change later.
 */

public class ActiveList extends ArrayList {

    MyModel model = new MyModel();

    /** Flag to temporarily turn off notifications during bulk loads. */
    boolean modelActive = true;

    /** The currently selected object.*/
    Object selected = null;

    public ActiveList() {

    }

    public ActiveList (Collection col) {

    addAll(col);
    }
    /** Return the currently selected object. Returns 'null' if no object is
     *  selected. You must cast the selected Object to the correct type. */
    public Object getSelected() {
      return selected;
    }
    /** Set the passed object as selected. Returns 'true' on success. Method
     *  fails if object is not in the list. */
    public boolean setSelected (Object selected) {
      if (this.contains(selected)) {
        this.selected = selected;
        return true;
      } else {
        return false;
      }
    }
// **** Pass-thru methods to notify observers ******

/**
 * Given a Collection,  checks to make sure no duplicates are entered.
 * Returns true if any were added. */

    public boolean addAll(Collection newList)
    {
    modelActive = false;        // don't notify until done
    boolean rtn = super.addAll(newList);

    modelActive = true;
    notifyObs(this);
    return rtn;
    }

/**
 * Adds the given Collection starting at the given index.
 * Checks to make sure no duplicates are
 * entered.  Returns true if any were added. */
    public boolean addAll(int index, Collection newList)
    {
    boolean rtn = super.addAll(index, newList);
    notifyObs(this);
    return rtn;
    }

/**
 * Add an object. Will not add object if its already in the list.
 * Returns 'true' if object was added.
 */
    public boolean add(Object obj) {
    if ( contains(obj) ) return false;  // no dups
        boolean rtn = super.add(obj);
    notifyObs(obj);
    return rtn;
    }

    // added this method -aww 07/14/2006
    /** Replace first equal object found in list with input object, else add input object, notify observers.
     * Returns object replaced, else null.
     * */
    public Object addOrReplace(Object obj) {
        int idx = indexOf(obj);
        Object ret = null;
        if (idx >=0) {
            ret = super.set(idx, obj);
            if (ret != null && ret == selected) setSelected(obj);  // added selection synch -aww 2008/03/24
        }
        else super.add(obj);
        notifyObs(obj);
        return ret;
    }

 /**
 * Insert an object at this index. Will not add object if its already in the list.
 * Returns 'true' if object was added.
 */
    public void add(int index, Object obj)
    {
    if ( contains(obj) ) return;    // no dups
     super.add(index, obj);
    notifyObs(obj);
    }

/**
 * Remove an object. Returns 'true' if object was removed.
 */
    public boolean remove(Object obj)
    {
    boolean rtn = super.remove(obj);
    notifyObs(obj);
    return rtn;
    }
/**
 * Remove all object in this range. Returns 'true' if object was removed.
 */
    public void removeRange(int from, int to)
    {
    super.removeRange(from, to);
    notifyObs(this);
    }

/**
 * Remove all objects in this collection. Returns 'true' if successful.
 */
    public boolean removeAll(Collection col)
    {
    boolean rtn = super.removeAll(col);
    notifyObs(this);
    return rtn;
    }

 /**
 * Remove all objects. Returns 'true' if successful.
 */
    public void clear()
    {
    super.clear();
    notifyObs(this);
    }

    /**
    *
    */
    public Object[] toArray() {

           return super.toArray();
    }

    /**
    *
    */
    public Object[] toArray(Object[] array) {

           return super.toArray(array);
    }

    /**
     * Actually deletes the object from the list. Synonymous with remove(). <p>
     * Use JasiReadingList if you want a collection of jasi objects that uses
     * the delete() method of the object to do a virtual delete and doesn't
     * actually remove it from the list. */
    public boolean delete (Object obj) {
      return remove(obj);
    }

    /** Notify observers and pass them an object */
    protected void notifyObs (Object obj) {
    if (modelActive)   model.setValue(obj);
    }
    /** Notify observer */
    public void addObserver(Observer obs) {
    model.addObserver(obs);

    }
    /** Delete this observer. */
    public void deleteObserver(Observer obs) {
    model.deleteObserver(obs);
    }
    /** Delete all observers. */
    public void deleteObservers() {
    model.deleteObservers();
    }
    /** Return count of registered observers. */
    public int countObservers() {
      return model.countObservers();
    }

    /** Returns the internal model. */
    public Observable getModel () {
       return model;
    }

    /** Had to extend Observable to get access to setChanged() which is
        protected */
    class MyModel extends Observable implements java.io.Serializable {

    public void setValue (Object obj) {
        setChanged();
        notifyObservers(obj);
    }

    } // end of MyModel
/*
  public static final class Tester {
    public static void main (String args[])  {
      int evid;
      if (args.length <= 0)   // no args
      {
        evid = 9506369;
        System.out.println ("Using evid "+ evid+" as a test...");
      } else {
        Integer val = Integer.valueOf(args[0]);    // convert arg String to 'double'
        evid = (int) val.intValue();
      }
      System.out.println ("Making connection...");
      DataSource ds = TestDataSource.create();
      // NOTE: this demonstrates making a static Channel list.
      //        System.out.println ("Reading in station list...");
      //  Channel.setList (Channel.getAllList());
      System.out.println ("Getting phases  for "+evid);
      ArrayList phList = (ArrayList) Phase.create().getBySolution(evid);
      ActiveList phaseList = new ActiveList(phList);
      System.out.println ("Number of phases = "+ phaseList.size());
//
// The VERY WEIRD syntax in the next line is because of:
//ActivePhaseList.java:198: No enclosing instance of class org.trinet.jiggle.ActivePhaseList is in scope;
//an explicit one must be provided when creating inner class org.trinet.jiggle.ActivePhaseList.TestObserver,
//as in "outer. new Inner()" or "outer. super()".
//
      Observer to = new Observer() {
        public void update(Observable obs, Object arg) {
          System.out.println ("TestObserver !!");
          System.out.println ("obs= "+obs.toString());
          System.out.println ("arg= "+arg.toString());
           if (arg instanceof Phase)
                   System.out.println ("Seeing new phase to list: "+((Phase)arg).toString());
        }
      };
      phaseList.addObserver(to);
      Phase newph = Phase.create();
      Channel chan = Channel.create().setChannelName("CI", "COK", "EHZ", null);
      newph.chan = chan;
      newph.description.set("PkP", "I", "U", 3);
      System.out.println ("Adding new phase to list: "+newph.toString());
      phaseList.add(newph);
    }

  } // end of Tester
*/
} // ActiveList
