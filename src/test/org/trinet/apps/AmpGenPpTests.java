package test.org.trinet.apps;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.trinet.apps.AmpGenPp;
import test.common.DatabaseTests;
import test.testqueries.common.TestQueries;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class AmpGenPpTests extends DatabaseTests {

    @BeforeAll
    public static void addDataToDatabase() throws Exception {
        TestQueries testQueries = new TestQueries(postgreSQLContainer.getJdbcUrl());
        int rows = testQueries.populateDataForArcImportTest();
        assertTrue(rows > 0);
        rows = testQueries.populateJasiChannelViewDataForAmpGenPpTest();
        assertTrue(rows > 0);
        // Application query must be run before app channels query
        rows = testQueries.populateApplicationsDataForAmpGenPpTest();
        assertTrue(rows > 0);
        rows = testQueries.populateAppChannelsDataForAmpGenPpTest();
        assertTrue(rows > 0);
    }

    @Test
    public void verify_amp_gen_pp_main_function_runs() throws Exception {
        // Have these tests on run locally for now
        if (isOnLocalMachine()) {
            try {
                AmpGenPp.main(new String[]{"39750312", getTestPropFilePath() + "AmpGenPp.props", "postgresql-devel1.gps.caltech.edu",
                        "5432", "org.postgresql.Driver", "trinetdb", "parchdb", "trinetdb"});
            } catch (Exception e) {
                fail("No exceptions thrown");
            }
        }
    }
}