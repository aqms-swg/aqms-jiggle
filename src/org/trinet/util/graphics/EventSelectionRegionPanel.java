package org.trinet.util.graphics;

import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Vector;
import java.util.StringTokenizer;
import javax.swing.*;
import javax.swing.border.*;

import org.trinet.jasi.*;
import org.trinet.jasi.TN.SolutionTN;
import org.trinet.util.*;
import org.trinet.util.gazetteer.LatLonZ;
import org.trinet.util.graphics.text.JTextClipboardPopupMenu;

public class EventSelectionRegionPanel extends JPanel {
    //"regionType"              // ComboBox (see below)
    //"regionRadiusPoint"       // LatLonChooser
    //"regionRadiusValue"       // NumberTextField > 0.
    //"regionBorderPlaceName"   // ComboBox Get names from geo_region table
    //"regionRadiusPlaceName"   // ComboBox Get names from gazetteerpt
    //"regionBoxLat"            // LatLonChooser paired with BoxLon
    //"regionBoxLon"            // LatLonChooser paired with BoxLat
    //"regionPolygon"           // input pt pairs into a text field string

     /** Properties passed as an argument to the constructor */
     EventSelectionProperties props;

     JComboBox typeComboBox = null;

     JTextField defaultMinDepthTextField = new JTextField(4);
     JTextField defaultMaxDepthTextField = new JTextField(4);

     JTextField defaultMinMagTextField = new JTextField(4);
     JTextField defaultMaxMagTextField = new JTextField(4);

     JTextField radiusTextField = new JTextField(8);
     JTextField placeTextField = new JTextField(8);
     JComboBox borderTextField = new JComboBox();
     JTextField polygonTextField = new JTextField(40);
     JLabel regionNamesLabel = new JLabel();

     JCheckBox reject = new JCheckBox("Exclude by region");
     JCheckBox includeNullMag = new JCheckBox("Include null magnitudes");

     Box defaultDepthRangeBox = null;
     Box defaultMagRangeBox = null;
     Box regionNamesBox = null;

     JTextArea polyListTextArea = null;

     LatLonChooser radiusLatLonChooser = null;
     LatLonChooser maxLatLonChooser = null;
     LatLonChooser minLatLonChooser = null;

     public EventSelectionRegionPanel(EventSelectionProperties props) {
          this.props = props;
          try  {
               jbInit();
          }
          catch(Exception ex) {
               ex.printStackTrace();
          }
     }

     private void jbInit() throws Exception {
        //this.setLayout(new GridLayout(0, 1));
        Box masterBox = Box.createVerticalBox();

        TitledBorder border = new TitledBorder("Region Types");
        border.setTitleColor(Color.black);
        JPanel regionTypePanel = new JPanel();
        regionTypePanel.setBorder(border);
        final Box regionTypeBox = Box.createHorizontalBox();
        JLabel jlabel = new JLabel("Type is");
        regionTypeBox.add(Box.createHorizontalGlue());
        regionTypeBox.add(jlabel);
        
        typeComboBox = new JComboBox( new String []
            {
             EventSelectionProperties.REGION_ANYWHERE
             ,EventSelectionProperties.REGION_BOX
             ,EventSelectionProperties.REGION_POINT
             ,EventSelectionProperties.REGION_PLACE
             ,EventSelectionProperties.REGION_BORDER
             ,EventSelectionProperties.REGION_POLYGON
             ,EventSelectionProperties.REGION_POLYLIST
            }
        );
        typeComboBox.setEditable(false);
        String str = props.getRegionType();
        if (str == null) str = EventSelectionProperties.REGION_ANYWHERE;
        typeComboBox.setSelectedItem(str);
        typeComboBox.setMaximumSize(new Dimension(100,20));
        //
        typeComboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean tf = (((String)typeComboBox.getSelectedItem()).equals(EventSelectionProperties.REGION_POLYLIST));
                //defaultDepthRangeBox.setVisible(tf);
                //defaultMagRangeBox.setVisible(tf);
                regionNamesBox.setVisible(tf);
                reject.setVisible(!tf);
                repaint();
            }
        });
        //
        regionTypeBox.add(typeComboBox);

        reject.setSelected(props.getBoolean("regionExcluded"));
        regionTypeBox.add(reject);

        DoubleRange dr = props.getDoubleRange("region.default.orgDepthRange");
        if (dr == null) dr = new DoubleRange(-9., 999.);
        defaultMinDepthTextField.setMaximumSize(new Dimension(120,20));
        defaultMinDepthTextField.setText(String.valueOf(dr.getMinValue()));
        defaultMaxDepthTextField.setMaximumSize(new Dimension(120,20));
        defaultMaxDepthTextField.setText(String.valueOf(dr.getMaxValue()));

        defaultDepthRangeBox = Box.createHorizontalBox();
        defaultDepthRangeBox.add(Box.createHorizontalGlue());
        defaultDepthRangeBox.add(new JLabel(" Default region depth min:"));
        defaultDepthRangeBox.add(defaultMinDepthTextField);
        defaultDepthRangeBox.add(new JLabel(" max:"));
        defaultDepthRangeBox.add(defaultMaxDepthTextField);
        defaultDepthRangeBox.add(Box.createHorizontalGlue());
        //regionTypeBox.add(defaultDepthRangeBox);

        dr = props.getDoubleRange("region.default.magValueRange");
        if (dr == null) dr = new DoubleRange(-1., 9.);
        defaultMinMagTextField.setMaximumSize(new Dimension(120,20));
        defaultMinMagTextField.setText(String.valueOf(dr.getMinValue()));
        defaultMaxMagTextField.setMaximumSize(new Dimension(120,20));
        defaultMaxMagTextField.setText(String.valueOf(dr.getMaxValue()));

        defaultMagRangeBox = Box.createHorizontalBox();
        defaultMagRangeBox.add(Box.createHorizontalGlue());
        defaultMagRangeBox.add(new JLabel(" Default region mag min:"));
        defaultMagRangeBox.add(defaultMinMagTextField);
        defaultMagRangeBox.add(new JLabel(" max:"));
        defaultMagRangeBox.add(defaultMaxMagTextField);
        defaultMagRangeBox.add(Box.createHorizontalGlue());
        //regionTypeBox.add(defaultMagRangeBox);

        regionNamesBox = Box.createVerticalBox();
        regionNamesBox.add(Box.createVerticalGlue());
        regionNamesBox.add(defaultDepthRangeBox);
        regionNamesBox.add(defaultMagRangeBox);

        includeNullMag.setSelected(props.getBoolean("region.default.includeNullMag"));
        regionNamesBox.add(includeNullMag);

        polyListTextArea = new JTextArea();
        EventSelectionProperties.EventRegion [] eregions = props.getEventRegions();
        if (eregions.length > 0) {
            for (int idx=0; idx < eregions.length; idx++) {
                polyListTextArea.append(eregions[idx].toPropertyString());
            }
        }
        new JTextClipboardPopupMenu(polyListTextArea);

        /*
        String [] names = props.getRegionNameList().toArray();
        str = null;
        for (int idx=0; idx < names.length; idx++) {
           str = "region."+names[idx]+".depthRange";
           polyListTextArea.append(str+"="+props.getProperty(str,"")+"\n");
           str = "region."+names[idx]+".magValueRange";
           polyListTextArea.append(str+"="+props.getProperty(str,"")+"\n");
           str = "region."+names[idx]+".polygon"; 
           polyListTextArea.append(str+"="+props.getProperty(str,"")+"\n");
        }
        */

        polyListTextArea.setEditable(true);
        JScrollPane jsp = new JScrollPane(polyListTextArea);
        regionNamesBox.add(jsp);
        regionNamesLabel.setText(props.getRegionNameList().toString(" "));
        regionNamesBox.add(regionNamesLabel);
        regionNamesBox.add(Box.createVerticalGlue());

        boolean tf = (((String)typeComboBox.getSelectedItem()).equals(EventSelectionProperties.REGION_POLYLIST));
        regionNamesBox.setVisible(tf);
        reject.setVisible(!tf);

        regionTypeBox.add(Box.createHorizontalGlue());
        Box regionBox = Box.createVerticalBox();
        regionBox.add(regionTypeBox);
        regionBox.add(regionNamesBox);
        regionTypePanel.add(regionBox);
        masterBox.add(regionTypePanel);

/*
//"regionBoxLat"
public DoubleRange getRegionBoxLatRange() {
public void setRegionBoxLatRange(double minLat, double maxLat) {
//"regionBoxLon"
public DoubleRange getRegionBoxLonRange() {
public void setRegionBoxLonRange(double minLon, double maxLon) {
*/
        border = new TitledBorder("Region By Box");
        border.setTitleColor(Color.black);
        JPanel regionBoxPanel = new JPanel();
        regionBoxPanel.setBorder(border);

        DoubleRange latRange = props.getRegionBoxLatRange();
        DoubleRange lonRange = props.getRegionBoxLonRange();
        double lat = (latRange != null) ? latRange.getMaxValue() : 0.;
        double lon = (lonRange != null) ? lonRange.getMaxValue() : 0.;
        maxLatLonChooser = new LatLonChooser(lat, lon);
        lat = (latRange != null) ? latRange.getMinValue() : 0.;
        lon = (lonRange != null) ? lonRange.getMinValue() : 0.;
        minLatLonChooser = new LatLonChooser(lat, lon);

        Box box = Box.createHorizontalBox();
        jlabel = new JLabel("Min (SW corner) ");
        box.add(jlabel);
        box.add(minLatLonChooser);
        box.add(Box.createHorizontalStrut(4));
        jlabel = new JLabel("Max (NE corner) ");
        box.add(jlabel);
        box.add(maxLatLonChooser);
        box.add(Box.createHorizontalGlue());

        regionBoxPanel.add(box);
        masterBox.add(regionBoxPanel);

    /*
    //"regionRadiusPoint"       // LatLonChooser
    public LatLonZ getRegionRadiusPoint() {
    public void setRegionRadiusPoint(double lat, double lon) {
    //"regionRadiusValue"       // NumberTextField > 0.
    public double getRegionRadiusValue() {
    public void setRegionRadiusValue(double dist) {
    */

        border = new TitledBorder("Region By Radius");
        border.setTitleColor(Color.black);
        JPanel regionRadiusPanel = new JPanel();
        regionRadiusPanel.setBorder(border);

        LatLonZ llz = props.getRegionRadiusPoint(); 
        radiusLatLonChooser = (llz == null) ?
           new LatLonChooser() : new LatLonChooser(llz);

        Box vbox = Box.createVerticalBox();
        box = Box.createHorizontalBox();
        jlabel = new JLabel("Radius");
        box.add(jlabel);
        radiusTextField.setMaximumSize(new Dimension(120,20));
        double val = props.getRegionRadiusValue();
        if (! Double.isNaN(val)) radiusTextField.setText(String.valueOf(val));
        /*
        radiusTextField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String str = ((JTextField) e.getSource()).getText();
                if (str != null && str.length() > 0) {
                    props.setRegionRadiusValue(Double.parseDouble(str));
                    LatLonZ llz = radiusLatLonChooser.getLatLonZ();
                    props.setRegionRadiusPoint(llz.getLat(), llz.getLon());
                }
                else {
                    props.remove("regionRadiusValue");
                    props.remove("regionRadiusPoint");
                }
            }
        });
        */
        box.add(radiusTextField);
        box.add(Box.createHorizontalGlue());
        vbox.add(box);

        box = Box.createHorizontalBox();
        jlabel = new JLabel("Radius point");
        jlabel.setVerticalAlignment(JButton.TOP);
        box.add(jlabel);
        box.add(radiusLatLonChooser);
        box.add(Box.createHorizontalGlue());
        vbox.add(box);

/*
"regionRadiusPlaceName"
public void setRegionRadiusPlaceName(String name) {
public String getRegionRadiusPlaceName() {
*/
        box = Box.createHorizontalBox();
        jlabel = new JLabel("Place");
        box.add(jlabel);
        placeTextField.setMaximumSize(new Dimension(120,20));
        placeTextField.setText(props.getRegionRadiusPlaceName());
        /*
        placeTextField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String str = ((JTextField) e.getSource()).getText();
                if (str != null && str.length() > 0) props.setRegionRadiusPlaceName(str);
            }
        });
        */
        box.add(placeTextField);
        box.add(Box.createHorizontalGlue());
        vbox.add(box);
        vbox.add(Box.createVerticalGlue());
        regionRadiusPanel.add(vbox);
        masterBox.add(regionRadiusPanel);

/*        
regionPolygon // and display the entered data array in text field string
public LatLonZ [] getRegionPolygon() {
*/
        border = new TitledBorder("Region By Polygon");
        border.setTitleColor(Color.black);
        JPanel regionPolyPanel = new JPanel();
        regionPolyPanel.setBorder(border);

        box = Box.createHorizontalBox();
        jlabel = new JLabel("Lat,Lon (3 or more pairs)");
        box.add(jlabel);
        polygonTextField.setMaximumSize(new Dimension(120,20));
        polygonTextField.setText(props.getProperty("regionPolygon", ""));
        box.add(polygonTextField);
        box.add(Box.createHorizontalGlue());
        regionPolyPanel.add(box);
        masterBox.add(regionPolyPanel);
/*
"regionBorderPlaceName"   // ComboBox Get names from geo_region table
public String getRegionBorderPlaceName() {
public void setRegionBorderPlaceName(String name) {
*/
        border = new TitledBorder("Region By Border");
        border.setTitleColor(Color.black);
        JPanel regionBorderPanel = new JPanel();
        regionBorderPanel.setBorder(border);

        box = Box.createHorizontalBox();
        jlabel = new JLabel("Name");
        box.add(jlabel);
        Vector vec = new Vector();
        try {
          Statement s = DataSource.getConnection().createStatement();
          ResultSet rs = s.executeQuery("select name from gazetteer_region");
          while (rs.next()) {
            vec.add(rs.getString(1));
          }
          rs.close();
          s.close();
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        borderTextField.setEditable(true);
        borderTextField.setModel(new DefaultComboBoxModel(vec));
        borderTextField.setMaximumSize(new Dimension(120,20));
        borderTextField.setSelectedItem(props.getRegionBorderPlaceName());
        //borderTextField.setText(props.getRegionBorderPlaceName());
        /*
        borderTextField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String str = ((JTextField) e.getSource()).getText();
                if (str != null && str.length() > 0) props.setRegionRadiusPlaceName(str);
            }
        });
        */
        box.add(borderTextField);
        box.add(Box.createHorizontalGlue());
        regionBorderPanel.add(box);
        masterBox.add(regionBorderPanel);

        JPanel jp = new JPanel();
        JButton cb = new JButton("Clear All");
        cb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                EventSelectionRegionPanel.this.clear();
            }
        });
        cb.setMaximumSize(new Dimension(90,20));
        jp.add(cb);

        if (props.getBoolean("enableRegionCreation")) {
          cb = new JButton("Create Region");
          cb.setToolTipText("Create Gazetteer_Region table row using name/polygon text fields");
          cb.addActionListener(new ActionListener() {
              public void actionPerformed(ActionEvent e) {
                //String name = borderTextField.getText();
                String name = (String) borderTextField.getSelectedItem();
                if (name == null || name.length() == 0) {
                    System.err.println("ERROR EventSelectionRegionPanel: No region name specified."); 
                    return;
                }

                String poly = polygonTextField.getText();
                if (poly == null || poly.length() == 0) {
                    System.err.println("ERROR EventSelectionRegionPanel: No polygon coordinates specified."); 
                    return; 
                }

                StringTokenizer st = new StringTokenizer(poly," ,\t");
                int cnt = st.countTokens();
                if (cnt < 6) {
                    System.err.println("ERROR EventSelectionRegionPanel: Polygon must be at least 3 pts."); 
                    return;
                }
                if (cnt%2 == 1) {
                    System.err.println("ERROR EventSelectionRegionPanel: Polygon lat,lon in pairs, found odd # of tokens."); 
                    return;
                }

                cnt = cnt/2;
                LatLonZ [] llz = new LatLonZ[cnt];
                try {
                  for (int idx = 0; idx < cnt; idx++) {
                    llz[idx] = new LatLonZ(Double.parseDouble(st.nextToken()), Double.parseDouble(st.nextToken()), 0.);
                  }
                }
                catch (NumberFormatException ex) {
                  System.err.println(ex.toString());
                }
                SolutionTN.createRegionByCallableStatement(DataSource.getConnection(), name, llz);
              }
          });
          jp.add(cb);
        }

        masterBox.add(jp);

        masterBox.add(Box.createVerticalGlue());

        this.add(masterBox);
        this.setMinimumSize(new Dimension(590,680));
    }

    public void clear() {
        typeComboBox.setSelectedItem(EventSelectionProperties.REGION_ANYWHERE);
        //minLatLonChooser.setLatLon(-90.,-180.);
        //maxLatLonChooser.setLatLon(90.,180.);
        minLatLonChooser.setLatLon(0.,0.);
        maxLatLonChooser.setLatLon(0.,0.);
        radiusTextField.setText(null);
        defaultMinMagTextField.setText(null);
        defaultMaxMagTextField.setText(null);
        placeTextField.setText(null);
        //borderTextField.setText(null);
        ((JTextField) borderTextField.getEditor().getEditorComponent()).setText(null);
        polygonTextField.setText(null);
        polyListTextArea.setText(null);
        radiusLatLonChooser.setLatLon(0.,0.);
    }

    public void updateProperties() {
        //Type
        props.setRegionType((String)typeComboBox.getSelectedItem());
        props.setProperty("regionExcluded", reject.isSelected());

        //Polylist
        if (polyListTextArea != null) {
          String text = polyListTextArea.getText();
          if (text != null && text.length() > 0) {
            // set the region(s) magValues range and polygon lat,lon list
            props.setProperties(text);
            ArrayList list = new ArrayList();
            StringTokenizer stoke = new StringTokenizer(text, "\n\r");
            String str = null;
            String regionName = null;
            int idx1 = -1;
            int idx2 = -1;
            while (stoke.hasMoreTokens()) {
              str = stoke.nextToken();
              idx1 = str.indexOf("region.");
              if (idx1 == 0) {
                idx2 = str.indexOf(".magValue");
                if (idx2 < 0) {
                  idx2 = str.indexOf(".orgDepth");
                  if (idx2 < 0) {
                    idx2 = str.indexOf(".polygon");
                    if (idx2 < 0) {
                      idx2 = str.indexOf(".include");
                      if (idx2 < 0 ) {
                          idx2 = str.indexOf(".includeNullMag");
                      }
                    }
                  }
                }
                if (idx1 >= 0 && idx2 > 0) {
                  str = str.substring(idx1+7,idx2);
                  if (regionName == null || !regionName.equals(str)) {
                    regionName = str;
                    list.add(regionName);
                  }
                }
              }
            }
            if (list.size() > 0) {
                String [] strs = (String []) list.toArray(new String [list.size()]);
                props.setRegionNameList(strs);
                regionNamesLabel.setText(new StringList(strs).toString(" "));
            }
          }
        }

        // Default polylist polygon depthRange
        String min = defaultMinDepthTextField.getText();
        if (min == null || min.length() == 0) min = "-9";
        String max = defaultMaxDepthTextField.getText();
        if (max == null || max.length() == 0) max = "999.";
        props.setProperty("region.default.orgDepthRange",  
            Double.parseDouble(min) + " " + Double.parseDouble(max));

        // Default polylist polygon magValueRange
        min = defaultMinMagTextField.getText();
        if (min == null || min.length() == 0) min = "-1.";
        max = defaultMaxMagTextField.getText();
        if (max == null || max.length() == 0) max = "9.";
        props.setProperty("region.default.magValueRange",  
            Double.parseDouble(min) + " " + Double.parseDouble(max));

        props.setProperty("region.default.includeNullMag", includeNullMag.isSelected());  

        //Radius
        String str = radiusTextField.getText();
        if (str != null && str.length() > 0) {
            props.setRegionRadiusValue(Double.parseDouble(str));
        }
        else {
            //props.setRegionRadiusValue(0.);
            props.setProperty("regionRadiusValue", (String) null); //remove
        }

        // lat,lon pt
        LatLonZ llz = radiusLatLonChooser.getLatLonZ();
        if (! llz.isNull()) props.setRegionRadiusPoint(llz.getLat(), llz.getLon());
        else props.setProperty("regionRadiusPoint", (String) null); //remove

        // or place name
        str = placeTextField.getText();
        if (str != null && str.length() > 0) {
            props.setRegionRadiusPlaceName(str);
        }
        else {
            //props.setRegionRadiusPlaceName("undefined");
            props.setRegionRadiusPlaceName(null); //remove
        }

        //Box
        LatLonZ minLLZ = minLatLonChooser.getLatLonZ();
        LatLonZ maxLLZ = maxLatLonChooser.getLatLonZ();
        boolean isNull = minLLZ.isNull() && maxLLZ.isNull();
        if (! isNull) props.setRegionBoxLatRange(minLLZ.getLat(), maxLLZ.getLat());
        else props.setProperty("regionBoxLat", (String) null); // remove
        if (! isNull) props.setRegionBoxLonRange(minLLZ.getLon(), maxLLZ.getLon());
        else props.setProperty("regionBoxLon", (String) null); // remove

        //Border
        //str = borderTextField.getText();
        str = (String) borderTextField.getSelectedItem();
        if (str != null && str.length() > 0) {
            props.setRegionBorderPlaceName(str);
        }
        else {
            props.setRegionBorderPlaceName(null); //remove
        }

        //Polygon
        str = polygonTextField.getText();
        if (str != null && str.length() > 0) {
            props.setProperty("regionPolygon", str);
        }
        else {
            /*
            DoubleRange latRange = props.getRegionBoxLatRange();
            DoubleRange lonRange = props.getRegionBoxLonRange();
            str =
              latRange.getMaxValue() + " " + lonRange.getMaxValue() + " " +
              latRange.getMinValue() + " " + lonRange.getMaxValue() + " " +
              latRange.getMinValue() + " " + lonRange.getMinValue() + " " +
              latRange.getMaxValue() + " " + lonRange.getMinValue();
            */
            props.setProperty("regionPolygon", (String) null); //remove
        }

    }
}
