
// *** PACKAGE NAME *** \
package org.trinet.jasi.EW;


// *** IMPORTS *** \
import java.util.*;
import java.sql.Connection;

import org.trinet.jasi.EW.SQL.EWSQLStatement;

// Individual SQL Statements
import org.trinet.jasi.EW.SQL.GetListStatement;
import org.trinet.jasi.EW.SQL.GetEventInfoStatement;
import org.trinet.jasi.EW.SQL.UpdateEventStatement;
import org.trinet.jasi.EW.SQL.CreateEventStatement;
import org.trinet.jasi.EW.SQL.SetPreferStatement;
import org.trinet.jasi.EW.SQL.GetPreferredInfoStatement;


/**
 * <!-- Class Description>
 *
 *
 * Created:  2002/06/05
 *
 * @author   ew_oci2java Tool / DK
 * @version  0.99
 **/



public class EWSolution
{


  // *** CLASS ATTRIBUTES *** \

  public static final int PREFER_TYPE_ORIGIN = 1;
  public static final int PREFER_TYPE_MECHFM = 2;
  public static final int PREFER_TYPE_MAGNITUDE = 3;

	// Data Attributes
  public long idEvent; // Description
  public int tiEventType; // Description
  public int iDubiocity; // Description
  public int bArchived; // Description
  public String sSource; // Description
  public String sHumanReadable; // Description
  public String sSourceEventID; // Description
  public long idOrigin; // Description
  public double tOrigin; // Description
  public float dLat; // Description
  public float dLon; // Description
  public float dDepth; // Description
  public float dPrefMag; // Description
  public String sComment; // Description

  // Criteria
  public int starttime; // Description
  public int endtime; // Description
  public String source; // Description
  public float minlat; // Description
  public float maxlat; // Description
  public float minlon; // Description
  public float maxlon; // Description
  public float minz; // Description
  public float maxz; // Description
  public float minmag; // Description
  public float maxmag; // Description
  public int eventtype; // Description

  // SQL Return Code
  public long Retcode; // Description

  // Flags indicating whether data fields should be recorded during update
  public int bSetEventType; // Description
  public int bSetDubiocity; // Description
  public int bSetArchived; // Description

  // Preferred Information
  public long idMag; // Description
  public long idMech; // Description
  public long idPrefered; // Description
  public long PreferType; // Description
  public long idPrefer; // Description

  // Preferred Origin Object
  public EWOrigin ewOrigin;

  // Alternate Origin List for this Event
  public Vector vOriginList;

  // *** CLASS SQL STATEMENTS *** \
  static EWSQLStatement es1;
  static EWSQLStatement es2;
  static EWSQLStatement es3;
  static EWSQLStatement es4;
  static EWSQLStatement es5;
  static EWSQLStatement es6;


  // *** CONSTRUCTORS *** \
  public EWSolution()
  {
    if(es1 == null)
      es1 = new GetListStatement(org.trinet.jasi.DataSource.getConnection());
    if(es2 == null)
      es2 = new GetEventInfoStatement(org.trinet.jasi.DataSource.getConnection());
    if(es3 == null)
      es3 = new UpdateEventStatement(org.trinet.jasi.DataSource.getConnection());
    if(es4 == null)
      es4 = new CreateEventStatement(org.trinet.jasi.DataSource.getConnection());
    if(es5 == null)
      es5 = new GetPreferredInfoStatement(org.trinet.jasi.DataSource.getConnection());
    if(es6 == null)
      es6 = new SetPreferStatement(org.trinet.jasi.DataSource.getConnection());
  }


  // *** CLASS METHODS *** \
  public void SetConnection(Connection IN_conn)
  {
    es1.SetConnection(IN_conn);
    es2.SetConnection(IN_conn);
    es3.SetConnection(IN_conn);
    es4.SetConnection(IN_conn);
    es5.SetConnection(IN_conn);
    es6.SetConnection(IN_conn);
  }


  public Vector ReadList()
  {
    Vector ResultList = new Vector();
    es1.CallQueryStatement((Object)this, ResultList);
    return(ResultList);
  }  // end EWSolution:ReadList()


  public EWSolution Read()
  {
    if(es2.CallStatement((Object)this))
      return(this);
    else
      return(null);
  }  // end EWSolution::Read()


  public EWSolution Update()
  {
    if(es3.CallStatement((Object)this))
      return(this);
    else
      return(null);
  }  // end EWSolution::Update()


  public EWSolution Write()
  {
    if(es4.CallStatement((Object)this))
      return(this);
    else
      return(null);
  }  // end EWSolution::Write()


  public EWSolution ReadPreferred()
  {

    if(es5.CallStatement((Object)this))
      return(this);
    else
      return(null);
  }  // end EWSolution:ReadPreferred()


  public EWSolution UpdatePrefer()
  {

    if(es6.CallStatement((Object)this))
      return(this);
    else
      return(null);
  }  // end EWSolution:UpdatePrefer()

}  // end class EWSolution
