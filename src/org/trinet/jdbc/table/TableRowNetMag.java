package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see NetMag
*/
public interface TableRowNetMag extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "NETMAG";

/** Number of column data fields in a table row.
*/
    //public static final int MAX_FIELDS =  15;
    public static final int MAX_FIELDS =  16; // assumes added NOBS column to NetMag table 2010/08/19 -aww

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "MAGSEQ";

/**  NETMAG table "magid" column data object offset in collection stored by implementing class.
*/
    public static final int MAGID = 0;

/**  NETMAG table "orid" column data object offset in collection stored by implementing class.
*/
    public static final int ORID = 1;

/**  NETMAG table "commid" column data object offset in collection stored by implementing class.
*/
    public static final int COMMID = 2;

/**  NETMAG table "magnitude" column data object offset in collection stored by implementing class.
*/
    public static final int MAGNITUDE = 3;

/**  NETMAG table "magtype" column data object offset in collection stored by implementing class.
*/
    public static final int MAGTYPE = 4;

/**  NETMAG table "auth" column data object offset in collection stored by implementing class.
*/
    public static final int AUTH = 5;

/**  NETMAG table "subsource" column data object offset in collection stored by implementing class.
*/
    public static final int SUBSOURCE = 6;

/**  NETMAG table "magalgo" column data object offset in collection stored by implementing class.
*/
    public static final int MAGALGO = 7;

/**  NETMAG table "nsta" column data object offset in collection stored by implementing class.
*/
    public static final int NSTA = 8;

/*  NETMAG table "nobs" column data object offset in collection stored by implementing class.
 */
    public static final int NOBS = 9;

/** String of know column names delimited by ",".  */
/**  NETMAG table "uncertainty" column data object offset in collection stored by implementing class.
*/
    public static final int UNCERTAINTY = 10;

/**  NETMAG table "gap" column data object offset in collection stored by implementing class.
*/
    public static final int GAP = 11;

/**  NETMAG table "distance" column data object offset in collection stored by implementing class.
*/
    public static final int DISTANCE = 12;

/**  NETMAG table "quality" column data object offset in collection stored by implementing class.
*/
    public static final int QUALITY = 13;

/**  NETMAG table "rflag" column data object offset in collection stored by implementing class.
*/
    public static final int RFLAG = 14;

/**  NETMAG table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 15;

    public static final String COLUMN_NAMES =
   //"MAGID,ORID,COMMID,MAGNITUDE,MAGTYPE,AUTH,SUBSOURCE,MAGALGO,NSTA,UNCERTAINTY,GAP,DISTANCE,QUALITY,RFLAG,LDDATE";  // replaced by below 2010/08/19 -aww
   "MAGID,ORID,COMMID,MAGNITUDE,MAGTYPE,AUTH,SUBSOURCE,MAGALGO,NSTA,NOBS,UNCERTAINTY,GAP,DISTANCE,QUALITY,RFLAG,LDDATE";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    //"NETMAG.MAGID,NETMAG.ORID,NETMAG.COMMID,NETMAG.MAGNITUDE,NETMAG.MAGTYPE,NETMAG.AUTH,NETMAG.SUBSOURCE,NETMAG.MAGALGO,NETMAG.NSTA,NETMAG.UNCERTAINTY,NETMAG.GAP,NETMAG.DISTANCE,NETMAG.QUALITY,NETMAG.RFLAG,NETMAG.LDDATE"; // replaced by below 2010/08/19 -aww
    "NETMAG.MAGID,NETMAG.ORID,NETMAG.COMMID,NETMAG.MAGNITUDE,NETMAG.MAGTYPE,NETMAG.AUTH,NETMAG.SUBSOURCE,NETMAG.MAGALGO,NETMAG.NSTA,NETMAG.NOBS,NETMAG.UNCERTAINTY,NETMAG.GAP,NETMAG.DISTANCE,NETMAG.QUALITY,NETMAG.RFLAG,NETMAG.LDDATE";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"MAGID", "ORID", "COMMID", "MAGNITUDE", "MAGTYPE", 
	//"AUTH", "SUBSOURCE", "MAGALGO", "NSTA", "UNCERTAINTY",   // replaced by below 2010/08/19 -aww
	"AUTH", "SUBSOURCE", "MAGALGO", "NSTA", "NOBS", "UNCERTAINTY", 
	"GAP", "DISTANCE", "QUALITY", "RFLAG", "LDDATE"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, false, true, false, false, 
	//false, true, true,  true, true,   // replaced by below 2010/08/19 -aww
	false, true, true, true, true, true, 
	true, true, true, true, true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATALONG, DATALONG, DATALONG, DATADOUBLE, DATASTRING, 
	//DATASTRING, DATASTRING, DATASTRING,  DATALONG, DATADOUBLE,    // replaced by below 2010/08/19 -aww
	DATASTRING, DATASTRING, DATASTRING, DATALONG, DATALONG, DATADOUBLE, 
	DATADOUBLE, DATADOUBLE, DATADOUBLE, DATASTRING, DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
//	0, 0, 0, 2, 0, 0, 0, 0, 0, 3, 1, 3, 1, 0, 0   // replaced by below 2010/08/19 -aww
	0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 3, 1, 3, 1, 0, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	//15, 15, 15, 5, 6, 15, 8, 15, 5, 5, 4, 7, 2, 2, 7   // replaced by below 2010/08/19 -aww
	15, 15, 15, 5, 6, 15, 8, 15, 5, 5, 5, 4, 7, 2, 2, 7
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	//null, null, null, null, null, null, null, null, null, null,    // replaced by below 2010/08/19 -aww
	null, null, null, null, null, null, null, null, null, null, null, 
	null, null, null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))"
    };
}
