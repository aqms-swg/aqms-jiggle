package org.trinet.jdbc.datasources;
import org.trinet.util.*;
public interface GenericDbServiceIF extends GenericServiceIF {
    public String getDriver();
    public String getHostName();
    public String getUserName();
    public String getDbaseName();

    public void setHostName(String hostName);
    public void setUserName(String userName);
    public void setDbaseName(String dbName);
}

