# Jiggle

This source repo for Jiggle is now the authoritative one. Though, caution it is not well-tested and is known to have bugs with Oracle.

#### Introduction

Jiggle is a Graphical User Interface (GUI) software application used for human review and analysis of earthquake waveform data and calculation of accurate earthquake (event) parameters.  Jiggle is part of the post-processing (PP) software suite in the Advanced National Seismic System Quake Monitoring System (AQMS).

This version of Jiggle in this GitLab repository should work with either a Postgres or Oracle database.

#### Jiggle directory structure
- ``bin/`` -> scripts to build and run Jiggle, and folder for executable Jiggle jar file.
- ``bin/mapdata/`` -> files for mapping (not committed to repository because of large file size; available for download at https://gitlab.com/aqms-swg/jiggle-archive/-/blob/master/mapdata.zip)
- ``lib/`` -> external libraries (.jar files) that Jiggle depends on
- ``prop/`` -> example Jiggle properties files
- ``prop/logs/`` -> Jiggle log files will be stored here when you run Jiggle
- ``src/`` -> source code, ``.class`` files, script to compile

#### How to compile, build, and run Jiggle

Requirements and notes: 
   - Java JDK 8 or 11 needs to be installed on your computer.  [Oracle, OpenJDK, Amazon Corretto]. Does not support Java version less than 8.
   - If on Windows/Linux and you have JDK installed, ignore `./compile_jiggle.sh: line 29: /usr/libexec/java_home: No such file or directory` error message when running `compile_jiggle`.
   - If on Windows using Windows Subsystem for Linux, it may be necessary to change the file format to Unix to correct the newline characters.
   - Optional - SDKMAN! may be helpful in managing different versions of JDK. See https://sdkman.io/install for usage.

This compiles Jiggle source code and creates the .class files in ``src/out_classes_8/`` directory.
```
$ cd src/
$ ./compile_jiggle.sh 8   # (for Java 1.8)
```

This builds the executable ``jiggle_p8g.jar`` (for Java 1.8, gtype schema).  You can rename it ``jiggle.jar``.
```
$ cd ../bin/
$ ./build_jiggle.sh 8   # (for Java 1.8)
```

This runs Jiggle (it runs ``jiggle_p8g.jar``), with the properties specified in the input file ``../prop/jiggle.props``.
```
## Copy all jar files from "lib" directory to "bin" directory. 
$ cd ../bin/
$ ./run_jiggle.sh 8 ../prop/jiggle.props   # (for Java 1.8)
```

Notes:

   - ``../prop/jiggle.props`` in this repository has fake server names, usernames, and passwords.  Replace these with your own input parameters.


#### Jiggle Documentation

* Jiggle user guide (work in progress): https://aqms-swg.gitlab.io/jiggle-userdocs/
* Jiggle user guide source code (work in progress): https://gitlab.com/aqms-swg/jiggle-userdocs
* Original Jiggle archive website: https://aqms-swg.gitlab.io/jiggle-www/
  * Jiggle release notes (pre-2018): https://aqms-swg.gitlab.io/jiggle-www/currentRelNotes.html
  * Jiggle properties info: https://aqms-swg.gitlab.io/jiggle-www/JiggleProperties.html
* Original Jiggle source code:
  * Initial commit in this GitLab repository is from [revision 8650](https://vault.gps.caltech.edu/trac/cisn/changeset/8650/) in [AQMS Jiggle SVN repository](https://vault.gps.caltech.edu/trac/cisn/browser/PP/branches/clean-uw-dev-jungle-gtype/) where you can view the change logs 
    * NOTE: access to AQMS Jiggle SVN repository at vault.gps.caltech.edu is restricted - not open to the general public

#### Gitlab Build

Gitlab pipeline is used to build and create a release. 

* When building from `master` branch, `src/org/trinet/jasi/Version.java` file is updated automatically by the pipeline with today's date to be used as the Jiggle version string.
* Release is created when a Git tag is pushed. Creating a git tag from `master` branch should have the updated Jiggle version string.

#### Gitlab Release

To create a release:

* Create and push a git tag.
* Gitlab pipeline builds and creates a release
* Manually update your release with release notes
* Assets are uploaded to generic package registry in Gitlab and linked from the release
