package org.trinet.util;

//import java.util.prefs.*;

public class GenericApplicationPrefs {

    //comment out until jdk1.4 implemented in production
/*
    protected Preferences sysPrefs;  // should not changed by client, universal
    protected Preferences userPrefs; // can be changed by client

    public GenericApplicationPrefs(Object obj) {
      initPrefs(obj.getClass());
    }
    protected void initPrefs(Class aClass) {
       if (sysPrefs == null) {
          sysPrefs = Preferences.systemNodeForPackage(aClass);
       }
       if (userPrefs == null) {
         userPrefs = Preferences.userNodeForPackage(aClass);
       }
    }
    public Preferences getSystemPreferences() {
      return sysPrefs;
    }
    public Preferences getUserPreferences() {
      return userPrefs;
    }
    public String getString(String key, String defaultValue) {
        return userPrefs.get(key, sysPrefs.get(key, defaultValue));
    }
    public double getDouble(String key, double defaultValue) {
        return userPrefs.getDouble(key, sysPrefs.getDouble(key, defaultValue));
    }
    public float getFloat(String key, float defaultValue) {
        return userPrefs.getFloat(key, sysPrefs.getFloat(key, defaultValue));
    }
    public int getInt(String key, int defaultValue) {
        return userPrefs.getInt(key, sysPrefs.getInt(key, defaultValue));
    }
    public long getLong(String key, long defaultValue) {
        return userPrefs.getLong(key, sysPrefs.getLong(key, defaultValue));
    }
    public boolean getBoolean(String key, boolean defaultValue) {
        return userPrefs.getBoolean(key, sysPrefs.getBoolean(key, defaultValue));
    }

    public void putString(String key, String value) {
        userPrefs.put(key, value);
    }
    public void putDouble(String key, double value) {
        userPrefs.putDouble(key, value);
    }
    public void putFloat(String key, float value) {
        userPrefs.putFloat(key, value);
    }
    public void putInt(String key, int value) {
        userPrefs.putInt(key, value);
    }
    public void putLong(String key, long value) {
        userPrefs.putLong(key, value);
    }
    public void putBoolean(String key, boolean value) {
        userPrefs.putBoolean(key, value);
    }
    public boolean saveUserPrefs() {
        return savePreferences(userPrefs);
    }
    public boolean saveSystemPrefs() {
        return savePreferences(sysPrefs);
    }
    protected boolean savePreferences(Preferences prefs) {
       boolean retVal = true;
       try {
         prefs.flush();
       }
       catch (BackingStoreException ex) {
         //ex.printStackTrace();
         System.err.println("Preferences Error: Backing store unavailable");
         retVal = false;
       }
       return retVal;
    }
    public void logSystemPreferences() {
       logPreferences(sysPrefs);
    }
    public void logUserPreferences() {
       logPreferences(userPrefs);
    }
    public void logAllPreferences() {
       logPreferences(sysPrefs);
       logPreferences(userPrefs);
    }
    private void logPreferences(Preferences prefs) {
      System.out.println(prefs.toString());
      try {
        String [] keys = prefs.keys();
        for (int idx = 0; idx < keys.length; idx++) {
          System.out.println(keys[idx] + "=" + prefs.get(keys[idx],"") );
        }
        System.out.println();
      }
      catch (BackingStoreException ex) {
         //ex.printStackTrace();
         System.err.println("Preferences Error: Backing store unavailable");
      }
    }
// end of jdk1.4 comment -aww
*/
}
