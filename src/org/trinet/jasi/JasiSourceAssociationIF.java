package org.trinet.jasi;
import org.trinet.jdbc.datatypes.*;
public interface JasiSourceAssociationIF {
    public void setAuthority(String network);
    public void setSource(String name);
    public void setProcessingState(String state);

    public boolean isAuto();
    public boolean isHuman();
    public boolean isFinal();
    public boolean isIntermediate();

    public String getProcessingStateString();
    public String getSource();
    public String getAuthority();
    //
    // Add methods to discriminate between Strings and DataObjects?
    //
    public DataString getProcessingState();

    //public DataString getAuthority();
    //public DataString getSource();
    //public String getSourceString();
    //public String getAuthorityString();

    public void setAuthority(DataString network);
    public void setSource(DataString name);

    public void setProcessingState(DataString state);

    public DataDate getTimeStamp();
    public void setTimeStamp(DataDate date);
}
