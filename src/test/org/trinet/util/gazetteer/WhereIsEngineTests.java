package test.org.trinet.util.gazetteer;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.trinet.jasi.TestDataSource;
import org.trinet.util.gazetteer.WhereIsEngine;
import test.common.DatabaseTests;
import test.testqueries.common.TestQueries;

import static org.junit.jupiter.api.Assertions.*;

public class WhereIsEngineTests extends DatabaseTests {

    @BeforeAll()
    public static void addDataToDatabase() throws Exception {
        if (!System.getProperty("java.version").startsWith("1.8")) {
            TestQueries testQueries = new TestQueries(postgreSQLContainer.getJdbcUrl());
            int rows = testQueries.populateGazetteerLineDataForWhereIsEngineTest();
            assertTrue(rows > 0);
        }
    }

    @Test
    public void verify_where_is_engine_where_function_returns_correct_value() throws Exception {
        if (!System.getProperty("java.version").startsWith("1.8")) {
            double dlat = 33.3427;
            double dlon = -116.3117;
            double z = 16.0;
            TestDataSource.create(postgreSQLContainer.getHost(), getDbName(),
                    getDbUsername(), getDbPassword(), getSubProtocol(), getPortNumber(), getDatabaseDriver());      // make connection

            WhereIsEngine whereEngine =
                    WhereIsEngine.create("org.trinet.util.gazetteer.TN.WheresFrom");
            whereEngine.setDistanceUnitsMiles();

            String whereString = whereEngine.where(dlat, dlon, z);

            assertEquals("\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "   4.3 km (   2.6 mi) S   ( 178. azimuth) from San Jacinto Fault\n", whereString);
        }
    }
}
