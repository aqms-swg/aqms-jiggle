package test.org.trinet.apps;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.trinet.apps.MakeGMP;
import test.common.DatabaseTests;
import test.testqueries.common.TestQueries;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MakeGmpTests extends DatabaseTests {

    public static final int MakeGMPTestEventId = 60172216;
    public static final int MakeGMPTestOrId = 171036;
    public static final int MakeGMPTestAmpId = 22063146;
    public static final int MakeGMPTestPreMag = 369891;
    public static final int MakeGMPTestPreMec = 16023;

    @BeforeAll
    public static void populateEventDataForMakeGMPTest() throws Exception {
        TestQueries testQueries = new TestQueries(postgreSQLContainer.getJdbcUrl());
        int rows = testQueries.populateOriginDataForMakeGmpTest();
        assertTrue(rows > 0);
        rows = testQueries.populateEventDataForMakeGmpTest();
        assertTrue(rows > 0);
        rows = testQueries.populateAmpDataForMakeGmpTest();
        assertTrue(rows > 0);
        rows = testQueries.populateAssoCamoDataForMakeGmpTest();
        assertTrue(rows > 0);
    }

    @Test
    public void verify_makegmp_main_function_output_contains_correct_value() {
        MakeGMP.main(new String[]{String.valueOf(MakeGMPTestEventId), getHost(), getDbName(), getDbUsername(), getDbPassword(),
                getSubProtocol(), getPortNumber(), getTestPropFilePath() + "jiggle.props"});
        assertTrue(MakeGMP.getMegasStr().contains("QID: " + MakeGMPTestEventId + " 004014100:--"));
    }
}
