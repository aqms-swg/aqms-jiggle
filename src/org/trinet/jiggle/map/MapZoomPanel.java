//package com.bbn.openmap.gui;
package org.trinet.jiggle.map;
import java.util.Properties;
import java.util.Iterator;

import com.bbn.openmap.PropertyConsumer;
import com.bbn.openmap.gui.ZoomPanel;
import com.bbn.openmap.util.PropUtils;
//import com.bbn.openmap.util.Debug;

public class MapZoomPanel extends com.bbn.openmap.gui.ZoomPanel {

    //public final static String defaultKey = "mapZoomPanel";
    //public final static String ZOOM_IN_FACTOR_PROPERTY = "zoomInFactor";
    //public final static String ZOOM_OUT_FACTOR_PROPERTY = "zoomOutFactor";
    public final static String ZOOM_SCALE_FACTOR_PROPERTY = "zoomScaleFactor";

    protected transient float zoomScaleFactor = 2.f; 

    public MapZoomPanel() {
        super();
        //setPropertyPrefix("mapZoomPanel");
    }

    @Override
   public void setProperties(String prefix, Properties props) {
        super.setProperties(prefix, props);

        prefix = PropUtils.getScopedPropertyPrefix(prefix);

        String str = props.getProperty(prefix + ZOOM_SCALE_FACTOR_PROPERTY, "2.");
        if (str != null) {
            zoomScaleFactor = Float.parseFloat(str);
            setZoomOutFactor(zoomScaleFactor);
            setZoomInFactor(1.f/zoomScaleFactor);
        }
        /*
        String str = props.getProperty(prefix + ZOOM_IN_FACTOR_PROPERTY);
        if (str != null) setZoomInFactor(Float.parseFloat(str));
        str = props.getProperty(prefix + ZOOM_OUT_FACTOR_PROPERTY);
        if (str != null) setZoomOutFactor(Float.parseFloat(str));
        */
    }

    @Override
    public Properties getProperties(Properties props) {
        props = super.getProperties(props);

        //props.put(PropUtils.getScopedPropertyPrefix(this) + ZOOM_IN_FACTOR_PROPERTY, String.valueOf(zoomInFactor));
        //props.put(PropUtils.getScopedPropertyPrefix(this) + ZOOM_OUT_FACTOR_PROPERTY, String.valueOf(zoomInFactor));
        props.put(PropUtils.getScopedPropertyPrefix(this) + ZOOM_SCALE_FACTOR_PROPERTY, String.valueOf(zoomScaleFactor));

        return props;
    }

    @Override
    public Properties getPropertyInfo(Properties props) {
        props = super.getPropertyInfo(props);

        //props.put(ZOOM_IN_FACTOR_PROPERTY, "Zoom in scale factor.");
        //props.put(ZOOM_OUT_FACTOR_PROPERTY, "Zoom out scale factor.");
        props.put(ZOOM_SCALE_FACTOR_PROPERTY, "Zoom scale factor.");

        return props;
    }

}
