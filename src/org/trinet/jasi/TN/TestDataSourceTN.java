package org.trinet.jasi.TN;

import org.trinet.jasi.*;
import org.trinet.jdbc.datasources.AbstractSQLDataSource;

public class TestDataSourceTN extends TestDataSource {

    private static final String str1 = "dbbrow";
    private static final String str2 = "dbbrow";

    public TestDataSourceTN() {
        this("databasert");
    }

    public TestDataSourceTN(String host) {
        this(host, "databasert");
    }

    public TestDataSourceTN(String host, String dbasename) {
        this(host, dbasename, str2, str1);
    }

    public TestDataSourceTN(String host, String dbasename, String username, String password) {
        super("jdbc:" + AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL + ":@" + host + ":" +
                        AbstractSQLDataSource.DEFAULT_DS_PORT + ":" + dbasename,
                AbstractSQLDataSource.DEFAULT_DS_DRIVER, username, password);
        setWriteBackEnabled(true);
    }

    public TestDataSourceTN(String host, String dbasename, String username, String password, String subProtocol) {
        super("jdbc:" + subProtocol + "://" + host + ":" +
                        AbstractSQLDataSource.DEFAULT_DS_PORT + "/" + dbasename,
                AbstractSQLDataSource.DEFAULT_DS_DRIVER, username, password);
        setWriteBackEnabled(true);
    }

    public TestDataSourceTN(String host, String dbasename, String username, String password, String subProtocol, String port) {
        super("jdbc:" + subProtocol + (subProtocol.equals(AbstractSQLDataSource.POSTGRESQL_DS_SUBPROTOCOL) ? "://" : ":@") + host + ":"
                        + port + (subProtocol.equals(AbstractSQLDataSource.POSTGRESQL_DS_SUBPROTOCOL) ? "/" : ":") + dbasename,
                AbstractSQLDataSource.DEFAULT_DS_DRIVER, username, password);
        setWriteBackEnabled(true);
    }

    public TestDataSourceTN(String host, String dbasename, String username, String password, String subProtocol, String port, String driver) {
        super("jdbc:" + subProtocol + (subProtocol.equals(AbstractSQLDataSource.POSTGRESQL_DS_SUBPROTOCOL) ? "://" : ":@") + host + ":"
                        + port + (subProtocol.equals(AbstractSQLDataSource.POSTGRESQL_DS_SUBPROTOCOL) ? "/" : ":") + dbasename,
                driver, username, password);
        setWriteBackEnabled(true);
    }

    public TestDataSourceTN(String host, String dbasename, String username, String password, String subProtocol, String port, String driver, String dbName) {
        super("jdbc:" + subProtocol + (subProtocol.equals(AbstractSQLDataSource.POSTGRESQL_DS_SUBPROTOCOL) ? "://" : ":@") + host + ":"
                        + port + (subProtocol.equals(AbstractSQLDataSource.POSTGRESQL_DS_SUBPROTOCOL) ? "/" : ":") + dbasename,
                driver, username, password, dbName);
        setWriteBackEnabled(true);
    }
}
