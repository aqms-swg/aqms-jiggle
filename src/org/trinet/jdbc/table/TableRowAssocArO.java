package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see AssocArO
*/
public interface TableRowAssocArO extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "ASSOCARO";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  20;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "";


/**  ASSOCARO table "orid" column data object offset in collection stored by implementing class.
*/
    public static final int ORID = 0;

/**  ASSOCARO table "arid" column data object offset in collection stored by implementing class.
*/
    public static final int ARID = 1;

/**  ASSOCARO table "commid" column data object offset in collection stored by implementing class.
*/
    public static final int COMMID = 2;

/**  ASSOCARO table "auth" column data object offset in collection stored by implementing class.
*/
    public static final int AUTH = 3;

/**  ASSOCARO table "subsource" column data object offset in collection stored by implementing class.
*/
    public static final int SUBSOURCE = 4;

/**  ASSOCARO table "iphase" column data object offset in collection stored by implementing class.
*/
    public static final int IPHASE = 5;

/**  ASSOCARO table "importance" column data object offset in collection stored by implementing class.
*/
    public static final int IMPORTANCE = 6;

/**  ASSOCARO table "delta" column data object offset in collection stored by implementing class.
*/
    public static final int DELTA = 7;

/**  ASSOCARO table "seaz" column data object offset in collection stored by implementing class.
*/
    public static final int SEAZ = 8;

/**  ASSOCARO table "in_wgt" column data object offset in collection stored by implementing class.
*/
    public static final int IN_WGT = 9;

/**  ASSOCARO table "wgt" column data object offset in collection stored by implementing class.
*/
    public static final int WGT = 10;

/**  ASSOCARO table "timeres" column data object offset in collection stored by implementing class.
*/
    public static final int TIMERES = 11;

/**  ASSOCARO table "EMA" column data object offset in collection stored by implementing class.
*/
    public static final int EMA = 12;

/**  ASSOCARO table "SLOW" column data object offset in collection stored by implementing class.
*/
    public static final int SLOW = 13;

/**  ASSOCARO table "vmodelid" column data object offset in collection stored by implementing class.
*/
    public static final int VMODELID = 14;

/**  ASSOCARO table "scorr" column data object offset in collection stored by implementing class.
*/
    public static final int SCORR = 15;

/**  ASSOCARO table "sdelay" column data object offset in collection stored by implementing class.
*/
    public static final int SDELAY = 16;

/**  ASSOCARO table "rflag" column data object offset in collection stored by implementing class.
*/
    public static final int RFLAG = 17;

/**  ASSOCARO table "ccset" column data object offset in collection stored by implementing class.
*/
    public static final int CCSET = 18;

/**  ASSOCARO table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 19;
/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES = "ORID,ARID,COMMID,AUTH,SUBSOURCE,IPHASE,IMPORTANCE,DELTA,SEAZ,IN_WGT,WGT,TIMERES,EMA,SLOW,VMODELID,SCORR,SDELAY,RFLAG,CCSET,LDDATE";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    "ASSOCARO.ORID,ASSOCARO.ARID,ASSOCARO.COMMID,ASSOCARO.AUTH,ASSOCARO.SUBSOURCE,ASSOCARO.IPHASE,ASSOCARO.IMPORTANCE,ASSOCARO.DELTA,ASSOCARO.SEAZ,ASSOCARO.IN_WGT,ASSOCARO.WGT,ASSOCARO.TIMERES,ASSOCARO.EMA,ASSOCARO.SLOW,ASSOCARO.VMODELID,ASSOCARO.SCORR,ASSOCARO.SDELAY,ASSOCARO.RFLAG,ASSOCARO.CCSET,ASSOCARO.LDDATE";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"ORID", "ARID", "COMMID", "AUTH", "SUBSOURCE", 
	"IPHASE", "IMPORTANCE", "DELTA", "SEAZ", "IN_WGT", 
	"WGT", "TIMERES", "EMA", "SLOW", "VMODELID",
        "SCORR", "SDELAY", "RFLAG", "CCSET", "LDDATE"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, false, true, true, true, 
	true, true, true, true, true, 
	true, true, true, true, true, 
	true, true, true, true, true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATALONG, DATALONG, DATALONG, DATASTRING, DATASTRING, 
	DATASTRING, DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, 
	DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, DATALONG,
        DATADOUBLE, DATADOUBLE, DATASTRING, DATASTRING, DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0, 1};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 0, 0, 0, 0, 0, 1, 1, 1, 3, 3, 2, 3, 2, 3, 0, 4, 4, 0, 0, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	15, 15, 15, 15, 8, 8, 2, 5, 4, 4, 4, 5, 5, 8, 3, 6, 7, 2, 1, 7
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, null, null, null, null, null, null, null, 
	null, null, null, null, null, null, null, null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))"
    };
}
