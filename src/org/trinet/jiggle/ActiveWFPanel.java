package org.trinet.jiggle;

// WFPanel.java

import java.applet.*;
import java.text.*;
import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import org.trinet.jasi.*;
import org.trinet.jasi.coda.*;
import org.trinet.jasi.engines.MagnitudeEngineIF;
import org.trinet.jasi.magmethods.MagnitudeMethodIF;
import org.trinet.jasi.picker.AbstractPicker;
import org.trinet.jasi.picker.PhasePickerIF;
import org.trinet.jasi.picker.PickerParmIF;
import org.trinet.util.*;
import org.trinet.util.graphics.text.JTextClipboardPopupMenu;

/** This class it the graphical representation of one waveform. It is different
 from a WFPanel in that it responds to mouse events and notifies the
 MasterWFVModel.  It is both a 'controller' and a 'view' in the MasterWFVModel
 MVC set.     <p>

 It also listens for change events from the phase and amplitude lists and will
 update in response.


Explanation of view areas used here.

Each "box" defines a 2-D area in time-amp space. Time is in seconds and amp is
in counts. These boxes are used to define and map time-amp areas into graphical
areas defined by 2-D boxes defined in terms of pixels in Height and Width.

<pre>
We use 3 WFSelectionBox'es:
        1) dataBox - defines the extent of the waveform data in time and amp.
           If there is no waveform this is null as tested by 'dataBox.isNull()'
        2) panelBox - defines the time-amp bounds of this WFPanel. Normally it is
           the same as the dataBox for a zoomPanel. For a WFPanel in a
           GroupPanel the panelBox time bounds will usually be equal to the
           exteme bounds of the ViewList. This insures all data is visable and
           time aligned. For dataless WFPanels the amp dimension is problematic
           and is arbitrarily set to +100/-100 counts.
        3) MasterWFViewBox - defines the bounds of the view. For example, in a
           zoomedpanel the viewbox represents the JViewport and the WFPanel expands
           to accomodate the scaling of the viewBox to the JViewport. This is
           only used by ActiveWFPanels.

     panelBox
    +===================================================================+
    |        +-----------------------------------------------+          |
    |        | MasterWFViewBox       +-----------------+     |          |
    |        |                       |dataBox          |     |          |
    |        |                       |                 |     |          |
    |        |                       +-----------------+     |          |
    |        +-----------------------------------------------+          |
    +===================================================================+


    .               +----------------------+
     panelBox       | MasterWFViewBox      |
    +===============+======================+============================+
    |               |                      |                            |
    |               |           +----------+------+                     |
    |               |           |dataBox   |      |                     |
    |               |           |          |      |                     |
    |               |           +----------+------+                     |
    |               +----------------------+                            |
    +===================================================================+
</pre>
*/

public class ActiveWFPanel extends WFPanel {

    double scanWindowStartTime = 0.;
    double scanWindowEndTime = 0.;
    String lastScanType = "";
    int phasePickFlag = PhasePickerIF.P_ONLY;

    protected static boolean confirmActionPopup = true;
    protected static boolean nestedStripMenus = true;

    static final int maskA1 = MouseEvent.ALT_MASK | MouseEvent.BUTTON1_MASK;
    static final int maskA2 = MouseEvent.ALT_MASK | MouseEvent.BUTTON2_MASK;
    static final int maskC1 = MouseEvent.CTRL_MASK | MouseEvent.BUTTON1_MASK;
    static final int maskC2 = MouseEvent.CTRL_MASK | MouseEvent.BUTTON2_MASK;
    static final int maskM1 = MouseEvent.META_MASK | MouseEvent.BUTTON1_MASK;
    static final int maskM2 = MouseEvent.META_MASK | MouseEvent.BUTTON2_MASK;

    static final int resetScanTimeMask = MouseEvent.CTRL_DOWN_MASK | MouseEvent.ALT_DOWN_MASK;

    protected static int codaSelectMask = maskC1;

    protected HashMap keyStatusMap = null;

    /** Bounds of the actual data in the WFView */
    WFSelectionBox dragBox = new WFSelectionBox();

    // The following mouse variables are here rather than in the mouse classes
    // because we need them in both MouseAdapter and MouseMotionAdapter
    /** Mouse drag status.*/
    boolean amDragging = false;
    /** Mouse drag start/stop coordinates*/
    int x1, x2, y1, y2;
    static final int sloppyDragX = 3; // drag must be more than this many pixels
    static final int sloppyDragY = 3; // drag must be more than this many pixels

    //boolean wasDragged = false;
    Point startPoint = new Point();      // scoped here to be seen by both mouse listeners
    Point mouseClickPoint = new Point();
    boolean mouseDown = false;
    boolean dragAmp = false;

    /** Used for dynamic cursor location views (MVC) */
    CursorLocModel cursorLocModel = null;

    // listen for changes to selected channel or time/amp window
    private WFWindowListener wfwindowListener = new WFWindowListener();
    //private WFViewListener wfviewListener = new WFViewListener(); // too much overhead for 1000 panels+ ?

    private JasiReadingChangeListener readingChangeListener = new JasiReadingChangeListener();

    static {
        if ( System.getProperty("os.name","").startsWith("Mac") ) {
            codaSelectMask = maskC1;
        }
    }

    public ActiveWFPanel() {
        super();
        addMouseListener ( new MsHandler() );                // mouse button handler
        addMouseMotionListener ( new MsMoveHandler() );        // mouse motion handler
        addKeyListener( new UserKeyListener() );
    }

    public ActiveWFPanel(WFView wfview)  {
        this ();
        setWFView(wfview);
    }
    public void setScanWindowTimes( double start, double end) {
        scanWindowStartTime = start;
        scanWindowEndTime = end;
        lastScanType = "";
    }

    private class UserKeyListener implements KeyListener {

        public void keyPressed(KeyEvent evt)  {
            processEvent(evt, "PRESSED");
        }

        public void keyReleased(KeyEvent evt) {
            processEvent(evt, "RELEASED");
        }

        public void keyTyped(KeyEvent evt) {
            processEvent(evt, "TYPED");
        }

        private void processEvent(KeyEvent evt, String state) {

            String keyState = state;

            if ( keyStatusMap == null ) keyStatusMap = new HashMap(7);
            //You should only rely on the key char if the event is a key typed event.
            String keyString = KeyEvent.getKeyText(evt.getKeyCode());
            if ( keyState.equals("PRESSED") ) {
                keyStatusMap.put(keyString, evt); 
            }
            else if ( keyState.equals("RELEASED") ) {
                keyStatusMap.remove(keyString); 
                //if ( evt2 == null ) System.out.println("No key in map event for: " +  keyString);
            }
            else if ( keyState.equals("TYPED") ) {
                keyStatusMap.remove(keyString); 
                //if ( evt2 == null ) System.out.println("No key in map event for: " +  keyString);
            }
            //Sytem.out.println("processEvent keyStatusMap: " + keyStatusMap):
        }
    }
         
    private String getKeyEvtMods(KeyEvent evt) { 
            int modifiersEx = evt.getModifiersEx();
            return KeyEvent.getModifiersExText(modifiersEx);
    }

    private String getKeyEvtLoc(KeyEvent evt) { 
            int location = evt.getKeyLocation();
            if (location == KeyEvent.KEY_LOCATION_STANDARD) {
                return "standard";
            } else if (location == KeyEvent.KEY_LOCATION_LEFT) {
                return "left";
            } else if (location == KeyEvent.KEY_LOCATION_RIGHT) {
                return "right";
            } else if (location == KeyEvent.KEY_LOCATION_NUMPAD) {
                return "numpad";
            } 
            return "unknown";
    }
         
    protected void createNoiseReading(TimeSpan ts, int filterMask) {
        Waveform wf = getRawWf();
        String filterStr = "";
        if ((filterMask & MouseEvent.SHIFT_MASK) == MouseEvent.SHIFT_MASK) {
            // use panel filtered wf instead of raw wf, e.g. Butterworth
            wf = getAlternateWf();
            if (wf == null) {
                wf = createAlternateWf(); // filter panel's raw wf using currently set filter (e.g. Butterworth HP)
            }
            filterStr = (wf == null) ? "" : wf.getFilterName();
        }  // end of test for use of alternate wf, shift pressed
        if (wf == null || ! wf.hasTimeSeries()) {
            JOptionPane.showMessageDialog(getTopLevelAncestor(),
                    "Filtered timeseries not found, First apply filter.",
                    "Timespan Noise Level", JOptionPane.PLAIN_MESSAGE);
            setScanWindowTimes(0.,0.);
            return;
        }
        float noise = wf.scanForNoiseLevel(ts);
        String wfUnits = Units.getString(wf.getAmpUnits());

        ChannelGain cg = wfv.getChannelObj().getCurrentGain();
        double gain  = (cg.isValidNumber()) ?  Math.abs(cg.doubleValue()) : 1.;
        String gUnits = wfUnits;
        if (cg.isVelocity()) gUnits = "vel";
        else if (cg.isAcceleration()) gUnits = "acc"; 

        final String noiseStr = String.format("noise level: %10.3f %-8s %10.5e %-8s %s", noise, wfUnits, noise/gain, gUnits, ts);
        final String filterTag = filterStr;
        SwingUtilities.invokeLater( new Runnable() {
            public void run() {
                JOptionPane.showMessageDialog(getTopLevelAncestor(), noiseStr, "Timespan Noise Level", JOptionPane.PLAIN_MESSAGE);
                System.out.printf("INFO: %s %-16s %s%n", noiseStr, wfv.getChannelObj().toDelimitedSeedNameString("."), filterTag); 
            }
        });

    }

    protected void createPhaseReading(TimeSpan ts, int filterMask, int phaseFlag) {

        if (wfv == null ||  wfv.mv == null) return;

        Object obj =  wfv.mv.getOwner();
        if (!(obj instanceof Jiggle)) return; 
        Jiggle jiggle = (Jiggle) obj;

        Solution sol = wfv.mv.getSelectedSolution(); 
        if (sol == null) return;

        AbstractPicker picker = (AbstractPicker) jiggle.getPhasePicker();
        if (picker == null) jiggle.setupPicker();
        else if (!picker.hasChannelParms()) jiggle.loadPickerChannelParms();
        picker = (AbstractPicker) jiggle.getPhasePicker();

        boolean status = false;
        String statusStr = "";

        if (picker != null) {

            if (!picker.hasChannelParms()) {
                JOptionPane.showMessageDialog(jiggle,
                    "Click \"OK\" after picker's parms loading dialog disappears",
                    "Loading Picker Parms", JOptionPane.PLAIN_MESSAGE);
                jiggle.loadPickerChannelParms(); // not done by setup
                //return;
            }

            Waveform wf = getRawWf();

            if ((filterMask & MouseEvent.SHIFT_MASK) == MouseEvent.SHIFT_MASK) {
                // use panel filtered wf instead of raw wf, e.g. Butterworth
                wf = getAlternateWf();

                if (wf == null) {
                  wf = createAlternateWf(); // filter panel's raw wf using currently set filter (e.g. Butterworth HP)
                }

                String filterName = (wf == null) ? "NULL" : wf.getFilterName();
                String text = "SHIFT pressed to create phase pick(s)" + " using panel's filtered waveform:" + filterName;
                if (wf == null || wf.getAmpUnits() != Units.COUNTS) { // filter output units must be counts for WA
                  text += " (try changing filter type, filter output units must be COUNTS).";
                  System.out.println(text);
                  JOptionPane.showMessageDialog(jiggle, text, "Phase Pick", JOptionPane.PLAIN_MESSAGE);
                  wf = null;
                  return;
                }
                if (wf == null || ! wf.hasTimeSeries()) {
                    JOptionPane.showMessageDialog(getTopLevelAncestor(),
                            "Filtered timeseries not found, First apply filter.",
                            "Create Phase Pick", JOptionPane.PLAIN_MESSAGE);
                    setScanWindowTimes(0.,0.);
                    return;
                }
                else System.out.println(text);
            }  // end of test for use of alternate wf, shift pressed

            if (wf != null) {
              if (!picker.reject(wf)) {

                // do distance here since a bulk engine magnitude calculation may have previously set the waveform distance or not 
                wf.getChannelObj().calcDistance(sol); // waveform distance null when it's first loaded into view - 11/26/2007 -aww

                int phFlag = phaseFlag;
                TimeSpan pSpan = null;
                TimeSpan sSpan = null;
                if (phaseFlag == PhasePickerIF.P_ONLY) {
                    //phFlag =  PhasePickerIF.P_ONLY;
                    pSpan= ts;
                    sSpan= new TimeSpan(Double.MAX_VALUE, Double.MAX_VALUE);
                }
                else if (phaseFlag == PhasePickerIF.S_ONLY) {
                    //phFlag = PhasePickerIF.S_ONLY;
                    pSpan= new TimeSpan(-Double.MAX_VALUE, -Double.MAX_VALUE);
                    sSpan= ts;
                }
                else if (phaseFlag == PhasePickerIF.P_AND_S ) {
                    phFlag = PhasePickerIF.P_AND_S;
                    pSpan= ts;
                    sSpan= ts;
                }

                TimeSpan scanSpan = new TimeSpan(ts.getStart()-picker.getScanStartOffset(), ts.getEnd()+picker.getScanEndOffset());
                //java.util.List aList =  picker.getChannelParms();
                //if (aList = null) aList = new ArrayList(0);
                //System.out.println( "Picker channel parm list size: " + aList.size());
                PickerParmIF parm =  picker.getChannelParm(wf);
                if (parm != null) {
                    PhaseList phList = picker.pick(wf, phFlag, parm, scanSpan, pSpan, sSpan);

                    if (phList.size() > 0) {
                      Phase ph = null;
                      for (int idx = 0; idx < phList.size(); idx++) {
                          ph = (Phase) phList.get(idx);
                          // Set state as "automatic"
                          //ph.setProcessingState(JasiProcessingConstants.STATE_AUTO_TAG); // new processing state for preferred trumps 
                          //System.out.println("DEBUG created new phase reading: "); 
                          //System.out.println(ph.toNeatString());
                          ph.assign(sol);
                          //sol.addOrReplace(ph);
                          wfv.mv.addReadingToCurrentSolution(ph, (idx == 0));
                      }
                      jiggle.updateStatusBarCounts();
                      status = true;
                    } // has pick?
                    else statusStr = " no valid picks";
                } // has parms?
                else statusStr = " picker parms undefined for channel";
              }// is included channel ?
              else statusStr = " picker does NOT allow this seedchan type";
            } // has waveform ?
            else statusStr = " null waveform in view";
        } // has picker ?
        else statusStr = " picker is null";

        if (! status)  {
            JOptionPane.showMessageDialog(jiggle,
                "Unable to add new phase(s) for this channel" + statusStr,
                "Autopick Phase", JOptionPane.PLAIN_MESSAGE);
            setScanWindowTimes(0.,0.);
        }
    }

    protected void createMagReading(String type, TimeSpan ts) {
        int filterMask = (getWf() == getAlternateWf()) ? MouseEvent.SHIFT_MASK : 0;
        createMagReading(type, ts, filterMask);
    }

    protected void createMagReading(String type, TimeSpan ts, int filterMask) {

        if (wfv == null ||  wfv.mv == null) return;

        Object obj =  wfv.mv.getOwner();
        if (!(obj instanceof Jiggle)) return; 

        Jiggle jiggle = (Jiggle) obj;

        Solution sol = wfv.mv.getSelectedSolution(); 
        if (sol == null) return;

        MagnitudeEngineIF magEng = jiggle.solSolverDelegate.initMagEngineForType(sol, ((type == "Amp") ? "ml" : "md")); 

        boolean status = false;
        String statusStr = "";

        if (magEng != null) {

          MagnitudeMethodIF magMeth = magEng.getMagMethod(); 
          //System.out.println("DEBUG ActiveWFPanel mag method name : " + magMeth.getMethodName() + " : " + magMeth.getClass().getName());

          if (magMeth != null) {

            Waveform wf = getRawWf();

            if (wf == null) { // is waveform null ?
                statusStr = " null waveform in view";
            } else if (magMeth.isIncludedComponent(wf)) {

              if ((filterMask & MouseEvent.SHIFT_MASK) == MouseEvent.SHIFT_MASK) {
                // use panel filtered wf instead of raw wf, e.g. Butterworth 12/06/2006 -aww
                wf = getAlternateWf();

                if (wf == null) {
                  wf = createAlternateWf(); // filter panel's raw wf using currently set filter (e.g. Butterworth HP)
                }

                String filterName = (wf == null) ? "NULL" : wf.getFilterName();
                String text = "SHIFT pressed to create " + type + " using panel's filtered waveform:" + filterName;
                if (wf == null || wf.getAmpUnits() != Units.COUNTS) { // filter output units must be counts for WA
                  System.out.println(text);
                  text += " (try changing filter type, filter output units must be COUNTS).";
                  System.out.println(text);
                  JOptionPane.showMessageDialog(jiggle, text, "Magnitude Data", JOptionPane.PLAIN_MESSAGE);
                  wf = null;
                  return;
                }
                if (wf == null || ! wf.hasTimeSeries()) {
                    JOptionPane.showMessageDialog(getTopLevelAncestor(),
                            "Filtered timeseries not found, First apply filter.",
                            "Create Magnitude Reading", JOptionPane.PLAIN_MESSAGE);
                    setScanWindowTimes(0.,0.);
                    return;
                }
                else System.out.println(text);
              }  // end of test for use of alternate wf, shift pressed

              // waveform is not null, checked above
              {
                // do distance here since a bulk engine magnitude calculation may have previously set the waveform distance or not 
                wf.getChannelObj().calcDistance(sol); // waveform distance null when it's first loaded into view - 11/26/2007 -aww


                MagnitudeAssocJasiReadingIF jr =
                   (MagnitudeAssocJasiReadingIF) magMeth.createAssocMagDataFromWaveform(sol, wf, ts);

                if (jr != null) {
                  Magnitude mag = null;
                  //System.out.println("DEBUG created new mag reading: "); 
                  if (jr instanceof Amplitude) {
                      mag = sol.getPrefMagOfType("l");
                      //System.out.println(Amplitude.getNeatStringHeader());
                  }
                  else if (jr instanceof Coda) {
                      mag = sol.getPrefMagOfType("d");
                      //System.out.println(Coda.getNeatStringHeader());
                  }
                  try { jr = magMeth.calcChannelMag(jr); } catch (WrongDataTypeException ex) { }
                  // Set state as "human" to trump automatic rejection
                  //jr.setProcessingState(JasiProcessingConstants.STATE_HUMAN_TAG); // jiggle automatically sets all readings as "H"
                  // BELOW NEEDS to have database table constraints for rflag revised - aww 11/08/2007
                  //jr.setProcessingState(JasiProcessingConstants.STATE_FINAL_TAG); // new processing state for preferred trumps 
                  jr.require(); // new processing state for preferred trumps 
                  //System.out.println(jr.toNeatString());
                  if (mag == null)  {
                      mag = magEng.getNewMag(sol);
                      mag.associate(jr);
                      mag.value.setValue(0.); // set "dummy" value, if null unable to add mag to solution below
                      sol.setPrefMagOfType(mag);
                  }
                  else {
                      jr.assign(mag);
                      jr.assign(sol);
                      JasiReading oldjr = (JasiReading) mag.getListFor(jr).addOrReplace(jr); // updates sol assignment for magnitude as of 11/28/2006
                      if (oldjr instanceof Amplitude) { // undo
                          wfv.mv.addToAmpUndoList((Amplitude)oldjr, true); // undo
                          sol.addOrReplace(jr);
                      }
                      else if (oldjr instanceof Coda) { // undo
                          wfv.mv.addToCodaUndoList((Coda)oldjr, true); // undo
                          sol.addOrReplace(jr);
                      }
                      //if (!mag.isPreferred()) { // see above comment 11/28/2006 -aww
                        //sol.addOrReplace(jr); // assumes reading is assigned sol in creation method
                      //}
                  }
                  //mag = magEng.calcSummaryMag(mag, false);
                  //System.out.println(mag.neatDump());
                  //System.out.println("Solution List:\n"+((JasiReadingList)sol.getListFor(jr)).toNeatString());
                  jiggle.updateMagTab(mag.getTypeString());
                  jiggle.updateStatusBarCounts();
                  status = true;
                } // is reading null?
                else {
                    statusStr = " : invalid seedchan, gain, period, amp(clip), timeseries(time-tear)";
                    if (type == "Coda") statusStr += ", or missing required phase pick";
                }
              }
            } // is included channel ?
            else statusStr = " mag method does NOT allow seedchan type";
          } // is magMethod null?
          else statusStr = " mag method is null";
        } // is magEngine null?

        if (! status)  {
            JOptionPane.showMessageDialog(jiggle,
                "Unable to create new " +type+ " for this channel" + statusStr,
                "Magnitude Data", JOptionPane.PLAIN_MESSAGE);
            setScanWindowTimes(0.,0.);
        }
    }

    /** Overrides WFPanel.setWFView(WFView wfv) so that ActiveWFPanels will
     * register with the MasterView's ActivePhaseList & masterWFVModel models.
     * This can only be done once the WFView is set because the WFView has the
     * reference to the MasterView.phaseList. */

    public void setWFView(WFView wfview) {
       setScanWindowTimes(0.,0.);
       if (wfview == wfv)  return;   // noop
       //System.out.println("DEBUG ActiveWFPanel setWFView view:" + ((wfview == null) ? "null" : wfview.toString()) );
       // if this is REPLACING a previous WFView we must clean up and remove the old listeners, etc.
       removeListeners();
       super.setWFView(wfview); // sets the MasterView and deals with WFLoadListener and revalidates (repaint queued)
       if (cursorLocModel != null) {
           cursorLocModel.setWFPanel(this); // TEST
       }
       addListeners();
    }

    public void clear() {
       setScanWindowTimes(0.,0.);
       removeListeners();
       removeWFViewListeners();
       dragBox.setNull();
       super.clear();
    }

    protected void addListeners() {
        addReadingListeners();
    }
    protected void removeListeners() {
        removeReadingListeners();
    }

    protected void selectView(boolean tf) {
        if (wfv != null) wfv.setSelected(tf);
        setRowHeaderBackground();
    }

/** Change this panel's selection state. The WFViewChangeListener and the
* WFWindowChangeListener are only registered for SELECTED WFPanels, otherwise
* every change of window must be processed by ALL WFPanels (> 1200). */
    public void setSelected(boolean tf) {

           if (tf == amSelected) return;   // no change, don't re-add listeners

           if (amSelected) {   // currently selected must be unselected now
              removeWFViewListeners();
              //setBackgroundColor(unselectedColor);
              amSelected = false;
              repaint();
           } else {
              addWFViewListeners();
              //setBackgroundColor(selectedColor);
              amSelected = true;
           }
//           repaint();
    }

    /** Add listeners to the default MasterView's WFView and WFWindow models  */
    protected void addWFViewListeners() {
       addWFViewListeners(wfv.mv);
    }

    /** Remove WF listeners  */
    protected void removeWFViewListeners() {
        removeWFViewListeners(wfv.mv);
    }

    /** Add listeners to this MasterView's WFView and WFWindow models  */
    protected void addWFViewListeners(MasterView mv) {
       if (mv == null) return;
       //if (mv.masterWFViewModel != null) {
           //mv.masterWFViewModel.removeChangeListener(wfviewListener);
           //mv.masterWFViewModel.addChangeListener(wfviewListener);
       //}
       if (mv.masterWFWindowModel != null) {
          mv.masterWFWindowModel.removeChangeListener(wfwindowListener);
          mv.masterWFWindowModel.addChangeListener(wfwindowListener);
       }
    }

    protected void removeWFViewListeners(MasterView mv) {
       if (mv == null) return;
       //if (mv.masterWFViewModel != null) mv.masterWFViewModel.removeChangeListener(wfviewListener);
       if (mv.masterWFWindowModel != null) mv.masterWFWindowModel.removeChangeListener(wfwindowListener);
       
   }
    /** Remove listeners from the WFView's reading lists (phases, amps, etc.)
    * These update the panel if phase, amps etc. are changed.   */
    protected void removeReadingListeners() {

         if (wfv == null) return;

       // listener for phase list changes
         if (wfv.phaseList != null)
             //wfv.phaseList.removeChangeListener(readingChangeListener); //aww
             wfv.phaseList.removeListDataStateListener(readingChangeListener);

       // listener for amp list changes
         if (wfv.ampList != null)
             //wfv.ampList.removeChangeListener(readingChangeListener); //aww
             wfv.ampList.removeListDataStateListener(readingChangeListener);

       // listener for amp list changes
         if (wfv.codaList != null)
             //wfv.codaList.removeChangeListener(readingChangeListener); //aww
             wfv.codaList.removeListDataStateListener(readingChangeListener);
    }

    /** Add listeners to the WFView's reading lists (phases, amps, etc.)
    * These update the panel if phase, amps etc. are changed. */
    protected void addReadingListeners() {

         if (wfv == null) return;

         removeReadingListeners(); // insurance

       // listener for phase list changes
         if (wfv.phaseList != null)
             //wfv.phaseList.addChangeListener(readingChangeListener); //aww
             wfv.phaseList.addListDataStateListener(readingChangeListener);

       // listener for amp list changes
         if (wfv.ampList != null)
             //wfv.ampList.addChangeListener(readingChangeListener); //aww
             wfv.ampList.addListDataStateListener(readingChangeListener);

       // listener for amp list changes
         if (wfv.codaList != null)
             //wfv.codaList.addChangeListener(readingChangeListener); //aww
             wfv.codaList.addListDataStateListener(readingChangeListener);
       }


    /** Set the CursorLocModel for dynamic display of cursor location */
    public void setCursorLocModel(CursorLocModel model) {
        cursorLocModel = model;
        cursorLocModel.wfp = this;
    }

    public void makePopup(Point point) {
        if (nestedStripMenus) doMakePopupNested(point, true);
        else doMakePopup(point, true);
    }

    protected void doMakePopupNested(Point point, boolean showAll) {

       if (wfv.mv == null) return;

       Solution sol = wfv.mv.getSelectedSolution();
       //if (sol == null) return;

       JPopupMenu popup = new JPopupMenu();

       DeletePopupListener deletePopupListener = null;
       StripPopupListener stripPopupListener = null;
       RejectPopupListener rejectPopupListener = null;
       RequirePopupListener reqPopupListener = null;

       JMenu requireMenu = null;
       JMenu rejectMenu = null;
       JMenu deleteMenu = null;
       JMenu stripMenu = null;

       //boolean hasPhases = wfv.hasPhases();
       //boolean hasAmps = wfv.hasAmps();
       //boolean hasCodas = wfv.hasCodas();

       boolean hasPhases = sol.getPhaseList().size() > 0;
       boolean hasAmps = sol.getAmpList().size() > 0;
       boolean hasCodas = sol.getCodaList().size() > 0;

       Object obj = wfv.mv.getOwner();
       if (obj instanceof Jiggle) {
          final Jiggle jiggle = (Jiggle) obj;
          WFScroller wfs = jiggle.getWFScroller();
          if (wfs != null && wfs.groupPanel != null && wfs.jbTriaxial != null && wfs.jbTriaxial.isSelected()) {
              if (jiggle.pickPanel != null) {
                  final JMenuItem jmi = new JMenuItem("Pick...");
                  popup.add(jmi);
                  popup.addSeparator();
                  jmi.addActionListener(new ActionListener() {
                      public void actionPerformed(ActionEvent evt) {
                        //Point xy = new Point(jmi.getPreferredSize().width/2, 0);
                        //new PhasePopup(jmi, jiggle.pickPanel.zwfp, xy);
                        Point xy = new Point(mouseClickPoint.x+100,mouseClickPoint.y);
                        new PhasePopup(ActiveWFPanel.this, Jiggle.jiggle.pickPanel.zwfp, xy);
                      }
                  });
              }
          }
       }

       if (hasPhases || hasAmps || hasCodas) {

         deletePopupListener = new DeletePopupListener(wfv.mv);
         deleteMenu = new JMenu("Delete...");
         deleteMenu.setToolTipText("permanently delete observations from lists");

         popup.add(deleteMenu);

         rejectPopupListener = new RejectPopupListener(wfv.mv);
         rejectMenu = new JMenu("Reject...");
         rejectMenu.setToolTipText("from summary, don't delete");
         popup.add(rejectMenu);

         stripPopupListener = new StripPopupListener(wfv.mv);
         stripMenu = new JMenu("Strip...");
         stripMenu.setToolTipText("delete by km or residual");
         popup.add(stripMenu);

         reqPopupListener = new RequirePopupListener(wfv.mv);
         requireMenu = new JMenu("Require...");
         requireMenu.setToolTipText("stripping actions should not delete it");
         popup.add(requireMenu);

       }

       JMenuItem mi = null;
       int typeCount = 0;
       boolean addSep = false;

       if (hasPhases) {
         mi = deleteMenu.add(new JMenuItem("Delete Pick"));
         mi.addActionListener(deletePopupListener);
         addSep = true;
       }

       if (hasAmps) {
         mi = deleteMenu.add(new JMenuItem("Delete Amp"));
         mi.addActionListener(deletePopupListener);
         addSep = true;
       }

       if (hasCodas) {
         mi = deleteMenu.add(new JMenuItem("Delete Coda"));
         mi.addActionListener(deletePopupListener);
         addSep = true;
       }

       if (addSep ) {
         deleteMenu.addSeparator();
         addSep = false;
       }

       // Place of right-click
       double clickTime =  dtOfPixel(mouseClickPoint);
       JasiReadingIF jr = null;

       if (hasPhases && sol != null) {
         jr = wfv.phaseList.getNearestToTime(clickTime, sol);
         if (jr != null) {
           mi = rejectMenu.add(new JMenuItem("Reject Pick"));
           mi.addActionListener(rejectPopupListener);
           mi.setEnabled(!jr.isReject());

           mi = rejectMenu.add(new JMenuItem("Unreject Pick"));
           mi.addActionListener(rejectPopupListener);
           mi.setEnabled(jr.isReject());
           addSep = true;
         }
       }

       if (hasAmps && sol != null) {
         jr = wfv.ampList.getNearestToTime(clickTime, sol);
         if (jr != null) {
           mi = rejectMenu.add(new JMenuItem("Reject Amp"));
           mi.addActionListener(rejectPopupListener);
           mi.setEnabled(!jr.isReject());

           mi = rejectMenu.add(new JMenuItem("Unreject Amp"));
           mi.addActionListener(rejectPopupListener);
           mi.setEnabled(jr.isReject());
           addSep = true;
         }
       }

       if (hasCodas && sol != null) {
         jr = wfv.codaList.getNearestToTime(clickTime, sol);
         if (jr != null) {
           mi = rejectMenu.add(new JMenuItem("Reject Coda"));
           mi.addActionListener(rejectPopupListener);
           mi.setEnabled(!jr.isReject());

           mi = rejectMenu.add(new JMenuItem("Unreject Coda"));
           mi.addActionListener(rejectPopupListener);
           mi.setEnabled(jr.isReject());
           addSep = true;
         }
       }

       if (addSep ) {
         rejectMenu.addSeparator();
         addSep = false;
       }

       if (hasPhases && sol != null) {
         jr = wfv.phaseList.getNearestToTime(clickTime, sol);
         if (jr != null) {
           mi = requireMenu.add(new JMenuItem("Require Pick"));
           mi.addActionListener(reqPopupListener);
           mi.setEnabled(!jr.isFinal());

           mi = requireMenu.add(new JMenuItem("Unrequire Pick"));
           mi.addActionListener(reqPopupListener);
           mi.setEnabled(jr.isFinal());
           addSep = true;
         }
       }

       if (hasAmps && sol != null) {
         jr = wfv.ampList.getNearestToTime(clickTime, sol);
         if (jr != null) {
           mi = requireMenu.add(new JMenuItem("Require Amp"));
           mi.addActionListener(reqPopupListener);
           mi.setEnabled(!jr.isFinal());

           mi = requireMenu.add(new JMenuItem("Unrequire Amp"));
           mi.addActionListener(reqPopupListener);
           mi.setEnabled(jr.isFinal());
           addSep = true;
         }
       }

       if (hasCodas && sol != null) {
         jr = wfv.codaList.getNearestToTime(clickTime, sol);
         if (jr != null) {
           mi = requireMenu.add(new JMenuItem("Require Coda"));
           mi.addActionListener(reqPopupListener);
           mi.setEnabled(!jr.isFinal());

           mi = requireMenu.add(new JMenuItem("Unrequire Coda"));
           mi.addActionListener(reqPopupListener);
           mi.setEnabled(jr.isFinal());
           addSep = true;
         }
       }

       if (addSep ) {
         requireMenu.addSeparator();
         addSep = false;
       }

       int phaseCnt = (sol == null) ? 0 : sol.getPhaseList().size();
       int ampCnt = (sol == null) ? 0 : sol.getAmpList().size();
       int codaCnt = (sol == null) ? 0 : sol.getCodaList().size();

       if (phaseCnt > 0 || ampCnt > 0 || codaCnt > 0 ) {
         if (deleteMenu == null) {
           deletePopupListener = new DeletePopupListener(wfv.mv);
           deleteMenu = new JMenu("Delete...");
           deleteMenu.setToolTipText("permanently delete observations from lists");
           popup.add(deleteMenu);
         }
         if (rejectMenu == null) {
           rejectPopupListener = new RejectPopupListener(wfv.mv);
           rejectMenu = new JMenu("Reject...");
           rejectMenu.setToolTipText("from summary, but don't delete");
           popup.add(rejectMenu);
         }
         if (stripMenu == null) {
           stripPopupListener = new StripPopupListener(wfv.mv);
           stripMenu = new JMenu("Strip...");
           stripMenu.setToolTipText("delete by km or residual");
           popup.add(stripMenu);
         }
         //
         // No global forcing all required or not?
         //
         //if (ampCnt > 0 || codaCnt > 0 ) {
         //  if (requireMenu == null) {
         //    reqPopupListener = new RequirePopupListener(wfv.mv);
         //    requireMenu = new JMenu("Require...");
         //    requireMenu.setToolTipText("amp or coda, summary magnitude QC should not delete it");
         //    popup.add(requireMenu);
         //  }
         //}
         //
       }

       if (showAll) {
         if (phaseCnt > 0) {
           mi = deleteMenu.add(new JMenuItem("Delete All Picks"));
           mi.addActionListener(deletePopupListener);

           mi = deleteMenu.add(new JMenuItem("Delete All Auto Picks"));
           mi.addActionListener(deletePopupListener);

           addSep = true;
           typeCount++;
         }
         if (ampCnt > 0) {
           mi = deleteMenu.add(new JMenuItem("Delete All Amps"));
           mi.addActionListener(deletePopupListener);

           mi = deleteMenu.add(new JMenuItem("Delete All Auto Amps"));
           mi.addActionListener(deletePopupListener);

           addSep = true;
           typeCount++;
         }
         if (codaCnt > 0) {
           mi = deleteMenu.add(new JMenuItem("Delete All Coda"));
           mi.addActionListener(deletePopupListener);

           mi = deleteMenu.add(new JMenuItem("Delete All Auto Coda"));
           mi.addActionListener(deletePopupListener);

           addSep = true;
           typeCount++;
         }
         if (addSep) {
           if (typeCount > 1) {
             mi = deleteMenu.add(new JMenuItem("Delete All"));
             mi.addActionListener(deletePopupListener);

             mi = deleteMenu.add(new JMenuItem("Delete All Auto"));
             mi.addActionListener(deletePopupListener);
           }
           deleteMenu.addSeparator();
           addSep = false;
         }
       }

       if (phaseCnt > 0) {
         mi = stripMenu.add(new JMenuItem("Delete more distant picks"));
         mi.addActionListener(stripPopupListener);

         mi = stripMenu.add(new JMenuItem("Delete more distant auto picks"));
         mi.addActionListener(stripPopupListener);

         mi = stripMenu.add(new JMenuItem("Delete more distant auto S picks"));
         mi.addActionListener(stripPopupListener);

         mi = stripMenu.add(new JMenuItem("Delete picks by residual"));
         mi.addActionListener(stripPopupListener);
         stripMenu.addSeparator();
       }

       if (ampCnt > 0) {
         mi = stripMenu.add(new JMenuItem("Delete more distant amps"));
         mi.addActionListener(stripPopupListener);

         mi = stripMenu.add(new JMenuItem("Delete amps by residual"));
         mi.addActionListener(stripPopupListener);
         stripMenu.addSeparator();

         //
         //mi = requireMenu.add(new JMenuItem("Require all amps"));
         //mi.addActionListener(reqPopupListener);

         //mi = requireMenu.add(new JMenuItem("Unrequire all amps"));
         //mi.addActionListener(reqPopupListener);
         //requireMenu.addSeparator();
         //

       }

       if (codaCnt > 0) {
         mi = stripMenu.add(new JMenuItem("Delete more distant codas"));
         mi.addActionListener(stripPopupListener);

         mi = stripMenu.add(new JMenuItem("Delete codas by residual"));
         mi.addActionListener(stripPopupListener);
         stripMenu.addSeparator();

         //
         //mi = requireMenu.add(new JMenuItem("Require all codas"));
         //mi.addActionListener(reqPopupListener);

         //mi = requireMenu.add(new JMenuItem("Unrequire all codas"));
         //mi.addActionListener(reqPopupListener);
         //requireMenu.addSeparator();
         //

       }

       if (showAll) {
         if (typeCount > 1) { 
           mi = stripMenu.add(new JMenuItem("Delete all more distant"));
           mi.addActionListener(stripPopupListener);

           mi = stripMenu.add(new JMenuItem("Delete all auto more distant"));
           mi.addActionListener(stripPopupListener);

           mi = stripMenu.add(new JMenuItem("Delete all by residual"));
           mi.addActionListener(stripPopupListener);
           stripMenu.addSeparator();
         }
       }

       if (phaseCnt > 0) {
         mi = rejectMenu.add(new JMenuItem("Reject more distant picks"));
         mi.addActionListener(rejectPopupListener);

         mi = rejectMenu.add(new JMenuItem("Reject picks by residual"));
         mi.addActionListener(rejectPopupListener);

         mi = rejectMenu.add(new JMenuItem("Unreject all picks"));
         mi.addActionListener(rejectPopupListener);

         rejectMenu.addSeparator();
       }

       if (ampCnt > 0) {
         mi = rejectMenu.add(new JMenuItem("Reject more distant amps"));
         mi.addActionListener(rejectPopupListener);

         mi = rejectMenu.add(new JMenuItem("Reject amps by residual"));
         mi.addActionListener(rejectPopupListener);

         mi = rejectMenu.add(new JMenuItem("Unreject all amps"));
         mi.addActionListener(rejectPopupListener);

         rejectMenu.addSeparator();
       }

       if (codaCnt > 0) {
         mi = rejectMenu.add(new JMenuItem("Reject more distant codas"));
         mi.addActionListener(rejectPopupListener);

         mi = rejectMenu.add(new JMenuItem("Reject codas by residual"));
         mi.addActionListener(rejectPopupListener);

         mi = rejectMenu.add(new JMenuItem("Unreject all codas"));
         mi.addActionListener(rejectPopupListener);
         rejectMenu.addSeparator();
       }

       if (showAll) {
         if (typeCount > 1) { 
           mi = rejectMenu.add(new JMenuItem("Reject all more distant"));
           mi.addActionListener(rejectPopupListener);

           mi = rejectMenu.add(new JMenuItem("Reject all by residual"));
           mi.addActionListener(rejectPopupListener);
           rejectMenu.addSeparator();
         }
         
       }

       popup.addSeparator();
       SortPopupListener sortPopupListener = new SortPopupListener(wfv.mv);
       JMenu sortMenu = new JMenu("Sort waveforms...");
       mi = new JMenuItem("by distance from THIS channel");
       mi.setToolTipText("panel in which mouse was clicked");
       mi = sortMenu.add(mi);
       mi.addActionListener(sortPopupListener);
       if ( ! (wfv.mv.masterWFViewModel.get() == wfv) ) {
         mi = new JMenuItem("by distance from SELECTED channel");
         mi.setToolTipText("highlighted panel (picking panel)");
         mi = sortMenu.add(mi);
         mi.addActionListener(sortPopupListener);
       }

       if (sol != null) {
         mi = sortMenu.add(new JMenuItem("by distance from EPICENTER"));
         mi.addActionListener(sortPopupListener);
         popup.add(sortMenu);
         popup.addSeparator();
       }

       ShowPopupListener showPopupListener = new ShowPopupListener(wfv.mv);

       //
       //mi = popup.add(new JMenuItem("Show full view (reset selection box)"));
       //mi.addActionListener(showPopupListener);
       //popup.addSeparator();
       //

       mi = popup.add(new JMenuItem("Show waveform info"));
       mi.addActionListener(showPopupListener);

       //
       //mi = popup.add(new JMenuItem("Show wavelet info"));
       //mi.addActionListener(showPopupListener);
       //

       mi = popup.add(new JMenuItem("Show channel info"));
       mi.addActionListener(showPopupListener);

       mi = popup.add(new JMenuItem("Show reading info"));
       mi.setToolTipText("for currently selected solution only");
       mi.addActionListener(showPopupListener);
       popup.addSeparator();

       if (! (this instanceof PickableWFPanel) ) {
         mi = popup.add(new JCheckBoxMenuItem("Hide channel label", ! getShowChannelLabel()));
         mi.addActionListener(showPopupListener);

         mi = popup.add(new JCheckBoxMenuItem("Hide time label", ! getShowTimeLabel()));
         mi.addActionListener(showPopupListener);
         popup.addSeparator();
       }
       //
       //else {
       //  mi = popup.add(new JMenuItem("Scroll"));
       //  mi.addActionListener(showPopupListener);
       //}
       //

       mi = popup.add(new JMenuItem("Reload timeseries"));
       mi.addActionListener(
         new ActionListener() {
           public void actionPerformed(ActionEvent evt) {
               wfv.reloadTimeSeries();
           }
         }
       );

       mi = popup.add(new JMenuItem("Unload timeseries"));
       mi.addActionListener(
         new ActionListener() {
           public void actionPerformed(ActionEvent evt) {
               wfv.unloadTimeSeries();
           }
         }
       );
       popup.addSeparator();

       Waveform wf = wfv.getWaveform();
       if (wf != null)  {
           mi = (wf.isClipped()) ? 
               popup.add(new JMenuItem("Set Unclipped")) : popup.add(new JMenuItem("Set Clipped"));
           mi.addActionListener(
             new ActionListener() {
               public void actionPerformed(ActionEvent evt) {
                   wfv.getWaveform().setClipped(evt.getActionCommand().equals("Set Clipped"));
                   setRowHeaderBackground();
               }
             }
           );
           mi.setToolTipText("Waveform timeseries");
           popup.addSeparator();
       }

       mi = popup.add(new JMenuItem( (wfv.isSelected()) ? "Unselect" : "Select" ));
       mi.setToolTipText("as candidate for other processing");
       mi.addActionListener(
         new ActionListener() {
           public void actionPerformed(ActionEvent evt) {
               if (wfv != null) selectView(!wfv.isSelected());
           }
         }
       );
       popup.addSeparator();

       ActionListener al = new ActionListener() {
           public void actionPerformed(ActionEvent evt) {
               String cmd = evt.getActionCommand();
               Object obj =  wfv.mv.getOwner();
               if (!(obj instanceof WaveformDisplayIF)) return; 
               WaveformDisplayIF wfd = (WaveformDisplayIF) obj;
               WFScroller wfScroller = wfd.getWFScroller();
               // Reset "visible" panels, i.e. those in WFScroller group having "active" WFView's 
               if (wfScroller != null && wfScroller.groupPanel != null) {
                    WFPanelList wfpList = wfScroller.groupPanel.wfpList;
                    int cnt = wfpList.size(); 
                    WFPanel wfp = null;
                    for (int idx = 0; idx < cnt; idx++) {
                      wfp = (WFPanel) wfpList.get(idx);
                      if (wfp.wfv != null && wfp.wfv.isActive) {
                          wfp.wfv.setSelected(cmd.startsWith("Select ALL"));
                          wfp.setRowHeaderBackground();
                      }
                    }
               }
           }
       };
       mi = popup.add(new JMenuItem("Unselect ALL active views"));
       mi.setToolTipText("as candidates for other processing");
       mi.addActionListener(al);
       mi = popup.add(new JMenuItem("Select ALL active views "));
       mi.setToolTipText("as candidates for other processing");
       mi.addActionListener(al);
       popup.addSeparator();

       mi = popup.add(new JMenuItem("Close Popup"));
       mi.addActionListener(showPopupListener);

       popup.show(this, point.x, point.y);

    }

    protected void doMakePopup(Point point, boolean showAll) {

       if (wfv.mv == null) return;

       Solution sol = wfv.mv.getSelectedSolution();
       //if (sol == null) return;

       JPopupMenu popup = new JPopupMenu();

       DeletePopupListener deletePopupListener = null;
       StripPopupListener stripPopupListener = null;
       RejectPopupListener rejectPopupListener = null;
       RequirePopupListener reqPopupListener = null;

       //boolean hasPhases = wfv.hasPhases();
       //boolean hasAmps = wfv.hasAmps();
       //boolean hasCodas = wfv.hasCodas();
       //
       boolean hasPhases = (sol == null) ? false : sol.getPhaseList().size() > 0;
       boolean hasAmps =  (sol == null) ? false : sol.getAmpList().size() > 0;
       boolean hasCodas =  (sol == null) ? false : sol.getCodaList().size() > 0;

       if (hasPhases || hasAmps || hasPhases) {

         deletePopupListener = new DeletePopupListener(wfv.mv);
         rejectPopupListener = new RejectPopupListener(wfv.mv);
         stripPopupListener = new StripPopupListener(wfv.mv);
         reqPopupListener = new RequirePopupListener(wfv.mv);

       }

       JMenuItem mi = null;
       int typeCount = 0;
       boolean addSep = false;

       if (hasPhases) {
         mi = popup.add(new JMenuItem("Delete Pick"));
         mi.addActionListener(deletePopupListener);
         addSep = true;
       }

       if (hasAmps) {
         mi = popup.add(new JMenuItem("Delete Amp"));
         mi.addActionListener(deletePopupListener);
         addSep = true;
       }

       if (hasCodas) {
         mi = popup.add(new JMenuItem("Delete Coda"));
         mi.addActionListener(deletePopupListener);
         addSep = true;
       }

       if (addSep ) {
         popup.addSeparator();
         addSep = false;
       }

       // Place of right-click
       double clickTime =  dtOfPixel(mouseClickPoint);
       JasiReadingIF jr = null;

       if (hasPhases && sol != null) {
         jr = wfv.phaseList.getNearestToTime(clickTime, sol);
         if (jr != null) {
           mi = popup.add(new JMenuItem("Reject Pick"));
           mi.addActionListener(rejectPopupListener);
           mi.setEnabled(!jr.isReject());

           mi = popup.add(new JMenuItem("Unreject Pick"));
           mi.addActionListener(rejectPopupListener);
           mi.setEnabled(jr.isReject());
         }
         addSep = true;
       }

       if (hasAmps && sol != null) {
         jr = wfv.ampList.getNearestToTime(clickTime, sol);
         if (jr != null) {
           mi = popup.add(new JMenuItem("Reject Amp"));
           mi.addActionListener(rejectPopupListener);
           mi.setEnabled(!jr.isReject());

           mi = popup.add(new JMenuItem("Unreject Amp"));
           mi.addActionListener(rejectPopupListener);
           mi.setEnabled(jr.isReject());
           addSep = true;
         }
       }

       if (hasCodas && sol != null) {
         jr = wfv.codaList.getNearestToTime(clickTime, sol);
         if (jr != null) {
           mi = popup.add(new JMenuItem("Reject Coda"));
           mi.addActionListener(rejectPopupListener);
           mi.setEnabled(!jr.isReject());

           mi = popup.add(new JMenuItem("Unreject Coda"));
           mi.addActionListener(rejectPopupListener);
           mi.setEnabled(jr.isReject());
           addSep = true;
         }
       }

       if (addSep ) {
         popup.addSeparator();
         addSep = false;
       }

       if (hasPhases && sol != null) {
         jr = wfv.phaseList.getNearestToTime(clickTime, sol);
         if (jr != null) {
           mi = popup.add(new JMenuItem("Require Pick"));
           mi.addActionListener(reqPopupListener);
           mi.setEnabled(!jr.isFinal());

           mi = popup.add(new JMenuItem("Unrequire Pick"));
           mi.addActionListener(reqPopupListener);
           mi.setEnabled(jr.isFinal());
         }
         addSep = true;
       }

       if (hasAmps && sol != null) {
         jr = wfv.ampList.getNearestToTime(clickTime, sol);
         if (jr != null) {
           mi = popup.add(new JMenuItem("Require Amp"));
           mi.addActionListener(reqPopupListener);
           mi.setEnabled(!jr.isFinal());

           mi = popup.add(new JMenuItem("Unrequire Amp"));
           mi.addActionListener(reqPopupListener);
           mi.setEnabled(jr.isFinal());
           addSep = true;
         }
       }

       if (hasCodas && sol != null) {
         jr = wfv.codaList.getNearestToTime(clickTime, sol);
         if (jr != null) {
           mi = popup.add(new JMenuItem("Require Coda"));
           mi.addActionListener(reqPopupListener);
           mi.setEnabled(!jr.isFinal());

           mi = popup.add(new JMenuItem("Unrequire Coda"));
           mi.addActionListener(reqPopupListener);
           mi.setEnabled(jr.isFinal());
           addSep = true;
         }
       }

       if (addSep ) {
         popup.addSeparator();
         addSep = false;
       }

       int phaseCnt = (sol == null) ? 0 : sol.getPhaseList().size();
       int ampCnt = (sol == null) ? 0 : sol.getAmpList().size();
       int codaCnt = (sol == null) ? 0 : sol.getCodaList().size();

       if (phaseCnt > 0 || ampCnt > 0 || codaCnt > 0 ) {
           deletePopupListener = new DeletePopupListener(wfv.mv);
           rejectPopupListener = new RejectPopupListener(wfv.mv);
           stripPopupListener = new StripPopupListener(wfv.mv);
         //
         // No global forcing all required or not?
         //
         //if (ampCnt > 0 || codaCnt > 0 ) {
         //    reqPopupListener = new RequirePopupListener(wfv.mv);
         //}
         //
       }

       if (showAll) {
         if (phaseCnt > 0) {
           mi = popup.add(new JMenuItem("Delete All Picks"));
           mi.addActionListener(deletePopupListener);

           mi = popup.add(new JMenuItem("Delete All Auto Picks"));
           mi.addActionListener(deletePopupListener);

           addSep = true;
           typeCount++;
         }
         if (ampCnt > 0) {
           mi = popup.add(new JMenuItem("Delete All Amps"));
           mi.addActionListener(deletePopupListener);

           mi = popup.add(new JMenuItem("Delete All Auto Amps"));
           mi.addActionListener(deletePopupListener);

           addSep = true;
           typeCount++;
         }
         if (codaCnt > 0) {
           mi = popup.add(new JMenuItem("Delete All Coda"));
           mi.addActionListener(deletePopupListener);

           mi = popup.add(new JMenuItem("Delete All Auto Coda"));
           mi.addActionListener(deletePopupListener);

           addSep = true;
           typeCount++;
         }
         if (addSep) {
           if (typeCount > 1) {
             mi = popup.add(new JMenuItem("Delete All"));
             mi.addActionListener(deletePopupListener);

             mi = popup.add(new JMenuItem("Delete All Auto"));
             mi.addActionListener(deletePopupListener);
           }
           popup.addSeparator();
           addSep = false;
         }
       }

       if (phaseCnt > 0) {
         mi = popup.add(new JMenuItem("Delete more distant picks"));
         mi.addActionListener(stripPopupListener);

         mi = popup.add(new JMenuItem("Delete more distant auto picks"));
         mi.addActionListener(stripPopupListener);

         mi = popup.add(new JMenuItem("Delete more distant auto S picks"));
         mi.addActionListener(stripPopupListener);

         mi = popup.add(new JMenuItem("Delete picks by residual"));
         mi.addActionListener(stripPopupListener);
         popup.addSeparator();
       }

       if (ampCnt > 0) {
         mi = popup.add(new JMenuItem("Delete more distant amps"));
         mi.addActionListener(stripPopupListener);

         mi = popup.add(new JMenuItem("Delete amps by residual"));
         mi.addActionListener(stripPopupListener);
         popup.addSeparator();

         //
         //mi = popup.add(new JMenuItem("Require all amps"));
         //mi.addActionListener(reqPopupListener);

         //mi = popup.add(new JMenuItem("Unrequire all amps"));
         //mi.addActionListener(reqPopupListener);
         //popup.addSeparator();
         //

       }

       if (codaCnt > 0) {
         mi = popup.add(new JMenuItem("Delete more distant codas"));
         mi.addActionListener(stripPopupListener);

         mi = popup.add(new JMenuItem("Delete codas by residual"));
         mi.addActionListener(stripPopupListener);
         popup.addSeparator();

         //
         //mi = popup.add(new JMenuItem("Require all codas"));
         //mi.addActionListener(reqPopupListener);

         //mi = popup.add(new JMenuItem("Unrequire all codas"));
         //mi.addActionListener(reqPopupListener);
         //popup.addSeparator();
         //

       }

       if (showAll) {
         if (typeCount > 1) { 
           mi = popup.add(new JMenuItem("Delete all more distant"));
           mi.addActionListener(stripPopupListener);

           mi = popup.add(new JMenuItem("Delete all auto more distant"));
           mi.addActionListener(stripPopupListener);

           mi = popup.add(new JMenuItem("Delete all by residual"));
           mi.addActionListener(stripPopupListener);
           popup.addSeparator();
         }
       }

       if (phaseCnt > 0) {
         mi = popup.add(new JMenuItem("Reject more distant picks"));
         mi.addActionListener(rejectPopupListener);

         mi = popup.add(new JMenuItem("Reject picks by residual"));
         mi.addActionListener(rejectPopupListener);

         mi = popup.add(new JMenuItem("Unreject all picks"));
         mi.addActionListener(rejectPopupListener);

         popup.addSeparator();
       }

       if (ampCnt > 0) {
         mi = popup.add(new JMenuItem("Reject more distant amps"));
         mi.addActionListener(rejectPopupListener);

         mi = popup.add(new JMenuItem("Reject amps by residual"));
         mi.addActionListener(rejectPopupListener);

         mi = popup.add(new JMenuItem("Unreject all amps"));
         mi.addActionListener(rejectPopupListener);

         popup.addSeparator();
       }

       if (codaCnt > 0) {
         mi = popup.add(new JMenuItem("Reject more distant codas"));
         mi.addActionListener(rejectPopupListener);

         mi = popup.add(new JMenuItem("Reject codas by residual"));
         mi.addActionListener(rejectPopupListener);

         mi = popup.add(new JMenuItem("Unreject all codas"));
         mi.addActionListener(rejectPopupListener);
         popup.addSeparator();
       }

       if (showAll) {
         if (typeCount > 1) { 
           mi = popup.add(new JMenuItem("Reject all more distant"));
           mi.addActionListener(rejectPopupListener);

           mi = popup.add(new JMenuItem("Reject all by residual"));
           mi.addActionListener(rejectPopupListener);
           popup.addSeparator();
         }
         
       }

       popup.addSeparator();
       SortPopupListener sortPopupListener = new SortPopupListener(wfv.mv);
       mi = new JMenuItem("Sort by distance from THIS channel");
       mi.setToolTipText("panel in which mouse was clicked");
       mi = popup.add(mi);
       mi.addActionListener(sortPopupListener);

       if ( ! (wfv.mv.masterWFViewModel.get() == wfv) ) {
         mi = new JMenuItem("Sort by distance from SELECTED channel");
         mi.setToolTipText("highlighted panel (picking panel)");
         mi = popup.add(mi);
         mi.addActionListener(sortPopupListener);
       }

       if (sol != null) {
         mi = popup.add(new JMenuItem("Sort by distance from EPICENTER"));
         mi.addActionListener(sortPopupListener);
         popup.addSeparator();
       }

       ShowPopupListener showPopupListener = new ShowPopupListener(wfv.mv);

       //
       //mi = popup.add(new JMenuItem("Show full view (reset selection box)"));
       //mi.addActionListener(showPopupListener);
       //popup.addSeparator();
       //

       mi = popup.add(new JMenuItem("Show waveform info"));
       mi.addActionListener(showPopupListener);

       //
       //mi = popup.add(new JMenuItem("Show wavelet info"));
       //mi.addActionListener(showPopupListener);
       //

       mi = popup.add(new JMenuItem("Show channel info"));
       mi.addActionListener(showPopupListener);

       mi = popup.add(new JMenuItem("Show reading info"));
       mi.setToolTipText("for currently selected solution only");
       mi.addActionListener(showPopupListener);
       popup.addSeparator();

       if (! (this instanceof PickableWFPanel) ) {
         mi = popup.add(new JCheckBoxMenuItem("Hide channel label", ! getShowChannelLabel()));
         mi.addActionListener(showPopupListener);

         mi = popup.add(new JCheckBoxMenuItem("Hide time label", ! getShowTimeLabel()));
         mi.addActionListener(showPopupListener);
         popup.addSeparator();
       }
       //
       //else {
       //  mi = popup.add(new JMenuItem("Scroll"));
       //  mi.addActionListener(showPopupListener);
       //}
       //

       mi = popup.add(new JMenuItem("Reload timeseries"));
       mi.addActionListener(
         new ActionListener() {
           public void actionPerformed(ActionEvent evt) {
               wfv.reloadTimeSeries();
           }
         }
       );

       mi = popup.add(new JMenuItem("Unload timeseries"));
       mi.addActionListener(
         new ActionListener() {
           public void actionPerformed(ActionEvent evt) {
               wfv.unloadTimeSeries();
           }
         }
       );
       popup.addSeparator();

       Waveform wf = wfv.getWaveform();
       if (wf != null)  {
           mi = (wf.isClipped()) ? 
               popup.add(new JMenuItem("Set Unclipped")) : popup.add(new JMenuItem("Set Clipped"));
           mi.addActionListener(
             new ActionListener() {
               public void actionPerformed(ActionEvent evt) {
                   wfv.getWaveform().setClipped(evt.getActionCommand().equals("Set Clipped"));
                   setRowHeaderBackground();
               }
             }
           );
           mi.setToolTipText("Waveform timeseries");
           popup.addSeparator();
       }

       mi = popup.add(new JMenuItem( (wfv.isSelected()) ? "Unselect" : "Select" ));
       mi.setToolTipText("as candidate for other processing");
       mi.addActionListener(
         new ActionListener() {
           public void actionPerformed(ActionEvent evt) {
               if (wfv != null) selectView(!wfv.isSelected());
           }
         }
       );
       popup.addSeparator();

       ActionListener al = new ActionListener() {
           public void actionPerformed(ActionEvent evt) {
               String cmd = evt.getActionCommand();
               Object obj =  wfv.mv.getOwner();
               if (!(obj instanceof WaveformDisplayIF)) return; 
               WaveformDisplayIF wfd = (WaveformDisplayIF) obj;
               WFScroller wfScroller = wfd.getWFScroller();
               // Reset "visible" panels, i.e. those in WFScroller group having "active" WFView's 
               if (wfScroller != null && wfScroller.groupPanel != null) {
                    WFPanelList wfpList = wfScroller.groupPanel.wfpList;
                    int cnt = wfpList.size(); 
                    WFPanel wfp = null;
                    for (int idx = 0; idx < cnt; idx++) {
                      wfp = (WFPanel) wfpList.get(idx);
                      if (wfp.wfv != null && wfp.wfv.isActive) {
                          wfp.wfv.setSelected(cmd.startsWith("Select ALL"));
                          wfp.setRowHeaderBackground();
                      }
                    }
               }
           }
       };
       mi = popup.add(new JMenuItem("Unselect ALL active views"));
       mi.setToolTipText("as candidates for other processing");
       mi.addActionListener(al);
       mi = popup.add(new JMenuItem("Select ALL active views "));
       mi.setToolTipText("as candidates for other processing");
       mi.addActionListener(al);
       popup.addSeparator();

       mi = popup.add(new JMenuItem("Close Popup"));
       mi.addActionListener(showPopupListener);

       popup.show (this, point.x, point.y);

    }

    private boolean confirmAction(Component src, String cmd) {
        return (confirmActionPopup) ?
          (JOptionPane.showConfirmDialog(src, cmd, "Confirm Action",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) : true;
    }

    private class SortPopupListener implements ActionListener {
      private MasterView mv = null;

      public SortPopupListener(MasterView mv) {
          this.mv = mv;
      }

      public void actionPerformed(ActionEvent e) {
        if (mv == null) return;
        if (mv.getSelectedSolution() == null) return; // first test for solution is loaded -aww

        Object obj = mv.getOwner();
        if (! (obj instanceof Jiggle)) return; 
        Jiggle jiggle = (Jiggle) obj;

        String cmd = e.getActionCommand();
        if (cmd.endsWith("by distance from THIS channel")) {
          jiggle.resortWFViews(wfv.getChannelObj().getLatLonZ());
          if (ActiveWFPanel.this instanceof PickableWFPanel) mv.masterWFViewModel.set(wfv, false);
          else mv.masterWFViewModel.set(wfv, true);
        }
        else if (cmd.endsWith("by distance from SELECTED channel")) {
          jiggle.resortWFViews(mv.masterWFViewModel.get().getChannelObj().getLatLonZ());
        }
        else if (cmd.endsWith("by distance from EPICENTER")) {
          jiggle.resortWFViews();
        }
      }

    }

    private class RequirePopupListener implements ActionListener {

      private MasterView mv = null;
      private Solution sol = null;
      private JComponent src = null;

      public RequirePopupListener(MasterView mv) {
        this.mv = mv;
      }

      public final void actionPerformed (ActionEvent evt) {
        if (mv == null) return;

        sol = mv.getSelectedSolution();
        if (mv.getSelectedSolution() == null) return;

        src = (JComponent) evt.getSource(); // the menu item

        String cmd = evt.getActionCommand();

        if (cmd == "Require Pick") {
            ((Phase)wfv.phaseList.getNearestToTime(dtOfPixel(mouseClickPoint), sol)).require();
        }
        if (cmd == "Require Amp") {
            ((Amplitude)wfv.ampList.getNearestToTime(dtOfPixel(mouseClickPoint), sol)).require();
        }
        else if (cmd == "Require Coda") {
            ((Coda)wfv.codaList.getNearestToTime(dtOfPixel(mouseClickPoint), sol)).require();
        }
        else if (cmd == "Unrequire Pick") {
            ((Phase)wfv.phaseList.getNearestToTime(dtOfPixel(mouseClickPoint), sol)).unrequire();
        }
        else if (cmd == "Unrequire Amp") {
            ((Amplitude)wfv.ampList.getNearestToTime(dtOfPixel(mouseClickPoint), sol)).unrequire();
        }
        else if (cmd == "Unrequire Coda") {
            ((Coda)wfv.codaList.getNearestToTime(dtOfPixel(mouseClickPoint), sol)).unrequire();
        }

      }
    }

    private class ShowPopupListener implements ActionListener {

      private MasterView mv = null;
      private Solution sol = null;
      private JComponent src = null;

      public ShowPopupListener(MasterView mv) {
        this.mv = mv;
      }

      public final void actionPerformed (ActionEvent evt) {
        if (mv == null) return;

        sol = mv.getSelectedSolution();
        //if (mv.getSelectedSolution() == null) return;

        src = (JComponent) evt.getSource(); // the menu item

        String cmd = evt.getActionCommand();
        //
        //else if (cmd.startsWith("Show full view")) {
        //    wfv.mv.masterWFWindowModel.setFullView(getWf());
        //    // don't notify scroller to scroll, if unchanged picking panel -aww 2009/04/03
        //    if (ActiveWFPanel.this instanceof PickableWFPanel) wfv.mv.masterWFViewModel.set(wfv, false);
        //    else wfv.mv.masterWFViewModel.set(wfv, true); // always notify zoompanel scroller
        //}
        //
        if (cmd == "Show waveform info" || cmd == "Show wavelet info") {
            if (wfv.hasWaveform()) {
                //String str = (cmd == "Show waveform info") ?  wfv.wf.toBlockString() : ((WaveletGroup)wfv.wf).toWaveletBlockString();
                Waveform wf = getWf();
                String str = null;
                if ( wf == null ) { 
                    str = ( cmd == "Show waveform info") ?  wfv.wf.toBlockString() : ((WaveletGroup)wfv.wf).toWaveletBlockString();
                }
                else {
                    str = (cmd == "Show waveform info") ?  wf.toBlockString() : ((WaveletGroup)wf).toWaveletBlockString();
                }
                JTextArea jta = new JTextArea(str);
                jta.setEditable(false);
                jta.setBackground(Jiggle.jiggle.getBackground());
                jta.setFont(new Font("Monospaced", Font.BOLD, 12));
                new JTextClipboardPopupMenu(jta);
                JOptionPane.showMessageDialog(Jiggle.jiggle, new JScrollPane(jta), "Waveform Info",
                          JOptionPane.PLAIN_MESSAGE);
            }
            else JOptionPane.showMessageDialog(Jiggle.jiggle, "No Waveform", "Waveform Info",
                          JOptionPane.PLAIN_MESSAGE);
        }
        else if ( (cmd == "Show channel info") || (cmd == "Show reading info") ) {
            JTextArea jta = new JTextArea();
            jta.setEditable(false);
            jta.setBackground(Jiggle.jiggle.getBackground());
            jta.setFont(new Font("Monospaced", Font.BOLD, 12));
            new JTextClipboardPopupMenu(jta);

            if (wfv != null) {
              if (cmd == "Show channel info") {
                  jta.append(wfv.getChannelObj().toDumpString());
                  if (jta.getLineCount() < 1) jta.append("No information");
              }
              else if (cmd == "Show reading info") {
                // NOTE: changed test from wfv object to sol object lists - aww 2012/06/08
                JasiReadingList jrl = (JasiReadingList) sol.phaseList.getAssociatedByChannel(wfv.getChannelObj());
                if (jrl.size() > 0) {
                    jta.append(jrl.toNeatString());
                    jta.append("\n");
                }
                jrl = (JasiReadingList) sol.ampList.getAssociatedByChannel(wfv.getChannelObj());
                if (jrl.size() > 0) {
                    jta.append(jrl.toNeatString());
                    jta.append("\n");
                }
                jrl = (JasiReadingList) sol.codaList.getAssociatedByChannel(wfv.getChannelObj());
                if (jrl.size() > 0) {
                    jta.append(jrl.toNeatString());
                    jta.append("\n");
                }
                if (jta.getLineCount() < 2) jta.append("No readings for selected event and channel");
              }
            }
            //System.out.println("ActiveWFPanel Show reading info textArea line count = " + jta.getLineCount());

            JOptionPane jp = new JOptionPane(jta,JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION);
            JDialog jd = jp.createDialog(Jiggle.jiggle, cmd);
            jd.setVisible(true);
            jd.dispose();

        }
        else if (cmd == "Hide channel label") {
            setShowChannelLabel( ! ((JCheckBoxMenuItem)src).getState() );
            repaint();
        }
        else if (cmd == "Hide time label") {
            setShowTimeLabel( ! ((JCheckBoxMenuItem)src).getState() );
            repaint();
        }
        //
        //else if (cmd == "Scroll") {
        //    wfv.mv.masterWFViewModel.set(wfv, true); // notify scroller
        //}
        //
        else if (cmd == "Close Popup") {
            // noop
        }

      }
    }

    private class StripPopupListener implements ActionListener {
      private MasterView mv = null;
      private Solution sol = null;
      private JComponent src = null;

      public StripPopupListener(MasterView mv) {
        this.mv = mv;
      }

      public final void actionPerformed(ActionEvent evt) {
        if (mv == null) return;
        sol = mv.getSelectedSolution();
        if (sol == null) return;

        src = (JComponent) evt.getSource(); // the menu item

        String cmd = evt.getActionCommand();

        //NOTE: strip methods in MasterView now refresh WFView readings -aww 2008/02/26
        if (cmd == "Delete picks by residual") {
             if (confirmAction(src, cmd)) mv.stripPhasesByResidual(sol);
        } else if (cmd == "Delete codas by residual") {
             if (confirmAction(src, cmd)) mv.stripCodasByResidual(sol);
        } else if (cmd == "Delete amps by residual") {
             if (confirmAction(src, cmd)) mv.stripAmpsByResidual(sol);
        } else if (cmd == "Delete all by residual") {
           if (confirmAction(src, cmd)) {
               mv.stripPhasesByResidual(sol);
               mv.stripAmpsByResidual(sol);
               mv.stripCodasByResidual(sol);
           }
        // Distance should be horizontalDistance()  -aww
        } else if (cmd == "Delete all more distant") {
           if (confirmAction(src, cmd)) {
               mv.stripPhasesByDistance(wfv.getHorizontalDistance(), sol, false, false); // aww 2010/09/16
               mv.stripAmpsByDistance(wfv.getHorizontalDistance(), sol, false); // aww 2010/09/16
               mv.stripCodasByDistance(wfv.getHorizontalDistance(), sol, false); // aww 2010/09/16
           }
        } else if (cmd == "Delete all auto more distant") {
           if (confirmAction(src, cmd)) {
               mv.stripPhasesByDistance(wfv.getHorizontalDistance(), sol, true, false); // aww 2010/09/16
               mv.stripAmpsByDistance(wfv.getHorizontalDistance(), sol, true); // aww 2010/09/16
               mv.stripCodasByDistance(wfv.getHorizontalDistance(), sol, true); // aww 2010/09/16
           }
        } else if (cmd == "Delete more distant picks") {
           if (confirmAction(src, cmd)) mv.stripPhasesByDistance(wfv.getHorizontalDistance(), sol, false, false); // aww 06/11/2004
        } else if (cmd == "Delete more distant auto picks") {
           if (confirmAction(src, cmd)) mv.stripPhasesByDistance(wfv.getHorizontalDistance(), sol, true, false); // aww 2010/09/16
        } else if (cmd == "Delete more distant auto S picks") {
           if (confirmAction(src, cmd)) mv.stripPhasesByDistance(wfv.getHorizontalDistance(), sol, true, true); // aww 2010/11/02
        } else if (cmd == "Delete more distant amps") {
           if (confirmAction(src, cmd)) mv.stripAmpsByDistance(wfv.getHorizontalDistance(), sol); // aww 06/11/2004
        //} else if (cmd == "Delete more distant auto amps") {
        //   if (confirmAction(src, cmd)) mv.stripAmpsByDistance(wfv.getHorizontalDistance(), sol, true); // aww 2010/09/16
        } else if (cmd == "Delete more distant codas") {
           if (confirmAction(src, cmd)) mv.stripCodasByDistance(wfv.getHorizontalDistance(), sol); // aww 06/11/2004
        //} else if (cmd == "Delete more distant auto codas") {
        //   if (confirmAction(src, cmd)) mv.stripCodasByDistance(wfv.getHorizontalDistance(), sol, true); // aww 2010/09/16
        }

        Object obj = (mv == null) ? null : mv.getOwner();
        if (obj instanceof Jiggle) {
            Jiggle jiggle = (Jiggle) obj;
            jiggle.resetSummaryTextAreas();
            jiggle.updateStatusBarCounts();
        }

      }
    }

    private class RejectPopupListener implements ActionListener {
      private MasterView mv = null;
      private Solution sol = null;
      private JComponent src = null;

      public RejectPopupListener(MasterView mv) {
        this.mv = mv;
      }

      public final void actionPerformed(ActionEvent evt) {
        if (mv == null) return;
        sol = mv.getSelectedSolution();
        if (sol == null) return;

        src = (JComponent) evt.getSource(); // the menu item

        String cmd = evt.getActionCommand();

        double clickTime =  dtOfPixel(mouseClickPoint);
        if (cmd == "Reject Pick") {
            JasiReadingIF jr = wfv.phaseList.getNearestToTime(clickTime, sol);
            if (jr != null) jr.setReject(true);
        }
        else if (cmd == "Reject Amp") {
            JasiReadingIF jr = wfv.ampList.getNearestToTime(clickTime, sol);
            if (jr != null) jr.setReject(true);
        }
        else if (cmd == "Reject Coda") {
            JasiReadingIF jr = wfv.codaList.getNearestToTime(clickTime, sol);
            if (jr != null) jr.setReject(true);
        }
        else if (cmd == "Unreject Pick") {
            JasiReadingIF jr = wfv.phaseList.getNearestToTime(clickTime, sol);
            if (jr != null) jr.setReject(false);
        }
        else if (cmd == "Unreject Amp") {
            JasiReadingIF jr = wfv.ampList.getNearestToTime(clickTime, sol);
            if (jr != null) jr.setReject(false);
        }
        else if (cmd == "Unreject Coda") {
            JasiReadingIF jr = wfv.codaList.getNearestToTime(clickTime, sol);
            if (jr != null) jr.setReject(false);
        }
        //NOTE: reject methods in MasterView refresh WFView readings
        else if (cmd == "Reject picks by residual") {
             if (confirmAction(src, cmd)) mv.rejectPhasesByResidual(sol);
        } else if (cmd == "Reject codas by residual") {
             if (confirmAction(src, cmd)) mv.rejectCodasByResidual(sol);
        } else if (cmd == "Reject amps by residual") {
             if (confirmAction(src, cmd)) mv.rejectAmpsByResidual(sol);
        } else if (cmd == "Reject all by residual") {
           if (confirmAction(src, cmd)) {
               mv.rejectPhasesByResidual(sol);
               mv.rejectAmpsByResidual(sol);
               mv.rejectCodasByResidual(sol);
           }
        } else if (cmd == "Reject all more distant") {
           if (confirmAction(src, cmd)) {
               mv.rejectPhasesByDistance(wfv.getHorizontalDistance(), sol);
               mv.rejectAmpsByDistance(wfv.getHorizontalDistance(), sol);
               mv.rejectCodasByDistance(wfv.getHorizontalDistance(), sol);
           }
        } else if (cmd == "Reject more distant picks") {
           if (confirmAction(src, cmd)) mv.rejectPhasesByDistance(wfv.getHorizontalDistance(), sol);
        } else if (cmd == "Reject more distant amps") {
           if (confirmAction(src, cmd)) mv.rejectAmpsByDistance(wfv.getHorizontalDistance(), sol);
        } else if (cmd == "Reject more distant codas") {
           if (confirmAction(src, cmd)) mv.rejectCodasByDistance(wfv.getHorizontalDistance(), sol);
        } else if (cmd == "Unreject all picks") {
            if (confirmAction(src, cmd)) sol.phaseList.unrejectAll();
        } else if (cmd == "Unreject all amps") {
            if (confirmAction(src, cmd)) sol.ampList.unrejectAll();
        } else if (cmd == "Unreject all codas") {
            if (confirmAction(src, cmd)) sol.codaList.unrejectAll();
        }

        Object obj = (mv == null) ? null : mv.getOwner();
        if (obj instanceof Jiggle) {
            Jiggle jiggle = (Jiggle) obj;
            jiggle.resetSummaryTextAreas();
            jiggle.updateStatusBarCounts();
        }

      }
    }

    private class DeletePopupListener implements ActionListener {

      private MasterView mv = null;
      private Solution sol = null;
      private JComponent src = null;

      public DeletePopupListener(MasterView mv) {
        this.mv = mv;
      }

      public final void actionPerformed(ActionEvent evt) {

        if (mv == null) return;
        sol = mv.getSelectedSolution();
        if (sol == null) return;
        src = (JComponent) evt.getSource(); // the menu item

        //get mouse-click-point as set in MsHandler
        double clickTime =  dtOfPixel(mouseClickPoint);
        //double clickTime = dtOfPixel(src.getX());  // this give x of popup menu! not mouseclick
        //System.out.println(LeapSeconds.trueToString(clickTime, "HH:mm:ss.fff"));

        String cmd = evt.getActionCommand();

        if (cmd == "Delete Pick") {
            // Delete the nearest pick associated with the selected solution
            // Delete it in Solutions's list so listeners know.
            // Nearest phase could be null (no phase) but phaseList.delete()
            // copes with that
            //sol.deletePhase(wfv.phaseList.getNearestPhase(clickTime, sol)); //aww
            Phase ph = (Phase) wfv.phaseList.getNearestToTime(clickTime, sol);
            mv.addToPhaseUndoList(ph, true);
            sol.erase(ph);
            setScanWindowTimes(0.,0.);
        }
        else if (cmd == "Delete Amp") {
            //sol.ampList.delete(wfv.ampList.getNearestToTime(clickTime, sol)); // aww 04/14/2005
            Amplitude amp = (Amplitude) wfv.ampList.getNearestToTime(clickTime, sol);
            mv.addToAmpUndoList(amp, true);
            sol.erase(amp);
                Magnitude mag = sol.getPrefMagOfType("l");
                if (mag != null) mag.getReadingList().erase(amp); // note: if different data instance in mag list use removeEquivalent
            setScanWindowTimes(0.,0.);
        }
        else if (cmd == "Delete Coda") {
            //sol.codaList.delete(wfv.codaList.getNearestToTime(clickTime, sol)); // aww 04/14/2005
            Coda coda = (Coda) wfv.codaList.getNearestToTime(clickTime, sol); 
            mv.addToCodaUndoList(coda, true);
            sol.erase(coda);
                Magnitude mag = sol.getPrefMagOfType("d");
                if (mag != null) mag.getReadingList().erase(coda);
            setScanWindowTimes(0.,0.);
        }
        else if (cmd == "Delete All") {
            if (confirmAction(src, cmd)) {
                // From wfv, erase members of List assoc with Sol
                mv.addToPhaseUndoList(sol.phaseList, true);
                int cnt = sol.phaseList.eraseAll();
                System.out.println("INFO: count of arrs erased: " + cnt);
                mv.addToAmpUndoList(sol.ampList, true);
                cnt = sol.ampList.eraseAll();
                System.out.println("INFO: count of amps erased: " + cnt);
                    Magnitude mag = sol.getPrefMagOfType("l");
                    if (mag != null) mag.getReadingList().eraseAll();
                mv.addToCodaUndoList(sol.codaList, true);
                cnt = sol.codaList.eraseAll();
                System.out.println("INFO: count of codas erased: " + cnt);
                    mag = sol.getPrefMagOfType("d");
                    if (mag != null) mag.getReadingList().eraseAll();
                mv.makeLocalLists(); // refresh readings seen in views -aww 2008/02/26
                setScanWindowTimes(0.,0.);
            }
        }
        else if (cmd == "Delete All Picks") {
            if (confirmAction(src, cmd)) {
                mv.addToPhaseUndoList(sol.phaseList, true);
                int cnt = sol.phaseList.eraseAll();
                System.out.println("INFO: count of arrs erased: " + cnt);
                mv.updatePhaseLists(); // refresh readings seen in views -aww 2008/02/26
            }
        }
        else if (cmd == "Delete All Amps") {
            // From wfv, erase members of ampList assoc with Sol, same for Coda
            if (confirmAction(src, cmd)) {
                mv.addToAmpUndoList(sol.ampList, true);
                int cnt = sol.ampList.eraseAll();
                System.out.println("INFO: count of amps erased: " + cnt);
                    Magnitude mag = sol.getPrefMagOfType("l");
                    if (mag != null) mag.getReadingList().eraseAll();
                mv.updateAmpLists(); // refresh readings seen in views -aww 2008/02/26
                setScanWindowTimes(0.,0.);
            }
        }
        else if (cmd == "Delete All Coda") {
            // From wfv, erase members of ampList assoc with Sol, same for Coda
            if (confirmAction(src, cmd)) {
                mv.addToCodaUndoList(sol.codaList, true);
                int cnt = sol.codaList.eraseAll();
                System.out.println("INFO: count of codas erased: " + cnt);
                    Magnitude mag = sol.getPrefMagOfType("d");
                    if (mag != null) mag.getReadingList().eraseAll();
                mv.updateCodaLists(); // refresh readings seen in views -aww 2008/02/26
                setScanWindowTimes(0.,0.);
            }

        }
        // Below added for "auto" type - aww 2010/09/16
        else if (cmd == "Delete All Auto") {
            if (confirmAction(src, cmd)) {
                //From wfv, erase members of ampList assoc with Sol, same for Coda
                mv.addToPhaseUndoList(sol.phaseList.getAutomaticReadings(), true);
                int cnt = sol.phaseList.eraseAllAutomatic();
                System.out.println("INFO: count of automatic arrs erased: " + cnt);
                mv.addToAmpUndoList(sol.ampList.getAutomaticReadings(), true);
                cnt = sol.ampList.eraseAllAutomatic();
                System.out.println("INFO: count of automatic amps erased: " + cnt);
                    Magnitude mag = sol.getPrefMagOfType("l");
                    if (mag != null) mag.getReadingList().eraseAllAutomatic();
                mv.addToCodaUndoList(sol.codaList.getAutomaticReadings(), true);
                cnt = sol.codaList.eraseAllAutomatic();
                System.out.println("INFO: count of automatic coda erased: " + cnt);
                    mag = sol.getPrefMagOfType("d");
                    if (mag != null) mag.getReadingList().eraseAllAutomatic();
                mv.makeLocalLists();
                setScanWindowTimes(0.,0.);
            }
        }
        else if (cmd == "Delete All Auto Picks") {
            if (confirmAction(src, cmd)) {
                mv.addToPhaseUndoList(sol.phaseList.getAutomaticReadings(), true);
                int cnt = sol.phaseList.eraseAllAutomatic();
                System.out.println("INFO: count of automatic arrs erased: " + cnt);
                mv.updatePhaseLists();
            }
        }
        else if (cmd == "Delete All Auto Amps") {
            // From wfv, erase members of ampList assoc with Sol, same for Coda
            if (confirmAction(src, cmd)) {
                mv.addToAmpUndoList(sol.ampList.getAutomaticReadings(), true);
                int cnt = sol.ampList.eraseAllAutomatic();
                System.out.println("INFO: count of automatic amps erased: " + cnt);
                    Magnitude mag = sol.getPrefMagOfType("l");
                    if (mag != null) mag.getReadingList().eraseAllAutomatic();
                mv.updateAmpLists();
                setScanWindowTimes(0.,0.);
            }
        }
        else if (cmd == "Delete All Auto Coda") {
            // From wfv, erase members of ampList assoc with Sol, same for Coda
            if (confirmAction(src, cmd)) {
                mv.addToCodaUndoList(sol.codaList.getAutomaticReadings(), true);
                int cnt = sol.codaList.eraseAllAutomatic();
                System.out.println("INFO: count of automatic coda erased: " + cnt);
                    Magnitude mag = sol.getPrefMagOfType("d");
                    if (mag != null) mag.getReadingList().eraseAllAutomatic();
                mv.updateCodaLists();
                setScanWindowTimes(0.,0.);
            }

        }

        Object obj = (mv == null) ? null : mv.getOwner();
        if (obj instanceof Jiggle) {
            Jiggle jiggle = (Jiggle) obj;
            jiggle.resetSummaryTextAreas();
            jiggle.updateStatusBarCounts();
        }

      }
    }

/** THIS IS THE *VIEW* PART OF THE MVC INTERACTION.
 * This is the method that is called by the ActiveWFVModel when the
 * selection changes. The TYPE of 'arg' is used to determine what action
 * to take. Two things can change; 1) the selected WFView and 2) the selected
 * time/amp box. <p>
 * An ActiveWFPanel is assumed to contain a WFView that is NOT
 * changed by the ActiveWFVModel. The ActiveWFPanel will change its look or
 * behavior if its WFView is the same as the one selected by the MasterWFVModel
 * but the WFView itself is constant.
 */
/*
    public void update (Observable obs, Object arg) {

        //        System.out.println ("UPDATE: "+ arg.getClass().toString());
        if (arg instanceof WFView)            // what changed?
        {
            WFView newWFV = (WFView) arg;
            boolean itsMe = (newWFV == wfv);        // clever readability step

            if (!amSelected) {        // not currently selected
                if (itsMe) {    // I am newly selected
                   amSelected = true;
                } else {        // no effect on me, don't repaint
                   return;
                }
            } else {
                if (!itsMe) {            //  I just got deselected
                  amSelected = false;
                }
            }

            // note that if (amSelected && itsMe) a repaint happens this
            // behavior is used to force a repaint if something else changes
            // like the phase list.

//            validate();
            repaint();
        }

// only the viewport changed
            if (arg instanceof WFSelectionBox)
        {
            if (amSelected) repaint();
        }

    } // end of update
*/
    /**
     * Add the listener that handles waveform load events. This is to support
     * asynchronous loading of timeseries in background. This overrides the
     * method in WFPanel because we must use a different inner class. */
    //    private void addWFLoadListener () {
    protected void addWFLoadListener() {
        //        System.out.println ("addWFLoadListener - ActiveWFPanel");
        if (getWf() != null) {
            if (wfLoadListener == null) wfLoadListener = new ActiveWFLoadListener(); // aww
            else getWf().removeChangeListener(wfLoadListener); // aww insurance
            getWf().addChangeListener(wfLoadListener); // aww
        }
    }

/** INNER CLASS: Handle changes to the JasiObject lists. */
// NOTE: repaint(), revalidate() are thread safe methods in swing, thus no invokeLater etc.
    private class JasiReadingChangeListener implements ListDataStateListener {
        /* removed ChangeListener implementation method - aww
        public void stateChanged (ChangeEvent changeEvent) {
            // 'arg' will be either a JasiReading or a JasiReadingList
            Object arg = changeEvent.getSource();
            if (arg instanceof JasiReading) {
                if (((JasiReading)arg).getChannelObj().equals(wfv.chan)) {
                    repaint();
                }
            }
            else if (arg instanceof JasiReadingList) {
                repaint();
            }
         } // end of stateChanged
         */

         public void intervalAdded(ListDataStateEvent e) {
                repaint();
         }
         public void intervalRemoved(ListDataStateEvent e) {
                repaint();
         }
         public void contentsChanged(ListDataStateEvent e) {
                repaint();
         }
         public void orderChanged(ListDataStateEvent e) {
                repaint();
         }
         public void stateChanged(ListDataStateEvent e) {
                repaint();
         }
    } // end of JasiReadingChangeListener

/*
    // INNER CLASS: Handle changes to the phase list.
    class PhaseListChangeListener implements ChangeListener {
        public void stateChanged (ChangeEvent changeEvent) {
//          wfv.updatePhaseList();
//          repaint();
            Object arg = changeEvent.getSource();
            // phase changed
            if (arg instanceof Phase) {
              Phase ph = (Phase) arg;
              // if it affects us, rebuild our local phaseList
              // This contributes to efficiency, otherwise ALL panels would repaint
              // on every phase edit!
              if (ph.getChannelObj().equals(wfv.chan)) {
                wfv.updatePhaseList();
                repaint();
              }
            }
            // phaseList changed don't know if it affects us, so rebuild local list
            else if (arg instanceof PhaseList) {
               wfv.updatePhaseList();
               repaint();
            }
        }
    } // end of PhaseListChangeListener

    // INNER CLASS: Handle changes to the amp list.
    class AmpListChangeListener implements ChangeListener {
        public void stateChanged (ChangeEvent changeEvent) {
          Object arg = changeEvent.getSource();
          // amp changed
          if (arg instanceof Amplitude) {
            Amplitude amp = (Amplitude) arg;
            // if it affects us, rebuild our local phaseList
            // This contributes to efficiency, otherwise ALL panels would repaint
            // on every phase edit!
            if (amp.getChannelObj().equals(wfv.chan)) {
                wfv.updateAmpList();
                repaint();
            }
          }
          // phaseList changed don't know if it affects us, so rebuild local list
          else if (arg instanceof org.trinet.jasi.AmpList) {
            wfv.updateAmpList();
            repaint();
          }
        }
    } // end of AmpListChangeListener
*/
/** INNER CLASS: Handle changes to the selected WFView. */

/** This listener is really only used to UN-select a WFView. It is too costly to have
* 1200+ WFPanels listening to see if they are selected, so the listener is only "added"
* to the SelectedWFViewModel WHEN the WFView is selected. This is done in the
* setSelected(tf) method which is called by the ActiveWFPanel MsHandler. */
/*      class WFViewListener implements ChangeListener {

             public void stateChanged (ChangeEvent changeEvent) {

            WFView newWFV = ((SelectedWFViewModel) changeEvent.getSource()).get();

            // selection logic
            if (newWFV == wfv) {    // its us
               if (amSelected) {
                   // already selected :. noop
               } else {
                   setSelected(true);  // shouldn't happen since no listener if
                                       // not already selected
               }

            } else {                // its NOT us
               if (amSelected) {
                   setSelected(false); // we were UNselected
               } else {                // not us & we're not selected, who cares?
                   // noop
               }
            }
//            if (amSelected && newWFV != wfv) setSelected(false);
          }
      }  // end of WFViewListener
*/

/** INNER CLASS: Handle changes to the selected time/amp window. */
      private class WFWindowListener implements ChangeListener {
          public void stateChanged (ChangeEvent changeEvent) {
              if (amSelected) repaint();
          }
      }  // end of WFWindowListener


    /** INNER CLASS: Change events are expected from Waveform objects when the
     *  timeseries is loaded. Repaint the WFPanel when timeseries shows up.
     *  This is to support asynchronous loading of timeseries in background.
     *  This over-rides WFPanel.WFLoadListener because it must update the
     *  ActiveWFVModel. If this WFPanel "isSelected" and the ActiveWFVModel was
     *  set BEFORE time-series was available the max/min amp bounds could not be
     *  set to fit the data. This must be adjusted now that the time-series is
     *  loaded. */
    private class ActiveWFLoadListener implements ChangeListener {
        public void stateChanged (ChangeEvent changeEvent) {
            if (SwingUtilities.isEventDispatchThread()) {
                        setupWf();
                        if (amSelected){
                          if (wfv.mv != null) {
                            wfv.mv.masterWFWindowModel.setAmpSpan(getWf(), dataBox.getMaxAmp(), dataBox.getMinAmp());
                          }
                        }
                        repaint();
            }
            else {
              SwingUtilities.invokeLater(
                new Runnable() {
                    public void run() {
                        setupWf();
                        // Adjust WFWindowModel's ampspan to the max/min of the data if Waveform data just got loaded for the first time
                        if (amSelected){
                          if (wfv.mv != null) {
                            wfv.mv.masterWFWindowModel.setAmpSpan(getWf(), dataBox.getMaxAmp(), dataBox.getMinAmp());
                          }
                        }
                        repaint();
                    }
                }
              );
            }
        }
    }    // end of Listener

    public void paintForeground(Graphics g) {
          paintScanWindowTimes(g);
          paintCursor(g);
    }

    private void paintScanWindowTimes(Graphics g) {

        if (scanWindowStartTime == 0. && scanWindowEndTime == 0.) return; 

        Graphics2D  g2d = (Graphics2D) g;
        Color oldColor = g2d.getColor();  // remember current color to reset to later
        Stroke oldStroke = g2d.getStroke();  // remember current stroke to reset to later

        BasicStroke bs = new BasicStroke(2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 10.f, new float [] {5.f, 5.f}, 5);
        g2d.setColor(wfColor);
        g2d.setStroke(bs);

        int windowPix = 0;
        if (scanWindowStartTime > 0.) windowPix = pixelOfTime(scanWindowStartTime);
        if (windowPix > 0) g.drawLine(windowPix, 0, windowPix, getHeight());
        windowPix = 0;
        if (scanWindowEndTime > 0.) windowPix = pixelOfTime(scanWindowEndTime);
        if (windowPix > 0) g.drawLine(windowPix, 0, windowPix, getHeight());

        g2d.setColor(oldColor);
        g2d.setStroke(oldStroke);
    }

   /** */
    public void paintBackground (Graphics g) {
      //System.out.println("Painting background of ActiveWFPanel."); /// snork
         Color oldColor = g.getColor();
         if (amSelected) {
              setBackgroundColor(selectedColor);
        } else {
              setBackgroundColor(unselectedColor);
        }
        super.paintBackground(g);

        // paint any highlight areas over the background but under the foreground
        if (wfv.mv != null) {
        //WFSelectionBox viewBox =  wfv.mv.masterWFWindowModel.getSelectionBox(getWf());
        if (amSelected) paintHighlight (g, wfv.mv.masterWFWindowModel.getSelectionBox(getWf()), highlightColor);

           // paint phase location cues
           if (getShowPhaseCues()) paintPhaseCues(g);

           if (amDragging) paintHighlight (g, dragBox, dragColor);
        }
        g.setColor(oldColor);
    }
 /**
 * Paint a highlighted box. It will always be at least 1 pixel in each dimension
 * so it will be visible.
 */
     public void paintHighlight(Graphics g, WFSelectionBox sb, Color color) {
        Color oldColor = g.getColor();
        int xx1, xx2, yy1, yy2;

          xx1 = pixelOfTime(sb.timeSpan.getStart());
          xx2 = pixelOfTime(sb.timeSpan.getEnd());

       // make sure its always at least 1 pixel wide
       if (xx1 == x2) xx2 += 1;

          yy1 = pixelOfAmp (sb.maxAmp);
          yy2 = pixelOfAmp (sb.minAmp);

        // make sure its always at least 1 pixel high
       if (yy1 == yy2) yy2 += 1;

        g.setColor(color);
        g.fillRect(xx1, yy1, xx2 - xx1, yy2 - yy1);
        g.setColor(oldColor);
    }
/** Return true if the mouse cursor was dragged. This enforces the "sloppy" constraints
 *  that don't consider it a real drag unless it moves some number of pixels.
 *  This prevents sloppy clicks from appearing to the user as if clicks
 *  "aren't working." */
    boolean wasDragged() {
      return (Math.abs(x1-x2) > sloppyDragX);
    }
//    boolean badDragged() {
////      return Math.abs(x1-x2) <= sloppyDragX || Math.abs(y1-y2) <= sloppyDragY;
//      return Math.abs(x1-x2) <= sloppyDragX;
//    }
// -------------------------------------------------------------------
/**
 *  Mouse interaction:
 *        1) Single Click Left button:
 *            Select new trace and put in zoomed panel centered on the
 *            time clicked, use existing timeSpan for the view, autoscale
 *            the amp axis.
 *        2) Double Click Left button:
 *            Select new trace and put in zoomed panel, reset timeSpan to
 *            include whole seismogram in the view, autoscale the amp axis.
 *        3) Right Button Click:
 *            Popup the popup
 *      4) Right Button Drag:
 *          Select a time window, set amp to full scale.
 *        5) Middle Button Drag:
 *            Select new trace and put in zoomed panel, select timeSpan AND
 *            Amp span using the highlighted dragged time.
 *            Treat these the same because some computers only have
 *            two buttons.
 */

 /* This is NOT called after a mouse drag! */

/*
  NOTE:
  On a mouse click the event handler will call:
  - MousePressed()
  - MouseReleased()
  - MouseClicked()

  On a drag it will call:
  - MousePressed()
  - MouseReleased()
*/

  private class MsHandler extends MouseAdapter {
    ActiveWFPanel wfp;   // source of the mouse action
    int mods;            // mouse modifiers
    int buttonMask;      // mouse mask (which button was pressed)
    WFSelectionBox oldBox;

    // Button pressed and released without moving. The mouse-click event is processed AFTER the mouse-released event.
    // BUG - Viewport Bombsight line disappears when mouse clicked on top of it, the panel x-center line,
    // i.e. no change in scroll time -aww.
    public void mouseClicked(MouseEvent e) {
        wfp = (ActiveWFPanel) e.getSource();

        if (wfp == null) return;
        // System.out.println("DEBUG ActiveWFPanel mouseClicked: " + e.toString());
        // remember the mouse click point. Used by popup listener
        mouseClickPoint = e.getPoint();

        double clickTime = getSampletimeNearX(mouseClickPoint.x);

        int clickCnt = e.getClickCount();
        // modifiers tell us which button, key is pressed, after click
        mods = e.getModifiers();
        int modsex = e.getModifiersEx();
        /*
        System.out.println(" MODS: " + Integer.toBinaryString(mods));
        System.out.println(" B1_MASK:"+Integer.toBinaryString(MouseEvent.BUTTON1_MASK)+" B1_DOWN:"+Integer.toBinaryString(MouseEvent.BUTTON1_DOWN_MASK));
        System.out.println(" B2_MASK:"+Integer.toBinaryString(MouseEvent.BUTTON2_MASK)+" B2_DOWN:"+Integer.toBinaryString(MouseEvent.BUTTON2_DOWN_MASK));
        System.out.println(" B3_MASK:"+Integer.toBinaryString(MouseEvent.BUTTON3_MASK)+" B3_DOWN:"+Integer.toBinaryString(MouseEvent.BUTTON3_DOWN_MASK));
        System.out.println(" CTRL_MASK:"+Integer.toBinaryString(MouseEvent.CTRL_MASK)+" CTRL_DOWN:"+Integer.toBinaryString(MouseEvent.CTRL_DOWN_MASK));
        System.out.println(" ALT_MASK:"+Integer.toBinaryString(MouseEvent.ALT_MASK)+" ALT_DOWN:"+Integer.toBinaryString(MouseEvent.ALT_DOWN_MASK));
        System.out.println(" SHIFT_MASK:"+Integer.toBinaryString(MouseEvent.SHIFT_MASK)+" SHIFT_DOWN:"+Integer.toBinaryString(MouseEvent.SHIFT_DOWN_MASK));
        */

        //System.out.println("Clickcnt: " + clickCnt);
        if ( ( (modsex & resetScanTimeMask) == resetScanTimeMask) || (clickCnt > 1) ) {  // && ((mods&maskA1)==maskA1 || (mods&maskC1)==maskC1)) 
            //System.out.println("Resetting  panel scan window timespan");
            setScanWindowTimes(0.,0.);
            repaint();
            return;
        }

        //if ( (mods & maskA2) == CTRL_MASK | MouseEvent.BUTTON2_MASK) ) {
        if ( ( (mods & maskA2) == maskA2) && (buttonMask == MouseEvent.BUTTON2_DOWN_MASK) && 
                ( (modsex & MouseEvent.ALT_DOWN_MASK) == MouseEvent.ALT_DOWN_MASK) ) { // noise SCAN
            if (clickCnt == 1) { 

              if ( !lastScanType.equals("Nse") ) setScanWindowTimes(0.,0.);
              lastScanType =  "Nse";

              //System.out.print("N1 time: " + LeapSeconds.trueToString( clickTime, "HH:mm:ss.fff").substring(0,11));
              if (scanWindowStartTime == 0. || clickTime < scanWindowStartTime) {
                  scanWindowStartTime =  clickTime;
                  repaint();
                  if (scanWindowEndTime == 0. || scanWindowEndTime < scanWindowStartTime) return;
              }
              else if (clickTime > scanWindowEndTime) {
                  scanWindowEndTime = clickTime;
              }
              else if (clickTime > scanWindowStartTime && clickTime < scanWindowEndTime) { 
                  int answer = JOptionPane.showOptionDialog(Jiggle.jiggle,
                        "Noise window start or end?", "Noise Scan Window Time Picked",
                        JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null ,
                        new String [] {"Start", "End"}, "End");
                  if (answer == JOptionPane.YES_OPTION) {
                      scanWindowStartTime =  clickTime;
                      repaint();
                      if (scanWindowEndTime == 0. || scanWindowEndTime < scanWindowStartTime) return;
                  }

                  scanWindowStartTime =  clickTime;
                  repaint();
                  if (scanWindowEndTime == 0. || scanWindowEndTime < scanWindowStartTime) return;
              }
              else {
                  scanWindowEndTime = clickTime;
              }

              createNoiseReading(new TimeSpan(scanWindowStartTime,scanWindowEndTime), mods);

              repaint();
              return;
            }
        }
        //else if ( (mods & maskC2) == CTRL_MASK | MouseEvent.BUTTON2_MASK) ) {
        else if ( ((mods & maskC2) == maskC2) && (buttonMask == MouseEvent.BUTTON2_DOWN_MASK) &&
                ( (modsex & MouseEvent.CTRL_DOWN_MASK) == MouseEvent.CTRL_DOWN_MASK) ) { // Phase SCAN
            if (clickCnt == 1) { 

              if (! lastScanType.startsWith("pk")) {
                  int answer = JOptionPane.showOptionDialog(Jiggle.jiggle,
                        "Pick which phase(s)?", "Auto Pick Arrival Time",
                        JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null ,
                        new String [] {"P", "S", "P&S"}, (wfp.wfv.getChannelObj().isVertical() ? "P" : "S"));
                  if (answer == JOptionPane.CLOSED_OPTION) return;

                  phasePickFlag = 0; 
                  if (answer == 0) {
                      phasePickFlag = PhasePickerIF.P_ONLY;
                      if ( !lastScanType.equals("pkP") ) setScanWindowTimes(0.,0.);
                      lastScanType =  "pkP";
                  }
                  else if (answer == 1) {
                      phasePickFlag = PhasePickerIF.S_ONLY;
                      if ( !lastScanType.equals("pkS") ) setScanWindowTimes(0.,0.);
                      lastScanType =  "pkS";
                  }
                  else if (answer == 2) {
                      phasePickFlag = PhasePickerIF.P_AND_S;
                      if ( !lastScanType.equals("pkPS") ) setScanWindowTimes(0.,0.);
                      lastScanType =  "pkPS";
                  }
              }

              //System.out.print("P1 time: " + LeapSeconds.trueToString( clickTime, "HH:mm:ss.fff").substring(0,11));
              if (scanWindowStartTime == 0. || clickTime < scanWindowStartTime) {
                  scanWindowStartTime =  clickTime;
                  repaint();
                  if (scanWindowEndTime == 0. || scanWindowEndTime < scanWindowStartTime) return;
              }
              else if (clickTime > scanWindowEndTime) {
                  scanWindowEndTime = clickTime;
              }
              else if (clickTime > scanWindowStartTime && clickTime < scanWindowEndTime) { 
                  int answer = JOptionPane.showOptionDialog(Jiggle.jiggle,
                        "Phase window start or end?", "Phase Scan Window Time Picked",
                        JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null ,
                        new String [] {"Start", "End"}, "End");
                  if (answer == JOptionPane.YES_OPTION) {
                      scanWindowStartTime =  clickTime;
                      repaint();
                      if (scanWindowEndTime == 0. || scanWindowEndTime < scanWindowStartTime) return;
                  }

                  scanWindowStartTime =  clickTime;
                  repaint();
                  if (scanWindowEndTime == 0. || scanWindowEndTime < scanWindowStartTime) return;
              }
              else {
                  scanWindowEndTime = clickTime;
              }

              createPhaseReading(new TimeSpan(scanWindowStartTime,scanWindowEndTime), mods, phasePickFlag);

              repaint();
              return;
            }
        }
        //else if ( (mods & maskA1) == (MouseEvent.ALT_MASK | MouseEvent.BUTTON1_MASK) ) {
        else if ( ((mods & maskA1) == maskA1) && (buttonMask == MouseEvent.BUTTON1_DOWN_MASK) ) { // Amp SCAN
            if (clickCnt == 1) { 
              if ( !lastScanType.equals("AMP") ) {
                  setScanWindowTimes(0.,0.);
              }
              lastScanType = "AMP";
              //System.out.print("ALT-DOWN time: " + LeapSeconds.trueToString( clickTime, "HH:mm:ss.fff").substring(0,11)); // now UTC true seconds - aww 2008/02/09
              if (scanWindowStartTime == 0. || clickTime < scanWindowStartTime) {
                  scanWindowStartTime =  clickTime;
                  repaint();
                  if (scanWindowEndTime == 0. || scanWindowEndTime < scanWindowStartTime) return;
              }
              else if (clickTime > scanWindowEndTime) {
                  scanWindowEndTime = clickTime;
              }
              else if (clickTime > scanWindowStartTime && clickTime < scanWindowEndTime) { 
                  int answer = JOptionPane.showOptionDialog(Jiggle.jiggle,
                        "Amp window start or end?", "Amp Scan Window Time Picked",
                        JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null ,
                        new String [] {"Start", "End"}, "End");
                  if (answer == JOptionPane.YES_OPTION) {
                      scanWindowStartTime =  clickTime;
                      repaint();
                      if (scanWindowEndTime == 0. || scanWindowEndTime < scanWindowStartTime) return;
                  }
                  else {
                      scanWindowEndTime = clickTime;
                  }
              }

              createMagReading("Amp", new TimeSpan(scanWindowStartTime,scanWindowEndTime), mods);
              repaint();
              return;
            }
        }
        //else if ( (mods & maskC1) == (MouseEvent.CTRL_MASK | MouseEvent.BUTTON1_MASK) ) {
        //else if ( ((mods & maskC1) == maskC1) && (buttonMask == MouseEvent.BUTTON1_DOWN_MASK) ) {
        else if ( (( keyStatusMap != null && keyStatusMap.get(KeyEvent.getKeyText(KeyEvent.VK_C)) != null ) ||
                   ((mods & codaSelectMask) == codaSelectMask)
                  ) && (buttonMask == MouseEvent.BUTTON1_DOWN_MASK) )
        {
            if (clickCnt == 1) { 
              if ( !lastScanType.equals("CDA") ) {
                  setScanWindowTimes(0.,0.);
              }
              lastScanType = "CDA";
              //System.out.print("CTRL-DOWN time: " + LeapSeconds.trueToString( clickTime, "HH:mm:ss.fff").substring(0,11)); // now UTC true seconds - aww 2008/02/09
              if (scanWindowStartTime == 0. || clickTime < scanWindowStartTime) {
                  scanWindowStartTime =  clickTime;
                  repaint();
                  if (scanWindowEndTime == 0. || scanWindowEndTime < scanWindowStartTime) return;
              }
              else if (clickTime > scanWindowEndTime) {
                  scanWindowEndTime = clickTime;
              }
              else if (clickTime > scanWindowStartTime && clickTime < scanWindowEndTime) { 
                  int answer = JOptionPane.showOptionDialog(Jiggle.jiggle,
                        "Coda window start or end?", "Coda Scan Window Time Picked",
                        JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null ,
                        new String [] {"Start", "End"}, "End");
                  if (answer == JOptionPane.YES_OPTION) {
                      scanWindowStartTime =  clickTime;
                      repaint();
                      if (scanWindowEndTime == 0. || scanWindowEndTime < scanWindowStartTime) return;
                  }
                  else {
                      scanWindowEndTime = clickTime;
                  }
              }

              createMagReading("Coda", new TimeSpan(scanWindowStartTime,scanWindowEndTime), mods);
              repaint();
              return;
            }
        }

        // ignore rightclick, it was handled in mousePressed()
        if (e.isPopupTrigger() ||
             (mods & MouseEvent.BUTTON2_MASK) != 0 ||
             (mods & MouseEvent.BUTTON3_MASK) != 0) return;

// <> One click
   // Move select Box so its centered on clicktime. Never change the size of
   // the selected time but change the size of the amp to show whole amp.
   // (don't change its dimensions unless a new WFView is selected
        if (clickCnt == 1) {
            //double clickTime = getSampletimeNearX(mouseClickPoint.x);

            if (UnpickMode.isTrue()) {
                // Delete it in Solution's list so listeners know
                if (wfv.mv != null) {
                  Solution sol = wfv.mv.getSelectedSolution();
                  if (UnpickMode.getMode() == UnpickMode.PHASE) {
                      Phase ph = (Phase) wfv.phaseList.getNearestToTime(clickTime, sol);
                      wfv.mv.addToPhaseUndoList(ph, true);
                      sol.erase(ph);
                  }
                  else if (UnpickMode.getMode() == UnpickMode.AMP) {
                      Amplitude amp = (Amplitude) wfv.ampList.getNearestToTime(clickTime, sol);
                      wfv.mv.addToAmpUndoList(amp, true);
                      sol.erase(amp);
                          Magnitude mag = sol.getPrefMagOfType("l");
                          if (mag != null) mag.getReadingList().erase(amp);
                  }
                  else if (UnpickMode.getMode() == UnpickMode.CODA) {
                      Coda coda = (Coda) wfv.codaList.getNearestToTime(clickTime, sol); 
                      wfv.mv.addToCodaUndoList(coda, true);
                      sol.erase(coda);
                          Magnitude mag = sol.getPrefMagOfType("d");
                          if (mag != null) mag.getReadingList().erase(coda);
                  }
                }
            } else {
               processSelection(clickTime);
            }
        }

        /* removed
        else { // double (or more) click, create selectedBox showing whole trace
            if (wfv.mv != null) {
              wfv.mv.masterWFWindowModel.setFullView(getWf());

              // don't notify scroller to scroll, if unchanged picking panel -aww 2009/04/03
              if (ActiveWFPanel.this instanceof PickableWFPanel) wfv.mv.masterWFViewModel.set(wfv, false);
              else wfv.mv.masterWFViewModel.set(wfv, true); // always notify zoompanel scroller
            }
        }
        */

        dragBox.setNull();  // erase drag box
    }

    protected void processSelection(double clickTime) {
      // this may also be done in a WFViewList object that contains us
      // but calling it twice does no harm
      setSelected(true);    // select self (if not already)

      // notify other components
      if (wfv.mv != null) {

    // WFView change
      //if (wfv.mv.masterWFViewModel.set(wfv)) {   // replaced by below ... -aww 2009/04/03
      //don't notify scroller to scroll if unchanged picking panel, else always notify scroller -aww 2009/04/03
      boolean newView = (ActiveWFPanel.this instanceof PickableWFPanel) ?
          wfv.mv.masterWFViewModel.set(wfv, false): wfv.mv.masterWFViewModel.set(wfv, true);
      if (newView) {
        // Window size...
        oldBox = wfv.mv.masterWFWindowModel.getSelectionBox(getWf()); // get old, may be null

        if (oldBox.isNull()) {          // null if first time selected
          wfv.mv.masterWFWindowModel.setFullView(getWf());
          wfv.mv.masterWFWindowModel.setCenterTime(clickTime);
        } else {
          // scale amp to this WFView
          //wfv.mv.masterWFWindowModel.setFullAmp(getWf()); //TEST REMOVE for scaling test below -aww
          wfv.mv.masterWFWindowModel.setAmpSpan(getWf(), dataBox); // TEST noise,gain scaling -aww 2011/10/24
          wfv.mv.masterWFWindowModel.setCenterTime(clickTime);
        }

      } else {      // selected WFView didn't change, just move window
          //System.out.println("DEBUG AWFP processSelection time: " + LeapSeconds.trueToString(clickTime));
          wfv.mv.masterWFWindowModel.setCenterTime(clickTime);  // wfp the same
      }

  }
}

/**
 * Event handler is called when ANY mouse button is pressed. :. must check which.
 * This selects the WFView and begins a drag box. This defines the starting corner
 * of the drag box, the position at mouseReleased will define the opposing corner.
 */
        public void mousePressed(MouseEvent e) {

            wfp = (ActiveWFPanel) e.getSource();

            if (wfp == null) return;

            // IMPORTANT: getting focus enables default keyboard control
            requestFocus();  // request focus when clicked for keyboard interation, etc.

            mouseDown = true;
            x1 = x2 = y1 = y2 = 0;

            // want to preserve ampSpan of zoomed panel
            dragBox.set(getVisibleBox());

            // set starting point of the selection area for either button
            startPoint = e.getPoint();
            mods = e.getModifiers();
            buttonMask = e.getModifiersEx() & (MouseEvent.BUTTON1_DOWN_MASK | MouseEvent.BUTTON2_DOWN_MASK | MouseEvent.BUTTON3_DOWN_MASK);
            //System.out.println(" PRESSED MODSEX: " + Integer.toBinaryString(e.getModifiersEx()) + " buttonMask: " + Integer.toBinaryString(buttonMask) );

            //System.out.print("PRESSED MouseEvent.BUTTON1_MASK: " + Integer.toBinaryString(MouseEvent.BUTTON1_MASK));
            //System.out.println(" MODS: " + Integer.toBinaryString(mods));

            if ( (e.isPopupTrigger() && ! ((e.getModifiersEx() & MouseEvent.CTRL_DOWN_MASK) == MouseEvent.CTRL_DOWN_MASK))
                    || (mods & MouseEvent.BUTTON3_MASK) != 0 ) {
                //System.err.println("mousePressed - popup trigger or (button 3)");
                mouseClickPoint = e.getPoint();
                makePopup(mouseClickPoint);
                mouseDown = false;

            } else if ( (mods & MouseEvent.BUTTON2_MASK) != 0 ||
                 ( (mods & MouseEvent.BUTTON1_MASK) != 0 &&
                        (mods & MouseEvent.SHIFT_MASK) != 0 ) ) {

              dragAmp = true;
            } else if ((mods & MouseEvent.BUTTON1_MASK) != 0) {        // must be button#1, change time only
              dragAmp = false;
            }

        }

        /**
        * Handle end of dragging to set the selection Box that was
        * set in the mouseDragged() method to the selected box.
        * Method is called whether or not mouse was dragged. :. is called on a click
        * or double click but those are ignored because action is only taken here
        * if wasDragged = true.
        */
        public void mouseReleased(MouseEvent e) {

            wfp = (ActiveWFPanel) e.getSource();
            if (wfp == null) return;
            // If no dragging happened, let mouseClicked() handle things.
            //System.out.println("release: x = "+ wfp.x1+" - "+wfp.x2);

            mouseDown = false;

            if (wasDragged()) {
              if ( (mods & maskA1) == maskA1 || (mods & maskC1) == maskC1) {
                  //System.out.println("Mouse dragged and released with maskC1 or maskA1...");
                  //mouseClickPoint = e.getPoint();
                  //double uncEndTime = getSampletimeNearX(e.getPoint().x);
                  double uncEndTime =  dtOfPixel(e.getPoint());
                  if (wfv.hasPhases()) {
                    Solution sol = wfv.mv.getSelectedSolution();
                    //System.out.println("    uncStartTime: " + LeapSeconds.trueToString(uncStartTime));
                    //System.out.println("    uncEndTime  : " + LeapSeconds.trueToString(uncEndTime)); 
                    if (uncStartTime > 0.) {
                      TimeSpan ts = new TimeSpan(uncStartTime, uncEndTime);
                      //Phase ph = (Phase) wfv.phaseList.getWithinTimeSpan(ts, sol);
                      Phase ph = (Phase) wfv.phaseList.getNearestToTime(ts.getCenter(), sol);
                      // set the phase "deltaTime" to evaluate pick "quality" 
                      if ( (ph != null) && (ts.getDuration() > 0.) && ts.contains(ph.getTime()) ) {
                          double dur = ts.getDuration();
                          double qual = ph.deltaTimeToQuality(dur);
                          if (ph.description.fm != PhaseDescription.DEFAULT_FM && qual < ph.description.getFmQualCut()) {
                              int answer = JOptionPane.showConfirmDialog(wfp.getTopLevelAncestor(),
                                    "Delta time quality < first motion cutoff, remove phase fm?", "First Motion Quality Check",
                                   JOptionPane.YES_NO_OPTION);
                              if (answer == JOptionPane.YES_OPTION) {
                                  ph.description.fm = PhaseDescription.DEFAULT_FM; // should "reset" fm here
                              }
                          }

                          ph.setDeltaTime(ts.getDuration()); // should "reset" quality but not fm? -aww 2010/01/24
                          repaint(); // repaint to update bar delta time centered on pick -aww 2010/01/24
                      }
                    } 
                  }
                  amDragging = false;
              }
              else {
                //System.out.println("DEBUG ActiveWFPanel was dragging");
                setSelected(true);
                amDragging = false;
                if (wfv.mv != null) {
                  //wfv.mv.masterWFViewModel.set(wfv); replaced by below ...
                   // don't notify scroller to scroll, if unchanged picking panel -aww 2009/04/03
                  if (ActiveWFPanel.this instanceof PickableWFPanel) wfv.mv.masterWFViewModel.set(wfv, false);
                  else wfv.mv.masterWFViewModel.set(wfv, true); // always notify zoompanel scroller
                  wfv.mv.masterWFWindowModel.set(getWf(), dragBox);
                }
              }
            // treat a short drag like a click
            // Logic readded by ddg, 3/2/05 else mouseClicked() is called twice!
            } else if (x1 != x2) {
            //} else {     // a sloppy click: so we'll call mouseClicked()
              //System.out.println("sloppy");
              mouseClicked(e);
            }
        }    // end of mouseReleased

        public void mouseExited(MouseEvent e) {
            wfp = (ActiveWFPanel) e.getSource();
            if (wfp == null) return;
            cursorTime = Double.NaN;
            cursorAmp = Double.NaN;
            repaint();
            if (wfv.mv == null) return;
            Object obj =  wfv.mv.getOwner();
            //
            if (obj instanceof Jiggle) {
                Jiggle jiggle = (Jiggle) obj;
                if (jiggle.pickPanel != null && jiggle.pickPanel.isVisible() && jiggle.pickPanel.zwfp != null) {
                    ActiveWFPanel zwfp = jiggle.pickPanel.zwfp;
                    if (zwfp.isShowing()) {
                      //if (wfv != null && zwfp.wfv != null && zwfp.wfv.equalsChannelId(wfv)) {
                      if (wfv != null && zwfp.wfv != null && zwfp.wfv.sameTriaxialAs(wfv)) {
                        zwfp.cursorTime = Double.NaN;
                        zwfp.cursorAmp = Double.NaN;
                        zwfp.repaint();
                      }
                    }
                }
                if (jiggle.wfScroller != null && jiggle.wfScroller.groupPanel != null) {
                    ArrayList wfpList = jiggle.wfScroller.groupPanel.wfpList.getTriaxialList(wfv);
                    WFPanel twfp = null;
                    for (int idx = 0; idx < wfpList.size(); idx++) {
                      twfp = (WFPanel) wfpList.get(idx);
                      if (twfp.isShowing()) {
                          if (wfv != null && twfp.wfv != null && twfp.wfv.sameTriaxialAs(wfv)) {
                            twfp.cursorTime = Double.NaN;
                            twfp.cursorAmp = Double.NaN;
                            twfp.repaint(); // destroys ChannelLabelViewport labeling
                            // ??
                          }
                      }
                    }
                }
            }
            //
        }
    } // end of MsHandler class

//---------------------------------------------------------------------
/**
 * Allow dynamic painting of the selected area.
 */

  double uncStartTime = 0.;

  private class MsMoveHandler extends MouseMotionAdapter {

    ActiveWFPanel wfp;   // source of the mouse action
    int mods;            // mouse modifiers

    public void mouseDragged(MouseEvent e) {

        wfp = (ActiveWFPanel) e.getSource();

        if (wfp == null) return;

        mods = e.getModifiers();
        //System.out.print("DRAGGED MouseEvent.BUTTON1_MASK: " + Integer.toBinaryString(MouseEvent.BUTTON1_MASK));
        //System.out.println(" MODS: " + Integer.toBinaryString(mods));

        if (mouseDown)            // active dragging
        {
//          wasDragged = true;

          Point cursorLoc = e.getPoint();

          // handle case when cursor moves to left of original point (backward selection)
          if (cursorLoc.x < startPoint.x)   // to the left, negative
          {
              x1 = cursorLoc.x;
              x2 = startPoint.x;

          }
          else            // to the right, positive
          {
              x1 = startPoint.x;
              x2 = cursorLoc.x;
          }

          if (cursorLoc.y < startPoint.y)   // above, negative
          {
              y1 = cursorLoc.y;
              y2 = startPoint.y;

          }
          else            // below, positive
          {
              y1 = startPoint.y;
              y2 = cursorLoc.y;
          }

// clip selection box to dimensions of the panel. In the future will want to
// allow autoscrolling if you go outside the viewport.
          x1 = Math.max(x1, 0);
          x2 = Math.min(x2, getWidth());
          y1 = Math.max(y1, 0);
          y2 = Math.min(y2, getHeight());

       // Protect users from fumble-fingered clicks that are really a drag
       // or vertical drags with zero width.
       if (Math.abs(x1-x2) > sloppyDragX) {
         if ( (mods & maskA1) == maskA1 || (mods & maskC1) == maskC1) {
            //System.out.println("Mouse dragged with maskC1 or maskA1...");
            uncStartTime =  dtOfPixel(x1);
         }
         else {
            uncStartTime = 0.;
         }

         //System.out.println("DEBUG ActiveWaveformPanel Drag after clip: x = "+ x1+" - "+x2);
         // set time span for either button
            dragBox.setTimeSpan(dtOfPixel(x1), dtOfPixel(x2));

         // set Amp (vertical) part of box only for middle or right button
            if (dragAmp) {
              y1 = Math.max(y1, 0);
              y2 = Math.min(y2, getHeight());
              dragBox.setAmpSpan((int) ampOfPixel(y1),
                                      (int) ampOfPixel(y2));
            }

            // display dragged rectangle, don't use selectionModel here because we
            // don't want the expense of the zoomed view getting repainted as we
            // drag. It's also rather annoying.
            // Completion of the drag will be processed by mouseReleased()
            // wasDragged = true;
            amDragging = true;
            repaint();
        }

      }
    }

  /**
   * Update cursor location model dynamically as mouse cursor moves about
   */
    public void mouseMoved(MouseEvent e) {

        ActiveWFPanel wfp = (ActiveWFPanel) e.getSource();
        if (wfp == null) return;

        if (cursorLocModel != null) {
            cursorLocModel.set(wfp, e.getPoint());
        }

        if (wfv == null) return;

        double dt = dtOfPixel(e.getPoint()); // +0.005; // removed .005 -aww 2008/07/18
        setToolTipText(getRowHeader().getToolTipText()+ LeapSeconds.trueToString(dt , "HH:mm:ss.fff")); // substring(0,12)) UTC 

        cursorTime = dt;
        cursorAmp = ampOfPixel(e.getPoint());

        repaint(); // destroys trailing ChannelLabelViewport labeling

        if (wfv.mv == null) return;

        Object obj =  wfv.mv.getOwner();
        //
        if (obj instanceof Jiggle) {
            Jiggle jiggle = (Jiggle) obj;
            jiggle.setSwarmCursorToTime(dt);
            if (jiggle.pickPanel != null && jiggle.pickPanel.isVisible()) {
                if (jiggle.pickPanel.zwfp != ActiveWFPanel.this && jiggle.pickPanel.zwfp != null) {
                    ActiveWFPanel zwfp = jiggle.pickPanel.zwfp;
                    if (zwfp.isShowing()) {
                        if (wfv != null && zwfp.wfv != null && zwfp.wfv.sameTriaxialAs(wfv)) {
                          zwfp.cursorTime = cursorTime;
                          // zwfp amp line only if same channel else you see bias offset between components 
                          if (wfv != null && zwfp.wfv != null && zwfp.wfv.equalsChannelId(wfv)) {
                              zwfp.cursorAmp = cursorAmp;
                          }
                          zwfp.repaint(); // destroys trailing ChannelLabelViewport labeling
                        }
                    }
                }
            }
            if (jiggle.wfScroller != null && jiggle.wfScroller.groupPanel != null) {
                ArrayList wfpList = jiggle.wfScroller.groupPanel.wfpList.getTriaxialList(wfv);
                WFPanel twfp = null;
                for (int idx = 0; idx < wfpList.size(); idx++) {
                  twfp = (WFPanel) wfpList.get(idx);
                  if (twfp.isShowing()) {
                      if (wfv != null && twfp.wfv != null && twfp.wfv.sameTriaxialAs(wfv)) {
                        twfp.cursorTime = cursorTime;
                        //twfp.cursorAmp = cursorAmp;  // amp line tracking enabled -aww  2011/04/12
                        twfp.repaint(); // destroys ChannelLabelViewport labeling
                      }
                  }
                }
            }
        }
        //
    }
  }
} // end of MultiWFPanel class
