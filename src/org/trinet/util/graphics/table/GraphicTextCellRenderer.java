package org.trinet.util.graphics.table;
import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import org.trinet.util.*;

    public class GraphicTextCellRenderer extends JPanel implements TableCellRenderer {
	static final int borderWidth = 2;
	final int height;
	final int baseline;
	final int maxCharWidth;

	int width;
	Color defaultBackgroundColor;
	Color selectedBackgroundColor;

	Color background;
	Color foreground = Color.black;

	String text = "";

	public GraphicTextCellRenderer(FontMetrics metrics, Color defaultBackgroundColor, Color selectedBackgroundColor) {
	    super();
	    baseline = metrics.getAscent() + borderWidth;
	    this.height = metrics.getHeight() + (2 * borderWidth);
//	    this.width = height; // not necessary, arbitrary here
	    this.defaultBackgroundColor = defaultBackgroundColor;
	    this.selectedBackgroundColor = selectedBackgroundColor;
	    this.maxCharWidth = metrics.charWidth('M');
	}

	public void setSelectedColor(Color selectedBackgroundColor) {
	    this.selectedBackgroundColor = selectedBackgroundColor;
	}

	public void setBackgroundColor(Color defaultBackgroundColor) {
	    this.defaultBackgroundColor = defaultBackgroundColor;
	}

	public Dimension getPreferredSize() {
	    return new Dimension(width, height);
	}
	
	//public void paint(Graphics g) { // aww since extends JPanel
	public void paintComponent(Graphics g) { // aww test
//			Debug.println("paint: h,w :" + getWidth()  + " "  + getHeight());
            super.paintComponent(g); // aww test
	    try {
		g.setColor(background);
		g.fillRect(0, 0, getWidth(), getHeight());
		g.setColor(foreground);
		g.drawString(text, borderWidth, baseline);
	    }
	    catch (Exception ex) {
		 System.err.println("Msg: " + ex.getMessage());
		 ex.printStackTrace();
	    }
	}

	public Component getTableCellRendererComponent(JTable table, Object value, boolean selected,
		 boolean focus, int row, int col) {

	    if (selected) {
		background = selectedBackgroundColor;
	    }
	    else {
		background = defaultBackgroundColor;
	    }

	    setValue(value);

	    return this;
	}

	public void setValue(Object value) {
	    if (value == null)  text = "";
	    else text = value.toString();
	    width = text.length() * maxCharWidth + 2 * borderWidth;
	}
    }

