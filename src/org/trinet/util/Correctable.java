package org.trinet.util;
public interface Correctable {
  public boolean hasCorrection(String type);
  public void setCorrection(DataCorrectionIF dc);
  public DataCorrectionIF getCorrection(String type);
}

