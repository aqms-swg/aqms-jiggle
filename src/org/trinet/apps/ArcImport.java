package org.trinet.apps;

import org.testcontainers.containers.PostgreSQLContainer;
import org.trinet.hypoinv.ArcSummary;
import org.trinet.jasi.*;
import org.trinet.jdbc.datasources.DbaseConnectionDescription;
import org.trinet.jdbc.datatypes.DataString;
import org.trinet.jiggle.JiggleProperties;
import org.trinet.util.gazetteer.GeoidalLatLonZ;
import org.trinet.util.gazetteer.LatLonZ;
import org.trinet.util.locationengines.LocationEngineHypoInverse;
import test.common.DatabaseTests;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.FileSystems;
import java.sql.Connection;
import java.util.List;
import java.util.Scanner;

/**
 * ARCOUT file import created based on LocationEngineHypoInverse.java.
 * 
 * Configurable parameters in the prop file
 * 
 *    - Database configuration
 *    - appName - origin.subsource
 *    - locationEngineType - origin.algorithm
 *    - localNetCode - Sets origin.auth and determines origin.gtype based on lat/long
 *    - autoProcessing - True to set origin.rflag to "A". False may set the value to "I"
 */
public class ArcImport extends JasiPropertyList {

  final String APPNAME = "HYGPD";
  final long NEW_EVENT_ID = 0;
  
  // From SolutionTN.java
  // Level of commit, allows partial save back to database
  private static final int COMMIT_READINGS     = 2; // above plus arrival,amp,coda & assoc
  private static final int COMMIT_ALL          = 9; // everything

  String authString = "";
  JiggleProperties jp;
  String locatorName;

  public ArcImport(String propFileName) {
    jp = this.getProps(propFileName);
    getDbConnection(jp);
    this.locatorName = jp.getProperty("locationEngineType", "Unknown");
    if (jp.isSpecified("localNetCode")) {
      // default is "??"
      this.authString = jp.getProperty("localNetCode");
    }

    String appname = jp.getProperty("appName", APPNAME);
    if (appname.length() > 8) {
      // database constraint
      System.err.println("appName cannot be more than 8 characters. Default to " + APPNAME);
      EnvironmentInfo.setApplicationName(APPNAME);
    } else {
      EnvironmentInfo.setApplicationName(appname);
    }
  }

  // Created for Unit Testing to connect to a container
  public ArcImport(PostgreSQLContainer<?> container) {
    jp = this.convertPostgreContainerToJiggleProps(container);

    getDbConnection(jp);
    this.locatorName = jp.getProperty("locationEngineType", "Unknown");
    String appname = jp.getProperty("appName", APPNAME);
    if (appname.length() > 8) {
      // database constraint
      System.err.println("appName cannot be more than 8 characters. Default to " + APPNAME);
      EnvironmentInfo.setApplicationName(APPNAME);
    } else {
      EnvironmentInfo.setApplicationName(appname);
    }
  }

  public static void main(String[] args) {
    if (args.length < 2) {
      System.err.println("ArcImport requires two command line arguments");
      System.err.println("Usage: ArcImport [ARCOUT File] [Properties-file]");
      System.exit(1);
    } else {
      String arcOutFile = args[0];
      String propFile = args[1];

      ArcImport arc = new ArcImport(propFile);
      arc.importArc(arcOutFile);
    }
  }

  public int importArc(String arcFileName) {
    boolean status = readArcFile(arcFileName);
    return status ? 0 : 1;
  }

  private String getLocatorName() {
    return locatorName;
  }

  /**
   * Process ARCOUT file, create a solution, create a phaselist, and save to DB
   */
  private boolean readArcFile(String arcFileName){
    int total = 0;
    PhaseList currentPhaseList = new PhaseList();
    PhaseList existingPhaseList = new PhaseList();
    boolean isHeader = true;
    Solution currentSol = null;
    String arcHeader = "";
    ArcSummary arcSummary = new ArcSummary();
    boolean isNew = false; // True when creating a new solution

    File arcFile = new File(arcFileName);
    try (Scanner arcReader = new Scanner(arcFile)) {
      while (arcReader.hasNextLine()) {
        String data = arcReader.nextLine();

        // read summary line
        if (isHeader) {
          isHeader = false;
          arcHeader = data;

          if (arcSummary.parseArcSummary(arcHeader)) {
            
            if (arcSummary.evid == NEW_EVENT_ID) {
              // If event id is -1, then it's a new event
              System.out.println("INFO: Event Id is " + NEW_EVENT_ID + ". Create a new event.");
              
              currentSol = createNewSolution(arcSummary, arcHeader);
              isNew = true; 
              if (currentSol == null) {
                System.out.println("WARN: Cannot create a new solution");
                return false;
              }
            } else {
              currentSol = Solution.create().getById(arcSummary.evid);

              if (currentSol != null) {
                currentSol = createFromExistingSolution(currentSol, arcSummary, arcHeader); // returns null when error
                existingPhaseList = currentSol.getPhaseList();
              } else {
                System.out.println("WARN: Event Id is found, but the matching solution does not exist");
                return false;
              }
            }
          } else {
            System.err.println("Bad Arc Summary Header parseStatus");
            return false; // added short-circuit here -aww 2010/08/24
          }

          System.out.println("Processed summary header - Solution is " + currentSol);
        } else if (data.length() != 0 && !data.startsWith("     ")) {
          // Process phase list
          total = setPhaseFromARC(currentSol, data, currentPhaseList, existingPhaseList, total);
        } else {
          System.out.println("Skipping empty or last line of ARC output: " + data);
        }
        System.out.println(data);
      }
      arcReader.close();
    } catch (FileNotFoundException e) {
      System.err.println("Error reading ARCOUT file. Error: ");
      e.printStackTrace();
    }

    if (currentPhaseList != null) {

      currentSol.getPhaseList().clear();
      currentSol.getPhaseList().addAll(currentPhaseList);
      
      LatLonZ oldLatLonZ = null;
      oldLatLonZ = currentSol.getLatLonZ();
      currentSol = setEventDetails(currentSol, oldLatLonZ);
      currentSol.source.setValue(EnvironmentInfo.getApplicationName());
      
      if (isNew) {
        saveToDatabase(currentSol, COMMIT_READINGS);          
      } else {
        saveToDatabase(currentSol, COMMIT_ALL);
      }
    } else {
        System.err.println("Empty phase list in the solution. Cannot process phase list.");
    }

    return true;
  }

  private Solution setARCSummaryDetail(Solution currentSol, String arcHeader) {
    // read new solution dependent fields, method returns input
    // WARNING! Hypoformat method doesn't throw exception or
    // set any status after a bad parse so junk could be returned.
    currentSol = HypoFormat.parseArcSummary(currentSol, arcHeader);

    if (currentSol == null) {
      return null; // returns null solution for exception, otherwise the input currentSol
    } else {
      // set all the fields that HypoFormat doesn't have
      currentSol.getEventAuthority(); // resets the authority if region changed -aww 2011/09/21
      //
      // NOTE: regional eventAuthority or solution's origin authority could be different
      //if ( !currentSol.authority.equalsValue(currentSol.getEventAuthority()) ) // -aww 2011/08/17, removed 2011/09/21
      //    currentSol.authority.setValue(currentSol.getEventAuthority()); // -aww 2011/08/17, removed 2011/09/21
      if (!currentSol.authority.equalsValue(EnvironmentInfo.getNetworkCode())) // reverted back -aww 2011/09/21
        currentSol.authority.setValue(EnvironmentInfo.getNetworkCode());

      if (!currentSol.source.equalsValue(EnvironmentInfo.getApplicationName()))
        currentSol.source.setValue(EnvironmentInfo.getApplicationName());

      if (!currentSol.algorithm.equalsValue(getLocatorName()))
        currentSol.algorithm.setValue(getLocatorName());

      if (!currentSol.type.equalsValue(Solution.HYPOCENTER))
        currentSol.type.setValue(Solution.HYPOCENTER); // a Hypocenter

      // saved state effected by solCommitResetsRflag property which sets currentSol.commitResetsRflag -aww 2015/04/03
      if (currentSol.commitResetsRflag && !currentSol.processingState.equalsValue(EnvironmentInfo.getAutoString())) {
        currentSol.processingState.setValue(EnvironmentInfo.getAutoString()); //automatic or human?
      }

      if (!currentSol.validFlag.equalsValue(1)) {
        System.out.println("INFO: LocationEngine currentSol (deleted?) selectflag (validFlag) = 0");
        //currentSol.validFlag.setValue(1);  // valid? - remove 2014/10/30 why is it changing the "delete" status? -aww
      }

      // however could have phase parsing error try block exception below
      // or could have too few phases for valid solution
      if (!currentSol.dummyFlag.equalsValue(0))
        currentSol.dummyFlag.setValue(0); // - added 07/28/2006 aww

      if (!currentSol.horizDatum.equalsValue(LocationEngineHypoInverse.datumHorizontal))
        currentSol.horizDatum.setValue(LocationEngineHypoInverse.datumHorizontal);

      if (!currentSol.vertDatum.equalsValue(LocationEngineHypoInverse.datumVertical))
        currentSol.vertDatum.setValue(LocationEngineHypoInverse.datumVertical);
      
      return currentSol;
    }
  }
  
  private void saveToDatabase(Solution currentSol, int saveMode) {
    String dumpStr = currentSol.fullDump();
    System.out.println("********* OUTPUT CHANGED **********");
    System.out.println(dumpStr);

    try {
      System.out.println("Committing solution...");
      
      if (saveMode == COMMIT_READINGS) {
        currentSol.commitReadings();
      } else {
        currentSol.commit();
      }
    } catch (JasiCommitException e) {
      System.err.println("Error reading ARCOUT file. Error: ");
      e.printStackTrace();
    }
    System.out.println(currentSol.getCommitStatus());    
  }
  
  /**
   * Code from LocationEngineHypoInverse.readLocationResults to set solution details
   */
  private Solution setEventDetails(Solution currentSol, LatLonZ oldLatLonZ) {
    currentSol.gap.setValue(currentSol.getPhaseList().getMaximumAzimuthalGap());

    currentSol.totalReadings.setValue(currentSol.phaseList.getChannelWithInWgtCount()); // all with inWgt> 0 -aww 2011/05/11
    currentSol.usedReadings.setValue(currentSol.phaseList.getChannelUsedCount());
    currentSol.sReadings.setValue(currentSol.phaseList.getChannelUsedCount("S")); // all S with weight > 0 -aww 2011/05/11

    currentSol.setStale(false); // reset flag since solution is now "fresh"
    currentSol.setSolveDate(); // set current time

    //Check for location change from previous, if so, set "stale" state for dependent magnitudes -aww
    double deltaKm = oldLatLonZ.distanceFrom(currentSol.getLatLonZ());
    // Flag a commit if its origin change > 10 meters
    if ( deltaKm > 0.01) {
      // Only redo its magnitudes if origin change > 100 meters
      if (deltaKm >= 0.10) {
        currentSol.setMagnitudeStatusStale(); // force new magnitude calculation
      }
      else {
        currentSol.setMagnitudeNeedsCommit(); // force synch orid update - aww 2008/03/11
      }
    }

    return currentSol;
  }

  /**
   * Calculates the distance from input location to the Channelable elements
   * of the input list and sets the distance attribute of these elements.
   */
  public void calcDistances(List list, GeoidalLatLonZ latLonZ) { // aww 06/11/2004
    int count = list.size();
    if (count == 0) return;
    for (int i = 0; i<count; i++) {
      ((Channelable) list.get(i)).calcDistance(latLonZ);
    }
  }

  /**
   * Create a new phase from ARCOUT and save to a list
   */
  private int setPhaseFromARC(Solution currentSol, String data, PhaseList currentPhaseList, PhaseList existingPhaseList, int total) {
    Phase phNew = HypoFormat.parseArcToPhase(data);
    // find matching phase in currentPhaseList & transfer 'result' part of newly
    // parsed phase (ie the 'assoc' object) into original input phase.
    if (phNew != null) {
      Phase matchingPhase = getMatchingPhase(phNew, existingPhaseList);

      if (matchingPhase != null) {
        currentPhaseList.addOrReplace(matchingPhase);
      } else {
        phNew.assign(currentSol); // associate with current solution
        currentPhaseList.addOrReplace(phNew);
      }
      total++;
    } else {
      System.err.println("HypoFormat phase line parse failure");
    }
    return total;
  }

  /**
   * Based on copyDependentData function
   * Find the original phase that returned in the Location output and copy all
   * the Solution dependent values to the original phase.
   * */
  private Phase getMatchingPhase(Phase newPhase, PhaseList phaseList) {
    //Note: phaselist method below does not check for a sol association match:
    Phase oldPhase = (Phase) phaseList.getLikeWithSameTime(newPhase);
    if (oldPhase == null)  {
      return null;
    } else {
      // copy stuff that depends on the solution (dist, rms, etc.)
      oldPhase.copySolutionDependentData(newPhase);
//      if (! oldPhase.getChannelObj().hasLatLonZ() ) {
//        System.out.println(" WARNING! LocationEngine phase missing LatLonZ for dist,az data: "+ oldPhase.toString());
//      }
      
      // Copy Phase description from the new phase data
      oldPhase.description.fm = newPhase.description.fm;
      oldPhase.description.ei = newPhase.description.ei;
      oldPhase.description.iphase = newPhase.description.iphase;
      oldPhase.description.setQuality(newPhase.getQuality());
      
      return oldPhase;
    }
  }

  private Solution createNewSolution(ArcSummary arcSummary, String arcHeader) {
    Solution newSol = Solution.create();
    long newId = newSol.setUniqueId();      // need unique ID from dbase

    System.out.println("INFO: New event id created is " + newId);

    arcSummary.evid = newId;
    
    printARCSummary(arcSummary);

    newSol.eventType.setValue(EventTypeMap3.toJasiType(EventTypeMap3.EARTHQUAKE)); //to not bump version -aww 2008/04/15

    newSol = setARCSummaryDetail(newSol, arcHeader);

    newSol.gtype.setValue(getGType(newSol, null));

    return newSol;
  }
  
  private void printARCSummary(ArcSummary arcSummary) {
    System.out.println(arcSummary.getFormattedErrorEllipseString());
    System.out.println(ArcSummary.getTitle());
    System.out.println(arcSummary.getFormattedOriginString());
  }
  
  /**
   * Code from LocationEngineHypoInverse.readLocationResults to setup a solution from the summary line in ARCOUT
   */
  private Solution createFromExistingSolution(Solution currentSol, ArcSummary arcSummary, String arcHeader) {
    // id not formatted correctly patch for arc summary parse
    if (!currentSol.getId().isNull() && currentSol.getId().longValue() > 0 ) {
      arcSummary.evid = currentSol.getId().longValue();
    }
    
    printARCSummary(arcSummary);

    // DataString savedEventType = (DataString) currentSol.eventType.clone(); // aww change
    DataString savedGType = (DataString) currentSol.gtype.clone();
    currentSol.clearLocationAttributes(); // reset all solution dependent fields
    currentSol.eventType.setValue(EventTypeMap3.toJasiType(EventTypeMap3.EARTHQUAKE)); // Set to EQ per Ellen
            
    currentSol = setARCSummaryDetail(currentSol, arcHeader);

    currentSol.gtype.setValue(getGType(currentSol, savedGType));

    if (currentSol != null) {
      // load phase list
      currentSol.loadPhaseList();
      return currentSol;
    } else return null;
  }
  
  /** Determine the GType. If localNetCode is set, check if the event is inside_border **/
  private String getGType(Solution solution, DataString savedGType) {
    if (this.authString != EnvironmentInfo.DEFAULT_NETCODE) { // localNetCode is set.
      // Per Ellen - if localNetCode is set, get gtype based on if inside the border. 
      boolean isInside = solution.isInsideRegion(this.authString);
      if (isInside) {
        return GTypeMap.LOCAL;
      } else {
        return GTypeMap.REGIONAL;
      }
    } else {
      // Set gtype to local if the previous record's gtype is null
      if (savedGType == null || savedGType.isBlankOrNull()) {
        return GTypeMap.LOCAL;
      } else {
        return savedGType.toString(); // restore the origin gtype
      }      
    }
  }

  /**
   * Create database connection
   */
  private Connection getDbConnection(JiggleProperties jp){
    // get current data source description from properties
    DbaseConnectionDescription dbDesc = jp.getDbaseDescription();
    new DataSource().set(dbDesc); // added 2008/05/14 -aww
    return DataSource.getConnection();
  }

  /**
   * Create database connection
   */
  private Connection getDbConnection(PostgreSQLContainer postgreSQLContainer) {
    // get current data source description from properties
    DbaseConnectionDescription dbDesc = jp.getDbaseDescription();
    new DataSource().set(dbDesc); // added 2008/05/14 -aww
    return DataSource.getConnection();
  }
    
  /**
   * Read Jiggle properties file
   */
  private JiggleProperties getProps(String propFileName) {
    JiggleProperties jp = new JiggleProperties();
    jp.setFilename(propFileName);
    jp.initialize("jiggle", propFileName, null);

    if (! jp.readPropertiesFile(propFileName)) {
      String str = "Error: unable to load command line properties file: " + propFileName;
      System.err.println(str);
      System.exit(1);
    } else {
      // success, so reconfigure properties object after read
      System.out.println("INFO: Loading properties from: " + propFileName);
      jp.setup();
    }
    setProperties(jp);
    return jp;
  }

  /**
   * @param postgreSQLContainer
   * @return
   */
  private JiggleProperties convertPostgreContainerToJiggleProps(PostgreSQLContainer postgreSQLContainer) {
    JiggleProperties jp = new JiggleProperties();
    String propFileName = "";
    if (System.getProperty("os.name").contains("Windows")) {
      propFileName = FileSystems.getDefault().getPath(new String("./"))
              .toAbsolutePath().getParent().toString() + "\\testprop\\jiggle.props";
    } else {
      // For gitlab pipeline
      propFileName = FileSystems.getDefault().getPath(new String("./"))
              .toAbsolutePath().getParent().toString() + "/../testprop/jiggle.props";
    }

    jp.initialize("jiggle", propFileName, null);

    if (!jp.readPropertiesFile(propFileName)) {
      String str = "Error: unable to load command line properties file: " + propFileName;
      System.err.println(str);
      System.exit(1);
    } else {
      // success, so reconfigure properties object after read
      System.out.println("INFO: Loading properties from: " + propFileName);
      jp.setup();
    }
    setProperties(jp);

    jp.setProperty("useLoginDialog", "false");
    jp.setProperty("dbaseHost", postgreSQLContainer.getHost());
    jp.setProperty("dbaseDomain", DatabaseTests.getDomain());
    jp.setProperty("dbasePort", DatabaseTests.getPortNumber());
    jp.setProperty("dbaseDriver", postgreSQLContainer.getDriverClassName());
    jp.setProperty("dbaseUser", DatabaseTests.getDbUsername());
    jp.setProperty("dbaseName", DatabaseTests.getDbName());
    jp.setProperty("dbasePasswd", DatabaseTests.getDbPassword());
    return jp;
  }  
}
