package org.trinet.util.gazetteer.EW;
import java.sql.Connection;  // DK CLEANUP
import java.io.*;
import org.trinet.util.gazetteer.*;
import java.util.Vector;
import org.trinet.util.Format;
import java.util.Collection;

public class WhereIsEW extends WhereIsEngine
{

  private class WhereObject
  {
    double   dDist;
    LatLonZ Location;
    String sName;
    int    iPlaceMajorType;
    int iPlaceMinorType;
    String sState;
    String sDescription;
    DistanceAzimuthElevation dae;

    public String toString(GeoidalUnits Units)
    {
      StringBuffer sb = new StringBuffer(512);
      sb.append(new Format("%-15s").form(this.sDescription));
      sb.append(new Format("%-40s").form(this.sName+","+this.sState));
      sb.append(new Format("%5.1f").form(this.dae.getDistance(Units)));
      sb.append(Units.getLabel()).append("  ");
      sb.append(GeoidalConvert.directionString(this.dae.getAzimuth()));
      sb.append("\n");
      return sb.toString();
    }

  }

  GeoidalUnits Units;
  LatLonZ      ReferencePoint;
  EWWhereIs    ewWhereIs;
  String       sWhereString;
  Vector       vWhereObjects= new Vector();
  protected    int iPlaceMajorType;
  protected    int iPlaceMinorType;


    public static final int IPLACEMAJORTYPE_CITY       = 1;
    public static final int IPLACEMAJORTYPE_FAULT      = 4;
    public static final int IPLACEMAJORTYPE_STRUCTURE  = 5;
    public static final int IPLACEMAJORTYPE_NOISE      = 6;

    public static final int IPLACEMINORTYPE_GENERIC = 0;

    public static final int IPLACEMINORTYPE_MEGOPOLIS = 1;
    public static final int IPLACEMINORTYPE_METROPOLIS = 2;
    public static final int IPLACEMINORTYPE_LARGECITY = 3;
    public static final int IPLACEMINORTYPE_CITY = 4;
    public static final int IPLACEMINORTYPE_CDP = 5;

    public static final int IPLACEMINORTYPE_LARGEFAULT = 1;
    public static final int IPLACEMINORTYPE_SMALLFAULT = 2;

    public static final int IPLACEMINORTYPE_BRIDGE = 1;
    public static final int IPLACEMINORTYPE_DAM = 2;
    public static final int IPLACEMINORTYPE_TUNNEL = 3;

    public static final int IPLACEMINORTYPE_AIRPORT = 1;
    public static final int IPLACEMINORTYPE_MILITARY_BASE = 2;
    public static final int IPLACEMINORTYPE_NUKE = 3;
    public static final int IPLACEMINORTYPE_QUARRY = 4;
    public static final int IPLACEMINORTYPE_MINE = 5;
    public static final int IPLACEMINORTYPE_OILFIELD = 6;

   public WhereIsEW()
   {
      Units = GeoidalUnits.KILOMETERS;
   }


/** GeoidalUnits.MILES == distances are reported in miles in the where(...) method output text strings.
* GeoidalUnits.KILOMETERS (default) == distances reported in kilometers in the output text strings. */
    public void setDistanceUnits(GeoidalUnits units)
    {
      this.Units = units;
    }


/** Returns String describing where the specified WhereIs is relative to closest database entry for all known database types.
* Uses z to determine elevation.
*/
    public String where(double lat, double lon, double z)
    {
      this.setReference(lat,lon,z);
      return(where());
    }

    public String where()
    {
      toEWWhereIs();
      Vector vEWWhereIsList = ewWhereIs.ReadList();
      if(vEWWhereIsList == null)
      {
        return "";
      }
      else
      {
        this.EWWhereIs_2_WhereIsEW(vEWWhereIsList);
        this.toString();
        return(this.sWhereString);
      }
    }

    public Collection whereSummary()
    {
      toEWWhereIs();
      Vector vEWWhereIsList = ewWhereIs.ReadList();
      if(vEWWhereIsList == null)
      {
        return(null);
      }
      else
      {
        this.EWWhereIs_2_WhereIsEW(vEWWhereIsList);
        return(this.toWhereSummaryItemList());
      }
    }

    public String toString()
    {
      if(vWhereObjects.size() == 0) return "";

      StringBuffer sb = new StringBuffer(512);
      sb.append("Closest Places:\n");

      WhereObject[] woArray = (WhereObject[])vWhereObjects.toArray(new WhereObject[vWhereObjects.size()]);
      for(int i=0; i < woArray.length; i++)
      {
        sb.append(new Format("%-15s").form(woArray[i].sDescription));
        sb.append(new Format("%-40s").form(woArray[i].sName+","+woArray[i].sState));
        sb.append(new Format("%5.1f").form(woArray[i].dae.getDistance(Units))+Units.getLabel());
        sb.append("  ").append(GeoidalConvert.directionString( woArray[i].dae.getAzimuth() ));
        sb.append("\n");
      }
      return sb.toString();
    }

    public String toString(String type)
    {
      if(this.vWhereObjects.size() == 0) return "";

      StringBuffer sb = new StringBuffer(512);
      sb.append("Closest ").append(type).append(":\n");

      ConvertStringToTypes(type);

      WhereObject[] woArray = (WhereObject[])vWhereObjects.toArray(new WhereObject[vWhereObjects.size()]);
      for(int i=0; i < woArray.length; i++)
      {
        if(woArray[i].iPlaceMajorType == this.iPlaceMajorType &&
           woArray[i].iPlaceMinorType == this.iPlaceMinorType)
        {
          sb.append(new Format("%-15s").form(woArray[i].sDescription));
          sb.append(new Format("%-40s").form(woArray[i].sName+","+woArray[i].sState));
          sb.append(new Format("%5.1f").form(woArray[i].dae.getDistance(Units))+Units.getLabel());
          sb.append("  ").append(GeoidalConvert.directionString( woArray[i].dae.getAzimuth() ));
          sb.append("\n");
        }
      }
      return sb.toString();
    }



    protected void ConvertStringToTypes(String type)
    {
      if(type.equalsIgnoreCase("Quarry"))
      {
        this.iPlaceMajorType = IPLACEMAJORTYPE_NOISE;
        this.iPlaceMinorType = IPLACEMINORTYPE_QUARRY;
      }
    }

    protected EWWhereIs toEWWhereIs()
    {
      ewWhereIs = new EWWhereIs();
      ewWhereIs.dLat = ReferencePoint.getLat();
      ewWhereIs.dLon = ReferencePoint.getLon();
      ewWhereIs.dDepth = ReferencePoint.getZ();
      return(ewWhereIs);
    }

    protected WhereIsEW EWWhereIs_2_WhereIsEW(Vector vEWWhereIsList)
    {
      EWWhereIs[] ewWIArray = (EWWhereIs[])vEWWhereIsList.toArray(new EWWhereIs[vEWWhereIsList.size()]);

      this.vWhereObjects.removeAllElements();

      for(int i=0; i < ewWIArray.length; i++)
      {
        WhereObject wo = new WhereObject();
        wo.Location = new LatLonZ(ewWIArray[i].dLat, ewWIArray[i].dLon, 0.0/*depth*/);
        wo.iPlaceMajorType = ewWIArray[i].iPlaceMajorType;
        wo.iPlaceMinorType = ewWIArray[i].iPlaceMinorType;
        wo.sDescription    = ewWIArray[i].sDescription;
        wo.sName           = ewWIArray[i].sName;
        wo.sState          = ewWIArray[i].sState;
        wo.dae = DistanceAzimuthElevation.create(GeoidalUnits.DEGREES);
        wo.dae.setDistanceAzimuthElevation(this.ReferencePoint, wo.Location);
        this.vWhereObjects.add(wo);
      }
      return(this);
    }
/** Returns String describing where the specified WhereIs is relative to the closest database entry
* for the specified input database type. Uses z to determine elevation.
*/
    public String whereType(double lat, double lon, double z, String type)
    {
      this.setReference(lat,lon,z);
      return(whereType(type));
    }

    public String whereType(String type)
    {
      toEWWhereIs();
      Vector vEWWhereIsList = ewWhereIs.ReadList();
      if(vEWWhereIsList == null)
      {
        return "";
      }
      else
      {
        this.EWWhereIs_2_WhereIsEW(vEWWhereIsList);
        this.toString(type);
        return(this.sWhereString);
      }
    }

    public WhereSummary whereSummaryType(String type)
    {
      toEWWhereIs();
      Vector vEWWhereIsList = ewWhereIs.ReadList();
      if(vEWWhereIsList == null)
      {
        return(null);
      }
      else
      {
        this.EWWhereIs_2_WhereIsEW(vEWWhereIsList);
        return(this.toWhereSummary(type));
      }
    }

/** Sets this object's geographic reference source point.
*   Distance, azimuth and elevation are calculated from the Gazetteer database points to this reference point.
*/

    public double kmToFault(double lat, double lon, String name) {
        return Double.MAX_VALUE;
    }

    public void setReference(double lat, double lon, double z)
    {
      ReferencePoint = new LatLonZ(lat, lon, z);
    }


    public Collection toWhereSummaryItemList()
    {
      if(this.vWhereObjects.size() == 0)
        return(new Vector(0));


      WhereSummaryItem[] wsiArray = new WhereSummaryItem[vWhereObjects.size()];
      WhereObject[] woArray = (WhereObject[])vWhereObjects.toArray(new WhereObject[vWhereObjects.size()]);
      for(int i=0; i < woArray.length; i++)
      {
        WhereObject2WhereSummaryItem(woArray[i],wsiArray[i]);
      }
      Vector vWSIArray = new Vector();
      vWSIArray.copyInto(wsiArray);
      return(vWSIArray);
    }

    protected void WhereObject2WhereSummaryItem(WhereObject wo, WhereSummaryItem wsi)
    {
      wsi.SetParams(wo.dae, wo.toString(this.Units), "");
    }



    public WhereSummary toWhereSummary(String type)
    {
      if(this.vWhereObjects.size() == 0)
        return(null);

      ConvertStringToTypes(type);

      WhereObject[] woArray = (WhereObject[])vWhereObjects.toArray(new WhereObject[vWhereObjects.size()]);
      for(int i=0; i < woArray.length; i++)
      {
        if(woArray[i].iPlaceMajorType == this.iPlaceMajorType &&
           woArray[i].iPlaceMinorType == this.iPlaceMinorType)
        {
          WhereSummaryItem wsItem = new WhereSummaryItem();
          WhereObject2WhereSummaryItem(woArray[i],wsItem);
          return((WhereSummary)wsItem);
        }
      }
      return(null);
    }

    public void setConnection(java.sql.Connection conn) {
        ewWhereIs.setConnection(conn);
    }

} // End of WhereIsEW class
