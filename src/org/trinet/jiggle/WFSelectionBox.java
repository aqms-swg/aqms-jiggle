package org.trinet.jiggle;

import java.util.*;
import javax.swing.event.*;
import org.trinet.util.*;

/**
 * WaveForm Selection box. Defines the limits of a selected waveform. Includes
 * a TimeSpan(start and end time) and amplitude limits (max and min) in whatever
 * the waveform units are (counts, cm/sec, etc.).
 */
/*
 Note that we always create a new instance of time span, else its just a
 reference to the original TimeSpan object that can be changed underneath us.
*/
class WFSelectionBox {

    /** The TimeSpan of the selected box */
    TimeSpan timeSpan = new TimeSpan();
    /** Maximum amplitude of the selected box */
    double maxAmp = 0.0;
    /** Minimum amplitude of the selected box */
    double minAmp = 0.0;

    /** Simple constructor */
    public WFSelectionBox()
    {
    }

    /** Make a box with these dimensions */
    public WFSelectionBox(TimeSpan ts, double ampMax, double ampMin )
    {
        set(ts, ampMax, ampMin);
    }
    /** Make a box with these dimensions */
    public WFSelectionBox(double startTime, double endTime,
                          double ampMax, double ampMin )
    {
        set(startTime, endTime, ampMax, ampMin);
    }

    /** Make a box based on the dimesions of this box */
    public WFSelectionBox( WFSelectionBox sb )
    {
        set(sb);
    }
    /** Set the selection box dimensions */
    public void set(double startTime, double endTime,
                     double ampMax, double ampMin)
    {
        setTimeSpan(startTime, endTime);
        setAmpSpan(ampMax, ampMin);
    }
    /** Set the selection box dimensions */
    public void set(TimeSpan ts, double ampMax, double ampMin)
    {
        setTimeSpan(ts);
        setAmpSpan(ampMax, ampMin);
    }
    /** Set the selection box dimensions. Set flag used to identify source of
        resize when handling depends on the source. */
/*    public void set(TimeSpan ts, double ampMax, double ampMin, int flag )
    {
        set(ts, ampMax, ampMin);
        setFlag(flag);
    }
*/
    /** Set the selection box dimensions based on the dimesions of this box */
    public void set(WFSelectionBox sb)
    {
    // NOTE: access values via the methods NOT directly.
    // This way, the WFDataBox class and others (which extends this class)
    // can change the values dynamically as the data changes or is updated.
        set(sb.getTimeSpan(), sb.getMaxAmp(), sb.getMinAmp());
    }

    /** Set the selection box dimensions. Set flag used to identify source of
        resize when handling depends on the source. */
/*
    public void set(WFSelectionBox sb, int flag)
    {
        set(sb);
        setFlag(flag);
    }
*/
    /** Set flag used to identify source of resize when handling depends on the
        source. */
/*    public void setFlag(int flag)
    {
        fromFlag = flag;
    }
*/
    /** Set the time dimension only. */
    public void setTimeSpan(TimeSpan ts)
    {
        //timeSpan = new TimeSpan(ts);
        setTimeSpan(ts.getStart(), ts.getEnd());
    }

    /** Set the time dimension only. */
    public void setTimeSpan(double t1, double t2)
    {
        timeSpan.setStart(t1);
        timeSpan.setEnd(t2);
    }

    /**
     * Center the selection box on this time, keep the size of the box the same.
     */
    public void setCenterTime(double centerTime)
    {
        timeSpan.setCenter(centerTime);
    }
    /**
     * Center the selection box on this amplitude, keep the size of the box the same.
     */
    public void setCenterAmp(double amp)
    {
        double delta = amp - getCenterAmp();

        // translate
        maxAmp += delta;
        minAmp += delta;
    }

    /**
     * Move the selection box start time to this time, keep the size of the box the
     * same.  */
    public void moveStartTime(double time)
    {
        getTimeSpan().moveStart(time);
    }
    /** Set the selection box to null. */
    public void setNull()
    {
        getTimeSpan().setNull();
        maxAmp = 0.0;
        minAmp = 0.0;
    }

    /** Set the amplitude dimension only. */
    public void setAmpSpan( double max, double min )
    {
        maxAmp = max;
        minAmp = min;
//       if (maxAmp == minAmp) maxAmp = maxAmp+1.0;
    }

    /** Set the amplitude dimension only. */
    public void setAmpSpan(WFSelectionBox sb) {
        setAmpSpan(sb.getMaxAmp(), sb.getMinAmp());
    }

    /** Returns true if two selection boxes have the same center time and the
    same time and amplitude dimensions. */
    public boolean equals(WFSelectionBox newBox)
    {
        if ( newBox.getStartTime() == getStartTime() &&
          newBox.getEndTime()   == getEndTime() &&
          newBox.getMaxAmp()    == getMaxAmp() &&
          newBox.getMinAmp()    == getMinAmp()) return true;
        return false;
    }

    /**
     * Returns true if time and amp of the two boxes are within the resolution of
     * timing.  This kludge was introduced to try to eliminate scrollpanel "crawl".  */
 /*   public boolean nearlyEquals(WFSelectionBox newBox)
    {
        if ( newBox.getTimeSpan().nearlyEquals(getTimeSpan()) &&
             Math.abs(newBox.getCenterTime() - getCenterTime()) > 0.01 &&
             newBox.getMaxAmp() == getMaxAmp() &&
             newBox.getMinAmp() == getMinAmp()) return true;

        return false;
    }
*/
    /**
     * Returns true if the selection box is null (undefined). Only time can really
     * be null, any value of amp, including 0 is valid */
    public boolean isNull()
    {
        if (getTimeSpan().isNull() &&
            getMaxAmp() == 0 &&
            getMinAmp() == 0 ) return true;
        return false;
    }

    /**
     * Return the length of the box in seconds
     */
    public double getTimeSize()
    {
        return (getTimeSpan().getDuration());
    }

    /**
     * Return the box time span (start-end)
     */
    public TimeSpan getTimeSpan()
    {
        return (timeSpan);
    }

    /**
     * Return the box time span in seconds
     */
    public double getTimeSpanSeconds()
    {
        return (timeSpan.getDuration());
    }

    /**
     * Return the box start time
     */
    public double getStartTime()
    {
        return (getTimeSpan().getStart());
    }

    /**
     * Return the box end time
     */
    public double getEndTime()
    {
        return (getTimeSpan().getEnd());
    }

    /**
     * Return time of the center of the box
     */
    public double getCenterTime()
    {
        return getTimeSpan().getCenter();
    }

    /**
     * Return the height of the box in waveform units. This calculates the
     * difference between the min and max amplitudes. If all samples have the
     * same value this would equal zero which could cause /0 errors in other
     * methods. Therefore, one is added to the value. Thus you can think of this
     * as the HEIGHT of the data, if all data has the same value the HEIGHT is 1.
     */
    public double getAmpSize() {
        return getMaxAmp() - getMinAmp();
    }

    /**
     * Return amp of the center of the box
     */
    public double getCenterAmp() {
        return (getMaxAmp() + getMinAmp()) / 2.0;
    }

    /**
     * Return the box's maximum amplitude in sample counts
     */
    public double getMaxAmp() {
        return (maxAmp);
    }

    /**
     * Return the box's minimum amplitude in sample counts
     */
    public double getMinAmp() {
        return (minAmp);
    }

    public String toString() {
        return getTimeSpan().toString() + " / " + getMinAmp() + " -> " + getMaxAmp();
    }

    // ///////////////////////////////////////////////////////
    // Support of state change events
    EventListenerList listenerList = new EventListenerList();
    ChangeEvent changeEvent = null;

    /** Register listener with list of ChangeListeners */
    public void addChangeListener(ChangeListener l) {
        removeChangeListener(l);
        listenerList.add(ChangeListener.class, l);
    }

    /** Unregister listener from list of ChangeListeners */
    public void removeChangeListener(ChangeListener l) {
        listenerList.remove(ChangeListener.class, l);
    }

    /** Notify all listeners that have registered interest for notification on
     * this event type.  The event instance is created on each call to reflect the
     * current state. */

    protected void fireStateChanged() {
        // Guaranteed to return a non-null array, :. no NULL check needed
        Object[] listeners = listenerList.getListenerList();

        // bail if no listeners
        if (listenerList.getListenerCount() == 0) return;

        ChangeEvent changeEvent = new ChangeEvent(this);

        // Notify the listeners last to first. Note: 'listeners' is an array of
        // PAIRS of ListenerType/Listener, hence the weird indexing.
        for (int i = listeners.length-2; i>=0; i-=2) {
            if (listeners[i] == ChangeListener.class) {
                ((ChangeListener)listeners[i+1]).stateChanged(changeEvent);
            }
        }
    }

    // /////////////////////////////////////////////////////////
}
