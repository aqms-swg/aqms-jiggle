package org.trinet.jasi;

import org.trinet.util.*;
import java.util.*;


/** A trigger time window object. Extends ChannelTimeWindow to include a trigger
* time and a trigger type string.
* @see: ChannelTimeWindow */
public class TriggerChannelTimeWindow extends ChannelTimeWindow {

    public double triggerTime = 0.0;
    public String triggerType = null;
 
    public TriggerChannelTimeWindow() {
    }
    
    public TriggerChannelTimeWindow(Channel chan) {
        super(chan);
    }

    /**
     * Construct an empty view window with defined viewSpan time bounds.
     */
    public TriggerChannelTimeWindow(Channel ch, double start, double end) {
        super(ch, start, end);
    }
    /**
    * Construct an empty view window with defined viewSpan time bounds.
    */
    public TriggerChannelTimeWindow(Channel ch, TimeSpan ts) {
        super(ch, ts);
    }
    public double getTriggerTime() {
        return triggerTime;
    }
    public void setTriggerTime(double time) {
        triggerTime = time;
    } 
    public String getTriggerType() {
        return triggerType;
    }
    public void setTriggerType(String type) {
        triggerType = type;
    }
    /**
     * Dump some info about the view for debugging
     */
    public String toString() {
        String str = "Window: "+ chan.toString() + " viewSpan = " + ts.toString();
    
        if (triggerTime != 0.0)
            str += " Trigger Time= "+ LeapSeconds.trueToString(triggerTime); // for UTC seconds - aww 2008/02/12
    
        if (triggerType != null)
            str += " Type= "+triggerType;

        return str;
    }
    
    /** Return the TriggerChannelTimeWindow with the earliest trigger time from a Collection.
    * If there are no trigger times, the first TriggerChannelTimeWindow in the list
    * is returned. If there is no list, a virgin TriggerChannelTimeWindow is returned. */
    public static TriggerChannelTimeWindow getEarliestTrigger(Collection tctwList) {
    
        TriggerChannelTimeWindow ctw = new TriggerChannelTimeWindow();
    
        TriggerChannelTimeWindow array[] =
                  (TriggerChannelTimeWindow[]) tctwList.toArray(new TriggerChannelTimeWindow[0]);
    
        if (array.length == 0) return ctw;
    
        double earliest = Double.MAX_VALUE;
    
        for (int i = 0; i < array.length; i++) {
            if (array[i].triggerTime != 0.0 &&
                    array[i].triggerTime < earliest) {
                ctw = array[i];
                earliest = ctw.triggerTime;
            }
        }
    
        if (earliest == Double.MAX_VALUE) ctw = array[0];
        return ctw;
    }
}
