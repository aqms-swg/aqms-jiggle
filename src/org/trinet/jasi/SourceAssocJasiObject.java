package org.trinet.jasi;
import org.trinet.jdbc.datatypes.*;
// Changed hierarchy switched inheritance positions with CommitableJasiObject - aww 10/03
// since data is not necessarily commitable, deletable, but usually has a source/authority attribute
//public abstract class SourceAssocJasiObject extends JasiObject implements JasiSourceAssociationIF, JasiProcessingConstants {
public abstract class SourceAssocJasiObject extends JasiObject implements JasiSourceAssociationIF {

    // Holder for the DataSource table row LDDATE or instance create/modify time
    protected DataDate timeStamp = new DataDate(); // added date stamp 02/14/2005 -aww

    /** Data authority. The 2-character FDSN network code from which this
        data came. Default to network code set in EnvironmentInfo.
        @See: EnvironmentInfo.getNetworkCode */
    public DataString  authority  = new DataString(EnvironmentInfo.getNetworkCode());

    /** State of processing. 'A'=automatic, 'H'=human, 'F'=final */
    public DataString  processingState = new DataString(EnvironmentInfo.getAutoString()); // rflag set to current enviroment setting

    /** Data source string. Optional site defined source string.
        Default to application name set in EnvironmentInfo.
        @See: EnvironmentInfo.getApplicationName */
    public DataString  source   = new DataString(EnvironmentInfo.getApplicationName());

    /** Default constructor. */ 
    protected SourceAssocJasiObject() { }

    public boolean isAuto()  {
        return JasiProcessingConstants.STATE_AUTO_TAG.equals(processingState.toString());
    }
    public boolean isHuman() {
        return JasiProcessingConstants.STATE_HUMAN_TAG.equals(processingState.toString());
    }
    public boolean isIntermediate() {
        return JasiProcessingConstants.STATE_INTERMEDIATE_TAG.equals(processingState.toString());
    }
    public boolean isFinal() {
        return JasiProcessingConstants.STATE_FINAL_TAG.equals(processingState.toString());
    }
    public boolean hasUnknownState() {
        return JasiProcessingConstants.STATE_UNKNOWN_TAG.equals(processingState.toString());
    }

    public String getProcessingStateString() {
        return processingState.toString();
    }
    public DataString getProcessingState() { return processingState; }

    public String getSource() { return source.toString(); }
    // Check how classes invoking "getter" methods expect a String returned - aww 9/2003
    // public DataString getSource() { return source; }
    //public String getSourceString() { return source.toString(); }

    public String getAuthority() { return authority.toString(); }
    //public DataString getAuthority() { return authority; }


    public void setProcessingState(String aState) {
        processingState.setValue(aState);
    }
    public void setAuthority(String aNetwork) {
        authority.setValue(aNetwork);
    }
    public void setSource(String aName) {
        source.setValue(aName);
    }

    public void setAuthority(DataString aNetwork) {
      authority.setValue(aNetwork);
    }
    public void setSource(DataString aName ) {
      source.setValue(aName);
    }
    public void setProcessingState(DataString aState) {
      processingState.setValue(aState);
    }
    public DataDate getTimeStamp() {
        return timeStamp;
    }
    public void setTimeStamp(java.util.Date date) {
        timeStamp.setValue(date);
    }
    public void setTimeStamp(DataDate date) {
        timeStamp = date;
    }
    public String toString() {
        StringBuffer sb = new StringBuffer(48);
        sb.append(authority.toString()).append("  ");
        sb.append(source.toString()).append("  ");
        sb.append(processingState.toString()).append(" ");
        sb.append(timeStamp.toStringSQL());
        return sb.toString();
    }

    public Object clone() {
        SourceAssocJasiObject sjo = (SourceAssocJasiObject) super.clone();
        sjo.authority             = (DataString) this.authority.clone();
        sjo.processingState       = (DataString) this.processingState.clone();
        sjo.source                = (DataString) this.source.clone();
        sjo.timeStamp             = (DataDate) this.timeStamp.clone();
        return sjo;
    }
}
