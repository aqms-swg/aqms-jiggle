package org.trinet.apps;

import org.trinet.formats.CubeFormat;

public class CheckSum {

    public static String checkSum;

    /**
     * @param args
     */
    public static void main(String[] args) {

        if (args.length < 1) {    // no args
            System.out.println
                    ("Usage: CheckSum <string> (put in quotes if it contains blanks)");
            System.exit(0);
        }

        checkSum = args[0] + CubeFormat.menloCheckChar(args[0]);
        System.out.println(args[0] + CubeFormat.menloCheckChar(args[0]));
    }

    public static String getCheckSum() {
        return checkSum;
    }
}