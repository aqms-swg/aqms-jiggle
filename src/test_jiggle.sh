#!/bin/bash

# build_jiggle.sh
# This script builds the jiggle.jar executable from compiled .class files and .jar external libraries.
# This script lives in the bin/ directory
# Assumes a "manifest.txt" file lives in the bin/ directory

if [ "$#" -ne 1 ]
then
   echo "Usage: ./build_jiggle.sh <java_version>"
   echo "Possible values of <java_version>: 8, 11"
   exit 1
fi

# Java version from command line
JAVA_VER=$1
echo "JAVA_VER: 1."${JAVA_VER}

case "${JAVA_VER}" in
   8 )
      echo "8"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.8`
#      export JAVA_HOME=`/usr/libexec/java_home -v 1.8.0_202`
      ;;
   11 )
      echo "Using JDK 11"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.11`
      ;;
   17 )
      echo "Using JDK:17"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.17`
      ;;
   * )
      echo "8"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.8`
#      export JAVA_HOME=`/usr/libexec/java_home -v 1.8.0_202`
      ;;
esac

# Input directory for .java source files
SRC_DIR="./"

# Input directory for .jar external libraries
LIB_DIR="../lib/"

TEST_PROP_DIR=${SRC_DIR}../testprop

# Output directory for .class files
CLASS_DIR="./out_classes_java"${JAVA_VER}
echo "CLASS_DIR: "${CLASS_DIR}

# Oracle database driver jar name
# Oracle database driver jar name
case "${JAVA_VER}" in
    17 )
      JDBC_FILE="ojdbc11.jar"
      ;;
    * )
      JDBC_FILE="ojdbc"${JAVA_VER}".jar"
      ;;
esac
echo "JDBC_FILE: "${JDBC_FILE}

# echo "Find where Java is"
# readlink -f $(which java)

if [ -d ${CLASS_DIR} ]
then
   rm -r ${CLASS_DIR}
fi
mkdir ${CLASS_DIR}

echo "DEBUG File Path?"
ls /builds/aqms-swg/aqms-jiggle/src/../testdata/wavearchive/60253301
chmod -R 777 /builds
chmod 777 /builds/aqms-swg/aqms-jiggle/src/../testdata/wavearchive/60253301/BK.THIS.BHE.00

# Compile Jiggle
CLASS_PATH="${SRC_DIR}*:${LIB_DIR}postgresql-42.2.5.jar:${LIB_DIR}${JDBC_FILE}:${LIB_DIR}acme.jar:${LIB_DIR}openmap.jar:${LIB_DIR}isti.openmap.all.jar:${LIB_DIR}seed-pdcc.jar:${LIB_DIR}looks-2.0.4.jar:${LIB_DIR}forms-1.0.7.jar:${LIB_DIR}colt.jar:${LIB_DIR}swarm.jar:${LIB_DIR}usgs.jar:${LIB_DIR}jregex1.2_01.jar:${LIB_DIR}pljava-api-1.5.8.jar:${LIB_DIR}junit-jupiter-api-5.8.1.jar:${LIB_DIR}junit-jupiter-engine-5.8.1.jar:${LIB_DIR}junit-platform-commons-1.8.1.jar:${LIB_DIR}junit-platform-console-standalone-1.3.1.jar:${LIB_DIR}testcontainers-postgresql-1.19.1.jar:${LIB_DIR}testcontainers-jdbc-1.19.1.jar:${LIB_DIR}testcontainers-database-commons-1.19.1.jar:${LIB_DIR}testcontainers-1.19.1.jar:${LIB_DIR}docker-java-api-3.3.3.jar:${LIB_DIR}docker-java-transport-3.3.3.jar:${LIB_DIR}docker-java-transport-zerodep-3.3.3.jar:${LIB_DIR}duct-tape-1.0.8.jar:${LIB_DIR}slf4j-api-1.7.32.jar:${LIB_DIR}jackson-annotations-2.10.3.jar:${LIB_DIR}jna-5.13.0.jar:${LIB_DIR}commons-compress-1.27.1.jar:${LIB_DIR}commons-io-2.17.0.jar:${LIB_DIR}commons-lang3-3.17.0.jar"
javac -version
javac -d ${CLASS_DIR} $(find ${SRC_DIR} -name "*.java") -cp ${CLASS_PATH} -encoding ISO8859-1

java -DCALCML_HOME=${TEST_PROP_DIR} -DJIGGLE_HOME=${TEST_PROP_DIR} -jar "${LIB_DIR}junit-platform-console-standalone-1.3.1.jar" --class-path="${CLASS_DIR}" --class-path="${CLASS_PATH}" --include-classname=.* --scan-classpath="${LIB_DIR}" --scan-classpath="${CLASS_DIR}" --include-engine=junit-jupiter --exclude-engine=junit-vintage --disable-ansi-colors  --details-theme=ascii --fail-if-no-tests --reports-dir="${SRC_DIR}/test/reports" --details=verbose

# Other parameters we could add
# --details=verbose
# --class-path="${CLASS_DIR}"