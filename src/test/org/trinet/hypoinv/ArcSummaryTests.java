package test.org.trinet.hypoinv;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.Test;
import org.trinet.hypoinv.ArcSummary;

public class ArcSummaryTests {

    @Test
    public void verify_parse_arc_summary_returns_true_with_valid_header() {
        String test = "202305251810130134 2086118W2761  517  0 43101  5  1925571  57133 9  24  0     19   21  23  55 15  00  00  0  003E      47    0 00   0 00  39805216   0  00   0  00  CI02MH 414  476";
        ArcSummary arcSummary = new ArcSummary();
        assertTrue(arcSummary.parseArcSummary(test));
    }

    @Test
    public void verify_parse_arc_summary_returns_false_with_short_header() {
        StringBuilder test = new StringBuilder();
        for (int i = 0; i < ArcSummary.MAX_ARC_SUMMARY_CHARS-1; i++) {
            test.append("1");
        }
        ArcSummary arcSummary = new ArcSummary();
        assertFalse(arcSummary.parseArcSummary(test.toString()));
    }
}
