package org.trinet.jiggle;

import org.trinet.jasi.Amplitude;

import java.awt.*;

/**
* Triangular graphical marker to show where an amplitude reading is.<p>
*
* Empty triangle means unassociate. Filled means associated.
*
* Colors are the same as for picks and are coded to the associated solution. <br>
*/

public class AmpFlag implements MarkerIF{


    // Default size of symbol in pixels
    final static int minWidth = 8;
    final static int minHeight= 8;

    int width  = minWidth;
    int height = minHeight;

    /** Amplitude represented by this flag. */
    Amplitude amp = null;

    /** WFPanel where flag will be drawn. */
    WFPanel wfp = null;

    /** Current color of the flag. */
    Color color = null;

    Point pos = new Point();

    /**
     * Create an amplitude flag representing this Amplitude
     * to be displayed in this WFPanel.
     */
     public AmpFlag(WFPanel wfp, Amplitude amp) {
        this.wfp = wfp;
        this.amp = amp;

     }
       /** Set the marker color. */
       public void setColor(Color color) {
          this.color = color;
       }

       /** Return the marker color. */
       public Color getColor() {
          return color;
       }

       /** Set the marker size. */
       public void setSize(Dimension dim) {
          width  = (int) dim.getWidth();
          height = (int) dim.getHeight();
       }
       /** Return the marker size. */
       public Dimension getSize() {
          return new Dimension (width, height);
       }
/*
 * Draw the AmpFlag in the Graphics window. Check the Viewport limits to insure
 * that the flag is always in view in a zoomable WFPanel. If isDeleted()
 * == true the flag will NOT be drawn. */
// TODO: handle case where flags overlap

    public void draw(Graphics g) {

        // don't draw a delete phase (later may want to add ability to show these)
        if (amp.isDeleted()) return;

        // If the wfPanel is zoomable keep flag within viewport so we can see it
        Rectangle vrec = wfp.getVisibleRect();
        pos.y = vrec.y + vrec.height;
        // MUST RECALC. THIS EVERY TIME BECAUSE FRAME MIGHT HAVE RESCALED!
        pos.x = wfp.pixelOfTime(amp.datetime.doubleValue()) ;

        Point pos2 = null;
        if (!amp.halfAmp && !Double.isNaN(amp.datetime2)) {
            pos2 = new Point(wfp.pixelOfTime(amp.datetime2), pos.y);
        }

        // lookup origin's color code

        color = wfp.wfv.mv.solList.getColorOf(amp.getAssociatedSolution());
        Color oldColor = g.getColor();
        g.setColor(color);

        int wid = width/2;

        // this makes a point-down triangle at the bottom of the WFPanel when amp ProcessingState.FINAL (REQUIRED)
        Polygon triangleF = null;
        if (amp.isFinal()) { 
          triangleF = new Polygon();
          triangleF.addPoint(pos.x, pos.y);
          pos.y -= height; // shift up
          triangleF.addPoint(pos.x - wid, pos.y);
          triangleF.addPoint(pos.x + wid, pos.y);
        }

        // this makes a point-up triangle at the bottom of the WFPanel
        Polygon triangleB = new Polygon();
        triangleB.addPoint(pos.x, pos.y - height);
        triangleB.addPoint(pos.x - wid, pos.y);
        triangleB.addPoint(pos.x + wid, pos.y);

        Polygon triangleT = new Polygon();
        triangleT.addPoint(pos.x, vrec.y + height);
        triangleT.addPoint(pos.x - wid, vrec.y);
        triangleT.addPoint(pos.x + wid, vrec.y);

        // fill if it's "used"
        if (amp.getWeightUsed() > 0.0) {
          g.fillPolygon(triangleT);
          g.fillPolygon(triangleB);
          if (amp.isFinal()) g.fillPolygon(triangleF);
        } else {
          g.drawPolygon(triangleT);
          g.drawPolygon(triangleB);
          if (amp.isFinal()) g.drawPolygon(triangleF);
        }

        if (pos2 != null) {
            // this makes a point-up triangle at the bottom of the WFPanel
            triangleB = new Polygon();
            triangleB.addPoint(pos2.x, pos2.y - height);
            triangleB.addPoint(pos2.x - wid, pos2.y);
            triangleB.addPoint(pos2.x + wid, pos2.y);
            g.drawPolygon(triangleB); // Unfilled (not used amp)

            triangleT = new Polygon();
            triangleT.addPoint(pos2.x, vrec.y + height);
            triangleT.addPoint(pos2.x - wid, vrec.y);
            triangleT.addPoint(pos2.x + wid, vrec.y);
            g.drawPolygon(triangleT); // Unfilled (not used amp)
        }

        g.setColor(oldColor);
    }

} // end of AmpFlag
