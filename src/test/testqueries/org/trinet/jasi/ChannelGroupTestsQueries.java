package test.testqueries.org.trinet.jasi;

import test.org.trinet.jasi.ChannelGrouperTests;
import test.testqueries.common.TestQueries;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

public class ChannelGroupTestsQueries extends TestQueries {

    public ChannelGroupTestsQueries(String URL) throws SQLException {
        super(URL);
    }

    public int populateEventDataForChannelGrouperTests() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.\"event\"\n" +
                "(evid, prefor, prefmag, prefmec, commid, auth, subsource, etype, selectflag, \"version\", lddate)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, ChannelGrouperTests.ChannelGrouperTestsEventId);
        statement.setInt(index++, ChannelGrouperTests.ChannelGrouperTestsOrId);
        statement.setInt(index++, 108867285);
        statement.setInt(index++, 3426229);
        statement.setNull(index++, Types.INTEGER);
        statement.setString(index++, "CI");
        statement.setString(index++, "RT1");
        statement.setString(index++, "eq");
        statement.setInt(index++, 1);
        statement.setInt(index++, 7);
        statement.setString(index++, "2021-01-27 09:28:15.000");
        return insertQueryHelper(statement);
    }

    public int populateAmp1DataForChannelGrouperTests() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.amp\n" +
                "(ampid, commid, datetime, sta, net, auth, subsource, channel, channelsrc, seedchan, \"location\", iphase, amplitude, amptype, units, ampmeas, eramp, flagamp, per, snr, tau, quality, rflag, cflag, wstart, duration, lddate)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, ChannelGrouperTests.ChannelGrouperTestsAmp1Id);
        statement.setNull(index++, Types.INTEGER);
        statement.setDouble(index++, 1611739641.798356);
        statement.setString(index++, "CCC");
        statement.setString(index++, "CI");
        statement.setString(index++, "CI");
        statement.setString(index++, "Jiggle");
        statement.setNull(index++, Types.VARCHAR);
        statement.setNull(index++, Types.VARCHAR);
        statement.setString(index++, "HHE");
        statement.setString(index++, "  ");
        statement.setNull(index++, Types.VARCHAR);
        statement.setDouble(index++, 0.0024246496614068747);
        statement.setString(index++, "WASF");
        statement.setString(index++, "cm");
        statement.setString(index++, "1");
        statement.setNull(index++, Types.DOUBLE);
        statement.setNull(index++, Types.VARCHAR);
        statement.setDouble(index++, 0.64);
        statement.setDouble(index++, 6.15496108153123);
        statement.setNull(index++, Types.DOUBLE);
        statement.setDouble(index++, 1.0);
        statement.setString(index++, "H");
        statement.setString(index++, "OS");
        statement.setDouble(index++, 1611739636.5037153);
        statement.setDouble(index++, 16.712730884552002);
        statement.setString(index++, "2021-02-01 18:33:03.000");
        return insertQueryHelper(statement);
    }

    public int populateAmp2DataForChannelGrouperTests() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.amp\n" +
                "(ampid, commid, datetime, sta, net, auth, subsource, channel, channelsrc, seedchan, \"location\", iphase, amplitude, amptype, units, ampmeas, eramp, flagamp, per, snr, tau, quality, rflag, cflag, wstart, duration, lddate)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, ChannelGrouperTests.ChannelGrouperTestsAmp2Id);
        statement.setNull(index++, Types.INTEGER);
        statement.setDouble(index++, 1611739641.798356);
        statement.setString(index++, "CCC");
        statement.setString(index++, "CI");
        statement.setString(index++, "CI");
        statement.setString(index++, "Jiggle");
        statement.setNull(index++, Types.VARCHAR);
        statement.setNull(index++, Types.VARCHAR);
        statement.setString(index++, "HHE");
        statement.setString(index++, "  ");
        statement.setNull(index++, Types.VARCHAR);
        statement.setDouble(index++, 0.0024246496614068747);
        statement.setString(index++, "WASF");
        statement.setString(index++, "cm");
        statement.setString(index++, "1");
        statement.setNull(index++, Types.DOUBLE);
        statement.setNull(index++, Types.VARCHAR);
        statement.setDouble(index++, 0.64);
        statement.setDouble(index++, 6.15496108153123);
        statement.setNull(index++, Types.DOUBLE);
        statement.setDouble(index++, 1.0);
        statement.setString(index++, "H");
        statement.setString(index++, "OS");
        statement.setDouble(index++, 1611739636.5037153);
        statement.setDouble(index++, 16.712730884552002);
        statement.setString(index++, "2021-02-01 18:33:03.000");
        return insertQueryHelper(statement);
    }

    public int populateAmp3DataForChannelGrouperTests() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.amp\n" +
                "(ampid, commid, datetime, sta, net, auth, subsource, channel, channelsrc, seedchan, \"location\", iphase, amplitude, amptype, units, ampmeas, eramp, flagamp, per, snr, tau, quality, rflag, cflag, wstart, duration, lddate)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, ChannelGrouperTests.ChannelGrouperTestsAmp3Id);
        statement.setNull(index++, Types.INTEGER);
        statement.setDouble(index++, 1611739641.798356);
        statement.setString(index++, "CCD");
        statement.setString(index++, "CI");
        statement.setString(index++, "CI");
        statement.setString(index++, "Jiggle");
        statement.setNull(index++, Types.VARCHAR);
        statement.setNull(index++, Types.VARCHAR);
        statement.setString(index++, "HHE");
        statement.setString(index++, "  ");
        statement.setNull(index++, Types.VARCHAR);
        statement.setDouble(index++, 0.0024246496614068747);
        statement.setString(index++, "WASF");
        statement.setString(index++, "cm");
        statement.setString(index++, "1");
        statement.setNull(index++, Types.DOUBLE);
        statement.setNull(index++, Types.VARCHAR);
        statement.setDouble(index++, 0.64);
        statement.setDouble(index++, 6.15496108153123);
        statement.setNull(index++, Types.DOUBLE);
        statement.setDouble(index++, 1.0);
        statement.setString(index++, "H");
        statement.setString(index++, "OS");
        statement.setDouble(index++, 1611739636.5037153);
        statement.setDouble(index++, 16.712730884552002);
        statement.setString(index++, "2021-02-01 18:33:03.000");
        return insertQueryHelper(statement);
    }

    public int populateAssoCamoAmp1DataForChannelGrouperTests() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.assocamo\n" +
                "(orid, ampid, commid, auth, subsource, delta, seaz, rflag, lddate)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, ChannelGrouperTests.ChannelGrouperTestsOrId);
        statement.setInt(index++, ChannelGrouperTests.ChannelGrouperTestsAmp1Id);
        statement.setNull(index++, Types.INTEGER);
        statement.setString(index++, "CI");
        statement.setString(index++, "Jiggle");
        statement.setDouble(index++, 50.8928);
        statement.setDouble(index++, 184.9);
        statement.setString(index++, "H");
        statement.setString(index++, "2021-02-01 18:33:03.000");
        return insertQueryHelper(statement);
    }

    public int populateAssoCamoAmp2DataForChannelGrouperTests() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.assocamo\n" +
                "(orid, ampid, commid, auth, subsource, delta, seaz, rflag, lddate)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, ChannelGrouperTests.ChannelGrouperTestsOrId);
        statement.setInt(index++, ChannelGrouperTests.ChannelGrouperTestsAmp2Id);
        statement.setNull(index++, Types.INTEGER);
        statement.setString(index++, "CI");
        statement.setString(index++, "Jiggle");
        statement.setDouble(index++, 50.8928);
        statement.setDouble(index++, 184.9);
        statement.setString(index++, "H");
        statement.setString(index++, "2021-02-01 18:33:03.000");
        return insertQueryHelper(statement);
    }

    public int populateAssoCamoAmp3DataForChannelGrouperTests() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.assocamo\n" +
                "(orid, ampid, commid, auth, subsource, delta, seaz, rflag, lddate)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, ChannelGrouperTests.ChannelGrouperTestsOrId);
        statement.setInt(index++, ChannelGrouperTests.ChannelGrouperTestsAmp3Id);
        statement.setNull(index++, Types.INTEGER);
        statement.setString(index++, "CI");
        statement.setString(index++, "Jiggle");
        statement.setDouble(index++, 50.8928);
        statement.setDouble(index++, 184.9);
        statement.setString(index++, "H");
        statement.setString(index++, "2021-02-01 18:33:03.000");
        return insertQueryHelper(statement);
    }

    public int populateOriginDataForChannelGroupTests() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.origin\n" +
                "(orid, evid, prefmag, prefmec, commid, bogusflag, datetime, lat, lon, \"depth\", mdepth, \"type\", algorithm, algo_assoc, auth, subsource, datumhor, datumver, gap, distance, wrms, stime, erhor, sdep, erlat, erlon, totalarr, totalamp, ndef, nbs, nbfm, locevid, quality, fdepth, fepi, ftime, vmodelid, cmodelid, crust_type, crust_model, gtype, lddate, rflag)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'), ?);";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, ChannelGrouperTests.ChannelGrouperTestsOrId);
        statement.setInt(index++, ChannelGrouperTests.ChannelGrouperTestsEventId);
        statement.setInt(index++, ChannelGrouperTests.ChannelGrouperTestsPrefMag);
        statement.setInt(index++, ChannelGrouperTests.ChannelGrouperTestsPrefMec);
        statement.setNull(index++, Types.INTEGER);
        statement.setInt(index++, 0);
        statement.setDouble(index++, 1611739625.44);
        statement.setDouble(index++, 35.981);
        statement.setDouble(index++, -117.3168333);
        statement.setDouble(index++, 2.75);
        statement.setDouble(index++, 3.905);
        statement.setString(index++, "H");
        statement.setString(index++, "HYP2000");
        statement.setNull(index++, Types.VARCHAR);
        statement.setString(index++, "CI");
        statement.setString(index++, "Jiggle");
        statement.setString(index++, "WGS84");
        statement.setString(index++, "AVERAGE");
        statement.setDouble(index++, 71.0);
        statement.setDouble(index++, 10.0);
        statement.setDouble(index++, 0.15);
        statement.setNull(index++, Types.DOUBLE);
        statement.setDouble(index++, 0.27);
        statement.setDouble(index++, 0.92);
        statement.setNull(index++, Types.INTEGER);
        statement.setNull(index++, Types.INTEGER);
        statement.setInt(index++, 33);
        statement.setNull(index++, Types.INTEGER);
        statement.setInt(index++, 33);
        statement.setInt(index++, 14);
        statement.setInt(index++, 9);
        statement.setNull(index++, Types.VARCHAR);
        statement.setDouble(index++, 1.0);
        statement.setString(index++, "n");
        statement.setString(index++, "n");
        statement.setString(index++, "n");
        statement.setString(index++, "02");
        statement.setString(index++, "CI");
        statement.setString(index++, "H");
        statement.setString(index++, "03E");
        statement.setString(index++, "l");
        statement.setString(index++, "2024-06-12 11:24:16.601");
        statement.setString(index++, "F");
        return insertQueryHelper(statement);
    }
}
