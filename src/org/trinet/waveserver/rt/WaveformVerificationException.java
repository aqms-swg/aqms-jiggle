package org.trinet.waveserver.rt;
class WaveformVerificationException extends WaveformDataException {
    WaveformVerificationException() {
         super();
    }

    WaveformVerificationException(String message) {
         super(message);
    }
}
