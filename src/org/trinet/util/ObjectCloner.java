package org.trinet.util;
import java.io.*;
import java.util.*;
import java.awt.*;
// returns a deep copy of an object that can be serialized
public class ObjectCloner {

   private ObjectCloner(){}

   static public Object deepCopy(Object oldObj) throws Exception {
      ObjectOutputStream oos = null;
      ObjectInputStream ois = null;
      try {
         ByteArrayOutputStream bos = 
               new ByteArrayOutputStream();
         oos = new ObjectOutputStream(bos);
         // serialize and pass the object
         oos.writeObject(oldObj);
         oos.flush();
         ByteArrayInputStream bin = 
               new ByteArrayInputStream(bos.toByteArray());
         ois = new ObjectInputStream(bin);
         // return the new object
         return ois.readObject();
      }
      catch(Exception e) {
         System.out.println("Exception in ObjectCloner = ");
         e.printStackTrace(); 
         throw e;
      }
      finally {
         oos.close();
         ois.close();
      }
   }
}
