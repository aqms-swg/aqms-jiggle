//abstract methods belong to StripableDistanceResidualIF implement by JasiReading, MagList, SolutionList?
package org.trinet.jasi;
public interface StripableDistanceResidualIF extends StripableResidualIF { 
  public int stripByDistance(double val);
}
