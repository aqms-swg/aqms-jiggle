package org.trinet.jasi;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;
// NOTE: This class is not used by the coda mag code as of 11/15/04 -aww
public abstract class ChannelCodaClipAmpData extends AbstractChannelData implements Cloneable, java.io.Serializable {

    protected UnitsAmp cutoffAmp  = new UnitsAmp();
    protected UnitsAmp clipAmp = new UnitsAmp();

    protected ChannelCodaClipAmpData() {}

    protected ChannelCodaClipAmpData(ChannelIdIF id) {
        super(id);
    }
    protected ChannelCodaClipAmpData(ChannelIdIF id, DateRange dateRange) {
        super(id, dateRange);
    }
    protected ChannelCodaClipAmpData(ChannelIdIF id, DateRange dateRange, UnitsAmp cutoffAmp, UnitsAmp clipAmp) {
        super(id, dateRange);
        this.cutoffAmp = cutoffAmp;
        this.clipAmp = clipAmp;
    }
    protected ChannelCodaClipAmpData(ChannelIdIF id, DateRange dateRange, double cutoffAmp, int cutoffAmpUnits,
                    double clipAmp, int clipAmpUnits) {
        super(id, dateRange);
        this.cutoffAmp = new UnitsAmp(cutoffAmp, cutoffAmpUnits);
        this.clipAmp = new UnitsAmp(clipAmp, clipAmpUnits);
    }

    public static final ChannelCodaClipAmpData create() {
        return create(JasiObject.DEFAULT);
    }
    public static final ChannelCodaClipAmpData create(int schemaType) {
        return create(JasiFactory.suffix[schemaType]);
    }
    public static final ChannelCodaClipAmpData create(String suffix) {
        return (ChannelCodaClipAmpData)
            JasiObject.newInstance("org.trinet.jasi.ChannelCodaClipAmpData", suffix);
    }

    public static ChannelCodaClipAmpData getByChannel(Channel chan, java.util.Date date) {
        return (ChannelCodaClipAmpData)
           ChannelCodaClipAmpData.create().getByChannelId(chan.getChannelObj().getChannelId(), date);
    }

    public boolean copy(ChannelDataIF cd) {
      if (cd instanceof ChannelCodaClipAmpData) return false;
      ChannelCodaClipAmpData data = (ChannelCodaClipAmpData) cd;
      cutoffAmp.setValue(data.cutoffAmp.doubleValue());
      cutoffAmp.setUnits(data.cutoffAmp.getUnits());
      clipAmp.setValue(data.clipAmp.doubleValue());
      clipAmp.setUnits(data.clipAmp.getUnits());
      return true;
    }

    public void setAmpUnits(int iunits) {
      cutoffAmp.setUnits(iunits);
      clipAmp.setUnits(iunits);
    }
    public boolean setAmpUnits(String sunits) {
       return (cutoffAmp.setUnits(sunits) && clipAmp.setUnits(sunits));
    }

    public int getCutoffAmpUnits() {
      return cutoffAmp.getUnits();
    }
    public double getCutoffAmpValue() {
      return cutoffAmp.doubleValue();
    }
    public void setCutoffAmpValue(double maxValue) {
      cutoffAmp.setValue(maxValue);
    }
    public boolean isCutoffAmpNull() {
      return cutoffAmp.isNull();
    }
    protected void setCutoffAmp(UnitsAmp cutoffAmp) {
      this.cutoffAmp = cutoffAmp;
    }
    protected UnitsAmp getCutoffAmp() {
      return cutoffAmp;
    }

    public int getClipAmpUnits() {
      return clipAmp.getUnits();
    }
    public double getClipAmpValue() {
      // return clipAmp.doubleValue();
      return (clipAmp.isNull()) ? 0. : clipAmp.doubleValue(); // was NaN for null -aww 2009/03/10
    }
    public void setClipAmpValue(double clipValue) {
      clipAmp.setValue(clipValue);
    }
    public boolean isClipAmpNull() {
      return clipAmp.isNull();
    }
    protected void setClipAmp(UnitsAmp clipAmp) {
      this.clipAmp = clipAmp;
    }
    protected UnitsAmp getClipAmp() {
      return clipAmp;
    }

    public String toString(){
      return cutoffAmp.toString() + " " + clipAmp.toString();
    }

    public Object clone() {
      ChannelCodaClipAmpData data = (ChannelCodaClipAmpData) super.clone();
      data.cutoffAmp = (UnitsAmp) cutoffAmp.clone();
      data.clipAmp = (UnitsAmp) clipAmp.clone();
      return data;
    }
    public String toNeatString() { return toString(); }
    public String getNeatHeader() { return getNeatStringHeader(); }
    public static String getNeatStringHeader() {
        return "CutoffAmp Units    ClipAmp   Units";
    }
}
