package test.testqueries.common;

import test.common.DatabaseTests;
import test.common.UnitTests;

import javax.xml.crypto.Data;
import java.nio.file.FileSystems;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DatabaseTestsQueries extends TestQueries {

    public DatabaseTestsQueries(String URL) throws SQLException {
        super(URL);
    }

    public int addWaverootsFilePathToDatabase() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.waveroots\n" +
                "(archive, wavetype, status, net, wave_fmt, datetime_on, datetime_off, wcopy, fileroot)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?);";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(index++, "SCEDC");
        statement.setString(index++, "T");
        statement.setString(index++, "T");
        statement.setString(index++, "*");
        statement.setInt(index++, 2);
        statement.setDouble(index++, -2209000000.0);
        statement.setDouble(index++, 32504000000.0);
        statement.setInt(index++, 1);
        statement.setString(index++, DatabaseTests.getWavearchivePathInContainer());
        return insertQueryHelper(statement);
    }
}
