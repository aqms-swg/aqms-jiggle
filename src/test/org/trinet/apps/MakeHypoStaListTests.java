package test.org.trinet.apps;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.trinet.apps.MakeHypoStaList;
import test.common.DatabaseTests;
import test.testqueries.common.TestQueries;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class MakeHypoStaListTests extends DatabaseTests {

    @BeforeAll
    public static void populateEventDataForMakeGMPTest() throws Exception {
        TestQueries testQueries = new TestQueries(postgreSQLContainer.getJdbcUrl());
        int rows = testQueries.populateJasChannelViewForMakeHypoStaListTest();
        assertTrue(rows > 0);
    }

    @Test
    public void verify_makehypostalist_main_function_output_contains_correct_value() {
        org.trinet.apps.MakeHypoStaList.main(new String[]{getHost(), getDbName(), getDbUsername(), getDbPassword(),
                getSubProtocol(), getPortNumber()});
        assertTrue(MakeHypoStaList.getResult().contains("B918  PB  EH1  35 56.1420N117 36.1020W10420.0  P  0.00  0.00  0.00  0.00 0  0.00"));
    }
}
