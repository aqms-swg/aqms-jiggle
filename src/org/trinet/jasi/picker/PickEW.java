package org.trinet.jasi.picker;

import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.util.Scanner;
import java.util.NoSuchElementException;
import java.util.InputMismatchException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import org.trinet.jasi.AbstractWaveform;
import org.trinet.jasi.Channel;
import org.trinet.jasi.ChannelGain;
import org.trinet.jasi.JasiProcessingConstants;
import org.trinet.jasi.Phase;
import org.trinet.jasi.PhaseDescription;
import org.trinet.jasi.PhaseList;
import org.trinet.jasi.Solution;
//import org.trinet.jasi.TravelTime;
import org.trinet.jasi.Waveform;
import org.trinet.jasi.WFSegment;

import org.trinet.util.DateTime;
import org.trinet.util.GenericPropertyList;
import org.trinet.util.LeapSeconds;
import org.trinet.util.TimeSpan;

public class PickEW extends AbstractPicker {

    public static final double SMALL_DOUBLE = 1.0e-10;
    public static final double BIG_ZCROSS_SCALAR = 3.; // multiple of 1st-difference to indicate a big zero-crossing
    public static final double FIRST_MOTION_SCALAR = 1.6; // eabs scaling factor for 1st-motion onset weights
    public static final double DEFAULT_MIN_PEAK_SNR = 2.0; // eabs scaling factor for 1st-motion onset weights
    public static boolean logSamples = false; // output continuous function values per sample
    public static boolean altNoiseTest = false; // do alternate test to evaluate pick as noise
    public static double minPeakSNR = DEFAULT_MIN_PEAK_SNR;

    private static Parm DefaultParm = null;

    static {
        DefaultParm = new Parm();
        DefaultParm.sncl = null;
        DefaultParm.PickFlag = PhasePickerIF.NONE;
        DefaultParm.Itr1 = 3;
        DefaultParm.MinSmallZC = 10;
        DefaultParm.MinBigZC = 3;
        DefaultParm.MinPeakSize = 20;
        DefaultParm.MaxMint = 500;
        DefaultParm.RawDataFilt = .939;
        DefaultParm.CharFuncFilt = 3.;
        DefaultParm.StaFilt = .4;
        DefaultParm.LtaFilt = .015;
        DefaultParm.EventThresh = 5.;
        DefaultParm.RmavFilt = .9961;
        //DefaultParm.DeadSta = 1200.;
        //DefaultParm.Erefs = 50000.;
    }

    // Set when time series was broken, picker restarted 
    private int  sampleIdx = -1;  // Current segment timeseries array sample index

    //Pick variables
    private class Pick {
       double   time = 0.;           // Epoch UTC time 
       double[] xpk = new double[3]; // Absolute value of first three extrema after pick enabled 
       String   firstMotion = null;  // First motion ..=Not determined, c.=up, d.=down 
       int      weight = 4;          // Hypoinverse weight (0-4) 
       int      status = 0;          // 0=idle, 1=active (incomplete), 2=complete (unreported), 3=pick reported 
    }

    // Parm: Picking parameters
    static class Parm implements PickerParmIF, java.io.Serializable, Cloneable, Comparable {
        String sncl = null;         // sncl should be same a map key 
        int    PickFlag = 0;        // To use or not use for phase pick?
        int    Itr1 = 0;            // Parameter used to calculate itrm 
        int    MinSmallZC = 0;      // Minimum number of small zero crossings 
        int    MinBigZC = 0;        // Minimum number of big zero crossings 
        int    MinPeakSize = 0;     // Minimum size of 1'st three peaks 
        int    MaxMint = 0;         // Max interval between zero crossings in samples 
        double RawDataFilt = 0.;    // Filter parameter for raw data 
        double CharFuncFilt = 0.;   // Filter parameter for characteristic function 
        double StaFilt = 0.;        // Filter parameter for short-term average 
        double LtaFilt = 0.;        // Filter parameter for long-term average 
        double EventThresh = 0.;    // STA/LTA event threshold 
        double RmavFilt = 0.;       // Filter parameter for running mean absolute value 
        //double DeadSta = 0.;      // Dead station threshold 
        //double Erefs = 0.;        // Event termination parameter 

        public Parm() { }

        public Parm(String sncl) {
            this.sncl = sncl;
        }

        public void setSNCL(String sncl) {
            this.sncl = sncl;
        }

        public String getSNCL() {
            return this.sncl;
        }

        public int getPickFlag() {
            return PickFlag;
        }

        public void setPickFlag(int pickFlag) {
            PickFlag = pickFlag;
        }

        public int compareTo(Object o) {
            Parm p = (Parm) o;
            if (this.sncl == null && p.sncl == null) return 0;
            if (p.sncl == null) return -1;
            return this.sncl.compareTo(p.sncl);
        }

        public void copy(PickerParmIF parm) {
            //if (! (parm instanceof Parm)) return;
            Parm p = (Parm) parm;
            this.sncl = p.sncl;
            this.PickFlag = p.PickFlag;
            this.Itr1 = p.Itr1;
            this.MinSmallZC = p.MinSmallZC;
            this.MinBigZC = p.MinBigZC;
            this.MinPeakSize = p.MinPeakSize;
            this.MaxMint = p.MaxMint;
            this.RawDataFilt = p.RawDataFilt;
            this.CharFuncFilt = p.CharFuncFilt;
            this.StaFilt = p.StaFilt;
            this.LtaFilt = p.LtaFilt;
            this.EventThresh = p.EventThresh;
            this.RmavFilt = p.RmavFilt;
            //this.DeadSta = p.DeadSta;
            //this.Erefs = p.Erefs;
        } 

        public boolean equals(Object o) {
            if (this == o) return true;
            if (! (o instanceof Parm)) return false; 
            Parm p = (Parm) o;
            return  ( (this.sncl == null && p.sncl == null) || (this.sncl != null && this.sncl.equals(p.sncl)) );
        }
        /*
        public boolean equals(Object o) {
            if (this == o) return true;
            if (! (o instanceof Parm)) return false; 
            Parm p = (Parm) o;
            return  ( (this.sncl == null && p.sncl == null) || (this.sncl != null && this.sncl.equals(p.sncl)) ) &&  
                (this.PickFlag == p.PickFlag) && (this.Itr1 == p.Itr1) &&
                (this.MinSmallZC == p.MinSmallZC) && (this.MinBigZC == p.MinBigZC) &&
                (this.MinPeakSize == p.MinPeakSize) && (this.MaxMint == p.MaxMint) &&
                (this.RawDataFilt == p.RawDataFilt) && (this.CharFuncFilt == p.CharFuncFilt) &&
                (this.StaFilt == p.StaFilt) && (this.LtaFilt == p.LtaFilt) &&
                (this.EventThresh == p.EventThresh) && (this.RmavFilt == p.RmavFilt) &&
                (this.DeadSta == p. DeadSta) && (this.Erefs == p.Erefs);
        }
        */

        public int hashCode() {
            return (sncl == null) ? 0 : sncl.hashCode();
        }

        public PickerParmIF clone() {
            Parm p = null;
            try {
                p = (Parm) super.clone();
            }
            catch (CloneNotSupportedException ex) {
            }
            return p;
        }

        public String getFormattedHeader() {
            return FORMATTED_HEADER;
        }

        static final String FORMATTED_HEADER =
            "#                          MinBigZC     RawDataFilt     LtaFilt            \n" +
            "#             Pflg     MinSmallZC  MaxMint         StaFilt         RmavFilt\n" +
            "#SNCL               Itr1       MinPeakSize    CharFuncFilt    EventThresh  \n" +
            "#--------------------------------------------------------------------------\n";
            //14405.HNZ.CE.-- 1  3  40  3   20  500  .9850 3.00 0.40 0.015  5.0  0.9961  
            //"#                          MinBigZC     RawDataFilt     LtaFilt             DeadSta\n" +
            //"#             Pflg     MinSmallZC  MaxMint         StaFilt         RmavFilt\n" +
            //"#SNCL               Itr1       MinPeakSize    CharFuncFilt    EventThresh          Erefs\n" +
            //"#---------------------------------------------------------------------------------------\n";
            //14405.HNZ.CE.-- 1  3  40  3   20  500  .9850 3.00 0.40 0.015  5.0  0.9961  1200.  50000.

        public String toFormattedString() {
            StringBuilder sb =  new StringBuilder(132);
            sb.append(String.format("%16s", sncl));
            sb.append(String.format(" %1d", PickFlag));
            sb.append(String.format(" %2d", Itr1));
            sb.append(String.format(" %3d", MinSmallZC));
            sb.append(String.format(" %2d", MinBigZC));
            sb.append(String.format(" %4d", MinPeakSize));
            sb.append(String.format(" %4d", MaxMint));
            sb.append(String.format(" %6.4f", RawDataFilt));
            sb.append(String.format(" %4.2f", CharFuncFilt));
            sb.append(String.format(" %4.2f", StaFilt));
            sb.append(String.format(" %5.3f", LtaFilt));
            sb.append(String.format(" %4.1f", EventThresh));
            sb.append(String.format(" %7.4f", RmavFilt));
            //sb.append(String.format(" %6.0f", DeadSta));
            //sb.append(String.format(" %8.1f", Erefs));
            return sb.toString();
       }

       public String toParseableString() {
           StringBuilder sb = new StringBuilder(132);
           //sb.append(sncl);
           sb.append(String.format("%16s",sncl));
           sb.append(" ").append(PickFlag);
           sb.append(" ").append(Itr1);
           sb.append(" ").append(MinSmallZC);
           sb.append(" ").append(MinBigZC);
           sb.append(" ").append(MinPeakSize);
           sb.append(" ").append(MaxMint);
           sb.append(" ").append(RawDataFilt);
           sb.append(" ").append(CharFuncFilt);
           sb.append(" ").append(StaFilt);
           sb.append(" ").append(LtaFilt);
           sb.append(" ").append(EventThresh);
           sb.append(" ").append(RmavFilt);
           //sb.append(" ").append(DeadSta);
           //sb.append(" ").append(Erefs);
           return sb.toString();
       }

       public String toLabelledString() {
           StringBuilder sb = new StringBuilder(132);
           sb.append("NSCL=").append(sncl);
           sb.append(" PickFlag=").append(PickFlag);
           sb.append(" Itr1=").append(Itr1);
           sb.append(" MinSmallZC=").append(MinSmallZC);
           sb.append(" MinBigZC=").append(MinBigZC);
           sb.append(" MinPeakSize=").append(MinPeakSize);
           sb.append(" MaxMint=").append(MaxMint);
           sb.append(" RawDataFilt=").append(RawDataFilt);
           sb.append(" CharFuncFilt=").append(CharFuncFilt);
           sb.append(" StaFilt=").append(StaFilt);
           sb.append(" LtaFilt=").append(LtaFilt);
           sb.append(" EventThresh=").append(EventThresh);
           sb.append(" RmavFilt=").append(RmavFilt);
           //sb.append(" DeadSta=").append(DeadSta);
           //sb.append(" Erefs=").append(Erefs);
           return sb.toString();
       }

       public String toString() {
           return toLabelledString();
       }

    }

    //Picker station config parameters
    private class Station {
       Channel chan = null;
       Pick   pick = new Pick();  // Pick structure 
       Parm   parm = null;       // new Parm();  // Picker Configuration parameters from input file or GUI settings
       double crtinc = 0.;           // Increment added to ecrit at each zero crossing 
       double eabs = 0.;             // Running mean absolute value (aav) of rdat 
       double ecrit = 0.;            // Criterion level to determine if event is over 
       double elta = 0.;             // Long-term average of edat  (typically a 1-2 secs?)
       float  enddata = 0.f;          // Last data value of previous message, (Originally was int value) 
       double endtime = 0.;          // Stop time of previous message 
       double eref = 0.;             // STA/LTA reference level 
       double esta = 0.;             // Short-term average of edat (typically a few samples)
       int    evlen = 0;            // Event length in samp 
       int    first = 0;          // 0 the first time after gap on this channel is found 
       int    isml = 0;             // Small zero-crossing counter where esta < ecrit
       int    k = 0;                // Index to array of windows to push onto stack 
       int    m = 0;                // 0 if no event, otherwise, zero-crossing counter 
       int    mint = 0;             // Interval between zero crossings in samples 
       int    next = 0;             // Counter of zero crossings early in P-phase 
       int    nzero = 0;            // Big zero-crossing counter 
       float  old_sample = 0.f;       // Old value of float sample data 
       int    ns_restart = 0;       // Number of samples since restart 
       double rdat = 0.;             // Filtered data value 
       double rbig = 0.;             // Threshold for big zero crossings 
       double rlast = 0.;            // Size of last big zero crossing 
       double rold = 0.;             // Previous value of filtered data 
       float[]  sarray = new float[10]; // First 10 sample points after pick for 1'st motion determ 
       double tmax = 0.;             // Instantaneous maximum in current half cycle 
       float  xdot = 0.f;             // First difference at pick time 
       double xfrz = 0.;             // weighting factor used in first motion calculation 
       boolean haveP = false;   // set true only when phase is saved
       boolean haveS = false;   // set true only when phase is saved
       double eabsAtPick = 0.;
    }

    public PickEW() { } 

    public PickEW(GenericPropertyList gpl) {
        setProperties(gpl);
    }

    public void setDefaultProperties() {
        super.setDefaultProperties();
        setMyDefaultProperties();
    }

    private void setMyDefaultProperties() {
        name = "PickEQ";
        logSamples = false; // output continuous function values per sample
        altNoiseTest = false; // do alternate test to evaluate pick as noise
        minPeakSNR = DEFAULT_MIN_PEAK_SNR;
    }

    public void setProperties(GenericPropertyList gpl) {
        setProperties(gpl, true);
    }

    protected  void setProperties(GenericPropertyList gpl, boolean dumpEnabled) {
        super.setProperties(gpl);
        setMyDefaultProperties();
        if (gpl != null) {
            String pre = getPropertyPrefix();
            if (gpl.isSpecified(pre + "logSamples")) logSamples = gpl.getBoolean(pre + "logSamples");
            if (gpl.isSpecified(pre + "altNoiseTest")) altNoiseTest = gpl.getBoolean(pre + "altNoiseTest");
            if (gpl.isSpecified(pre + "minPeakSNR")) minPeakSNR = gpl.getDouble(pre + "minPeakSNR");
        }

        if (dumpEnabled && (debug || verbose)) {
            getProperties(new GenericPropertyList()).dumpProperties();
        }
    }

    public GenericPropertyList getProperties(GenericPropertyList gpl) {
        GenericPropertyList newgpl = super.getProperties(gpl);
        if (newgpl == null) return null;
        String pre = getPropertyPrefix();
        newgpl.setProperty(pre + "altNoiseTest", altNoiseTest);
        newgpl.setProperty(pre + "logSamples", logSamples);
        newgpl.setProperty(pre + "minPeakSNR", minPeakSNR);
        return newgpl;
    }
/*
// OLD FORMAT:
                                MinBigZC       RawDataFilt     LtaFilt         DeadSta          PreEvent
P   Pin  SNCL          MinSmallZC     MaxMint          StaFilt        RmavFilt           AltCoda
         Sta   Cmp Nt L Itr1   MinPeakSize     CharFuncFilt   EventThresh          CodaTerm         Erefs
 ----------------------------------------------------------------------------------------------------------
1   503  14405 HNZ CE -- 3  40  3  20  500  0  .985  3.  .4  .015 5.  .9961  1200.  49.14  .8  1.5  500000.
*/
    public GenericPropertyList getDefaultParmValues(GenericPropertyList gpl) {
        String pre = getPropertyPrefix();
        gpl.setProperty(pre + "parm.PickFlag", DefaultParm.PickFlag);
        gpl.setProperty(pre + "parm.Itr1", DefaultParm.Itr1);
        gpl.setProperty(pre + "parm.MinSmallZC", DefaultParm.MinSmallZC);
        gpl.setProperty(pre + "parm.MinBigZC", DefaultParm.MinBigZC);
        gpl.setProperty(pre + "parm.MinPeakSize", DefaultParm.MinPeakSize);
        gpl.setProperty(pre + "parm.MaxMint", DefaultParm.MaxMint);
        gpl.setProperty(pre + "parm.RawDataFilt", DefaultParm.RawDataFilt);
        gpl.setProperty(pre + "parm.CharFuncFilt", DefaultParm.CharFuncFilt);
        gpl.setProperty(pre + "parm.StaFilt", DefaultParm.StaFilt);
        gpl.setProperty(pre + "parm.LtaFilt", DefaultParm.LtaFilt);
        gpl.setProperty(pre + "parm.EventThresh", DefaultParm.EventThresh);
        gpl.setProperty(pre + "parm.RmavFilt", DefaultParm.RmavFilt);
        //gpl.setProperty(pre + "parm.DeadSta", DefaultParm.DeadSta);
        //gpl.setProperty(pre + "parm.Erefs", DefaultParm.Erefs);
        return gpl;
    }

    public void setDefaultParmValues(GenericPropertyList gpl) {
        String pre = getPropertyPrefix();
        if (gpl.isSpecified(pre + "parm.PickFlag")) DefaultParm.PickFlag = gpl.getInt(pre + "parm.PickFlag");
        if (gpl.isSpecified(pre + "parm.Itr1")) DefaultParm.Itr1 = gpl.getInt(pre + "parm.Itr1");
        if (gpl.isSpecified(pre + "parm.MinSmallZC")) DefaultParm.MinSmallZC = gpl.getInt(pre + "parm.MinSmallZC");
        if (gpl.isSpecified(pre + "parm.MinBigZC")) DefaultParm.MinBigZC = gpl.getInt(pre + "parm.MinBigZC"); 
        if (gpl.isSpecified(pre + "parm.MinPeakSize")) DefaultParm.MinPeakSize = gpl.getInt(pre + "parm.MinPeakSize"); 
        if (gpl.isSpecified(pre + "parm.MaxMint")) DefaultParm.MaxMint = gpl.getInt(pre + "parm.MaxMint"); 
        if (gpl.isSpecified(pre + "parm.RawDataFilt")) DefaultParm.RawDataFilt = gpl.getDouble(pre + "parm.RawDataFilt");
        if (gpl.isSpecified(pre + "parm.CharFuncFilt")) DefaultParm.CharFuncFilt = gpl.getDouble(pre + "parm.CharFuncFilt");
        if (gpl.isSpecified(pre + "parm.StaFilt")) DefaultParm.StaFilt = gpl.getDouble(pre + "parm.StaFilt");
        if (gpl.isSpecified(pre + "parm.LtaFilt")) DefaultParm.LtaFilt = gpl.getDouble(pre + "parm.LtaFilt");
        if (gpl.isSpecified(pre + "parm.EventThresh")) DefaultParm.EventThresh = gpl.getDouble(pre + "parm.EventThresh");
        if (gpl.isSpecified(pre + "parm.RmavFilt")) DefaultParm.RmavFilt = gpl.getDouble(pre + "parm.RmavFilt"); 
        //if (gpl.isSpecified(pre + "parm.DeadSta")) DefaultParm.DeadSta = gpl.getDouble(pre + "parm.DeadSta"); 
        //if (gpl.isSpecified(pre + "parm.Erefs")) DefaultParm.Erefs = gpl.getDouble(pre + "parm.Erefs"); 
    }

    public void setDefaultParmValues(PickerParmIF parm) {
        DefaultParm.copy(parm);
    }

    public PickerParmIF getDefaultParm() {
        return DefaultParm;
    }

    public String toParseableString(PickerParmIF parm) {
        return (parm instanceof Parm) ? ((Parm)parm).toParseableString() : (String) null;
    }

    /**
     * @param phasesToPickFlag PhasePickerIF.NONE, do not pick, a no-op, ignores input sncl mapped PickerParmIF flag.
     * @param phasesToPickFlag PhasePickerIF.P_ONLY, P arrival, ignores input sncl mapped PickerParmIF flag 
     * @param phasesToPickFlag PhasePickerIF.S_ONLY, S arrival, ignores input sncl mapped PickerParmIF flag.
     * @param phasesToPickFlag PhasePickerIF.P_AND_S, both arrivals, ignores input sncl mapped PickerParmIF flag.
     * @param phasesToPickFlag PhasePickerIF.DEFAULT, uses input sncl napped PickerParmIF flag, if none, use defaults by component type.
     * @param scnlMappedParm  picker configuration parameters mapped to String of the form: net.sta.seed.loc derived from input waveform Channel.
     * @param scanSpan timespan over which to scan waveform timeseries for phase picks
     * @param pSpan timespan over which to assign a P pick.
     * @param sSpan timespan over which to assign a S pick.
     * @return list containing phases or empty list if no picks were made
     */
    public PhaseList pick(Waveform wf, int phasesToPickFlag, PickerParmIF snclMappedParm, TimeSpan scanSpan, TimeSpan pSpan, TimeSpan sSpan) {
        PhaseList phList = new PhaseList(2);

        if (reject(wf)) {
            if (debug) {
                System.out.printf(" INFO %s channel rejected by SNCL attribute or its absence from candidate list.%n", name, snclMappedParm.getSNCL());
            }
            return phList;
        }

        // Create new Waveform here, EXTRACT subsegments from original, DEMEAN new waveform before scan for picks
        AbstractWaveform newWf = (AbstractWaveform) wf.createWaveform();
        newWf.copyHeader(wf);

        double scanStartTime = scanSpan.getStart();
        double scanEndTime = scanSpan.getEnd();

        newWf.setSegmentList(wf.getSubSegments(scanStartTime, scanEndTime), true); // resets packet spans, not that it matters here
        newWf.trimSpanToWFSegmentSpan(); // reset epoch start and end in new wf
        //Don't demean, RawDataFilt, high-pass filter removes bias, plus demean here could create offset between time gapped segments
        //newWf.demean(); // remove the bias from the timeseries
        WFSegment[] segs = newWf.getArray(); // get the new demeaned segments
        TimeSpan segSpan = newWf.getTimeSpan(); // new wf time span
         if (debug) System.out.printf("DEBUG %s scanning for picks in timespan of: %s%n", name, segSpan);

        // Make new picker Station for input
        Station sta = new Station();
        sta.parm = (Parm) snclMappedParm;

        int phFlag = phasesToPickFlag;
        if (phFlag >= PhasePickerIF.DEFAULT) {
            phFlag = sta.parm.getPickFlag();
            if (phFlag >=PhasePickerIF.DEFAULT) phFlag = 0; // undefined state, don't pick
        }
        if (phFlag <= 0) {
            if (debug) System.out.printf("DEBUG %s pick flag %d is set to NO picks: %s%n", name, phFlag, sta.parm.getSNCL());
            return phList; // don't pick
        }

        sta.chan = newWf.getChannelObj(); // alias new copy

        // TOP OF LOOP over all WFSegment's for a channel, scan timeseries for arrivals:
        WFSegment wfseg = null;
        for (int idx = 0; idx<segs.length; idx++) { // LOOP over segments
            wfseg = segs[idx];
            // Skip from start of the waveform to ? before starting to average data
            segSpan = wfseg.getTimeSpan();

            if (segSpan.isBefore(scanStartTime)) {
                if (debug) System.out.printf("DEBUG %s segment %d skipped, timespan before %s%n", name, idx, LeapSeconds.trueToString(scanStartTime));
                continue; // shouldn't be the case if subsegs extracted, but skip forward
            }
            if (segSpan.isAfter(scanEndTime)) {
                if (debug) System.out.printf("DEBUG %s segment %d skipped, timespan after %s%n", name, idx, LeapSeconds.trueToString(scanEndTime));
                break; // no need to go further?, shouldn't be the case if subsegs extracted 
            }

            int nsamp = wfseg.size();
            int gapSize = 0;
            if ( sta.first == 0 ) { // Do this the first time for a SNCL to force accumulation of averages without picking
                sta.endtime = wfseg.getEpochEnd();
                gapSize = restartLength;
            }
            else { // not the 1st segment so check for time gaps, and if found, interpolate samples BETWEEN waveform segments
                // Compute the number of samples since the end of the previous message.
                // If (gapSize == 1), no data has been lost between messages.
                // If (1 < gapSize <= maxSampleGap), data will be interpolated.
                double gsd = (wfseg.getEpochStart() - sta.endtime)/wfseg.getSampleInterval();
                // size < 0 Invalid. Time going backwards. 
                gapSize = ( gsd < 0. ) ? 0 : (int) Math.round(gsd);

                if (gapSize > 1) { // time-tear
                    if (gapSize <= maxSampleGap) { // Interpolate missing samples and prepend them to the current timeseries buffer
                        if (debug) System.out.printf("%s Found %4d sample gap. Interpolating station:%s%n",
                                name, gapSize, sta.chan.toDelimitedSeedNameString());
                        wfseg = interpolate((double)sta.enddata, sta.endtime, wfseg, gapSize);
                        nsamp = wfseg.size();
                    }
                }
            }

            // reset sample index offset for current segment before restart in case we need to accumulate new esta/elta statistics
            sampleIdx = -1;

            boolean restarting = false;
            if (debug) System.out.printf("DEBUG %s gapSize: %d maxSampleGap: %d restartLength: %d sta.ns_restart: %d%n",
                    name, gapSize, maxSampleGap, restartLength, sta.ns_restart);
            if ( (gapSize > maxSampleGap) || (gapSize >= restartLength) ||
                  (sta.ns_restart > 0 && sta.ns_restart < restartLength) ) { //  picker should go into restart mode.

                // For large gap, enter restart mode. In restart mode, calculate STAs and LTAs without picking.
                // Start picking again after a specified number of samples has been processed.
                //samples2scan = Math.max(samples2scan, (int) scanStartOffset*wf.getSampleRate())
                int samples2scan = Math.min(nsamp, restartLength); 
                if ( (gapSize > maxSampleGap) || (gapSize >= restartLength)) {
                    if (debug) System.out.printf("DEBUG %s Found %4d sample gap. Restarting channel %s%n",
                                                  name, gapSize, sta.chan.toDelimitedSeedNameString());
                }
                else samples2scan = Math.min(nsamp, restartLength-sta.ns_restart); // no gap, a continuation of active restart into next segment
                  
                if (debug) System.out.printf("DEBUG %s doing restart scan of samples2scan: %d%n", name, samples2scan);

                restarting = restart( sta, samples2scan, gapSize );
                if (restarting) {  // add more data samples to Sta statistics
                   float[] ts = wfseg.getTimeSeries();
                   for ( int i=0; i < samples2scan; i++ ) {
                       doOneSample(ts[i], sta );
                   }
                   if (samples2scan < nsamp) restarting = false;
                }
            }

            if (! restarting) {
                sta.first = 1; // not in restart
                if (debug) System.out.printf("DEBUG %s not in restart mode, calling pickRA for %s%n", name, sta.parm.getSNCL());
                int status = pickRA(sta, wfseg, pSpan, sSpan, phList, phFlag);
                if (debug) System.out.printf("DEBUG %s pickRA return status: %d%n", name, status);
                if (status == 3) { // pick was made and saved
                    if (phFlag == P_ONLY) break; // only want P, so we're done
                    if (phFlag == S_ONLY) break; // only want S, so we're done
                    if (phFlag == P_AND_S && phList.size() == 2) break; // want both P and S, so we're done
                }
            }

            // Save time and amplitude of the end of the current message
            sta.enddata = wfseg.getTimeSeries()[nsamp - 1];
            sta.endtime = wfseg.getEpochEnd();
            if (debug) System.out.printf("DEBUG %s sta.enddata=%12.2g sta.endtime=%s%n", name, sta.enddata, LeapSeconds.trueToString(sta.endtime));
            // scan another segment til restart or pick exhausted
        } // end of loop

        return phList;
    } // end of pick method

    private Phase makePhase(Station sta, String type) {

        Phase ph = Phase.create();

        ph.setChannelObj(sta.chan);
        ph.setProcessingState(JasiProcessingConstants.STATE_AUTO_TAG);
        ph.datetime.setValue(sta.pick.time);
        ph.description.iphase = type;
        ph.description.setWeight(sta.pick.weight);
        ph.qualityToDeltaTime(); // added 2011/05/12 -aww

        double quality = ph.getQuality();
        boolean dofm = (quality > firstMoQualityMin);
        dofm &= (type.equals("P") || firstMoOnS); 
        dofm &= (sta.chan.isVertical() || firstMoOnHoriz);
        if (dofm) {
            ph.description.fm = sta.pick.firstMotion;
        }
        else {
            ph.description.fm = PhaseDescription.DEFAULT_FM;
        }

        // Kludge here:
        if (sta.pick.weight == 0) ph.description.ei = "i";
        else if (sta.pick.weight == 1) ph.description.ei = "i";  // should 1 weight be "i" ?
        else ph.description.ei = "e"; // replaced "w" with "e" -aww 
        
        return ph;
    }

    /**
    * Check for breaks in the time sequence of messages.
    * @param nsamp sample count in timeseries segment
    * @param gapSize missing sample count 
    * @return true if picker is in restart mode
    * @return false if picker is not in restart mode
    */
    private boolean restart( Station sta, int nsamp, int gapSize ) {
        
        // If gapSize>maxSampleGap, begin a restart sequence.
        if ( gapSize > maxSampleGap) {
          initVar( sta ); // Initialize internal variables.
          sta.ns_restart = nsamp; // Save the number of samples processed in restart mode.
          return true;
        }

        if ( sta.ns_restart >= restartLength) return false; // The restart sequence is over

        sta.ns_restart += nsamp; // The restart sequence is still in progress

        return true;
    }

    /**
    * Initialize Station variables to 0 for one station.
    */
    private void initVar( Station sta ) {
        sta.crtinc     = 0.; // Increment added to ecrit at each zero crossing 
        sta.eabs       = 0.; // Running mean absolute value (aav) of rdat 
        sta.ecrit      = 0.; // Criterion level to determine if event is over 
        sta.elta       = 0.; // Long-term average of edat 
        sta.enddata    = 0.f; // Sample at end of previous message 
        sta.endtime    = 0.; // Time at end of previous message 
        sta.eref       = 0.; // STA/LTA reference level 
        sta.esta       = 0.; // Short-term average of edat 
        sta.evlen      = 0;  // Event length in samp 
        sta.first      = 0;  // => First segment after a gap, no stats accumulated yet for this channel
        sta.isml       = 0;  // Small zero-crossing counter where esta < ecrit
        sta.k          = 0;  // Index to array of windows to push onto stack 
        sta.m          = 0;  // 0 if no event; otherwise, zero-crossing counter 
        sta.next       = 0;  // Counter of zero crossings early in P-phase 
        sta.ns_restart = 0;  // Restart sample count 
        sta.nzero      = 0;  // Big zero-crossing counter 
        sta.old_sample = 0;  // Old value of integer data 
        sta.rdat       = 0.; // Filtered data value 
        sta.rbig       = 0.; // Threshold for big zero crossings 
        sta.rlast      = 0.; // Size of last big zero crossing 
        sta.rold       = 0.; // Previous value of filtered data 
        sta.tmax       = 0.; // Instantaneous maximum in current half cycle 
        sta.xdot       = 0.f;  // First difference at pick time 
        sta.xfrz       = 0.; // weighting factor used in first motion calculation 

        for (int i = 0; i < 10; i++) {
           sta.sarray[i] = 0.f;         // First 10 points of first motion 
        }

        // Pick variables
        sta.pick.time = 0.;         // Pick time 

        for (int i = 0; i<3; i++) {
           sta.pick.xpk[i] = 0.;    // Absolute value of extrema after pick enabled 
        }

        sta.pick.firstMotion = ".."; // First motion  ..=Not determined  c.=Up  d.=Down 
        sta.pick.weight = 3;    // Hypoinverse weight (0-3) 
        sta.pick.status = 0;    // status: 0=idle, 1=active (incomplete), 2=complete

    }

    /**
    *  Process one digital sample.
    *  The constant SmallDouble is used to avoid underflow in the calculation of rdat.
    *  Modifies: rold, rdat, old_sample, esta, elta, eref, eabs.
    *  @param sample      One waveform data sample
    *  @param Sta         Station list
    */
    private void doOneSample( float sample, Station sta ) {

        //System.out.println("sta == null? " + (sta == null));
        //if (sta != null) System.out.println("sta.parm == null? " + (sta.parm == null));

        // Store present value of filtered data 
        sta.rold = sta.rdat;

        // Compute new value of filtered data 
        sta.rdat = (sta.rdat * sta.parm.RawDataFilt) + (double) (sample - sta.old_sample) + SMALL_DOUBLE;

        // Compute 1'st difference of filtered data 
        double rdif = sta.rdat - sta.rold;

        // Store old data value 
        sta.old_sample = sample;

        // Compute characteristic function 
        double edat = (sta.rdat * sta.rdat) + (sta.parm.CharFuncFilt * rdif * rdif);

        // Compute esta, the short-term average of edat 
        sta.esta += sta.parm.StaFilt * (edat - sta.esta);

        // Compute elta, the long-term average of edat 
        sta.elta += sta.parm.LtaFilt * (edat - sta.elta);

        // Compute eref, the reference level for event checking 
        sta.eref = sta.elta * sta.parm.EventThresh;

        // Compute eabs, the running mean absolute value of rdat 
        sta.eabs = (sta.parm.RmavFilt * sta.eabs) + (( 1.0 - sta.parm.RmavFilt ) * Math.abs( sta.rdat ));
        if (logSamples) {
            //maxEsta = Math.max(maxEsta, sta.esta);
            //maxElta = Math.max(maxElta, sta.elta);
            //maxEabs = Math.max(maxEabs, sta.eabs);
            //maxEref = Math.max(maxEref, sta.eref);
            //maxEcrit = Math.max(maxEcrit, sta.ecrit);
            //if (sta.first == 0)
            System.out.printf("%10.1f %10.1f %10.1f %10.1f %10.3f %10.2f%n", sta.ecrit, sta.esta, sta.elta, sta.eref, sta.crtinc, sta.eabs);
        }        
    } 

    /**
    * Search for a pick event.
    * @return sample index if event found; otherwise -1
    **/
    private int scanForEvent( Station sta, WFSegment wfseg ) {

        // Set pick calculations to inactive mode
        sta.pick.status = 0;

        // Loop through all samples in the message
        float old_sample = 0.f;  // Previous sample 
        float new_sample = 0.f;  // Current sample 
        double old_eref = 0.;    // Old value of eref 

        int nsamp = wfseg.size();
        float[] ts = wfseg.getTimeSeries();
        if (debug) {
             System.out.printf(" INFO %s scanForEvent sample size: %d starting at sample %d -> %s%n", name, nsamp, sampleIdx+1,
                     LeapSeconds.trueToString(wfseg.getEpochStart() + (sampleIdx+1)*wfseg.getSampleInterval()) );
        }
        while ( ++sampleIdx < nsamp ) {

           new_sample = ts[sampleIdx];
           old_sample = sta.old_sample;
           old_eref   = sta.eref; // short/long term average ratio

           // Update sta.rold, sta.rdat, sta.old_sample, sta.esta, sta.elta, sta.eref, and sta.eabs using the current sample
           doOneSample( new_sample, sta );

           // Station is assumed dead when (eabs > DeadSta)
           //if ( sta.eabs > sta.parm.DeadSta ) continue; // to next sample along series

           // Has the short-term average abruptly increased with respect to the long-term average?
           if ( sta.esta > sta.eref ) {

               // Initialize pick variables
               sta.pick.time = wfseg.getEpochStart() + (sampleIdx-lagAdjustment) * wfseg.getSampleInterval();
               if (debug) {
                   System.out.printf(" INFO %s NEW PICK at sample %d time: %14.4f %s%n",
                           name, sampleIdx, sta.pick.time, LeapSeconds.trueToString(sta.pick.time) );
               }

               sta.crtinc    = 0.;  // was set to sta.eref / sta.parm.Erefs; // incr added to ecrit at z-cross
               sta.ecrit     = old_eref; // level to determine if event done
               sta.evlen     = 0; // number of samples
               sta.isml      = 0; // small z-crossing count where esta < ecrit
               sta.k         = 0; // window array index
               sta.m         = 1; // z-crossing count
               sta.mint      = 0; // samples between z-crossings
               //sta.ndrt      = 0;
               sta.next      = 0; // early z-crossing count
               sta.nzero     = 0; // big z-crossing count
               sta.rlast     = sta.rdat; // last big z-cross value
               //sta.rsrdat    = 0.;
               sta.sarray[0] = new_sample; // 1st point after pick 4 1st motion
               sta.tmax      = Math.abs( sta.rdat ); // max in current half-cycle
               sta.xfrz      = FIRST_MOTION_SCALAR * sta.eabs;

               // Compute threshold for big zero crossings
               sta.xdot = new_sample - old_sample; // 1st difference
               sta.rbig = ( (sta.xdot < 0.) ? -sta.xdot : sta.xdot ) / BIG_ZCROSS_SCALAR; // threshold for big z-crossing, divisor was hard-coded 3.
               sta.rbig = (sta.eabs > sta.rbig) ? sta.eabs : sta.rbig; // the larger of the abs running average or 1st-diff fraction
               sta.eabsAtPick = sta.eabs;

               sta.pick.status = 1; // pick is now active 

               return sampleIdx;
           }
        } // end of while

        return -1; // Message ended; event not found 
    }

    /**
    *  @return |x| * sign( y )
    */
    private double sign( double x, double y ) {
        if (x == 0.) return 0.;
        if (x < 0.) {
          return (y < 0.) ? x : -x;
        }
        return (y > 0.) ? x : -x;
    }

    /**
    * Pick one waveform segment buffer.
    * @param sta station being processed
    * @param wfseg waveform segment buffer
    * @return event activity flag
    */
    private int pickRA(Station sta, WFSegment wfseg, TimeSpan pSpan, TimeSpan sSpan, PhaseList phList, int phaseFlag) {

        int evtFlag = 0;     // 1 if an event is active
        // sampleIdx = -1;  // sample index offset reset at start of current segment NOTE:  reset moved to pickEW before restart 

        if ( debug ) System.out.printf( "DEBUG %s pickRA sta: %s pick.status: %d%n", name, sta.chan.toDelimitedSeedNameString(), sta.pick.status );

        // Is a pick is still active? if so, continue it's calculation
        if ( (sta.pick.status == 1) ) {

           if ( debug ) System.out.printf( "DEBUG %s pickRA: Pick still active.%n", name );

           evtFlag = eventActive( sta, wfseg, pSpan, sSpan, phList, phaseFlag );
           if ( evtFlag == 1 ) return 1; // Event active at end of segment, need to get another segment to complete

        }

        // Go into search mode for a new pick event, then scan along segment until all pick are done or aborted on current segment
        int sampleIdx = 0;
        while ( true ) {

           if ( debug ) System.out.printf( "DEBUG %s pickRA: Scanning for new pick event.%n", name );

           if ( scanForEvent( sta, wfseg ) < 0 ) { 
              if ( debug ) System.out.printf( "DEBUG %s pickRA: Event not found, last sampleIdx= %d%n", name, sampleIdx);
              break;
           }
           if ( debug ) System.out.printf( "DEBUG %s pickRA: Event found.%n", name );

           evtFlag = eventActive( sta, wfseg, pSpan, sSpan, phList, phaseFlag );
           if ( evtFlag == 1 ) break; // Event active at end of segment, get another segment to complete

           //else do next pick in this segment

        }

        return evtFlag;
    }

    /**
    * eventActive
    *  @return 0 if pick is over
    *  @return 1 if pick is still active
    *  @return -2 No recent zero crossings. Pick aborted.
    *  @return -3 Event determined to be noise. Pick aborted.
    */
    private int eventActive( Station sta, WFSegment wfseg, TimeSpan pSpan, TimeSpan sSpan, PhaseList phList, int phaseFlag ) {

        // An event pick is active. See if it should be declared over.
        float [] ts = wfseg.getTimeSeries();
        int nsamp = ts.length;
        float new_sample = 0.f; // Current sample

        String statusStr = null;
        if (sta.pick.status < 0) statusStr = "Pick aborted";
        else if (sta.pick.status == 0) statusStr = "Pick inactive";
        else if (sta.pick.status == 1) statusStr = "Pick active"; 
        else statusStr = "Pick complete";

        while ( ++sampleIdx < nsamp ) {

           new_sample = ts[sampleIdx];

           // Update sta.rold, sta.rdat, sta.old_sample, sta.esta, sta.elta, sta.eref, and sta.eabs using the current sample
           doOneSample( new_sample, sta );

           // BEGIN PICK CALCULATION
           if ( sta.pick.status == 1 ) { // the pick is active
              // Save first 10 points after pick for first motion determination later
              if ( ++sta.evlen < 10 ) sta.sarray[sta.evlen] = new_sample;

              // Store current data if it is a new extreme value ie. 3-zero crossing=>1-cycle 
              if ( sta.next < 3 ) {
                 double adata = Math.abs( sta.rdat );
                 if ( adata > sta.tmax ) sta.tmax = adata;
              }

              // Test for large zero crossing. Large zero-crossing amplitude must exceed rbig and must represent a
              // crossing of opposite polarity to previous crossing.
              // System.out.printf( "DEBUG %s eventActive rdat, rbig, rlast: %10.1g %10.1g %10.1g%n", name, sta.rdat, sta.rbig, sta.rlast ); 
              if ( Math.abs( sta.rdat ) >= sta.rbig ) {
                 if ( sign( sta.rdat, sta.rlast ) != sta.rdat ) {
                    sta.nzero++;
                    sta.rlast = sta.rdat;
                    //System.out.printf( "DEBUG %s eventActive nzero: %d%n", name, sta.nzero ); 
                 }
              }

              // Increment zero crossing interval counter.  Terminate pick if no zero crossings have occurred recently.
              if ( ++sta.mint > sta.parm.MaxMint ) {
                  //if ( debug ) System.out.printf("DEBUG %s eventActive No recent zero crossings. Picking aborted.%n", name );
                  statusStr = "No recent zero crossings. Pick aborted.";
                  sta.pick.status = -2;
                  break; // BREAK-OUT and RETURN
              }

              // Test for small zero crossing
              if ( sign( sta.rdat, sta.rold ) == sta.rdat ) {
                  //if ( debug ) System.out.printf("DEBUG %s eventActive No small zero crossing at this sample.%n", name );
                  continue;  // SKIPS BELOW CODE, go to next sample, pick is not over
              }

              // Small zero crossing found.  Reset zero crossing interval counter.
              sta.mint = 0;

              // Update ecrit to determine whether at this crossing esta is still above ecrit.
              // If esta is greater than ecrit, set isml, the number of successive small crossings, =0, else increment isml, 
              sta.ecrit += sta.crtinc; // Bump threshold higher as time progresses
              sta.isml++;
              if ( sta.esta > sta.ecrit ) sta.isml = 0;

              // Store extrema of preceeding half cycle
              if ( sta.next < 3 ) {
                 sta.pick.xpk[sta.next++] = sta.tmax;

                 if ( sta.next == 1 ) {
                    double vt3 = sta.tmax / BIG_ZCROSS_SCALAR; // divisor was hard-coded 3.
                    sta.rbig = ( vt3 > sta.rbig ) ? vt3 : sta.rbig; // reset rbig to 1/3 of the max of this 1/2-cycle
                 }

                 sta.tmax = 0.; // reset for next half-cycle
              }

              // Compute itrm, the number of small z-crossings to be allowed before declaring the pick over.
              // This will start out quite small and increase during the event to a maximum of 50.
              int itrm = ( sta.m > 150 ) ? 50 : sta.parm.Itr1 + (sta.m / sta.parm.Itr1);

              // For pick to be over need at least the min z-cross and successive z-crossing count >= itrm
              if ( (++sta.m != sta.parm.MinSmallZC) && (sta.isml < itrm) ) {
                  //if ( debug ) System.out.printf("DEBUG %s eventActive small zero crossings %d!=%d and %d<%d%n",
                  //                                name sta.m, sta.parm.MinSmallZC, sta.isml, itrm);
                  continue;   // SKIP BELOW, go to next sample, pick is not over
              }

              if ( debug ) {
                 System.out.printf("DEBUG %s eventActive first 3 peaks: %10.5g %10.5g %10.5g cnts, #smallZC= %d #bigZC= %d prePickNoise: %10.5g%n",
                                 name, sta.pick.xpk[0], sta.pick.xpk[1], sta.pick.xpk[2], sta.m, sta.nzero, sta.eabsAtPick);
              }

              // See if the pick was a noise pick
              boolean isNoise = true;  // assume noise
              int xpkCntOkCnt = 0; 
              //ChannelGain gain = wfseg.getChannelObj().getGain(new DateTime(sta.pick.time, true));
              for (int i=0; i<3; i++ ) {
                 //System.out.println("DEBUG PICKEW wfseg gain: " + sta.pick.xpk[i]/gain.doubleValue());
                 if ( sta.pick.xpk[i] >= (double) sta.parm.MinPeakSize ) {
                    if (sta.pick.xpk[i] > minPeakSNR*sta.eabsAtPick) xpkCntOkCnt++; 
                    if ( (sta.m >= sta.parm.MinSmallZC) && (sta.nzero >= sta.parm.MinBigZC) ) isNoise = false;
                    //break;
                 }
              }
              if (altNoiseTest && xpkCntOkCnt == 3) isNoise = false; // Kludge?

              if ( isNoise ) { // If so, terminate pick.
                  //if ( debug ) System.out.printf( "DEBUG %s eventActive Noise. Picking aborted.%n", name );
                  statusStr = "Noise, too few zero-crossings or peaks < MinPeakSize . Pick aborted.";
                  sta.pick.status = -3; // pick event declared noise
                  break; // BREAK-OUT and RETURN
              }

              // VALID pick was found.  Determine the first motion.
              sta.pick.firstMotion = "..";  // First motion unknown

              int k=0;
              while (true) {
                 if ( sta.xdot <= 0 ) { // 1st diff going D?
                    if ( (sta.sarray[k+1] > sta.sarray[k]) || (k == 8) ) { // motion reversed, or still D at end
                       if ( k == 0 ) break; // we don't know
                       sta.pick.firstMotion = "d."; // Assume first motion is down
                       break;
                    }
                 }
                 else { // 1st diff going U?
                    if ( (sta.sarray[k+1] < sta.sarray[k]) || (k == 8) ) { // motion has reversed, or still U at end
                       if ( k == 0 ) break; // we don't know
                       sta.pick.firstMotion = "c."; // Assume first motion is up
                       break;
                    }
                 }
                 k++; // check next pairing
              }

              // Pick weight calculation
              // Is 1st half-cycle peak diff > 1st sample in pick, use it, else the second half-cycle peak
              double xpc = ( sta.pick.xpk[0] > Math.abs( (double)sta.sarray[0] ) ) ?  sta.pick.xpk[0] : sta.pick.xpk[1];
              double xon = Math.abs( (double)sta.xdot / sta.xfrz );
              //NOTE:  xpk are extrema of the rdat=rdat*parm.RawDataFilt+(new_sample-old_sample) within the half-cycle
              double xp0 = sta.pick.xpk[0] / sta.xfrz;
              double xp1 = sta.pick.xpk[1] / sta.xfrz;
              double xp2 = sta.pick.xpk[2] / sta.xfrz;

              sta.pick.weight = 3; // start with low weight

              if ( (xp0 > 2.) && (xon > .5) && (xpc > 25.) ) sta.pick.weight = 2;

              if ( (xp0 > 3.) && ((xp1 > 3.) || (xp2 > 3.)) && (xon > .5) && (xpc > 100.) ) sta.pick.weight = 1;

              if ( (xp0 > 4.) && ((xp1 > 6.) || (xp2 > 6.)) && (xon > .5) && (xpc > 200.) ) sta.pick.weight = 0;

              sta.pick.status = 2; // Pick event complete, done 

              //

              double pTime = (pSpan.isValid()) ? pSpan.getCenter() : -Double.MAX_VALUE;
              double sTime = (sSpan.isValid()) ? sSpan.getCenter() :  Double.MAX_VALUE;
              boolean sameTime = (pTime == sTime);

              double smp = (pTime == -Double.MAX_VALUE || sTime ==  Double.MAX_VALUE) ? 0. : sTime - pTime; 

              double pDelta = (pTime == -Double.MAX_VALUE) ?  9999.9 : Math.abs(pTime-sta.pick.time);
              double sDelta = (sTime ==  Double.MAX_VALUE) ?  9999.9 : Math.abs(sTime-sta.pick.time);
              if (debug) {
                  String pStr = (pTime == -Double.MAX_VALUE) ? "Undef" : LeapSeconds.trueToString(pTime); 
                  String sStr = (sTime ==  Double.MAX_VALUE) ? "Undef" : LeapSeconds.trueToString(sTime); 
                  System.out.printf("DEBUG %s eventActive predicted P: %s, S: %s%n", name, pStr, sStr); 
                  System.out.printf("DEBUG %s eventActive pDelta=%6.3f sec, sDelta=%6.3f sec%n", name, pDelta, sDelta);
              }

              boolean saveP = !sta.haveP && pSpan.contains(sta.pick.time); // none and in time range
              String savePmsg = "";
              if (!saveP && debug) savePmsg = String.format("DEBUG %s eventActive saveP? (!haveP & pSpan contains pick): %s%n", name, saveP);
              saveP &= (phaseFlag == PhasePickerIF.P_ONLY || phaseFlag == PhasePickerIF.P_AND_S);
              if (!saveP && debug) savePmsg = String.format("DEBUG %s eventActive saveP? (phase flag OK): %s%n", name, saveP);
              saveP &= sameTime || ( ((sta.pick.time <= sTime) && pDelta <= sDelta) || (smp < 1.0) );
              if (!saveP && debug) savePmsg = String.format("DEBUG %s eventActive saveP? (pick<=sTime & pDelta<=sDelta): %s%n", name, saveP);

              boolean saveS = !saveP && !sta.haveS;
              String saveSmsg = "";
              if (!saveS && debug) saveSmsg = String.format("DEBUG %s eventActive saveS? (!saveP & !haveS): %s%n", name, saveS);
              saveS &= sSpan.contains(sta.pick.time); // none and in time range
              if (!saveS && debug) saveSmsg = String.format("DEBUG %s eventActive saveS? (sSpan contains pick): %s%n", name, saveS);
              saveS &= (phaseFlag == PhasePickerIF.S_ONLY || phaseFlag == PhasePickerIF.P_AND_S);
              if (!saveS && debug) saveSmsg = String.format("DEBUG %s eventActive saveS? (phase flag OK): %s%n", name, saveS);
              saveS &= sameTime || ( (sta.pick.time >= pTime) && (sDelta <= pDelta) );
              if (!saveS && debug) saveSmsg = String.format("DEBUG %s eventActive saveS? (pick>=pTime & sDelta<=pDelta): %s%n", name, saveS);

              if (saveP) {
                      Phase ph = makePhase(sta, "P");
                      if (debug) System.out.printf(" INFO %s pick P: %s%n", name, ph.toNeatString());
                      if (ph != null) {
                          phList.add(ph);
                          sta.pick.status = 3; // pick reported, Phase saved
                          sta.haveP = true;
                      }
                      saveS = false;
              }
              else if (debug) System.out.print(savePmsg);

              if (saveS) {
                      if (sDownWeightByOne) sta.pick.weight++;
                      Phase ph = makePhase(sta, "S");
                      if (debug) System.out.printf(" INFO %s pick S: %s%n", name, ph.toNeatString());
                      if (ph != null) {
                          phList.add(ph);
                          sta.pick.status = 3; // pick reported, Phase saved 
                          sta.haveS = true;
                      }
                      saveP = false;
              }
              else if (debug) System.out.print(saveSmsg);

           } // END CURRENT PICK EVENT CALCULATION 

           if (sta.pick.status == 3) statusStr = "Pick complete, saved.";
           else if (sta.pick.status == 2) statusStr = "Pick complete, but skipped.";
           else statusStr = "Pick still active.";

           // Is the current pick event over? If so, bail and scan segment for a new pick event
           if ( (sta.pick.status > 1) ) { 
               break;
           }

        } // end of while loop

        if ( debug) {
           if (debug) statusStr += " phaseFlag:" + phaseFlag;
           System.out.printf( " INFO %s %s%n", name, statusStr);
        }

        // Probably here, because of insufficient z-crossing count in while loop above
        return sta.pick.status;  // current pick event is still active

    } // End of eventActive

    /*
    # Earthworm 7 pick_ew.c format:
    #                               MinBigZC       RawDataFilt     LtaFilt         DeadSta          PreEvent
    #Pick   Pin  SNCL          MinSmallZC     MaxMint          StaFilt        RmavFilt           AltCoda
    #            Sta   Cmp Nt L Itr1   MinPeakSize     CharFuncFilt     EvtThresh           CodaTerm        Erefs
    #  --------------------------------------------------------------------------------------
    #   1   503  14405 HNZ CE -- 3  40  3  20  500  0  .985  3.  .4  .015 5.  .9961  1200.  49.14  .8  1.5  50000.
    */
    public PickerParmIF parseChannelParm(String parmLineStr) { 

        //if (isComment(parmLineStr) return null;

        Parm parm = new Parm();
        boolean status = true;

        int ifield = 1;

        try {

                Scanner scanner = new Scanner(parmLineStr);

                // NEW Jiggle input record Format:
                parm.sncl = scanner.next(); // 1st field

                ifield++;
                parm.PickFlag = scanner.nextInt();

                ifield++;
                parm.Itr1 = scanner.nextInt();

                ifield++;
                parm.MinSmallZC = scanner.nextInt();

                ifield++;
                parm.MinBigZC = scanner.nextInt();

                ifield++;
                parm.MinPeakSize = scanner.nextInt();

                ifield++;
                parm.MaxMint = scanner.nextInt();

                ifield++;
                parm.RawDataFilt = scanner.nextDouble();

                ifield++;
                parm.CharFuncFilt = scanner.nextDouble();

                ifield++;
                parm.StaFilt = scanner.nextDouble();

                ifield++;
                parm.LtaFilt = scanner.nextDouble();

                ifield++;
                parm.EventThresh = scanner.nextDouble();

                ifield++;
                parm.RmavFilt = scanner.nextDouble();

                //ifield++;
                //parm.DeadSta = scanner.nextDouble();

                //ifield++;
                //parm.Erefs = scanner.nextDouble();


                scanner.close();

        }
        catch(InputMismatchException ex) {
            System.err.println(ex.getMessage());
            reportDataFieldError(ifield);
            status = false;
        }
        catch(NoSuchElementException ex) {
            System.err.println(ex.getMessage());
            reportDataFieldError(ifield);
            status = false;
        }
        catch(IllegalStateException ex) {
            System.err.println(ex.getMessage());
            reportDataFieldError(ifield);
            status = false;
        }
        catch(Exception ex) {
            ex.printStackTrace();
            reportDataFieldError(ifield);
            status = false;
        }

        return (status) ? parm : null;
    }

    private void reportDataFieldError(int ifield) {
        System.out.printf("Error %s parseChannelParm parsing input line at field: %d%n", name, ifield);
    }

    public JComponent getPropertyEditor(GenericPropertyList gpl) {
        return new org.trinet.jiggle.DPpicker(gpl);
    }
    
    public JComponent getPropertyEditor(AutoPickGeneratorIF apg, GenericPropertyList gpl) {
        return new org.trinet.jiggle.DPpicker(apg, gpl);
    }
    
    public JComponent getChannelParmEditor(String sncl) {
        Parm parm = null;
        if (sncl != null && chanParmMap != null) parm = (Parm) chanParmMap.get(sncl);
        if (sncl == null || parm == null) {
            parm = (Parm) DefaultParm.clone();
        }
        if (sncl != null) {
            parm.sncl = sncl;
        }

        JComponent jc = null;
        try {
          jc = new ParmEditorPanel(parm, (sncl == null)); // if input sncl null, assume edit of DefaultParm
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return jc;
    }
       
    private class ParmEditorPanel extends JPanel implements PickerParmEditorIF {

        private DataFieldEditor pickFlagEditor = null;
        private DataFieldEditor itr1Editor = null;
        private DataFieldEditor minSmallZCEditor = null;
        private DataFieldEditor minBigZCEditor = null;
        private DataFieldEditor minPeakSizeEditor = null;
        private DataFieldEditor maxMintEditor = null;
        private DataFieldEditor rawDataFiltEditor = null;
        private DataFieldEditor charFuncFiltEditor = null;
        private DataFieldEditor staFiltEditor = null;
        private DataFieldEditor ltaFiltEditor = null;
        private DataFieldEditor eventThreshEditor = null;
        private DataFieldEditor rmavFiltEditor = null;
        //private DataFieldEditor deadStaEditor = null;
        //private DataFieldEditor erefsEditor = null;

        private Parm oldParm = null;
        private Parm newParm = null;

        private boolean isDefault = false;

        protected ParmEditorPanel(Parm parm, boolean isDefault) {

            newParm = parm;
            oldParm = (Parm) parm.clone();

            this.isDefault = isDefault;

            setLayout(new BorderLayout());
            Dimension prefSize = new Dimension(80,24);
            pickFlagEditor = makeDataEditor("PickFlag", "0= None, 1=P, 2=S, 3=Both", String.valueOf(parm.PickFlag), prefSize);
            itr1Editor = makeDataEditor("Itr1", "Small z-cross count scalar for pick termination", String.valueOf(parm.Itr1), prefSize);
            minSmallZCEditor = makeDataEditor("MinSmallZC", "Minimum number of small zero crossings", String.valueOf(parm.MinSmallZC), prefSize);
            minBigZCEditor = makeDataEditor("MinBigZC", "Minimum number of big zero crossings", String.valueOf(parm.MinBigZC), prefSize);
            minPeakSizeEditor = makeDataEditor("MinPeakSize", "Minimum size of 1'st three peaks", String.valueOf(parm.MinPeakSize), prefSize);
            maxMintEditor = makeDataEditor("MaxMint", "Max interval between zero crossings in samples", String.valueOf(parm.MaxMint), prefSize);
            rawDataFiltEditor = makeDataEditor("RawDataFilt", "Filter parameter for raw data", String.valueOf(parm.RawDataFilt), prefSize);
            charFuncFiltEditor = makeDataEditor("CharFuncFilt", "parameter for characteristic function", String.valueOf(parm.CharFuncFilt), prefSize);
            staFiltEditor = makeDataEditor("StaFilt", "parameter for short-term average", String.valueOf(parm.StaFilt), prefSize);
            ltaFiltEditor = makeDataEditor("LtaFilt", "Filter parameter for long-term average", String.valueOf(parm.LtaFilt), prefSize);
            eventThreshEditor = makeDataEditor("EventThresh", "STA/LTA event threshold", String.valueOf(parm.EventThresh), prefSize);
            rmavFiltEditor = makeDataEditor("RmavFilt", "Filter parameter for running mean absolute value", String.valueOf(parm.RmavFilt), prefSize);
            //deadStaEditor = makeDataEditor("DeadSta", "Dead station threshold", String.valueOf(parm.DeadSta), prefSize);
            //erefsEditor =  makeDataEditor("Erefs",   "Event termination parameter", String.valueOf(parm.Erefs), prefSize);

            Box vbox = Box.createVerticalBox();
            vbox.setBorder(BorderFactory.createTitledBorder((newParm.sncl == null) ? "Default" : newParm.sncl));
            vbox.add( pickFlagEditor.getEditorComponent() );
            vbox.add( itr1Editor.getEditorComponent() );
            vbox.add( minSmallZCEditor.getEditorComponent() );
            vbox.add( minBigZCEditor.getEditorComponent() );
            vbox.add( minPeakSizeEditor.getEditorComponent() );
            vbox.add( maxMintEditor.getEditorComponent() );
            vbox.add( rawDataFiltEditor.getEditorComponent() );
            vbox.add( charFuncFiltEditor.getEditorComponent() );
            vbox.add( staFiltEditor.getEditorComponent() );
            vbox.add( ltaFiltEditor.getEditorComponent() );
            vbox.add( eventThreshEditor.getEditorComponent() );
            vbox.add( rmavFiltEditor.getEditorComponent() );
            //vbox.add( deadStaEditor.getEditorComponent() );
            //vbox.add( erefsEditor.getEditorComponent() );

            // Picker properties action buttons
            ActionListener al =  new EditorButtonActionListener();

            JButton setPropsButton = new JButton("Set");
            setPropsButton.addActionListener(al);
            setPropsButton.setToolTipText("Set channel's picking parms to current text field values.");

            JButton cancelPropsButton = new JButton("Reset");
            cancelPropsButton.addActionListener(al); 
            cancelPropsButton.setToolTipText("Reset channel's picking parms to original input values.");

            JButton deletePropsButton = new JButton("Delete");
            deletePropsButton.addActionListener(al); 
            deletePropsButton.setToolTipText("Delete this channel's picking parms from cache.");
            deletePropsButton.setEnabled(!isDefault);

            Box pickerButtonBox = Box.createHorizontalBox();
            pickerButtonBox.add(Box.createHorizontalGlue());
            pickerButtonBox.add(setPropsButton);
            pickerButtonBox.add(cancelPropsButton);
            pickerButtonBox.add(deletePropsButton);
            pickerButtonBox.add(Box.createHorizontalGlue());

            // Parm label, field boxes
            add(vbox, BorderLayout.CENTER);

            // Control buttons at bottom
            add(pickerButtonBox, BorderLayout.SOUTH);

        }

        private DataFieldEditor makeDataEditor(String label, String tip, String value, Dimension prefSize) {
            return new DataFieldEditor(label, tip, value, prefSize);
        }

        private class EditorButtonActionListener implements ActionListener {

            public void actionPerformed(ActionEvent evt) {
                
                String cmd = evt.getActionCommand();

                try {
                  if (cmd == "Set") {
                      if (newParm != null) {
                          setParmValues(newParm);
                          if (isDefault) {
                              DefaultParm.copy(newParm); 
                          }
                          else addChannelParm(newParm.sncl, newParm); //creates new map if null and does chanParmMap.put(newParm.sncl, newParm);
                      }
                  }
                  else if (cmd == "Reset") {
                      if (oldParm != null) {
                          setEditorValues(oldParm);
                          if (isDefault) {
                              DefaultParm.copy(oldParm);
                          }
                          else addChannelParm(oldParm.sncl, oldParm); //creates new map if null and does chanParmMap.put(oldParm.sncl, oldParm);
                      }
                  }
                  else if (cmd == "Delete") {
                      if (oldParm != null) {
                          int yn = JOptionPane.showConfirmDialog(ParmEditorPanel.this.getTopLevelAncestor(),
                                  "Delete " + oldParm.sncl + " from picker channel map?", "Delete Parm", JOptionPane.YES_NO_OPTION);
                          if (yn != JOptionPane.YES_OPTION) chanParmMap.remove(oldParm.sncl);
                      }
                  }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        public void setEditorValues(PickerParmIF p) {
            Parm parm = (Parm) p; 
            pickFlagEditor.setText(String.valueOf(parm.PickFlag));
            itr1Editor.setText(String.valueOf(parm.Itr1));
            minSmallZCEditor.setText(String.valueOf(parm.MinSmallZC));
            minBigZCEditor.setText(String.valueOf(parm.MinBigZC));
            minPeakSizeEditor.setText(String.valueOf(parm.MinPeakSize));
            maxMintEditor.setText(String.valueOf(parm.MaxMint));
            rawDataFiltEditor.setText(String.valueOf(parm.RawDataFilt));
            charFuncFiltEditor.setText(String.valueOf(parm.CharFuncFilt));
            staFiltEditor.setText(String.valueOf(parm.StaFilt));
            ltaFiltEditor.setText(String.valueOf(parm.LtaFilt));
            eventThreshEditor.setText(String.valueOf(parm.EventThresh));
            rmavFiltEditor.setText(String.valueOf(parm.RmavFilt));
            //deadStaEditor.setText(String.valueOf(parm.DeadSta));
            //erefsEditor.setText(String.valueOf(parm.Erefs));
        }

        public void setParmValues(PickerParmIF p) {
            Parm parm = (Parm) p; 
            parm.PickFlag = pickFlagEditor.getInt();
            parm.Itr1 = itr1Editor.getInt();
            parm.MinSmallZC = minSmallZCEditor.getInt();
            parm.MinBigZC = minBigZCEditor.getInt();
            parm.MinPeakSize = minPeakSizeEditor.getInt();
            parm.MaxMint = maxMintEditor.getInt();
            parm.RawDataFilt = rawDataFiltEditor.getDouble();
            parm.CharFuncFilt = charFuncFiltEditor.getDouble();
            parm.StaFilt = staFiltEditor.getDouble();
            parm.LtaFilt = ltaFiltEditor.getDouble();
            parm.EventThresh = eventThreshEditor.getDouble();
            parm.RmavFilt = rmavFiltEditor.getDouble();
            //parm.DeadSta = deadStaEditor.getDouble();
            //parm.Erefs = erefsEditor.getDouble();
        }

        private class DataFieldEditor {

            private JTextField jtf = null;
            private JLabel jlbl = null;
            private Box hbox = null;

            DataFieldEditor(String label, String tip, String value, Dimension prefSize) {

                jlbl = new JLabel(label);
                jlbl.setToolTipText(tip);
                if (prefSize != null) jlbl.setPreferredSize(prefSize);

                jtf = new JTextField();
                jtf.setText(value);
                jtf.setToolTipText(tip);
                if (prefSize != null) jtf.setPreferredSize(prefSize);

                hbox = Box.createHorizontalBox();
                hbox.add(jlbl);
                hbox.add(jtf);
                hbox.add(Box.createHorizontalGlue());

            }

            public Box getEditorComponent() {
                return hbox;
            }

            public String getText() {
                return jtf.getText();
            }

            public int getInt() {
                return Integer.parseInt(jtf.getText());
            }

            public double getDouble() {
                return Double.parseDouble(jtf.getText());
            }

            public void setText(String text) {
                jtf.setText(text);
            }

            public void setDouble(double ival) {
                jtf.setText(String.valueOf(ival));
            }

            public void setInt(int rval) {
                jtf.setText(String.valueOf(rval));
            }

        } // End of DataFieldEditor class

    } // End of ParmEditorPanel class

} // End of PickEW class 
