//
//TODO: Search code tree identifying all named properties, collect them
//into class(es) containing the property name defined by a static String
//constant which should be referenced in getProperty(name) instead of
//a literal string in the code.
//TODO: Remove location,mag calc engine stuff to a supporting class
//and use listeners to an event fired by this class to update Jiggle GUI
//similar to what's done in Mung prototype -aww
//
//
// Note: status panel interference by competing threads when starting up
//
package org.trinet.jiggle;

import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.io.*;
import java.net.MalformedURLException;
import java.util.*;

import javax.swing.*;
import javax.swing.event.*;

import org.trinet.jiggle.common.JiggleConstant;
import org.trinet.jiggle.common.JiggleSingleton;
import org.trinet.jiggle.common.LogUtil;
import org.trinet.jiggle.common.PropConstant;
import org.trinet.jiggle.common.type.OriginTypeList;
import org.trinet.jiggle.common.type.SolutionEvent;
import org.trinet.jiggle.map.*;

import org.trinet.jasi.*;
import org.trinet.jasi.engines.*;
import org.trinet.jasi.magmethods.*;
import org.trinet.jasi.picker.AbstractPicker;
import org.trinet.jasi.picker.AutoPickGeneratorIF;
import org.trinet.jasi.picker.PhasePickerIF;
import org.trinet.jasi.picker.PickerParmIF;
import org.trinet.jasi.seed.SeedReader;

import org.trinet.jdbc.datasources.*;

import org.trinet.jiggle.ui.common.SolutionChanged;
import org.trinet.jiggle.ui.common.SolutionListener;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;
import org.trinet.util.gazetteer.TN.GazetteerType;
import org.trinet.util.velocitymodel.UniformFlatLayerVelocityModel;
import org.trinet.util.graphics.*;
import org.trinet.util.graphics.text.*;
import org.trinet.util.graphics.table.*;
import org.trinet.util.locationengines.*;

import com.bbn.openmap.*;

import gov.usgs.swarm.wave.JiggleSwarmWaveViewerFrame;
import gov.usgs.swarm.wave.JiggleSwarmWaveClipboard;
import gov.usgs.util.Util;

/**
   The main Jiggle application. Jiggle is a graphical earthquake analysis tool.
   Because it is written in Java, Jiggle is platform independent.
   It has been run on Solaris, MS Windows, and FreeBSD.

    Its basic functions are:
<pre>
          Read/write of seismic data from/to any data source in any format.
          (Requires appropriate "jasi" implementation)
          Display user defined catalog list
          Display waveforms, phase picks, amplitudes, etc.
          Manipulation of waveforms (scaling, zooming, filtering, etc.)
          Editing and creation of phase picks, amplitudes, etc.
          Location calculation (using a remote location server)
          Magnitude calculation
</pre>
*/
public class Jiggle extends JFrame implements PropertyChangeListener, CacheRefreshListener, AutoPickGeneratorIF, WaveformDisplayIF, SolutionListener {
//
// static data members, change if needed
//

    protected org.trinet.util.SwingWorker pickWorker = null;

    public static String MyOS = "Windows";

    protected static boolean networkModeWAN = false;

    protected static long lastCacheRefreshTime = 0l;

    private static boolean versionCheckDisabled = false;
    private static boolean prefmagCheckDisabled = false;
    private static boolean duplicateCheckDisabled = false;
    private static boolean eventTypeCheckDisabled = false;
    private static boolean autoLoadAfterDelete = false;

    protected static boolean scopeMode = false; // or true for "scope" // added 2008/05/11 -aww
    protected static ScopeConfig scopeConfig= null;

    // debugging flag
    private static boolean debug = false;  // can set true via property
    //private static boolean dbTrace = false;  // can set true via property
    private static boolean cacheRefreshed = false;
    private static boolean cacheRefreshing = false;
    private static boolean committing = false; // set by commit thread
    private static boolean wfCache2FilePurged = true;

    // Set True to see lots of status & startup messages
    //private static boolean verbose = false;
    //
    private static String oldZoomValue = "";
    private static String solNextProcStateMask = "AHIFC?";
    private static String knownMagTypes = "";

    protected static final String versionNumber  = Version.buildTag;
    protected static final String authorName = "CISN";

    private static PrintStream logStream = null;
    private static PrintStream sysOut = System.out;
    private static PrintStream sysErr = System.err;
    private static String myLogFileName = null;

    /** Jiggle frame title */
    public static final String DEFAULT_FRAME_TITLE = "Jiggle  v" + versionNumber + " UTC";

    public static final String NEW_CP_PROPNAME = "newCatPane"; 
    public static final String NEW_MV_PROPNAME = "newMasterView"; 
    public static final String NEW_MV_SLIST_PROPNAME = "newMasterViewSolList"; 
    public static final String NEW_MV_WFVIEW_PROPNAME = "newMasterViewWFView"; 
    public static final String NEW_MAP_PROPNAME = "newMapFrame"; 
    public static final String NEW_EVENTPROPS_PROPNAME = "newEventSelectionProps"; 
    public static final String NEW_VMODEL_PROPNAME = "newVmodel"; 
    //public static final String NEW_PROPERTIES_PROPNAME = "newJiggleProps"; 

    /** Jiggle properties list */
    protected static JiggleProperties props = null;

    /** Name of the properties file. Path is assumed to be <user-home-dir>/.jiggle
        @see: PropertyList  */
    //protected static String OLD_PROPERTY_FILENAME = "properties";  // migrate to new new name, similar others, notepad edit
    protected static String DEFAULT_PROPERTY_FILENAME = "jiggle.props";  // jiggle.props, like for input arg?  -aww
    protected static String propertyFileName = DEFAULT_PROPERTY_FILENAME;

    /** Event properties */
    protected static EventSelectionProperties eventProps;

    /** The master view (note its static so there will only be one, ever.) */
    // Create an empty one to avoid null pointer exceptions before a valid one
    // is loaded with real data
    protected static MasterView mv = new MasterView();

    /** The list of solutions displayed in the Catalog Panel */
    protected static SolutionList catSolList = null;

    //private static SelectableReadingList selectedMagReadingList;
    private static HashMap prefMagSelectableReadingLists = new HashMap(11);
    private static JTabbedPane prefMagTabPane = null;
    protected static SelectableReadingList srlPhases = null;

    /** A solutionlock object that matches the current DataSource. Some data sources
     * may not support event locking. This can be checked with boolean result of
     * solLock.isSupported()  */
    protected static SolutionLock solLock = null;
    private long lastLoadedEvid = 0l;

    /** Time benchmark class */
    private static BenchMark bench1 = null;  // must be static to use in main(...)

    // Delegate solution solving to engine methods of helper class
    protected static JiggleSolutionSolverDelegate solSolverDelegate = null;

    protected static PhasePickerIF picker = null;

//
// GUI components
//

  /* put these here so I'd remember how to do it, for later use.
    private static int screenRes = getToolkit().getScreenResolution();
    private static Dimension screenSize = getToolkit().getScreenSize();
  */

    /** If true plot WFSegment boundaries in PickingPanel. Specified in
        properties */
    private static boolean showSegments = false;

    // StatusFrame constant values BEEP_OFF, BEEP_ON, BEEP_ON_OPEN, BEEP_ON_CLOSE
    protected static int beep = StatusFrame.BEEP_OFF;

    // MapFrame knows how to use the MapHandler to locate and place certain objects.
    protected static JiggleMapPanel mapPanel = null;
    private static Component mapFrame = null;
    private static PropertyHandler mapPropHandler = null;
    private static boolean mapWindowActiveFlag = false; 
    private static boolean haveCatSolList = false;

    // Need static instance reference to do some things
    public static Jiggle jiggle = null;

    /** Main tool bar */
    protected static JiggleToolBar toolBar = null;

    private static JTextClipboardPopupMenu msgTabPopup = null;
    private static JTextArea msgTextArea = null;
    private static SetupHelpAction helpAction  = new SetupHelpAction();

    //Swarm stuff here -aww
    private static Container swarmComponent = null;
    private static JFrame scJFrame = null;
    private static JiggleSwarmDataSource swarmDataSource = null;
    private static JiggleSwarmWaveViewerFrame swarmWaveViewFrame = null;
    private static JiggleSwarmWaveClipboard swarmClipboard = null;

    /** The main menu bar */
    protected static JiggleMenuBar menuBar = null;  // main menu bar

    /** Where status of loads is shown */
    protected static JiggleStatusBar statusBar = null;

    /** The status panel in the JiggleStatusBar. */
    // a subcomponent internal to JiggleStatusBar
    protected static StatusPanel statusPanel = null; // used to be private access aww

    // work status popup
    //private static WorkerStatusDialog statusFrame = null;
    private static StatusDialog statusFrame = null;

    //private JiggleFileChooser fileChooser = new JiggleFileChooser(this);

    //private static Object menuObject;  // possible action object passed from the Frame menu?
    // this menu is global so it can be disabled if no event is loaded.
    //private static JMenu eventMenu; // menuSort what about this one too?

    protected static JSplitPane   mainSplit = null;     // big splitpane
    protected static JSplitPane   wfSplit = null;       // waveform splitpane
    protected static JSplitPane   catMapSplit = null;   // catalog map splitpane
    protected static PickingPanel pickPanel = null;    //? aww  = new PickingPanel();  // zoom pane
    protected static WFScroller   wfScroller = null;

    private static int lastWfDividerLoc = -1;

    //* The panel containg the catalog table, a snapshot view of DataSource data.*/
    //protected static CatPane catPane = null;
    protected static CatalogPanel catPane = null;

// Text panels for the tabPane
    protected static JTabbedPane tabPane = null;

// Define all the tabs
    protected static final int TAB_LOCATION = 0;
    protected static final int TAB_MAGNITUDE= 1;
    protected static final int TAB_MESSAGE  = 2;
    protected static final int TAB_CATALOG  = 3;
    protected static final int TAB_WAVEFORM = 4;

    // Define tab titles
    private static final String tabTitle[] = {
                 "ORG",
                 "MAG",
                 "MSG",
                 "CAT",
                 "WFS"};

    private static final ImageIcon tabImage[] = new ImageIcon[5];
    static {
        Image image = IconImage.getImage("locate.gif");
        if (image != null) tabImage[0] = new ImageIcon(image);
        image = IconImage.getImage("mini-m-blk-script.gif");
        if (image != null) tabImage[1] = new ImageIcon(image);
        image = IconImage.getImage("file.gif");
        if (image != null) tabImage[2] = new ImageIcon(image);
        image = IconImage.getImage("mini-zagscroll.gif");
        if (image != null) tabImage[3] = new ImageIcon(image);
        image = IconImage.getImage("sinewave2.gif");
        if (image != null) tabImage[4] = new ImageIcon(image);
    }

    // Define tab tooltips
    private static final String tabTip[] = {
                 "Origin Location Data",
                 "Preferred Magnitude Data",
                 "Misc Text Messages",
                 "Catalog of Events",
                 "Event Waveforms"};

    /** Array of panels containing all the tab panes*/
    protected static JPanel tabPanel[] = new JPanel[tabTitle.length];

    /** The WHERE engine, returns info about close landmarks, etc.*/
    protected static WhereIsEngine whereEngine = null;

    private SolutionChanged solutionChangedListener;

///////////////// End of class data attribute member declarations ///////////////////////
//
//
////////////////////////// BEGIN METHOD DECLARATIONS //////////////////////////////////
//
/** Command line arguments:
 * 1 property file name in user directory (default = "jiggle.props"). */
    public static void main(String args[]) {
        try {
            if (args.length > 0) {
                if (args[0].compareToIgnoreCase("-version") == 0 ||
                    args[0].compareToIgnoreCase("--version") == 0) {
                    org.trinet.jasi.Version.printVersion();
                    return;
                }
            }
        } catch (Exception ex) {}
        System.out.println("*** Starting Jiggle v"+versionNumber + "***");
        // Need non-static instance to do some things
        // This reference to jiggle is NOT preserved in instance scope!
        jiggle = new Jiggle();
        jiggle.MyOS = System.getProperty("os.name");
        System.out.println("Running on os : " + jiggle.MyOS);
        if (args.length > 0) jiggle.propertyFileName = args[0]; // aww override default name
        jiggle.setup(args);
    }

    public Jiggle() {
        // Similar to how "jiggle" static var is used.
        JiggleSingleton.getInstance().setMainJFrame(this);
        this.solutionChangedListener = new SolutionChanged();
    }

    /** Pop a information dialog via the GUI event queue and return. */
    protected void popInfoFrame(String title, Object message) {
        final String tit = title;
        final Object msg = message;  // (message instanceof String) ? ((String)message) : message;
        final Runnable r =
            new Runnable() {
                public void run() {
                    JOptionPane.showMessageDialog(
                        Jiggle.this, msg, tit, JOptionPane.PLAIN_MESSAGE
                    );
                }
            };
        if ( SwingUtilities.isEventDispatchThread() ) r.run();
        else SwingUtilities.invokeLater(r);
    }

/**
 * Present the shutdown confirmation dialog box.
 * Called no matter which button, menu or widget is used to exit.
 */
    protected boolean confirmShutDown() {
        Solution sol = mv.getSelectedSolution();
        String msg = (sol != null && sol.getNeedsCommit()) ?
            "NOTE: Selected Waveform view Solution needs COMMIT" : "";
        //pop-up confirming  yes/no dialog:
        int yn = JOptionPane.showConfirmDialog(
                   Jiggle.this, "Are you sure you want to exit Jiggle?\n"+msg,
                   "Jiggle: Exit Confirmation",
                   JOptionPane.YES_NO_OPTION);

        return (yn == JOptionPane.YES_OPTION);

    }

/**
 * Save the current Properties, Frame location and size and exit the application.
 */
    public void stop() {
        System.out.println("INFO: Jiggle stop() at " + new DateTime());
        releaseAllSolutionLocks();
        JiggleProperties jp = getProperties();
        // Save properties by default, unless overridden by user declaration
        boolean saveProps = jp.isSavePropsOnExit();
        if (saveProps) {
            saveProperties();      // save user properties
        }
        // Also a menu/toolbar option to save the MasterChannelList to disk. 
        // WARNING:
        // 1)"older" channel data loaded via lookUp is written so output file
        // might contain "date" ranged Channel data for turned-off channels.
        // 2)If channel database tables are updated cache is stale, so it
        // should be deleted and rebuilt next time application is started.
        if (! jp.getBoolean("channelListCacheWrite")) disposeAndExit();
        else {
          final org.trinet.util.SwingWorker worker = new org.trinet.util.SwingWorker() {
              // NO GRAPHICS CALLS ALLOWED IN THIS METHOD UNLESS using SwingUtilites.invokeLater(new Runnable(...))!
              WorkerStatusDialog workStatus = new WorkerStatusDialog(Jiggle.this, true);
              boolean status = true;
              public Object construct() {
                  try {
                  workStatus.setBeep(beep);
                  String msg = "Writing channel cache to disk ...";
                  System.out.println(msg);
                  workStatus.pop("Save Channel Cache", msg, true);
                  ChannelList cList = MasterChannelList.get();
                  if (cList != null) status = cList.writeToCache();
                  }
                  catch (Exception ex) {
                      ex.printStackTrace();
                  }
                  return null;
              }

              //Runs on the event-dispatching thread graphics calls OK.
              public void finished() {
                  setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR)); // set back to normal
                  workStatus.dispose(); // don't invoke unpop before dispose (may cause exception)
                  if (statusPanel != null) statusPanel.clearOnlyOnMatch("Saving channel cache...");
                  if (status) Jiggle.this.disposeAndExit();
                  else {
                       if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(Jiggle.this,
                             "Cache write failed, continue to Exit?", "Exit Jiggle",
                             JOptionPane.YES_NO_OPTION) ) Jiggle.this.disposeAndExit();
                  }
              }
          };
          initStatusPanelForWork("Saving channel cache...");
          worker.start();  //required for SwingWorker 3
        }
    }

    private void disposeAndExit() {
        try {
          if (wfCache2FilePurged) {
            System.out.println("Disposing waveform file cache ...");
            AbstractWaveform.clearCacheDir(0l, null, null);
          }
          else if (AbstractWaveform.cache2File) {
            System.out.println("Writing waveform file cache ...");
            //toolBar.cacheWfButton.doClick();
            toolBar.writeWfCache(0);
          }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
        System.out.println("Disposing Jiggle frames ...");
        if (statusFrame != null) statusFrame.dispose();
        dispose(); // release window resources
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        Solution sol = mv.getSelectedSolution();
        long evid = (sol != null) ? sol.getId().longValue() : 0l;
        System.out.println(">>> Jiggle Exit ... last viewing evid: " + evid + " at " + new DateTime());
        closeLog(); // if non-null logStream flush, close, and null
        if (! DataSource.isClosed()) DataSource.close(); // close existing connection, if open
        System.exit(0);
    }
/**
 * Pop dialog asking if preferences should be saved.
 */
    protected void savePropertiesDialog() {
        final JiggleProperties jp = getProperties();
        // if not user property list or not saving properties on exit
        if (!(jp instanceof UserPropertyList) || !jp.isSavePropsOnExit()) {
            // pop confirming yes/no dialog:
            int answer = JOptionPane.showConfirmDialog(Jiggle.this,
                    "Save current property settings to a user disk file?", "Save Jiggle Properties?",
                    JOptionPane.YES_NO_OPTION);
            if (answer != JOptionPane.YES_OPTION)
                return;
        }

        SwingUtilities.invokeLater(
          new Runnable() {
            public void run() {

              if (jp instanceof UserPropertyList) {
                  saveProperties();
                  return;
              }
              File file = new File(jp.getUserPropertiesFileName());
              JFileChooser jfc = new JFileChooser(file);
              jfc.setSelectedFile(file);
              jfc.setDialogTitle("Save current Jiggle properties to user file");
              //jfc.setFileFilter(new MyFileFilter());
              //jfc.setFileSelectionMode(JFileChooser.FILES_ONLY); // forces save to be inside jiggle user dir -aww 2008/06/24
            
              if (jfc.showSaveDialog(Jiggle.this) == JFileChooser.APPROVE_OPTION) {
                File aFile = jfc.getSelectedFile();
                String filepath = aFile.getAbsolutePath();
                String filename = aFile.getName();

                /* see if file already exists, if so rename it before save
                if (aFile.exists()) {
                    aFile.renameTo(filepath+EpochTime.epochToString(System.currentTimeMillis()/1000.,"yyyy-MM-dd_HHmmss"));
                }
                */

                if ( jfc.getCurrentDirectory().getAbsolutePath().equals( jp.getUserFilePath()) ) {
                  jp.setFilename(filename);
                  saveProperties();
                }
                else { // save outside startup jiggle user dir
                  int answer = JOptionPane.showConfirmDialog(Jiggle.this,
                           "Save directory is NOT the same as startup JIGGLE_USER_DIR",
                           "Save Properties", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                  if (answer == JOptionPane.YES_OPTION) {
                      jp.setUserPropertiesFileName(filepath);
                      saveProperties();
                  }
                }

              }
            }
          } );
    }

    /**
     * Save all the environment's properties.
     */
    public void saveProperties() {
        // set properties that aren't set dynmically or by other dialogs
        JiggleProperties jp = getProperties();
        jp.put ("mainframeWidth",  String.valueOf(getSize().width));
        jp.put ("mainframeHeight",  String.valueOf(getSize().height));
        jp.put ("mainframeX",  String.valueOf(getLocation().x));
        jp.put ("mainframeY",  String.valueOf(getLocation().y));
        jp.saveProperties();
    }

    public JiggleSwarmWaveClipboard getSwarmClipboard() {
        return swarmClipboard;
    }

    public static JiggleProperties getProperties() {
        return props;
    }

    public static PropertyHandler getMapPropHandler() {
        return mapPropHandler;
    }

    public static boolean isNetworkModeLAN() {
        return ! networkModeWAN;
    }

    public boolean getVerbose() {
        return (getProperties().getBoolean("debug") || getProperties().getBoolean("verbose"));
    }

    public SolutionWfEditorPropertyList getWfPanelProperties() {
        return props;
    }

    public WFScroller getWFScroller() {
        return wfScroller;
    }

    public ZoomPanel getZoomPanel() {
        return pickPanel;
    }

    public MasterView getMasterView() {
        return mv;
    }

    private boolean initLogFromProperties() {

        // here logStream must be null or closed
        closeLog();

        StringBuffer sb = new StringBuffer(48);
        sb.append(props.getUserFilePath()).append(GenericPropertyList.FILE_SEP);
        sb.append("logs");

        File fd = new File(sb.toString());
        if (! fd.exists()) fd.mkdirs(); 
        sb.append(GenericPropertyList.FILE_SEP);
        //
        File [] files = fd.listFiles( new FilenameFilter() {
                                 String test = EnvironmentInfo.getApplicationName().toLowerCase()+"_.*log";
                                 public boolean accept(File dir, String name) {
                                     return name.matches(test);
                                 }
                              }
                         );
        if ( files.length > 0 ) {
          double maxAgeDays = getProperties().getDouble("maxLogAgeDays", 14.);
          double minModSecs = ((double)System.currentTimeMillis() - (86400000. * maxAgeDays))/1000.;
          double fileModSecs = 0.;
          boolean deleteMe = false;
          boolean autoDelete = getProperties().getBoolean("autoDeleteOldLogs");
          if (! autoDelete ) {
              int yn = JOptionPane.showConfirmDialog(Jiggle.this, "Delete " + files.length + " logs older than " + maxAgeDays + " days?",
                        "Jiggle Log Cleanup", JOptionPane.YES_NO_OPTION);
              autoDelete = (yn == JOptionPane.YES_OPTION);
          }
          for ( int idx=0; idx<files.length; idx++) {
              fileModSecs = (double)files[idx].lastModified()/1000.;
              if ( fileModSecs < minModSecs ) { 
                if (autoDelete) {
                  deleteMe = true;
                }
                else {
                  int yn = JOptionPane.showConfirmDialog(Jiggle.this, "Delete log? " + files[idx].getName() + " age>" + maxAgeDays + " days",
                        "Jiggle Logs", JOptionPane.YES_NO_OPTION);
                  deleteMe = (yn == JOptionPane.YES_OPTION);
                }
                if (deleteMe) {
                    if (files[idx].delete()) {
                      System.out.println("INFO: log file: " + files[idx].getName() + " last modified: " +
                                  EpochTime.epochToString(fileModSecs) + " deleted");
                    }
                    else {
                      JOptionPane.showMessageDialog(Jiggle.this, "Unable to delete file", "File Deletion Failed", JOptionPane.PLAIN_MESSAGE);
                    }
                }
              }
          }
        }

        String dateStr = new DateTime().toDateString("yyyy-MM-dd(HHmmss)");  // UTC time -aww 2008/02/10
        sb.append(EnvironmentInfo.getApplicationName().toLowerCase()).append("_");
        sb.append(dateStr);
        sb.append(".log");

        myLogFileName = sb.toString(); // save current stream name

        boolean status = false;
        try {
            logStream = new PrintStream(new FileOutputStream(myLogFileName));

            // Log to console instead of file for development. Prop's value is not used so just add to main prop file
            if (!props.isSpecified("debugConsoleLog")) {
                String str =
                        "INFO: Text message logging to file: " + myLogFileName + "\n      Logging STARTED at : " + dateStr;

                System.out.println(str); // notify user at terminal
                System.setOut(logStream);
                System.setErr(logStream);
                System.out.println(str + " JiggleVersion:" + versionNumber); // write first message to log file
            }

            status = true;
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (SecurityException ex) {
            ex.printStackTrace();
        }
        if (! status) {
          JOptionPane.showMessageDialog( Jiggle.this,
             "Exception creating log file, check your terminal window for messages!",
             "File Logging Enable Failed",
              JOptionPane.PLAIN_MESSAGE
          );
        }

        return status;
    }
 
    public PrintStream getLogStream() {
        return logStream;
    }
    public boolean isLogging() {
        return (logStream != null);
    }


    protected static void closeLog() {
        if (logStream != null) {
          String str = "INFO: Text message logging to file: " + myLogFileName + " JiggleVersion:" + versionNumber +
              "\n      Logging ENDED at : " + new DateTime().toDateString("yyyy-MM-dd(HHmmss)");  // UTC time -aww 2008/02/10
          System.out.println(str); // write last message to log file
          logStream.close(); // flushes
          logStream = null;
          // Set back to original terminal output streams
          System.setOut(sysOut);
          System.setErr(sysErr);
          System.out.println(str); // notify user at terminal
        }
    }

    public PhasePickerIF getPhasePicker() {
        return picker;
    }

    public void setPhasePicker(PhasePickerIF picker) {
        this.picker = picker;
    }

    public boolean setupPicker() {
        return setupPicker(getProperties());
    }

    public boolean setupPicker(GenericPropertyList props) {
        // picker.selected.name=PickEW
        // picker.class.PickEW=PickEW
        // picker.props.PickEW.props=pickerEW.props
        String pickerName = props.getProperty("picker.selected.name");
        if (pickerName == null) {
            System.out.println("Jiggle INFO: No autopicker class defined");
            return false;
        }
        String pickerClassName = props.getProperty("picker.class."+pickerName);
        //String pickerPropsFilename = props.getProperty("picker.props."+name);
        String pickerPropsFilename = props.getUserFileNameFromProperty("picker.props."+pickerName);
        System.out.println("Jiggle INFO: Picker " + pickerClassName + " loading properties from " + pickerPropsFilename);
        GenericPropertyList gpl = (pickerPropsFilename == null) ? props : new GenericPropertyList(pickerPropsFilename, (String)null);
        gpl.setFiletype("JIGGLE");
        return setupPicker(pickerClassName, gpl, true); 
    }

    public boolean setupPicker(String pickerClassName, GenericPropertyList props, boolean clearParameters) {

        if (pickerClassName == null) {
            System.err.println("Jiggle: setupPicker error, picker classname is not defined, check properties"); 
            return false;
        }

        // NOTE: make sure to set staParmsMap null after changing picker's sta properties file/data.
        if (clearParameters && picker != null) picker.clearChannelParms();

        boolean status = true;
        try {
          picker = (PhasePickerIF) Class.forName(pickerClassName).newInstance(); 
          picker.setProperties(props);
          //if (picker != null) picker.loadPickerChannelParms(); // autoLoadChannel parms is a picker property setting, default is false auto load

        }
        catch (Exception ex) {
            status = false;
            System.err.println("Jiggle: Error creating instance of pickerClassName: " + pickerClassName); 
            ex.printStackTrace();
        }
        return status;
    }

    public void autoPick(int pickFlag) {

        if (picker == null) {
            if (!setupPicker()) {
                JOptionPane.showMessageDialog(Jiggle.this, "Error creating picker instance!", "Auto Phase Picker", JOptionPane.PLAIN_MESSAGE);
                return;
            }
        }
        final Solution sol = mv.getSelectedSolution();
        if (sol == null) { // check here to avoid null point exception
            String str = "No selected input solution for picker " + picker.getName();
            JOptionPane.showMessageDialog( Jiggle.this, str, "Auto Phase Picker", JOptionPane.PLAIN_MESSAGE);
            return;
        }

        boolean useLocation = true;
        if (sol.hasLatLonZ()) {
          if (sol.isDummy()) {
            int yn = JOptionPane.showConfirmDialog( Jiggle.this,
                         "Bogus event location? YES use it, NO, pick from start of timeseries, or CANCEL picking?", "Autopick",
                         JOptionPane.YES_NO_CANCEL_OPTION
                   );
             if (yn  == JOptionPane.CANCEL_OPTION || yn == JOptionPane.CLOSED_OPTION) {
                 return;
             }
             else if (yn == JOptionPane.NO_OPTION) {
                 useLocation = false;
             }
          }
        }
        else { // a null solution LatLonZ - aww 2011/08/02 
            useLocation = false;
            int yn = JOptionPane.showConfirmDialog( Jiggle.this,
                         "Null event location, edit it? YES, NO, pick from start of timeseries, or CANCEL picking?", "Autopick",
                         JOptionPane.YES_NO_CANCEL_OPTION
                   );
             if (yn  == JOptionPane.CANCEL_OPTION || yn == JOptionPane.CLOSED_OPTION) {
                 return;
             }
             else if (yn == JOptionPane.YES_OPTION) {
                 yn = JOptionPane.showConfirmDialog( Jiggle.this,
                         "Set location to selected zoom panel channel?", "Origin Location",
                         JOptionPane.YES_NO_CANCEL_OPTION
                 );
                 if (yn  == JOptionPane.YES_OPTION) {
                   LatLonZ llz = mv.masterWFViewModel.get().getChannelObj().getLatLonZ();
                   llz.setZ(0.);
                   sol.setLatLonZ(llz);
                 }
                 editEventParams(EventEditDialog.LLZTab);
             }
        }

        final int phasesToPickFlag = pickFlag;
        final boolean useEventLocation = useLocation;
        final PhaseList solPhList = sol.getPhaseList();

        // First delete existing autopicks, since picker's parms may not re-pick same phases
        System.out.printf("INFO: Jiggle AutoPick removed %d existing auto phases from list of %d%n",
                solPhList.eraseAllAutomatic(), solPhList.size());
        
        pickWorker = new org.trinet.util.SwingWorker() {
 

            int status = 0;
            int addCnt = 0;
            int pickCnt = 0;

            // Note: Not using solution's phaselist in construct() because add/removes to it cause events in SelectableReadingList
            // which invoke calcDistance sorts in EventQueue, but array changes by construct thread cause indexing exceptions sorting 
            // Instead in finished method update solution's phaselist in SelectableReadingList with new newSolPhList contents
            PhaseList newSolPhList = null; // Put picked phases into this list in worker thread

            WorkerStatusDialog workStatus = new WorkerStatusDialog(Jiggle.this, true);

            public Object construct() {

                try {

                    Thread myThread = Thread.currentThread();
                    myThread.setName("AutoPickerWorkerNew");

                    Thread[] tarray = new Thread[25];
                    int tcnt = 0;
                    tcnt = Thread.enumerate(tarray);
                    for (int ii=0; ii<tcnt; ii++) {
                        //System.out.println("AutoPickerSwingWorker Thread enumerate#" + ii + " name: " + tarray[ii].getName());
                        if (tarray[ii].getName().equals("AutoPickerWorkerWFLoad")) {
                            if (myThread != tarray[ii]) {
                                System.out.println("INFO: AutoPicker SwingWorker Interrupting executing thread named: " + tarray[ii].getName());
                                tarray[ii].interrupt();
                            }
                        }
                    }
                    myThread.setName("AutoPickerWorkerWFLoad");
                    //System.out.println("AutoPickerSwingWorker current thread name: " + myThread.getName());

                    workStatus.setBeep(1);
                    workStatus.pop("Auto Picker", "Loading channel picking parameters...", true);

                    // Lazy 1st time configuration of picker's station channel data initialize once unless changed
                    if (! picker.hasChannelParms()) {
                        if (! picker.loadChannelParms() ) {
                            status = -1;
                            return null;
                        } 
                    }

                    mv.wfvList.stopCacheManager(); // test here to see if fewer "bad" waveserver loads

                    if (useEventLocation && sol.hasLatLonZ()) pickEQ(sol, phasesToPickFlag);
                    else pickTrigger(sol, phasesToPickFlag);
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }

                return null;
            }

            void pickEQ(Solution sol, int phasesToPickFlag) {
                WFPanelList wfpList = wfScroller.groupPanel.wfpList;
                int panelCnt = wfpList.size();
                workStatus.setText("Picking phases for " +panelCnt+ " waveforms...");

                Waveform wf = null;
                WFPanel wfp = null;
                PhaseList phList = null;  
                newSolPhList = new PhaseList(solPhList); // should be only human, final picks here

                for (int idx = 0; idx < panelCnt; idx++) {

                  wfp = ((WFPanel)wfpList.get(idx));
                  //if (!wfp.isVisible()) continue; // ? do we want to skip hidden panels or not ?

                  Channel ch = wfp.wfv.getChannelObj(); // use view's since its distAz may be revised, but that of waveform distAz might not be current

                  wf = wfp.getRawWf(); // need raw counts, unfiltered here for picker ?
                  if (wf == null) {
                      if (picker.isDebug()) System.out.printf("Autopicking skipped, no waveform for %s%n", ch.toDelimitedSeedNameString("."));
                      continue; // no waveform in panel
                  }

                  int phFlag = phasesToPickFlag;
                  double chDist = ch.getHorizontalDistance();
                  double maxPdist = picker.getMaxDistance();
                  if (chDist > maxPdist) {
                      if (picker.isVerbose()) 
                          System.out.printf("Autopicking stopped, max distance exceeded: %6.1f Km %s%n", chDist, ch.toDelimitedSeedNameString("."));
                      break; // assumes panel list is distance sorted
                  }
                  if (phFlag == PhasePickerIF.DEFAULT) {
                      phFlag = getDefaultPickFlag(picker, ch);
                  }
                  if (phFlag == PhasePickerIF.NONE) {
                     if (picker.isDebug()) 
                         System.out.println("Autopicking NONE for channel: " + ch.toDelimitedSeedNameString("."));
                     continue;
                  }

                  Magnitude prefmag = sol.getPreferredMagnitude();
                  double mag = (prefmag == null || prefmag.hasNullValue() || prefmag.hasNullType()) ?
                         AbstractPicker.DEFAULT_NULL_MAG_VALUE : prefmag.getMagValue();
                  maxPdist = picker.getMaxDistance(mag);
                  if (chDist > maxPdist) {
                      if (picker.isVerbose()) 
                          System.out.printf("Autopicking stopped, max prefmag distance exceeded: %6.1f Km %s%n",
                                            chDist, ch.toDelimitedSeedNameString("."));
                      break; // assumes panel list is distance sorted
                  }
                  //
                  if (phFlag == PhasePickerIF.S_ONLY) {
                      double magSdist = maxPdist * picker.sMaxDistScalar();
                      if (chDist > magSdist) {
                          if (picker.isDebug()) 
                              System.out.printf("Autopicking of skipped, max prefmag S distance exceeded: %6.1f>%6.1f Km %s%n",
                                                   chDist, magSdist, ch.toDelimitedSeedNameString("."));
                          continue; // assumes panel list is distance sorted
                      }
                  }

                  if (phFlag == PhasePickerIF.P_ONLY && !picker.pOnH() && ch.isHorizontal()) {
                      if (picker.isDebug()) System.out.printf("Autopicking skipped, %s no P-picking on horz%n", ch.toDelimitedSeedNameString("."));
                      continue; // skip
                  }
                  else if (phFlag == PhasePickerIF.S_ONLY && !picker.sOnV() && ch.isVertical()) {
                      if (picker.isDebug()) System.out.printf("Autopicking skipped, %s no S-picking on vert%n", ch.toDelimitedSeedNameString("."));
                      continue; // skip
                  }

                  // For group picking only phases allowed by property flags
                  if (phFlag == PhasePickerIF.P_AND_S && !picker.pOnH() && ch.isHorizontal()) phFlag = PhasePickerIF.S_ONLY;
                  else if (phFlag == PhasePickerIF.P_AND_S && !picker.sOnV() && ch.isVertical()) phFlag = PhasePickerIF.P_ONLY;

                  phList = picker.pick(wf, sol, phFlag, true, false); // do velocity filter, but not distance filter (which is done above)
                  int cnt = phList.size();
                  if (cnt > 0) {
                      pickCnt += cnt;
                      addCnt += addOrReplaceAutoPicks(sol, phList, newSolPhList);
                  }
                  else if (picker.isDebug()) System.out.printf("INFO: Jiggle AutoPick NO picks for %s%n", ch.toDelimitedSeedNameString("."));

                } //end of for loop
            }

            private int getDefaultPickFlag(PhasePickerIF picker, Channel ch) {
                int flag = PhasePickerIF.NONE;
                PickerParmIF parm = picker.getChannelParm(ch.toDelimitedSeedNameString("."));
                if (parm != null) return parm.getPickFlag();
                else if (picker.isDebug()) {
                    System.out.println("Autopicking found no default parameters specified in parms file for " + 
                        ch.toDelimitedSeedNameString("."));
                }
                return flag;
            }

            void pickTrigger(Solution sol, int phasesToPickFlag) {
                WFPanelList wfpList = wfScroller.groupPanel.wfpList;
                int panelCnt = wfpList.size();
                workStatus.setText("Picking phases for " +panelCnt+ " waveforms...");

                Waveform wf = null;
                WFPanel wfp = null;
                PhaseList phList = null;  

                newSolPhList = new PhaseList(solPhList); // should be only human, final picks here

                for (int idx = 0; idx < panelCnt; idx++) {

                  wfp = ((WFPanel)wfpList.get(idx));
                  //if (!wfp.isVisible()) continue; // ? do we want to skip hidden panels or not ?

                  Channel ch = wfp.wfv.getChannelObj(); // use view's since its distAz may be revised, but that of waveform distAz might not be current

                  wf = wfp.getRawWf(); // need raw counts, unfiltered here for picker ?
                  if (wf == null) {
                      if (picker.isDebug()) System.out.printf("Autopicking skipped, no waveform for %s%n", ch.toDelimitedSeedNameString("."));
                      continue; // no waveform in panel
                  }

                  int phFlag = phasesToPickFlag;
                  if (phFlag == PhasePickerIF.P_ONLY && !picker.pOnH() && ch.isHorizontal()) {
                      if (picker.isDebug()) System.out.printf("Autopicking skipped, %s no P-picking on horz%n", ch.toDelimitedSeedNameString("."));
                      continue; // skip
                  }
                  else if (phFlag == PhasePickerIF.S_ONLY && !picker.sOnV() && ch.isVertical()) {
                      if (picker.isDebug()) System.out.printf("Autopicking skipped, %s no S-picking on vert%n", ch.toDelimitedSeedNameString("."));
                      continue; // skip
                  }

                  // For group picking only phases allowed by property flags
                  if (phFlag == PhasePickerIF.P_AND_S && !picker.pOnH() && ch.isHorizontal()) phFlag = PhasePickerIF.S_ONLY;
                  else if (phFlag == PhasePickerIF.P_AND_S && !picker.sOnV() && ch.isVertical()) phFlag = PhasePickerIF.P_ONLY;

                  phList = picker.pick(wf, phFlag, true); // do velocity filter, but not distance filter (which is done above)
                  int cnt = phList.size();
                  if (cnt > 0) {
                      pickCnt += cnt;
                      addCnt += addOrReplaceAutoPicks(sol, phList, newSolPhList);
                  }
                  else if (picker.isDebug()) System.out.printf("INFO: Jiggle AutoPick NO picks for %s%n", ch.toDelimitedSeedNameString("."));

                } //end of for loop
            }

            //Runs on the event-dispatching thread graphics calls OK.
            public void finished() {
                try {
                    setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR)); // set back to normal
                    workStatus.dispose();
                    if (statusPanel != null) statusPanel.clearOnlyOnMatch("Auto picking waveforms...");
                    if (status < 0) {
                        System.err.println("ERROR: Auto picking of scroller waveforms failed: " + picker.getName() );
                        String str = null;
                        if (status == -1) str = "Error loading station parameters for picker " + picker.getName();
                        else str = "Error picking with picker "  + picker.getName();
                        JOptionPane.showMessageDialog( Jiggle.this, str, "Auto Phase Picker", JOptionPane.PLAIN_MESSAGE);
                    }
                    int oldSize = solPhList.size();
                    solPhList.clear(false);
                    solPhList.fastAddAll(newSolPhList);
                    mv.updatePhaseLists(); // refreshes phases in views
                    updateStatusBarCounts();
                    selectTab(TAB_WAVEFORM);
                    System.out.printf("INFO: Jiggle AutoPick total counts (phases at start: %d, at end: %d) (autopicked: %d, added/replaced: %d)%n", 
                            oldSize, solPhList.size(), pickCnt, addCnt);
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
                finally {
                    pickWorker = null;
                }

            }
        };

        initStatusPanelForWork("Auto picking waveforms...");
        pickWorker.start();  //required for SwingWorker 3
    }

    private int addOrReplaceAutoPicks(Solution sol, PhaseList pickList, PhaseList newSolPhList) {
        int addCnt = 0;
        // Now for each picker phase, check for allowable replacement
        mv.clearPhaseUndoList(); // undo ?
        for (int ii = 0; ii < pickList.size(); ii++) {

             boolean doitFlag = true;
             Phase newPhase = (Phase) pickList.get(ii);
             newPhase.assign(sol);

             Phase oldPhase = null;

             PhaseList staPhList = (PhaseList) newSolPhList.getAllOfSameStaType(newPhase);  // P or S
             String reason = "";
             String addrep = "ADD";
             if (staPhList.size() > 0) { // if others from same station, check before add or replace
                 String type = ((String) newPhase.getTypeQualifier()).trim();
                 // Loop over matching station types
                 Channel oldChan = null;
                 Channel newChan = null;
                 addrep = "REPLACE";
                 for (int jj=0; jj<staPhList.size(); jj++) {

                     oldPhase = staPhList.getPhase(jj);
                     if (oldPhase.isDeleted()) continue; // skip over existing deleted, assume they are replaceable

                     // Check for AUTO versus NON-auto pick on same station, don't replace non-auto picks
                     if ( newPhase.isAuto() && !oldPhase.isAuto() ) {
                       reason = "Auto";
                       doitFlag = false;
                       break;
                     }


                     newChan = newPhase.getChannelObj();
                     oldChan = oldPhase.getChannelObj();
                     // Don't replace VEL with ACC picks
                     if ( newChan.isAcceleration() && oldChan.isVelocity() ) {
                       reason = "Vel";
                       doitFlag = false; 
                       break;
                     }
                     else if (newChan.isVertical()) {
                         if (type.equals("S")) { 
                            if (oldChan.isHorizontal()) { // don't replace horz S with vert S
                                reason = "SonV";
                                doitFlag = false;
                                break;
                            }
                         }
                     }
                     else if (newChan.isHorizontal()) {
                         if (type.equals("P")) {
                             if (oldChan.isVertical()) { // don't replace vert P with horz P
                                reason = "PonH";
                                 doitFlag = false;
                                 break;
                             }
                         }
                         else if (oldChan.isHorizontal()) { // don't replace existing Horizontal of higher weight
                             if (oldPhase.getQuality() > newPhase.getQuality()) {
                                 reason = "Qual";
                                 doitFlag = false;
                                 break;
                             }
                         }
                     }

                     // Check seedchan codes
                       String oldseed = oldPhase.getChannelObj().getSeedchan(); 
                       String newseed = newPhase.getChannelObj().getSeedchan(); 

                       /* Don't replace surface with downhole sensor
                       char orientOld = oldseed.charAt(2);
                       char orientNew = newseed.charAt(2);
                       // Kludge: need emplacement depth, no emplacement depth in ChannelTN, so assume a "number" is downhole
                       if ( Character.isDigit(orientNew) && !Character.isDigit(orientOld) ) {
                          reason = "downH";
                          doitFlag = false;
                          break;
                       }
                       */
                       if ( newChan.isBorehole() && !oldChan.isBorehole() ) {
                          reason = "downH";
                          doitFlag = false;
                          break;
                       }

                       // Check freq band
                       char rateOld = oldseed.charAt(0);
                       char rateNew = newseed.charAt(0);
                       // only replace HH with HH 
                       if (rateOld == 'H' && rateNew != 'H') {
                          reason = "HH~";
                          doitFlag = false;
                          break;
                       }
                       // only replace EH with HH, EH
                       else if (rateOld == 'E' && !(rateNew == 'H' || rateNew == 'E')) {
                          reason = "EH~";
                          doitFlag = false;
                          break;
                       }
                       // Only replace SH with HH, EH, SH
                       else if (rateOld == 'S' && !(rateNew == 'H' || rateNew =='E' || rateNew == 'S')) {
                          reason = "SH~";
                          doitFlag = false;
                          break;
                       }
                       // If same freq band, next check high vs low gain type
                       else if (rateOld == rateNew) {
                           char typeOld = oldseed.charAt(1);
                           char typeNew = newseed.charAt(1);
                           // Only replace HH,EH,SH with like HH, EH, SH, skip low gain G L N  type
                           if (typeOld == 'H' && typeNew != 'H') {
                              reason = "H2L";
                              doitFlag = false;
                              break;
                           }
                       }

                     // Log quality check, else we won't know if the picker parms changes made poorer pick than old one
                     if ( newPhase.getQuality() < oldPhase.getQuality() ) {
                           System.out.printf("INFO: Jiggle AutoPick quality %s new %3.2f < old %3.2f %s%n",
                               newChan.toDelimitedSeedNameString("."), newPhase.getQuality(),
                               oldPhase.getQuality(), oldChan.toDelimitedSeedNameString(".") // bugfix replace newChan with oldChan ref -aww 2011/10/06
                           );
                       //doitFlag = false; //don't replace lower quality ?
                       //break; 
                     }

                 } // end of loop over matching existing sta picks

             } // end of matching existing sta pick checking block

             if (doitFlag) {
                 if (picker.isDebug()) {
                     System.out.printf("INFO: Jiggle AutoPick new phase %s %s%n", addrep,
                         newPhase.getChannelObj().toDelimitedSeedNameString("." ));
                     System.out.printf(" new: %s%n", newPhase.toNeatString());
                 }
                 mv.addToPhaseUndoList(staPhList, false); // undo ?
                 newSolPhList.addOrReplaceAllOfSameStaType(newPhase, false); // onlyAuto=false, ok since we checked auto flag above
                 addCnt++;
             }
             else {
                 System.out.printf("INFO: Jiggle AutoPick new phase SKIPPED (%s) %s%n", reason,
                         newPhase.getChannelObj().toDelimitedSeedNameString("." ));
                 if (picker.isDebug()) {
                     System.out.printf(" new: %s%n old: %s%n", newPhase.toNeatString(), oldPhase.toNeatString());
                 }
             }
        } // end of loop over all autopicks made
        return addCnt;
    } // end of addOrReplace method

    public void loadPickerChannelParms() {
        if (picker == null) return;
        // Lazy 1st time configuration of picker's station channel data initialize once unless changed
        if (picker.hasChannelParms()) return;
        // NOTE: make sure to set staParmsMap null after changing picker's sta properties file/data.
        final org.trinet.util.SwingWorker worker = new org.trinet.util.SwingWorker() {

                boolean status = false;
                WorkerStatusDialog workStatus = new WorkerStatusDialog(Jiggle.this, true);

                public Object construct() {
                    workStatus.setBeep(1);
                    workStatus.pop("Auto Picker", "Loading channel picking parameters...", true);
                    status = picker.loadChannelParms();
                    return null;
                }

                //Runs on the event-dispatching thread graphics calls OK.
                public void finished() {
                    setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR)); // set back to normal
                    workStatus.dispose();
                    if (statusPanel != null) statusPanel.clearOnlyOnMatch("Loading picker channel parms...");

                    if (!status) {
                        System.err.println("ERROR: Picking Panel unable to load station parameters for picker: " + picker.getName() );
                        JOptionPane.showMessageDialog( Jiggle.this,
                            "Error loading channel picking parameters for " + picker.getName(),
                            "Auto Phase Picker",
                            JOptionPane.PLAIN_MESSAGE
                        );
                    }
                }
        };
        initStatusPanelForWork("Loading picker channel parms...");
        worker.start();  //required for SwingWorker 3
    }
 
    /** Here the GUI and components are set up */
    public void setup(String [] args) {

        // Declare application id for logs and use by JasiPropertyList subclasses for subdir location
        EnvironmentInfo.setApplicationName("Jiggle");

        //Kludge here: for popup feedback when funky waveserver spec or java.net.ConnectException ...
        //when WaveServerGroup property is parsed inside the SolutionWfProperty class via jp.initialize
        //Setting is now configured via boolean property "waveServerPopupOnAddServerError"
        //WaveServerGroup.doPopupOnAddServerError = true;

        // Read and setup properties
        System.out.println("Jiggle property filename: " + propertyFileName);
        JiggleProperties jp = UserPropertyList.createJiggleProperties(null);
        jp.setFilename(propertyFileName);

        // change default name -aww
        jp.setDefaultPropertiesFileName( jp.getDefaultFilePath() + GenericPropertyList.FILE_SEP + DEFAULT_PROPERTY_FILENAME);

        boolean status = true;
        if ( jp.hasDefaultPropertiesFile() ) {
            status = jp.initialize("jiggle", propertyFileName, DEFAULT_PROPERTY_FILENAME);
        }
        else { // no default props file exists
            status = jp.initialize("jiggle", propertyFileName, null);
        }

        if ( ! status) {
          String str = getPropertyFileStatusFor(jp, "Are these files OK?  Enter 'YES' to continue.");
          System.err.println(str);
          int yn = JOptionPane.showConfirmDialog( Jiggle.this,
                         str, "JiggleProperties Initialization",
                         JOptionPane.YES_NO_OPTION
                   );
           if (yn != JOptionPane.YES_OPTION) System.exit(0); // bail
        }
        //
        if (args.length > 1) {
            for (int idx = 1; idx < args.length; idx++) {
                if (! jp.readPropertiesFile(args[idx])) {
                    String str = "Error: unable to load command line properties file: " + args[idx] + "\nDo you wish to continue?";
                    System.err.println(str);
                    int yn = JOptionPane.showConfirmDialog( Jiggle.this,
                         str, "JiggleProperties Initialization",
                         JOptionPane.YES_NO_OPTION
                    );
                    if (yn != JOptionPane.YES_OPTION) System.exit(0); // bail
                }
                else { // success, so reconfigure properties object after read
                    System.out.println("INFO: Loading properties from: " + args[idx]);
                    jp.setup();
                }
            } // end of loop over args
        }
        // Note: DataSource connection is not yet configured, so AbstractWaveform source is set null
        setProperties(jp);
        setMasterViewProperties();  // e.g. current channelTimeWindowModel name for status bar label update - aww 2010/07/27
        setupPicker(); // added 2010/10/15 -aww

        if (debug) { // debug
          jp.dumpProperties();

          java.util.List aList = jp.getKnownPropertyNames();
          System.out.println(Lines.MINUS_TEXT_LINE);
          System.out.println("DEBUG: JiggleProperties property names KNOWN:\n" + aList);
          System.out.println(Lines.ANGLE_LR_TEXT_LINE);

          aList = jp.getUnknownPropertyNames();
          if (aList.size() > 0) {
              System.out.println("DEBUG: JiggleProperties property names UNRECOGNIZED:\n" + aList);
              System.out.println(Lines.ANGLE_LR_TEXT_LINE);
          }
          aList = jp.getUndefinedPropertyNames();
          if (aList.size() > 0) {
              System.out.println("DEBUG: JiggleProperties property names UNDEFINED:\n" + aList);
              System.out.println(Lines.ANGLE_RL_TEXT_LINE);
          }
        }

        if (getVerbose()) {
          System.out.println(Lines.STAR_TEXT_LINE);
          System.out.println(DEFAULT_FRAME_TITLE + " loaded properties, finishing setup...");
          bench1 = new BenchMark(" * Elapsed time since program start: ");

          System.out.println("Jiggle TravelTime model: " + TravelTime.getInstance().toString());
        }

        if (debug) EnvironmentInfo.dump();


        // set system icon image
        setLogo();

        // try to create a global connection
        boolean connected = makeDataSourceConnection();
        if (! connected) {
          System.out.println("Unable to connect to specified data store, it is unavailable");
        }

        // NEED init log here since DataSource is required to init LeapSeconds: see log init note in setProperties(props,boolean) method
        // Option to redirect STDOUT/STDERR to log file in user's application home directory - aww 06/22/2005 
        if ( ! props.getBoolean("fileMessageLogging") ) {
            closeLog();
            System.out.println("INFO: No text message logging to file; Logging is OFF");
        }
        // Note below doesn't reset to new log if reloaded properties have changed log file spec
        else if ( logStream == null) initLogFromProperties(); 

        // code to check if user is authorized to run application (via database security table) ?

        // create the main frame toolbar, menus, and subpanels
        makeGUI();

        //register solution change listener
        jiggle.getSolutionChangedListener().addSolutionChangedListener(this);

        // ChannelList may be loaded from a serialized cache or a db store, expects main GUI display
        if (! loadChannelList() ) {
            LogUtil.warning("Jiggle could not load Channel list.");
        }

        // setup the where, location, magnitude engines, etc. they use channelList set above,
        // init does the MagMethod types mapping
        initEngines();

        if (scopeMode) {
            if (toolBar != null) toolBar.scopeModeButton.doClick();
        }
        if (getVerbose()) bench1.print();      // timestamp

        if (jp instanceof UserPropertyList) {
            // store to user properties
            ((UserPropertyList) jp).storeUserProperties();
        }
    } // end of setup()

    private void setLogo() {
        String fileName = "JiggleLogo.gif";
        //IconImage.setDebug(true);
        Image image = IconImage.getImage(fileName);
        if (image != null) this.setIconImage(image);
    }

    public void setDebug(boolean tf) {
        debug = tf;
    }

    public void setProperties(JiggleProperties properties) {
        setProperties(properties, true);
    }

    // Added private method - aww 2008/10/19
    private void setProperties(JiggleProperties properties, boolean resetEventSelectionProps) {
        JigglePropertiesUtils.initProperties(properties);
        JigglePropertiesUtils.validate(properties);

        props = properties;
        JiggleSingleton.getInstance().setJiggleProp(props);
        JiggleAuthenticator.setup(props);

        // check version
        versionCheckDisabled = props.getBoolean("versionCheckDisabled");

        checkVersion(); // using "webSite" override if any -aww 2008/09/12

        // NOTE: db connection must exist to intialize LeapSeconds, so do not do log init here (requires DateTime) when setProperties(props) is called!!!
        if ( ! resetEventSelectionProps) { // kludge here to have a user's preferences dialog edit reset file logging -aww 2015/11/15
            // note assume a "preferences" reset, so do this re init log (assumes LeapSeconds init'd from DataSource in setup() call)
            // Option to redirect STDOUT/STDERR to log file in user's application home directory
            if ( ! props.getBoolean("fileMessageLogging") ) {
                closeLog();
                System.out.println("INFO: No text message logging to file; Logging is OFF");
            }
            // below doesn't reset to new log if reloaded properties have changed log file spec
            else if ( logStream == null) initLogFromProperties(); 
        }

        // set class static elements:
        beep = props.getInt("beep"); // audible option popups - aww

        debug = props.getBoolean("debug");
        // verbose = (debug || props.getBoolean("verbose"));

        scopeMode = props.getBoolean("scopeMode");

        // default network mode is a datacenter LAN, not remote WAN access
        if (props.isSpecified("networkModeWAN"))
            networkModeWAN = props.getBoolean("networkModeWAN", true);

        prefmagCheckDisabled = props.getBoolean("prefmagCheckDisabled");
        duplicateCheckDisabled = props.getBoolean("duplicateCheckDisabled");
        eventTypeCheckDisabled = props.getBoolean("eventTypeCheckDisabled");
        autoLoadAfterDelete = props.getBoolean("autoLoadAfterDelete");

        // Temporary option until test code for new Waveform structure proven
        int wfType = props.getInt("defaultWfType");
        if (wfType == AbstractWaveform.SINGLE || wfType == AbstractWaveform.GROUP) {
             AbstractWaveform.defaultType = wfType;
        }
        if (debug) System.out.println("DEBUG FYI: AbstractWaveform defaultType = " + AbstractWaveform.defaultType);

        // Multiples of the S-P time to add to the P-time for end time of the energy scan window
        if ( props.isSpecified("wfSmPWindowMultiplier") ) {
          double d = props.getDouble("wfSmPWindowMultiplier");
          // Force to be at least 1.5
          if (d >= 1.5) AbstractWaveform.smpWindowMultiplier = d;
        }

        if ( props.isSpecified("maxWaveletCount") ) 
          WaveletGroup.MAXCNT = props.getInt("maxWaveletCount");

        if ( props.isSpecified("phasePopupMenuFlat") ) 
           PhasePopup.menuLayoutFlat = props.getBoolean("phasePopupMenuFlat");

        if ( props.isSpecified("confirmWFPanelDeleteAction") ) 
           ActiveWFPanel.confirmActionPopup = props.getBoolean("confirmWFPanelDeleteAction");

        if ( props.isSpecified("wfpPopupMenuFlat") ) 
           ActiveWFPanel.nestedStripMenus = ! props.getBoolean("wfpPopupMenuFlat");

        //if ( props.isSpecified("wfSplitDividerLoc") ) 
        //   lastWfDividerLoc = props.getInt("wfSplitDividerLoc");

        // Overrides properties file set, Jiggle user in GUI has last word on setting preferred magnitude
        Solution.setPrefMagByRulesOnCommit(false);

        if ( props.isSpecified("solNextProcStateMask") )
           solNextProcStateMask = props.getProperty("solNextProcStateMask", "AHIFC?");

        if (props.getProperty("pickFlagFont","").equalsIgnoreCase("BIG")) PickFlag.setBigFont(); // -aww 2008/02/09
        else PickFlag.setSmallFont();

        // Temporary here until all RSN convert to dashes -aww 2011/02/24
        if (props.isSpecified("locationCodeSpace2Dash")) {
            HypoFormat.locationCodeSpace2Dash = props.getBoolean("locationCodeSpace2Dash");
        }

        if (props.isSpecified("wfCache2File")) {
            AbstractWaveform.cache2File = props.getBoolean("wfCache2File"); // set true for segment serial to disk file cache
            if (toolBar != null) toolBar.cacheWfButton.setEnabled(AbstractWaveform.cache2File); 
        }

        if (props.isSpecified("wfCache2FileDir")) 
            AbstractWaveform.cache2FileDir = props.getUserFileNameFromProperty("wfCache2FileDir");

        if (props.isSpecified("wfCache2FilePurged")) 
            wfCache2FilePurged = props.getBoolean("wfCache2FilePurged");

        //if (props.isSpecified("loadCatalogPrefMags")) // Undocumented property, not configurable via GUI
        //    Solution.loadCatalogPrefMags = props.getBoolean("loadCatalogPrefMags"); // to be used with MD ML catalog columns
        setLoadCatalogPrefMags( props.getProperty("catalogColumnList","") );

        if (debug) {
          // Should debug return completeString from toDelimitedNameString() -aww
          //if (isSpecified("debugFullChannelName") )
          //  ChannelName.useFullName = getBoolean("debugFullChannelName");
          ChannelName.useFullName = true;
          // Temporarily dump out values below until db is cleaned up
          System.out.println("Jiggle DEBUG channel matching properties set as follows:");
          System.out.println("      JasiChannelDbReader.defaultMatchMode = "+JasiChannelDbReader.defaultMatchMode);
          System.out.println("      ChannelName.useLoc                   = "+ChannelName.useLoc);
          System.out.println("      ChannelName.copyOnlySNCL             = "+ChannelName.copyOnlySNCL);
          System.out.println("      ChannelName.useFullName              = "+ChannelName.useFullName);
        }

        // configure event selection properties for catalog panel SolutionList initialization
        if (resetEventSelectionProps) setEventProperties();

        knownMagTypes = "";
        String [] mtypes = props.getMagMethodTypes();
        for (int i=0; i<mtypes.length; i++) knownMagTypes += mtypes[i]; 
        //knownMagTypes = knownMagTypes.toLowerCase();
        knownMagTypes = knownMagTypes.toLowerCase() + "mlr";

        if (solSolverDelegate != null) solSolverDelegate.setDebug(props.getBoolean("delegateDebug"));

        if (props.isSpecified("hypoinvWgt2DeltaTime")) Phase.DeltaTimes = props.getDoubleArray("hypoinvWgt2DeltaTime"); 
        if (props.isSpecified("defaultZoomFilterType")) ZoomPanel.defaultFilterType = props.getProperty("defaultZoomFilterType"); 
        if (props.isSpecified("defaultZoomFilterAlways")) ZoomPanel.defaultFilterAlways = props.getBoolean("defaultZoomFilterAlways"); 

        ZoomPanel.biasButtonOn = props.getBoolean("zoomBiasButtonOn", false);
        if (pickPanel != null) {
            pickPanel.biasButton.setSelected(ZoomPanel.biasButtonOn);
            pickPanel.alwaysResetLowQualFm = props.getBoolean("phasePopupMenu.alwaysResetLowQualFm", false);
            pickPanel.phaseDescWtCheck = props.getBoolean("phasePopupMenu.phaseDescWtCheck", true); 
            //System.out.println("<<<<<<  DEBUG Jiggle pickPanel.phaseDescWtCheck=" + pickPanel.phaseDescWtCheck); 
        }

        PickFlag.DEFAULT_UNUSED_PICK_COLOR = props.getUnusedPickColor();
        PickFlag.upColor = PickFlag.DEFAULT_UNUSED_PICK_COLOR;

        PickFlag.DEFAULT_DELTIM_COLOR = props.getDeltimLineColor();
        PickFlag.dtColor = PickFlag.DEFAULT_DELTIM_COLOR;

        checkProperties(); // report anomalies
    }

    public void setEventProperties() {
        setEventProperties( props.getEventSelectionProps() );
    }

    public void setEventProperties(String userPropFileName) {

        EventSelectionProperties evtProps = new EventSelectionProperties();
        //Note: use EventSelectionProperties.DEFAULT_FILENAME for user if not defined
        if (userPropFileName == null) userPropFileName = EventSelectionProperties.DEFAULT_FILENAME;

        String defaultPropFileName = props.getEventSelectionDefaultProps(); // property recovers default name -aww
        // If value is absent or blank assume user doesn't want to load the defaults -aww
        //if (defaultPropFileName == null) defaultPropFileName = EventSelectionProperties.DEFAULT_FILENAME; // removed 2008/08/15 -aww

        //evtProps.initialize("jiggle", userPropFileName, EventSelectionProperties.DEFAULT_FILENAME);
        //aww - replaced above line with below defaultPropFileName 01/8/2005
        boolean status = evtProps.initialize("jiggle", userPropFileName, defaultPropFileName);
        if (! status) {
          String str = getPropertyFileStatusFor(evtProps, null);
          System.err.println(str);
          JOptionPane.showMessageDialog( Jiggle.this,
                                         str, "Event Selection Properties",
                                         JOptionPane.PLAIN_MESSAGE
                                       );
        }
        setEventProperties(evtProps);
    }

    /** Set Jiggle catalog event selection properties, and re-initialize. */
    public void setEventProperties(EventSelectionProperties properties) {
        Properties oldProps = eventProps;
        eventProps = properties;
        if (oldProps != properties) firePropertyChange(NEW_EVENTPROPS_PROPNAME, oldProps, properties);

        JiggleSingleton.getInstance().setEventProp(eventProps);

        if (debug) {
          eventProps.dumpProperties();
          java.util.List aList = eventProps.getKnownPropertyNames();
          if (aList.size() > 0) {
              System.out.println("DEBUG: EventProperties property names KNOWN:\n" + aList);
              System.out.println(Lines.ANGLE_LR_TEXT_LINE);
          }
          aList = eventProps.getUnknownPropertyNames();
          if (aList.size() > 0) {
              System.out.println("DEBUG: EventProperties property names UNRECOGNIZED:\n" + aList);
              System.out.println(Lines.ANGLE_LR_TEXT_LINE);
          }
          aList = eventProps.getUndefinedPropertyNames();
          if (aList.size() > 0) {
              System.out.println("DEBUG: EventProperties property names UNDEFINED:\n" + aList);
              System.out.println(Lines.ANGLE_RL_TEXT_LINE);
          }
        }
    }

    public EventSelectionProperties getEventProperties() {
        return eventProps;
    }

/** Handle display and processing of the Preferences dialog. */
     protected void doPreferencesDialog() {
        LocationServiceDescIF lsd = getProperties().getLocationServerGroup().getSelectedService();
        JLabel serviceLabel = new JLabel((lsd == null) ? "No Location Service" : lsd.toString());
        serviceLabel.setBackground(new Color(Integer.parseInt("40e0d0",16))); // turquoise
        serviceLabel.setOpaque(true);
        doPreferencesDialog(0);
     }

/** Handle display and processing of the Preferences dialog.
 *  Start with the given tab showing.
 *  @See: PreferencesDialog() */
    protected void doPreferencesDialog(final int tabNumber) {
        final org.trinet.util.SwingWorker worker = new org.trinet.util.SwingWorker() {
            // NO GRAPHICS CALLS ALLOWED IN THIS METHOD UNLESS using SwingUtilites.invokeLater(new Runnable(...))!
            WorkerStatusDialog workStatus = new WorkerStatusDialog(Jiggle.this, true);
            PreferencesDialog prefDialog = null;
            public Object construct() {
                workStatus.setBeep(beep);
                workStatus.pop("Preferences Dialog", "Configuring property tab panes...", true);
                prefDialog = new PreferencesDialog(Jiggle.this, tabNumber); // displays the dialog
                return prefDialog;
            }
            //Runs on the event-dispatching thread graphics calls OK.
            public void finished() {
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR)); // set back to normal
                //workStatus.dispose(); // disposal moved to below processPreferencesDialog -aww
                //prefDialog.setSize(new Dimension(624,600));

                prefDialog.setVisible(true);
                if (prefDialog.getButtonStatus() == JOptionPane.OK_OPTION) {
                    processPreferenceDialog(prefDialog, true); // this takes awhile
                    savePropertiesDialog();
                }
                if (statusPanel != null) statusPanel.clearOnlyOnMatch("Edit properties...");
                workStatus.dispose();
            }
        };
        initStatusPanelForWork("Edit properties...");
        worker.start();  //required for SwingWorker 3
    }

    private void doPreferencesDialog(int tabNumber, boolean refresh) {
        PreferencesDialog prefDialog = new PreferencesDialog(Jiggle.this, tabNumber); // displays the dialog
        prefDialog.setVisible(true);
        if (prefDialog.getButtonStatus() == JOptionPane.OK_OPTION) {
            processPreferenceDialog(prefDialog, refresh); // this takes awhile
            savePropertiesDialog();
        }
    }

    private void processPreferenceDialog(PreferencesDialog prefDialog, boolean refresh) {
        try {
            boolean dataSourceChanged = prefDialog.dataSourceChanged();
            boolean waveSourceChanged = prefDialog.waveSourceChanged();
            boolean locEngineChanged = prefDialog.locationEngineChanged();
            boolean magEngineChanged = prefDialog.magConfigChanged();

            boolean pickerChanged =       prefDialog.pickerChanged(); // picker block added 2010/09/29 -aww
            boolean pickerPropsChanged =  prefDialog.pickerPropsChanged();
            boolean pickerParmsChanged = prefDialog.pickerParmsChanged();

            boolean ctwModelChanged = prefDialog.channelTimeWindowModelChanged();
            ctwModelChanged |= prefDialog.waverootsCopyChanged(); // have to request new waveform path info from db - aww 2010/02/05
            boolean velocityModelChanged = prefDialog.velocityModelChanged(); // added - aww 2008/03/25
            boolean resetCatCols = prefDialog.catalogColsChanged();
            boolean catColorChanged = prefDialog.catalogColorChanged();
            boolean wfColorChanged = prefDialog.wfColorChanged();
            boolean tabTextColorChanged = prefDialog.tabTextColorChanged();
            boolean readingListColorChanged = prefDialog.readingListColorChanged();
            boolean solColorChanged = prefDialog.solColorChanged();
            boolean etypeMenuChoicesChanged = prefDialog.eventTypeMenuChanged();
            //boolean magTypeMenuChoicesChanged = prefDialog.magTypeMenuChanged();
            boolean mapInSplitChanged = prefDialog.mapInSplitChanged();
            boolean mapPropertiesChanged = prefDialog.mapPropsChanged();
            boolean channelCacheChanged = prefDialog.channelCacheFilenameChanged();
            boolean swarmInWaveformTabChanged = prefDialog.swarmInWaveformTabChanged();
            boolean sightColorChanged = prefDialog.sightColorChanged();
            boolean deltimColorChanged = prefDialog.deltimColorChanged();
            boolean unusedColorChanged = prefDialog.unusedPickColorChanged();

            boolean updateGUI = prefDialog.updateGUI();
            boolean resetGUI = prefDialog.resetGUI();
            boolean mapExists = (mapFrame != null);
            boolean mapIsShowing = (mapExists && mapFrame.isVisible());

            // replace current properties with the changed properties below line
            // resets certain values but doesn't configure DataSource, WaveDataSource -aww
            setProperties(prefDialog.getJiggleProperties(), false); // CatalogPanel table is not always updated with new properties - aww

            // savePropertiesDialog(); // could invoke this here to force user save

            setMasterViewProperties();  // like alignment mode etc.

            ChannelName.useFullName = getProperties().getBoolean("channelUseFullName");

            if (! refresh) {
                return; // when setProperties checkProperties finds problem, so recursive -aww
            }

            if (channelCacheChanged) {
                loadChannelList();
                mapPropertiesChanged = false; // toggle since loadChannelList does map rebuild since station layer needs refresh
            }

            if (velocityModelChanged) { // synch with TravelTime added - aww 2008/03/25
                TravelTime.setDefaultModel(getProperties().getSelectedVelocityModel());
                updateGUI = true; // to do mv.alignViews - aww 2009/04/01
            }

            if (waveSourceChanged) {
                Object obj = getProperties().getWaveSource();
                System.out.println("Jiggle INFO: waveform source changed to:\n" +  obj + "\n-------------------------------");
                AbstractWaveform.setWaveDataSource(obj);
            }

            //initEngines(); // creates a new whereEngine and a new loc/mag engine delegate, replaced by below - aww 2008/11/06
            // NOTE: if we ever do have more than one locEngine class, and the engine prop config changes, we need to update when those change here too.
            // Added below condition test to reset delegate (to avoid method appchannels lookup) -aww 2010/08/19
            if (channelCacheChanged || magEngineChanged || locEngineChanged) {
              solSolverDelegate = prefDialog.getSolutionSolverDelegate(); // delegate initialized with changed magmethod props - aww test 2008/11/06
              // magEngine/magMethod properties mapped to magType should be values last set by GUI (not those read back from the disk properties files)
              initSolutionSolverDelegate(false); // update state, since other delegate may have been configured in tab panels like debug, resets properties
            }

            if (picker != null) { // picker block added 2010/09/29 -aww
                if (pickerChanged) {
                    System.out.println("DEBUG Jiggle Picker Changed!");
                    setupPicker();
                }
                else {
                    if (pickerPropsChanged) {
                        System.out.println("DEBUG Jiggle Picker Props Changed!");
                        /*
                        int answer = JOptionPane.YES_OPTION;
                        if (pickerParmsChanged) {
                            answer = JOptionPane.showOptionDialog(Jiggle.this,
                                         "Set parameters from editor panel or read from disk file (property)?",
                                         "Picker Properties & Parms Changed",
                                         JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,

                            if (answer == JOptionPane.YES_OPTION)
                        }
                        */
                        picker.setProperties(prefDialog.getPickerPropertyList());
                    }
                    if (pickerParmsChanged) {
                        System.out.println("DEBUG Jiggle Picker Parms Changed!");
                        picker.clearChannelParms(); // empty existing map first
                        java.util.List list = prefDialog.getPickerChannelParms();
                        //System.out.println("DEBUG new list size: " + list.size());
                        picker.addChannelParms(list);
                    }
                }
            }

            Solution oldSol = mv.getSelectedSolution();

            // check wf cache for loaded event -aww 2010/07/02
            if (oldSol != null && (waveSourceChanged || ctwModelChanged)) {
                if (AbstractWaveform.cache2FileDir != null) {  
                  File file = new File(AbstractWaveform.cache2FileDir);  
                  if ( file.exists()) {
                    final long evid = oldSol.getId().longValue();
                    File [] files = file.listFiles(
                        new java.io.FileFilter() {
                          public boolean accept(File file) {
                              String filename = file.getAbsolutePath();
                              if (!filename.endsWith(".seg")) return false;
                              return (filename.indexOf(String.valueOf(evid)) >= 0);
                          }
                        }
                    );

                    if (files.length > 0) {
                        int answer = JOptionPane.showConfirmDialog(Jiggle.this,
                          "Wavesource or WindowModel changed: delete event's waveform cache before reset?",
                          "WaveSource/WindowModel Change", JOptionPane.YES_NO_OPTION);

                        if (answer == JOptionPane.YES_OPTION)
                            AbstractWaveform.clearCacheDir(evid, null, null);
                    }
                  }
                }
            }

            // Must make new connection if data source was changed, however Jiggle
            // lacks a tool button to force new connection when prefDialog UNCHANGED,
            // thus we do a check here also for DataSource either null or closed. - aww
            if (dataSourceChanged || DataSource.isNull() || DataSource.isClosed()) {
                // boolean flags set false below here since makeGUI() is called by switchDataSource call -aww
                wfColorChanged = false;
                sightColorChanged = false;
                deltimColorChanged = false;
                unusedColorChanged = false;
                readingListColorChanged = false;
                tabTextColorChanged = false;
                solColorChanged = false;
                etypeMenuChoicesChanged = false;
                //magTypeMenuChoicesChanged = false;
                updateGUI = false;
                resetGUI = false;
                ctwModelChanged = false;
                if (oldSol != null && oldSol.getNeedsCommit()) {
                  int answer = JOptionPane.showConfirmDialog(Jiggle.this,
                          "Property change requires GUI reset, save current event before reset?",
                          "Database Source Changed", JOptionPane.YES_NO_OPTION);
                  if (answer == JOptionPane.YES_OPTION) {
                    saveToDb(oldSol);
                  }
                }
                disposeMap(); // don't reuse map frame
                switchDataSource();
                if (oldSol != null) {
                  int answer = JOptionPane.showConfirmDialog(Jiggle.this,
                          "Load same event id from new source?",
                          "Database Source Changed", JOptionPane.YES_NO_OPTION);
                  if (answer == JOptionPane.YES_OPTION) {
                    loadSolution(oldSol.id.longValue(), isNetworkModeLAN());
                    resetCatPanelSelection();
                  }
                } 
                // rebuild of map if it existed after catalog is rebuilt by switchDataSource
                if (mapExists) updateMapFrame(mapInSplitChanged, mapPropertiesChanged, mapIsShowing);
                if (swarmInWaveformTabChanged) toggleSwarmLayout();
                return;
            }
            else if (getProperties().getProperty("dbWriteBackEnabled") != null) { // new property name(use this instead)
              DataSource.setWriteBackEnabled(getProperties().getBoolean("dbWriteBackEnabled"));
            }

            // Does property change require GUI rebuild?
            if (resetGUI) {
                disposeMap(); // don't reuse map frame

                if (oldSol != null && oldSol.getNeedsCommit()) {
                  int answer = JOptionPane.showConfirmDialog(Jiggle.this,
                          "Property change requires GUI reset, save current event before reset?",
                          "GUI Layout Changed", JOptionPane.YES_NO_OPTION);
                  if (answer == JOptionPane.YES_OPTION) {
                    saveToDb(oldSol);
                  } 
                }
                remakeGUI();
                if (oldSol != null) {
                  int answer = JOptionPane.showConfirmDialog(Jiggle.this,
                          "Reload last viewed event id ?",
                          "GUI Layout Changed", JOptionPane.YES_NO_OPTION);
                  if (answer == JOptionPane.YES_OPTION) {
                    loadSolution(oldSol.id.longValue(), isNetworkModeLAN());
                    resetCatPanelSelection();
                  }
                } 
                // rebuild of map if it existed after catalog is rebuilt by remakeGUI()
                if (mapExists) updateMapFrame(mapInSplitChanged, mapPropertiesChanged, mapIsShowing);
                if (swarmInWaveformTabChanged) toggleSwarmLayout();
                return;
            }

            // No change forced makeGUI(), so below here it's more like a repaint of existing components
            if (mapExists) updateMapFrame(mapInSplitChanged, mapPropertiesChanged, mapIsShowing);

            if (waveSourceChanged || ctwModelChanged) {
                // boolean flags set false since wfpanels are reset by below resetGUI or loadSolution call -aww
                if (scopeMode) { // Test, ? does this clear pick/group wfpanels after change of waveserver when using scope mode ? -aww 2015/02/08
                    System.out.println("DEBUG Jiggle: scopeModel (source/model change) scopeLoadMasterView...");
                    scopeLoadMasterView(false);
                    statusBar.updateDataSourceLabels();
                }
                else {
                    readingListColorChanged = false;
                    wfColorChanged = false;
                    sightColorChanged = false;
                    deltimColorChanged = false;
                    unusedColorChanged = false;
                    solColorChanged = false;
                    etypeMenuChoicesChanged = false;
                    //magTypeMenuChoicesChanged = false;
                    updateGUI = false;

                    statusBar.updateDataSourceLabels();

                    if (oldSol != null) {
                      tabTextColorChanged = false; // reload does the updateTextViews -aww
                      if (JOptionPane.NO_OPTION ==
                          JOptionPane.showConfirmDialog(Jiggle.this,
                              "Reload all data for event from database?\nNO = Keeps current view picks, amps, and coda",
                              "WaveSource or ChannelTimeWindowModel Changed", JOptionPane.YES_NO_OPTION)) {
                          //mv.refreshWFViewListByCurrentModel();
                          //resetGUI(true); // only redo data components not toolbars
                          refreshWFViewList();
                      } else {
                          reloadSolution();
                      }
                    }
                }

                resetMenuAndToolBars();
            }

            if (solColorChanged) {
                if (mv != null && mv.solList != null) mv.solList.setColors();
                if (toolBar.solPanel != null && toolBar.solPanel.solCombo != null)
                       toolBar.solPanel.solCombo.populateList();
                if (pickPanel != null) pickPanel.zwfp.resetWfColor();
                if (wfScroller != null) wfScroller.groupPanel.resetWfColor();
                // ? need to set PickFlag.setMarkerColorByPhase, PickFlag.colorP,and PickFlag.colorS here by properties, or not ?
            }

            if (wfColorChanged) { // repaint visible waveforms with new color map values
                //getProperties().loadComponentColors(); // this is done by Preferences OK
                if (pickPanel != null) pickPanel.zwfp.resetWfColor();
                if (wfScroller != null) wfScroller.groupPanel.resetWfColor();
            }

            if (sightColorChanged) { // repaint pick panel sight lines
                if (pickPanel != null) pickPanel.setSightColor(getProperties().getSightLineColor());
            }

            if (deltimColorChanged) { // repaint pick panel sight lines
                PickFlag.DEFAULT_DELTIM_COLOR = getProperties().getDeltimLineColor();
                PickFlag.dtColor = PickFlag.DEFAULT_DELTIM_COLOR;
            }

            if (unusedColorChanged) { // repaint visible waveforms with new color map values
                //getProperties().loadComponentColors(); // this is done by Preferences OK
                PickFlag.DEFAULT_UNUSED_PICK_COLOR = getProperties().getUnusedPickColor();
                PickFlag.upColor = PickFlag.DEFAULT_UNUSED_PICK_COLOR;
                if (pickPanel != null) pickPanel.zwfp.repaint();
                if (wfScroller != null) wfScroller.groupPanel.repaint();
            }

            if (tabTextColorChanged) {
               updateTextViews();
            }

            if (readingListColorChanged) {
               if (srlPhases != null) srlPhases.refreshColors();
               if (! prefMagSelectableReadingLists.isEmpty()) {
                 Iterator iter = prefMagSelectableReadingLists.values().iterator();
                 SelectableReadingList srl = null;
                 while (iter.hasNext()) {
                     srl = (SelectableReadingList) iter.next();
                     srl.refreshColors();
                 }
               }
            }

            if (updateGUI) {

                mv.alignViews(); // in case the velocity model or alignmode mode changed -aww 2009/04/01


                if (wfScroller != null) {
                  wfScroller.groupPanel.setWFDataBoxScaling(WFGroupPanel.wfDataBoxScaling); // ?
                  wfScroller.groupPanel.resetPanelBoxSize();
                  wfScroller.groupPanel.setShowCursorTimeAsLine(getProperties().getBoolean("showCursorTimeAsLine"));
                  wfScroller.groupPanel.setShowCursorAmpAsLine(getProperties().getBoolean("showCursorAmpAsLine"));
                  wfScroller.setShowRowHeader(getProperties().getBoolean("showRowHeaders"));
                  wfScroller.showChannelLabels(getProperties().getInt("channelLabelMode"));
                  wfScroller.groupPanel.wfpList.setShowPhaseCues(getProperties().getBoolean("showPhaseCues"));
                  wfScroller.groupPanel.wfpList.setShowDeltaTimes(getProperties().getBoolean("showDeltimes"));
                  wfScroller.groupPanel.wfpList.setShowResiduals(getProperties().getBoolean("showResiduals"));
                  wfScroller.groupPanel.setShowSamples(getProperties().getBoolean("showSamples"));
                  wfScroller.groupPanel.setShowSegments(getProperties().getBoolean("showSegments"));
                  wfScroller.groupPanel.showPhaseDescriptions(getProperties().getBoolean("showPickFlags",true));
                  wfScroller.groupPanel.setTimeTicksFlag(getProperties().getInt("wfpanel.timeTicksFlag",0));
                  wfScroller.groupPanel.setShowTimeLabel(getProperties().getBoolean("wfpanel.showTimeScaleLabel",true));
                  wfScroller.groupPanel.setShowTimeScale(getProperties().getBoolean("wfpanel.showTimeScale",true));
                  wfScroller.groupPanel.repaint();
                  wfScroller.revalidate();
                }
                if (pickPanel != null) {
                  pickPanel.zwfp.setWFDataBoxScaling(WFGroupPanel.wfDataBoxScaling); // ?
                  pickPanel.makeChannelLabel(); // redraw sta label
                  pickPanel.zwfp.setShowCursorTimeAsLine(getProperties().getBoolean("showCursorTimeAsLine"));
                  pickPanel.zwfp.setShowCursorAmpAsLine(getProperties().getBoolean("showCursorAmpAsLine"));
                  pickPanel.zwfp.setShowPhaseCues(getProperties().getBoolean("showPhaseCues"));
                  pickPanel.zwfp.setShowSamples(getProperties().getBoolean("showSamples"));
                  pickPanel.zwfp.setShowSegments(getProperties().getBoolean("showSegments"));
                  pickPanel.zwfp.setShowDeltaTimes(getProperties().getBoolean("showDeltimes"));
                  pickPanel.zwfp.setShowResiduals(getProperties().getBoolean("showResiduals"));
                  pickPanel.zwfp.setAntialias(getProperties().getBoolean("antialiasWaveform"));
                  pickPanel.setScaleList(getProperties().getStringArray("zoomScaleList"));
                  pickPanel.cursorLocPanel.setShowPhyUnits(getProperties().getBoolean("zoom.cursorShowsPhyUnits"));
                  pickPanel.zwfp.setTimeTicksFlag(getProperties().getInt("wfpanel.timeTicksFlag",0));
                  pickPanel.zwfp.setShowTimeLabel(getProperties().getBoolean("wfpanel.showTimeScaleLabel",true));
                  pickPanel.zwfp.setShowTimeScale(getProperties().getBoolean("wfpanel.showTimeScale",true));
                  pickPanel.zwfp.repaint();
                }

                resetMenuAndToolBars();
            }
            else if (etypeMenuChoicesChanged) toolBar.solPanel.resetChooserTypes(); 

            if (getProperties().getBoolean("waveformInTab")) {
                if (mainSplit != null) setWaveformsInTabPane(tabPane, wfSplit); // works if method makes mainSplit null
            }
            else if (mainSplit == null) {
                setWaveformsInMainSplit(wfSplit);
            }

            if (mainSplit != null) {
                mainSplit.setOrientation(getProperties().getInt("mainSplitOrientation"));
            }

            //if (waveSourceChanged) statusBar.updateDataSourceLabels(); // done above -aww

            if (locEngineChanged) {
                updateLocationTab();
                statusBar.updateLocServiceLabel();
            }

            if (resetCatCols) {
                if (getProperties().getBoolean("catalog.loadFromFile")) { // aww 2014/10/02
                    resetCatPanel(true, getProperties().getProperty("catalog.evidfile")); // aww 2014/10/02
                }
                else resetCatPanel(true); // -aww 2008/06/10 as test
            }
            else if (catColorChanged && catPane != null) catPane.setJiggle(Jiggle.this);  // propagates catalog row color property changes

            if (swarmInWaveformTabChanged) toggleSwarmLayout();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    protected void resetMenuAndToolBars() {
        getContentPane().remove(toolBar); // Create new tool bar with new property settings
        getContentPane().add(makeToolBar(), BorderLayout.NORTH); // Create new tool bar with new property settings
        setJMenuBar(makeMenuBar()); // Create new menu bar with new property settings
        resetBarAndMenuStates();
        validate();
        repaint();
    }

    private void toggleSwarmLayout() {
        if (swarmComponent == null) return;

        boolean swarmVisible = swarmComponent.isVisible();

        if (swarmComponent instanceof JFrame) {
            JFrame jFrame = (JFrame) swarmComponent;
            swarmComponent = (Container) jFrame.getContentPane().getComponent(0);
            jFrame.dispose();
            swarmComponent.setVisible(swarmVisible);

            swarmComponent = createSwarmComponentForTabPane(swarmComponent);
        }
        else { // splitPane in waveform tab
            if (tabPanel[TAB_WAVEFORM] != null) {
                tabPanel[TAB_WAVEFORM].remove(swarmComponent);
                tabPanel[TAB_WAVEFORM].revalidate();
            }

            swarmComponent.setVisible(true); // else not seen, blank frame
            final JFrame jFrame = createFrameFromSwarmComponent(swarmComponent);

            if (swarmVisible) {
              jFrame.toFront(); // no effect because preferences dialog saving properties at end gives focus to Jiggle frame
            }
            else jFrame.setVisible(false);

            swarmComponent = jFrame;
        }
    }

    protected void updateMapFrame(boolean mapInSplitChanged, boolean mapPropertiesChanged) {
        updateMapFrame( mapInSplitChanged, mapPropertiesChanged, (mapFrame != null && mapFrame.isVisible()) );
    }

    private void updateMapFrame(boolean mapInSplitChanged, boolean mapPropertiesChanged, boolean isShowing) {

        // Now only dispose if changes effect map view -aww 2010/08/04

        if (mapPropertiesChanged) {
            disposeMap(); // sets mapFrame null, removes handler from panel
            if (debug) System.out.println("DEBUG Jiggle updateMapFrame disposeMap() setting mapPanel and mapPropHandler null.");
            mapPanel = null;
            mapPropHandler = null;
        }

        if (catMapSplit != null) catMapSplit.setOrientation(getProperties().getInt("mapSplitOrientation"));
        if ((mapInSplitChanged || mapPropertiesChanged) && isShowing) {
            disposeMap(); // sets mapFrame null first
            showMap(); // only show (rebuild) if it changed - aww 2010/08/04
        }
        else {
            Jiggle.this.toFront();
        }
    }


    protected void reset() {
        System.out.println(">>> Jiggle RESET masterview properties....");
        setMasterViewProperties();
        System.out.println(">>> Jiggle RESET datasource connection....");
        switchDataSource();
        System.out.println(">>> Jiggle RESET WaveDataSource....");
        AbstractWaveform.setWaveDataSource(getProperties().getWaveSource());
        statusBar.updateDataSourceLabels();
        System.out.println(">>> Jiggle RESET initEngines....");
        initEngines();
        statusBar.updateLocServiceLabel();
        // added code below aww - 08/09/2006
        System.out.println(">>> Jiggle RESET loadChannelList....");
        loadChannelList();
        System.out.println(">>> Jiggle RESET travelTime model: " + TravelTime.getInstance().toString());
        statusBar.updateVelModelLabel();
        System.out.println(">>> Jiggle RESET DONE <<<");
    }


    /** Sanity and completeness check of properties. */
    // Note: this can't be done in the JiggleProperties class
    // because it may have no graphics context.
    protected void checkProperties() {

        // ? DEPRECATE jasiObjectType? will there ever be another type -aww
        if (!JasiObject.setDefault(props.getProperty("jasiObjectType"))) {
            // Set the jasi object type to be used for the run
            helpAction.makeHelpDialog();
            int yn = JOptionPane.showConfirmDialog(
                   Jiggle.this, "WARNING: Default jasi object type undefined or invalid.\n"+
                 "Click [YES] to set it in preferences",
                   "Bad Jasi Type", JOptionPane.YES_NO_OPTION);
            if (yn == JOptionPane.YES_OPTION) {
                doPreferencesDialog(PreferencesDialog.TAB_MISC, false); // update properties, but don't refresh graphics yet
            }
        }

        // Net code
        if (props.getProperty("localNetCode").equals("??")) {
            helpAction.makeHelpDialog();
            int yn = JOptionPane.showConfirmDialog(
                   Jiggle.this, "WARNING: Network code is undefined.\n"+
                 "Click [YES] to set it in preferences",
                   "No Net Code", JOptionPane.YES_NO_OPTION);
            if (yn == JOptionPane.YES_OPTION) {
                doPreferencesDialog(PreferencesDialog.TAB_MISC, false); // update properties, but don't refresh graphics yet
            }
        }

        // Db connection 
        if (props.getProperty("dbaseTNSname", "").equals("") && props.getProperty("dbaseName", "").equals("")) {
            helpAction.makeHelpDialog();
            int yn = JOptionPane.showConfirmDialog(
                   Jiggle.this, "WARNING: Database connection not defined.\n"+
                 "Click [YES] to set it in preferences",
                   "Missing Connection Info", JOptionPane.YES_NO_OPTION);
            if (yn == JOptionPane.YES_OPTION) {
                doPreferencesDialog(PreferencesDialog.TAB_DATACONNECTION, false); // update properties, but don't refresh graphics yet
            }
        }

        if (Phase.DeltaTimes.length < Phase.DEFAULT_DELTATIMES.length) {
            int yn = JOptionPane.showConfirmDialog(
                   Jiggle.this, "ERROR: Phase.DeltaTimes should have length:"+Phase.DEFAULT_DELTATIMES.length+
                   " found: "+Phase.DeltaTimes.length+"\n"+
                 "Click [YES] to reset it in preferences",
                   "Invalid Phase Delta Time Array", JOptionPane.YES_NO_OPTION);
            if (yn == JOptionPane.YES_OPTION) {
                doPreferencesDialog(PreferencesDialog.TAB_PICKLAYOUT, false); // update properties, but don't refresh graphics yet
                //Phase.DeltaTimes = Phase.DEFAULT_DELTATIMES;
            }
        }

        if (org.trinet.filters.FilterTypes.getType(props.getProperty("defaultZoomFilterType")) < 0) {
            int yn = JOptionPane.showConfirmDialog(
                   Jiggle.this, "ERROR: Unknown filter type:" +props.getProperty("defaultZoomFilterType")+"\n"+
                 "Click [YES] to reset it",
                   "Invalid Default Zoom Panel Filter", JOptionPane.YES_NO_OPTION);
            if (yn == JOptionPane.YES_OPTION) {
                doPreferencesDialog(PreferencesDialog.TAB_WF_CONTROLS, false); // update properties, but don't refresh graphics yet
                //ZoomPanel.defaultFilterType = "HIGHPASS";
            }
        }
    }

    protected boolean checkEventProperties(EventSelectionProperties eProps) {
        // Check for event selection properties file(s) presence
        boolean status = true;

        // Check event selection properties has specified a valid time span
        if (! eProps.hasValidTimeSpan()) {
            JOptionPane.showMessageDialog(
                   Jiggle.this, "WARNING: Event selection properties specifies an invalid time span!\n"+
                 "Edit your catalog table time properties",
                   "Catalog Time Span Invalid", JOptionPane.PLAIN_MESSAGE);
            status = false;
        }

        // Since large time spans may return large result sets dialog
        // below give user opportunity to stop before creating catalog
        // if the timespan input from properties was a mistake.
        // Check total days in TimeSpan requested
        int days = (int) (eProps.getTimeSpan().getDuration()/ 86400. + 0.5);
        if (days > 31) {
            status = (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(
                         Jiggle.this, "WARNING: Event selection properties time span days: "+days+" > 31 days.\n"+
                         "Press YES, to accept this time span.",
                         "Large Catalog Time Span", JOptionPane.YES_NO_OPTION)
                     );
        }

        return status;
    }

    protected String getPropertyFileStatusFor(GenericPropertyList gpl, String msg) {
          StringBuffer sb = new StringBuffer(256);
          //sb.append("<html>");
          sb.append("Input properties file status:\n");
          //sb.append("<br>");
          sb.append(" Default: ").append(gpl.getDefaultPropertiesFileName());
          sb.append(" --> exists: " ).append(gpl.hasDefaultPropertiesFile()).append("\n");
          //sb.append("<br>");
          sb.append(" User   : ").append(gpl.getUserPropertiesFileName());
          sb.append(" --> exists: ").append(gpl.hasUserPropertiesFile()).append("\n");
          if (msg != null) {
            //sb.append("<br>");
            sb.append(msg);
            //sb.append("</html>");
          }
          return sb.toString();
    }

    /** Checks this code's version against highest available back at the home
     * web site. Pops a warning dialog if we are out of rev.
     * */
    private void checkVersion() {

        if (versionCheckDisabled) return;

        String webSite = (props == null) ? VersionChecker.homeAddress : props.getProperty("webSite");
        System.out.println("Jiggle INFO: VersionChecker webSite = " + webSite);

        VersionChecker vc = VersionChecker.vc;
        if (vc == null) vc = new VersionChecker(webSite);

        int check = vc.versionDifference(versionNumber);

        if (check == 0) return;  // same version #

        if (check == -1) {
          /*
          msg = "<html><h2>WARNING:</h2><b>Your version of Jiggle is ahead-of-date.<p>"+
                 "Download current version <b>"+
                 vc.getVersion()+
                 "</b> from:<br><it>"+
                 vc.getDownloadAddress()+
                 "</it></b> </html>";
          title = "Jiggle Ahead-of-Date "+versionNumber;
          */
          System.out.println("NOTE: This Jiggle version " +versionNumber+ " is a test version.");
          System.out.println("      Please report any suspected bugs to developers. Thank you.");
          return;
        }

        String msg = "<html><h2>WARNING:</h2><b>Your version of Jiggle is out-of-date.<p>"+
                     "Download current version <b>"+
                     vc.getVersion()+
                     "</b> from:<br><it>"+
                     vc.getDownloadAddress()+
                     "</it></b> </html>";
        String title = "Jiggle Out-of-Date "+versionNumber;
        System.out.println(title);

        Object[] options = {"Dismiss" , "Open Browser", "Exit"};

        int rtn = JOptionPane.showOptionDialog(Jiggle.this, msg, title,
                       JOptionPane.DEFAULT_OPTION,
                       JOptionPane.WARNING_MESSAGE,
                       null,
                       options,
                       options[0]);  // default

        if (rtn == 1) { //startup external pointed at Jiggle web site
            try {
              Runtime.getRuntime().exec(
                 "cmd /c start "+vc.getDownloadAddress());
            } catch (Exception ex) {
              ex.printStackTrace();
            }
        }
        else if (rtn == 2)  System.exit(0);
    }

//
////////////////////// Begin DataSource Configuration Methods ////////////////////////////////
//
    /**
     * Make the actual connection to the data source. Returns 'true' on success.
     */
    public boolean makeDataSourceConnection() {
        // get current data source description from properties
        DbaseConnectionDescription dbDesc = getProperties().getDbaseDescription();

        if (getVerbose() || !dbDesc.isValid()) System.out.println(dbDesc.toString());

        if (!dbDesc.isValid() || getProperties().getBoolean("useLoginDialog")) {
            if (!dbDesc.isValid()) System.out.println("Invalid database connection properties, reverting to login dialog");
            // quit if user closes dialog (probably wants to edit something)
            if ( ! updateDbaseDescriptionFromDialog(dbDesc)) System.exit(0);
        }

        java.util.Date dbDate = null; 

        boolean statusOk = true; // New connection OK?
        if (DataSource.isNull()) {
          //new DataSource(dbDesc.getURL(), dbDesc.getDriver(), dbDesc.getUserName(), dbDesc.getPassword()); // removed 2008/05/14 -aww
          new DataSource().set(dbDesc); // added 2008/05/14 -aww

          statusOk = ! (DataSource.isNull() || DataSource.isClosed());

          if (!versionCheckDisabled) dbDate = DataSource.getDbVersionDate("Jiggle");
          //System.out.println("DEBUG dbDate: " + dbDate);
        }
        else {
          if (! DataSource.isClosed()) {
              System.out.println("FYI- Jiggle releasing locks and closing prior DataSource: " + DataSource.toDumpString()); 
              releaseAllSolutionLocks(); // added this for case where JasiEventLock transactions not replicated -aww 2008/10/29
              DataSource.close(); // close existing connection, if open
          }
          statusOk = DataSource.set(dbDesc);
        }


        if (! statusOk) {
          System.err.println("Database Connection failure to:\n URL    = " + dbDesc.getURL() +
                             "\n driver = " + dbDesc.getDriver() +
                             "\n user   = " + dbDesc.getUserName()
                             );
          String err = DataSource.getConnectionStatus();
          String msg = "WARNING: DataSource could not be opened.\n"+
                 err + "\n"+
                 "Check parameters.\n"+
                 "Host = "+ DataSource.getHostName() + "\n"+
                 "Dbase = "+ DataSource.getDbaseName() + "\n"+
                 "Username = " + DataSource.getUserName()+ "\n"+
                 "Port #: "+ DataSource.getPort();
          popInfoFrame("Bad DataSource", msg);
          if (statusPanel != null) statusPanel.setText("Connection failed.");
          if (statusBar != null) statusBar.updateDataSourceLabels();
          return false;
        }

        // Reset other connection objects ?
        EventTypeMap3.getMap().loadEventTypes(); // after connection load event types 2008/10/09 -aww

        org.trinet.jasi.seed.SeedReader.reconnectBlobReader(); // aww 2007/01/10
        if (whereEngine != null) whereEngine.setConnection(DataSource.getConnection()); // aww 2007/01/10

        getProperties().setProperty("dbLastLoginURL", dbDesc.getURL());

        if (getProperties().getProperty("writeBackEnabled") != null) { // old property name (remove from files)
          DataSource.setWriteBackEnabled(getProperties().getBoolean("writeBackEnabled"));
        }
        else if (getProperties().getProperty("dbWriteBackEnabled") != null) { // new property name(use this instead)
          DataSource.setWriteBackEnabled(getProperties().getBoolean("dbWriteBackEnabled"));
        }

        //if (dbDate == null || dbDate.compareTo(Version.getBuildDate()) > 0) {
        java.util.Date myVersionDate = Version.getBuildDate(); 
        //System.out.println("DEBUG myDate: " + myVersionDate);
        if (dbDate != null && dbDate.compareTo(myVersionDate) > 0) {
            String str = "Invalid Jiggle version: " + EpochTime.dateToString(myVersionDate,"yyyy-MM-dd")+
                " valid version(s) are dated: " + dbDate + " or later";
            str += "\nDO NOT save any event data with this Jiggle version you may corrupt database";
            JOptionPane.showMessageDialog( Jiggle.this, str, "Invalid Jiggle Version", JOptionPane.PLAIN_MESSAGE);
            System.out.println(str);
            DataSource.setWriteBackEnabled(false);
            getProperties().setProperty("dbWriteBackEnabled", "false");
            if (! DataSource.isClosed()) {
              //System.out.println("Invalid Jiggle version exiting and closing DataSource...");
              //stop();
            }
            return false;
        }

        if ( getProperties().getBoolean("dbTrace") ) {
           DataSource.enableSessionTrace();
           System.out.println("INFO: Session SQL trace ENABLED on database.");
        }

        if (DataSource.utcCompliant() == 0) {
          String msg = "<html><b>ERROR:</b> Database requires NOMINAL version, this Jiggle is UTC version!<br>"+
                       "URL = "+ DataSource.getHostName() + "<br>"+
                       "Dbase = "+ DataSource.getDbaseName() +"<br>"+
                       "Username = " + DataSource.getUserName()+"<br>"+
                       "Port #: "+ DataSource.getPort()+"</html>";
          popInfoFrame("Jiggle/DB Mismatch", msg);
          if (confirmShutDown()) stop();
        }

        if (DataSource.isReadOnly()) {
          String msg = "<html><b>WARNING:</b> You have READ-ONLY access to this database.<br>"+
                       " You will not be able to save any of the work you do.<br>"+
                       "Check parameters.<p>"+
                       "URL = "+ DataSource.getHostName() + "<br>"+
                       "Dbase = "+ DataSource.getDbaseName() +"<br>"+
                       "Username = " + DataSource.getUserName()+"<br>"+
                       "Port #: "+ DataSource.getPort()+"</html>";
          popInfoFrame("Read-only Access", msg);
        }

        // set static attribute in waveform class about type of data source
        getProperties().setupWaveDataSource();
        System.out.println("Jiggle INFO: waveform source:\n" + getProperties().getWaveSource() + "\n-------------------------------");

        if (!DataSource.isReadOnly() && DataSource.isWriteBackEnabled()) setupLockingOnDataSource();

        if (statusPanel != null) statusPanel.clear();
        if (statusBar != null) statusBar.updateDataSourceLabels();

        if (debug) {
          System.out.println(
              "Jiggle DEBUG makeDataSourceConnection to:\n"+
              //dbDesc.getDbaseName()+" on "+dbDesc.getHostName()
              DataSource.toDumpString()
          );
        }

        return true;
    }

// Optional user connection configuration method
    private boolean updateDbaseDescriptionFromDialog(DbaseConnectionDescription dbd) {
        DataSourceDialog dataSourceDialog =
          new DataSourceDialog(Jiggle.this, "Database Connection Info", 16);
        dataSourceDialog.setModal(true);
        dataSourceDialog.setLocation(325,350);
        dataSourceDialog.setSize(400,200);

        // Use last login source first, if defined, else default input URL
        String lastURL = getProperties().getProperty("dbLastLoginURL");
        if (lastURL != null) dataSourceDialog.setHost(lastURL);
        else dataSourceDialog.setHost(dbd.getURL());

        dataSourceDialog.setSchema(dbd.getUserName());
        dataSourceDialog.setPassword(dbd.getPassword());

        dataSourceDialog.setVisible(true);
        if (! dataSourceDialog.okPressed()) return false;

        String dbURLString = dataSourceDialog.getHost();
        if (debug) System.out.println("Jiggle DEBUG login dialog host field =\""+dbURLString+"\"");

        if (dbURLString.indexOf(".") > 0) { // URL
            dbd.setURL(dbURLString);
        }
        // below convenience for trinet type db naming convention
        else if (dbURLString.length() > 0) { // assume simple host name
          dbd.setHostName(dbURLString);
          dbd.setDbaseName(dbURLString+"db"); // trinet dbname convention
        }
        /* no-op, revert to original input URL source
        else { // use last login source
          dbURLString = getProperties().getProperty("dbLastLoginURL");
          if (dbURLString != null) dbd.setURL(dbURLString);
        }
        */

        if (debug) System.out.println("Jiggle DEBUG description URL =\""+dbd.getURL()+"\"");

        String dbUserText = dataSourceDialog.getSchema();
        dbd.setUserName(dbUserText);

        String dbPasswordText = dataSourceDialog.getPassword();
        dbd.setPassword(dbPasswordText);
        return true;
    }

    protected String getConnectionInfo() {
        StringBuffer sb = new StringBuffer(512);
        sb.append("DataSource:");
        sb.append( (DataSource.isNull()) ? "null" : DataSource.describeConnection());
        return sb.toString();
    }

    private boolean setupLockingOnDataSource() {
        if (getProperties().getBoolean("solLockingDisabled")) return true;
        //solLock = SolutionLock.create();
        if (solLock == null) solLock = SolutionLock.create();
        solLock.setVerbose(debug);
        // Check if locking works for data source
        //if (isNetworkModeLAN() && ! solLock.checkLockingWorks()) {
        if (! solLock.checkLockingWorks()) {
          String msg = "WARNING: Event locking is not supported by this data source.";
          System.out.println(msg);
          //
          final JDialog jd = new CenteredDialog(Jiggle.this, "Event Lock Warning", true);
          JPanel jp = new JPanel(new BorderLayout());
          jp.add(new JLabel(msg), BorderLayout.NORTH);
          jp.add(new JLabel("You can procede with the possiblity of collisions with other users."), BorderLayout.CENTER);
          JButton jb = new JButton("OK");
          jb.addActionListener(new ActionListener() {
              public void actionPerformed(ActionEvent evt) {
                  jd.dispose();
              }
          });
          Box hbox = Box.createHorizontalBox();
          hbox.add(Box.createHorizontalGlue());
          hbox.add(jb);
          hbox.add(Box.createHorizontalGlue());
          jp.add(hbox, BorderLayout.SOUTH);
          jd.getContentPane().add(jp);
          jd.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
          jd.pack();
          jd.setVisible(true);
          //
          //popInfoFrame("Event Lock Warning", msg);
          //
          return false;
        }
        return true;
    }

    /** Switch to a newly defined data source. Clear the GUI because you don't
     *  want to write old, previously loaded, data to a new data source. */
    private boolean switchDataSource() {
        if (makeDataSourceConnection() ) {
          remakeGUI();
          if (statusBar != null) statusBar.updateDataSourceLabels();
          return true;
        }
        return false;
    }

//
////////////////////// End of DataSource Configuration Methods ////////////////////////////////
//

    private void initEngines() {
        initWhereEngine(); // used to be makeWhereEngine - aww 2008/06/09
        initSolutionSolverDelegate(true);
    }

    private void initSolutionSolverDelegate(boolean createNew) {
        if (solSolverDelegate == null || createNew) { // always make new instance -aww 2008/11/07
          if (getVerbose()) System.out.println(">>> Creating new JiggleSolutionSolverDelegate.");
          if (solSolverDelegate != null) solSolverDelegate.disconnect(); // close any residual service connection -aww 2010/05/24
          solSolverDelegate = new JiggleSolutionSolverDelegate(Jiggle.this);
        }
        //above assumes instantiation of delegate sets default use of current MasterList else need:
        //solSolverDelegate.setChannelList(MasterChannelList.get());
        //solSolverDelegate.initializeEngineDelegate(); // added - aww 01/24/2005 constructor doesn't initialize but below does (channellist etc.)
        solSolverDelegate.setDelegateProperties(getProperties()); // - aww 2007/03/08
        String [] mtypes = getProperties().getMagMethodTypes();
        Solution sol = mv.getSelectedSolution();
        for (int i=0; i<mtypes.length; i++) {
            //solSolverDelegate.initMagMethod(sol, mtypes[i]); // use below instead ?
            solSolverDelegate.initMagEngineForType(sol, mtypes[i], true); // added -aww 2010/04/19 
        }
    }

    /*
     * Load the Channel list. This is done one time at program startup.
     * The list is used to lookup lat/lon for new data as it is read in.
     */
    protected boolean loadChannelList() {

        boolean status = true;

        ChannelList cList = MasterChannelList.get();
        if (cList != null) {
            cList.removeCacheRefreshListener(Jiggle.this);
            LogUtil.debug(debug, "DEBUG Jiggle removeCacheRefreshListener(Jiggle.this)");
        }

        String dsStr = "<html>Reading Channels from database:<p> "+DataSource.getDbaseName()+" </html>";

        //DEBUG: here disable loading of associated channel data like corrections if db tables are missing
        if ( getProperties().isSpecified("channelListReadOnlyLatLonZ") &&
             getProperties().getBoolean("channelListReadOnlyLatLonZ") ) { // must test for t/f - aww 03/04/2005
            MasterChannelList.readOnlyChannelLatLonZ();
        }


        //boolean readCache = getProperties().getBoolean("cacheChannelList"); // removed aww 02/03/2005
        boolean readCache = getProperties().getBoolean("channelListCacheRead"); // added aww 02/03/2005
        String cacheFileName = getProperties().getUserFileNameFromProperty("channelCacheFilename");

        String str = null;
        if (readCache) {
          str = "<html>Reading Channels from file cache <p>"+cacheFileName+"</html>";
        } else {
          str = dsStr;
        }
        if (getVerbose()) {
            LogUtil.info(str);
        }
        initStatusGraphicsForThread("Reading Channel Data", str);

        // read Channel list from cache using the filename in input properties
        // uses default location if cacheFileName is null:
        String chanGroupName = getProperties().getChannelGroupName(); // is there one?
        try {
          int cnt = 0;
          while ( !scopeMode && ! haveCatSolList  && cnt < 120) { // 120 secs timeout (doubled the timeout) - aww 08/08/2006
              Thread.currentThread().sleep(1000L); // sleep for a second, wait for catalog to finish
              cnt++;
          }
        }
        catch (InterruptedException ex) { }

        // Assumes catalog is sorted in time order, get origin date of latest event
        // to be used for channellist load -aww - added test for NULL is case catalog thread not complete 08/09/06
        java.util.Date firstSolDate = (catSolList == null || catSolList.isEmpty()) ?
                null : catSolList.getSolution(catSolList.size()-1).getDateTime();

        String dbLoadStr = ">>> Load of " +
            ( (firstSolDate == null) ? "CURRENT" : firstSolDate.toString() ) +
            " Channels associated chanGroupName = " + chanGroupName +
            " from : " + DataSource.getDbaseName() +
            " might take minutes ...";

        if (readCache) {

          File file = new File(cacheFileName);
          if (file.exists()) {
              lastCacheRefreshTime = file.lastModified();
              LogUtil.debug(debug,"Jiggle DEBUG: cache file exists, last modified date: " + new Date(lastCacheRefreshTime));
          }

          MasterChannelList.set(ChannelList.readFromCache(cacheFileName));

          // cache read failed, read from dbase and write to cache for future use
          // uses default location if cacheFileName is null:
          if (MasterChannelList.isEmpty()) {
            String failStr = "Could not load channel data from a cache file.";
            LogUtil.info(failStr);
            LogUtil.info(dbLoadStr);

            updateStatusTextForThread("<html><b><center>"+failStr+"</b><p>"+dsStr+"</center></html>");
            if (chanGroupName == null) {
                MasterChannelList.set(ChannelList.readList(firstSolDate));
            } else {
              MasterChannelList.set(ChannelList.readListByName(chanGroupName, firstSolDate));
            }

            //db read succeeded -- write this "current" list to cache file in background
            if (MasterChannelList.get() != null && !MasterChannelList.get().isEmpty()) {
                lastCacheRefreshTime = System.currentTimeMillis();
                status = MasterChannelList.get().writeToCacheInBackground(); // write cache file for future use
            }
            else {
                status = false;
                LogUtil.warning("WARNING! Jiggle loadChannelList, empty list, check database channel data or channelGroupName.");
            }
          } else { // cache read succeeded
            // check to overwrite old Channel cache data with update of  "current" date Channel data
            if (getProperties().getBoolean("autoRefreshChannelList") && !cacheRefreshed) {
                int yn = JOptionPane.YES_OPTION;
                if (!isNetworkModeLAN()) {
                    yn = JOptionPane.showConfirmDialog(Jiggle.this,
                             "You are configured in WAN mode (not LAN), Do you want to refresh your channel cache?",
                             "Auto Refresh ChannelList WAN Mode", JOptionPane.YES_NO_OPTION
                         );
                }
                if (yn == JOptionPane.YES_OPTION) refreshMasterCache();
            }
          }
        // don't read cache file, instead read "current" list and write it to cache.
        } else {
          //if (verbose)
              System.out.println(dbLoadStr);
          if (chanGroupName == null) MasterChannelList.set(ChannelList.readList(firstSolDate));
          else {
            MasterChannelList.set(ChannelList.readListByName(chanGroupName, firstSolDate));
          }
          if (MasterChannelList.get() != null && !MasterChannelList.get().isEmpty()) {
              lastCacheRefreshTime = System.currentTimeMillis();
              status = MasterChannelList.get().writeToCacheInBackground(); // write cache file for future use
          }
          else {
              System.out.println("WARNING! Jiggle loadChannelList, empty list, check database channel data or channelGroupName.");
              status = false;
          }
        }

        // added channel map code logic -aww
        // create HashMap of channels in MasterChannelList, if not already one
        if (! MasterChannelList.get().hasLookupMap()) {
            MasterChannelList.get().createLookupMap();
        }
        if (getVerbose()) System.out.println(">>> Finished loading of MasterChannelList, total channels: " + MasterChannelList.get().size());
        if (solSolverDelegate != null) solSolverDelegate.setChannelList(MasterChannelList.get()); // added -aww 2008/11/14
        resetStatusGraphicsForThread();

        if (mapFrame != null) updateMapFrame( false, true, mapFrame.isVisible() ); // force map update since cached stations may have changed

        return status;
    }

    protected void refreshMasterCache() {
        ChannelList cList = MasterChannelList.get();
        if (cList != null) {
            if (getVerbose()) System.out.println("INFO: Auto refresh of channel list in background.");
            cList.addCacheRefreshListener(Jiggle.this);
            // BEWARE null input date, so only "currently" active Channels are cached
            cacheRefreshing = true;
            cList.refreshCache(null, getProperties().getChannelGroupName()); // added channel group name, else it loads all active channels -aww 2010/08/03
        }
    }

    public void channelCacheRefresh(CacheRefreshEvent evt) {
        cacheRefreshed = true;
        if (evt != null && evt.success() && getProperties().getBoolean("channelListCacheRead")) {
            Thread t = new Thread(new Runnable() {
                public void run() {
                    cacheRefreshing = false;
                    String msg = "INFO: Cache refresh has completed";
                    System.out.println(msg);
                    final JDialog jd = new CenteredDialog(Jiggle.this, "Cache File Refreshed", false);
                    JPanel jp = new JPanel(new BorderLayout());
                    jp.add(new JLabel(msg+", reload master channel list from refreshed cache file?"), BorderLayout.NORTH);
                    JButton jb = new JButton("YES");
                    jb.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent evt) {
                            jd.dispose();
                            System.out.println("Now reloading the master channel list from cache file...");
                            loadChannelList();
                            lastCacheRefreshTime = System.currentTimeMillis();
                        }
                    });
                    Box hbox = Box.createHorizontalBox();
                    hbox.add(Box.createHorizontalGlue());
                    hbox.add(jb);
                    jb = new JButton("NO");
                    jb.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent evt) {
                            jd.dispose();
                        }
                    });
                    hbox.add(jb);
                    hbox.add(Box.createHorizontalGlue());
                    jp.add(hbox, BorderLayout.SOUTH);
                    jd.getContentPane().add(jp);
                    jd.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                    jd.pack();
                    jd.setVisible(true);
                }
            });
            t.setDaemon(true);
            t.start();
        }
        else {
           System.out.println("INFO Jiggle channelCacheRefresh no-op, type code: " + evt.getType() +
                   " channelListCacheRead=" + getProperties().getBoolean("channelListCacheRead") );
        }
    }

    /** Make and configure the WhereEngine */
    protected void initWhereEngine() { // used to be makeWhereEngine - aww 2008/06/09
        //if (whereEngine == null) {// instead, always make new instance -aww 2008/11/07
          if (getVerbose()) System.out.println(">>> Creating WhereEngine.");
          whereEngine = WhereIsEngine.create(getProperties().getProperty("WhereIsEngine"));
          // Since initWhereEngine() was removed from makeCatPane() method do below -aww 2008/11/07
          if (catPane != null) catPane.setWhereIsEngine(whereEngine);
        //}

        /* DEPRECATED PROPERTY : engine implementation writes out km and mi units in output string
        // set Where units: km or miles
        String units = getProperties().getProperty("whereUnits");
        if (units != null) {
            if (units.startsWith("M") || units.startsWith("m")) {
              whereEngine.setDistanceUnits(GeoidalUnits.MILES);
            } else if (units.startsWith("K") || units.startsWith("k")) {
              whereEngine.setDistanceUnits(GeoidalUnits.KILOMETERS);
            }
        }
        */
    }

    protected WhereIsEngine getWhereIsEngine() {
        return whereEngine;
    }

//
///////////////// BEGIN GUI MANIPULATION METHODS ////////////////////
//

    /**
     * Create the whole GUI.
     */
    private void makeGUI() {
    // Main Frame is a 'swing' JFRAME
        // Upon first invocation only, add window listener
        if (menuBar == null && toolBar == null) {
          // Handle frame close using native close widget. Don't call 'shutDown()' here
          // because I can't get [No] option in shutDown dialog to work when the native
          // frame widget is used to close the framer.
          setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

          addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                if (confirmShutDown()) stop();
            }
          });
        }

        getContentPane().removeAll(); // else clear main frame, of any contents

        // Set main frame title to origin summary string
        DbaseConnectionDescription desc = getProperties().getDbaseDescription();
        StringBuffer sb = new StringBuffer(132);
        sb.append(DEFAULT_FRAME_TITLE);
        sb.append("     ");
        sb.append(desc.getHostName()).append("/");
        sb.append(desc.getDbaseName()).append("/");
        sb.append(desc.getUserName());
        setTitle(sb.toString());

        // Create a set frame menu bar
        setJMenuBar(makeMenuBar()); // a JFrame method

        // Create and add toolbar to frame
        getContentPane().add(makeToolBar(), BorderLayout.NORTH);

        // 1st need a tabPane component (for catalog, output from engines, messages ...)
        tabPane = makeTabPane();

        // 2nd need wfSplit component (for timeseries of masterview selected solution)
        wfSplit = makeWaveformSplitPane();

        // Depending on current setting of JiggleProperty:
        // add wfSplit to either the new tabPane as a new tab
        // or to a new mainSplit pane (to be shared with the tabPane)
        if (getProperties().getBoolean("waveformInTab")) {
          setWaveformsInTabPane(tabPane, wfSplit); // adds to contentPane CENTER
        }
        else {
          setWaveformsInMainSplit(wfSplit); // adds to contentPane CENTER
        }

        if (getProperties().getBoolean("mapInSplit")) catMapSplit = makeCatMapSplitPane();

        // add new status component to frame's contentPane
        statusBar = makeStatusPanel();
        getContentPane().add(statusBar, BorderLayout.SOUTH);

        // use property settings to configure frames size and location
        setSize(getProperties().getInt("mainframeWidth"), getProperties().getInt("mainframeHeight"));
        setLocation(getProperties().getInt("mainframeX"), getProperties().getInt("mainframeY"));

        // make Jiggle GUI visible (frames are created hidden)
        setVisible(true);  // same effect as show();

        // Make catalog for the tabPane after setVisible since a dbase
        // query using EventSelectionProperties could take a lot of time
        // NOTE: if user uses the menu to "open event/load id" the new
        // MasterView solution is not added to the catalog solution list.
        // If it didn't match the event properties it would disappear upon
        // catalog refresh (it would have to be cached internally
        // and "re-added")
        //
        //Boolean option below is to enable/disable loadCatalogList db querying
        //i.e. by bypassing the catalog query just create empty list, thay way
        //user can "open" a specific id without waiting for default catalog to load.
        if (getProperties().getBoolean("catalog.loadFromFile")) { // aww 2014/10/02
            resetCatPanel(false, getProperties().getProperty("catalog.evidfile")); // aww 2014/10/02
        }
        else resetCatPanel(getProperties().getBoolean("autoLoadCatalog")); // try this -aww 2010/08/30
        //resetCatPanel(true);

        resetBarAndMenuStates();

        // No OpenMap catalog/station map frame by default, user must create it through toolbar button

        repaint(); // Toolbar not always painted - does this fix it - aww 01/22/2007 


    }  // end of makeGUI

    public void solutionChanged(SolutionEvent evt) {
        jiggle.mv.getSelectedSolution().setComment(evt.getSolution().getComment());
        this.updateFrameTitle();
    }

    private JMenuBar makeMenuBar() {
      menuBar = new JiggleMenuBar(Jiggle.this);
      return menuBar;
    }

    private JToolBar makeToolBar() {
        toolBar = new JiggleToolBar(Jiggle.this);
        return toolBar;
    }
    private JTabbedPane makeTabPane() {
        msgTabPopup = null;
        msgTextArea = null;
        //
        // KLUDGE is use of SCROLL_TAB_LAYOUT (rather than the default WRAP_TAB_LAYOUT)
        // JTabbedPane seems to update repaint "tab header area" better with WFV changes
        //1.4 code:
        tabPane = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT); // aww 02/24/2005
        //tabPane = new JTabbedPane(JTabbedPane.TOP); // aww 02/24/2005
        // create the tab panels (note the length-1, thus waveform tab is no made yet)
        //tabPane.setBorder(BorderFactory.createLineBorder(Color.GREEN)); // test to see boundary
        for (int i = 0; i < tabPanel.length - 1; i++) {
            makeTabPanel(tabPane, i);
        }
        // if you don't set min size the splitpane divider won't move because
        // the contained components won't allow themselves to get small enough
        tabPane.setMinimumSize(new Dimension(125, 50));

        // TEMP KLUDGE: Force a GUI frame repaint upon WAVEFORM_TAB selection,
        // to overcome some bug either in JTabbedPane tab panel redraw or
        // in the  jiggle xxxWF...Panel graphics implementation, or both?
        // JTabbedPane not repainted correctly when the mv selected wfv changes
        // (e.g. SelectableReadingList choice, ChangeEvent to PickingPanel in wfSplit).
        // Odd, why does a wfv change effect drawing of the tab panel?
        // (see scalePanel() in paintComponent() and getPreferredSize() for zwfp)
        // The "dirty area" is around perimeter of zwfp part of tabpane when test
        // borders are drawn.
        // Garbage still apparent even with transparent WFPanel setOpaque(false)
        // still there when switching tab context from SelectedReading update of wfv.
        // NOTE: JTabbedPane "header tabs" paint between zwfp timescale and toolbar.
        tabPane.addChangeListener(
            new ChangeListener() {
              public void stateChanged(ChangeEvent e) {
                  final JTabbedPane jtp = (JTabbedPane) e.getSource();
                  if (jtp.getSelectedIndex() == TAB_WAVEFORM) {
                    if (pickPanel != null) {
                      //tabPane.revalidate(); // hmm? doesn't restore "tabs" aww 12/18/03
                      //tabPane.repaint(); // similar to above, doesn't restore "tabs" aww
                      Jiggle.this.repaint(); // restores tabpane dirty area - aww
                    }
                  }
                  else if (jtp.getSelectedIndex() == TAB_MESSAGE) {
                      if (msgTextArea != null) msgTextArea.requestFocus();
                  }
                  else if (jtp.getSelectedIndex() == TAB_LOCATION) {
                      if (srlPhases != null) srlPhases.requestListFocus();
                  }
                  else if (jtp.getSelectedIndex() == TAB_MAGNITUDE) {
                      if (prefMagTabPane != null) {
                          SelectableReadingList srl =
                               (SelectableReadingList) prefMagSelectableReadingLists.get(prefMagTabPane.getTitleAt(prefMagTabPane.getSelectedIndex()));
                          if (srl != null) srl.requestListFocus();
                      }
                  }
                  Jiggle.this.updateFrameTitle();
              }
            }
        );

        return tabPane;
    }

    /**
    * Create a tab pane using info for tab number 'num'.
    */
    private void makeTabPanel(JTabbedPane tabPane, int num) {
        tabPanel[num] =  new JPanel(new BorderLayout(), true); // dblBuff see no diff. changed to true from false 11/19/03
        tabPanel[num].add(new JLabel(tabTitle[num]), BorderLayout.CENTER);
        //tabPane.addTab(tabTitle[num], null, tabPanel[num], tabTip[num]);
        tabPane.addTab( (tabImage[num] == null) ? tabTitle[num] : null,
                tabImage[num], tabPanel[num], tabTip[num]);
    }

    /** - Not Used? - aww
    * An empty panel used as a filler until real data is loaded
     private Component makeTextPanel(String text) {
         JPanel panel = new JPanel(false);
         JLabel filler = new JLabel(text);
         filler.setHorizontalAlignment(JLabel.CENTER);
         panel.setLayout(new GridLayout(1, 0));
         panel.add(filler);
         return panel;
     }
    */

    private JSplitPane makeWaveformSplitPane() {

        //if (getVerbose()) System.out.println(">>> Creating PickingPanel.");
        // <PickingPanel> placeholder, replaced when an event is selected
        JPanel pickPanelTmp = new JPanel(new BorderLayout(), false);
        pickPanelTmp.add(new JLabel("Picking Panel."), BorderLayout.CENTER);

        //if (getVerbose()) System.out.println(">>> Creating Scroller.");
        // <scrolling WFGroupPanel> placeholder, replaced when an event is selected
        JPanel scrollerTmp = new JPanel(new BorderLayout(), false);
        scrollerTmp.add(new JLabel("Waveform Scroller."), BorderLayout.CENTER);

        if (getVerbose()) System.out.println(">>> Creating waveformSplitPane.");
        // <wfSplit> make a split pane with the pickPanel and wfScroller
        JSplitPane wfSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT,    // 0 value
               false,       // don't repaint until resizing is done
               pickPanelTmp,      // top component
               scrollerTmp);      // bottom component

        wfSplit.setOneTouchExpandable(true);
        // if you don't set min size the splitpane divider won't move because
        // the contained components won't allow themselves to get small enough
        wfSplit.setMinimumSize(new Dimension(125, 125));
        wfSplit.setResizeWeight(0.);
        /*wfSplit.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                System.out.println(e.toString());
            }
        });
        */
        wfSplit.addPropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, new PropertyChangeListener() {
             public void propertyChange(PropertyChangeEvent pce) {
                if (pce.getPropertyName().equals(JSplitPane.DIVIDER_LOCATION_PROPERTY)) {
                      lastWfDividerLoc = Jiggle.this.wfSplit.getDividerLocation();
                  //System.out.println("loc: " + lastWfDividerLoc  + " last: " + Jiggle.this.wfSplit.getLastDividerLocation() + 
                  //    " min: " + Jiggle.this.wfSplit.getMinimumDividerLocation() + " max: " + Jiggle.this.wfSplit.getMaximumDividerLocation());
                }
            }
        });

        return wfSplit;
    }

    private JSplitPane makeCatMapSplitPane() {

        //if (getVerbose()) System.out.println(">>> Creating PickingPanel.");
        // <PickingPanel> placeholder, replaced when an event is selected
        JPanel mapPanelTmp = new JPanel(new BorderLayout(), false);
        mapPanelTmp.add(new JLabel("Map"), BorderLayout.CENTER);

        JPanel catPanelTmp = new JPanel(new BorderLayout(), false);
        catPanelTmp.add(new JLabel("Catalog"), BorderLayout.CENTER);

        if (getVerbose()) System.out.println(">>> Creating catMapSplitPane.");

        int orient = JSplitPane.VERTICAL_SPLIT;    // 0 value vertical left/right
        if (! getProperties().isSpecified("mapSplitOrientation") ) {
            getProperties().setProperty("mapSplitOrientation", String.valueOf(orient));
        }
        else orient = getProperties().getInt("mapSplitOrientation"); // 1 for horizontal top/bottom

        JSplitPane cmSplit = new JSplitPane(orient,
               false,       // don't repaint until resizing is done
               catPanelTmp,      // left component
               mapPanelTmp);      // right component

        cmSplit.setOneTouchExpandable(true);
        // if you don't set min size the splitpane divider won't move because
        // the contained components won't allow themselves to get small enough
        cmSplit.setMinimumSize(new Dimension(50, 50));
        cmSplit.setResizeWeight(1.);
        return cmSplit;
    }

    protected void updateStatusBar() {
        statusBar.updateDataSourceLabels();  // datasource, wfsource, etc.
        statusBar.updateLocServiceLabel();  // current service name
        statusBar.updateVelModelLabel(); // default traveltime model
    }

    private JiggleStatusBar makeStatusPanel() {
        JiggleStatusBar statusBar = new JiggleStatusBar(Jiggle.this);
        // get statusPanel which is a sub-component of the statusBar
        statusPanel = statusBar.getStatusPanel();
        statusPanel.setProgressStringPainted(false); // don't write out %completed
        statusBar.updateDataSourceLabels();  // datasource, wfsource, etc.
        statusBar.updateLocServiceLabel();  // service name
        statusBar.updateVelModelLabel(); // default traveltime model

        // Auxillary helper statusFrame can be used to create popup
        // to inform user of work being done before starting
        // work processing in Swing Event thread
        // if not reusing it dispose the old one before creating new
        // to release resources
        //if (statusFrame == null) statusFrame = new WorkerStatusDialog(Jiggle.this, false); // was modal - aww 2013/03/15
        if (statusFrame == null) statusFrame = new StatusDialog(Jiggle.this, false); // was modal - aww 2013/03/15
        return statusBar;
    }


    protected void initStatusPanelForWork(String message) { // temp change from private access - aww
        if (statusPanel == null) return;
        statusPanel.setProgressBarValue(0, message);
        statusPanel.setProgressIndeterminate(true); // aww don't have any feedback
    }

    protected void resetStatusGraphicsForThread() {
        SwingUtilities.invokeLater(
          new Runnable() {
            public void run() {
                statusFrame.unpop();
            //  BEWARE: clear() resets text and the progress bar status that
            //  could have been set by another thread that hasn't finished)
            //  how do you share it? maybe have a check in status panel as to
            //  what thread object configured it?
                statusPanel.clear();
            }
          }
        );
    }

    protected void initStatusGraphicsForThread(String title, String message) {
        final String t = title;
        final String str = message;
        Runnable r =
          new Runnable() {
            public void run() {
                initStatusPanelForWork(str);
                statusFrame.setBeep(beep);
                statusFrame.pop(t, str, true);
            }
          };
        if ( SwingUtilities.isEventDispatchThread() ) {
            r.run();
        } else SwingUtilities.invokeLater(r);
    }

    protected void updateStatusTextForThread(String text) {
      final String str = text;
      SwingUtilities.invokeLater(
          new Runnable() {
            public void run() {
              statusPanel.setText(str);
              statusFrame.setText(str);
            }
          }
      );
    }
    /**
     * Controls user preference of where waveforms are displayed.
     * If waveforms are in a split pane put them in a tab and visa versa.
     */
    protected void toggleTabSplit() {
        if (getProperties().getBoolean("waveformInTab")) {
          // wfSplit is in tabPane now, change it to mainSplitPane.
          setWaveformsInMainSplit(wfSplit);
        } else {
          // wfSplit is in mainSplitPane now, change it to tabPane.
          setWaveformsInTabPane(tabPane, wfSplit);
        }
        validate(); // redundant force re-layout of frame aww
        repaint();  // redundant aww
    }

    private void setWaveformsInMainSplit(JSplitPane wfSplit) {
        if (tabPanel[TAB_WAVEFORM] != null) {
          //tabPanel[TAB_WAVEFORM].remove(wfSplit); // removes waveforms from tab
          tabPanel[TAB_WAVEFORM].removeAll(); // removes waveforms from tab
          //tabPane.remove(tabPanel[TAB_WAVEFORM]); // remove the tab
          int idx = tabPane.indexOfComponent(tabPanel[TAB_WAVEFORM]); // does it already exist ?
          if (idx >= 0) tabPane.removeTabAt(idx); // if so remove it
          tabPane.revalidate(); // aww, redundant insurance?
          tabPanel[TAB_WAVEFORM] = null; // gc ?
        }
        // remove tabPane from contentPane of Jiggle
        if (mainSplit != null) getContentPane().remove(mainSplit);
        else if (catMapSplit != null) getContentPane().remove(catMapSplit); // test split - aww 2010/07/21

        mainSplit = new JSplitPane(Integer.parseInt(getProperties().getProperty("mainSplitOrientation","0")), // 0 by default => VERTICAL
               false,      // don't repaint until resizing is done
               wfSplit,    // top/left component
               catMapSplit);   // bottom/right component // test split - aww 2010/07/21
        mainSplit.setOneTouchExpandable(true);  //add widget for fast expansion

        // put mainSplitPane in contentPane of Jiggle
        getContentPane().add(mainSplit, BorderLayout.CENTER);
        getProperties().setProperty( "waveformInTab", false);
    }

    private void setWaveformsInTabPane(JTabbedPane tabPane, JSplitPane wfSplit) {
        if (mainSplit != null) {
          getContentPane().remove(mainSplit);
          mainSplit = null;
        }

        int idx = tabPane.indexOfComponent(tabPanel[TAB_WAVEFORM]); // does it already exist ?
        if (idx >= 0) {
          tabPane.removeTabAt(idx); // if so remove it
          tabPanel[TAB_WAVEFORM].removeAll(); // remove all waveforms from old panel
        }

        makeTabPanel(tabPane, TAB_WAVEFORM); // creates new TAB_WAVEFORM panel and new tab pane
        // put waveforms in new TAB_WAVEFORM panel
        //tabPanel[TAB_WAVEFORM].setBorder(BorderFactory.createLineBorder(Color.RED)); // test to see boundary
        tabPanel[TAB_WAVEFORM].add(wfSplit, BorderLayout.CENTER);
        tabPanel[TAB_WAVEFORM].revalidate(); //aww, redundant insurance?
        // put tabPane in contentPane of Jiggle
        updateTabPanelWithSplitCatalog();
        if (catMapSplit != null) getContentPane().add(catMapSplit, BorderLayout.CENTER); // test split - aww 2010/07/21

        getProperties().setProperty( "waveformInTab", true);
        selectTab(TAB_WAVEFORM);
    }

    /**
    * Select a tab (bring it to front) using the tab enumeration. For example:
    * TAB_MESSSAGE. Thread-safe.
    */
    protected void selectTab(int tabType) {

      // protect against selecting a non-existent tab
      if (tabType == TAB_WAVEFORM &&
          !getProperties().getBoolean("waveformInTab")) return;

      final int tab = tabType;

    // do thread-safe because this may be called by other threads
      SwingUtilities.invokeLater(
          new Runnable() {
            public void run() {
              int idx = tabPane.indexOfTab(tabTitle[tab]);
              if (idx == -1) idx = tabPane.indexOfTab(tabImage[tab]);
              tabPane.setSelectedIndex(idx);
            }
          }
      );

    }

    /** Make panel containing catalog of events found in DataSource meeting filter criteria. */
    private CatalogPanel makeCatPane() {
        if (! validateConnection("Catalog Panel")) { 
            System.err.println("Jiggle making new Catalog Panel, but data source is closed.");
            // return new CatalogPanel(); // removed this 12/13/2006 -aww
        }

        // get list to columns to show in the table
        String catPanelColumns[] = getProperties().getStringArray("catalogColumnList");

        // if (whereEngine == null) initWhereEngine(); // remove, done by initEngines() - aww 2008/11/07

        if (getVerbose()) System.out.println(">>> Creating CatalogPanel.");
        return new org.trinet.jiggle.CatalogPanel(Jiggle.this, catPanelColumns);
    }

    protected void resetCatPanel() {
        resetCatPanel(true);
    }

    public void resetCatPanel(boolean tf) {
        resetCatPanel(tf, null);
    }

    // Note: to read in a list of events from a disk file we need
    // a FileChooser popup in CatalogPanel to select the file.
    // Create SolutionList from ids read from this file and
    // reset CatalogPanel with the newly created SolutionList.
    private SolutionList createCatalogPanelSolutionList(String idFileName) {
        SolutionList idList = new SolutionList();

        if (idFileName.equals("STATE")) { 
            Collection c = Solution.create().getByState(getProperties().getProperty("catalog.pcsState","Jiggle"));
            if (c != null) { idList.addAll(c); } 
            return idList;
        }

        int count = 0;
        BufferedReader idStream = null;
        try {
            if (idFileName.indexOf(GenericPropertyList.FILE_SEP) < 0) {
              idStream = new BufferedReader(new FileReader(getProperties().getUserFilePath()+GenericPropertyList.FILE_SEP+idFileName));
            }
            else idStream = new BufferedReader(new FileReader(idFileName));
            String idString = null;
            Solution factory = Solution.create();
            Solution sol = null;
            while (true) {
                idString = idStream.readLine();
                if (idString == null) {
                    break;
                }
                count++; // line counter
                if (idString.length() == 0) continue;

                sol = null;
                try {
                    sol = factory.getById( Long.parseLong(new StringTokenizer(idString).nextToken()) );
                }
                catch (Exception ex0) { // weird char in line ?
                    String str = "Cannot parse evid token in file line: " + count;
                    System.err.println(str);
                    System.err.println("\""+idString+"\"");
                    popInfoFrame(str, idFileName);
                    break;
                }
                if (sol != null) idList.add(sol);
            }
        }
        // upon error do what behavior? return the list, a null, or throw an exception?
        catch (NumberFormatException ex) {
            ex.printStackTrace();
            System.err.println("Error: Number format in input file: " + idFileName + " at line: " + count);
        }
        catch (FileNotFoundException ex) {
            ex.printStackTrace();
            System.err.println("Error: Input file not found: " + idFileName);
        }
        catch (IOException ex) {
            ex.printStackTrace();
            System.err.println("Error: i/o for input file: " + idFileName + " at line: " + count);
        }
        finally {
            try {
              if (idStream != null) idStream.close();
            }
            catch (IOException ex2) { }
        }
        return idList;
    }

    protected final boolean validateConnection(String label) {
        boolean status = true;
        if (DataSource.isNull() || DataSource.isClosed()) {
          status = makeDataSourceConnection();
        }
        else if (getProperties().getBoolean("testConnection")) { // invoke a test here e.g.  "select sysdate from dual"  - 2011/06/06 -aww
            if (!DataSource.testConnection()) status = makeDataSourceConnection(); // try to reconnect
        }

        if (status) statusBar.updateDataSourceLabels();
        else popInfoFrame("Validate Database Connection", "Invalid database connection for " + label);

        return status;
    }

    protected void setLoadCatalogPrefMags(String catColStr) {
        // Note added "Me" energy magnitude element !
        Solution.loadCatalogPrefMags =
            (catColStr.indexOf("Md") >= 0 || catColStr.indexOf("Ml") >= 0 || catColStr.indexOf("Mlr") >= 0 ||
             catColStr.indexOf("Mw") >= 0 || catColStr.indexOf("Me") > 0);
    }

    protected void resetCatPanel(boolean tf, final String idFileName) {

        if (! validateConnection("Catalog reset")) return; 

        if (!getProperties().getBoolean("autoLoadCatalog")) {
            int yn = JOptionPane.showConfirmDialog(Jiggle.this,
                             "Catalog loading by selection properties is disabled, Do you want to enable it?",
                             "Empty Event List",
                             JOptionPane.YES_NO_OPTION
                         );
            if (yn == JOptionPane.YES_OPTION) {
                getProperties().setProperty("autoLoadCatalog", true);
                tf = true;
            }
        }

        final boolean queryDb = tf && (idFileName == null ) && checkEventProperties(eventProps);
        //System.out.println("DEBUG Jiggle resetCatPanel queryDb:" +queryDb + " tf: " + tf + " idFileName=" + idFileName);

        final org.trinet.util.SwingWorker worker = new org.trinet.util.SwingWorker() {
          // NO GRAPHICS CALLS ALLOWED IN THIS METHOD UNLESS using SwingUtilites.invokeLater(new Runnable(...))!
          WorkerStatusDialog workStatus = new WorkerStatusDialog(Jiggle.this, false); // made not modal -aww 2010/08/30
          //workStatus.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
          String filename = idFileName;

          public Object construct() {
            workStatus.setBeep(beep);
            workStatus.pop("Catalog", "Creating catalog", true);

            setLoadCatalogPrefMags( getProperties().getProperty("catalogColumnList","") );

            haveCatSolList = false;
            //External edits of EventSelectionProperties via GUI must set catPane.catFileName null to override loading id file list:
            if (catPane != null && filename == null) filename = catPane.catFileName;
            // Do db query for snapshot of Solutions in DataSource for the catalog panel, may take awhile:
            if (filename == null) catSolList = (queryDb) ? loadCatalogList() : new SolutionList(0);
            else catSolList = createCatalogPanelSolutionList(filename); // returns non-null SolutionList
            haveCatSolList = true;
            return null;
          }

          //Runs on the event-dispatching thread so graphics calls are OK here.
          public void finished() {
            if (getVerbose()) System.out.println(">>> Creating catalog table of size: "+catSolList.size());
            // set/reset selected to Catalog selected event to that in MasterView
            // must do it by ID because loadCatalogList creates new Solution
            // instances in the for CatalogPanel, differing from those in MasterView.
            // get list to columns to show in the table
            CatalogPanel oldCP = catPane;
            if (oldCP != null) oldCP.destroy(); // added this to remove sollist LDSListener -aww 03/12/2009
            catPane = makeCatPane();
            if (catSolList != null && catPane != null) {
              catPane.setSolutionList(catSolList);
              resetCatPanelSelection();
              if (filename != null) catPane.catFileName = filename;
              firePropertyChange(NEW_CP_PROPNAME, oldCP, catPane);
            }

            updateTabPanelWithSplitCatalog();
            selectTab(TAB_CATALOG); // test split - aww 2010/07/21

            if (statusPanel != null) statusPanel.clearOnlyOnMatch("Resetting catalog ...");
            //workStatus.unpop(); // uses invokeLater event queue, just use dispose
            workStatus.dispose(); // don't invoke unpop before dispose (may cause exception)

            if (catPane != null && catSolList.size() == 0 && filename == null) {
                if (!scopeMode && getProperties().getBoolean("autoLoadCatalog")) {
                  int yn = JOptionPane.showConfirmDialog(Jiggle.this,
                             "No events found, Do you want to edit event selection properties?",
                             "Empty Event List",
                             JOptionPane.YES_NO_OPTION
                         );
                  if (yn == JOptionPane.YES_OPTION) catPane.doFilterOption("Edit", eventProps);
                }
            }

          }
        };
        initStatusPanelForWork("Resetting catalog ...");
        worker.start();
    }

    private void updateTabPanelWithCatalog(boolean tf) { // test split - aww 2010/07/21
        if (catPane == null) return;
        tabPanel[TAB_CATALOG].removeAll();
        tabPanel[TAB_CATALOG].add(catPane, BorderLayout.CENTER);
        tabPanel[TAB_CATALOG].revalidate(); // aww, redundant insurance?
        if (tf) {
            selectTab(TAB_CATALOG); // test split - aww 2010/07/21
        }
    }

    private void updateTabPanelWithSplitCatalog() {
        updateTabPanelWithCatalog(false); // test split - aww 2010/07/21
        if (catMapSplit == null) catMapSplit = makeCatMapSplitPane();
        if (catPane != null) catMapSplit.setLeftComponent(tabPane); // test split - aww 2010/07/21
        if (getProperties().getBoolean("mapInSplit")) {
          if (mapFrame != null && mapFrame instanceof JInternalFrame) catMapSplit.setRightComponent(new JScrollPane(mapFrame));
          catMapSplit.setOrientation(getProperties().getInt("mapSplitOrientation")); // 0 by default => VERTICAL
          catMapSplit.setResizeWeight(.45);
        }
        else {
            catMapSplit.setRightComponent(null);
            catMapSplit.setDividerLocation(1.);
        }

        if (mainSplit != null) mainSplit.setRightComponent(catMapSplit); // test split - aww 2010/07/21
    }
    protected void resetCatPanelSelection() {
        Solution sol = mv.getSelectedSolution();
        if (sol == null && catSolList != null) sol = (Solution) catSolList.getSelected();
        if (debug)
            System.out.println("Jiggle DEBUG resetCatPanelSelection to selection = " +
                ( (sol == null) ? "0" : sol.getId().toString() )
            );
        if (sol != null && catSolList != null && catPane != null) {
          catSolList.setSelected(sol); // aww 
          // catPane.updateTable(); // aww - refresh table and rehighlight the selection?
          catPane.updateTableRowSelection(); // aww - do this to refresh table and rehighlight the selection?
        }
    }

/**
 * Load the catalog list from the data source using parameters in
 * EventSelectionProperties instance for start and end times, etc. */
    public SolutionList loadCatalogList() {
      int maxRows = eventProps.getMaxCatalogRows();
      if (debug) {
        System.out.println(
            "Jiggle DEBUG loadCatalogList() event selection properties ...\n"+
            eventProps.listToString() +
            "Jiggle DEBUG loadCatalogList() creating SolutionList data from DataSource ... "
        );
        if (maxRows > 0) {
          System.out.println("INFO: Event selection properties has catalog max rows returned set to: " + maxRows);
        }
      }

      if (! isNetworkModeLAN() && catSolList != null) {
          int yn = JOptionPane.showConfirmDialog(Jiggle.this,
                           "Requery database for event catalog?",
                           "Network Mode WAN",
                           JOptionPane.YES_NO_OPTION);
          if (yn != JOptionPane.YES_OPTION) return catSolList;
      }

      if (! validateConnection("Catalog load")) return new SolutionList(0); 

      SolutionList aList = new SolutionList(eventProps);
      if ( maxRows > 0 && aList.size() >= maxRows ) {
       popInfoFrame("Jiggle Catalog Load", "Warning: Event catalog query reached limit of " + maxRows + " rows,\n" +
              "set by the \"maxCatalogRows\" event selection property value");
      }
      if (debug) {
          System.out.println(
                  "Jiggle DEBUG loadCatalogList() completed");
      }
      aList.setConnection(DataSource.getConnection()); // init of SolutionList does this too -aww 04/06/2006

      return aList;
    }

   /**
     * Toggle unpick mode on/off.
     */
    protected void setUnpickMode(boolean tf) {
        UnpickMode.set(tf);
        // could fire property change here if WFGroupPanel and PickingPanel
        // implement property change listeners which would change the
        // WPPanel cursor depending on the t/f property value
        // Cursor c = (tf) ? new Cursor(Cursor.DEFAULT_CURSOR) : new Cursor(Cursor.CROSSHAIR_CURSOR);
        // firePropertyChange("jiggleUnpickModeSetCursor", null, c);
        if (debug) System.out.println ("Jiggle DEBUG unpick mode = "+ tf);
    }


/** Get an new MasterView instance. */
    private MasterView clearMasterView(boolean eventMode) {

        if (wfScroller != null) {
            wfScroller.removeListeners(mv);
            if (wfScroller.jdHide != null) {
                wfScroller.jdHide.dispose();
                wfScroller.jdHide = null;
            }
        }
        if (pickPanel != null) {
            pickPanel.removeListeners(mv);
        }

        mv.phUndoList.clear(false);
        mv.amUndoList.clear(false);
        mv.coUndoList.clear(false);

        // if (solSolverDelegate != null)  solSolverDelegate.reset(); // clears results, reset() is invoked in loadSolution method

        if (! prefMagSelectableReadingLists.isEmpty()) {
          Iterator iter = prefMagSelectableReadingLists.values().iterator();
          SelectableReadingList srl = null;
          while (iter.hasNext()) {
              srl = (SelectableReadingList) iter.next();
              srl.destroy();
          }
          prefMagSelectableReadingLists.clear();
        }

        if (srlPhases != null) {
          srlPhases.destroy();
          srlPhases = null;
        }

        Solution oldSol = mv.getSelectedSolution();

        mv.removePropertyChangeListener(Jiggle.this); // b4 destroy doesn't notify list change -aww
        mv.destroy();
        // kludge to clean-up if wfCache2File=true, destroy() above calls WFView clear() unload timeseries makes new cache files !
        if (wfCache2FilePurged) {
            if (oldSol != null) {
                //System.out.println("Jiggle.clearMasterView() AbstractWaveform clearCacheDir for : " + oldSol.getId());
                AbstractWaveform.clearCacheDir(oldSol.getId().longValue(), null, null);
            }
        }
        mv = new MasterView(); // new MasterView(statusPanel); // changed to no-arg constructor 02/16/2005 -aww
        mv.setOwner(Jiggle.this);
        mv.addPropertyChangeListener(Jiggle.this);
        if (debug) System.out.println("MV: clearMasterView() created new default view"); 

        //if (eventMode) { // don't do below config for scope mode
          setMasterViewProperties();
          //mv.solList.addChangeListener(new SolListChangeListener(Jiggle.this)); // aww replaced by below
          mv.solList.addListDataStateListener(new JiggleSolutionListListener(Jiggle.this)); // aww
        //}
        if (debug) System.out.println("MV: clearMasterView() set new view model to current JiggleProperties"); 
        return mv;
    }

    // Process property change events from any listened to components members like MasterView
    // notify listeners like MapFrame
    public void propertyChange(PropertyChangeEvent evt) {
        String propName = evt.getPropertyName();
        if (propName.equals(MasterView.NEW_SLIST_PROPNAME) ) {
          if ( evt.getSource() == Jiggle.this.mv ) {
            firePropertyChange(NEW_MV_SLIST_PROPNAME, evt.getOldValue(), evt.getNewValue());
          }
        }
        else if (propName.equals(MasterView.NEW_WFVIEW_PROPNAME) ) {
          if ( evt.getSource() == Jiggle.this.mv ) {
            firePropertyChange(NEW_MV_WFVIEW_PROPNAME, evt.getOldValue(), evt.getNewValue());
          }
        }
        else if (propName.equals(EngineIF.SOLVED)) {
          firePropertyChange(propName, evt.getOldValue(),evt.getNewValue());
        }
        else if (propName.equals(WFPanel.FILTER_CHANGED)) {
          //System.out.println("DEBUG : Jiggle notifying WFPanel.FILTER_CHANGED");
          firePropertyChange(propName, evt.getOldValue(),evt.getNewValue());
        }    
}

//
// START of scope crap below here  -aww

    /*
       Need new button on toobar to switch to scope mode, and in scope mode we need:
       a new Scope settings panel popup:
       This panel has:
       1 datetimechooser for the WFView window endtime
       1 secs chooser or number text field for duration to get window start time
       1 editable jtextfield for list of channel ids to display
       1 button to invoke jfilechooser to get file to populate list in text area
    */
    //
    protected void scopeLoadMasterView(boolean showDialog) {
        int direction =  (scopeConfig == null) ? 0 : scopeConfig.lastDirection; 
        scopeLoadMasterView(showDialog, direction);
    }
    //
    protected void scopeLoadMasterView(boolean showDialog, int direction) {
        try {

            if (scopeConfig == null) {
                scopeConfig = new ScopeConfig(Jiggle.this);
            }

            if (!scopeConfig.processDialog(showDialog, direction)) {
                return;
            }

            // after dialog returns:
            scopeMode = scopeConfig.scopeModeCheckBox.isSelected();

            WaveServerGroup waveClient = null;
            if (scopeMode) {
                getProperties().setupWaveServerGroup(); // added 2009/03/24 -aww
                waveClient = getProperties().getWaveServerGroup();
                if (waveClient == null || waveClient.numberOfServers() <= 0) {
                    JOptionPane.showMessageDialog(Jiggle.this,
                            "Missing valid RT waveserver spec for Scope view, check Jiggle properties.",
                            "Scope RT Snapshot Failed",
                            JOptionPane.ERROR_MESSAGE
                    );
                    System.err.println("Error: scopeLoadMasterView getWaveServerGroup no/bad waveserver spec in properties file. ");
                    System.err.println("waveServerGroupList = " + props.getProperty("waveServerGroupList"));
                    scopeMode = false;
                }
            }

            if (!scopeMode) { // catalog mode
                AbstractWaveform.setWaveDataSource(getProperties().getWaveSource());
                statusBar.updateDataSourceLabels();
                // clearMasterView(true); this is done by loadSolution
                selectTab(TAB_CATALOG);
            } else { // scope mode
                AbstractWaveform.setWaveDataSource(waveClient);
                statusBar.updateDataSourceLabels();

                double scopeUserSelectedTime = scopeConfig.getScopeUserSetViewTime().getTrueSeconds(); // UTC leap seconds -aww 2008/05/13
                TimeSpan ts = null;
                if (scopeConfig.isUseStartTime()) {
                    // When using startTime, add duration to the user's scopeTime
                    double endTime = scopeUserSelectedTime + scopeConfig.scopeDuration;
                    ts = new TimeSpan(scopeUserSelectedTime, endTime);
                } else {
                    double startTime = scopeUserSelectedTime - scopeConfig.scopeDuration;
                    ts = new TimeSpan(startTime, scopeUserSelectedTime);
                }

                if (makeScopeMasterView(scopeConfig.scopeChanList, ts, scopeConfig.isNew)) {
                    scopeConfig.isNew = false;
                }
            }
        } catch (Exception ex) {
            System.err.println(ex.toString());
            ex.printStackTrace();
        }
    }
      

    private boolean makeScopeMasterView(ChannelableListIF chanList, final TimeSpan ts, boolean isNew) {
        if (! validateConnection("Scope view")) return false; 

        if (chanList.size() == 0) {
            JOptionPane.showMessageDialog(Jiggle.this,
                 "Channel name list empty, check N.S.C.L channel name strings in Scope setup dialog.",
                 "Scope RT Snapshot Failed",
                  JOptionPane.ERROR_MESSAGE
            );
            return false;
        }

        selectTab(TAB_WAVEFORM); // make waveforms panel visible

        if (isNew) {
            //AbstractWaveform.setWaveDataSource( getProperties().getWaveServerGroup() );
            // make a new view list with current time window
            ArrayList viewList = new ArrayList(chanList.size());

            // make a list of channel time windows
            //System.out.println("Snapshot mode time span: " + ts);
            for (int i = 0 ; i< chanList.size(); i++) {
                viewList.add(new  ChannelTimeWindow( ((Channelable)chanList.get(i)).getChannelObj(), ts));
            }

            Solution oldSol = mv.getSelectedSolution();
            if (oldSol != null) {
                if (oldSol.getNeedsCommit()) {
                  int answer = JOptionPane.showConfirmDialog(Jiggle.this,
                          "ChannelList changes require a new MasterView,\n Save your current event's data?",
                          "MasterView View List Changed", JOptionPane.YES_NO_OPTION);
                  if (answer == JOptionPane.YES_OPTION) {
                    saveToDb(oldSol);
                  }
                }
                //if (wfCache2FilePurged) {
                //    AbstractWaveform.clearCacheDir(oldSol.getId().longValue(), null, null);
                //}
            }


            //mv.destroy(); // destroy previous, free listeners, etc.
            //mv = new MasterView(); // define new Master View
            mv = clearMasterView(false); // try this instead of above -aww 2009/03/12
            mv.defineByChannelTimeWindowList(viewList);
            //mv.setWaveformLoadMode(MasterView.LoadAllInBackground);
            //mv.setWaveformLoadMode(MasterView.Cache);
            resetGUI(true);
            scopeConfig.myMv = mv;
            firePropertyChange(NEW_MV_PROPNAME, null, mv); // notification here for new masterview property change listeners 
        }
        else { // only change in timespan, reset spans and reload timeseries 
            if (debug) System.out.println("Jiggle resetting MasterView spans...");
            mv.setAllViewSpans(ts, true);
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    mv.masterWFWindowModel.setTimeSpan(ts); // THIS FIRES WFWindowModel change EVENT!
                    //mv.masterWFWindowModel.setCenterTime(ts.getCenter()); // THIS FIRES WFWindowModel change EVENT!
                } 
            });
        }
        return true;
    }

// END of scope mode setup -aww

    private void setMasterViewProperties() {
       JiggleProperties jp = getProperties();
       if (jp == null) return;
       mv.verbose = jp.getBoolean("verbose");
       mv.setDebug(jp.getBoolean("debug"));
       //mv.setChannelTimeWindowModel(jp.getCurrentChannelTimeWindowModelInstance()); // aww 06/15/2005
       mv.setChannelTimeWindowModel(jp.getCurrentChannelTimeWindowModel()); // aww 06/15/2005
       mv.setResidualStripValue(jp.getDouble("pickStripValue"));
       mv.setResidualAmpStripValue(jp.getDouble("ampStripValue"));
       mv.setResidualCodaStripValue(jp.getDouble("codaStripValue"));
       mv.setClockQualityThreshold(jp.getDouble("clockQualityThreshold"));

       mv.ampRetrieveFlag = jp.getBoolean("masterViewRetrieveAmps", true);
       mv.codaRetrieveFlag = jp.getBoolean("masterViewRetrieveCodas", true);
       mv.phaseRetrieveFlag =jp.getBoolean("masterViewRetrievePhases", true);
       mv.loadSpectralAmps = jp.getBoolean("masterViewLoadSpectralAmps", false);
       mv.loadPeakGroundAmps = jp.getBoolean("masterViewLoadPeakGroundAmps", false);

       // Waveforms are loaded cache mode, unless a jiggle property overrides this default -aww
       int mode = MasterView.Cache; // = 3
       if (jp.isSpecified("masterViewWaveformLoadMode")) mode = jp.getInt("masterViewWaveformLoadMode");
       mv.setWaveformLoadMode(mode);
       // set cache size after load mode is set in case of side effect:
       mv.setCacheSize(jp.getInt("cacheAbove"), jp.getInt("cacheBelow"));

       mode = MasterView.AlignOnTime;
       if (jp.isSpecified("masterViewWaveformAlignMode")) mode = jp.getInt("masterViewWaveformAlignMode");
       mv.setAlignmentMode(mode);
       if (jp.isSpecified("masterViewWaveformAlignVel")) mv.setAlignmentVelocity(jp.getDouble("masterViewWaveformAlignVel"));
       mv.replacePhasesByStaType = jp.getBoolean("masterViewReplacePhasesByStaType");

       mv.badChannelListName = jp.getProperty("badChannelListName");         
    }


/** Clear the Master view, leaves GUI in virgin state, like at startup. */
    public void remakeGUI() {
        SwingUtilities.invokeLater(
            new Runnable() {
              public void run() {
                  releaseAllSolutionLocks();
                  // nuke the old MasterView
                  clearMasterView(true);
                  // remake the GUI
                  makeGUI();
              }
           }
      );
    }
/** Reset the GUI on the Event-Dispatch thread so graphics operations should be OK.
 * Resets tabs also.*/
    protected void resetGUI(boolean takeFocus) {
        final boolean focus = takeFocus;
        SwingUtilities.invokeLater(
            new Runnable() {
                public void run() {
                    resetGUIunsafe(focus);
                }
            }
        );
    }
    private void resetGUIunsafe(boolean takeFocus) {

        // resets bars, buttons, and menus based upon MasterView
        resetBarAndMenuStates();

        //set mainframe title to sol summary string and tab text
        updateTextViews();

        // reset waveform display
        resetWaveformDisplay(takeFocus);

        // cleanup possibly left over processing graphics
        resetStatusGraphicsForThread();

        // use MasterView selected solution, if any, here
        resetCatPanelSelection();

        validate(); //redundant layout of frame again? aww
        repaint(); // kludge to cleanup artifacts, again? aww
    }

    private void resetPickPanel() { // active WF zoomable
        oldZoomValue = "";
        if (pickPanel != null) {
          wfSplit.remove(pickPanel);  // needed?
          // remember old zoom value to pass it along to the next instance
          oldZoomValue = pickPanel.getZoomValue();
          pickPanel.removeListeners(mv);
          pickPanel.clear(); // resets wfp - aww
          pickPanel.removeAll();
          pickPanel.mv = null;
        }
        //pickPanel = new PickingPanel(mv); // aww new instance by default?
        pickPanel = new PickingPanel(); // aww new instance by default?
        pickPanel.setMasterView(mv);
        pickPanel.setFilterEnabled(true); // allow filtering
        pickPanel.setSightColor(getProperties().getSightLineColor());
        pickPanel.getActiveWFPanel().setShowCursorTimeAsLine(getProperties().getBoolean("showCursorTimeAsLine"));
        pickPanel.getActiveWFPanel().setShowCursorAmpAsLine(getProperties().getBoolean("showCursorAmpAsLine"));
        pickPanel.getActiveWFPanel().setShowPhaseCues(getProperties().getBoolean("showPhaseCues"));
        pickPanel.getActiveWFPanel().setShowSegments(getProperties().getBoolean("showSegments"));
        pickPanel.getActiveWFPanel().setShowSamples(getProperties().getBoolean("showSamples"));
        pickPanel.getActiveWFPanel().setShowDeltaTimes(getProperties().getBoolean("showDeltimes"));
        pickPanel.getActiveWFPanel().setShowResiduals(getProperties().getBoolean("showResiduals"));
        pickPanel.getActiveWFPanel().setAntialias(getProperties().getBoolean("antialiasWaveform")); // 10/14/2005 -aww added
        pickPanel.setScaleList(getProperties().getStringArray("zoomScaleList"));
        pickPanel.cursorLocPanel.setShowPhyUnits(getProperties().getBoolean("zoom.cursorShowsPhyUnits"));
        ZoomPanel.biasButtonOn = props.getBoolean("zoomBiasButtonOn", false);
        pickPanel.biasButton.setSelected(ZoomPanel.biasButtonOn);
        pickPanel.alwaysResetLowQualFm = props.getBoolean("phasePopupMenu.alwaysResetLowQualFm", false);
        pickPanel.phaseDescWtCheck = props.getBoolean("phasePopupMenu.phaseDescWtCheck", true); 
        //System.out.println(">>>>> DEBUG Jiggle reset pickPanel.phaseDescWtCheck=" + pickPanel.phaseDescWtCheck); 
        pickPanel.setMinimumSize(new Dimension(50, 50));

        if (jiggle.scopeMode) pickPanel.disablePicking(); // null pointer to pickPanel buttons here if used PickingPanel(mv) constructor is used
        else pickPanel.enablePicking();
        // System.out.println("Jiggle resetPickPanel jiggle.pickPanel.pickingEnabled: "  + jiggle.pickPanel.pickingEnabled);

        //pickPanel.setZoomValue(oldZoomValue);
        wfSplit.setTopComponent(pickPanel);
        // jdk1.4 wfSplit pickPanel,zoomPanel doesn't repaint properly loading waveforms! - aww
        wfSplit.revalidate();   // aww, redundant insurance?

    }
    private void resetBarAndMenuStates() {
        toolBar.resetUnpick();
        toolBar.resetHidePanelToggleButtons();
        toolBar.setMasterView();  // Sets origin list etc.
        // set/reset the min drag time
        mv.masterWFWindowModel.setMinTimeSize(getProperties().getFloat("minDragTime"));
        // No selected solution: disable some buttons and menu items
        boolean haveSol = (mv.getSelectedSolution() != null);
        toolBar.setEventEnabled(haveSol);
        menuBar.setEventEnabled(haveSol);
        statusBar.setEventEnabled(haveSol);
        // NOTE: these will not update dynamically!
        statusBar.setMasterView();  // Sets channelFinder. // aww
    }
/**
* Reset the GUI using the new MasterView. If 'takeFocus' is true the waveform
* tab will come to the front. This is not thread-safe
*/
    private void resetWaveformDisplay(boolean takeFocus) {
        // remake the zoom picking panel
        resetPickPanel();
        // remake the waveform scrolling group panel
        resetWFScroller();
        //reset selected WFPanel because PickingPanel and WFScroller are new
        resetSelectedWFPanel();
        //pop waveforms to foreground
        if (takeFocus) selectTab(TAB_WAVEFORM);

    }

    private void resetSelectedWFPanel() {
        // if waveforms are in a tab bring that tab to the front
        //  if (getProperties().getBoolean("waveformInTab")) selectTab(TAB_WAVEFORM);
        // Must reset selected WFPanel because PickingPanel and WFScroller are
        // new and must be notified (via listeners) of the selected WFPanel.
        // It might be null if no data is loaded, so default to the first WFPanel in the list
        WFView wfvSel = mv.masterWFViewModel.get();
        // none selected, use the 1st one in the scroller
        if (wfvSel == null) {
          if (mv.getWFViewCount() > 0) {
            wfvSel = (WFView) mv.wfvList.get(0);
            //mv.masterWFWindowModel.setFullView(wfvSel.getWaveform()); // always is that way when first loaded
            mv.masterWFViewModel.set(wfvSel,((mv.getAlignmentMode() != MasterView.AlignOnTime) ? 1 : 0));
          }
        } else {                               // just reset old view and window
          mv.masterWFViewModel.reset();
          mv.masterWFWindowModel.reset();
        }
        // pickPanel.setWFView(wfvSel); // kludge added by aww to update "No View" label
        // System.out.println("DEBUG Jiggle resetSelectedWFPanel old zoom value: " + oldZoomValue);
        if (pickPanel != null) {
           if (oldZoomValue != "") pickPanel.setZoomValue(oldZoomValue); // aww 08/26/2006 reset scale to previous value
           else {
              double scale = getProperties().getDouble("secsPerPage");
              if (scale != 0.) pickPanel.setZoomValue(String.valueOf(Math.abs(scale))); // aww 08/26/2006 reset scale to property value
           }
           pickPanel.cursorLocModel.setWFPanel(pickPanel.zwfp); // ??
        }
        if (wfvSel != null && mv.getAlignmentMode() == MasterView.AlignOnTime) {
            if (wfvSel.hasPhases())
                mv.masterWFWindowModel.setCenterTime(((Phase)wfvSel.phaseList.get(0)).getTime());
            else
                mv.masterWFWindowModel.setCenterTime(wfvSel.dataSpan.getCenter());
        }
        //mv.masterWFWindowModel.setFullAmp(); // test this for case of panel scaling by noise/gain

    }


/** Reset the scrolling group panel. */
    private void resetWFScroller() {
        WFScroller oldWfScroller = wfScroller;
        if (oldWfScroller != null) {
          wfSplit.remove(oldWfScroller);
          oldWfScroller.removeListeners(mv);
          oldWfScroller.clear(); // trashes WFPanels wfv - aww
          oldWfScroller.removeAll();
        }
        if (getProperties().getBoolean("showHideTraceMenu")) WFScroller.ENABLE_HIDE=true;
        if (getProperties().getBoolean("triaxialScrollZoomWithGroup")) WFScroller.triaxialScrollZoomWithGroup=true;
        String location =  getProperties().getProperty("groupPanelFilterButtonLocation", "bottom");
        if (location.equalsIgnoreCase("top")) {
            WFScroller.buttoncorner = ScrollPaneConstants.UPPER_RIGHT_CORNER; // must set static value before creating scroller -aww
        }
        else {
            WFScroller.buttoncorner = ScrollPaneConstants.LOWER_RIGHT_CORNER; // must set static value before creating scroller -aww
        }
        wfScroller = new WFScroller(mv, getProperties().getInt("tracesPerPage"), true); // new instance by default aww?
        //if (WFScroller.buttoncorner == ScrollPaneConstants.UPPER_RIGHT_CORNER) // this is done in WFScroller constructor
        //    wfScroller.setColumnHeaderView(new JLabel("  ")); // need columnHeader to make space for upper right corner button -aww
        wfScroller.showChannelLabels(getProperties().getInt("channelLabelMode"));
        wfScroller.groupPanel.setShowCursorTimeAsLine(getProperties().getBoolean("showCursorTimeAsLine"));
        wfScroller.groupPanel.setShowCursorAmpAsLine(getProperties().getBoolean("showCursorAmpAsLine"));
        wfScroller.groupPanel.wfpList.setShowPhaseCues(getProperties().getBoolean("showPhaseCues"));
        wfScroller.groupPanel.wfpList.setShowDeltaTimes(getProperties().getBoolean("showDeltimes"));
        wfScroller.groupPanel.wfpList.setShowResiduals(getProperties().getBoolean("showResiduals"));
        wfScroller.setSecondsInViewport(getProperties().getDouble("secsPerPage"));
        if (oldWfScroller != null) wfScroller.setShowFullTime(oldWfScroller.getShowFullTime());
        toolBar.updateFullTimeButton();
        wfScroller.setShowRowHeader(getProperties().getBoolean("showRowHeaders"));

        wfScroller.groupPanel.setTimeTicksFlag(getProperties().getInt("wfpanel.timeTicksFlag", 0));
        wfScroller.groupPanel.setShowTimeScale(getProperties().getBoolean("wfpanel.showTimeScale", true));
        wfScroller.groupPanel.setShowTimeLabel(getProperties().getBoolean("wfpanel.showTimeScaleLabel", true));

        wfScroller.setMinimumSize(new Dimension(50,125));

        wfSplit.setBottomComponent(wfScroller);

        //System.out.println("set loc to: " + lastWfDividerLoc  + " was last: " + Jiggle.this.wfSplit.getLastDividerLocation() + 
        //   " if > min: " + Jiggle.this.wfSplit.getMinimumDividerLocation() + " and < max: " + Jiggle.this.wfSplit.getMaximumDividerLocation());
        if (lastWfDividerLoc > wfSplit.getMaximumDividerLocation()) lastWfDividerLoc = wfSplit.getMaximumDividerLocation();

        if (lastWfDividerLoc > wfSplit.getMinimumDividerLocation()) {
                    wfSplit.setDividerLocation(lastWfDividerLoc); 
        }

        wfSplit.revalidate(); //aww, redundant insurance?

        //System.out.println(">>> DEBUG resetWFScroller secsPerPage: " + getProperties().getDouble("secsPerPage") +
        //           " secondsInViewport: " + wfScroller.getSecondsInViewport() +
        //           " showFull: " + wfScroller.getShowFullTime());

    }
/**
 * Sort the WFViewList by distance from the current
 * selected solution. Remake the WFScroller to reflect the new order.  */
    protected void resortWFViews() {
        Solution sol = mv.getSelectedSolution();
        // TODO: implement a phase PICK sort order and renable DIST sort here
        if (!mv.getChannelTimeWindowModel().getTriggerSortOrder().equalsIgnoreCase("DIST")) return; // aww 2012/02/06 - don't reshuffle trigger time sort?
        if (sol != null) resortWFViews(sol); //resortWFViews(sol.getLatLonZ());// aww 06/11/2004
    }
/**
 * Sort/resort the WFViewList by distance from the current
 * selected solution. Remake the WFScroller to reflect the new order.  */
    protected void resortWFViews(GeoidalLatLonZ latLonZ) { // aww 06/11/2004
        if (latLonZ == null || wfScroller == null) return;

        if (!latLonZ.hasLatLonZ()) {
            int yn = JOptionPane.showConfirmDialog(Jiggle.this,
                           "Reference location is undefined: lat,lon = 0, Skip sort?",
                           "Sort Waveform Views",
                           JOptionPane.YES_NO_OPTION);
            if (yn == JOptionPane.YES_OPTION) return;
        }

        mv.distanceSort(latLonZ);
        mv.alignViews(); // for P,S,V modes depend on distance -aww 2009/04/01
        //final WFView wfv = mv.masterWFViewModel.get();
        //resetGUI(false);   // false => do not bring waveform tab into focus // removed -aww 2009/03/25
        //resetWFScroller(); // instead do below 4 lines -aww 2009/03/26
        wfScroller.setViewportView(null);
        wfScroller.setRowHeaderView(null);
        wfScroller.getWFGroupPanel().resortPanels();
        wfScroller.setViewportView(wfScroller.getWFGroupPanel());
        //
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
              if (wfScroller.getShowRowHeader()) {
                // seems to synch labels here rather than when done above -aww 2009/03/26
                wfScroller.setRowHeaderView(wfScroller.getWFGroupPanel().getRowHeaderPanel());
              }
              /* REMOVED instead of scrolling to last viewed trace, always scroll back to  at top
              WFPanel wfp = wfScroller.getWFGroupPanel().getWFPanel(wfv);
              if (wfp != null) {
                  wfScroller.makePanelVisible(wfp);
                  wfp.setSelected(true);
              }
              */
              //wfScroller.getViewport().setViewPosition(new Point(0,0));

              int panelCnt = wfScroller.groupPanel.wfpList.size();
              if (panelCnt == 0) return;
              wfScroller.makePanelVisible((WFPanel)wfScroller.groupPanel.wfpList.get(0), mv.getAlignmentMode());  // reset centertime and scroll to 1st
              wfScroller.centerViewportOnPanel((WFPanel)wfScroller.groupPanel.wfpList.get(0)); // center 1st around the new center time
              //mv.masterWFViewModel.selectFirst(mv.wfvList); //select closest channel (commented out here to preserve last zoomed view) -aww 2009/04/13
            }
        });
        pickPanel.makeChannelLabel(); // update distance range
        //
    }

/**
* Update all the tab info, and the frame header. This is "thread-safe".
*/
    public void updateTextViews() {
      SwingUtilities.invokeLater(
          new Runnable() {
            public void run() {
              // reset frame title bar to reflect new location
              Solution sol = mv.getSelectedSolution();
              if (sol == null) {
                //dumpStringToTab("No event selected", TAB_WHERE, false);
                dumpStringToTab("No event selected", TAB_MAGNITUDE, false);
                dumpStringToTab("No event selected", TAB_LOCATION, false);
              } else {
                updateFrameTitle();
                updateLocationTab();
                updateMagTab();
                updateCatalogTab(); //aww 11/13 test activation
                updateStatusBarCounts();  // 10/18/2006 aww
                if (mapPanel != null) // force update of map if any
                    ((JiggleMapBean)mapPanel.getMapBean()).getMasterViewLayer().setSelectedSolution(sol);
              }
            }
          }
         );
    }

    protected void updateStatusBarCounts() {  // 10/18/2006 aww
        //statusBar.setMasterView();
        statusBar.setCounts();
    }

    /** Update Frame title bar with current Solution description. */
    public void updateFrameTitle() {
        StringBuffer sb = new StringBuffer(132);
        Solution sol = mv.getSelectedSolution();
        if (sol != null) {
          sb.append((sol.isStale() ? "STALE " : ""));
          sb.append(sol.toSummaryString());
          // append comment if there is one
          if (sol.hasComment()) sb.append(" [").append(sol.getComment()).append("]");
          //if (sol.depthFixed) sb.append(" Z fixed");
        }
        else {
          sb.append(DEFAULT_FRAME_TITLE);
        }
        DbaseConnectionDescription desc = getProperties().getDbaseDescription();
        sb.append("     ");
        sb.append(desc.getHostName()).append("/");
        sb.append(desc.getDbaseName()).append("/");
        sb.append(desc.getUserName());
        sb.append(" UTC");
        String frameTitle = sb.toString();
        setTitle(frameTitle);
        setMapFrameTitle(frameTitle);
    }

    /** Update Map Frame title bar with input String. */
    protected void setMapFrameTitle(String title) {
        if (mapFrame == null) return;
        else if (mapFrame instanceof JFrame)
            ((JFrame)mapFrame).setTitle(title);
        else if (mapFrame instanceof JInternalFrame) 
            ((JInternalFrame)mapFrame).setTitle(title);
    }

    protected void updateCatalogTab() { // aww 11/13 test
        if (debug) System.out.println("DEBUG updateCatalogTab()");
        catPane.update();
        resetCatPanelSelection();
    }

    // Used by Swarm waveform panel
    public void setJiggleCursorToTime(double dt) {
            if (Double.isNaN(dt)) {
                jiggle.pickPanel.zwfp.cursorTime = Double.NaN;
            }
            else {
                //pickPanel.zwfp.cursorTime = dt; // below is LeapSeconds version -aww
                pickPanel.zwfp.cursorTime = LeapSeconds.nominalToTrue(dt); // modified for LeapSeconds UTC version 2008/07/18
            }
            pickPanel.zwfp.repaint();
    }

    public void setSwarmCursorToTime(double dt) {
        //if (swarmWaveViewFrame != null) swarmWaveViewFrame.timeChanged(gov.usgs.util.Util.ewToJ2K(dt)); // below is LeapSeconds version -aww
        if (swarmWaveViewFrame != null) swarmWaveViewFrame.timeChanged(gov.usgs.util.Util.ewToJ2K(LeapSeconds.trueToNominal(dt))); // UTC version 2008/07/18
    }

    private void initSwarmComponents() {
        try {
              swarmDataSource = new JiggleSwarmDataSource(jiggle);

              if (swarmWaveViewFrame != null) Jiggle.this.removeWindowListener(swarmWaveViewFrame);
              swarmWaveViewFrame = new JiggleSwarmWaveViewerFrame(swarmDataSource);
              Jiggle.this.addWindowListener(swarmWaveViewFrame); // do listen to jiggle window close

              swarmClipboard = new JiggleSwarmWaveClipboard();
              swarmClipboard.addTimeListener(swarmWaveViewFrame);
              swarmClipboard.setPreferredSize(swarmWaveViewFrame.getPreferredSize());

              JSplitPane swarmSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT, swarmWaveViewFrame, swarmClipboard);
              swarmSplit.setOneTouchExpandable(true);
              swarmSplit.setResizeWeight(1.);
              swarmSplit.addPropertyChangeListener(swarmWaveViewFrame);

              boolean internal = (getProperties().getBoolean("swarmInWaveformTabPane") && tabPanel[TAB_WAVEFORM] != null);
              if (internal) {
                  swarmComponent = createSwarmComponentForTabPane(swarmSplit);
                  swarmSplit.setDividerLocation(.5);
              }
              else {
                  swarmComponent = createFrameFromSwarmComponent(swarmSplit);
                  swarmSplit.setDividerLocation(.5);
              }

              swarmComponent.addComponentListener(swarmWaveViewFrame);
        }
        catch (Exception ex) { ex.printStackTrace(); }
    }

    protected void showSwarmWindow() {
        try {
          if (swarmComponent == null) {
              initSwarmComponents();
          }
          else if (swarmComponent instanceof JFrame) {
            JFrame jFrame = (JFrame) swarmComponent;
            jFrame.setVisible(true);
            if ( (jFrame.getExtendedState() & Frame.ICONIFIED) == 1) {
              if ((jFrame.getExtendedState() | Frame.ICONIFIED) == 1) 
                 jFrame.setExtendedState(Frame.NORMAL); // restored from iconified -aww
              else
                 jFrame.setExtendedState(Frame.MAXIMIZED_BOTH); // restored from iconified -aww
            }
            jFrame.toFront();
          }
          else {
            swarmComponent.setVisible(! swarmComponent.isVisible()); // toggle visibility -aww
            if (tabPanel[TAB_WAVEFORM] != null) tabPanel[TAB_WAVEFORM].revalidate();
          }

          if (swarmComponent.isVisible()) swarmComponent.requestFocus();

        }
        catch (Exception ex) { ex.printStackTrace(); }
    }
    
    private Container createSwarmComponentForTabPane(Container comp) {

        if (tabPanel[TAB_WAVEFORM] == null)  return comp;

        if (wfSplit != null) {
            Dimension d = wfSplit.getSize();
            comp.setPreferredSize( new Dimension(d.width,(int)(d.height*.45)));
        }
        tabPanel[TAB_WAVEFORM].add(comp, BorderLayout.SOUTH);
        tabPanel[TAB_WAVEFORM].revalidate();

        return comp;
    }

    private JFrame createFrameFromSwarmComponent(Container comp) {
        scJFrame = new JFrame("Swarm Utility Frame");
        Image image = IconImage.getImage("waveclip.gif");
        if (image != null) scJFrame.setIconImage(image);
        scJFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        scJFrame.getContentPane().add(comp);
        scJFrame.pack();

        Rectangle swarmRec = scJFrame.getBounds();
        Rectangle jiggleRec = Jiggle.this.getBounds(); 
        swarmRec.y = jiggleRec.y + (int) (.5 * jiggleRec.height);
        swarmRec.x = jiggleRec.x + (int) (.1 * jiggleRec.width);
        swarmRec.width = jiggleRec.width;
        scJFrame.setBounds(swarmRec);
        scJFrame.setVisible(true);

        if (menuBar == null && toolBar == null) { // only do once!
            Jiggle.this.addWindowListener(new WindowAdapter() { // close swarm frame upon jiggle window close
                public void windowClosing(WindowEvent evt) {
                    scJFrame.dispose();
                }
            });
        }

        return scJFrame;
    }

    protected void showWhereDialog() {
       if (whereEngine == null) initWhereEngine();  // used to be makeWhereEngine - aww 2008/06/09
       if (! validateConnection("Where Event")) return; 

       Solution sol = mv.getSelectedSolution();
       String head = "Where Event";

       String whereString = "No event is selected in the loaded waveform view";
       LatLonZ latLonZ = null;
       if (sol != null) {
           head = "Event "+ sol.id.longValue();
           latLonZ = sol.getLatLonZ();
           if (latLonZ != null && !latLonZ.isNull()) {
               Format f74 = new Format("%7.4f");
               head += " lat,lon: " + f74.form(latLonZ.getLat())+", "+f74.form(latLonZ.getLon());
               whereString = whereEngine.where(latLonZ.getLat(), latLonZ.getLon(), latLonZ.getZ());
           }
           else whereString = "has no location";
       }

       JTextArea jta = new JTextArea(whereString);
       jta.setBackground(getBackground());
       jta.setEditable(false);

       popInfoFrame(head, jta);
    }

/**
* Update the text in the Magnitude tab pane with the latest info.
*/
    protected void updateMagTab() {
       //System.out.println("DEBUG updateMagTab(null)");
       updateMagTab((String) null);
    }

    protected void updateMagTab(String magType) {
      if (debug) System.out.println("DEBUG updateMagTab()");

      Solution sol = mv.getSelectedSolution();

      if (sol == null)  {
          dumpStringToTab("No selected solution.", TAB_MAGNITUDE, false);
          return;
      }

      Collection prefMags = sol.getPrefMags();
      if (prefMags.size() == 0) {
        dumpStringToTab("No preferred magnitudes for this solution.", TAB_MAGNITUDE, false);
        return;
      }

      int idx = (prefMagTabPane == null) ? 0 : prefMagTabPane.getSelectedIndex();

      prefMagTabPane = makePrefMagTabPane(prefMags);

      if (magType != null) setMagTabByType(magType);
      else if (prefMagTabPane != null) prefMagTabPane.setSelectedIndex(idx);
      //System.out.println("DEBUG magtabpane idx selected: " + idx + " magType: " + magType);

      tabPanel[TAB_MAGNITUDE].removeAll();      // remove prevous contents
      tabPanel[TAB_MAGNITUDE].add(prefMagTabPane, BorderLayout.CENTER);
      tabPanel[TAB_MAGNITUDE].revalidate(); // aww, redundant insurance?

    }

   protected void setMagTabByType(final String magType) {
       SwingUtilities.invokeLater( new Runnable() {
           public void run() {
               //System.out.println("DEBUG selected now : " + prefMagTabPane.getSelectedIndex()); 
               if (prefMagTabPane == null || magType == null) return;
               int idx = prefMagTabPane.indexOfTab(magType);
               //System.out.println("DEBUG magType : \"" + magType + "\" set idx: " + idx);
               prefMagTabPane.setSelectedIndex(((idx >= 0) ? idx : 0));
           }
       });
    }

    private JTabbedPane makePrefMagTabPane(Collection prefMags) {
      if (prefMagTabPane == null)
          //1.4 code:
          prefMagTabPane = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT); // aww 02/24/2005
          //prefMagTabPane = new JTabbedPane(JTabbedPane.TOP); // aww 02/24/2005
      else
          prefMagTabPane.removeAll();

      Iterator iter = null;
      SelectableReadingList srl = null;

      if (! prefMagSelectableReadingLists.isEmpty()) {
          iter = prefMagSelectableReadingLists.values().iterator();
          while (iter.hasNext()) {
              srl = (SelectableReadingList) iter.next();
              srl.destroy();
          }
          prefMagSelectableReadingLists.clear();
      }

      iter = prefMags.iterator();

      while (iter.hasNext()) {

        final Magnitude prefMag = (Magnitude) iter.next();
        if (prefMag.isDeleted()) continue; // "hide" skips deleted magnitudes from used 2006/12/04 aww

        final String magTypeString = prefMag.getTypeString();
        //System.out.println("DEBUG makePrefMagTabPane prefMag:\n" + prefMag.toDumpString());

        srl = new SelectableReadingList(mv, prefMag);
        srl.setEditable(getProperties().getProperty("editableMagTypes", "Ml Md").indexOf(magTypeString) >= 0);  // aww -2016/02/17
        srl.addRecalcListener( // enable recalc button in list
          new ActionListener() {
              final String myMagType = magTypeString;
              public void actionPerformed(ActionEvent e) {
                  calcMag(myMagType);
              }
          },
          "Recalculate magnitude using readings (no change of event preferred)"
        );
        prefMagSelectableReadingLists.put(magTypeString, srl); // save the SelectableReadingList in map

        // Create panel to hold active Selectable list components
        JPanel aPanel =  new JPanel(new BorderLayout(), true);
        boolean isEventPreferred = prefMag.isPreferred();
        JLabel jlabel = new JLabel( (isEventPreferred) ? "EVENT PREFERRED " : "          " );
        if (! isEventPreferred) {
          MouseInputAdapter mouseMenu = new MouseInputAdapter() {
            public void mousePressed(MouseEvent evt) {
                if (evt.isPopupTrigger()) doPopup(evt);           
            }
            public void mouseClicked(MouseEvent evt) {
                if (evt.isPopupTrigger()) doPopup(evt);           
            }
            public void mouseReleased(MouseEvent evt) {
                if (evt.isPopupTrigger()) doPopup(evt);           
            }

            private void doPopup(MouseEvent evt) {
                JPopupMenu jpm = new JPopupMenu();
                AbstractAction a = new AbstractAction() {
                        final Magnitude myPrefMag = prefMag;
                        public void actionPerformed(ActionEvent e) {
                            setPreferredMagnitude(mv.getSelectedSolution(), myPrefMag);
                        }
                };
                a.putValue(Action.NAME, "Set Event Preferred");
                jpm.add( new JMenuItem(a) );

                a = new AbstractAction() {
                        final Magnitude myPrefMag = prefMag;
                        public void actionPerformed(ActionEvent evt) {
                            if (myPrefMag == null) return;
                            boolean doDelete = true;
                            if (myPrefMag.isPreferred()) {
                                popInfoFrame("Mag Delete", "Can't delete event preferred!");
                                System.out.println("Jiggle INFO: ABORTED delete of prefmag magid: " +
                                    myPrefMag.getIdentifier().toString() + " " + myPrefMag.toNeatString()); 
                            }
                            else { 
                                if (myPrefMag.getTypeSubString().equals(MagTypeIdIF.ML)) {
                                  java.util.List aList = myPrefMag.sol.getMagnitudeByType(MagTypeIdIF.MLR);
                                  if ( aList.size() > 0 ) {
                                      Magnitude mlr = (Magnitude) aList.get(0); 
                                      if (mlr.isPreferred()) {
                                          popInfoFrame("Magnitude Virtual Delete", "Can't delete the Mlr magnitude, event preferred!");
                                          doDelete = false;
                                      }
                                      else if ( ! mlr.isDeleted() ) {
                                          mlr.setDeleteFlag(true);
                                          int idx = prefMagTabPane.indexOfTab("Mlr");
                                          if ( idx > 0 ) {
                                              prefMagTabPane.removeTabAt(idx);
                                          }
                                          System.out.println("Jiggle INFO: User deleted Mlr prefmag magid: " +
                                              mlr.getIdentifier().toString() + " " + mlr.toNeatString()); 
                                      }
                                  }
                                }
                                if ( doDelete ) {
                                    if (! myPrefMag.sol.getNeedsCommit()) {
                                        System.out.println("Jiggle INFO: Solution needs commit to delete prefmag association in database");
                                        myPrefMag.sol.setNeedsCommit(true);
                                        if (srlPhases != null) srlPhases.resetSummaryTextArea();
                                    }
                                    myPrefMag.setDeleteFlag(true);
                                    removeTab(prefMagTabPane, myPrefMag.getTypeString());
                                    System.out.println("Jiggle INFO: User deleted prefmag magid: " +
                                        myPrefMag.getIdentifier().toString() + " " + myPrefMag.toNeatString()); 
                                } else {
                                    System.out.println("Jiggle INFO: ABORTED delete of prefmag magid: " +
                                       myPrefMag.getIdentifier().toString() + " " + myPrefMag.toNeatString()); 
                                }
                            }
                        }
                };
                a.putValue(Action.NAME, "Delete magnitude");
                jpm.add( new JMenuItem(a) );

                jpm.add(new JMenuItem("Cancel"));
                jpm.show((java.awt.Component) evt.getSource(), evt.getX(), evt.getY());
            };
          };
          aPanel.addMouseListener(mouseMenu);
        }

        aPanel.add(jlabel, BorderLayout.NORTH);
        aPanel.add(srl, BorderLayout.CENTER);
        JPanel buttonPanel = new JPanel();
        //JButton deleteButton = new JButton("Delete");
        Image image = IconImage.getImage("delete_xbold_red.gif");
        JButton deleteButton = (image == null) ?  new JButton("Delete") : new JButton(new ImageIcon(image));
        deleteButton.setActionCommand("Delete");
        deleteButton.setToolTipText("Delete magnitude, if not EVENT preferred");
        deleteButton.addActionListener(
            new ActionListener() {
                final Magnitude myPrefMag = prefMag;
                public void actionPerformed(ActionEvent evt) {
                    if (myPrefMag == null) return;
                    if (myPrefMag.isPreferred()) {
                        popInfoFrame("Magnitude Virtual Delete", "Can't delete the event preferred!");
                        System.out.println("Jiggle INFO: ABORTED delete of prefmag magid: " +
                            myPrefMag.getIdentifier().toString() + " " + myPrefMag.toNeatString()); 
                    } else {
                       int yn = JOptionPane.showConfirmDialog(Jiggle.this,
                           "YES, magnitude will no longer be the preferred of type if event is saved to db.",
                           "Magnitude Virtual Delete",
                           JOptionPane.YES_NO_OPTION);
                       if (yn == JOptionPane.YES_OPTION) {
                          // SwingWorker finalToDb may not be needed here similar to finalCommit().
                          // NOTE: isDeleted() -> doDeleteCommit -> dbaseDelete() -> EPREF.deletePreferredMag(magid)
                          boolean doDelete = true;
                          if (myPrefMag.getTypeSubString().equals(MagTypeIdIF.ML)) {
                            java.util.List aList = myPrefMag.sol.getMagnitudeByType(MagTypeIdIF.MLR);
                            if ( aList.size() > 0 ) {
                                Magnitude mlr = (Magnitude) aList.get(0); 
                                if (mlr.isPreferred()) {
                                    popInfoFrame("Magnitude Virtual Delete", "Can't delete the Mlr magnitude, event preferred!");
                                    doDelete = false;
                                } else if ( ! mlr.isDeleted() ) {
                                    mlr.setDeleteFlag(true);
                                    System.out.println("Jiggle INFO: User deleted Mlr prefmag magid: " +
                                        mlr.getIdentifier().toString() + " " + mlr.toNeatString()); 
                                    int idx = prefMagTabPane.indexOfTab("Mlr");
                                    if ( idx > 0 ) {
                                        prefMagTabPane.removeTabAt(idx);
                                    }
                                }
                            }
                          }
                          if ( doDelete ) {
                              if (!myPrefMag.sol.getNeedsCommit()) {
                                  System.out.println("Jiggle INFO: Solution needs commit to delete prefmag association in database");
                                  myPrefMag.sol.setNeedsCommit(true);
                                  if (srlPhases != null) srlPhases.resetSummaryTextArea(); //
                              }
                              myPrefMag.setDeleteFlag(true);
                              System.out.println("Jiggle INFO: User deleted prefmag magid: " +
                                  myPrefMag.getIdentifier().toString() + " " + myPrefMag.toNeatString());
                              removeTab(prefMagTabPane, myPrefMag.getTypeString());
                          }
                       } else {
                            System.out.println("Jiggle INFO: ABORTED delete of prefmag magid: " +
                                myPrefMag.getIdentifier().toString() + " " + myPrefMag.toNeatString()); 
                       }
                   }
                };
            });

          deleteButton.setEnabled(! isEventPreferred);

        //JButton prefButton = new JButton("Preferred");
        image = IconImage.getImage("accept_check_thin_green.gif");
        JButton prefButton = (image == null) ? new JButton("Preferred") : new JButton(new ImageIcon(image));
        prefButton.setActionCommand("Preferred");
        prefButton.setToolTipText("Set EVENT preferred");
        prefButton.addActionListener(
            new ActionListener() {
                final Magnitude myPrefMag = prefMag;
                public void actionPerformed(ActionEvent evt) {
                    if (myPrefMag == null) return;
                    if (! myPrefMag.isPreferred()) {
                        setPreferredMagnitude(mv.getSelectedSolution(), myPrefMag);
                    }
                }
            }
        );

        buttonPanel.add(Box.createGlue());
        buttonPanel.add(deleteButton);
        if (! isEventPreferred) buttonPanel.add(prefButton);
        buttonPanel.add(Box.createGlue());
        aPanel.add(buttonPanel, BorderLayout.SOUTH);

        // Add panel to a tab in tabPane, make the event preferred 1st tab
        if ( isEventPreferred ) {
            prefMagTabPane.insertTab(magTypeString, null, aPanel, "Preferred magnitude of type", 0);
            //prefMagTabPane.setSelectedIndex(prefMagTabPane.indexOfTab(magTypeString)); // removed 2008/03/03 -aww
        }
        else prefMagTabPane.addTab(magTypeString, null, aPanel, "Preferred magnitude of type");
      } // end of iteration
      return prefMagTabPane;
    }

    /** Check if tab exists before removing a tab from tabpane **/
    private void removeTab(JTabbedPane tabgPane, String tabName) {
        int idx = tabgPane.indexOfTab(tabName);
        if (idx > 0) {
            tabgPane.removeTabAt(idx);
        }
    }

    protected void resetSummaryTextAreas() {
        if (jiggle.srlPhases != null) jiggle.srlPhases.resetSummaryTextArea(); // aww 01/07/2008
        if (! prefMagSelectableReadingLists.isEmpty()) {
            Iterator iter = prefMagSelectableReadingLists.values().iterator();
            SelectableReadingList srl = null;
            while (iter.hasNext()) {
                srl = (SelectableReadingList) iter.next();
                srl.resetSummaryTextArea(); 
            }
        }
    }

/**
* Update the text in the Location tab pane with the latest info.
*/
    protected void updateLocationTab() {
        if (debug) System.out.println("DEBUG updateLocationTab()");

      LocationEngineIF engine = null;

      // DUMP the location engine results to the str for display in Message tab
      String str = "";

      if (solSolverDelegate != null) {
          engine = solSolverDelegate.getLocationEngine();
          //raw locEng results output is reset below by call to getInstName()
          //str = solSolverDelegate.getResultsString(HypoMagEngineDelegateIF.LOCATION);
          if (engine != null && engine.success()) str = solSolverDelegate.getPrtFileContents();
      }

      if (str == null || str == "" || str.startsWith("No such file")) {
          str = "Location engine has not been run for this event.";
          menuBar.setLocationServerEnabled(false);
      }
      else menuBar.setLocationServerEnabled(true);

      //if (debug) System.out.println("Jiggle DEBUG updateLocationTab string:\n" + str);

      dumpStringToTab(str, TAB_MESSAGE, false);

      LocationServiceDescIF lsd = getProperties().getLocationServerGroup().getSelectedService();
      JLabel serviceLabel = new JLabel((lsd == null) ? "No Location Service" : lsd.toString());
      serviceLabel.setBackground(new Color(Integer.parseInt("40e0d0",16))); // turquoise
      serviceLabel.setOpaque(true);

      Image image = IconImage.getImage("mini-doc.gif");
      ImageIcon icon = null;
      if (image != null) icon = new ImageIcon(image);
      JButton propsButton = (icon == null) ? new JButton("E") : new JButton(icon);
      propsButton.setMinimumSize(new Dimension(18,18));
      propsButton.setMaximumSize(new Dimension(18,18));
      propsButton.setPreferredSize(new Dimension(18,18));
      propsButton.setToolTipText("Change location server setting");
      propsButton.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
              doPreferencesDialog(PreferencesDialog.TAB_LOCSERVER);
          }
      });

      JButton cmdButton = null;
      final JLabel cmdLabel = new JLabel();
      if (lsd != null) {

          // HYP 2000 command file:
          // Note this next call blows away the location results string 
          String name = (solSolverDelegate == null ) ? null : solSolverDelegate.getInstName();
          //Doing a reset() results in solserver EOT disconnect after auto recalc of magnitude -aww 2010/02/26
          //if (engine != null) engine.reset(); // clear name from results string, set back to "",
          if (engine != null) {
              engine.setStatusString("");
              engine.setResultsString(""); // clear hypinst. filename from results string, set back to ""
          }

          if (name != null) {
              // remove leading directory path
              int ii = name.lastIndexOf("/", name.length());
              if (ii < 0) ii = name.lastIndexOf("\\", name.length());
              if (ii > 0) {
                  name = name.substring(ii+1);
              }
          }
          cmdLabel.setText(" Command file= " + name);

          cmdLabel.setOpaque(true);
          cmdLabel.setBackground(new Color(Integer.parseInt("40e0d0",16))); // turquoise

          // if there's a location service and editing is enabled
          // add button for popup dialog that allows alternative hyp2000 command file selection and editing  -aww 2009/11/30
          if (getProperties().getBoolean("hypoinvCmdFileEditing")) {
              image = IconImage.getImage("center.gif");
              icon = null;
              if (image != null) icon = new ImageIcon(image);
              cmdButton = (icon == null) ? new JButton("C") : new JButton(icon);
              cmdButton.setMinimumSize(new Dimension(18,18));
              cmdButton.setMaximumSize(new Dimension(18,18));
              cmdButton.setPreferredSize(new Dimension(18,18));
              cmdButton.setToolTipText("Select the hypoinverse command file to use for this event on the solution server");
              cmdButton.addActionListener(new ActionListener() {
                  public void actionPerformed(ActionEvent e) {
                      DPHypinst instPanel = new DPHypinst(solSolverDelegate, getProperties());
                      JOptionPane.showMessageDialog(Jiggle.this, instPanel, 
                              "Hypoinverse Command File Setup", JOptionPane.PLAIN_MESSAGE);
                      cmdLabel.setText(" Command file= " + instPanel.getSelectedInstFile()); // should be same as below
                      //cmdLabel.setText(" Command file= " + solSolverDelegate.getInstName());
                  }
              });
          }
      }


      /*
      image = IconImage.getImage("mini-disk.gif");
      ImageIcon icon = null;
      if (image != null) icon = new ImageIcon(image);
      JButton arcButton = (icon == null) ? new JButton("D") : new JButton(icon);
      arcButton.setMinimumSize(new Dimension(18,18));
      arcButton.setMaximumSize(new Dimension(18,18));
      arcButton.setPreferredSize(new Dimension(18,18));
      arcButton.setToolTipText("");
      arcButton.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
              // Popup menu for arc file read/write into current Solution
              // clear phaseList,  (what about amps, codas?)
          }
      });
      */

      Box buttonBox = Box.createHorizontalBox();
      buttonBox.add(serviceLabel);
      buttonBox.add(cmdLabel);
      if (cmdButton != null) {
          buttonBox.add(cmdButton);
      }
      buttonBox.add(propsButton);
      //buttonBox.add(arcButton);
      buttonBox.add(Box.createHorizontalGlue());

      JPanel jp = new JPanel();
      jp.setLayout(new BorderLayout());
      jp.add(buttonBox, BorderLayout.NORTH);

      Solution sol = mv.getSelectedSolution();
      if (sol != null) {
          if (srlPhases != null) {
              srlPhases.destroy(); // for GC cleanup  -aww 2009/03/16
          }
          if (mv.getChannelTimeWindowModel().getTriggerSortOrder().equalsIgnoreCase("PICK")) {// aww 2012/02/06 - PICK time, not distance, sort
              sol.phaseList.timeSort(); // sort by time
              srlPhases = new SelectableReadingList(mv, sol, sol.phaseList, false); // no distance sort
          }
          else srlPhases = new SelectableReadingList(mv, sol, sol.phaseList);

          srlPhases.addRecalcListener( // enable recalc button in list
              new ActionListener() {
                  public void actionPerformed(ActionEvent e) {
                      // boolean tf = 
                      editEventParams(EventEditDialog.LLZTab);
                      /* Now prompt for locate
                      if (tf) {

                        int yn = JOptionPane.showConfirmDialog(
                                  Jiggle.this,
                                  "Relocate using current settings?",
                                  "Locate Event",
                                  JOptionPane.YES_NO_OPTION
                                 );

                        if (yn == JOptionPane.YES_OPTION) locate();
                      }
                      */
                  }
              },
          "Edit currently selected solution parameters"
          );
          jp.add(srlPhases, BorderLayout.CENTER);
          if (toolBar.solPanel.fixDepthButton != null) // - aww 2009/09/12 resynch fix color state for case where location algo fixed depth
              toolBar.solPanel.fixDepthButton.setSolution(sol);
      }

      tabPanel[TAB_LOCATION].removeAll();      // remove prevous contents
      tabPanel[TAB_LOCATION].add(jp, BorderLayout.CENTER);
      tabPanel[TAB_LOCATION].revalidate(); // aww, redundant insurance?
    }

/**
* Write text to scrolling text area in a tab panel.
* If select = true the tab will be selected, e.g. brought to the front.
*/
    protected void dumpStringToTab(String str, int tabNumber, boolean select) {
        if (msgTextArea == null) {
          msgTextArea = new JTextArea();
          msgTextArea.setEditable(false);
          msgTextArea.setLineWrap(false);
          msgTextArea.setFont(new Font("Monospaced", Font.PLAIN, 12));
          msgTextArea.addKeyListener(
              new java.awt.event.KeyAdapter() {
                  // event is fired on both key down and release so don't act until release
                  public void keyReleased(KeyEvent e) {
                      final int key = e.getKeyCode();
                      SwingUtilities.invokeLater(
                          new Runnable() {
                              public void run() {
                                  if (key == KeyEvent.VK_W) {
                                       selectTab(Jiggle.TAB_WAVEFORM);
                                  }
                                  else if (key == KeyEvent.VK_L) {
                                       selectTab(Jiggle.TAB_LOCATION);
                                  }
                                  else if (key == KeyEvent.VK_M) {
                                       selectTab(Jiggle.TAB_MAGNITUDE);
                                  }
                                  else if (key == KeyEvent.VK_C) {
                                       selectTab(Jiggle.TAB_CATALOG);
                                  }
                              }
                          }
                      );
                  }
              }
          );
        }
        Color fg = getProperties().getColor("color.tab.text.fg");
        if (fg != null) msgTextArea.setForeground(fg);
        Color bg = getProperties().getColor("color.tab.text.bg");
        if (bg != null) msgTextArea.setBackground(bg);
        Color sbg = getProperties().getColor("color.tab.text.selection.bg");
        if (sbg != null) msgTextArea.setSelectionColor(sbg);
        Color sfg = getProperties().getColor("color.tab.text.selection.fg");
        if (sfg != null) msgTextArea.setSelectedTextColor(sfg);

        msgTextArea.setText(str);
        JScrollPane textScroller = new JScrollPane(msgTextArea);
        msgTextArea.setCaretPosition(0); // this is simpler, somehow it works - aww

        // allow cut and paste into text field from clipboard
        if (msgTabPopup == null) {
            msgTabPopup = new JTextClipboardPopupMenu(msgTextArea);
            msgTabPopup.addSeparator();
            ActionListener al = new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    String cmd = evt.getActionCommand();
                    if (cmd.startsWith("Clear")) {
                        if (cmd.startsWith("Clear selected")) msgTextArea.replaceSelection("");
                        else msgTextArea.setText("");
                    }
                    else {
                        saveTextAreaToFile( getFilenameFromChooser(), msgTextArea, cmd.startsWith("Save selected")); 
                    }
                }
            };
            JMenuItem menuItem = new JMenuItem("Clear all text ...");
            menuItem.addActionListener(al);
            msgTabPopup.add(menuItem);
            menuItem = new JMenuItem("Clear selected text ...");
            menuItem.addActionListener(al);
            msgTabPopup.add(menuItem);
            msgTabPopup.addSeparator();
            menuItem = new JMenuItem("Save all text to file...");
            menuItem.addActionListener(al);
            msgTabPopup.add(menuItem);
            menuItem = new JMenuItem("Save selected text to file...");
            menuItem.addActionListener(al);
            msgTabPopup.add(menuItem);
        }
        else msgTabPopup.setComponent(msgTextArea);

        tabPanel[tabNumber].removeAll(); // remove prevous contents, if any
        tabPanel[tabNumber].add(textScroller, BorderLayout.CENTER);
        tabPanel[tabNumber].revalidate(); // aww, redundant insurance?
        if (select) tabPane.setSelectedIndex(tabNumber);
    }

//
///////////////// END OF  GUI MANIPULATION METHODS ////////////////////
//
    private static String getFilenameFromChooser() {
        JFileChooser jfc = new JFileChooser(jiggle.getProperties().getUserFilePath());
        Solution sol = mv.getSelectedSolution();
        String filename = (sol == null) ? null : sol.getEventAuthority() + sol.getId().longValue() + ".txt";
        if (filename != null) jfc.setSelectedFile(new File(filename)); 
        int answer = jfc.showSaveDialog(jiggle);
        if (answer == JFileChooser.APPROVE_OPTION) {
            File file =jfc.getSelectedFile();
            if ( file != null && (!file.exists() || file.canWrite())) {
                System.out.println("Saving message tab text to file: " + file);
                try {
                      filename = file.getCanonicalPath();
                }
                catch (IOException ex) {
                      ex.printStackTrace();
                }
                catch (SecurityException ex) {
                      ex.printStackTrace();
                }
            }
            else {
                JOptionPane.showMessageDialog(jiggle, "Cannot open file:" + file, "File Open Failure", JOptionPane.ERROR_MESSAGE);
            }

        }
        else filename = null;

        return filename;
    }

    private static void saveTextAreaToFile(String filename, JTextArea textArea, boolean selectedOnly) {
        if (filename == null || filename.length() == 0) return;
        BufferedWriter idStream = null;
        try {
              File file = new File(filename);
              boolean append = false;
              if (file.exists()) {
                int yn = JOptionPane.showConfirmDialog(
                    null, "File exists, append text?", "File Exists",
                    JOptionPane.YES_NO_OPTION);
                append = (yn == JOptionPane.YES_OPTION);

              }
              idStream = new BufferedWriter(new FileWriter(file, append));
              idStream.write((selectedOnly) ? textArea.getSelectedText() : textArea.getText());
        }
        catch (FileNotFoundException ex) {
              ex.printStackTrace();
              System.err.println("Error: Output file not found: " + filename);
        }
        catch (IOException ex) {
              ex.printStackTrace();
              System.err.println("Error: i/o for output file: " + filename);
        }
        finally {
              try {
                if (idStream != null) {
                    idStream.flush();
                    idStream.close();
                }
              }
              catch (IOException ex2) { }
        }
    } 

//
//////////////////// Begin Solution Event Handling methods //////////////////////////
//
/**
 * Check to see if this solution id is locked by another user. If it is, pop a dialog
 * to inform the user and return 'false'. Otherwise, return true.  Also returns 'true'
 * if locking is not supported otherwise you would never be allowed access to events.
*/
    private boolean handleLock(long id) {

        if (solLock == null || getProperties().getBoolean("solLockingDisabled")) {
            if (debug) System.out.println("Jiggle Locking of event id: " + id + " DISABLED by property(s) " +
                "dbWriteBackEnabled=false and/or solLockingDisabled=true");
            return true;
        }

        if (debug) System.out.println("Jiggle DEBUG handleLock for id " + id);

        // attempt lock. Remember, this returns 'true' if locking is NOT enabled
        solLock.setId(id);

        if (solLock.lock()) {
            return true; // lock was successfull
        } else {
            // NOTE: database error from solLock.lock() returns false which displays this dialog making it seem
            //       as if there is a lock for the selected event.
            //       SolutionLockTN.isLocked() sets username to empty string to signal invalid lock

            if (!solLock.getUsername().isEmpty()) {
                // lock failed, pop a dialog
                String msg = "<html><b>EVENT " + id + " IS LOCKED.</b><p>" +
                        "Username:    " + solLock.getUsername() + "<br>" +
                        "Hostname:    " + solLock.host + "<br>" +
                        "Application: " + solLock.application + "<br>" +
                        "Time:        " + LeapSeconds.trueToString(solLock.datetime).substring(0, 19) + // use UTC true time - aww 2008/02/10
                        "</html>";

                popInfoFrame("Event Is Locked", msg);
            } else {
                String msg = "Unable to lock EVENT " + id + "." + JiggleConstant.NEWLINE +
                        JiggleConstant.NEWLINE +
                        "Please try again or restart the application if the problem persists!";

                popInfoFrame("Failed to Lock Event", msg);
            }

            return false;
        }
    }

/** Release all Solution Locks */
    public void releaseAllSolutionLocks() {
        if (getProperties().getBoolean("solLockingDisabled")) return;
        if (debug) System.out.println("Jiggle DEBUG releaseAllSolutionLocks");
        //SolutionLock solLock = SolutionLock.create();
        if (solLock != null) solLock.unlockAllMyLocks();
    }

//
// Methods below usually result in GUI panel updates
//
/**
 * Load one event from the dataSource. This is only one way to create a
 * MasterView. Others will be implemented later.  */

    public boolean loadSolution(Solution sol) {
        return (sol == null) ? false : loadSolution(sol.id.longValue());
    }

/**
 * Load one event from the dbase. This is only one way to create a
 * MasterView. Others will be implemented later. Returns true if load successful. */

    public boolean loadSolution(long id) {
        return loadSolution(id, true);
    }

    public boolean loadSolution(long id, boolean checkExistance) {

        if (! validateConnection("Load solution")) return false; 

        boolean status = true;
        if (scopeMode && scopeConfig != null) {
            status = scopeConfig.turnScopeOff();
        }
        if (!status) {
          JOptionPane.showMessageDialog( Jiggle.this, "Scope mode still ON, check messages",
              "Scope Config Action",JOptionPane.PLAIN_MESSAGE);
            return false;
        }

        String str = "";

        // Check that all modified solutions have been saved
        Solution sol[] = mv.solList.getArray();
        for (int i = 0; i< sol.length; i++) {
          if (sol[i].hasChanged() ) {
            str = "Event "+sol[i].id.longValue()+" is new or has changed since last save.\nSave (Yes), don't save (No), or abort (Cancel)?";
            int yn = JOptionPane.showConfirmDialog(
                Jiggle.this, str,
                "Jiggle: Save event "+sol[i].id.longValue()+"?",
                JOptionPane.YES_NO_CANCEL_OPTION);

            if (yn == JOptionPane.CANCEL_OPTION) return true;

            if (yn == JOptionPane.YES_OPTION) {

              if (sol[i].isEventDbType(EventTypeMap3.TRIGGER) && sol[i].hasLatLonZ()) {
                yn = JOptionPane.showConfirmDialog(
                    Jiggle.this, "Trigger Lat,Lon,Z are not 0., 0., 0. continue to save?",
                    "Jiggle: Save trigger "+sol[i].id.longValue()+"?",
                    JOptionPane.YES_NO_OPTION);

                if (yn != JOptionPane.YES_OPTION) continue;

              }

              if (! saveToDb(sol[i])) {
                popInfoFrame("Changed Event Save Failure", "Unable to save "+sol[i].id.longValue()+ ", load cancelled for " + id);
                return true;
              }

            }
          }
        }

        Solution newSol = Solution.create();
        newSol.setId(id);

        // networkModeLAN  ??
        if (checkExistance && ! newSol.existsInDataSource()) {
            popInfoFrame("No Such Event Id", "Event "+id+" does not exist in database, load failed!");
            return true;
        }

        // 1 day (could make if configurable by prop), should cacheRefreshing status be moved to callable method in ChannelList ?
        if (!cacheRefreshing && (System.currentTimeMillis() - lastCacheRefreshTime)/86400000l >= 1) {
          str = "Current Channel cache is older than 1 day, Do you want to refresh it in background thread now?";
          int yn = JOptionPane.showConfirmDialog(
              Jiggle.this, str,
              "Jiggle: Refresh Cache",
              JOptionPane.YES_NO_OPTION);
              if (yn == JOptionPane.YES_OPTION) refreshMasterCache();
              else if (yn == JOptionPane.NO_OPTION) lastCacheRefreshTime = System.currentTimeMillis();
        }

        // Use pop dialog to confirm reload of loaded Solution
        if (mv.solList != null && mv.solList.contains(id))  {

          str = "Are you sure you want to RELOAD the selected event "
              + id + "?\n Data for this event alone will be loaded.";
          int yn = JOptionPane.showConfirmDialog(
              Jiggle.this, str,
              "Jiggle: Reload event?",
              JOptionPane.YES_NO_OPTION);

          if (yn != JOptionPane.YES_OPTION) return true;

        }
        else { 
          // Getting selected to clear existing disk waveform file cache ?
          Solution selected = mv.getSelectedSolution();
          if (selected != null && wfCache2FilePurged) {
              System.out.println("Purging local waveform file cache for evid: " + selected.getId().longValue());
              AbstractWaveform.clearCacheDir(selected.getId().longValue(), null, null);
          }
        }

        // release previously held locks.
        releaseAllSolutionLocks();

        // lock the event, show dialog & bail if event is locked by someone else
        if (! handleLock(id))  return false;

        // Read the data source and build a master view

        if (getVerbose()) bench1 = new BenchMark (" * Elapse time to load event "+id+": ");

        if (solSolverDelegate != null)  solSolverDelegate.reset(); // clear results

        if (getVerbose()) System.out.println(">>> Creating new MasterView for "+id+"...");
        // Create a brand new MasterView, kills the old master view
        // (stops old cache manager) prevent memory leaks
        clearMasterView(true);  // NOTE: this might create wf cache dir files, unloading current timeseries to files !!!
        if (prefMagTabPane != null) prefMagTabPane.setSelectedIndex(0);  // must reset updateTabPane index -aww 2008/03/03

        if (! validateConnection("Solution Load")) return false;

        loadSolutionInThread(id);
        return true;
    }

/** Load the data for this solution in a thread. Allows status info in GUI
 * to be updated. */
    private void loadSolutionInThread(long id) {
      final long evid = id;
      final org.trinet.util.SwingWorker worker = new org.trinet.util.SwingWorker() {
          // NO GRAPHICS CALLS ALLOWED IN THIS METHOD UNLESS using SwingUtilites.invokeLater(new Runnable(...))!
          WorkerStatusDialog workStatus = new WorkerStatusDialog(Jiggle.this, true);
          boolean status = false;
          BenchMark bm = null;

          // NO GRAPHICS CALLS ALLOWED IN THIS METHOD UNLESS InvokeLater!
          public Object construct() {
            bm = new BenchMark();
            workStatus.setBeep(beep);
            workStatus.pop("MasterView", "Loading event data for " + evid, true);
            if (mv.defineByCurrentModel(evid)) {   // load the event
              Solution sol = mv.getSelectedSolution();
              //if (sol != null) catSolList.addOrReplaceById(sol); // ! aliases Solution mv == catalog - aww 05/02/2007
              // loading phases, amps, coda, sets stale thus needCommit 'true', so fix it.
              //sol.setStale(false);
              //sol.setNeedsCommit(false);// reset a stale enabled commit - added 02/01/2005 -aww
              //sol.getPreferredMagnitude().setStale(false); // added 01/24/2005 -aww
              //sol.getPreferredMagnitude().setNeedsCommit(false); // added 02/01/2005 -aww
              if (sol != null) {
                  sol.resetStatusFlags(); // replaced above with this 10/17/2006 - aww 
                  status = true;
              }
            }
            return null;
          }

          //Runs on the event-dispatching thread graphics calls OK.
          public void finished() {
            if (getVerbose()) bm.printTimeStamp(" * Elapsed (thread) time to load event "+evid); // UTC time - aww 2008/02/10
            if (! status) {
              popInfoFrame("MasterView Solution Load Failure", "Unable to retrieve id:" + evid);
            }
            else { // success
              Solution sol = mv.getSelectedSolution();
              if (sol == null) {
                  System.err.println("Jiggle Error: No selected solution for evid: " + evid);
                  return;
              }
              System.out.println(sol.getNeatStringHeader());
              System.out.println(sol.toNeatString());

              resetVelocityModel(sol);

            }

            //
            resetGUIunsafe(true); // aww versus "invokeLater"
            // set back to normal (not "working") cursor
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            //workStatus.unpop(); // uses invokeLater event queue, just use dispose
            workStatus.dispose(); // don't invoke unpop before dispose (may cause exception)
            firePropertyChange(NEW_MV_PROPNAME, null, mv); // put notification here to ensure WFViews are loaded for Map etc. 
          }
      };
      initStatusPanelForWork("Loading event...");
      worker.start();  //required for SwingWorker 3
    }

    protected void resetAuthority(Solution sol) {

        // Prop to short-circuit reset, e.g. avoid WAN db queries
        if (!getProperties().getBoolean("authRegionEnabled")) return; // -aww 2011/08/16 short-circuit NO-OP

        //
        // One problem is that the etype changes from subnet trigger (st) to local event (le) after relocation so we can't test for trigger here.
        //
        if (sol.isOriginGType(GTypeMap.TELESEISM)) return;  // ? ignore teleseisms ?

        /* In lieu of a stored proc use jiggle input property with ordered list from 1st to last region to check for inclusion
        String [] names =  getProperties().getStringArray("authRegionNames");
        if (names == null || names.length <= 1) return; // else check
        boolean insideRegion = false;
        for (int idx = 0; idx < names.length; idx++) {
            insideRegion = sol.isInsideRegion(names[idx]);
            if (debug) System.out.println("DEBUG Jiggle event is in authority "+ names[idx] + " region: " + insideRegion);
            if (insideRegion) { // change default velocity model to new region
                if (debug) System.out.println("DEBUG Jiggle event/origin/magnitude authority is reset");
                // Need mapping of region names to auth codes, so for now assume region names are the same as network 2-char codes!!!
                sol.setAuthRegion(names[idx]);
                break;
            }
        }
        */

        // Below call resets the eventAuthority only if it's a loaded trigger or new event clone
        sol.getEventAuthority(); // (calls stored proc to check region polygons for new auth)
    }

    protected void resetVelocityModel(Solution sol) {
        // Here assume user doesn't "delete" models from list, 
        // or change from 1 model to another with different name
        // short-circuit query when list size = 1 or a reload of same event 
        // hidden prop to short-circuit VM reset, e.g. avoid WAN db queries ?
        if (getProperties().getBoolean("disableVelocityModelRegionReset")) return;

        VelocityModelList vmList = getProperties().getVelocityModelList();
        if (vmList.size() <= 1) return; // else have to check every time -aww 2011/08/10

        // New event with multiple models so now check velocity model before resetting GUI (cues) - aww 2008/03/25
        boolean insideRegion = false;
        // Removed logic below to avoid inclusive current/default model from preventing testing for sub-region,
        // The name of the inclusive model (i.e. boundaries overlap those of internal subregion polygons must be
        // listed last in the velocityModelList property to enable a successful inside sub-region test. -aww 2011/07/27
        /*
        UniformFlatLayerVelocityModel model = getProperties().getSelectedVelocityModel();
        TravelTime.setDefaultModel(model); // revert model to currently selected as default
        String sname = model.getName(); // the selected model's name
        insideRegion = sol.isInsideRegion(sname); // outside?, attempt to change TravelTime velocity model  
        System.out.println("DEBUG Jiggle selected model name: "+ sname + " event is inside it's region? " + insideRegion);
        if (! insideRegion)  { // outside?, attempt to change TravelTime velocity model  
        */
        String [] names =  vmList.getModelNames();
        if (names == null || names.length <= 1) return;

        for (int idx = 0; idx < names.length; idx++) {
            insideRegion = sol.isInsideRegion(names[idx]);
            if (debug) System.out.println("DEBUG Jiggle event is in velocity model "+ names[idx] + " region: " + insideRegion);
            if (insideRegion) { // change default velocity model to new region
                if (debug) System.out.println("DEBUG Jiggle default TravelTime velocity model is reset");
                setVelocityModel(vmList, names[idx]);
                resetMenuAndToolBars();
                statusBar.updateVelModelLabel();
                break;
            }
        }
    }

    protected void setVelocityModel(String modelName) {
        setVelocityModel( getProperties().getVelocityModelList(), modelName);
    }
    protected void setVelocityModel(VelocityModelList vmList, String modelName) {
        String oldModelName = getProperties().getDefaultVelocityModelName();
        getProperties().setDefaultVelocityModelName( modelName ); // sets selected model in list
        TravelTime.setDefaultModel( vmList.getByName( modelName) );
        if (!modelName.equals(oldModelName)) firePropertyChange(NEW_VMODEL_PROPNAME, oldModelName, modelName);
    }

    protected void refreshWFViewList() {

      mv.wfvList.stopCacheManager(); // kill cache mgr
      //mv.masterWFWindowModel.clearChangeListeners(); // is it safe? what about readinglist listening besides the wf panels?

      final org.trinet.util.SwingWorker worker = new org.trinet.util.SwingWorker() {
          // NO GRAPHICS CALLS ALLOWED IN THIS METHOD UNLESS using SwingUtilites.invokeLater(new Runnable(...))!
          WorkerStatusDialog workStatus = new WorkerStatusDialog(Jiggle.this, true);
          boolean status = false;
          BenchMark bm = null;
          long evid = 0l;
           
          // NO GRAPHICS CALLS ALLOWED IN THIS METHOD UNLESS InvokeLater!
          public Object construct() {
            bm = new BenchMark();
            workStatus.setBeep(beep);
            Solution sol = mv.getSelectedSolution();
            if (sol != null) evid = sol.getId().longValue();
            workStatus.pop("MasterView", "Loading event data for " + evid, true);
            mv.timeSpan = new TimeSpan(); // reset the span, don't reuse existing
            mv.setWaveformLoadMode(mv.waveformLoadMode); // restarts cache mgr here
            status = mv.refreshWFViewListByCurrentModel(); // rebuild view list here and load wfs
            return null;
          }

          //Runs on the event-dispatching thread graphics calls OK.
          public void finished() {
            if (getVerbose()) bm.printTimeStamp(" * Elapsed (thread) time to load event "+evid);
            if (! status) {
              popInfoFrame("MasterView Solution Load Failure", "Unable to refresh waveforms for id:" + evid);
            }
            //System.out.println("DEBUG refresh masterview timespan = " + mv.timeSpan.toString());
            resetGUIunsafe(true); // no need here for "invokeLater", rebuilds wf components
            mv.masterWFViewModel.selectFirst(mv.wfvList); // select 1st panel
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR)); // set back to normal
            workStatus.dispose(); // don't invoke unpop before dispose (may cause exception)
          }
      };
      initStatusPanelForWork("Loading event...");
      worker.start();  //required for SwingWorker 3
    }

    /*  BELOW DONE IN MASTERVIEW -aww
    private void loadPrefMagsFor(Solution sol) {
       // System.out.println("DEBUG: loadSolutionInThread now loading prefmags...");
              sol.loadPrefMags(); // test added here to get prefMags - aww 10/19/2004
              // TEMPORARY fix - stuff "preferred" mag into map; SQL exception
              // when creating Solution (i.e. missing mag calibration or prefmag tables)
              // may prevent loading prefmag map with preferred ?
              //if (sol.getPrefMags().size() == 0 && sol.getPreferredMagnitude() != null) {
                //sol.setPrefMagOfType(sol.getPreferredMagnitude());
              //}
              //
       // System.out.println("DEBUG: loadSolutionInThread finished loading prefmags...");
              Iterator iter = sol.getPrefMags().iterator();
              Magnitude prefMag = null;
              while (iter.hasNext()) {
                prefMag = (Magnitude) iter.next();
                if (prefMag.getReadingsCount() == 0) {
       // System.out.println("DEBUG: loadSolutionInThread prefmags loading readings...");
                    boolean stale = prefMag.isStale();
                    boolean needsCommit = prefMag.getNeedsCommit();
                    prefMag.loadReadingList();
                    prefMag.setStale(stale); // assume no reading changes in db, added 01/24/2005 -aww
                    prefMag.setNeedsCommit(needsCommit); // assumed to be false for those from db
                }
              }
       // System.out.println("DEBUG: loadSolutionInThread prefmags with readings done.");
       // System.out.println("DEBUG: loadSolutionInThread done ...");
    }
    */

    /** Reload the current solution. Used when the model is changed. */
    protected void reloadSolution() {
        if (mv == null || mv.getSelectedSolution() == null) return;
        lastLoadedEvid = mv.getSelectedSolution().getId().longValue(); 
        loadSolution(lastLoadedEvid, isNetworkModeLAN()); // dont' check for existance
    }

    protected void rollbackEventPreferred() {
        if (mv == null || mv.getSelectedSolution() == null) return;
        if (! mv.getSelectedSolution().rollbackPrefs() )
            popInfoFrame( "Rollback Preferred", "Unable to set prior prefor and prefmag for current solution");
    }

    /** Dialog to get event ID number and load it. */
    private final class MyJDialog extends CenteredDialog {
        JTextField jtf = null;

        public MyJDialog(String id) {
            super(Jiggle.this);
            jtf = new JTextField(id);
            jtf.selectAll();
            jtf.setPreferredSize(new Dimension(100,20));
            jtf.setMaximumSize(new Dimension(100,20));
            jtf.addActionListener( new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    returnValue = JOptionPane.OK_OPTION;
                    dispose();
                }
            });

            // allow cut and paste into text field from clipboard
            new JTextClipboardPopupMenu(jtf);

            Box jbox = Box.createHorizontalBox();
            jbox.add(Box.createVerticalGlue());
            jbox.add(new JLabel("Enter id to load: "));
            jbox.add(jtf);
            jbox.add(Box.createVerticalGlue());
            JButton jbCancel = new JButton("Cancel");
            jbCancel.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    returnValue = JOptionPane.CANCEL_OPTION;
                    dispose();
                }
            });

            JButton jbOK = new JButton("OK");
            jbOK.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    returnValue = JOptionPane.OK_OPTION;
                    dispose();
                }
            });

            JPanel jbPanel = new JPanel();
            jbPanel.add(jbOK);
            jbPanel.add(jbCancel);

            JPanel contentPanel = new JPanel();
            contentPanel.setLayout(new BorderLayout());
            contentPanel.add(jbox, BorderLayout.CENTER);
            contentPanel.add(jbPanel, BorderLayout.SOUTH);

            getContentPane().add(contentPanel);

            //setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
            setSize(200,100);
            centerDialog();
        }

        public void setText(String id) {
            jtf.setText(id);
        }

        public String getText() {
            return jtf.getText();
        }

    }

    protected void loadIdDialog() {
        
        String defId = "";

        if (mv != null) {
          Solution selSol = mv.getSelectedSolution();
          if (selSol != null)  defId = selSol.id.toString();
        }

        MyJDialog jd = new MyJDialog(defId); 
        jd.setModal(true);
        jd.setVisible(true); // blocks until disposed when modal == true , else falls through
        
        if (jd.getButtonStatus() == JOptionPane.OK_OPTION) {
           String strId = jd.getText();
           if (strId != null && strId.length() > 0) {  // is so [cancel] hit
             long evid = Long.valueOf(strId.trim()).longValue();
             if (! loadSolution(evid, isNetworkModeLAN())) {
                String str = "Loading of data into viewer failed for id = "+evid;
                popInfoFrame( "Solution Load Failure", str);
             }
           }
        }
    }

/** Load the event following the currently selected one.  Does not wrap so if
    there is no next solution a dialog box says so and this method returns
    false. */
    public void loadNextSolution() {
        loadNextSolution(mv.getSelectedSolution());
    }

/** Load the event following this one. Does not wrap so if there is no next
    solution a dialog box says so and this method returns false. */
    public void loadNextSolution(final Solution currentSol) {

        if (scopeMode) return;

        if (currentSol == null) {
            popInfoFrame("No Next Solution to Load",  "Input (current) solution is null.");
            return;// false;
        }

        initStatusGraphicsForThread("Get next solution", "Searching for solution of type :" + solNextProcStateMask);
        final org.trinet.util.SwingWorker worker = new org.trinet.util.SwingWorker() {
            //WorkerStatusDialog workStatus = new WorkerStatusDialog(Jiggle.this, true); // ???? aww
            Solution nextSol = currentSol;
            Solution fromDbSol = null;
            int currentIdx = -1;
            int nextIdx = -1;
            
            public Object construct() {
              currentIdx = catPane.getSortedTableRowIndex(nextSol);
              do {
                  //nextSol = (Solution) catSolList.getNext(nextSol); // get next list event after this one - removed 12/05/2007 -aww replaced by below
                  nextSol = catPane.getNextSortedSolution(nextSol);
                  //System.out.println("DEBUG loadNextSolution catPane.getNextSortedSolution(nextSol) returns: " + ((nextSol == null)? "null" : nextSol.getId().longValue()));  

                  // If deleted in masterview, nextSol is null, catalog list selection should be next, or whatever catalog row user last picked
                  if (nextSol == null && currentSol.isDeleted()) nextSol = (Solution) catSolList.getSelected(); // added 2008/10/10 -aww

                  // added id test to condition to avoid infinite loop when no matching processing state found and back at top. -aww 10/23/2007
                  if ( nextSol == currentSol || nextSol == null || nextSol.getId().equals(currentSol.getId()) ) break; // none, so bail.

                  // NEW LOGIC : loop thru list until solution matching criteria is found in database,
                  // refreshing catalog rows by query until next one found -aww 05/02/2007
                  fromDbSol = null;
                  while (fromDbSol == null) {
                    if (catPane.catFileName != null) {
                        fromDbSol = (Solution) nextSol.getById(nextSol.getId());
                        //if (fromDbSol != null && !fromDbSol.isValid()) fromDbSol = null; // do we reject one read from file if "deleted" ??
                    }
                    else {
                        fromDbSol = nextSol.refresh(eventProps);
                    }
                
                    // Below are database refresh alternatives -aww 05/02/2007
                    //fromDbSol = (Solution) nextSol.getById(nextSol.getId());
                    //if (! eventProps.matches(fromDbSol)) fromDbSol = null; 
                    // Simple kludge here:
                    //Boolean flag =  eventProps.getEventValidFlag();
                    //if (fromDbSol != null && flag != null) {
                    //    if (flag.booleanValue() && ! fromDbSol.isValid()) fromDbSol = null; // reject "deleted"
                    //}
                    if (fromDbSol == null) { // must be db deleted, so remove from our catalog list
                      //fromDbSol = (Solution) catSolList.getNext(nextSol); // temp save of next one to query - removed 12/05/2007 -aww replaced by below
                      fromDbSol = catPane.getNextSortedSolution(nextSol);   // temp save of next one to query
                      //NOTE: method below assigns list selection next solution in list only if (selected == nextSol)
                      catSolList.remove(nextSol);
                      nextSol = fromDbSol; // now ok to assign next to do
                      fromDbSol = null; // must reset back to null to continue loop
                    }
                    if (nextSol == null) {
                       break; // must be an empty list
                    }
                  }

                  if (fromDbSol != null) { // have most recent db data
                    nextSol = fromDbSol;
                    catSolList.addOrReplaceById(nextSol); // update catalog list with db data
                  }
                  // end of new logic -aww 05/02/2007

                  nextIdx = catPane.getSortedTableRowIndex(nextSol);

              } // if next event doesn't have desired processing state get next one in list.
              while (nextSol != null && solNextProcStateMask.indexOf(nextSol.getProcessingStateString()) < 0);
              return null;
            }

            //Runs on the event-dispatching thread so graphics calls are OK here.
            public void finished() {
              resetStatusGraphicsForThread();

              // added id test to condition to avoid infinite loop when no matching processing state found and back at top. -aww 10/23/2007
              if ( nextSol == null || nextSol == currentSol || nextSol.getId().equals(currentSol.getId()) ) { // none, so bail.
                  popInfoFrame("No Next Solution",  "There are no solutions following this one.");
                  return; // false;
              } 
              else if (nextIdx <= currentIdx) {
                int yn = JOptionPane.showConfirmDialog(
                     Jiggle.this, "Next id to load by state preceeds the last loaded id of sorted catalog, load it?",
                     "Catalog Wrap Around",
                     JOptionPane.YES_NO_OPTION);
                if (yn != JOptionPane.YES_OPTION) {
                    return; // user declined
                }
              }

             if (loadSolution(nextSol.id.longValue(), isNetworkModeLAN())) return; // true;
             // if next is blocked, recursively try next.
             loadNextSolution(nextSol); //return loadNextSolution(nextSol);
            }
        };
        worker.start();
        return; // true;
    }
    /**
     * Delete the current selected solution from the data source. This is
     * NOT just setting a flag internal to Jiggle.
     * It commits the delete after confirming with a
     * dialog box.  */
    protected boolean deleteCurrentSolution() {
         return deleteSolution(mv.getSelectedSolution(), autoLoadAfterDelete);
    }
    /**
     * Delete this solution from the data source. This is NOT just setting a
     * flag.  It commits the delete after confirming with a dialog box.  */
    protected boolean deleteSolution(long evid) {
       return deleteSolution(mv.solList.getById(evid), autoLoadAfterDelete);
    }
    /**
     * Delete this solution from the data source. This is NOT just setting a
     * flag.  It commits the delete after confirming with a dialog box.  */
    protected boolean deleteSolution(Solution sol) {
       return deleteSolution(sol, autoLoadAfterDelete);
    }

    protected boolean deleteSolution(Solution sol, boolean autoLoadAfterDelete) {

        if (! validateConnection("Delete solution")) return false; 

        if (sol == null) {
          popInfoFrame("No Solution", "There is no currently selected solution.");
          menuBar.setEventEnabled(false);

          return false;
        }

        // confirm delete action
        String str =
            "WARNING: This action will permanently delete \n"+
            "solution "+sol.id.toString()+" and all related data. \n"+
            "Is this what you want to do?";

        //pop-up confirming  yes/no dialog:
        int check = JOptionPane.showConfirmDialog(
                   Jiggle.this, str,
                   "DELETE SOLUTION",
                   JOptionPane.YES_NO_OPTION);

        if (check == JOptionPane.YES_OPTION) {

           System.out.println("Jiggle INFO: Deleting: "+ sol.toNeatString() +
                   " at: "  + new DateTime().toString()); // UTC time - aww 2008/02/10

           boolean status = true;
           boolean wasSelected = (mv.getSelectedSolution() == sol) ;

          // set delete flag of sol
          if (sol.delete()) {
              // remove solution and all its data from MasterView
              // mv.removeSolution(sol);
              // commit to dbase
              try {
                if ( sol.commit() ) {
                   if (wasSelected) solSolverDelegate.reset();

                   boolean eventValid = true; // added logic block - aww 2008/04/16
                   if (eventProps != null) {
                     Boolean b = eventProps.getEventValidFlag(); // does user want to see "deleted" ?
                     eventValid = ! (b == null || b.booleanValue() == false);
                   }

                   mv.solList.delete((Object)sol); // delete solution, tell all listeners

                   Solution nextSol = null;
                   if (catSolList != null) {
                       int idx = catSolList.getIndex(sol); // catalog too -aww 05/10/2007
                       if (idx >= 0) {
                           if (eventValid) { // added test, only remove when user does not want to view "deleted"
                             //System.out.println("DEBUG deleteSolution getNextSortedSolution(catSolList.get("+idx+"))  where idx id is: " + ((Solution)catSolList.get(idx)).getId().longValue());
                             nextSol = (catPane == null) ?
                                 null : catPane.getNextSortedSolution( (Solution) catSolList.get(idx) ); // added -aww 2008/04/16
                               //System.out.println("DEBUG deleteSolution catPane.getNextSortedSolution returned id: " + nextSol.getId().longValue());
                               catSolList.remove(idx); // catalog too -aww 05/10/2007
                               //System.out.println("DEBUG deleteSolution catSolList.remove("+id+"): " sol.getId().longValue());
                               if (nextSol != null) {
                                 catSolList.setSelected(nextSol); // added -aww 2008/04/16
                                 //System.out.println("DEBUG deleteSolution catSolList.setSelected(nextSol) nextSol id: " + ((Solution)catSolList.getSelected()).getId().longValue());
                               }
                           }
                           else {
                               catSolList.getSolution(idx).validFlag.setValue(0);
                           }
                           // refresh of table cells after delete is being done without this call 2010/01/27 -aww
                           //catPane.update();
                       }
                   }
                   // Below IS NOT executed if solutions are not REMOVED from solList above
                   if (mv.solList.size() < 1) {   // clear the GUI if no more solutions
                     //remakeGUI(); // removed as test 07/05/2007 aww replace by below
                     clearMasterView(true);
                     if (prefMagTabPane != null) prefMagTabPane.setSelectedIndex(0);  // must reset updateTabPane index -aww 2008/03/03
                     selectTab(TAB_CATALOG);
                   }

                   if (autoLoadAfterDelete && nextSol != null) loadSolution(nextSol.id.longValue(), isNetworkModeLAN());

                } else {
                   String msg = "<html>Could not delete this solution from the data source.<p>"+
                            sol.getCommitStatus()+"</html>";
                         popInfoFrame("Can't Delete", msg);
                   status = false;
                }
              } catch (JasiCommitException ex) {
                long evid = sol.id.longValue();
                // bad save
                String title = "Delete Error "+ evid;
                String msg = "<html><b>WARNING:</b> Error during delete of event "+evid+".<p>"+
                ex.toString()+"</html>";
                popInfoFrame(title, msg);
                status = false;
             }
          }
          return status;
        }
        // the JOptionPane answer was no
        return false;
    }

/**
 * Create a new solution from scratch. Adds it to our list. Locks it. Updates views.
 */
    protected Solution createNewSolution() {

        if (mv == null) return null;

        if (! validateConnection("New solution")) return null; 

        // rather than default, require property declaration -aww  03/02/2007
        boolean confirm = (getProperties().isSpecified("confirmNewSolutionAction")) ?
          getProperties().getBoolean("confirmNewSolutionAction") : true;

        if (confirm) {
            int yn = JOptionPane.showConfirmDialog(
                     Jiggle.this, "Create new event inside currently loaded waveform view?",
                     "Jiggle: New Event Confirmation",
                     JOptionPane.YES_NO_OPTION);
            if (yn != JOptionPane.YES_OPTION) return null;
        }

        long parentId = 0l;

        Solution parentSol = mv.getSelectedSolution();
        if (parentSol != null) {
            if (parentSol.getNeedsCommit()) {
                int yn = JOptionPane.showConfirmDialog(Jiggle.this,
                          "Current parent event, changed, needs commit,\n Save parent event's data?",
                          "New Solution Creation", JOptionPane.YES_NO_OPTION);
                if (yn == JOptionPane.YES_OPTION) {
                    saveToDb(parentSol);
                }
            }

            // if selected is a clone, revert to it's parent -aww 05/25/2007
            if (parentSol.isClone()) parentId = parentSol.getParentId().longValue();
            else parentId = parentSol.getId().longValue();
        }

        boolean waveServerLoad = 
            (getProperties().getInt("waveformReadMode") == AbstractWaveform.LoadFromWaveServer);

        String dbase = getProperties().getDbaseDescription().getDbaseName();
        if (waveServerLoad) {
          //String str = parentId + " loaded from WaveServer";
          int yn = JOptionPane.showConfirmDialog( Jiggle.this, 
                     "If " +dbase.toUpperCase()+ " is RealTime db and you save this clone,"+
                     " waveform REQUEST event associations will be generated for clone.\nContinue to create?",
                      "New Event", JOptionPane.YES_NO_OPTION);
          if (yn != JOptionPane.YES_OPTION) return null;
        }

        Solution newSol = Solution.create();
        newSol.setLatLonZ(LatLonZ.NullValue, LatLonZ.NullValue, LatLonZ.NullValue); // so map can filter else NaN

        double start = mv.getViewSpan().getStart();
        if ( !mv.getViewSpan().isValid() || start == Double.MAX_VALUE || start == -Double.MAX_VALUE || Double.isNaN(start) )
            start = parentSol.getTime();
        newSol.setTime( mv.getViewSpan().getStart() ); // set origin time start, like for map MasterViewLayer? -aww 2012/06/08 

        if (scopeMode) {
            newSol.setTime( mv.masterWFWindowModel.getTimeSpan().getCenter() ); // center time
            WFView wfvSel = mv.masterWFViewModel.get();
            if (wfvSel != null) {
                Channel ch = wfvSel.getChannelObj(); 
                newSol.setLatLonZ(ch.getLatLonZ()); // ch elev is -km
                newSol.depth.setValue(newSol.getDepth()+5.); // for geoid adjust by channel elev - aww 2015/10/10
                newSol.mdepth.setValue(5.); // below channel elev - aww 2015/10/10
                newSol.dummyFlag.setValue(0l);
            }
        }

        long newId = newSol.setUniqueId();      // need unique ID from dbase


        // set parent ID #
        if (parentId > 0l) newSol.setParentId(parentId);

        // associate waveforms (needed to associate waveforms with the event if required
        // by the underlying data source)
        // NOTE: in TN version of Solution this is done via EPREF.cloneAssocWaE()
        newSol.addWaveforms(chooseWaveformSet(newSol));
        //newSol.setEventType(EventTypeMap.LOCAL); // bumps version, so removed -aww 04/15/2008
        newSol.eventType.setValue(EventTypeMap3.toJasiType(EventTypeMap3.EARTHQUAKE)); //to not bump version -aww 2008/04/15
        newSol.gtype.setValue(GTypeMap.LOCAL);
        newSol.setStale(true);

        mv.addSolution(newSol, false, false);
        mv.setSelectedSolution(newSol);
        if (prefMagTabPane != null) prefMagTabPane.setSelectedIndex(0);  // must reset updateTabPane index -aww 2008/03/03

        // lock the new id
        handleLock(newId);

//        updateTextViews();  // now done by listener

        updateFrameTitle();

        resetBarAndMenuStates(); 

        if (scopeMode) {
            editEventParams(EventEditDialog.LLZTab);
            pickPanel.enablePicking(); // ? null pick buttons?
        }

        //if (debug || waveServerLoad || scopeMode) {
            System.out.println("Jiggle INFO: event " +newId+ " cloned from " +parentId+ " on database: "+dbase);
            System.out.println(newSol.toNeatString());
        //}


        return newSol;

    }

/** Manually edit event parameters. */
    protected boolean editEventParams() {
        return editEventParams(-1);
    }

    protected boolean editEventParams(int topTabPaneId) {
        Solution sol = mv.getSelectedSolution();
        EventEditDialog dialog = //new EventEditDialog(sol, getProperties(), topTabPaneId);
                new EventEditDialog(sol, Jiggle.this, "Edit Solution Parameters", true, getProperties(), topTabPaneId);
        // set state properties according to user preferences
        getProperties().setProperty("EventEdit.decimalLatLon", dialog.isDecimalLatLonStyle());
        //getProperties().setProperty("EventEdit.magTabOnTop", dialog.isMagTabOnTop());  // removed 09/10/2007 aww
        // apply results if [OK] was hit
        if (dialog.getButtonStatus() == JOptionPane.OK_OPTION) {
            sol = dialog.getSolution(); // get modified solution
            toolBar.solPanel.fixDepthButton.setSolution(sol); // - aww 02/16/2007 Do we want to force it to trial setting?

            if (dialog.hasChangedMag()) {
                Magnitude mag = dialog.getMagnitude(); // edited preferred
                Magnitude testMag = (Magnitude) mag.clone();
                int priority = 0;
                if ( mag.algorithm.toString().equals("HAND") && 
                     ( mag.getTypeSubString().equals("h") || mag.getTypeSubString().equals("n") )
                   )
                    priority = mag.getPriority();
                else {
                    testMag.algorithm.setNull(true);
                    priority = testMag.getPriority();
                }

                int maxPriority = priority;

                Magnitude prefMag = sol.getPrefMagByPriority();
                // If virgin magnitude is hand entered, above returns NULL so need to test - 07/24/2006 -aww
                if (prefMag != null) maxPriority = prefMag.getPriority();

                // Get priority of same magtype as the edited, if any
                Magnitude prefTypeMag = sol.getPrefMagOfType(testMag);

                int yn = JOptionPane.NO_OPTION;

                boolean setPrefMag = false;

                // is Edited mag the highest priority
                if (priority >= maxPriority) {
                   sol.setPreferredMagnitude(mag);
                   // flag as yes
                   yn = JOptionPane.YES_OPTION;
                   setPrefMag = true;
                }
                else { // not highest, so ask to override the event preferred 
                  yn = JOptionPane.showConfirmDialog( Jiggle.this,
                      "Event priority magnitude: " + prefMag.toString() + "\n" +
                      "Dialog edited magnitude: " + mag.toString() + "\n" +
                      "[YES] set edited magnitude as EVENT preferred .\n" +
                      " [NO] do not set edited magnitude as EVENT preferred.",
                      "Set Event Preferred?",
                      JOptionPane.YES_NO_OPTION);
                   // if yes, new mag is set event preferred 
                   if (yn == JOptionPane.YES_OPTION) {
                       sol.setPreferredMagnitude(mag);
                       setPrefMag = true;
                   }
                }
                if ( setPrefMag ) {
                    System.out.println("Jiggle INFO: User set event preferred magid: " +
                        mag.getIdentifier().toString() + " " + mag.toNeatString()); 
                }

                // if not set above, ask again to override the preferred of magtype
                if (! (yn == JOptionPane.YES_OPTION || mag.isSameType(prefMag)) ) {
                    String str = "Dialog edited magnitude: " + mag.toString() + "\n";
                    if (prefTypeMag != null) str =
                      "Type priority magnitude: " + prefTypeMag.toString() + "\n" +
                      "Dialog edited magnitude: " + mag.toString() + "\n";

                    yn = JOptionPane.showConfirmDialog( Jiggle.this, str +
                      "[YES] set edited magnitude as MAGTYPE preferred.\n" +
                      " [NO] do not set edited magnitude as MAGTYPE preferred.",
                      "Set Preferred of MagType?",
                      JOptionPane.YES_NO_OPTION);
                    // if yes, new mag is set preferred of magtype
                    if (yn == JOptionPane.YES_OPTION) sol.setPrefMagOfType(mag);
                }
                // else noop
            }

            if (sol.isEventDbType(EventTypeMap3.TRIGGER) && !sol.getLatLonZ().isNull()) {
                sol.eventType.setValue(EventTypeMap3.toJasiType(EventTypeMap3.EARTHQUAKE)); //to not bump version -aww 2008/04/15
                sol.gtype.setValue(GTypeMap.LOCAL);
                toolBar.solPanel.typeChooser.updateEventTypeSelection(sol);
                resetAuthority(sol);
            }

            if (dialog.hasChangedOrigin() && sol.getPhaseList().size() > 3) {
                int yn = JOptionPane.showConfirmDialog(
                                  Jiggle.this,
                                  "Relocate using current settings?",
                                  "Locate Event",
                                  JOptionPane.YES_NO_OPTION
                                 );

                if (yn == JOptionPane.YES_OPTION) {
                    locate();
                    return true;
                }
                else {
                    resetVelocityModel(sol); // added 2011/07/28 -aww
                    resetAuthority(sol);
                }
            }


            updateTextViews(); // update the text the tabs & frame title
            resortWFViews(); // added this to resort alignment if origin changed - aww 2009/04/03

            return true;
        }

        return false;
    }

/** Pop a comment dialog box that allows editing the current or adding a
new comment. String is the default comment */
    protected void addComment(String str){
        Solution sol = mv.getSelectedSolution();
        if (sol != null) {
            //JTextField textField = new JTextField(str)
            JTextArea textField = new JTextArea();
            textField.setToolTipText("To SET comment to field's text press OK; to ERASE comment, press DELETE; to NOT CHANGE comment, press CANCEL");
            textField.setText(str);
            new JTextClipboardPopupMenu(textField); // enable paste,copy from/to clipboard
            JScrollPane jsp = new JScrollPane(textField);
            jsp.setPreferredSize(new Dimension(340,80));
            jsp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            jsp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

            int choice = JOptionPane.showOptionDialog(Jiggle.this, jsp, "Edit Event Comment",
                    JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE,
                    null, new String [] {"OK", "DELETE", "CANCEL"}, "YES");

            if (choice == JOptionPane.OK_OPTION) {
                String newComment = textField.getText();
                if (newComment != null) { // Update
                    if (! (!sol.hasComment() && newComment.equals("")) ) { // Not a commentless sol and nonblank input
                       if (! (sol.hasComment() && sol.getComment().equals(newComment)) ) { // Not a commented sol with text matching input
                           sol.setComment(newComment); // new or revised comment
                           //updateTextViews(); // ? - removed 02/17/2006 -aww
                           updateFrameTitle();
                       }
                    }
                }
            } else if (choice == JOptionPane.NO_OPTION) {
                textField.setText("");
                if (sol.hasComment()) sol.setComment("");
                updateFrameTitle();
                System.out.println("Deleted event comment text.");
            }
            // choice == cancel => add no comment or leavescurrent comment text unchanged
        }
    }

/** Pop a comment dialog box that allows editing the current or adding a
new comment. The default comment string is set to current solution's comment if
there is one. */
    protected void addComment(){
        String str = "";
        if (mv.getSelectedSolution().hasComment()) {
            str = mv.getSelectedSolution().getComment();
        }
        addComment(str);
    }

/**
 * Return a Collection of Waveforms that are associated with this solution. This is
 * a decision making method. It scans the available Waveforms (that are in the
 * WFViews) and decides which should be connected to this solution based on an
 * algorithm. The current algorithm is EVERYTHING.  <p>
 * Other possibilities are: <br>
 * Only waveforms with phase picks.<br>
 * Waveforms base on a distance decay function.<br>
 * Let operator pick <br>*/

    private Collection chooseWaveformSet(Solution sol) {
        // sol is not currently used but may be later when the logic is more sophistocated
        return mv.getWaveformList();    // just get ALL waveforms in the MasterView
    }

    private boolean checkForDuplicate(Solution sol) {

        if (duplicateCheckDisabled) return true;

        //System.out.println("DEBUG test check for duplicate on save.... now checking via stored procedure.");
        long dupId = sol.getAnyDuplicate();
        if (dupId <= 0l) return true;

        Object[] options = { "Continue", "Abort" };
        int yn = JOptionPane.showOptionDialog( Jiggle.this, sol.getById(dupId).toSummaryString(),
                "Event may have duplicate in catalog!!!", 
        JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);

        return (yn == JOptionPane.YES_OPTION) ? true : false;
    }

/**
 * Save the current selected solution to the dbase.
 */
// TODO: add progress bars

    public boolean saveToDb()  {
        return saveToDb(mv.getSelectedSolution());
    }

/**
 * Save the given solution to the dbase.
 */
    public boolean saveToDb(Solution sol) { // uses thread delegate

        if (! validateConnection("Save to database")) return false; 

        if (! doCommit()) return false;

        if (sol == null) return false;

        if (sol.isAuto()) sol.setProcessingState(JasiProcessingConstants.STATE_HUMAN); // added for AVO, upon save flips to "I" state 2012/02/03 -aww

        // Check if solution is stale (could allow saving of stale sol if asked).
        if (! hasGoodLocation(sol) ) {
          if (getVerbose()) popInfoFrame("SaveToDb", "Db save ABORT: relocate, review results, click SAVE again.");
          return false; // abort commit
        }

        // networkModeLAN  ??
        if (eventTypeChange(sol)) { // aww 06/15/2006
           if ( hasStaleLocation(sol) ) {
             return false; // abort commit
           }
           //updateFrameTitle(); // done by invocation via eventTypeChange or location engine listener
        }

        if (hasStaleMag(sol)) return false; // abort commit aww 10/11/2006

        // networkModeLAN  ??
        //if (! checkPrefor(sol)) return false; // aww  2014 not implemented yet
        if (! checkPrefMag(sol)) return false; // aww 06/15/2006

        // Here if user overrode of the test check above so reset flag to allow commit -aww 08/04/2006
        sol.setStale(false);

        // networkModeLAN  ??
        if (! checkForDuplicate(sol) ) {
          return false; // abort commit
        }

        // Check to avoid writing null gtype into db
        defaultNullGType(sol); // method added 2017/03/18 -aww

        // Not sure we need this:
        if (sol.isFinal() && getProperties().getBoolean("confirmFinalOnSave")) { // flip state without relocation -aww 2009/05/15
            int yn = JOptionPane.showConfirmDialog( Jiggle.this,
                   "[YES] keep finalized (F)\n[NO] set incomplete (I)\n",
                   "Input state is finalized (F)", JOptionPane.YES_NO_OPTION);

            if (yn != JOptionPane.YES_OPTION) sol.setProcessingState(JasiProcessingConstants.STATE_INTERMEDIATE_TAG);
        }


        final long evid = sol.id.longValue();
        final String msg = "SAVING event: "+ evid + " to database at " + new DateTime().toString(); // UTC time - aww 
        System.out.println(msg); //debug

        // do commit in background thread
        final Solution aSol = sol;
        final org.trinet.util.SwingWorker worker = new org.trinet.util.SwingWorker() {
            WorkerStatusDialog workStatus = new WorkerStatusDialog(Jiggle.this, true);
            boolean status = false;
            String exMessage = "";
            // NO GRAPHICS CALLS ALLOWED IN THIS METHOD UNLESS using SwingUtilites.invokeLater(new Runnable(...))!
            public Object construct() {
              workStatus.setBeep(beep);
              workStatus.pop("SaveToDb", msg, true);
              committing = true;
              try {
                  status = aSol.commit();
              } catch (JasiCommitException ex) {
                // aSol.rollbackPrefs(); // aww: could try to erase partial data here ?
                status = false;
                exMessage = ex.toString();
                ex.printStackTrace();
              }
              return null;
            }

            //Runs on the event-dispatching thread so graphics calls are OK here.
            public void finished() {
              workStatus.dispose(); // don't invoke unpop before dispose (may cause exception)
              if (! status) System.out.println(evid+" commit ERROR status message:\n"+aSol.getCommitStatus());
              if (status) { // update catPanel
                  if (status) {
                    // Do intermediate commit of summary info, rollback if problems
                    if ( aSol.postInterimCommit ) { // aww 2015/04/03 -posts even if commitResetsRflag is false, default is true
                      int rank =   getProperties().getInt("postInterimRank", 100);
                      int result = getProperties().getInt("postInterimResult", 0);
                      int icode = aSol.postId("TPP", "TPP", "INTERIM", rank, result);
                      String str = "Jiggle saveToDb: " + evid + " post to TPP TPP INTERIM " + rank + " " + result;
                      if ( icode < 0 ) {
                          str += " returned error status: "+ icode;
                          popInfoFrame("SaveToDb", str);
                      }
                      System.out.println(str);
                    }
                  }
                  SolutionList solList = catPane.getSolutionList();
                  int index = solList.indexOfIdentifier(Long.valueOf(evid)); //was aSol.getIdentifier() - aww 2009/03/05
                  // Avoid loaded data list refs in the catalog solutionlist, so create a new solution - aww 2009/03/05
                  Solution newSol = aSol.getById(evid);
                  if (index >= 0) solList.set(index, newSol); // was aSol, which may have loaded data- aww 2009/03/05
                  else solList.add(newSol); // was aSol - aww 2009/03/05
                  // does update do catalog sort here ?
                  updateCatalogTab();
                  resetSummaryTextAreas();
              }
              else { // bad save
                  popInfoFrame("Save Error", "<html><b>ERROR:</b> "+msg+".<p>"+
                                exMessage+"<p>"+aSol.getCommitStatus()+"</html>"
                  );
              }
              committing = false;
            }
        };
        worker.start();
        return true;
    }

    private boolean doCommit() {
        if (committing) {
            int yn = JOptionPane.showConfirmDialog(Jiggle.this, "Previous commit thread may still be active, do you wish to continue", "Commit",
                JOptionPane.YES_NO_OPTION);
            return (yn == JOptionPane.YES_OPTION);
        }
        return true;
    }

/**
 * Save all currently loaded solutions to the dbase.
 */
// TODO: use determinate progress bar, setting progress by list size count

    public void saveAllToDb() { // uses thread delegate

        if (! validateConnection("Save all to database")) return; 

        if (! doCommit()) return;

        final org.trinet.util.SwingWorker worker = new org.trinet.util.SwingWorker() {

          boolean finalStatusOk = true; // assume all will go ok

          WorkerStatusDialog workStatus = new WorkerStatusDialog(Jiggle.this, true);
          Solution mvSols[] = mv.solList.getArray();

          // NO GRAPHICS CALLS ALLOWED IN THIS METHOD UNLESS using SwingUtilites.invokeLater(new Runnable(...))!
          public Object construct() {
            int size = mvSols.length;
            workStatus.setBeep(beep);
            workStatus.pop("SaveToDb", "Saving solution list of size:"+size, true);

            Solution aSol = null;
            String msg = "";
            long evid = 0l;
            boolean status = false;
            committing = true;

            for (int i = 0; i<size; i++) {

              aSol = mvSols[i];
              if (aSol == null) continue;

              if (aSol.isAuto()) aSol.setProcessingState(JasiProcessingConstants.STATE_HUMAN); // added for AVO, upon save flips to "I" 2012/02/03 -aww

              evid = aSol.id.longValue();
              // Warn if solution is stale but allow saving of stale sol if asked.
              if (! hasGoodLocation(aSol) ) {
                msg ="Unable to save; check location is valid for id: " + evid;
                System.out.println(msg);
                workStatus.setText(msg);
                continue;
              }
              if (hasStaleMag(aSol) && !getProperties().getMagStaleCommitOk()) { // aww 10/11/2006
                msg ="Unable to save; a magnitude stale for id: " + evid;
                System.out.println(msg);
                workStatus.setText(msg);
                continue;
              }

              // networkModeLAN  ??
              //if (! checkPrefor(sol)) return false; // aww  2014 not implemented yet
              if (! checkPrefMag(aSol) ) { 
                msg ="Unable to save; preferred magnitude invalid for id: " + evid;
                System.out.println(msg);
                workStatus.setText(msg);
                continue;
              }
              
              // networkModeLAN  ??
              if (! checkForDuplicate(aSol) ) {
                msg ="Unable to save; duplicate in catalog for id: " + evid;
                System.out.println(msg);
                workStatus.setText(msg);
                continue;
              }

              // Here if user overrode of the test check above so reset flag to allow commit -aww 08/04/2006
              aSol.setStale(false);

              msg = "SAVING event: "+ evid + " to database at " + new DateTime().toString(); // UTC time - aww 
              System.out.println(msg);
              workStatus.setText(msg);
              try {
                  status = aSol.commit();
                  if (! status) {
                    System.out.println(evid+" commit ERROR status message:\n"+aSol.getCommitStatus());
                  }
                  if (status) { // update catPanel list with committed solution
                    SolutionList solList = catPane.getSolutionList();
                    int index = solList.indexOfIdentifier(Long.valueOf(evid)); //was aSol.getIdentifier() - aww 2009/03/05
                    // Avoid loaded data list refs in the catalog solutionlist, so create a new solution - aww 2009/03/05
                    Solution newSol = aSol.getById(evid);
                    if (index >= 0) solList.set(index, newSol); // was aSol, which may have loaded data- aww 2009/03/05
                    else solList.add(newSol); // was aSol - aww 2009/03/05
                    // Do intermediate commit of summary info, rollback if problems
                    if ( aSol.postInterimCommit ) { // aww 2015/04/03 -posts event if commitResetsRflag is false, default is true
                      int rank =   getProperties().getInt("postInterimRank", 100);
                      int result = getProperties().getInt("postInterimResult", 0);
                      int icode = aSol.postId("TPP", "TPP", "INTERIM", rank, result);
                      String str = "Jiggle saveToDb: " + evid + " post to TPP TPP INTERIM " + rank + " " + result;
                      if ( icode < 0 ) {
                          str += " returned error status: "+ icode;
                      }
                      System.out.println(str);
                    }
                  }
                  else {
                    finalStatusOk = false; // for any bad commits
                  }

              } catch (Exception ex) { // JasiCommitException ex) {
                // aSol.rollbackPrefs(); // aww: could try to erase partial data here
                workStatus.setText("Error during commit of id: "+evid);
                workStatus.beep(); // test
                System.err.println(ex.toString());
                System.err.println(evid+" commit status message: "+aSol.getCommitStatus());
                // give user time to notice error?
                try {
                  Thread.currentThread().sleep(1500l);
                }
                catch(InterruptedException ex2) {
                  // do nada
                }
              }

            }
            return null;
          }
          //Runs on the event-dispatching thread so graphics calls are OK here.
          public void finished() {
              workStatus.dispose(); // don't invoke unpop before dispose (may cause exception)
              updateCatalogTab();
              resetSummaryTextAreas();
              if (! finalStatusOk)
                popInfoFrame("Save All Error", "Error saving solution(s) in master list, check output.");
            committing = false;
          }
        };
        worker.start();
    }

/**
 * Save the current selected solution to the dbase and take site-specific finalization action.
 */
    public boolean finalToDb()  {
        return finalToDb(mv.getSelectedSolution());
    }

    private boolean eventTypeChange(Solution sol) {

        // don't bother checking for quarry unless enabled in properties
        if (eventTypeCheckDisabled) return false;

        // Ignore teleseism and unknown null gtypes plus any event types not listed in below test conditions 
        if ( ! (sol.isOriginGType(GTypeMap.LOCAL) ||
                sol.isOriginGType(GTypeMap.REGIONAL) ||
                sol.isEventDbType(EventTypeMap3.TRIGGER) ||
                sol.isEventDbType(EventTypeMap3.LONGPERIOD) ||
                sol.isEventDbType(EventTypeMap3.UNKNOWN) )
           ) return false;

        if (sol.isQuarry()) {

            whereEngine.setReference(sol.lat.doubleValue(), sol.lon.doubleValue());
            WhereSummary ws = whereEngine.whereSummaryType(GazetteerType.QUARRY);
            String units = getProperties().getProperty("whereUnits");
            GeoidalUnits gu = ( (units != null)  && (units.startsWith("M") || units.startsWith("m")) ) ?
                GeoidalUnits.MILES : GeoidalUnits.KILOMETERS;

            String quarryName = ws.fromWhereString(gu, false);

            int yn = JOptionPane.showConfirmDialog(
                   Jiggle.this,
                   quarryName + "\n" +
                   "[YES] set type QUARRY 'qb', fix depth, set comment name.\n"+
                   "[NO]  continue, do not change event type.\n",
                   "EVENT MAY BE A BLAST!",
                   JOptionPane.YES_NO_OPTION);

            if (yn == JOptionPane.YES_OPTION) {
                toolBar.solPanel.typeChooser.setSelectedItemByDbType(EventTypeMap3.QUARRY);
                return true;
            }
        }

        // Not a quarry or teleseism, check for regional or local mismatch
        boolean inNetwork = sol.isInsideNetwork(); 

        if (inNetwork) {
            if (sol.isOriginGType(GTypeMap.REGIONAL)) { 
                int yn = JOptionPane.showConfirmDialog(
                     Jiggle.this,
                     "Event declared REGIONAL is INSIDE local network\n"+
                     "[YES] set type LOCAL 'le'\n"+
                     "[NO]  continue, do not change event type.\n",
                     "EVENT MAY BE LOCAL!",
                     JOptionPane.YES_NO_OPTION);

                if (yn == JOptionPane.YES_OPTION) {
                    toolBar.solPanel.gtypeChooser.setSelectedItemByJasiType(GTypeMap.LOCAL);
                    return true;
                }
            }
        }
        else {
            if (sol.isOriginGType(GTypeMap.LOCAL)) {
                int yn = JOptionPane.showConfirmDialog(
                     Jiggle.this,
                     "Event declared LOCAL is OUTSIDE local network\n"+
                     "[YES] set type REGIONAL 're'\n"+
                     "[NO]  continue, do not change event type.\n",
                     "EVENT MAY BE REGIONAL!",
                     JOptionPane.YES_NO_OPTION);

                if (yn == JOptionPane.YES_OPTION) {
                    toolBar.solPanel.gtypeChooser.setSelectedItemByJasiType(GTypeMap.REGIONAL);
                    return true;
                }

            } 
        }

        return false;
    }

    private class MagRadioButtonModel extends JToggleButton.ToggleButtonModel {
        public Magnitude mag = null;

        public MagRadioButtonModel(Magnitude mag) {
            super();
            this.mag = mag;
        }
    }

    //private boolean checkPrefor(Solution sol) {
        //if (preforCheckDisabled) return true; // new property set true to reduce network query overhead 
        //long prefor = sol.getPreforByPriority();
        //if (prefor == 0l || prefor == sol.prefor.longValue() ) return true;
    //}

    private boolean checkPrefMag(Solution sol) {

        // Added check for preferred magnitude "size" vs. threshold  -aww 2010/08/11
        Magnitude prefMag = sol.magnitude;
        if (prefMag == null) return true;

        double magval = prefMag.getMagValue();
        double prefmagCheckValue = props.getDouble("prefmagCheckValue", 3.);
        if (magval >= prefmagCheckValue) {
           int yn = JOptionPane.showConfirmDialog( Jiggle.this,
                String.format("Event preferred magnitude %5.2f > %5.2f.%n Proceed anyway?", magval, prefmagCheckValue),
                "Prefmag Value Check",
                JOptionPane.YES_NO_OPTION
           );

           // if YES not clicked return 
           if (yn != JOptionPane.YES_OPTION) {
               return false;
           }
        }

        if (prefmagCheckDisabled) return true; // new property set true to reduce network query overhead 

        prefMag = sol.getPrefMagByPriority();
        if (prefMag == null || prefMag == sol.magnitude ) return true;

        JPanel jp = new JPanel();
        jp.setLayout(new GridLayout(0,1));
        jp.add(new JLabel("Highest ranking magnitude by priority is : " + prefMag.toString()));
        jp.add(new JLabel("The currently set preferred magnitude is checked below."));
        ButtonGroup bg = new ButtonGroup();

        Iterator iter = sol.getPrefMags().iterator();
        Magnitude mag = null;
        JRadioButton jrb = null;
        while (iter.hasNext()) {
            mag = (Magnitude) iter.next();
            jrb = new JRadioButton(mag.toString());
            jrb.setModel(new MagRadioButtonModel(mag));
            if (mag == sol.magnitude) jrb.setSelected(true);
            bg.add(jrb);
            jp.add(jrb);
        }
        jp.add(new JLabel("Select your preference then continue to save or abort save."));

                   //"Priority event preferred magnitude: " + prefMag.toString() + "\n" +
                   //"Current event preferred magnitude: " + sol.magnitude.toString() + "\n" +
                   //"[YES] change event preferred to priority magnitude.\n" +
                   //"[NO] do not change the event preferred magnitude.\n" +
                   //"[CANCEL] aborts current command.",
        Object[] options = { "Continue", "Abort" };
        int yn = JOptionPane.showOptionDialog(Jiggle.this, jp, "Preferred magnitude is NOT highest priority !!!", 
        JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);

        if (yn == JOptionPane.YES_OPTION) { // make preferred change
            mag = ((MagRadioButtonModel) bg.getSelection()).mag;   // save for comparison
            //Note do NOT set if already preferred, because setting resets lastPrefmagId and hasChanged() commit test fails
            if (mag != sol.magnitude) setPreferredMagnitude(sol, mag); 
            return true;
        }

        return false; // otherwise, abort further command actions
    }

    private void setPreferredMagnitude(Solution sol, final Magnitude prefMag) {
        // update display after change -aww 06/15/2006
        if (sol == null || prefMag == null) return;
        sol.setPreferredMagnitude(prefMag);
        System.out.println("Jiggle INFO: User set event preferred magid: " +
            prefMag.getIdentifier().toString() + " " + prefMag.toNeatString()); 
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                updateMagTab(prefMag.getTypeString());
            }
        });
    }

    /* Save the given solution to the dbase and take site-specific finalization action
     * */
    public boolean finalToDb(Solution sol) { // uses thread delegate

        if (! validateConnection("Finalize to database")) return false; 

        if (sol == null) return false;

        if (! doCommit()) return false;

        if (sol.isAuto()) sol.setProcessingState(JasiProcessingConstants.STATE_HUMAN); // added for AVO, upon save flips to "I" 2012/02/03 -aww

        // Check if solution is stale (could allow saving of stale sol if asked).
        if (! hasGoodLocation(sol) ) {
          if (getVerbose()) popInfoFrame("Finalize", "Db finalize ABORT: relocate, review, click FINALIZE again.");
          return false; // abort commit
        }

        // networkModeLAN  ??
        if (eventTypeChange(sol)) {
           if ( hasStaleLocation(sol) ) {
             return false; // abort commit
           }
        }

        //
        if ( sol.isEventDbType(EventTypeMap3.TRIGGER) ) {  // added this condition -aww 2010/10/19
            popInfoFrame("Finalize Event", "Finalize is not allowed for a subnet trigger");
            return false;
            /*
            int answer = JOptionPane.showConfirmDialog(Jiggle.this,
                "Are you sure you want to finalize a subnet trigger?",
                "Finalize Event", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            if (answer != JOptionPane.YES_OPTION) {
                return false;
            }
            */
        }
        //

        if (hasStaleMag(sol)) return false; // abort commit aww 10/11/2006

        // networkModeLAN  ??
        //if (! checkPrefor(sol)) return false; // aww  2014 not implemented yet
        if (! checkPrefMag(sol)) return false;

        // Here if user overrode of the test check above so reset flag to allow commit -aww 08/04/2006
        sol.setStale(false);

        // networkModeLAN  ??
        if (! checkForDuplicate(sol) ) {
          return false; // abort commit
        }

        // Check to avoid writing null gtype into db
        defaultNullGType(sol); // method added 2017/03/18 -aww

        final long evid = sol.id.longValue();
        final String msg = "FINALIZING event: " + evid + " to database at " + new DateTime().toString(); // UTC time - aww  " to database";
        System.out.println(msg); // debug

        // do commit in background thread
        final Solution aSol = sol;
        final org.trinet.util.SwingWorker worker = new org.trinet.util.SwingWorker() {
            // NO GRAPHICS CALLS ALLOWED IN THIS METHOD UNLESS using SwingUtilites.invokeLater(new Runnable(...))!
            WorkerStatusDialog workStatus = new WorkerStatusDialog(Jiggle.this, true);
            boolean status = false;
            String exMessage = "";

            public Object construct() {
              workStatus.setBeep(beep);
              workStatus.pop("Finalize", msg, true);
              committing = true;
              try {
                status = aSol.finalCommit();
              } catch (Exception ex) { // JasiCommitException ex) {
                 status = false;
                 exMessage = ex.toString();
                 ex.printStackTrace();
              }
              return null;
            }

            public void finished() {
                try {
                    workStatus.dispose(); // don't invoke unpop before dispose (may cause exception)
                    if (!status)
                        System.out.println("Jiggle.finalToDb ERROR status msg for id:" + evid + "\n" + aSol.getCommitStatus());
                    if (status) { // update catPanel just in case of changes
                        SolutionList solList = catPane.getSolutionList();
                        int index = solList.indexOfIdentifier(Long.valueOf(evid)); //was aSol.getIdentifier() - aww 2009/03/05
                        // Avoid loaded data list refs in the catalog solutionlist, so create a new solution - aww 2009/03/05
                        Solution newSol = aSol.getById(evid);
                        if (index >= 0) {
                            solList.set(index, newSol); // was aSol, which may have loaded data- aww 2009/03/05
                        } else solList.add(newSol); // was aSol - aww 2009/03/05
                        updateCatalogTab();
                        resetSummaryTextAreas();

                        // When finalizing in Scope mode, require a user to create a new event before allowing new picks
                        if (jiggle.scopeMode && getProperties().getBoolean(PropConstant.SCOPE_REQUIRE_NEW_EVENT_ON_FINAL) == true) {
                            jiggle.getMasterView().solList = new SolutionList();
                            pickPanel.disablePicking();

                            toolBar.setEventEnabled(false);
                            menuBar.setEventEnabled(false);
                            statusBar.setEventEnabled(false);
                            toolBar.resetUnpick();
                            toolBar.resetHidePanelToggleButtons();
                        }
                    } else { // bad finalize
                        popInfoFrame("Finalize Error",
                                "<html><b>ERROR:</b> " + msg + "<br>" +
                                        exMessage + "<br>" + aSol.getCommitStatus() + "</html>"
                        );
                    }
                    //System.out.println("DEBUG Jiggle.finalToDb pingDatabase(2) return = " +
                    //     String.valueOf(((oracle.jdbc.OracleConnection)DataSource.getConnection()).pingDatabase(2)));

                } catch (Exception ex) {
                    status = false;
                    exMessage = ex.toString();
                    ex.printStackTrace();
                }
                committing = false;
            }
        };

        worker.start();

        return true;
    }

    private void defaultNullGType(Solution sol) { // added this method 2017/03/18 -aww

        if (! sol.isOriginGType(GTypeMap.NULL) ) return; // only reset the gtype if its NULL

        int yn = JOptionPane.showConfirmDialog(
                     Jiggle.this,
                     "Preferred origin has NULL gtype, set default gtype?\n"+
                     "[YES] Set gtype LOCAL (REGIONAL if valid location).\n"+
                     "[NO]  Keep NULL gtype.\n",
                     "EVENT ORIGIN HAS NULL GTYPE!",
                     JOptionPane.YES_NO_OPTION
                 );

        if (yn != JOptionPane.YES_OPTION) return; // do not change gtype!

        if ( sol.hasLatLonZ() && ! sol.isInsideNetwork() ) { // origin is located outside of network, default to the regional gtype
            sol.setOriginGType(GTypeMap.REGIONAL);
            toolBar.solPanel.gtypeChooser.setSelectedItemByJasiType(GTypeMap.REGIONAL);
        }

        // Otherwise assume event is inside network, even when lat,lon,z are unknown
        sol.setOriginGType(GTypeMap.LOCAL);
        toolBar.solPanel.gtypeChooser.setSelectedItemByJasiType(GTypeMap.LOCAL);

    }

/** Checks two things: 1) if event needs to be relocated and 2) if the solution
* is good. Return true if there's a good, "fresh" (not stale)  location OR
* if the operator chose not to relocate. Warn user with
* dialog, if event is stale or location is null (lat, lon and z are 0.0) */
    protected boolean hasGoodLocation(Solution sol) {   // temp change from private access - aww
        // give option of relocating stale event, proceed regardless of outcome
        if (sol == null || hasStaleLocation(sol)) return false;

        // not stale, now check that its non-zero
        if (sol.hasLatLonZ()) return true;

        int ync = JOptionPane.YES_NO_OPTION;
        if (getProperties().getBoolean("confirmLocationOnSave")) { // -aww 2012/07/06
            ync = JOptionPane.showConfirmDialog( Jiggle.this,
                "WARNING: Solution has no location.\n"+ "Proceed anyway?",
                "No Location: ID = "+sol.id.toString(),
                JOptionPane.YES_NO_OPTION
            );
        }

        // [YES] return true to proceed
        return (ync == JOptionPane.YES_OPTION) ? true : false;
     }

    protected boolean hasStaleMag(Solution sol) {
        Iterator iter = sol.getPrefMags().iterator();
        MagList magList = new MagList();
        Magnitude mag = null;
        while (iter.hasNext()) {
            mag = (Magnitude) iter.next();
            if (mag.isStale()) magList.add(mag);
            if (! mag.algorithm.toString().equalsIgnoreCase("HAND") ) { 
                // below is temporary kludge for BK Mw Mec table problem -aww
                if (knownMagTypes.indexOf(mag.getTypeString().toLowerCase()) == -1) {
                    mag.setDependsOnOrigin(false); // won't commit to db 2006/11/14 -aww
                }
            }
        }
        if (magList.size() == 0) return false;

        // have at least one stale mag in list, so inform user
        int ync = JOptionPane.showConfirmDialog( Jiggle.this,
                "WARNING: Solution has stale magnitude.\n" +magList.dumpToString()+ "\nProceed anyway?",
                "EVID = "+sol.id.toString(),
                JOptionPane.YES_NO_OPTION
        );

        /*
        if (ync == JOptionPane.YES_OPTION ) {
          // If user reset all stale magnitudes state here, the magStaleCommitNoOp
          // property function is neutered (i.e. magnitude Orid association upon commit) 
          for (int idx = 0; idx < magList.size(); idx++) {
            ((Magnitude) magList.get(idx)).setStale(false);
          }
        }
        */

        // [YES] user override declares mag not stale (FALSE) 
        return  ! (ync == JOptionPane.YES_OPTION );

    }


    /**
       Check for stale solution. Warn user with dialog if stale and give option
       of locating the event. Return true is event is still stale, false if its OK.
    */
    private boolean hasStaleLocation(Solution sol) {
      if (sol.hasStaleLocation()) {

        //pop-up confirming  yes/no dialog:
        int ync = JOptionPane.showConfirmDialog(
            Jiggle.this, "WARNING: Solution is stale and should be relocated.\n"+
                     "[OK] relocate event and cancel database save to permit review.\n"+
                     "[NO] save to database without doing a relocation.\n"+
                     "[CANCEL] do not relocate or save to database (abort).\n"+
            "Relocate now?",
            "Stale Solution: ID = "+sol.id.toString(),
            //JOptionPane.OK_CANCEL_OPTION);
            JOptionPane.YES_NO_CANCEL_OPTION);

        // [YES] Re-locate, return result
        //if (ync == JOptionPane.OK_OPTION) {
        if (ync == JOptionPane.YES_OPTION) {
                locate() ;
                return true; // implied stale return forces human review and a re-commit ? - aww
        }
        // [NO] ignore stale solution
        else if (ync == JOptionPane.NO_OPTION) {
            // Resetting any of these counts below forces new origin creation by commit, and arrivals will be associated with new orid
            if (!props.getBoolean("solStaleCommitOk")) {
               JOptionPane.showMessageDialog( Jiggle.this,
                                              "Stale solution commit is disabled (check in QC Debug tab of property editor dialog)",
                                              "Stale Solution Commit Aborted",
                                              JOptionPane.PLAIN_MESSAGE
                                            );
                return true;
            }
            ync = JOptionPane.showConfirmDialog( Jiggle.this,
                                              "Set origin arrival count = 0, and null derived origin data associated with phases?\n",
                                              "Stale Solution Commit",
                                              JOptionPane.YES_NO_OPTION
                                         );
            if (ync == JOptionPane.YES_OPTION) {
              sol.usedReadings.setValue(0); // it's stale => so reset, unknown -aww 2011/05/11
              sol.sReadings.setValue(0);    // it's stale => so reset, unknown -aww 2011/05/11
              sol.phaseList.initSolDependentData(); // reset residuals, weight used
              sol.totalReadings.setValue(sol.phaseList.getChannelWithInWgtCount()); // do total update here, instead of in SolutionTN commit -aww 2011/05/11

              if (! sol.isDummy()) {
                ync = JOptionPane.showConfirmDialog(
                       Jiggle.this, "WARNING: Stale commit and origin is not already flagged bogus (dummy).\n"+
                       "[YES] set bogus.\n"+ "[NO] do not set bogus.\n"+ "Set origin bogus?",
                       "Stale Solution: ID = "+sol.id.toString(),
                       JOptionPane.YES_NO_OPTION
                    );
                if (ync == JOptionPane.YES_OPTION) {
                  sol.dummyFlag.setValue(1l);
                }
              }
            }
            return false ;
        }
        // [CANCEL] bail out, report as stale
        //if (ync == JOptionPane.CANCEL_OPTION) return true;

        // otherwise, abort further actions - aww 06/15/2006
        else return true;

      }
      return false;
    }

//
///////////////////// End of Solution Event Methods ////////////////////////
//

    public void locate() {
        Solution sol = mv.getSelectedSolution();
        if (sol == null) {
            popInfoFrame("Locate", "No solution is selected.");
        } else {
            boolean isOriginTypeOk = true;
            if (!sol.getOriginType().equals(Solution.UNKNOWN_TYPE) && !sol.getOriginType().equals(Solution.HYPOCENTER)) {
                OriginTypeList originList = new OriginTypeList(props, false);
                String selectedOrigin = originList.getDescriptionForType(sol.getOriginType());
                String msg = "Locating the event will reset the origin type from " + selectedOrigin +
                        " to " + originList.getDescriptionForType(Solution.HYPOCENTER) + ".\n\n" +
                        "Continue?";
                int response = JOptionPane.showConfirmDialog(this, msg, "Confirm Origin Type",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                isOriginTypeOk = response == JOptionPane.YES_OPTION ? true : false;
            }

            if (isOriginTypeOk) {
                if (solSolverDelegate != null) {
                    solSolverDelegate.locate(sol);
                } else {
                    JOptionPane.showMessageDialog(JiggleSingleton.getInstance().getMainJFrame(),
                            "Please wait for all channels to load before locating an event.",
                            "Cannot Locate",
                            JOptionPane.WARNING_MESSAGE);
                }
            }
        }
    }
    public void calcMagFromWaveforms(String type) {
        calcMag(type, true);
    }
    public void calcMag(String type) {
        calcMag(type, false);
    }
    protected void calcMag(String type, boolean fromWaveforms) {
        Solution sol = mv.getSelectedSolution();
        if (sol == null) {
          popInfoFrame("Calculate Magnitude", "No solution is selected.");
          return;
        }
        if (fromWaveforms) {
           if ( getProperties().getBoolean("confirmWFScanForMag") == false || JOptionPane.YES_OPTION ==
                   JOptionPane.showConfirmDialog(Jiggle.this, "WF scan destroys all existing amps/codas, continue?", "Confirm waveform scan",
                   JOptionPane.YES_NO_OPTION) ) solSolverDelegate.calcMagFromWaveforms(sol, type);
        }
        else {
            solSolverDelegate.calcMag(sol, type);
        }
    }

// --------------------------------------------------------------------------------
// OpenMap stuff
// --------------------------------------------------------------------------------
    protected void showMap() {
        final org.trinet.util.SwingWorker worker = new org.trinet.util.SwingWorker() {
          // NO GRAPHICS CALLS ALLOWED IN THIS METHOD UNLESS using SwingUtilites.invokeLater(new Runnable(...))!
          WorkerStatusDialog workStatus = new WorkerStatusDialog(Jiggle.this, true);
          //workStatus.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
          Component omf = mapFrame;
          boolean status = true;
          public Object construct() {
            workStatus.setBeep(beep);
            workStatus.pop("Map", "Creating map layers...", true);
            if (mapPanel == null) status = initMapPanel();
            return null;
          }
          //Runs on the event-dispatching thread so graphics calls are OK here.
          public void finished() {
            if (mapFrame == null) {
              initMapFrame(mapPanel);
              if (mapFrame instanceof JFrame) {
                Window w = ((Window) mapFrame);
                w.pack();
                w.setLocationRelativeTo(Jiggle.this);
              }
            }
            if (statusPanel != null) statusPanel.clearOnlyOnMatch("Making map ...");
            if (omf != mapFrame) firePropertyChange(NEW_MAP_PROPNAME, omf, mapFrame);
            workStatus.dispose();
            if (! status) System.err.println("Jiggle initMapPanel() failed to create map panel, see error messages.");
            if (getVerbose()) System.out.println(">>> finished creating map frame");
            showMapFrame();
          }
        };
        initStatusPanelForWork("Making map ...");
        worker.start();
    }

    private boolean initMapPanel() {

        boolean status = false;

        String mapPropFileName = getProperties().getUserFileNameFromProperty("mapPropFilename");

        if (getVerbose()) System.out.println("MapPanel property filename = "+mapPropFileName);
        if (mapPropFileName != null) {
            try {
                mapPropHandler = new JiggleMapPropertyHandler(Jiggle.this, mapPropFileName);
                if (debug) {
                    System.out.println("DEBUG Jiggle dump of mapHandler PROPERTIES");
                    mapPropHandler.getProperties().list(System.out);
                }
            } catch (MalformedURLException ex) {
                ex.printStackTrace();
                mapPropHandler = null;
            } catch (IOException ex) {
                ex.printStackTrace();
                mapPropHandler = null;
            }
        }

        if (mapPropHandler != null) { // properties defined create map panel
            mapPanel = new JiggleMapPanel(Jiggle.this, mapPropHandler);
            status = true;
        }
        return status;
    }


    // SwingWorker thread:
    private void initMapFrame(JiggleMapPanel mapPanel) {
        if (mapPanel == null) {
            popInfoFrame("InitMapFrame Error", "Map panel is null, unable to initialize map frame."); 
            return;
        }

        if (getProperties().getBoolean("mapInSplit")) 
           mapFrame = new JiggleMapInternalFrame("Jiggle Catalog Station Map");
        else
           mapFrame = new JiggleMapFrame("Jiggle Catalog Station Map");

        if (menuBar == null && toolBar == null) { // only do once!
            Jiggle.this.addWindowListener(new WindowAdapter() {        // Jiggle Frame closing
                public void windowClosing(WindowEvent e) {
                    if (mapFrame != null) disposeMap();
                }
            });
        }

        /*
        mapFrame.addWindowListener(new WindowAdapter() {
            public void windowActivated(WindowEvent e) {
                // window (or one of its  subcomponents) will receive keyboard events.
                mapWindowActiveFlag = true;
                //
                //MapHandler mapHdl = (MapHandler) mapFrame.getBeanContext();
                //if (mapHdl != null) {
                  //MapBean mapBean = mapHdl.getMapBean();
                  //if (mapBean != null && mapBean instanceof JiggleMapBean)
                    //mapBean.updateMapToolTipsEnabled(); //if bean OK then update tips enabled
                  //}
                //}
                //
            }

            public void windowDeactivated(WindowEvent e) {
                mapWindowActiveFlag = false;
                //
                //MapHandler mapHdl = (MapHandler) mapFrame.getBeanContext();
                //if (mapHdl != null) {
                  //MapBean mapBean = mapHdl.getMapBean();
                  //if (mapBean != null && mapBean instanceof JiggleMapBean)
                    //mapBean.updateMapToolTipsEnabled(); //if bean OK then update tips enabled
                  //}
                //}
                //
            }
        });

        */

        mapPanel.setVisible(true);
        MapHandler mapHandler = mapPanel.getMapHandler();
        mapHandler.add(mapFrame);
        
        //Iterator i = mapHandler.iterator();
        //while ( i.hasNext()) {
            //System.out.println(i.next().getClass().getName());
        //}

    }

    private void disposeMap() {
        if (mapFrame == null) return;

        if (mapPanel != null) {
            mapPanel.getMapHandler().remove(mapFrame);
        }

        if (mapFrame instanceof Window)
            ((Window)mapFrame).dispose();
        else if (mapFrame instanceof JInternalFrame) 
            ((JInternalFrame)mapFrame).dispose();

        mapFrame = null;
    }

    // Note the BasicMapPanel creates a MapBean, when bean or other children are added to MapHandler (beancontext)
    // the child's findAndInit method is invoked on Iterator of the bean context child members.
    // For example, the LayerHandler will add the bean to it's listener list when either it or the bean
    // is added to the mapHandler context, the bean receives a list of the "layers" sent via setLayers(LayerEvent).
    private void showMapFrame() {
        if (mapFrame != null) {
            //tabPanel[TAB_MAP].removeAll();
            //tabPanel[TAB_MAP].add(mapFrame, BorderLayout.CENTER);
            //tabPanel[TAB_MAP].revalidate(); // aww, redundant insurance?
            //selectTab(TAB_MAP);
            if (mv != null) setMapFrameTitle(getTitle());
            mapFrame.setVisible(true);
            resetMap();
            if (mapFrame instanceof Frame) {
                //System.out.println("DEBUG Jiggle.java calling toFront() in showMapFrame");
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        //((Frame)mapFrame).setExtendedState(Frame.MAXIMIZED_BOTH); // full screen 
                        ((Frame)mapFrame).setExtendedState(Frame.NORMAL); // restored from iconified 07/24/2007 -aww
                        ((Frame)mapFrame).toFront();
                    }
                });
            }
            updateTabPanelWithSplitCatalog();
        }
        else {
            popInfoFrame("Error", "Unable to show Map Frame");
            //dumpStringToTab("No map frame", TAB_MAP, false);
        }
    }

    protected void resetMap() {
        // Get the default MapBean that the BasicMapPanel created.
        MapBean mapBean = (MapBean) mapPanel.getMapBean();
        if (mapBean == null) {
            popInfoFrame("Error", "Unable to create bean for Map Frame");
            return;
        }
        mapBean.showLayerPalettes(); // ?? do we need this here ??

        // OpenMap Environment class sets default Latitude, Longitude and Scale via properties read by Handler
        // below uses other defined center and scale properties 
        //((JiggleMapBean) mapBean).setResetViewMapCenterScale();
    }

    public boolean isMapWindowActive() {
        return mapWindowActiveFlag;
    }

    public boolean setPickPanelToChannelStartingWith(String str) {
        return
            mv.masterWFViewModel.set( mv.wfvList.getChannelableStartingWith(str), mv.wfvList );
    }
    public void setTrialLocation(LatLonZ latLonZ) {
        solSolverDelegate.getLocationEngine().setTrialLocation(latLonZ);
    }

    public SolutionChanged getSolutionChangedListener() {
        return solutionChangedListener;
    }

    //
    // Use mv.wfvList list of the selected WFView objects 
    protected static int updateBadChannelList() {

        if (mv.wfvList == null || mv.wfvList.size() == 0) return 0;

        String badChannelListName = getProperties().getProperty("badChannelListName");         
        if ( badChannelListName == null) return 0;

        java.sql.CallableStatement ps = null;
        int status = 0;
        final String sql = "{ ?=call epref.updateBadChannelList(?,?,?,?,?,?,?,?) }";
        try {
          // progid-name,net,sta,seedchan,location,ondate_secs,offdate_secs
          ps = (java.sql.CallableStatement) DataSource.getConnection().prepareCall(sql);
          ps.registerOutParameter(1, java.sql.Types.INTEGER);
          WFView wfvSel = null;
          int cnt = mv.wfvList.size();
          for (int idx=0; idx<cnt; idx++) {
              wfvSel = (WFView) mv.wfvList.get(idx);
              if ( wfvSel.isSelected() ) {
                  ps.setString(2, badChannelListName);
                  ps.setString(3, wfvSel.chan.getNet());
                  ps.setString(4, wfvSel.chan.getSta());
                  ps.setString(5, wfvSel.chan.getSeedchan());
                  ps.setString(6, wfvSel.chan.getLocation());
                  ps.setString(7, wfvSel.getComment());
                  ps.setDouble(8, wfvSel.viewSpan.getStart());
                  ps.setDouble(9, wfvSel.viewSpan.getEnd());
                  ps.executeUpdate();
                  status += ps.getInt(1);
              }
          }

        }  
        catch (java.sql.SQLException ex) {
          ex.printStackTrace();
        }
        catch (MissingPropertyException ex) {
          ex.printStackTrace();
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, ps, debug);
          Utils.closeQuietly(ps);
        }
        return status;
    }
    //

} // end of Jiggle class

