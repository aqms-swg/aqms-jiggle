package org.trinet.util.graphics.table;
import java.awt.*;
import java.beans.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import org.trinet.jasi.*;
import org.trinet.util.*;
import org.trinet.util.graphics.*;
import org.trinet.util.graphics.text.*;

public abstract class AbstractJasiTableRowTextParser extends AbstractSolutionAssociatedTextParser
  implements JasiTableRowTextParserIF, PropertyChangeListener {
  protected Window owner;
  protected JasiListTableModelIF tableModel;
  protected AbstractJasiListPanel tablePanel;
  protected JTextArea textArea;
  protected JTextClipboardPopupMenu clipboardMenu;
  protected JOptionPane jop;
  protected JDialog dialog;

  public AbstractJasiTableRowTextParser() {}

  public void setDialogOwner(Window aWindow) {
          owner = aWindow;
  }
  public void setRowModel(JasiListTableModelIF tableModel) {
          this.tableModel = tableModel;
  }
  public void setTablePanel(AbstractJasiListPanel tablePanel) {
          this.tablePanel = tablePanel;
          setRowModel(tablePanel.getModel());
  }
  // arg should be generic component to abstract to interface:
  public void createInputTextDialog(JComponent parserOwningPanel) {
    owner = (parserOwningPanel == null) ? null :
        (Window) parserOwningPanel.getTopLevelAncestor();
    if (parserOwningPanel instanceof AbstractJasiListPanel)
            setTablePanel((AbstractJasiListPanel)parserOwningPanel);
    initDialog();
    dialog.setVisible(true);
    //Container comp = jop.getFocusCycleRootAncestor();
    //comp.transferFocus();
    //jdk1.4 issue textArea.requestFocusInWindow(); // aww 02/24/2005
  }
  protected void initOptionPane() {
    if (jop != null) {
      jop.setValue(null); // JOptionPane.UNINITIALIZED_VALUE); // jop.setValue(null)?
      return;
    }
    textArea = new JTextArea("", 24, 80);
    textArea.setEditable(true);
    textArea.setFont(new Font("Monospaced", Font.PLAIN, 12));
    clipboardMenu = new JTextClipboardPopupMenu(textArea);
    clipboardMenu.setAddUndo();
    JScrollPane jsp = new JScrollPane(textArea);
    JPanel textPanel = new JPanel();
    textPanel.setLayout(new BorderLayout());
    Box box = new Box(BoxLayout.X_AXIS);
    box.add(Box.createHorizontalGlue());
    JButton clearButton = new JButton("Clear");
    clearButton.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent evt) {
           textArea.setText("");
        }
    });
    box.add(clearButton);
    box.add(Box.createHorizontalGlue());
    textPanel.add(box, BorderLayout.NORTH);
    textPanel.add(jsp,BorderLayout.CENTER);
    jop = new JOptionPane(textPanel,JOptionPane.PLAIN_MESSAGE,JOptionPane.OK_CANCEL_OPTION);
    if (tablePanel != null) jop.addPropertyChangeListener(this);
  }
  protected void initDialog() {
    initOptionPane();
    if (dialog != null) dialog.dispose(); 
    dialog = jop.createDialog(owner,"Jasi Table Row Input " + parserName);
    dialog.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
    dialog.setModal(false);
    if (dialogTitle != null) dialog.setTitle(dialogTitle);
    dialog.setSize(new Dimension(600,200));
  }
  public void addWindowListener(WindowListener listener) {
    if (dialog != null) dialog.addWindowListener(listener);
  }
  public final void addTopLevelWindowListener(Window window) {
    window.addWindowListener(this.new DialogWindowListener());
  }
  protected class DialogWindowListener extends WindowAdapter {
    public void windowClosing(WindowEvent evt) {
      if (dialog != null) {
        dialog.dispose();
      }
    }
  }
  public void propertyChange(PropertyChangeEvent e) {
    //System.out.println("Src: "+e.getSource().getClass().getName()+" PropName: "+e.getPropertyName().toString());
    //if (e.getNewValue() != null) System.out.println("newValue: "+e.getNewValue().toString());
    if ((e.getSource() == jop) && e.getPropertyName().equals(JOptionPane.VALUE_PROPERTY)) {
      Object value = e.getNewValue();
      if (value == JOptionPane.UNINITIALIZED_VALUE) return;
      if (value != null && value instanceof Integer) { 
        int state = ((Integer)value).intValue();
        if (state == JOptionPane.OK_OPTION) {
          parseInputTableRows(textArea.getText());
          if (jrList == null) return;
          if (hasParseErrors()) {
            InfoDialog.informUser(tablePanel,
              "INFO", "Errors encountered parsing data for panel "
              +tablePanel.getPanelName(), null);
            if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(tablePanel,"Save parsed rows in table")) {
              InfoDialog.informUser(tablePanel,
                 "INFO", " No parsed rows saved for panel " + tablePanel.getPanelName(), null);
              return;
            }
          }
          if (parseCount() < 1) {
            InfoDialog.informUser(tablePanel, "INFO",
              "No data rows parsed for panel "
              +tablePanel.getPanelName(), null);
          }
          else {
            for (int idx = 0; idx < jrList.size(); idx++) {
              //tablePanel.insertRow((JasiCommitableIF)jrList.get(idx)); // 01/15/03 aww try this:
              addOrReplaceRow((JasiCommitableIF)jrList.get(idx));
            }
            // aww - below code line needed to only to synch a modelList cloned
            // from the input solution for the newly added parsed objects
            // if list not cloned below method only just resets listUpdateNeeded flag.
            tablePanel.synchInputList();
          }
          //if (jrList != null) jrList.clear(); // not here, perhaps better done at beginning of parse method
        }
        else if (state == JOptionPane.CANCEL_OPTION) {
          System.out.println("Parser input cancelled for:" + getClass().getName());
        }
        else {
          System.err.println("Unknown selection of input option: " + state);
        }
        //jop.setValue(JOptionPane.UNINITIALIZED_VALUE);
      }
    }
  }
  //need ListDataListeners implemented by model, list changes then model notifies 
  protected boolean addOrReplaceRow(JasiCommitableIF jc) {
    //return tablePanel.insertRow((JasiCommitableIF)jrList.get(idx));
    return tablePanel.addOrReplaceRow(jc);
  }
}

