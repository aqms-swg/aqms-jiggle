// this is not yet used by any class 08/2004
package org.trinet.jasi.TN;
// import org.trinet.jasi.*;
public interface DbaseMagAssocReadingTN extends DbaseSolAssocReadingTN {
    public boolean dbaseAssociateWithMag(boolean updateExisting);
    // add others if needed to jasi package access
}
