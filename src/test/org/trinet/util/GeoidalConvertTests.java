package test.org.trinet.util;

import org.junit.jupiter.api.Test;
import org.trinet.util.gazetteer.GeoidalConvert;
import org.trinet.util.gazetteer.GeoidalUnits;
import org.trinet.util.gazetteer.LatLonZ;
import test.common.UnitTests;

import static org.junit.jupiter.api.Assertions.*;

public class GeoidalConvertTests extends UnitTests {

    @Test
    public void verify_horizontal_distance_km_between_returns_correct_value() {
        assertEquals(314.40330633695874, GeoidalConvert.horizontalDistanceKmBetween(1, 2, 3, 4));
    }

    @Test
    public void verify_horizontal_distance_km_between_with_geoidal_input_returns_correct_value() {
        assertEquals(471.50908588878855, GeoidalConvert.horizontalDistanceKmBetween(new LatLonZ(1, 2, 3), new LatLonZ(4, 5, 6)));
    }

    @Test
    public void verify_horizontal_distance_km_between_with_geoidal_input_returns_correct_nan() {
        assertEquals(Double.NaN, GeoidalConvert.horizontalDistanceKmBetween(new LatLonZ(0.0, 0.0, 0.0), new LatLonZ(4, 5, 0.0)));
    }

    @Test
    public void verify_distance_km_between_returns_correct_value() {
        assertEquals(471.5186296167745, GeoidalConvert.distanceKmBetween(1, 2, 3, 4, 5, 6));
    }

    @Test
    public void verify_distance_km_between_with_geoidal_input_returns_correct_value() {
        assertEquals(471.5186296167745, GeoidalConvert.distanceKmBetween(new LatLonZ(1, 2, 3), new LatLonZ(4, 5, 6)));
    }

    @Test
    public void verify_azimuth_degrees_to_input_returns_correct_value() {
        assertEquals(44.951998353879446, GeoidalConvert.azimuthDegreesTo(1, 2, 3, 4));
    }

    @Test
    public void verify_azimuth_degrees_to_with_geoidal_input_returns_correct_value() {
        assertEquals(44.91707273404404, GeoidalConvert.azimuthDegreesTo(new LatLonZ(1, 2, 3), new LatLonZ(4, 5, 6)));
    }

    @Test
    public void verify_elevation_degrees_to_with_geoidal_input_returns_correct_value() {
        assertEquals(0.364542379154231, GeoidalConvert.elevationDegreesTo(new LatLonZ(1, 2, 3), new LatLonZ(4, 5, 6)));
    }

    @Test
    public void verify_elevation_degrees_to_returns_correct_value() {
        assertEquals(0.364542379154231, GeoidalConvert.elevationDegreesTo(1, 2, 3, 4, 5, 6));
    }

    @Test
    public void verify_direction_string_returns_N() {
        assertEquals("N  ", GeoidalConvert.directionString(-1.0));
    }

    @Test
    public void verify_direction_string_returns_NNE() {
        assertEquals("NNE", GeoidalConvert.directionString(23.5));
    }

    @Test
    public void verify_direction_string_returns_NE() {
        assertEquals("NE ", GeoidalConvert.directionString(46.5));
    }

    @Test
    public void verify_direction_string_returns_ENE() {
        assertEquals("ENE", GeoidalConvert.directionString(68.5));
    }

    @Test
    public void verify_direction_string_returns_E() {
        assertEquals("E  ", GeoidalConvert.directionString(90.0));
    }

    @Test
    public void verify_direction_string_returns_ESE() {
        assertEquals("ESE", GeoidalConvert.directionString(113.0));
    }

    @Test
    public void verify_direction_string_returns_SE() {
        assertEquals("SE ", GeoidalConvert.directionString(135.0));
    }

    @Test
    public void verify_direction_string_returns_SSE() {
        assertEquals("SSE", GeoidalConvert.directionString(158.5));
    }

    @Test
    public void verify_direction_string_returns_S() {
        assertEquals("S  ", GeoidalConvert.directionString(182.5));
    }

    @Test
    public void verify_direction_string_returns_SSW() {
        assertEquals("SSW", GeoidalConvert.directionString(204.5));
    }

    @Test
    public void verify_direction_string_returns_SW() {
        assertEquals("SW ", GeoidalConvert.directionString(227.0));
    }

    @Test
    public void verify_direction_string_returns_WSW() {
        assertEquals("WSW", GeoidalConvert.directionString(250.0));
    }

    @Test
    public void verify_direction_string_returns_W() {
        assertEquals("W  ", GeoidalConvert.directionString(272.5));
    }

    @Test
    public void verify_direction_string_returns_WNW() {
        assertEquals("WNW", GeoidalConvert.directionString(302.5));
    }

    @Test
    public void verify_direction_string_returns_NW() {
        assertEquals("NW ", GeoidalConvert.directionString(304.5));
    }

    @Test
    public void verify_direction_string_returns_NNW() {
        assertEquals("NNW", GeoidalConvert.directionString(334.5));
    }

    @Test
    public void verify_to_km_returns_correct_value_with_meters_unit() {
        assertEquals(1000, GeoidalConvert.toKm(1000000, GeoidalUnits.METERS));
    }

    @Test
    public void verify_to_km_returns_correct_value_with_kilometers_unit() {
        assertEquals(1000000, GeoidalConvert.toKm(1000000, GeoidalUnits.KILOMETERS));
    }

    @Test
    public void verify_to_km_returns_correct_value_with_feet_unit() {
        assertEquals(304.80091927957255, GeoidalConvert.toKm(1000000, GeoidalUnits.FEET));
    }

    @Test
    public void verify_to_km_returns_correct_value_with_miles_unit() {
        assertEquals(1609347.0878864445, GeoidalConvert.toKm(1000000, GeoidalUnits.MILES));
    }
}

