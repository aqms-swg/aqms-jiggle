package org.trinet.pcs;
import org.trinet.jasi.*;

// NOTE: You need to configure implementaton to module needs
public class DummyProcessingController extends AbstractChannelListProcessingController {

    public DummyProcessingController() { }

    public DummyProcessingController(String appName, String propFileName) {

        super(appName, null, null, null);

        if ( propFileName == null || propFileName.equals("") ) {
          // Load "default" properties file which should be in the
          // subdir below users home named: "."+appName.toLowerCase()
          setProperties((PcsPropertyList)null); 
        }
        else setProperties(propFileName);// Note props must be SolutionWfEditor subclass to use scanNoiseType and channelTimeWindowModel props
    }

// Test method should be in a class of a user test package e.g. org.trinet.test.TestDummyProcessor
    public static final void main(String [] args) {

        String propFileName     = "";
        String appName     = "dummy";
        String user = null;
        String pass = null;

        int nargs = args.length;
        if (nargs < 1 || args[0].equals("?")) {
            System.out.println("args: [appName] [propFile] [user] [pwd]");
            return;
        }
        switch (nargs) {
            case 4 :
                pass = args[3];
            case 3 :
                user = args[2];
            case 2:
                propFileName = args[1];
            case 1:
                appName = args[0];
        }

        try (DummyProcessingController pc = new DummyProcessingController(appName, propFileName)) {

            System.out.println("Processing events for: " +
                    pc.getGroupName() + " " + pc.getThreadName() + " " + pc.getStateName());
            System.out.println("Using propertyfile: " + ((propFileName.equals("")) ? "DEFAULT" : propFileName));

            if (user != null) pc.props.setDbaseUser(user);
            if (pass != null) pc.props.setDbasePasswd(pass);

            // test above user/pass logic by removing their defs from properties file.
            // user and pass will be used for connection only if missing from properties
            int result = pc.setupForProcessing();

            System.out.println(">>> Processing Settings Setup <<<");
            System.out.println(pc.getProcessingAttributesString());
            System.out.println(">>><<<");

            if (result <= 0) {
                System.err.println("ERROR DummyProccessingController: Check for configuration or DB ERROR.");
            }
            else {

                result = pc.processIds();

                if (result > 0)
                    System.out.println("\nDummyProcessingController events processed: " + result);
                else if (result == 0)
                    System.out.println("DummyProccessingController: Check for valid event id state postings or rank BLOCKING.");
                else if (result < 0)
                    System.err.println("ERROR DummyProccessingController: Check LOG for processing error messages.");

            }

            pc.closeProcessing();
        }
    }
}
