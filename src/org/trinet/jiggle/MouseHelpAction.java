package org.trinet.jiggle;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.*;
import org.trinet.util.graphics.text.JTextClipboardPopupMenu;

public class MouseHelpAction extends AbstractAction {
    
    private JDialog jd = null;

    public MouseHelpAction() {
        super("Mouse button action help");
    }

    public void actionPerformed(ActionEvent e) {
        makeHelpDialog();
    }
    
    protected void makeHelpDialog() {

        if (jd != null && jd.isVisible()) {
            return;
        }

        JTextArea jta = new JTextArea(); 
        jta.setEditable(false);
        jta.append("Jiggle Tab Pane Navigation\n");
        jta.append(" Typing 'C' in Waveform, Location, Magnitude, or Message tab selects Catalog tab.\n");
        jta.append(" Typing 'L' in Waveform, Magnitude, Catalog, or Message tab selects Location tab.\n");
        jta.append(" Typing 'M' in Waveform, Location, Catalog, or Message tab selects Magnitude tab.\n");
        jta.append(" Typing 'T' in Waveform, Location, Magnitude, or Catalog tab selects Message tab.\n");
        jta.append(" Typing 'W' in Location, Magnitude, Catalog, or Message tab selects Waveform tab.\n");
        jta.append(" Case-sensitive in Waveform panel, because 'c' and 't' are defined for other actions, see Picking help\n\n"); 

        jta.append("Location, Magnitude Tab Data Lists:\n");
        jta.append(" SHIFT+LEFT click, selects list data element then shows selected channel waveform's panel in waveform tab.\n");
        jta.append(" DELETE key, deletes selected channel data element from list (i.e. highlighted phase, amp).\n\n");

        jta.append("Waveform Tab Picking Panel:\n");
        jta.append(" LEFT click drag and release defines viewport time bounds using waveform's min,max amp bounds.\n");
        jta.append(" SHIFT+LEFT click drag and release defines both viewport time bounds and viewport amp bounds.\n\n");

        jta.append(" Phase time picking:\n");
        jta.append("  LEFT click centers waveform viewport at the click time.\n");
        jta.append("  RIGHT click pops up a phase descriptor menu for pick at the viewport center time.\n\n");
        jta.append("  Uncertainty in phase time:\n");
        jta.append("    CTRL+LEFT click DRAG straddling a pick sets its uncertainty duration, centered on the pick.\n\n");
        
        jta.append(" Coda duration picking:\n");
        jta.append("  Note a P or S phase pick is required to scan waveform for coda decay fit.\n");
        jta.append("      Inside S coda, CTRL+LEFT click to define start time then CTRL+LEFT click again to define end time.\n");
        jta.append("      or use SHIFT+CTRL+LEFT click to scan a \"filtered\" waveform if its amplitude units is counts.\n");
        jta.append("      a double-click, or an CTRL+ALT click, resets the timespan (erases window marker lines).\n\n");

        jta.append(" Peak amplitude picking:\n");
        jta.append("  Note a phase pick is not required to scan waveform for maximum Wood-Anderson amplitude.\n");
        jta.append("      ALT+LEFT click to define start time then ALT+LEFT click again to define end time.\n");
        jta.append("      or use SHIFT+ALT+LEFT click to scan a \"filtered\" waveform if its amplitude units is counts.\n");
        jta.append("      a double-click, or an CTRL+ALT click, resets the timespan (erases window marker lines).\n\n");

        jta.append(" Auto phase pick :\n");
        jta.append("  Requires an automatic phase picker instance setup via properties. Requires use of a MIDDLE mouse button.\n");
        jta.append("      CTRL+MIDDLE click to define start time before pick then CTRL+MIDDLE click again to define end time after pick.\n");
        jta.append("      or use SHIFT+CTRL+MIDDLE click to scan a \"filtered\" waveform if its amplitude units is counts.\n");
        jta.append("      a double-click, or an CTRL+ALT MIDDLE click, resets the timespan (erases window marker lines).\n\n");

        jta.append(" Noise level in timespan:\n");
        jta.append("  Requires use of a MIDDLE mouse button.\n");
        jta.append("      ALT+MIDDLE click to define noise span's start time then ALT+MIDDLE click again to define its end time.\n");
        jta.append("      or use SHIFT+ALT+MIDDLE click to scan a \"filtered\" waveform.\n");
        jta.append("      a double-click, or an CTRL+ALT click, resets the timespan (erases window marker lines).\n\n");

        jta.append(" Peak amplitude picking:\n");
        jta.append("  Note a phase pick is not required to scan waveform for max WA amp.\n");
        jta.append("      ALT+LEFT click to define start time then ALT+LEFT click again to define end time.\n");
        jta.append("      or use SHIFT+ALT+LEFT click to scan a \"filtered\" waveform if its amplitude units is counts.\n");
        jta.append("      a double-click, or an CTRL+ALT click, resets the timespan (erases window marker lines).\n\n");

        jta.append("Catalog Tab Table:\n");
        jta.append(" LEFT double-click a table column header cell to sort table rows with column's value ascending.\n");
        jta.append(" LEFT+SHIFT double-click a table column header cell to sort table rows with column's value descending.\n\n");
        //jta.append(" LEFT+ALT click a table cell with a truncated value to resize its column width.\n\n");

        jta.append("Map Layer Mouse Actions:\n");
        jta.append(" Network Stations:\n");
        jta.append("   LEFT double-click on a station's symbol to change view selection to first one matching that station's name.\n");
        jta.append(" Event Catalog:\n");
        jta.append("   LEFT double-click on an event's symbol to load that event into waveform view.\n"); 
        jta.append(" MasterView Solutions:\n");
        jta.append("   LEFT+SHIFT click anywhere on map to change the currently selected event's origin to the mouse's map location.\n");
        jta.append(" Faults:\n");
        jta.append("   LEFT click on a map fault segment to reveal the fault name tooltip (e.g. when regions boundaries layer is active).\n\n");

        jta.setCaretPosition(0);

        jta.setBackground(Jiggle.jiggle.getBackground());

        new JTextClipboardPopupMenu(jta);

        JOptionPane jop = new JOptionPane(new JScrollPane(jta), JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION);
        jd = jop.createDialog(Jiggle.jiggle,  "Mouse button action help");
        jd.setModal(false);
        jd.pack();
        Dimension screenSize = Jiggle.jiggle.getToolkit().getScreenSize();
        //jd.setSize(new Dimension((int)(.75*screenSize.width), (int)(.75*screenSize.height)));
        jd.setSize(new Dimension(screenSize.width/2, screenSize.height/2));
        Dimension size = jd.getSize();
        jd.setLocation((int)(screenSize.width - size.width)/2, (int)(screenSize.height - size.height)/2);
        jd.setVisible(true);
    }
}

