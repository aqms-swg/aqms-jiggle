package org.trinet.util.velocitymodel;

/** Modified Hadley/Kanamori model used to locate southern California earthquakes. */
public class HK_SoCalVelocityModel extends UniformFlatLayerVelocityModel {
    public static final double [] BOUNDARY_DEPTHS   = {0.0, 5.5, 16., 32.} ;
    public static final double [] LAYER_VELOCITIES  = {5.5, 6.3, 6.7, 7.8} ;
//    public static final double DEFAULT_PS_RATIO = 1.78;
    public static final double DEFAULT_PS_RATIO = 1.73;

    public HK_SoCalVelocityModel()  {
        super("HK_SOCAL", BOUNDARY_DEPTHS, LAYER_VELOCITIES, DEFAULT_PS_RATIO);
    }
}
