package org.trinet.util.graphics;

import java.awt.*;                                                                  
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import org.trinet.util.*;
//import com.borland.jbcl.layout.*;

/** a Component made up of two DateTimeChoosers for specifing a date/time range.
For use in dialogs. */
public class DateRangeChooser extends JPanel {

    static final String startLabelDef = "Start Time (UTC)";
    static final String endLabelDef   = "End Time (UTC)";

    String startLabel ;
    String endLabel ;

    JButton startDeltaButton   = new JButton();
    JButton endDeltaButton     = new JButton();
    JButton endZeroButton      = new JButton();
    JButton startZeroButton    = new JButton();

    /** Starting time. */
    DateTimeChooser startChooser;

    /** Ending time. */
    DateTimeChooser endChooser;

    String defaultUnits = "Hours";
    double defaultValue = 1.;

    private boolean endAdjustingStart = false;
    private boolean startAdjustingEnd = false;

    JPanel endButtonPanel   = new JPanel();
    JButton nowButton       = new JButton();
    JPanel startButtonPanel = new JPanel();

/** Create a DateRangeChooser with default labels: "Start Time", "End Time"*/
     public DateRangeChooser() {
            this(startLabelDef, endLabelDef);
     }

/** Create a DateRangeChooser with default labels: "Start Time", "End Time"*/
     public DateRangeChooser(String startLabel, String endLabel, TimeSpan ts) {
            this(startLabelDef, endLabelDef);
            setTimeSpan(ts);
     }

/** Create a DateRangeChooser with the specified labels for start and end. */
     public DateRangeChooser(String startLabel, String endLabel) {

            this.startLabel = startLabel;
            this.endLabel = endLabel;

          try  {
               jbInit();
          }
          catch(Exception ex) {
               ex.printStackTrace();
          }

          // set default time
        DateTime now  = new DateTime();	// set time to current UTC
        double endTime = now.getTrueSeconds(); // used to nominal, changed to UTC -aww 2008/02/07
        double hoursBack = 36.;
        double startTime = endTime - (3600. * hoursBack);


        setTimeSpan(new TimeSpan(startTime, endTime));
     }

     /** Create the GUI*/
     void jbInit() throws Exception {

        // start panel
        JPanel startPanel = new JPanel();
        startChooser = new DateTimeChooser(startLabel);
        startChooser.setMutate(true);

          startPanel.setLayout(new BorderLayout());
          startDeltaButton.setText("Delta...");
          startDeltaButton.addActionListener(new java.awt.event.ActionListener() {

               public void actionPerformed(ActionEvent e) {
                    startDeltaButton_actionPerformed(e);
               }
          });

          nowButton.addActionListener(new java.awt.event.ActionListener() {

               public void actionPerformed(ActionEvent e) {
                    nowButton_actionPerformed(e);
               }
          });

          // zero button
          startZeroButton.setText("00:00:00");
          startZeroButton.addActionListener(new java.awt.event.ActionListener() {
               public void actionPerformed(ActionEvent e) {
                    startChooser.setTrueSeconds(startChooser.getDateTime().getStartOfDay()); // UTC -aww 2008/02/12
               }
          });

//          startButtonPanel.setLayout(new BorderLayout());
          startButtonPanel.setLayout(new ColumnLayout());

          startButtonPanel.add(startDeltaButton);
          startButtonPanel.add(startZeroButton);

          startPanel.add(startChooser, BorderLayout.CENTER);
          startPanel.add(startButtonPanel, BorderLayout.EAST);

          // end panel
          JPanel endPanel = new JPanel();
	     endChooser   = new DateTimeChooser(endLabel);
         endChooser.setMutate(true);

          endPanel.setLayout(new BorderLayout());
          endDeltaButton.setText("Delta...");
          endDeltaButton.addActionListener(new java.awt.event.ActionListener() {

               public void actionPerformed(ActionEvent e) {
                    endDeltaButton_actionPerformed(e);
               }
          });

          endZeroButton.setText("00:00:00");
          endZeroButton.addActionListener(new java.awt.event.ActionListener() {
               public void actionPerformed(ActionEvent e) {
                    endChooser.setTrueSeconds(endChooser.getDateTime().getStartOfDay()); // UTC -aww 2008/02/12
               }
          });

          endButtonPanel.setLayout(new ColumnLayout());
          nowButton.setText("Now");

          endButtonPanel.add(endDeltaButton);
          endButtonPanel.add(endZeroButton);
          endButtonPanel.add(nowButton);

          endPanel.add(endChooser, BorderLayout.CENTER);
          endPanel.add(endButtonPanel, BorderLayout.EAST);


        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(startPanel);
        add(endPanel);
     }

     public void addItemListeners() {
        //startChooser.addActionListener(
        //    new ActionListener() {
        //        public void actionPerformed(ActionEvent evt) {
        //
        startChooser.addItemListener(
                new ItemListener() {
                public void itemStateChanged(ItemEvent evt) {
                    if (!(evt.getStateChange() == ItemEvent.SELECTED)) return;
                    if (endAdjustingStart) return;  // it's resetting comboboxes, which gets us here, so don't go further
                    startAdjustingEnd = true;       // set flag to indicate start action is adjusting the end time
                    DateTime start = startChooser.getDateTime();
                    if (start.compareTo(endChooser.getDateTime()) > 0) {
                        endChooser.setTime(start);
                    }
                    startAdjustingEnd = false;
                }
            }
        );

        //endChooser.addActionListener(
        //    new ActionListener() {
        //        public void actionPerformed(ActionEvent evt) {
        endChooser.addItemListener(
            new ItemListener() {
                public void itemStateChanged(ItemEvent evt) {
                    if (!(evt.getStateChange() == ItemEvent.SELECTED)) return;
                    if (startAdjustingEnd) return; // it's resetting comboboxes, which gets us here, so don't go further
                    endAdjustingStart = true;      // set flag to indicate end action is adjusting the start time
                    DateTime end = endChooser.getDateTime();
                    if (end.compareTo(startChooser.getDateTime()) < 0) {
                        startChooser.setTime(end);
                    }
                    endAdjustingStart = false;
                }
            }
        );
        //
     }


     /** Enable/disable all the active components that make up this component.*/
     public void setEnabled(boolean tf) {
        startDeltaButton.setEnabled(tf);
        endDeltaButton.setEnabled(tf);
        nowButton.setEnabled(tf);
        startChooser.setEnabled(tf);
        endChooser.setEnabled(tf);
        startZeroButton.setEnabled(tf);
        endZeroButton.setEnabled(tf);
     }

     /** Set the start and end times of the chooser with a TimeSpan that has true UTC seconds values. */
     public void setTimeSpan(TimeSpan ts) {
         startChooser.setTrueSeconds(ts.getStart()); // for UTC -aww 2008/02/13
         endChooser.setTrueSeconds(ts.getEnd()); // for UTC -aww 2008/02/13
     }


     /** Return the time span selected in this chooser. */
     public TimeSpan getTimeSpan() {
         return new TimeSpan(startChooser.getTrueSeconds(), endChooser.getTrueSeconds()); // used to nominal, changed to UTC -aww 2008/02/07
     }


     void startDeltaButton_actionPerformed(ActionEvent e) {
          DeltaTimeDialog startDialog = new DeltaTimeDialog(null, "Time before end time", true);
          startDialog.deltaPanel.setUnits(defaultUnits);
          startDialog.deltaPanel.setValue(defaultValue);
          startDialog.setVisible(true);

          if ( startDialog.getButtonStatus() == JOptionPane.OK_OPTION ) {
             double endTime = endChooser.getTrueSeconds(); // used to be nominal, changed to UTC -aww 2008/02/07
             double time    = endTime - startDialog.getSeconds();
               // make sure its a legal time
             if (time < endTime) {   // OK
                 startChooser.setTrueSeconds(time); // for UTC -aww 2008/02/13
             } else {
                 JOptionPane.showMessageDialog(null,
				      "Illegal time span.", "Date Range Error",
				      JOptionPane.PLAIN_MESSAGE);
              }
          }
     }

     void endDeltaButton_actionPerformed(ActionEvent e) {
          DeltaTimeDialog endDialog = new DeltaTimeDialog(null, "Time after start time", true);
          endDialog.deltaPanel.setUnits(defaultUnits);
          endDialog.deltaPanel.setValue(defaultValue);
          endDialog.setVisible(true);

          if ( endDialog.getButtonStatus() == JOptionPane.OK_OPTION ) {

              double startTime = startChooser.getTrueSeconds(); // used to nominal, changed to UTC -aww 2008/02/07
              double time      = startTime + endDialog.getSeconds();
              // make sure its a legal time
              if (time > startTime) {   // OK
                endChooser.setTrueSeconds(time); // for UTC -aww 2008/02/13
              } else {
                 JOptionPane.showMessageDialog(null,
				      "Illegal time span.", "Date Range Error",
				      JOptionPane.PLAIN_MESSAGE);
              }

          }
     }

     void nowButton_actionPerformed(ActionEvent e) {
          double now = (new DateTime()).getTrueSeconds(); // used to nominal, changed to UTC -aww 2008/02/07
          endChooser.setTrueSeconds(now); // for UTC -aww 2008/02/13
     }
     
/*
     public static void main(String[] args) {
        JFrame frame = new JFrame("DateRangeChooser");

        frame.addWindowListener(new WindowAdapter()
	   {
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });

        frame.getContentPane().add(new DateRangeChooser("Alpha", "Omega"));

        frame.pack();
        frame.setVisible(true);
     }
*/
} 
