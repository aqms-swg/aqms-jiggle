package org.trinet.pcs;

import java.io.*;
import java.sql.*;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jdbc.datasources.DbaseConnectionDescription;
import org.trinet.util.*;

/** Utilizes Oracle database stored package CODE.PCS. */
public abstract class AbstractProcessingController implements AutoCloseable, ProcessingControllerIF {

    // Concrete subclass should set specific value when instantiated by constructor: 
    protected String appName = "unknown";

    // properties to configure the concrete instance
    protected PcsPropertyList props = null;
    protected PrintStream logStream = null;
    protected String logFileName = null;
    protected File logFile = null;
    protected long logRotationInterval = Long.MAX_VALUE; // default is no rotation
    protected long lastLogRotationMilliSecs = Long.MAX_VALUE;
    protected long myTimeMilliSecs = 0l;
    private long startTimeMilliSecs = 0l;

    private int logCount = 0;
    private int logFailures = 0;

    protected boolean autoProcessing = true;

    // made defaults static unless subclass needs instance level settings
    protected static String defaultStateName  = "defaultState";
    protected static String defaultThreadName = "defaultThread";
    protected static String defaultGroupName  = "defaultGroup";

    //static final String PCS_GET_ALL_IDS_FOR_PROCESSING_FUNCTION;
    // below input group,thread,state
    //public static final String PCS_GET_NEXT_ID_FOR_PROCESSING_FUNCTION   = "{?=call PCS.getNext(?,?,?,?)}";
    public static final String PCS_GET_NEXT_ID_FOR_PROCESSING_FUNCTION   = "{?=call PCS.next_id(?,?,?,?)}";

    // below input group,thread,id,state,result, call does state transitions in lieu of trigger
    //public static final String PCS_RESULT_ID_PROCESSING_FUNCTION         = "{?=call PCS.putResult(?,?,?,?,?,?)}"; // new with rank in key
    public static final String PCS_RESULT_ID_PROCESSING_FUNCTION         = "{?=call PCS.putResult(?,?,?,?,?)}"; // old without rank in key
    // NOTE: result_id call below doesn't do state transitions:
    //public static final String PCS_RESULT_ID_PROCESSING_FUNCTION         = "{?=call PCS.result_id(?,?,?,?,?)}";

    // below input group,thread,id,state,rank
    //public static final String PCS_POST_ID_FOR_PROCESSING_FUNCTION       = "{?=call PCS.putState(?,?,?,?,?)}";
    // Overloaded post_id, last arg value = 1, commit row posting 
    //public static final String PCS_POST_ID_FOR_PROCESSING_FUNCTION       = "{?=call PCS.post_id(?,?,?,?,?, 1)}";
    public static final String PCS_POST_ID_FOR_PROCESSING_FUNCTION       = "{?=call PCS.post_id(?,?,?,?,?)}";

    // below input group,thread,id,state,rank,result value can be non-zero
    //public static final String PCS_POST_RESULT_OF_PROCESSING_FUNCTION = "{?=call PCS.putState(?,?,?,?,?,?)}";

    // below input: group,thread,state,appName,host,dbname, and username; return sigid
    public static final String PCS_SIGNAL_START_FUNCTION       = "{?=call PCS.signal_start(?,?,?,?,?,?)}";

    // below input sigid; return 0 false, 1 true
    public static final String PCS_SIGNAL_STOP_FUNCTION        = "{?=call PCS.signal_stop(?,?)}";

    // below input sigid, message; return 0 false, 1 true
    public static final String PCS_SIGNAL_MESSAGE_FUNCTION     = "{?=call PCS.signal_message(?,?)}";

    // below input sigid; return 0 false, 1 true
    public static final String PCS_SIGNAL_HAS_STOP_FUNCTION    = "{?=call PCS.has_stop_signal(?)}";

    public static final String PCS_BULK_POST    = "{?=call PCS.bulk_post(?,?,?,?,?)}";

    public static final int DEFAULT_THREAD_SLEEP_MILLIS = 100;
    public static final int MAX_REPOSTS = 10;

    // Convenience flags for use by subclass implementations of list processing 
    protected boolean listExclusiveProcessing = false; // true => do not process ids not found in input list
    protected boolean forceStateProcessing = true; // true => only posted ids processed
                                                   // false => bypass process posted requirement
                                                   // (e.g. process an evid obtained from an input Solution method). 

    /** Run time mode =0, a one-shot, else runs continuously sleeping for specified sleep seconds and before requestig next id to process */
    protected int runMode = 0;

    protected boolean debug = false;
    protected boolean verbose = false;

    /** Set <i>true</i> multiple id processing fails upon first id processing error,
    * otherwise the error state resulted for id and loop processing continues.
    * Default is <i>true</i>.
    */
    protected boolean fastFailLoops = true;

    /** Sleep time to wait, possibly allow other threads time. */
    protected int defaultSleepTimeMillis = DEFAULT_THREAD_SLEEP_MILLIS;

    /** Arbitrary default lddate lag seconds for next posted id, default lag is 0 seconds, subclass can override. */
    protected int defaultNextLagSecs = 0;

    /** Arbitrary default rank for posting, default is 100, subclass can override. */
    protected int defaultPostingRank = 100;

    /** Current state description of processing instance. */
    protected ProcessControlIdentifier myPCID = null;

    /** Unique id for row in PCS_SIGNAL table (processing run identifier). */
    protected long sigId = 0l;

    // Global Counters for total ids processed
    protected int count = 0;
    protected int failures = 0;

    // Database JDBC connection
    protected Connection connection;

    // Local created (=true) or set to already existing (=false)
    protected boolean instanceDataSource = true;

    // Below statements are initialized when first invoked by methods
    protected CallableStatement pcsGetNextIdForProcessingStmt;
    protected CallableStatement pcsResultIdProcessingStmt;
    protected CallableStatement pcsPostIdForProcessingStmt;
    protected CallableStatement pcsBulkPostIdsForProcessingStmt;

    protected CallableStatement pcsSignalStartStmt;
    protected CallableStatement pcsSignalStopStmt;
    protected CallableStatement pcsSignalMessageStmt;
    protected CallableStatement pcsStopSignalledStmt;

    protected PreparedStatement pcsGetPostIdsForDateRangeProcessingStmt;
    protected PreparedStatement pcsCheckIdTableStmt;

    // configure prepared statement SQL Strings below in a concrete subclass
    protected String pcsCheckIdSQLString;                 // a table specific query, usually Event
    protected String pcsGetPostIdsForDateRangeSQLString;  // a table specific query, usually Event

    protected AbstractProcessingController () {}

    protected AbstractProcessingController(String appName, String groupName, String threadName, String stateName) {
        this(appName, groupName, threadName, stateName, DEFAULT_THREAD_SLEEP_MILLIS);
    }

    protected AbstractProcessingController(String appName, String groupName, String threadName, String stateName,
                                           int defaultSleepTimeMillis) {
        this.appName = appName;
        setProcessControlIdentifier(groupName, threadName, stateName);
        setDefaultSleepTimeMillis(defaultSleepTimeMillis);
    }

    public String getAppName() {
        return appName;
    }

    /** Sets instance properties, read specified user property files
     * and configures environment. If PcsPropertyList is null will attempt to 
     * open the file called <app-name>.props (all lowercase) in the default user home path
     * defined by Java System properties <app-name>_USER_HOMEDIR (all uppercase) or user.home,
     * then initializes from properties. */
    public boolean setProperties(PcsPropertyList newProps) {

        if (newProps == null) {
            newProps = new PcsPropertyList(); // do one with most options 
            newProps.setFiletype(appName.toLowerCase()); // like "hypomag"
            newProps.setFilename(appName.toLowerCase()+".props"); // like "hypomag.props" 
        }

        props =  newProps;

        props.reset(); // read user property files

        return (props.setup() && initFromProperties()); // initialize settings
    }

    /** Sets instance properties, read specified input user property file,
     * if input String is null, does setProperties(PcsPropertyList) with a null argument,
     * then initializes from properties. */
    public boolean setProperties(String propFileName) {

        if (propFileName == null) return setProperties((PcsPropertyList) null); 

        props = new PcsPropertyList();

        if ((new File(propFileName)).isAbsolute()) {
            //set below filetype for path to use for relative files if looked up via app_name_USER_HOMEDIR
            props.setFiletype(appName.toLowerCase()); // ?
            props.setUserPropertiesFileName(propFileName);
            return setProperties(props); 
        }

        // Else assume property file is relative to path defined by user home dir system property
        props.initialize(appName, propFileName, null); // does reset(): reads file, and then setup()

        return initFromProperties();
    }

    protected boolean initFromProperties() {

        if (props == null) return false;

        boolean status = true;

        // Do this here (when leap seconds are read from db) before initLogging if DateTime creates timestamp using LeapSeconds for string 
        // LeapSeconds static arrays init needs a db connection
        setupDataSourceFromProperties();

        initLogging();

        System.out.println("Java Build Version: " + Version.buildTag);

        debug = props.getProperty("debug", "false").equalsIgnoreCase("true") || Debug.isEnabled();
        if (debug) System.out.println("DEBUG: " + getClass().getName() + " appName=" + appName);
        verbose = debug || props.getBoolean("verbose");
        if (verbose) {
            props.dumpProperties();

            if (debug) {

              List aList = props.getKnownPropertyNames();
              System.out.println("DEBUG: property names KNOWN:\n" + aList);
              System.out.println(Lines.ANGLE_LR_TEXT_LINE);

              aList = props.getUnknownPropertyNames();
              if (aList.size() > 0) {
                System.out.println("INFO: property names UNRECOGNIZED:\n" + aList);
                System.out.println(Lines.ANGLE_LR_TEXT_LINE);
              }

              aList = props.getUndefinedPropertyNames();
              if (aList.size() > 0) {
                System.out.println("INFO: property names UNDEFINED:\n" + aList);
                System.out.println(Lines.ANGLE_RL_TEXT_LINE);
              }
            }
        }

        if (debug) {
          // for debug return completeString from toDelimitedNameString() -aww
          ChannelName.useFullName = true;
          System.out.println("INFO: " + getClass().getName() + " property file: " +
                props.getUserPropertiesFileName());
          //props.list(System.out); // see above verbose dump
        }
        // override default group
        if (myPCID == null)
            myPCID = new ProcessControlIdentifier(getDefaultGroupName(), getDefaultThreadName(), getDefaultStateName());
        if (props.isSpecified("pcsGroupName"))
          myPCID.setGroupName(props.getProperty("pcsGroupName"));
        // override default thread
        if (props.isSpecified("pcsThreadName"))
          myPCID.setThreadName(props.getProperty("pcsThreadName"));
        // override default processing state
        if (props.isSpecified("pcsStateName"))
          myPCID.setStateName(props.getProperty("pcsStateName"));
        // override default thread sleep
        if (props.isSpecified("pcsSleepMilliSecs")) 
          defaultSleepTimeMillis = (int) props.getDouble("pcsSleepMilliSecs", 0.); // deprecate?
        else
          defaultSleepTimeMillis = (int) (1000.*props.getDouble("pcsSleepSecs", 0.)); // replace above?

        if (defaultSleepTimeMillis < DEFAULT_THREAD_SLEEP_MILLIS) {
            defaultSleepTimeMillis = DEFAULT_THREAD_SLEEP_MILLIS;
        }

        if (props.isSpecified("pcsRunMode"))
            runMode = props.getInt("pcsRunMode");

        if (props.isSpecified("pcsListExclusive"))
            listExclusiveProcessing = props.getBoolean("pcsListExclusive");
        if (props.isSpecified("pcsForceStateProcessing"))
            forceStateProcessing = props.getBoolean("pcsForceStateProcessing");
        if (props.isSpecified("pcsFastFailLoops"))
            fastFailLoops = props.getBoolean("pcsFastFailLoops");
        if (props.isSpecified("pcsDefaultPostingRank"))
            defaultPostingRank = props.getInt("pcsDefaultPostingRank");
        if (props.isSpecified("pcsDefaultNextLagSecs"))
            defaultNextLagSecs = props.getInt("pcsDefaultNextLagSecs");

        if (props.isSpecified("pcsAutomatic")) autoProcessing = props.getBoolean("pcsAutomatic");

        // Check the jasi object type to be used for the run
        if (props.getProperty("jasiObjectType") != null ) {
          if (!JasiObject.setDefault(props.getProperty("jasiObjectType"))) {
            System.err.println("ERROR: " + getClass().getName() +
                  " default jasi object type undefined or invalid.");
            status = false;
          }
        }

        initEnvironmentInfo();

        if (verbose) {
            // dump environment in case it was reset above or props.setup() 
            System.out.println(">>>  EnvironmentInfo settings <<<");
            System.out.println(EnvironmentInfo.getString());
            System.out.println();
        }

        return status;
    }

    /** Initialize EnvironmentInfo with the controller's name and
     * the current setting of its "automatic" processing flag.
     * Default are "unknown" and "automatic".
     * */
    protected void initEnvironmentInfo() {
        EnvironmentInfo.setApplicationName(appName);
        EnvironmentInfo.setAutomatic(autoProcessing);
    }

    /**
     * Logging can be controlled with properties:
     * 
     * pcsLogging,
     * pcsLogFilePath,
     * pcsLogFileName,
     * pcsLogRotationInterval.
     * 
     * The resulting log file will be a concatenation of pcsLogFilePath+pcsLogFileName+datetime string
     * with the file suffix ".log".
     * pcsLogFilePath defaults to "./" (the local dir)
     * pcsLogFileName defaults to a string based on the application name and date
     * 
     * If filename is specified but not filepath the filename will be used verbatim, 
     * so it can include a path as a prefix, the date time stamp is appended.
     * Example:  "./THIS_PROC_2007-04-12_032743"
     */
    protected boolean initLogging() {

        String logFilePath = "." + GenericPropertyList.FILE_SEP;  // "./" or ".\"

        if ( props.isSpecified("pcsLogFilePath") ) {
            // tack on ending "/" for safety, its OK if there are two
            logFilePath = props.getUserFileNameFromProperty("pcsLogFilePath")+ GenericPropertyList.FILE_SEP; 
        }

        // if filename but not path is specified, include the "./" default path
        StringBuffer sb = new StringBuffer(132);
        if ( props.isSpecified("pcsLogFileName") ) {
            if (props.isSpecified("pcsLogFilePath")) { // use the path
               sb.append(logFilePath+GenericPropertyList.FILE_SEP+props.getProperty("pcsLogFileName"));
            }
            else { // use the default path constructed from app name / user system properties
               sb.append(props.getUserFileNameFromProperty("pcsLogFileName"));
            }

            // If logging && rotating logs append a UTC timestamp to end of filename like "./appName.log_2008-04-04_163139"
            //if (props.getBoolean("pcsLogging") && props.isSpecified("pcsLogRotationInterval") ) {
            //     sb.append("_");
            //     sb.append(new DateTime().toDateString("yyyy-MM-dd_HHmmss"));
            //}
        }
        else {
            sb.append(logFilePath);
            sb.append(appName);
            sb.append("_");
            sb.append(new DateTime().toDateString("yyyy-MM-dd_HHmmss")); // UTC time? - aww 2008/02/08
            sb.append(".log");
        }

        String filename = sb.toString();
        
        if ( props.isSpecified("pcsLogRotationInterval") ) {
            setLogRotationInterval(props.getProperty("pcsLogRotationInterval"));
        }

        if ( !props.getBoolean("pcsLogging") ) {
            System.out.println("INFO: No logging to file; pcsLogging is OFF");
        }
        else if (filename != null) {
            initLogStream(filename); // output goes to log file now
        }

        return true;
    }

    protected void initLogStream(String logFileName) {
 
        closeLog(); // close existing stream
        if (logFileName == null) return; // no file specified

        try {
          this.logFileName = logFileName; // save current stream name
          System.out.println("INFO: Logging output to file: " + logFileName);
          System.out.println("INFO  Log rotation interval: " + props.getProperty("pcsLogRotationInterval", "NONE"));

          // If logging && rotating logs append a UTC timestamp to end of filename like "./appName.log_2008-04-04_163139"
          logFile = new File(logFileName);
          if (logFile.exists() && logFile.isFile()) {
              logFile.renameTo(new File(logFileName+"_"+(new DateTime().toDateString("yyyy-MM-dd_HHmmss"))));
              logFile = new File(logFileName);
          }
          logStream = new PrintStream(new FileOutputStream(logFile));
          System.setOut(logStream);
          System.setErr(logStream);
          DateTime dt = new DateTime(); // for UTC -aww 2008/02/08
          lastLogRotationMilliSecs = Math.round(dt.getNominalSeconds()*1000.);
          System.out.println("INFO: Started logging at : " + dt);
        }
        catch(IOException ex) {
          ex.printStackTrace();
        }
        catch(SecurityException ex) {
          ex.printStackTrace();
        }
    }

    protected void closeLog() {
        if (logStream != null) {
            System.out.println("INFO: Closing output to log file: " + logFileName + 
              " at : " + new DateTime().toString()); // for UTC -aww 2008/02/08
            logStream.close(); // flushes
        }
    }

    /** In continuous run mode, open new log file after each log rotation interval. */ 
    public void rotateLogFile() {
      if (logStream == null) {
          System.out.println("** No logging stream open for " + logFileName + " at "+ EpochTime.dateToString(new java.util.Date()) );
      }
      else {
          logStream.println("** Rotating log file at "+ EpochTime.dateToString(new java.util.Date()) ); // date at GMT
      }

      initLogging();

      if (verbose) {
          props.dumpProperties();

          System.out.println(">>>  EnvironmentInfo settings <<<");
          System.out.println(EnvironmentInfo.getString());
          System.out.println();

          System.out.println(">>>  Processing settings <<<");
          System.out.println(getProcessingAttributesString());
          System.out.println();
      }
    }

    /** Set rotation interval using a string that describes the units of the interval.
     * For example: "1200s" = 1200 seconds, "1d" = 1 day. A space between number & letter
     * is allowed: e.g. "3600 s". Case does not matter: "3600 s" = "3600 S".
     * Allowed qualifiers are: S = seconds, M = minutes, H = hours, D = days, W = weeks.
     * If there is no qualifier, S for seconds is assumed.
     * An invalid string parse will result in an error message and NO log rotation. */
    private void setLogRotationInterval(String instr) {

      String  str = instr.trim();  // trim whitespace

      int seconds = 0;
      int numberLen = str.length();

      char lastChar = str.charAt(numberLen-1);

      if (Character.isLetter(lastChar)) {
        numberLen--;
        lastChar = Character.toUpperCase(lastChar);
      } else {
        lastChar = 'S';   // default
      }

      try {
        // get the number part of the string - trim() allows space between number & letter
        seconds = Integer.parseInt(str.substring(0,numberLen).trim());
      }
      catch (NumberFormatException ex) {
        System.err.println ("Pcs Property Error: Bad log rotation interval: "+ str);
        return;
      }

      if (lastChar == 'S') {
        // noop
      } else if (lastChar == 'M') {
        seconds *= 60;
      } else if (lastChar == 'H') {
        seconds *= 60 * 60;
      } else if (lastChar == 'D') {
        seconds *= 60 * 60 * 24;
      } else if (lastChar == 'W') {
        seconds *= 60 * 60 * 24 * 7;
      } else {
        System.err.println("Pcs Property Error: Unknown log rotation interval units: "+ str + " defaulting to: " + logRotationInterval + " seconds");
        return;
      }

      logRotationInterval = (long) seconds;

    }

    public void setAutomatic(boolean tf) {
        autoProcessing = tf;
    }

    public final void setVerbose(boolean tf) {
        verbose = tf;
    }
    public final void setDebug(boolean tf) {
        debug = tf;
        if (debug) verbose = true;
    }

    /** Sudden death of id loop processing upon error. */
    public final void setFastFailLoopProcessing(boolean tf) {
        fastFailLoops = tf;
    }

    /** Default rank number for posting. */
    public int getDefaultPostingRank() {
        return defaultPostingRank;
    }

    public void setDefaultPostingRank(int postRank) {
        if (myPCID != null) myPCID.setRank(postRank);
        defaultPostingRank = postRank;
    }

    /** Default seconds lag behind SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) of next posting. */
    public int getDefaultNextLagSecs() {
        return defaultNextLagSecs;
    }

    public void setDefaultNextLagSecs(int lagSecs) {
        defaultNextLagSecs = lagSecs;
    }

    public final void setDefaultSleepTimeMillis(int millisecs) {
        defaultSleepTimeMillis = millisecs;
    }

    /** Force current thread to sleep for default millis. */
    public void sleep() {
        sleep(defaultSleepTimeMillis);
    }

    /** Force current thread to sleep for specified millis. */
    public void sleep(int millisecs) {
        try {
            Thread.currentThread().sleep(millisecs);
        }
        catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    /** Return JDBC connection object to be used by this processing thread. */
    public final Connection getConnection() {
        return connection;
    }

    /** Set JDBC connection object to be used by this processing thread. */
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    /** Subclass implementation must define its defaults or initialize by properties. */
    public String getDefaultGroupName() {
        return defaultGroupName;
    }
    public String getDefaultStateName() {
        return defaultStateName;
    }
    public String getDefaultThreadName() {
        return defaultThreadName;
    }
    public void setDefaultGroupName(String name) {
        defaultGroupName = name;
    }
    public void setDefaultStateName(String name) {
        defaultStateName = name;
    }
    public void setDefaultThreadName(String name) {
        defaultThreadName = name;
    }

    public String getGroupName() {
        return (myPCID == null) ? null : myPCID.getGroupName();
    }
    public void setGroupName(String groupName) {
        if (myPCID == null) myPCID = new ProcessControlIdentifier();
        myPCID.setGroupName(groupName);
    }

    public String getThreadName() {
        return (myPCID == null) ? null : myPCID.getThreadName();
    }
    public void setThreadName(String threadName) {
        if (myPCID == null) myPCID = new ProcessControlIdentifier();
        myPCID.setThreadName(threadName);
    }

    public String getStateName() {
        return (myPCID == null) ? null : myPCID.getStateName();
    }
    public void setStateName(String stateName) {
        if (myPCID == null) myPCID = new ProcessControlIdentifier();
        myPCID.setStateName(stateName);
    }

    public ProcessControlIdentifier getProcessControlIdentifier() {
         return myPCID;
    }
    public void setProcessControlIdentifier(ProcessControlIdentifier pcId) {
         myPCID = pcId;
    }
    public void setProcessControlIdentifier(String groupName, String threadName, String stateName) {
        setProcessControlIdentifier(groupName, threadName, stateName, getDefaultPostingRank());
    }
    public void setProcessControlIdentifier(String groupName, String threadName, String stateName, int rank) {
        if (groupName == null) groupName = getDefaultGroupName();
        if (threadName == null) threadName = getDefaultThreadName();
        if (stateName == null) stateName = getDefaultStateName();
        if (myPCID == null) myPCID = new ProcessControlIdentifier();
        myPCID.setProcessControlNames(groupName, threadName, stateName);
        myPCID.setRank(rank);
    }

    /** Implementation here returns <i>false</i> for all ids &lt= 0, and <i>true</i>
     * otherwise unless concrete subclass assigns a String in JDBC PreparedStatement
     * format to the instance data member pcsCheckIdSQLString.
     * SQL String must have the input id test for in its WHERE condition and
     * return a long id value upon success.<br>
     * <pre> e.g. "SELECT EVID FROM EVENT WHERE EVID=?").</pre>
     * */
    public boolean isValidId(long id) {

        if (id < 1l) return false; // don't allow 0 for id 10/12/05 -aww

        // by default assume any id is valid unless subclass defines query
        if (pcsCheckIdSQLString == null) return true; // 10/12/05 -aww


        boolean status = false;
        ResultSet rs = null;
        try {
            if (pcsCheckIdTableStmt == null)
                pcsCheckIdTableStmt = connection.prepareStatement(pcsCheckIdSQLString);
            pcsCheckIdTableStmt.setLong(1, id);
            rs = pcsCheckIdTableStmt.executeQuery();
            if (rs.next()) {
               status = ( rs.getLong(1) > 0 && ! rs.wasNull() );
            }
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
          try {
            if (rs != null) rs.close();
          } catch (SQLException e) {}
        }
        if (!status) System.err.println("ERROR ProcessingController isValidId unable to find id: " + id + " in table.");
        return status;
    }

    /** Parse List of ids to be processed from the input filename. Input file format is one numeric id per line. */
    public List getIdListToProcess(String idFileName) {
        System.out.println("INFO: Input id(s) to process read from file: " + idFileName);
        ArrayList idList = new ArrayList();
        int count = 0;
        BufferedReader idStream = null;
        try {
            idStream = new BufferedReader(new FileReader(idFileName));
            String idString = null;
            while (true) {
                idString = idStream.readLine();
                if (idString == null) {
                    break;
                }
                count++; // line counter
                idList.add(Long.valueOf(idString.trim()));
            }
        }
        // upon error do what behavior? return the list, a null, or throw an exception?
        catch (NumberFormatException ex) {
            ex.printStackTrace();
            System.err.println("Error: Number format in input file: " + idFileName + " at line: " + count);
        }
        catch (FileNotFoundException ex) {
            ex.printStackTrace();
            System.err.println("Error: Input file not found: " + idFileName);
        }
        catch (IOException ex) {
            ex.printStackTrace();
            System.err.println("Error: i/o for input file: " + idFileName + " at line: " + count);
        }
        finally {
            try {
              if (idStream != null) idStream.close();
            }
            catch (IOException ex2) { }
        }
        return idList;
    }

    /** Subclass implements the processing of the id. */
    abstract public int processId(long id);

    /** Lookup the next id posted in PCS state table for state of interest to subclass implementation and process it.
     * Returns positive value for success, negative value for error.
     * */
    public int processNextId() {
        long id = getNextId();
        return (id > 0l) ? processId(id) : 0;
    }

    /** Process all ids already posted in the PCS state table for the state of interest 
     * to subclass implementation. 
     * Returns positive value for success, negative value for error.
     * */
    public int processIds() {
        return doProcessing(null);
    }

    /** Process ids already posted for the state of interest to subclass 
     * implementation that are found in input List.
     * If listExclusiveProcessing is set "true" and input is not null, 
     * process only posted ids that are in input List.
     * Returns positive value for success, negative value for error.
     * */
    protected int doProcessing(List idList) {

        startTimeMilliSecs = System.currentTimeMillis(); // start time of invocation

        // for run, until stopped
        count = 0;
        failures = 0;

        // per log if rotated, resets
        logCount = 0;
        logFailures = 0;

        int istat = 0;
        boolean error = false;
        long oldId = -1l;
        int repost = 0;

        // Does wait if mode not one-shot (runMode!=0)
        long id = doGetNext(); // returns negative value on error

        while (id > 0l) {
            // Note if db connection dies, no "auto" reset: cleanup (of log, signal table entry)
            // followed by reconnection (new log signal entry)
            // thus, after any SQL failure the app exits

            if ( hasStopSignal() ) break; // done

            if (idList != null) { // check for exclusive list membership processing
              if ( listExclusiveProcessing && ! idList.contains(Long.valueOf(id)) ) {
                  if (verbose) System.out.println("INFO: Posted id not in input list, skipping id: " + id); 
                  continue;
              }
            }

            // log current time for processing this event 
            myTimeMilliSecs = System.currentTimeMillis();
            
            // Where real processing happens
            istat = processId(id);
            
            if (istat > 0) { // bump if success
                count++;
                logCount++;
            }
            else if (istat <= 0) { // error, failed
              error = true;
              failures++;
              logFailures++;

              if (fastFailLoops) break;
              else { 
                // If state processing then sudden death here since upon result failure
                // must exit to avoid infinite loop getNext - aww 05/18/2005
                if (istat == ProcessingResult.ID_RESULT_FAILURE.getIdCode() ) {
                  if (getNextId() == id) {
                      System.err.println("ERROR " + getClass().getName() +
                        ".doProcessing(List) unable to result id : " + id +
                        " terminating loop istat: " + istat);
                      break;  // have to break to avoid infinite loop getNextId() - aww 05/18/2005 
                  }
                }
              }
            }


            // Break if same id as b4 as insurance to avoid infinite loop,
            // if case above PCS result update to state table failed 
            if ( id == oldId ) {
              repost++; // bump duplicate processing counter

              if (runMode == 0) {
                System.out.println("ERROR last processed id identical, Exiting One-shot loop with id: " + id);
                break; 
              }
              // In continous mode, allow MAX_REPOSTS, then bail
              else if (repost < MAX_REPOSTS) {
                System.out.println("INFO: Last processed Continuous loop id identical: " + id);
              }
              else {
                System.out.println("ERROR exceeded maximum repeat posting count of: "+ MAX_REPOSTS);
                System.out.println("ERROR last processed id identical, Exiting Continuous loop with id: " + id);
                break; 
              }
            }
            // reset counter, ids not identical
            else repost = 0;

            oldId = id; // valid id processed

            if (debug) {
              System.out.println(
                   "DEBUG: Event processing time: " +
                   EpochTime.elapsedTimeToText((int) (System.currentTimeMillis() - myTimeMilliSecs)/1000)
              );
            }

            id = doGetNext();

        }

        if (verbose) {
          System.out.println(
              "INFO: Log processing time: " +
              EpochTime.elapsedTimeToText((int) (System.currentTimeMillis() - startTimeMilliSecs)/1000)
              + " log id counts for success : " + logCount + " failed: " + logFailures
          );
          System.out.println(
            "INFO: Total processing time: " +
            EpochTime.elapsedTimeToText((int) (System.currentTimeMillis() - startTimeMilliSecs)/1000)
            + " total id counts for success : " + count + " failed: " + failures
          );
        }

        return (error && fastFailLoops) ? istat : count;
    }

    private long doGetNext() {

        long id = getNextId(); // returns negative value on error (to break out of continuous loop)

        while (runMode != 0 && id == 0l) { // one-shot mode = 0, else continuous loop

            if ( hasStopSignal() ) {
                System.out.println("Received Stop Signal, Exiting Continuous Loop with id: " + id);
                break; // done
            }

            sleep();  // hibernate and wait for next post
            id = getNextId();

            // if not one-shot mode, check elapsed time log file rotation
            // log current time 
            myTimeMilliSecs = System.currentTimeMillis();

            if ( runMode != 0 && ((myTimeMilliSecs - lastLogRotationMilliSecs)/1000l >= logRotationInterval) ) {
                if (verbose) {
                  System.out.println(
                    "INFO: Log processing time: " +
                    EpochTime.elapsedTimeToText((int) (System.currentTimeMillis() - startTimeMilliSecs)/1000)
                    + " log id counts for success : " + logCount + " failed: " + logFailures
                  );
                }
                rotateLogFile(); 
                logCount = 0;
                logFailures = 0;
            }
            else if ( id == 0l && logFile != null ) {  // update timestamp as proof of looping -aww 2012/10/10
                logFile.setLastModified(myTimeMilliSecs);
            }


        }
        return id;
    }

    /** Post all ids in input List for currently set state description at default rank, then process them.
     * Returns positive value for success, negative value for error.
     * */
    public int processIds(List idList) {
        return processIds(idList, getDefaultPostingRank());
    }

    /** Post all ids in input List for currently set state description at the input rank, then process them. 
     * Returns positive value for success, negative value for error.
     */
    public int processIds(List idList, int rank) { // list of Longs
        int istat = postIds(idList, rank);
        // Note: below does all "postings" inclusively in table, if listExclusiveProcessing == false
        return (istat > 0) ? doProcessing(idList) : istat;
    }

    /** Return next id posted in state table for current state description.
     * Returns 0 if none.
     * */
    public long getNextId() {
       if (myPCID == null) throw new NullPointerException("Must initialize ProcessControlIdentifier names.");
       return getNextId(myPCID.getGroupName(), myPCID.getThreadName(), myPCID.getStateName());
    }
    /** Return next id posted in state table for input state description.
     * Returns 0 if none.
     */ 
    public long getNextId(ProcessControlIdentifier pcId) {
        return getNextId(pcId.getGroupName(), pcId.getThreadName(), pcId.getStateName());
    }
    /** Return next id posted in state table for input state description.
     * Returns 0 if none.
     * */
    public long getNextId(String groupName, String threadName, String stateName) {

        // If continuous run mode check for a valid connection, if none do a reconnect if possible
        try {
            if (runMode == 0) {
                if (connection == null || connection.isClosed()) {
                    // ? Could "sleep" for some seconds before trying to reconnect ?
                    if (!reconnect()) return ProcessingResult.DB_NO_CONNECTION.getIdCode(); // negative number returned
                }
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return ProcessingResult.JAVA_EXCEPTION.getIdCode(); // negative number returned
        }

        long id = 0l;
        try {
            // "{?=call PCS.getNext(?,?,?,?)}";
            if (pcsGetNextIdForProcessingStmt == null) {
                pcsGetNextIdForProcessingStmt = connection.prepareCall(PCS_GET_NEXT_ID_FOR_PROCESSING_FUNCTION);
            }
            pcsGetNextIdForProcessingStmt.registerOutParameter(1, java.sql.Types.BIGINT);
            pcsGetNextIdForProcessingStmt.setString(2, groupName);
            pcsGetNextIdForProcessingStmt.setString(3, threadName);
            pcsGetNextIdForProcessingStmt.setString(4, stateName);
            pcsGetNextIdForProcessingStmt.setInt(5, defaultNextLagSecs);
            pcsGetNextIdForProcessingStmt.execute();
            id = pcsGetNextIdForProcessingStmt.getLong(1);
        }
        catch (SQLException ex) {
        //catch (Exception ex) {
            ex.printStackTrace();
            id = ProcessingResult.DB_SQL_EXCEPTION.getIdCode();
        }
        return id;
    }

    protected boolean reconnect() {
        // NOTE: SolutionLockTN getConnection does this too
        Connection conn = DataSource.getNewConnect(); // internal if installed on server
        if (conn == null) { // failed
            System.err.println("ERROR " + getClass().getName() + " reconnect() connection returned null");
            return false;
        }
        DataSource.set(conn); // reset Datasource connection, local or not
        nullJdbcStatements(); // to reset existing statements
        setConnection(conn);
        return true;
    }

    /** Returns true if rank >= 0. */
    public boolean isValidRank(int rank) { return (rank > -1) ; }

    /** Post the input id at the default posting rank for the currently set state processing description.
     * Returns positive value for success, negative value for error.
     * */
    public int postId(long id) {
        return postId(id, myPCID.getGroupName(), myPCID.getThreadName(), myPCID.getStateName(), myPCID.getRank());
    }
    /** Post the input id at the input rank for the currently set state processing description.
     * Sets the default posting rank to the input rank when different.
     * Returns positive value for success, negative value for error.
     * */
    public int postId(long id, int rank) {
        return postId(id, myPCID.getGroupName(), myPCID.getThreadName(), myPCID.getStateName(), rank);
    }
    /** Post the input id at the input rank for the input set state processing description.
     * Sets the default posting rank to the input rank when different.
     * Returns positive value for success, negative value for error.
     * */
    public int postId(long id, String groupName, String threadName, String stateName, int postingRank) {
        if (! isValidId(id))
             return ProcessingResult.INVALID_ID.getIdCode();
        if ( ! isValidRank(postingRank))
             return ProcessingResult.INVALID_POSTING_RANK.getIdCode();

        if (getDefaultPostingRank() != postingRank) setDefaultPostingRank(postingRank);

        int istat = ProcessingResult.UNIT_SUCCESS.getIdCode();

        try {
            // "{call PCS.putState(?,?,?,?,?)}";
            if (pcsPostIdForProcessingStmt == null) {
                pcsPostIdForProcessingStmt = connection.prepareCall(PCS_POST_ID_FOR_PROCESSING_FUNCTION);
            }
            pcsPostIdForProcessingStmt.registerOutParameter(1, java.sql.Types.INTEGER);
            pcsPostIdForProcessingStmt.setString(2, groupName);
            pcsPostIdForProcessingStmt.setString(3, threadName);
            pcsPostIdForProcessingStmt.setLong(4, id);
            pcsPostIdForProcessingStmt.setString(5, stateName);
            pcsPostIdForProcessingStmt.setInt(6, postingRank);
            pcsPostIdForProcessingStmt.execute();
            istat = pcsPostIdForProcessingStmt.getInt(1);
            //
            //istat = ProcessControl.putState(groupName,threadName,id,stateName,postingRank);
            //
            try {
                if (istat > 0l) {
                    connection.commit();
                }
            }
            catch (SQLException ex) {
                ex.printStackTrace();
                istat = ProcessingResult.DB_COMMIT_FAILURE.getIdCode();
            }
        }
        catch (SQLException ex) {
        //catch (Exception ex) {
            ex.printStackTrace();
            istat = ProcessingResult.DB_SQL_EXCEPTION.getIdCode();
        }
        return istat;
    }

    /** Use bulk SQL to post input List of ids to current state description at default rank
     * (more efficient than invoking SQL once per id), then process postings.
     * Returns positive value for success, negative value for error.
     * */
    public int processIdsBulkPost(List idList) {
        return processIdsBulkPost(idList, getDefaultPostingRank());
    }
    /** Use bulk SQL to post input List of ids to current state description at input rank
     * (more efficient than invoking SQL once per id), then process postings.
     * Returns positive value for success, negative value for error.
     * */
    public int processIdsBulkPost(List idList, int rank) { // list of Longs
        int istat = bulkPost(idList, rank);
        // Note: below does all "postings" inclusively in table, if listExclusiveProcessing == false
        return (istat > 0) ? doProcessing(idList) : istat;
    }
    /** Use bulk SQL to post input List of ids to current state description at default rank.
     * Returns positive value for success, negative value for error.
     * */
    public int bulkPost(List idList) {
        return bulkPost(idList, getDefaultPostingRank());
    }
    /** Use bulk SQL to post input List of ids to current state description at input rank.
     * Returns positive value for success, negative value for error.
     * */
    public int bulkPost(List idList, int rank) {
        int count = idList.size();
        if (count <= 0) return 0; 

        long [] ids = new long [count];
        for (int i = 0; i<count; i++)  {
          ids[i] = ((Long)idList.get(i)).longValue();
        }
        return bulkPost(ids, rank);
    }
    /** Use bulk SQL to post input array of ids to current state description at default rank.
     * Returns positive value for success, negative value for error.
     * */
    public int bulkPost(long [] ids) {
        return bulkPost(ids, getDefaultPostingRank());
    }
    /** Use bulk SQL to post input array of ids to input state description at input rank.
     * Returns positive value for success, negative value for error.
     * */
    public int bulkPost(long [] ids, int rank) {
        return bulkPost(ids, myPCID.getGroupName(), myPCID.getThreadName(), myPCID.getStateName(), rank);
    }
    /** Use bulk SQL to post input array of ids to input state description at input rank.
     * Sets the default posting rank to the input rank when different.
     * Returns positive value for success, negative value for error.
     * */
    @SuppressWarnings("deprecation")
    public int bulkPost(long [] ids, String groupName, String threadName, String stateName, int postingRank) {
        if ( ! isValidRank(postingRank))
             return ProcessingResult.INVALID_POSTING_RANK.getIdCode();

        if (getDefaultPostingRank() != postingRank) setDefaultPostingRank(postingRank);

        int istat = ProcessingResult.UNIT_SUCCESS.getIdCode();

        try {
            if (pcsBulkPostIdsForProcessingStmt == null) {
                pcsBulkPostIdsForProcessingStmt = connection.prepareCall(PCS_BULK_POST);
            }
            pcsBulkPostIdsForProcessingStmt.registerOutParameter(1, java.sql.Types.INTEGER);
            // Database type created for CODE schema (CREATE OR REPLACE TYPE ID_TABLE AS TABLE OF INTEGER)
            // Type is qualified with user's schema by Oracle jdbc if not fully qualified in descriptor constructor -aww 
            //oracle.sql.ArrayDescriptor aDesc = new oracle.sql.ArrayDescriptor("CODE.ID_TABLE", connection);
            oracle.sql.ArrayDescriptor aDesc = new oracle.sql.ArrayDescriptor("ID_TABLE", connection);
            pcsBulkPostIdsForProcessingStmt.setArray(2, new oracle.sql.ARRAY(aDesc, connection, ids));
            // To replace above with below requires Oracle version 11.2.0.5 or higher -aww 2015/03/19
            //pcsBulkPostIdsForProcessingStmt.setArray(2, ((oracle.jdbc.OracleConnection)getConnection()).createOracleArray("ID_TABLE", ids));
            pcsBulkPostIdsForProcessingStmt.setString(3, groupName);
            pcsBulkPostIdsForProcessingStmt.setString(4, threadName);
            pcsBulkPostIdsForProcessingStmt.setString(5, stateName);
            pcsBulkPostIdsForProcessingStmt.setInt(6, postingRank);
            pcsBulkPostIdsForProcessingStmt.execute();
            istat = pcsBulkPostIdsForProcessingStmt.getInt(1);
            if (istat <= 0) System.err.println("ERROR: " + getClass().getName() +" bulkPost return status=" + istat);
        }
        catch (SQLException ex) {
        //catch (Exception ex) {
            ex.printStackTrace();
            istat = ProcessingResult.DB_SQL_EXCEPTION.getIdCode();
        }
        return istat;
    }
    //

    /** Post all ids in the input List at the default rank for the currently set state processing description.
     * Returns positive value for success, negative value for error.
     * */
    public int postIds(List idList) {
        return postIds(idList, getDefaultPostingRank());
    }
    /** Post all ids in the input List at the input rank for the currently set state processing description.
     * Returns positive value for success, negative value for error.
     * */
    public int postIds(List idList, int rank ) { // list of Longs

        int istat = 0; 
        int idCount = idList.size();

        if (idCount <= 0) {
            System.out.println("INFO: Empty input list, no ids to post in input list.");
            return istat;
        }
        if (debug) System.out.println("DEBUG " + idCount + " id(s) to be posted, ids:\n" + idList.toString());

        int count = 0;
        for (int idx = 0; idx < idCount; idx++) {
            if ( hasStopSignal() ) break;
            istat = postId( ((Long) idList.get(idx)).longValue(), myPCID.getGroupName(), myPCID.getThreadName(), myPCID.getStateName(), rank);
            if (istat > 0) count++;
            else break; // fail fast, id posting should not cause errors
        }
        return (istat < 0) ? istat : count;
    }

    /** Update the state table entry that matches input id and current state description with result value.
     * Returns positive value for success, negative value for error. For predefined values see class ProcessingResult.
     * @see ProcessingResult
     * */
    public int resultId(long id, int resultCode) {
        return resultId(id, myPCID.getGroupName(), myPCID.getThreadName(), myPCID.getStateName(), myPCID.getRank(), resultCode);
    }
    /** Update the state table entry that matches input id and input state description with result value.
     * Returns positive value for success, negative value for error.
     * @see ProcessingResult
     * */
    public int resultId(long id, ProcessControlIdentifier pcId, int resultCode) {
        return resultId(id, pcId.getGroupName(), pcId.getThreadName(), pcId.getStateName(), pcId.getRank(), resultCode);
    }
    /** Update the state table entry that matches input id and input state description with result value.
     * Returns positive value for success, negative value for error.
     * @see ProcessingResult
     * */
    public int resultId(long id, String groupName, String threadName, String stateName, int resultCode) {
        return resultId(id, groupName, threadName, stateName, myPCID.getRank(), resultCode);
    }
    public int resultId(long id, String groupName, String threadName, String stateName, int rank, int resultCode) {
        int istat = ProcessingResult.UNIT_SUCCESS.getIdCode();
        try {
            // "{call PCS.putResult(?,?,?,?,?,?)}"; // new with rank in key
            // "{call PCS.putResult(?,?,?,?,?)}"; // old without rank in key
            if (pcsResultIdProcessingStmt == null) {
                pcsResultIdProcessingStmt = connection.prepareCall(PCS_RESULT_ID_PROCESSING_FUNCTION);
            }
            pcsResultIdProcessingStmt.registerOutParameter(1, java.sql.Types.INTEGER);
            pcsResultIdProcessingStmt.setString(2, groupName);
            pcsResultIdProcessingStmt.setString(3, threadName);
            pcsResultIdProcessingStmt.setLong(4, id);
            pcsResultIdProcessingStmt.setString(5, stateName);
            pcsResultIdProcessingStmt.setInt(6, resultCode);
            // for below rank replacement must 1st update PCS stored package function signature
            //pcsResultIdProcessingStmt.setInt(6, rank);
            //pcsResultIdProcessingStmt.setInt(7, resultCode);
            pcsResultIdProcessingStmt.execute();
            istat = pcsResultIdProcessingStmt.getInt(1);
            //
            if (debug) System.out.println("DEBUG status of executing result statement: " + istat);
            if (istat < 1) istat = ProcessingResult.ID_RESULT_FAILURE.getIdCode();
            try {
                // PostgreSQL version of stored proc does not commit, need to do it here.
                if (istat >= 1) connection.commit();
            }
            catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        catch (SQLException ex) {
        //catch (Exception ex) {
            ex.printStackTrace();
            istat = ProcessingResult.DB_SQL_EXCEPTION.getIdCode();
        }
        return istat;
    }

    /** Post all ids returned by subclass implementation of a prepared statement query assigned to 
     * "pcsGetPostIdsForDateRangeSQLString". Query should returns only those ids valid for the
     * input date range. Returns positive value for success, negative value for error.
     * Does a no-op and returns 0 if the pcsGetPostIdsForDateRangeSQLString is null, undefined
     * by the concrete subclass implementation.
     */ 
    public int postIds(int rank, DateRange dateRange) {
        return postIds(rank, dateRange.getMinTrueSecs(), dateRange.getMaxTrueSecs()); // for UTC time -aww 2008/02/08
    }
    /** Post all ids returned by subclass implementation of a prepared statement query assigned to 
     * "pcsGetPostIdsForDateRangeSQLString". Query should returns only those ids valid for the
     * input date range. Returns positive value for success, negative value for error.
     * Does a no-op and returns 0 if the pcsGetPostIdsForDateRangeSQLString is null, undefined
     * by the concrete subclass implementation.
     */ 
    public int postIds(int rank, double startTime, double endTime) {

        if (pcsGetPostIdsForDateRangeSQLString == null) return 0; // 10/12/05 aww

        int istat = 0;
        int count = 0;

        ResultSet rs = null;
        try {
            if (pcsGetPostIdsForDateRangeProcessingStmt == null) {
                pcsGetPostIdsForDateRangeProcessingStmt =
                    connection.prepareStatement(pcsGetPostIdsForDateRangeSQLString);
            }
            pcsGetPostIdsForDateRangeProcessingStmt.setDouble(1, startTime);
            pcsGetPostIdsForDateRangeProcessingStmt.setDouble(2, endTime);
            rs = pcsGetPostIdsForDateRangeProcessingStmt.executeQuery();
            if (rs != null) {
              while (rs.next()) {
                if ( hasStopSignal() ) break;
                istat = postId(rs.getLong(1), myPCID.getGroupName(), myPCID.getThreadName(), myPCID.getStateName(), rank);
                if (istat > 0) count++;
                else break; // fail fast, id posting should not cause errors
              }
            }
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            istat = ProcessingResult.DB_SQL_EXCEPTION.getIdCode();
        }
        finally {
            try {
              if (rs != null) rs.close();
            } catch (SQLException e) {}
        }
        return (istat < 0) ? istat : count;
    }

    /** Close JDBC PreparedStatements or other resources used by implementation. */
    public void closeProcessing() {
        closeDbProcessing();
        closeDataSource(); // if DataSource opened locally, closes connection
        closeLog(); // if non-null logStream flush and close
    }

    protected void closeDbProcessing() {

        try {

            if (connection == null || connection.isClosed()) {
                nullJdbcStatements();
                return;
            }

            signalStop();
          
            if (pcsGetNextIdForProcessingStmt != null) pcsGetNextIdForProcessingStmt.close();
            if (pcsResultIdProcessingStmt != null) pcsResultIdProcessingStmt.close();
            if (pcsPostIdForProcessingStmt != null) pcsPostIdForProcessingStmt.close();
            if (pcsBulkPostIdsForProcessingStmt != null) pcsBulkPostIdsForProcessingStmt.close();
            if (pcsGetPostIdsForDateRangeProcessingStmt != null) pcsGetPostIdsForDateRangeProcessingStmt.close();
            if (pcsCheckIdTableStmt != null) pcsCheckIdTableStmt.close();

            if ( pcsSignalStartStmt != null ) pcsSignalStartStmt.close();
            if ( pcsSignalMessageStmt != null ) pcsSignalMessageStmt.close();
            if ( pcsStopSignalledStmt != null ) pcsStopSignalledStmt.close();
            if ( pcsSignalStopStmt != null ) pcsSignalStopStmt.close();

        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }

        nullJdbcStatements();

    }

    private void nullJdbcStatements() {
        pcsGetNextIdForProcessingStmt = null;
        pcsResultIdProcessingStmt = null;
        pcsPostIdForProcessingStmt = null;
        pcsBulkPostIdsForProcessingStmt = null;
        pcsGetPostIdsForDateRangeProcessingStmt = null;
        pcsCheckIdTableStmt = null;
        pcsSignalStartStmt = null;
        pcsSignalMessageStmt = null;
        pcsStopSignalledStmt = null;
        pcsSignalStopStmt = null;
    }
    /** Invoke at the start of application processing.  Enters row entry into PCS_SIGNAL table.
     * Sets internal key for row lookup.
     * Return true upon success.
     * */
    public boolean signalStart() {

        if (sigId > 0l) return false; // signalStop()

        sigId = 0l;

        try {
            if (pcsSignalStartStmt == null) {
                pcsSignalStartStmt = connection.prepareCall(PCS_SIGNAL_START_FUNCTION);
            }
            // returns sigId
            pcsSignalStartStmt.registerOutParameter(1, java.sql.Types.BIGINT);
            // input group table state, appName, localHostName, localUserName
            pcsSignalStartStmt.setString(2, myPCID.getGroupName());
            pcsSignalStartStmt.setString(3, myPCID.getThreadName());
            pcsSignalStartStmt.setString(4, myPCID.getStateName());
            pcsSignalStartStmt.setString(5, EnvironmentInfo.getApplicationName());
            pcsSignalStartStmt.setString(6, EnvironmentInfo.getLocalHostShortName());
            pcsSignalStartStmt.setString(7, EnvironmentInfo.getUsername());
            pcsSignalStartStmt.execute();
            sigId = pcsSignalStartStmt.getLong(1);
            try {
                if (sigId > 0l) {
                    connection.commit();
                }
            }
            catch (SQLException ex){
                ex.printStackTrace();
                return false;
            }
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            return false; // ProcessingResult.DB_SQL_EXCEPTION.getIdCode();
        }
        return (sigId > 0l);
    }

    /** Invoke at the end of application processing.  Updates the row entry in PCS_SIGNAL table
     * created by signalStart() to flag that processing is to end.
     * Return true upon success.
     */
    public boolean signalStop() {
        int istat = 0;
        try {
            if (pcsSignalStopStmt == null) {
                pcsSignalStopStmt = connection.prepareCall(PCS_SIGNAL_STOP_FUNCTION);
            }
            // returns sigId
            pcsSignalStopStmt.registerOutParameter(1, java.sql.Types.INTEGER);
            // input group table state, appName, localHostName, localUserName
            pcsSignalStopStmt.setLong(2, sigId);
            pcsSignalStopStmt.setString(3, "Processed count = " + String.valueOf(count));
            pcsSignalStopStmt.execute();
            istat = pcsSignalStopStmt.getInt(1);
            try {
                if (istat >= 0) {
                    connection.commit();
                }
            }
            catch (SQLException ex) {
                ex.printStackTrace();
                return false;
            }
            sigId = 0l;
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            return false; // ProcessingResult.DB_SQL_EXCEPTION.getIdCode();
        }
        return (istat >= 0);
    }

    /** Returns <i>true</i> after signalStop() is invoked. Application should stop further id processing. */
    public boolean hasStopSignal() {
        int istat = 0;
        try {
            if (pcsStopSignalledStmt == null) {
                pcsStopSignalledStmt = connection.prepareCall(PCS_SIGNAL_HAS_STOP_FUNCTION);
            }
            // returns <0 error, =0 no, =1 yes
            pcsStopSignalledStmt.registerOutParameter(1, java.sql.Types.INTEGER);
            // input sigId
            pcsStopSignalledStmt.setLong(2, sigId);
            pcsStopSignalledStmt.execute();
            istat = pcsStopSignalledStmt.getInt(1);
            try {
                if (istat >= 0) {
                    connection.commit();
                }
            }
            catch (SQLException ex) {
                ex.printStackTrace();
                return false;
            }
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            return false; // ProcessingResult.DB_SQL_EXCEPTION.getIdCode();
        }
        return (istat > 0);
    }

    /** Update PCS_SIGNAL row created by signalStart() with a status message (e.g. current id being processed).
     * Return true upon success.
     * */
    public boolean signalMessage(String message) {
        int istat = 0;
        try {
            if (pcsSignalMessageStmt == null) {
                pcsSignalMessageStmt = connection.prepareCall(PCS_SIGNAL_MESSAGE_FUNCTION);
            }
            // returns <0 error, =0 no, =1 yes
            pcsSignalMessageStmt.registerOutParameter(1, java.sql.Types.INTEGER);
            // input sigId, message
            pcsSignalMessageStmt.setLong(2, sigId);
            pcsSignalMessageStmt.setString(3, message);
            pcsSignalMessageStmt.execute();
            istat = pcsSignalMessageStmt.getInt(1);
            try {
                if (istat <= 0) {
                    connection.commit();
                }
            }
            catch (SQLException ex) {
                ex.printStackTrace();
                return false;
            }
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            return false; // ProcessingResult.DB_SQL_EXCEPTION.getIdCode();
        }
        return (istat > 0);
    }

    /** Close DataSource, if it was created by this instance */
    protected void closeDataSource() {
        if (instanceDataSource) DataSource.close();
    }

    /**
    * Create a connection to the data source as described by input properties.
    * Returns 'true' on success.
    */
    protected boolean setupDataSourceFromProperties() {

        // No-op if a connection exists and is not closed
        // Alternative would be to close all db statements and the connection, null the statements
        // then do a datasource reconnect
        if (connection != null) {
            try {
                if (!connection.isClosed()) return true; // no-op, keep reusing the existing connection
            }
            catch (SQLException ex) {
                ex.printStackTrace();
            }
        } 

        // Else create a new connection from an existing data source, if none, create one from a dbase description in properties
        if (DataSource.isNull()) { // make local DataSource
            if (debug) System.out.println("DEBUG " + getClass().getName()+" creating new instance of DataSource ...");
        }
        else { // use existing DataSource
            instanceDataSource = false;
            if (debug) System.out.println("DEBUG " + getClass().getName()+" creating new connection from an existing DataSource...");
        }

        if (!instanceDataSource) {
            // create a new connection from the existing DataSource, return on failure
            setConnection(DataSource.getNewConnect());
            if (connection == null) {
                System.out.println("ERROR: " + getClass().getName()+" unable to create new connection from existing DataSource");
                System.out.println( "DataSource:\n"+ DataSource.toDumpString());
                return false; // failed 
            }
        }
        else { // make the new instance DataSource from input properties, return on failure

        //if (DataSource.onServer()) {
        //    DataSource.set(DataSource.createInternalDbServerConnection());
        //}
        //else {

            DbaseConnectionDescription dcd = props.getDbaseDescription();
            System.out.println(dcd.toString());

            // DataSource connection configured from current properties
            if (!props.setupDataSource()|| DataSource.isNull() || DataSource.isClosed()) {
              System.err.println("ERROR: " + dcd.describeUndefinedAttributes());
              System.err.println("Check input properties for valid connection description");
              System.err.println("or command line input of properties (e.g. user/pass)");
              System.err.println("Database Connection failure: "+ DataSource.getConnectionStatus());
              return false; // failed
            }
            else {
              props.setProperty("dbLastLoginURL", props.getDbaseDescription().getURL());
            }

        //}

        }

        // At this point should have a connection, is it the write type?
        if (!DataSource.isWriteBackEnabled() || DataSource.isReadOnly()) {
           System.out.println("INFO: DataSource is set READ-ONLY, PCS result processing not possible.");
           if (instanceDataSource) DataSource.close();
           return false; // failed
        }

        // Set static attribute in AbstractWaveform class about type of data source (Database or WaveServer)
        props.setupWaveDataSource();

        if (debug)
            System.out.println( "DEBUG ProcessingController DataSource:\n"+ DataSource.toDumpString());

        // Set instance connection to be used for processing
        connection = DataSource.getConnection();

        return (connection != null);
    }

    @Override
    public void close() {
        closeProcessing(); 
    }

    /** Return String description of current settings. */
    public String getProcessingAttributesString() {
        StringBuffer sb = new StringBuffer(1024);
        sb.append(" ProcessingControlId=").append((myPCID == null) ? "null" : myPCID.toString());
        sb.append(" defaultPostingRank=").append(defaultPostingRank);
        sb.append(" debug=").append(debug);
        sb.append(" verbose=").append(verbose);
        if ( props.getBoolean("pcsLogging") ) {
          sb.append(System.getProperty("line.separator"));
          sb.append("logFileName=").append(logFileName);
          if (logRotationInterval == Long.MAX_VALUE) sb.append(" logRotationInterval=none");
          sb.append(" logRotationInterval=").append(logRotationInterval);
        }
        else sb.append(" logging=off");
        sb.append(System.getProperty("line.separator"));
        sb.append(" listExclusiveProcessing=").append(listExclusiveProcessing);
        sb.append(" forceStateProcessing=").append(forceStateProcessing);
        sb.append(" runMode=").append(((runMode == 0) ? "one-shot" : "continuous"));
        sb.append(" fastFailLoops=").append(fastFailLoops);
        sb.append(" defaultSleepTimeMillis=").append(defaultSleepTimeMillis);
        sb.append(System.getProperty("line.separator"));
        sb.append(" PCS_SIGNAL id=" + sigId);
        sb.append(" appName=").append(appName);
        sb.append(" autoProcessing=").append(autoProcessing);
        sb.append(" instanceDataSource=").append(instanceDataSource);
        sb.append(System.getProperty("line.separator"));
        if (instanceDataSource) sb.append(DataSource.describeConnection());
        else sb.append(System.getProperty("line.separator"));

        return sb.toString();
    }
}
