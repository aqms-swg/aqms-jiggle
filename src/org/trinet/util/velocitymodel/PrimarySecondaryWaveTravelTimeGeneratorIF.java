package org.trinet.util.velocitymodel;
import org.trinet.util.gazetteer.*;

public interface PrimarySecondaryWaveTravelTimeGeneratorIF {
    void setSource(Geoidal source) ;
    void setSource(GeoidalLatLonZ source) ;
    void setSource(double lat, double lon, double z) ;

    Geoidal getSource() ;

    double pTravelTime(double range) ;
    double pTravelTime(double lat, double lon) ;
    double pTravelTime(Geoidal receiver) ;

    double sTravelTime(double range) ;
    double sTravelTime(double lat, double lon) ;
    double sTravelTime(Geoidal receiver) ;

    void setModel(VelocityModelIF model);
    VelocityModelIF getModel();
}

