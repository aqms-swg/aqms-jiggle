package org.trinet.jiggle;

import java.text.*;
import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

//import org.trinet.jdbc.*;
import org.trinet.util.DateTime;
import org.trinet.util.TimeSpan;
import org.trinet.util.FilterIF;
import org.trinet.util.ListDataStateAdapter;
import org.trinet.util.ListDataStateEvent;
import org.trinet.jasi.Channelable;
import org.trinet.jasi.ChannelDistanceSorter;
//import org.trinet.filters.WAFilter;

/**
 * A panel containing all the WFPanels for a given MasterView.
 * Will be placed in a JPanel or ScrollPane
 * so it implements the Scrollable interface to support custom scrolling behavior.
 */
public class WFGroupPanel extends JPanel implements Scrollable  {

    private MasterView mv;            // the master view

    /** Array of WFPanel that make up the group. */
    protected WFPanelList wfpList = new WFPanelList();    // list of WFPanels in this group

    private boolean trackWidth  = false;
    private boolean trackHeight = false;

    /** The default width of one WFPanel in this group */
    private static final int wfWidth = 800;
    /** The default height of one WFPanel in this group */
    private static final int wfHeight= 80;

    protected static double horzBlockScrollFactor = 0.10; // default factor 1/10 the single panel width -added -aww  2009/10/06
    protected static int horzUnitScrollPixels = 5; // default 5 pixels scalar -added -aww  2009/10/06

    protected static int wfDataBoxScaling = 0;

    /** The size of one WFPanel in this group */
    protected Dimension singlePanelSize = new Dimension(wfWidth, wfHeight) ;

    private MyChannelDistanceSorter distSorter = null;

    /** True if you want to time align the traces */
//    boolean timeAlign = true;

   /** Set true to use ActiveWFPanels rather then passive ones.
    * ActiveWFPanels respond to mouse events and notify the ActiveWFVModel
    */
    private boolean activePanels = false;

    public WFGroupPanel() { }

    /**
     * Create a WFGroupPanel that has one passive
     * WFPanel for each WFView in the MasterView.
     */
    public WFGroupPanel(MasterView mview) {
        this(mview, false);
    }

    /**
     * Create a WFGroupPanel that has one WFPanel
     * for each WFView in the MasterView. If the second argument is 'true'
     * the WFPanels will be active and will respond to mouse events and
     * notify the ActiveWFVModel.
     */
    public WFGroupPanel(MasterView mview, boolean activePanels) {
        this();
        setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));  // default JComponent max is Short.MAX_VALUE=32767 pixels -aww 2010/01/25
        this.activePanels = activePanels;
        setMasterView(mview);
    }

    protected void clear() {
        if (wfpList != null) wfpList.clear();
    }

    /**
     * Create the WFGroupPanel for this MasterView using the default dimensions.
     */
    public void setMasterView(MasterView mview) {
        setMasterView(mview, activePanels);
    }

    public void setMasterView(MasterView mview, boolean activePanels) {
        if (mv == mview ) return; // added 2009/03/25 -aww
        this.activePanels = activePanels;
        setMasterView(mview, singlePanelSize.width, singlePanelSize.height);
    }

    /**
     * Create the WFGroupPanel for this MasterView using the dimensions given.
     * The size of the individual WFPanels will be adjusted to fit.
     */
    public void setMasterView(MasterView mview, int width, int height) {
        if (mv == mview && singlePanelSize.width == width && singlePanelSize.height == height) return; // added 2009/03/25 -aww
        if (wfpList.size() > 0) clear(); // added 2009/03/25 -aww
        this.mv = mview;
        wfpList.setMasterView(mv);
        addWFPanels(mv);
        mv.alignViews(); // do this to set the mv timespan uniformly for all of the newly added panels above
        setPanelSize(wfWidth, wfHeight);
    }
    
    /**
     * Return the calculated size of the whole WFGroupPanel. The size is based on the size
     * set for the single panels using setSinglePanelSize().
     */
    public Dimension getPreferredSize() {
        if (mv == null) return  new Dimension(singlePanelSize);
        // Added this logic to prevent scroller "hidden" panels contributing, keeps viewport panels the same size -aww 2009/03/26
        int cnt = 0;
        if (wfpList != null) {
            for (int i=0; i< wfpList.size(); i++) {
               if ( ((WFPanel)wfpList.get(i)).isVisible()) cnt++;
            }
        }
        else cnt = mv.getWFViewCount();

        return new Dimension(singlePanelSize.width, singlePanelSize.height * cnt);
    }

    /** Set the size of one WFPanel. This actually sets the size of the WFGroupPanel
    * to accomodate all the WFPanels in the MasterView at this size (height). On
    * repaint the individual WFPanels will "stretch" to fill the space.
    * */
    public void setSinglePanelSize(Dimension size) {
        singlePanelSize = new Dimension(size);
        //for (int i = 0; i < wfpList.size(); i++) ((WFPanel)wfpList.get(i)).setPreferredSize(size);
        // set the group panel's size
        setPreferredSize(getPreferredSize());
        setSize(getPreferredSize()); // setSize() does a revalidate() in event queue
    }

    /** Set the size of one WFPanel. The size of the WFGroupPanel will be adjusted to
     * accomodate all the WFPanels in the MasterView at this size.*/
    public void setSinglePanelSize( int width, int height) {
        setSinglePanelSize(new Dimension(width, height));
    }

    /*
    void setPixelsPerSec(double pps) {
        Dimension d = new Dimension(singlePanelSize);
        d.width = (int) mv.getTimeSpan().getDuration()*pps;
        setSinglePanelSize(d);
    }
    */

    /** Set the size of the whole group panel. The size of the individual panels are adjusted
     * to fit in this dimension. */
    public void setPanelSize(Dimension dim) {
        setPanelSize(dim.width, dim.height);
    }

    /** Set the size of the whole group panel */
    private void setPanelSize(int width, int height) {
        if (mv != null && !mv.wfvList.isEmpty())
            singlePanelSize = new Dimension(width, (height / mv.wfvList.size()));
        setSize(width, height); // does a revalidate() in event queue so do after singlePanelSize is set -aww 2009/04/04
    }

    /** Turns on or off the display of phase descriptions in the "flag" part
    * of the phase pick flags. */
    public void showPhaseDescriptions(boolean tf) {
        WFPanel mwfp[] = wfpList.getArray();
        for ( int i=0; i < mwfp.length; i++){
             mwfp[i].showPhaseDescriptions(tf);
        }
    }

    public void setShowTimeLabel(boolean tf) {
       WFPanel mwfp[] = wfpList.getArray();
       for ( int i=0; i < mwfp.length; i++){
              mwfp[i].setShowTimeLabel(tf);
       }
    }
    public void setShowTimeScale(boolean tf) {
       WFPanel mwfp[] = wfpList.getArray();
       for ( int i=0; i < mwfp.length; i++){
              mwfp[i].setShowTimeScale(tf);
       }
    }
    public void setTimeTicksFlag(int flag) {
       WFPanel mwfp[] = wfpList.getArray();
       for ( int i=0; i < mwfp.length; i++){
              mwfp[i].setTimeTicksFlag(flag);
       }
    }

    public void setShowSamples(boolean tf) {
       WFPanel mwfp[] = wfpList.getArray();
       for ( int i=0; i < mwfp.length; i++){
              mwfp[i].setShowSamples(tf);
       }
    } 

    public void setShowSegments(boolean tf) {
       WFPanel mwfp[] = wfpList.getArray();
       for ( int i=0; i < mwfp.length; i++){
              mwfp[i].setShowSegments(tf);
       }
    }

    public void setShowPhaseCues(boolean tf) {
       WFPanel mwfp[] = wfpList.getArray();
       for ( int i=0; i < mwfp.length; i++){
              mwfp[i].setShowPhaseCues(tf);
       }
    }

    public void setShowCursorTimeAsLine(boolean tf) {
       WFPanel mwfp[] = wfpList.getArray();
       for ( int i=0; i < mwfp.length; i++){
              mwfp[i].setShowCursorTimeAsLine(tf);
       }
    }
    public void setShowCursorAmpAsLine(boolean tf) {
       WFPanel mwfp[] = wfpList.getArray();
       for ( int i=0; i < mwfp.length; i++){
              mwfp[i].setShowCursorAmpAsLine(tf);
       }
    }

    /** Set filter of all WFPanels and set them to show the filtered waveform. */
    public void setShowAlternateWf(boolean tf) {
        WFPanel mwfp[] = wfpList.getArray();
        for ( int i=0; i < mwfp.length; i++){
            mwfp[i].setShowAlternateWf(tf);
        }
    }

    /**
     * Create MultWFPanels for the masterView's WFViews and stack them into this
     * WFGroupPanel.
     */
    protected void addWFPanels(MasterView mv) {

        this.removeAll(); // remove existing panels

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));            // stack vertically

        WFView[] wfv = mv.wfvList.getArray();

        WFPanel wfp;
        //Is this feasible user property option to do here before creating the group of WFPanels? 
        //   To scale WFPanel amplitudes relative by gain, loop through wfv array and for each
        //   wfv save its channel gain, Math.abs(wfv.chan.getGain(wfv.wf.getTimeSpan().getEnd())
        //   while in loop also determine max(gain) by the test (gain > maxGain) maxGain = gain.
        //   Then loop over the array again, and set for each view a value normRatio=maxGain/gain.
        //   In WFPanel class setupWf() do not scale timeseries sample value to value*normRatio,
        //   Would need variant of WFDataBox to be able to scale panels boxes min,max amp
        //   by normRatio*wf.getMxxAmp() in order to get panel box limits scaled by relative gain. 
        //   Alternate wf plots for gain dependent wf filters like like WA would be effected,
        //   unless logic added to create the 2 different WFDataBox configuration based on testing
        //   for filter or amp units type.

        Object obj = mv.getOwner();
        double factor = 1.;
        if (obj instanceof WaveformDisplayIF) {
           factor = ((WaveformDisplayIF)obj).getWfPanelProperties().getDouble("wfpanel.ampScaleFactor", 1.);
           if (factor <= 0.) factor = 1.;
        }

        // make a WFPanel for each WFView
        for ( int i=0; i < wfv.length; i++) {

            wfv[i].isActive = true; // reset in case "hide" in scroller toggled it false for current wfv's in mv -aww 2010/02/07

            if (activePanels){
                //wfp = new ActiveWFPanel(wfv[i]); // set view below after set wfdatabox "scaling" flag
                wfp = new ActiveWFPanel();
            } else {
                //wfp = new WFPanel(wfv[i]); // set view below after set wfdatabox "scaling" flag
                wfp = new WFPanel();
            }
            wfp.setAmpScaleFactor(factor);
            wfp.dataBox.setScaling(wfDataBoxScaling);
            wfp.setWFView(wfv[i]);
            wfpList.add(wfp);

    //     wfp.setPreferredSize(getSinglePanelSize());
    //     wfp.setSize(getSinglePanelSize());
            wfp.setPreferredSize(new Dimension(wfWidth, wfHeight));
            wfp.setSize(new Dimension(wfWidth, wfHeight));

            add(wfp);     // add the graphical component to this panel
        }
        System.out.println("Total WFViews = " + wfv.length + " at " + new DateTime());

    }

    private class MyChannelDistanceSorter extends ChannelDistanceSorter {
      public int compare (Object o1, Object o2) {
        Channelable c1 = ((WFPanel)o1).wfv;
        Channelable c2 = ((WFPanel)o2).wfv;
        return super.compare(c1, c2);
      }

      public boolean equals(Object o) {
        return (o instanceof MyChannelDistanceSorter); 
      }
    }

    public void resortPanels() {
        if (distSorter == null) distSorter = new MyChannelDistanceSorter(); 
        synchronized (distSorter) {
          this.removeAll(); // remove existing panels
          Collections.sort(wfpList, distSorter); // sort list
          WFPanel wfp = null;
          for ( int i=0; i < wfpList.size(); i++) {
            wfp = (WFPanel) wfpList.get(i); 
            wfp.updateToolTip();
            add(wfp); // add WFPanel in sort order
          }
        }
    }

    public void resetWfColor() {
        wfpList.resetWfColor();
    }

    /**
     * Return the WFPanel that contains this WFView. Returns null if not found.
     */
    public WFPanel getWFPanel(WFView wfv) {
        return wfpList.get(wfv);
    }

    public WFPanelList getWFPanelList() {
        return wfpList;
    }

    /** Return number of WFPanels in this component. */
    public int getPanelCount() {
        //return getComponentCount();
        return wfpList.size();
    }

/**
 * Align all the WFPanels in this group by setting their 'viewSpans' to the
 * timeSpan of the MasterView. If false, set 'viewSpan' equal to the WFView's
 * dataSpan.
 */
    public void alignOnTime() {
        setTimeBounds(mv.timeSpan);
    }

    /**
     * Set the time window shown in the WFGroupPanel. This will also align all
     * the WFPanels on time so traces will be time aligned.
     * This sets the panelBox of the member WFPanels.
     * This does NOT change the WFView's timeSpans so it
     * limits what is painted not what is actually in memory.
     */
    public void setTimeBounds(TimeSpan ts)  {
     WFPanel mwfp[] = wfpList.getArray();
        for ( int i=0; i < mwfp.length; i++) {
            mwfp[i].setTimeBounds(ts);
        }
    }

    public void resetPanelBoxSize()  {
        WFPanel wfp[] = wfpList.getArray();
        for ( int i=0; i < wfp.length; i++) {
            wfp[i].setPanelBoxSize();
        }
    }

    public void setWFDataBoxScaling(int flag) {
        wfDataBoxScaling = flag;
        WFPanel wfp[] = wfpList.getArray();
        for ( int i=0; i < wfp.length; i++) {
            wfp[i].setWFDataBoxScaling(wfDataBoxScaling);
        }
    }

    public void resetAmpScaleFactor(double factor) {
        wfpList.resetAmpScaleFactor(factor);
    }


    /** Set filter of all WFPanels and set them to show the filtered waveform. */
    public void setFilter(FilterIF filter) {
        WFPanel mwfp[] = wfpList.getArray();
        for ( int i=0; i < mwfp.length; i++){
             mwfp[i].setFilter(filter);
        }
    }

   /**
    * Scrollable interface methods
    */
    public Dimension getPreferredScrollableViewportSize() {
        return getPreferredSize();
    }

    public boolean getScrollableTracksViewportWidth() {
        return trackWidth;
    }

    public boolean getScrollableTracksViewportHeight() {
        return trackHeight;
    }
    public void setScrollableTracksViewportWidth(boolean tf) {
        trackWidth = tf;;
    }
    public void setScrollableTracksViewportHeight(boolean tf) {
        trackHeight = tf;
    }

    /**
     * Determines scrolling increment when bar widget is clicked. (Coarse scrolling)
     * Scroll increment is equal to the viewport height less height of one WFPanel vertically and
     * the "horzBlockScrollFactor" fraction of the viewport width (default 0.10) horizontally.
     * The "horzBlockScrollFactor" value can be set by Jiggle property. 
     */
    public int getScrollableBlockIncrement(Rectangle visRec, int orient, int dir) {
        int move = 1;
        if (orient == SwingConstants.VERTICAL) {
          // visible viewport height minus one trace panel height
          move = ((int)getVisibleRect().getHeight()) - ((int)singlePanelSize.getHeight());
        }
        else { // horizontal
            // used to jump 0.10 of wfpanel width, changed to settable scalar fraction of viewport width -aww  2009/10/06
            //return (int) Math.round(wfWidth*horzBlockScrollFactor);
            move = (int) Math.round(getVisibleRect().getWidth()*horzBlockScrollFactor);
        }
        if (move < 1) move = 1;
        return move;
    }

    /**
     * Determines scrolling increment when arrow widgit is clicked. (Fine scrolling)
     * Return the value, in pixels, to be used as the vertical scrolling unit
      increment. Jump one trace.
     */
    public int getScrollableUnitIncrement(Rectangle visRec, int orient, int dir) {
      int move = 1;
      if (orient == SwingConstants.VERTICAL) {
          move = singlePanelSize.height;
      }
      else { // horizontal
          move = horzUnitScrollPixels; // pixels
      }
      if (move < 1) move = 1;
      return move;
    }

    /** Return the size of a single panel in this group */
    public Dimension getSinglePanelSize() {
        return singlePanelSize ;
    }

    public boolean isOpaque() {
        return true;
    }

    /** Return a row header for this GroupPanel.
     * By default it will contain the channel name.
     * */
    public JComponent getRowHeaderPanel() {
      JPanel rowHeader = new JPanel();
      rowHeader.setLayout(new BoxLayout(rowHeader, BoxLayout.Y_AXIS));
      //rowHeader.setLayout(new ColumnLayout());

      WFPanel wfp[] = wfpList.getArray();
      for ( int i=0; i < wfp.length; i++){
        rowHeader.add(wfp[i].getRowHeader());
      }
      return rowHeader;
    }

} // end of class
