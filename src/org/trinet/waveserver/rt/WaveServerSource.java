package org.trinet.waveserver.rt;

/**
 * Needed as a base for all WaveServerGroups so they can be recognized by
 * the Waveform class.
 */

public interface WaveServerSource {

  public int getTimeSeries(org.trinet.jasi.Waveform waveform) ;

}
