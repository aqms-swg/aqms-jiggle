package org.trinet.jasi;

import java.util.*;
import org.trinet.util.*;

/**

Abstract model that determines and returns a Collection of channel/time-windows
that characterize a seismic event, where the included windows should
have seismic energy for the specified Solution. Specific algorithms for
determing the window Collection extend this class.<p>

The model name and its explanation are typically set by the specific subtype 
implementations. However to allow discrimination of customized configurations,
both the model name and its explanation can also be set using input properties:
<i>modelName</i> and <i>modelExplanation</i>.<p>

There are two main parts to a model implementation:<p>
1) Deciding which channels to include <br>
2) Deciding the start-time and duration of the time window for each channel. Each
channel may have a different window.<p>

Most models (algorithms) require a Solution. The origin time is needed to set
time window starts and the magnitude is often used to set window durations.<p>

This abstract class has a default algorithm (method getTimeWindow()) based on
the coda duration curve for southern California. Override it if it is not
appropriate for your application.<p>

The basic properties that this base class makes available for selecting
channel time-windows are:<p>

Channel list properties for certain models :<br>
candidateListName -- named list of application channels from JASI_CHANNEL_VIEW view id<br>
minDistance -- all channels are returned that are less than or equal to this distance from the event (km)<br>
maxDistance -- no channels are returned that are beyond this distance from the event (km)<br>
maxChannels -- only the first 'maxChannels' channels by known distance are included <br>
includeAllComponents -- if true, include all orientations of a channel type are included<br>
defaultNullMag -- if the magnitude is unknown then use this value for energy cutoff estimate<br>
includeAllMag -- if the mag >= this value keep all channels<br>
includePhases -- include all channels that have phase readings
includeMagAmps -- include all channels that have magnitude-amplitude readings
includePeakAmps -- include all channels that have peak-amplitude readings
includeCodas -- include all channels that have coda readings
allowedSeedChanTypes -- include only those channels whose seedchan types are listed by property
allowedLocationTypes -- include only those channels whose seedchan types are listed by property
allowedNetTypes -- include only those channels whose net types are listed by property
allowedStaTypes -- include only those sta whose names are listed by property
rejectedSeedChanTypes -- exclude only those channels whose seedchan types are listed by property
rejectedLocationTypes -- exclude only those channels whose seedchan types are listed by property
rejectedNetTypes -- exclude only those channels whose net types are listed by property
rejectedStaTypes -- exclude only those sta whose names are listed by property
synchCandidateListBySolution -- rebuilds candidate list, if needed, when current solution changes. 
filterWfListByChannelList -- waveform list channels are those found in candidate list, if any.

<br>
Time window properties for certain models:<br>
requiresLocation -- model requires valid origin solution to calculate window time span<br> 
preEventSize -- save this many seconds before expected P-arrival time<br>
postEventSize -- save this many seconds after expected coda decay time <br>
minWindowSize -- always save at least this many seconds<br>
maxWindowSize -- never save more than this many seconds<br>
magTauIntercept -- Empirical magnitude (Ml) where tau = 1<br>
magTauSlope -- slope of line fitting log10(tau) = (Ml - magTauIntercept)/magTauSlope<br>
NOTE: Window time span starts from predicted P-arrival time minus preEventSize to S-traveltime plus postEventSize + the coda duration.
If total span is less than minWindow size then the end time is set to start time + minWindowSize, and if the span
is greater than maxWindow size then the end time is set to start+maxWindowSize.<br>

Waveform list properties for certain models:<br>
includeDataSourceWf -- include Waveforms timeseries looked up from DataSource (but using timespan defined by getChannelTimeWindow()<br>
*/

public abstract class ChannelTimeWindowModel implements Cloneable, java.io.Serializable {

    protected BenchMark bm = new BenchMark();

    public boolean debug = false;

    // Lookup channel list that was current on date of Solution?
    protected boolean synchListBySolution = false;
    protected boolean filterWfListByChannelList = false;

    /** Set 'true' if this model requires an event location to function. */
    protected boolean requiresLocation = true;
    
    /** The solution for which the model is calculated. */
    protected Solution sol = null;
    /** Last solution for which the candidate channel list was screened. */
    protected Solution oldSol = null;

    /** Model name string. */
    protected String modelName = "Default";

    /** A string with an brief explanation of the model. For help and tooltips. 
     * Returned by getExplanation() */
    protected String explanation = "default channel time window model";

    /** A ChannelList from which channel time windows will be selected. */
    protected ChannelableList candidateList = null;

    /** List of channels that MUST be include in the final list.*/
    protected ChannelableList requiredList = new ChannelableList();

    /** String list of channels that are allowed. The 'candidateList' will be
     * filtered to exclude all but these channels. Blank list means "allow all".
     */
    protected String staIncludeList[]  = {};
    protected String locIncludeList[] = {};
    protected String chanIncludeList[] = {};
    protected String netIncludeList[]  = {};

    protected String staRejectList[]   = {};
    protected String locRejectList[]  = {};
    protected String chanRejectList[]  = {};
    protected String netRejectList[]   = {};

    /** Window start time (secs). Some algorithms may need an minimum amount of data.*/
    protected static final double defStartTime = 0.;
    protected double starttime    =  defStartTime;
    /** Minimum window duration (secs). Some algorithms may need an minimum amount of data.*/
    protected double minWindow    =  30.0;
    /** Maximum window duration (secs). Some algorithms may need an minimum amount of data.*/
    protected double maxWindow    = 600.0;
    
    /** Some models may allow setting a fixed Time window */
    protected TimeSpan fixedTimeWindow = null;

    /** Time added to start of time window calculated by the model algorithm (secs).*/
    protected double preEvent     =  10.0;
    /** Time added to duration of time window calculated by the model algorithm (secs).*/
    protected double postEvent    =  10.0;

    /** Magnitude where in the duration-mag relationship that tau = 1.
     * Default value = -0.95 based on historic SoCal coda relationship. */
    protected double magTauIntercept =  -0.95;
    /** Slope of a line of fitting the duration-mag relationship.
     * Default value = 2.15 based on historic SoCal coda relationship. */
    protected double magTauSlope     = 2.15;

    /** The maximum distance allowed by the getDistanceCutoff() method.
     * All channel beyond this distance will be excluded from the Channel set.
     * Default = Double.MAX_VALUE
     */
    protected double maxDistance = Double.MAX_VALUE;

    /** The minimum distance allowed by the getDistanceCutoff() method.
     * All channel within this distance will be included in the Channel set.
     * Default = 0.
     */
    protected double minDistance = 0.0;

    /** The maximum number of channels that will be allowed to belong to
     * this Channel set.
     * Default = Integer.MAX_VALUE
     * */
    protected int maxChannels = Integer.MAX_VALUE;

/** If true, include all components (orientations) of a channel type if any is included. */
    protected boolean includeAllComponents = false;

    /** Include all channels if the magnitude >= this value.*/
    protected double includeAllMag = 10.0;
    protected double defaultNullMag = 1.5;

    /** If true, include all channels that have phase readings. */
    protected boolean includePhases = true;
    /** If true, include all channels that have magnitude-amplitude readings. */
    protected boolean includeMagAmps = true;
    /** If true, include all channels that have peak-amplitude readings. */
    protected boolean includePeakAmps = true;
    /** If true, include all channels that have coda readings. */
    protected boolean includeCodas = true;

    protected boolean includeDataSourceWf = false;

    protected boolean useMasterList = false;

    protected String triggerSortOrder = "DIST";

    protected boolean loadCandidateListLLZ = false;

    /** Default Candidate list name. (= "RCG-TRINET") */
    protected String defaultCandidateListName = "RCG-TRINET";

    /** Named list of channels that will be the candidate list (superset) of
     * channels from which the model will glean it's final list. The string is set
     * with the "candidateListName" property. The concrete class will determine how
     * this is interpreted. It could be a filename.
     * The default implementation uses a group named in the applications table
     * of the dbase associated with channel names in JASI_CONFIG_VIEW.
     * @see: Channel.getNamedChannelList() */

    protected String candidateListName = defaultCandidateListName;
    protected String oldCandidateListName = candidateListName;

    // Restrict timespan of window if true, no coda duration added, only S-arrival+postEvent secs
    protected String windowDurationType = "default"; // added -aww 2010/07/09

    /** Default TravelTime Model
     * @see: TravelTime */
    protected TravelTime tt = null;
    {
        tt = TravelTime.getInstance();
    }

    public ChannelTimeWindowModel() { }

    public ChannelTimeWindowModel(GenericPropertyList gpl, Solution sol, ChannelableList candidateList ) {
        setProperties(gpl);
        this.sol = sol;
        //setSolution(sol); // invokes doSetSolution which generates a new candidate list which is then overridden by input arg
        // Below insures Channel LatLonZ's, and if solution is not null, sorts list by distance
        setCandidateList(candidateList);
    }

/** Sets the Solution and the list of channels from which the algorithm can select channels.*/
    public ChannelTimeWindowModel(Solution sol, ChannelableList candidateList ) {
        this(null, sol, candidateList); // insures LatLonZ for solution origin time if not it's not null
    }
/** Sets the list of channels from which the algorithm can select channels.*/
    public ChannelTimeWindowModel(ChannelableList candidateList ) {
        this(null, null, candidateList);
    }
/** Sets the Solution.*/
    public ChannelTimeWindowModel(Solution sol ) {
        doSetSolution(sol); // builds the default candidate list insuring LatLonZ for solution's origin time if it's not null
    }

    /**
     * Set the TravelTime Model
    */
    public void setTravelTimeModel(TravelTime ttm) {
        tt = ttm;
    }
    /**
     * Get the TravelTime Model
     */
    public TravelTime getTravelTimeModel() {
        return tt ;
    }
    /** Set the descriptive name for the model. */
    public void setModelName(String name) {
        modelName = name;
    }
    /** Get the descriptive name for the model. */
    public String getModelName() {
        return modelName;
    }
    /** Get the Class name for the model. */
    public String getClassname() {
        return getClass().getName();
    }

    /** Set the longer description of the model. This may be used in help,
     *  error messages, etc.*/
    public void setExplanation(String str) {
        explanation = str;
    }
    /** Get the longer description of the model. This may be used in help,
     *  error messages, etc.*/
    public String getExplanation() {
        return explanation;
    }

    /** Don't calculate amps greater then this far away (in km).*/
    public void setMaxDistance(double distKm ) {
        maxDistance = distKm;
    }
    /** Return the maximum distance value. Returns  Double.MAX_VALUE if it
    * was not explicitly set, so it is safe to use.*/
    public double getMaxDistance() {
        return maxDistance;
    }

    /** Calculate include channels less then this far away (in km).*/
    public void setMinDistance(double distKm ) {
        minDistance = distKm;
    }
    /** Return the minimum distance value. Returns  Double.MIN_VALUE if it
    * was not explicitly set, so it is safe to use.*/
    public double getMinDistance() {
        return minDistance;
    }

    /** Return the maximum distance value. Returns  Int.MAX_VALUE if it
    * was not explicitly set, so it is safe to use.*/
    public int getMaxChannels() {
        return maxChannels;
    }

    /** Set the name of the candidate list.*/
    public void setCandidateListName(String newName) {
        oldCandidateListName = candidateListName; // save old name
        candidateListName = newName; // set new name
        if ((oldCandidateListName == null || ! oldCandidateListName.equals(candidateListName)) ) {
            if (candidateList != null) candidateList = null; // Reset list if name changes
        }
    }

    public String getCandidateListName() {
        return candidateListName;
    }

    /** Don't calculate amps greater then this far away (in km).*/
    public void setMaxChannels(int maxChannels ) {
        this.maxChannels = maxChannels;
    }

    /** Returns true if Solution is requiered by the model.*/
    public boolean getRequiresLocation() {
        return requiresLocation;
    }    
    
    /** Set true if the model requires a Solution.     */
    public void setRequiresLocation(boolean tf) {
        requiresLocation = tf;
    }

    /** Returns true if Candidate List should be for TIME of Solution.*/
    public boolean getSynchListBySolution() {
        return synchListBySolution;
    }
    /** Set true if Candidate List should be for TIME of Solution.   */
    public void setSynchListBySolution(boolean tf) {
        synchListBySolution = tf;
    }

    /** Set the Solution to use in the model.
     * If model's candidate list is not empty sorts list by channel distance from solution.
     */
    public void setSolution(Solution sol) {
        doSetSolution(sol);
    }

    private void doSetSolution(Solution sol) {
      this.sol = sol;
      // if solution synched, getCandidateList creates new list (false => no distance sorting inside method).
      if (sol != null && getCandidateList(false) != null) {
          candidateList.distanceSort(sol.getLatLonZ()); // now sort list again
      }
    }

    /** Return the Solution used in the model.*/
    public Solution getSolution() {
        return sol;
    }

    /** Channel/timewindows will only be created for
     * channels in this list. The list is defined by the "candidateListName" property.
     * The concrete class may override this method to change how the named list of
     * channels is created, for example, it could be a filename.
     * The default implementation uses a group named in the applications table
     * of the dbase associated with channel names in JASI_CONFIG_VIEW which can be joined
     * to JASI_CHANNEL_VIEW by SNCL to create the list.
     * @see: Channel.getNamedChannelList() */
    /** Set a candidate ChannelList from which the model will select channels to include.
     *  If there is a allowed channels to include list, channel/timewindows will only be
     *  created for the channels found in this list, otherwise ALL channels are permissible.*/
    public void setCandidateList(ChannelableList candidateListIn) {
        setCandidateList(candidateListIn, true); // <= sorts list by distance, if this.sol is not null
    }

    public void setCandidateList(ChannelableList candidateListIn, boolean sortByDist) {
        if (candidateListIn == null ) {
          candidateList = null; // or new ChannelList(); // empty list -aww
          //if (debug)
              System.out.println("INFO ChannelTimeWindowModel: " + getModelName() + " setCandidateList NULL list");
          return;
        }

        if (candidateListIn.isEmpty()) {
          candidateList = new ChannelList(0);
          //if (debug)
              System.out.println("INFO ChannelTimeWindowModel: " + getModelName() + " setCandidateList EMPTY list");
          return;
        }

        if (candidateListIn instanceof ChannelList) candidateList = candidateListIn;
        // Parse out channels from input list if not already a ChannelList object
        else {
            candidateList = new ChannelList();
            channelableListToChannelList(candidateList, candidateListIn);
        }
        candidateList = filterListByChannelPropertyAttributes(candidateList);

        // make sure all Channels in candidateList have LatLonZ for time 
        if (loadCandidateListLLZ) {
          DateTime time = (sol != null) ? sol.getDateTime() : new DateTime();
          ChannelableList.insureLatLonZinfo(candidateList, time); 
        }
        if ( MasterChannelList.isEmpty()) {
            //if (debug) 
                System.out.println(getModelName() + " DEBUG setCandidateList: MasterChannelList.isEmpty => MasterChannelList.set(candidateList)"); 
            MasterChannelList.set(candidateList);
        }

        if (sol != null && sortByDist) candidateList.distanceSort(sol);
        if (debug) System.out.println(getModelName() + " DEBUG setCandidateList size = " + candidateList.size());
    }

/**
 * Apply all the filter criteria to the list, e.g. included and excluded net, station and channel types.
 * @param inList
 * @return inList
 */
    protected  ChannelableList filterListByChannelPropertyAttributes(ChannelableList inList) {
        ChannelableList outList = inList; // aliased
        //
        // An empty include list means "allow all"
        //
        if (staIncludeList != null && staIncludeList.length > 0) {
           // NOTE: cull of an empty include input list returns NO channels
            System.out.println(getModelName() + " cullByChannelNameAttribute ALLOWED STA " + Arrays.asList(staIncludeList));
          outList =
              (ChannelableList) outList.cullByChannelNameAttribute(staIncludeList, ChannelIdIF.STA, ChannelableListIF.INCLUDE);
        }
        if (netIncludeList != null && netIncludeList.length > 0) {
            System.out.println(getModelName() + " cullByChannelNameAttribute ALLOWED NET " + Arrays.asList(netIncludeList));
            outList =
                (ChannelableList) outList.cullByChannelNameAttribute(netIncludeList, ChannelIdIF.NET, ChannelableListIF.INCLUDE);
        }
        if (chanIncludeList != null && chanIncludeList.length > 0) {
           // NOTE: cull of an empty include input list returns NO channels
            System.out.println(getModelName() + " cullByChannelNameAttribute ALLOWED SEEDCHAN " + Arrays.asList(chanIncludeList));
          outList =
              (ChannelableList) outList.cullByChannelNameAttribute(chanIncludeList, ChannelIdIF.SEEDCHAN, ChannelableListIF.INCLUDE);
        }
        if (locIncludeList != null && locIncludeList.length > 0) {
           // NOTE: cull of an empty include input list returns NO channels
            System.out.println(getModelName() + " cullByChannelNameAttribute ALLOWED LOCATION " + Arrays.asList(locIncludeList));
          outList =
              (ChannelableList) outList.cullByChannelNameAttribute(locIncludeList, ChannelIdIF.LOCATION, ChannelableListIF.INCLUDE);
        }

        // After includes now do REJECT
        if (netRejectList != null && netRejectList.length > 0) {
            System.out.println(getModelName() + " cullByChannelNameAttribute REJECTED NET " + Arrays.asList(netRejectList));
            outList =
                (ChannelableList) outList.cullByChannelNameAttribute(netRejectList, ChannelIdIF.NET, ChannelableListIF.EXCLUDE);
        }
        if (chanRejectList != null && chanRejectList.length > 0) {
           // NOTE: cull of an empty include input list returns NO channels
            System.out.println(getModelName() + " cullByChannelNameAttribute REJECTED SEEDCHAN " + Arrays.asList(chanRejectList));
          outList =
              (ChannelableList) outList.cullByChannelNameAttribute(chanRejectList, ChannelIdIF.SEEDCHAN, ChannelableListIF.EXCLUDE);
        }
        if (locRejectList != null && locRejectList.length > 0) {
           // NOTE: cull of an empty include input list returns NO channels
            System.out.println(getModelName() + " cullByChannelNameAttribute REJECTED LOCATION " + Arrays.asList(locRejectList));
          outList =
              (ChannelableList) outList.cullByChannelNameAttribute(locRejectList, ChannelIdIF.LOCATION, ChannelableListIF.EXCLUDE);
        }
        if (staRejectList != null && staRejectList.length > 0) {
           // NOTE: cull of an empty include input list returns NO channels
            System.out.println(getModelName() + " cullByChannelNameAttribute REJECTED STA " + Arrays.asList(staRejectList));
          outList =
              (ChannelableList) outList.cullByChannelNameAttribute(staRejectList, ChannelIdIF.STA, ChannelableListIF.EXCLUDE);
        }

        return outList;
    }

    /** Return the master ChannelableList from which the model will select channels to include. */
    public ChannelableList getCandidateList() {
        return getCandidateList(true);
    }

    // Returns null only if list name is null, not defined, in case list name is defined/changed later
    private ChannelableList getCandidateList(boolean sortByDist) {
        if (bm != null) {
            bm.setPrintStream(System.out);
            bm.reset();
        }
        if (candidateList == null && useMasterList) {
            //if (debug) 
                bm.printTimeStamp("INFO ChannelTimeWindowModel: " + getModelName() + " getCandidateList() candidateList is MasterList of size:" +
                       ((MasterChannelList.get() != null) ? MasterChannelList.get().size() : 0));
            //setCandidateList(new ChannelList(MasterChannelList.get()), sortByDist); // takes 10 seconds do clone instead -aww 2010/05/28
            setCandidateList((ChannelList)MasterChannelList.get().clone(), sortByDist);
        }
        else if (candidateList == null) {
          if (getCandidateListName() != null) {
            // if (debug)
                bm.printTimeStamp("INFO ChannelTimeWindowModel: " + getModelName() + " getCandidateList() initializing candidateList by name: " +
                    getCandidateListName()); 
            Solution aSol = (synchListBySolution) ?  sol : null;
            DateTime time = (aSol != null) ? aSol.getDateTime() : new DateTime();
            setCandidateList(Channel.create().getNamedChannelList(getCandidateListName(), time), sortByDist);  // added time 09/20/2007
            oldSol = aSol;
          }
        }
        else if (oldSol != sol && synchListBySolution ) {
            DateTime time = (sol != null) ? sol.getDateTime() : new DateTime();
            if (debug)
                bm.printTimeStamp(getModelName() + " model: DEBUG getCandidateList() initializing list by name: " +
                    getCandidateListName() + " because solution datetime changed, sol.datetime=" + time + " at");
            setCandidateList(Channel.create().getNamedChannelList(getCandidateListName(), time), sortByDist); // added time 09/20/2007
            oldSol = sol;
        }
        //else candidateList.distanceSort(sol);  // need to revise subclasses method call signature to active this option -aww

        if (debug) {
            bm.printTimeStamp(getModelName() + " model: DEBUG getCandidateList() returned candidateList of size: " + candidateList.size()); 
        }

        return candidateList;
    }

    /** Set/define the list of Channel objects that MUST be included in the final model list.
     * Begin with the passed list parse out the Channel objects and add any others based
     *  on config parameters * e.g. 'includePhase = true'). <p>
     * The required list will lat/lon/z  .*/
    public void setRequiredChannelList(ChannelableList requiredChannelList) {

        try {
          if (requiredChannelList == null) {
              throw new NullPointerException(getModelName() + " model: ERROR: list input to method setRequiredChannelList is null !");
          }
        }
        catch (NullPointerException ex) {
            System.err.println(ex.getMessage());
            requiredList.clear();
            return;
        }

        requiredList = requiredChannelList;

        // Check to see if input list is empty or not list of channels
        if ( ! requiredList.isEmpty() && ! (requiredList instanceof ChannelList) ) {
            // lazy check for Channels, assume all are like first
            if ( ! (requiredList.get(0) instanceof Channel)) { // then parse channels from the input list 
              requiredList = new ChannelableList();
              channelableListToChannelList(requiredList, requiredChannelList);
            }
        }

        /* If you don't cast to (ChannelableListIF) the final list is a MIX of
         * Objects (Amps, Phases, etc.) and duplicate channels are not filtered out.*/
        if (sol != null ) {
          if (getIncludePhases()) {
            if (debug) System.out.println("DEBUG " + getModelName() + " setRequiredChannelList adding phases to required list of size: " + requiredChannelList.size() + " at: " + new DateTime()); 
            sol.listenToPhaseListChange(false);
            if (sol.getPhaseList().isEmpty()) sol.loadPhaseList(); // load if empty list, else use current list -aww 2008/02/21
            sol.listenToPhaseListChange(true);
            channelableListToChannelList(requiredList, sol.getPhaseList());
          }
          if (getIncludePeakAmps()) { // all amps associated with Origin, should include mag amps, but RT code not making AssocAmO rows -aww
            if (debug) System.out.println("DEBUG " + getModelName() + " setRequiredChannelList adding peak amps to required list of size: " + requiredChannelList.size() + " at: " + new DateTime()); 
            if (sol.getAmpList().isEmpty()) sol.loadAmpList(); // load if empty list, else use current list -aww 2008/02/21
            channelableListToChannelList(requiredList, sol.getAmpList());
          }
          else if (getIncludeMagAmps()) { // only amps associated with prefmag Ml
            if (debug) System.out.println("DEBUG " + getModelName() + " setRequiredChannelList adding mag amps to required list of size: " + requiredChannelList.size() + " at: " + new DateTime()); 
            boolean state = Solution.listenToMagReadingListAdd;
            Solution.listenToMagReadingListAdd = false;
            if (sol.getAmpList().isEmpty()) sol.loadPrefMagAmpList(true); // load if empty list, else use current list -aww 2008/02/21
            Solution.listenToMagReadingListAdd = state;
            channelableListToChannelList(requiredList, sol.magnitude.getAmpList());
          }
          if (getIncludeMagCodas()) { // only coda associated with prefmag Md
            if (debug) System.out.println("DEBUG " + getModelName() + " setRequiredChannelList adding codas to required list of size: " + requiredChannelList.size() + " at: " + new DateTime()); 
            boolean state = Solution.listenToMagReadingListAdd;
            if (sol.getCodaList().isEmpty()) sol.loadPrefMagCodaList(true); // load if empty list, else use current list -aww 2008/02/21
            Solution.listenToMagReadingListAdd = state;
            channelableListToChannelList(requiredList, sol.getCodaList());
          }
        }

        if (debug) System.out.println("DEBUG " + getModelName() + " setRequiredChannelList insureLatLonZinfo for required list of size: " + requiredChannelList.size() + " at: " + new DateTime()); 
        // Now get lat,lon info then calc distance and sort by distance if solution is known
        // This may take a long time, quicker to use a master list cache with LatLonZ preloaded
        //if (loadCandidateListLLZ) {
            ChannelableList.insureLatLonZinfo(requiredList, ((sol != null) ? sol.getDateTime() : new DateTime()));
        //}
        if (sol != null)  requiredList.distanceSort(sol);
        if (debug) bm.printTimeStamp("DEBUG " + getModelName() + " setRequiredChannelList done,");
    }

    /** Given any ChannelableList, like a list of Amps or Phases return a list
     * of Channel objects - Just the name, lat/lon/z, etc.*/
    protected void channelableListToChannelList(ChannelableList outList, ChannelableListIF inList) {
      int count = inList.size();
      outList.ensureCapacity(outList.size()+count);
      Channelable chn = null;
      for (int i=0; i<count; i++) {
        chn = (Channelable) inList.get(i);
        if (! outList.hasChannelId(chn)) outList.add(chn.getChannelObj()); // add only if channel id not already in list -aww 2009/01/05
      }
    }

    /** Return the list of channels that MUST be included in the final model list.*/
    public ChannelableList getRequiredChannelList() {
        return requiredList;
    }

    protected void addRequiredChannelsToList(ChannelableList aList) {
        // loads and sorts include reading channels by distance
        if (debug) System.out.println("DEBUG " + getModelName() + " BEGIN addRequiredChannels to input list of size: " + aList.size()); 
        setRequiredChannelList((includeAllComponents) ? new ChannelableSetList() : new ChannelableList());
        Channelable ch = null;
        boolean added = false;
        for (int idx = 0; idx < requiredList.size(); idx++) {
            ch = (Channelable) requiredList.get(idx);
            // only include those in candidate list
            if (include(ch) && !aList.hasChannelId(ch)) { // add only if channel id not already in list -aww 2009/01/05
                added = aList.add(ch.getChannelObj());
                if (debug) {
                  System.out.println("DEBUG " + getModelName() + " added: " + added + " channel: " +
                            ch.getChannelObj().toDelimitedSeedNameString() + " size: " + aList.size());
                }
            }
        }
        if (sol != null) aList.distanceSort(sol); // resort for added channels
        if (debug) System.out.println("DEBUG " + getModelName() + " END  addRequiredChannels returns with list size: " + aList.size()); 
    }

    /**
    * Cull any channels that were not "active" on this true UTC seconds time.
    * NOOP - STUB
    */
    protected void cullChannelSetByTime(double trueEpochSecs) {
        /// getChannelList().cull...
    }

    /** If true, include all components (orientations) of a channel type if any is included. */
    public boolean getIncludeAllComponents() {
        return includeAllComponents;
    }
    /** If true, include all components (orientations) of a channel type if any is included. */
    public void setIncludeAllComponents(boolean tf ) {
        includeAllComponents = tf;
    }
    /** Return the mag value for which all channels will be included.  */
    public double getIncludeAllMag() {
        return includeAllMag;
    }
    /** Include all channels if the magnitude equals or exceeds this value.*/
    public void setIncludeAllMag(double mag ) {
        includeAllMag = mag;
    }

    /** Return the mag value to assume for channel window distance energy cutoff estimation when a solution has no magnitude.  */
    public double getDefaultNullMag() {
        return defaultNullMag;
    }
    /** Default magnitude to assume if input solution has no magnitude. */
    public void setDefaultNullMag(double mag ) {
        defaultNullMag = mag;
    }

    public String getTriggerSortOrder() {
        return triggerSortOrder;
    }

    /**
    * The channel list set by setCandidateList() will be filtered to include only
    * those that match the channels in this array of channelname strings.
    * If the chanList is null or empty ALL channels are allowed.
    * Wildcards are not allowed.*/
    public void setAllowedStaTypes(String[] staList) {
      staIncludeList = staList;
    }
    public void setAllowedChannelTypes(String[] chanList) {
      chanIncludeList = chanList;
    }
    public void setAllowedLocationTypes(String[] chanList) {
      locIncludeList = chanList;
    }
    public void setAllowedNetTypes(String[] netList) {
      netIncludeList = netList;
    }
    public void setRejectedStaTypes(String[] staList) {
      staRejectList = staList;
    }
    public void setRejectedChannelTypes(String[] chanList) {
      chanRejectList = chanList;
    }
    public void setRejectedLocationTypes(String[] chanList) {
      locRejectList = chanList;
    }
    public void setRejectedNetTypes(String[] netList) {
      netRejectList = netList;
    }

    /** Set the minimum and maximum window size (seconds) that can be returned.*/
    public void setWindowLimits(double minSize, double maxSize) {
        setMinWindowSize(minSize);
        setMaxWindowSize(maxSize);
    }
    public void setMaxWindowSize(double maxSize) {
        maxWindow = maxSize;
    }
    public void setMinWindowSize(double minSize) {
        minWindow = minSize;
    }
    public double getMaxWindowSize() {
        return maxWindow;
    }
    public double getMinWindowSize() {
        return minWindow;
    }

    public double getStartTime() {
        return starttime;
    }  

    public void setStartTime(double dt) {
        starttime = dt;
    }
  
    /** 
     * Set the value of the static time window used for all CTW's
     *
     * @param ts
     */
    public void setFixedTimeWindow (TimeSpan ts) {
        fixedTimeWindow = ts;
    }
  
    /** 
     * Return fix Time window, if any.
     * @return NULL if none set.
     */
    public TimeSpan getFixedTimeWindow() {

      return fixedTimeWindow;

    }
    // tau = 10 ** ((mag - magIntercept - dist*magDistSlope - depth*magDepthSlope) / magTauSlope) 
    public void setMagTauIntercept(double val) {
        magTauIntercept = val;
    }
    public double getMagTauIntercept() {
        return magTauIntercept;
    }
    public void setMagTauSlope(double val) {
        magTauSlope = val;
    }
    public double getMagTauSlope() {
        return magTauSlope;
    }
    /*
    public void setMagDepthSlope(double val) {
        magDepthSlope = val;
    }
    public double getMagDepthSlope() {
        return magDepthSlope;
    }
    public void setMagDistSlope(double val) {
        magDistSlope = val;
    }
    public double getMagDistSlope() {
        return magDistSlope;
    }
    */

    public void setIncludePhases( boolean tf) {
        includePhases = tf;
    }
    public boolean getIncludePhases() {
        return includePhases;
    }
    public void setIncludeMagAmps( boolean tf) {
        includeMagAmps = tf;
    }
    public boolean getIncludeMagAmps() {
        return includeMagAmps;
    }
    public void setIncludePeakAmps( boolean tf) {
        includePeakAmps = tf;
    }
    public boolean getIncludePeakAmps() {
        return includePeakAmps;
    }
    public void setIncludeMagCodas( boolean tf) {
        includeCodas = tf;
    }
    public boolean getIncludeMagCodas() {
        return includeCodas;
    }

    public void setIncludeDataSourceWf( boolean tf) {
        includeDataSourceWf = tf;
    }
    public boolean getIncludeDataSourceWf() {
        return includeDataSourceWf;
    }

    /** When set true, the list returned by getWaveformList() are only those AssocWaE channels
     * that are also found in the set candidate list. Those not found in the candidate
     * list are rejected.
     */
    public void setFilterWfListByChannelList(boolean tf) {
       filterWfListByChannelList = tf;
    }
    /** If true, the list returned by getWaveformList() are only those AssocWaE channels
     * that are also found in the set candidate list. Those not found in the candidate
     * list are rejected.
     */
    public boolean getFilterWfListByChannelList() {
      return filterWfListByChannelList;
    }


    /** Set the value of the amount of time to save before onset of energy. */
    public void setPreEventSize(double seconds) {
        preEvent = seconds;
    }

    /** Set the value of the amount of time to save after the decay of energy. */
    public void setPostEventSize(double seconds) {
      postEvent = seconds;
    }
    /** Get the value of the amount of time to save before onset of energy. */
    public double getPreEventSize() {
        return preEvent;
    }

    /** Get the value of the amount of time to save after the decay of energy. */
    public double getPostEventSize( ) {
      return postEvent;
    }

    /** Given a distance, return the TimeSpan (time window) at this distance,
     * using traveltime model to calculate predicted phase arrival times. 
     * If windowDurationType="coda" the default, the window start and end time are predicted values for:<br>
     * P-arrival time - preEventSize<br>
     * P-arrival time + postEventSize + coda duration seconds <br>
     * when type is "P2S", the coda duration seconds is NOT added to the end time.
     * when type is "amp", the end time is P-arrival Time + wfSmpWindowMultiplier*(sTime-pTime) + postEventSize 
     */
    public TimeSpan getTimeWindow(double dist) {

        tt.setSource(sol); // assumes TravelTime.setSource(Solution) uses model depth -aww 2015/10/10

        double start = 0.0;
        double end   = 0.0;

        double ot = sol.datetime.doubleValue();

        DoubleRange ps = tt.getTTps(dist);

        double pTime = ot + ps.getMinValue();
        double sTime = ot + ps.getMaxValue();

        // window around arrivals
        start = pTime - preEvent;
        end   = sTime + postEvent;

        // NOTE: by convention, tau duration is measured from pTime
        if (!windowDurationType.equalsIgnoreCase("p2s")) { // added condition - aww 2010/06/22
            end = (windowDurationType.equalsIgnoreCase("amp")) ?
                pTime + AbstractWaveform.smpWindowMultiplier*(sTime-pTime) + postEvent : pTime + getCodaDuration() + postEvent;
        }

        double duration = end - start;
        if (debug)  {
          Format df = new Format("%6.2f");
          System.out.println("dist: " + df.form(dist) +" ot: "+ot+
                 " pTime: "+df.form(pTime-ot)+ " sTime: "+df.form(sTime-ot)
                 +" duration: " +df.form(duration)+
                " start: " +LeapSeconds.trueToString(start)+ " end: " + LeapSeconds.trueToString(end)); // for UTC datetime -aww 2008/02/12
        }

        // check extremes
        if (duration > maxWindow) {
          end = start+maxWindow;
        } else if (duration < minWindow) { // extends beyond end of event energy ?
          end = start+minWindow;
        }

        return new TimeSpan(start, end);

    }

    /** Return the coda duration in seconds for an event of this magnitude.
     * This is based on the straight line fit of a plot of
     * ML vs log duration (tau). The equation is:<p>
     * <tt>
      log10 (tau) = (mag - intercept) / slope
      Emperical data from southern california yields default values of:
      intercept = -0.95 and slope = 2.15.
     * </tt>*/
    public double getCodaDuration(double mag) {
        //double value = Math.pow(10.0, ((mag - getMagTauIntercept())/getMagTauSlope()));
        //System.out.println("DEBUG CTWM getCodaDuration() value: " +value+ " mag: " +sol.magnitude.value.doubleValue() +
        //        "  "+ getMagTauIntercept() +" "+ getMagTauSlope());
        //return value;
        return Math.pow(10.0, ((mag - getMagTauIntercept())/getMagTauSlope()));
    }

    public double getCodaDuration(Solution sol) {
        return (sol.magnitude == null || sol.magnitude.hasNullType() || sol.magnitude.hasNullValue()) ?
            100. : getCodaDuration(sol.magnitude.value.doubleValue());
    }

    /** Return the coda duration in seconds for an event of the magnitude of
     * the "set" solution.  Returns 100.0 if the event magnitude is not defined.
     * Otherwise the value is based on the straight line fit of a plot of
     * ML vs log duration (tau). The equation is:<p>
     * <tt>
      log10(tau) = (mag - intercept) / slope

      Emperical data from southern california yields default values of:
      intercept = -0.95 and slope     = 2.15
     * </tt>*/
    public double getCodaDuration() {
        return getCodaDuration(sol);
    }

    /** Return a Collection of ChannelTimeWindow objects selected by the model.
     * Default implementation Expects a non-null instance Solution and candidate channel list members
     * set by constructor or method invocation, subclasses should override this method as needed
     * to change list criteria.
     * @see ChannelTimeWindow
     * @see #setSolution(Solution)
     * @see #setCandidateList(ChannelableList)
     * @see #setCandidateListName(String)
     * @see #getCandidateList()
     * */
    public ChannelableList getChannelTimeWindowList() {

      if (starttime == 0. && sol == null) {
        System.err.println(getModelName() + " : ERROR solution reference is null and no start time defined for windows, no-op");
        System.err.println(getModelName() + " : ERROR Required solution reference is null, no-op");
        return null;
      }

      // if it's name is set, and list is null, this creates candidate list by default
      ChannelableList ctwCandidateList = getCandidateList(); // may be null if not defined
      if (debug) System.out.println("DEBUG " + getModelName() + " ctwCandidateList size: " + ctwCandidateList.size());

      if (ctwCandidateList == null) {
        System.err.println(getModelName() + " : ERROR candidate channel list is null.");
        return null;
      }

      if (ctwCandidateList.isEmpty()) {
        System.err.println(getModelName() + " : ERROR? candidate channel list is empty.");
        return new ChannelableList(0);
      }
      // candidateList is auto sorted when model solution is changed -aww 09/25/2007
      //ctwCandidateList.distanceSort(sol);

      ChannelableList newList = new ChannelableList(ctwCandidateList);
      addRequiredChannelsToList(newList); // does dist sort
      int count = newList.size();

      // NOTE: In implementation of subclasses it may be needed when 
      // includeAllComponents=true to force inclusion of all orientations
      // of channel type if any is added.
      // When "filterWfListByChanelList = true" only components are 
      // already in the candidateList are kept. Otherwise implemenation 
      // may need to address "phantom" channels not in the database.
      ChannelableList outList = (getIncludeAllComponents()) ?
          new ChannelTimeWindowSetList(count) : new ChannelableList(count);
      if (debug) System.out.println("DEBUG " + getModelName() + " # channels to test for distance: " + count);
      if (count == 0) return outList;

      double cutoff = getMaxDistance();
      double dist = 0.;
      Channelable ch = null;
      for (int i=0; i< count; i++) {
          ch = (Channelable) newList.get(i);
          dist = ch.getHorizontalDistance(); // aww 06/11/2004
          if (dist > cutoff) {
            if ( !sol.isEventJasiType(EventTypeMap3.getMap().toJasiType(EventTypeMap3.TRIGGER)) || triggerSortOrder.equalsIgnoreCase("DIST") ) {
              if (debug) System.out.println("DEBUG " + getModelName() + " break at cutoff of : "+ cutoff +" at "+ ch.getChannelObj().getChannelId());
              break;    // hit the cutoff dist, bail
            }
          }
          
          outList.add( new ChannelTimeWindow(ch.getChannelObj(), getTimeWindow(dist)) );
      }

      outList = filterListByChannelPropertyAttributes(outList);
      if (filterWfListByChannelList)  filterListByChannelList(outList);

      return (outList.size() > getMaxChannels()) ? new ChannelableList(outList.subList(0,getMaxChannels())) : outList;
    }

    /** Return a Collection of Waveform objects selected by the model.
     * The Waveforms will not contain time-series yet. To load time-series you must set
     * the loader mode with a call to AbstractWaveform.setWaveDataSource(Object) then 
     * invoke the loadTimeSeries() of each Waveform instance.<p>
     * @return null when error occurs creating list
     * @see AbstractWaveform
     */
    public List getWaveformList() {

      // getChannelTimeWindowList() is abstract :. it's where the concrete instance kicks in
      // assumes that maxdist filter would have been done in concrete subclass implementation
      if (debug) bm.printTimeStamp(getModelName() + " DEBUG getWaveformList() calling getChannelTimeWindowList,");
      ChannelableList ctwList = getChannelTimeWindowList();

      if (ctwList == null) return null; // must be error

      // Manufacture new Waveform from ChannelTimeWindow, limit list size to user's requested maximum
      int count = ctwList.size();
      if (debug) System.out.println(getModelName() + " DEBUG getWaveformList() initial channel time window count : " + count + " at: " + new DateTime());
      
      ChannelableList wfList = new ChannelableList(count);

      Waveform wf = null;

      if (count > 0) {
        ChannelTimeWindow ctw = null;
        for (int i = 0; i < count; i++) {
          ctw = (ChannelTimeWindow) ctwList.get(i);
          wf = AbstractWaveform.createDefaultWaveform();
          wf.setChannelObj(ctw.getChannelObj());
          wf.setTimeSpan(ctw.getTimeSpan());
          wf.setAmpUnits(Units.COUNTS); // default to this - aww 09/08/2006
          wfList.add(wf);

        }
      }

      ArrayList solList = null;
      if (includeDataSourceWf) { // For some models getChannelTimeWindowList() does a candidateList filtering before here -aww
          // Update filtered list of ChannelTimeWindow Waveform with DataSource AssocWaE file data
          solList = (ArrayList) AbstractWaveform.createDefaultWaveform().getBySolution(sol);
          count = (solList == null) ?  0 : solList.size();
          if (count > 0) {
              // Replace original waveform in list with the solution's waveform that matching channel
              // and set it's timespan to that of the original ChannelTimeWindow (doesn't add to list)
              // NOTE: resetting timespan here does NOT trim the wavelets
              Waveform solWf = null;
              int idx = -1;
              for (int i = 0; i < count; i++) {
                  solWf = (Waveform) solList.get(i);
                  idx = wfList.getIndexOf(solWf);
                  if (idx >= 0) {
                      //System.out.println("chan: "+solWf.getChannelObj().toDelimitedSeedNameString()+
                      //" old: "+solWf.getTimeSpan()+" new : "+((Waveform) wfList.get(idx)).getTimeSpan());
                      wf = (Waveform) wfList.get(idx);
                      // Note this does not change end times in the wavelets group, doesn't trim or expand wavelet time bounds
                      solWf.setTimeSpan(wf.getTimeSpan());
                      //solWf.setSegmentList(solWf.getSubSegments(wf.getStart().doubleValue(),wf.getEnd().doubleValue()), true); // reset packet spans
                      wfList.set(idx, solWf); // replace Solution's in original list
                  }
              }
          }
      }
      //System.out.println(getClass().getName() + " filterWfListByChannelList="+filterWfListByChannelList);
      //REMOVED below filter, since done in getChannelTimeWindowList() -aww 2008/09/26
      //wfList = (filterWfListByChannelList) ?  filterListByChannelList(wfList) : filterListByChannelPropertyAttributes(wfList);
      return (wfList.size() > maxChannels) ? new ChannelableList(wfList.subList(0, maxChannels)) : wfList;
    }

    /** Return a Collection of Waveform objects selected by the model whose
     * channels are in the input list.
     * The Waveforms will not contain time-series yet. To load time-series you set
     * the loader mode with a call to AbstractWaveform.setWaveDataSource(Object) then 
     * invoke the loadTimeSeries() of each Waveform instance.<p>
     *
     * @see AbstractWaveform
     */
    public List getWaveformList(Solution sol, ChannelableList candidateList) {
        if (candidateList == null) {
          System.err.println(getModelName() + " : ERROR getWaveformList input candidateList is null");
          return null;
        }
        setCandidateList(candidateList, false); // do not sort by distance

        return getWaveformList(sol);
    }

    /** Return a Collection of Waveform objects selected by the model using 
    * this model's candidate channel list.
    * The Waveforms will not contain time-series yet. To load time-series you must set
    * the loader mode with a call to AbstractWaveform.setWaveDataSource(Object) then 
    * invoke the loadTimeSeries() of each Waveform instance.<p>
    *
    * @see AbstractWaveform
    */
    public List getWaveformList(Solution sol) {
        if (sol == null) {
          System.err.println(getModelName() + " : ERROR getWaveformList input solution is null");
          return null;
        }
        setSolution(sol);

        List aList = getWaveformList();

        if (debug) System.out.println("DEBUG>>> ChannelTimeWindowModel getWaveformList(sol) : " + sol.getId().longValue() + " list size: " + aList.size());

        if (aList == null || aList.size() == 0) return aList; // no data, just return -aww 2010/08/10

        // Set the waveform's associated evid so that it can be used for waveform cache file 
        AbstractWaveform wf = null;
        for (int ii = 0; ii<aList.size(); ii++) {
            wf = (AbstractWaveform) aList.get(ii);
            if (wf.getSavingEvent() == null) {
                wf.setSavingEvent(String.valueOf(sol.getId().longValue())); // for wf disk file cache
            }
        }
        return aList;
    }

    protected boolean includeRequired(Channelable chn) {
      ChannelableList testCandidateList = (candidateList == null) ? new ChannelableList(0) : candidateList;
      ChannelableList testRequiredList  = (requiredList == null) ? new ChannelableList(0) : requiredList;
      if (testCandidateList.isEmpty()) return true;
      if (testRequiredList.hasChannelId(chn)) return true;
      if (testCandidateList.hasChannelId(chn)) return true;
      return false;
    }

    /** Return true if input Channelable is in either the required list or the candidate List,
     *  or the candidate list is null or empty.
     */
    protected boolean include(Channelable chn) {
      if (candidateList == null || candidateList.isEmpty()) return true;
      boolean hasChn = candidateList.hasChannelId(chn);
      if ( debug && !hasChn && chn != null ) {
        System.out.println("DEBUG: " + getModelName() + " channel not in candidateList: " + chn.getChannelObj().toDelimitedSeedNameString());
      }
      return hasChn;
    }

    /** Return the subset of 'inList' whose Channelable members matches those in the list set by
     *  setCandidateList(). This enforces the rule that returned Channelables
     *  must be in the candidate list.
     *  @see setCandidateList() */
    protected ChannelableList filterListByChannelList(ChannelableList inList) {

        if (candidateList == null || candidateList.isEmpty()) {
            System.out.println("INFO ChannelTimeWindowModel: " + getModelName() + " filterListByChannelList candidateList is empty => no filtering");
            return inList;
        }
        if (inList == null) return inList;
        int count = inList.size();
        // For new list use active subtype of List, we don't want duplicate adds from input list
        ChannelableList newList = new ChannelableList(count);
        Channelable ch = null;
        for (int i=0; i< count; i++) {
          ch = (Channelable) inList.get(i);
          if (include(ch)) newList.add(ch);
        }
        return (newList.size() > maxChannels) ? new ChannelableList(newList.subList(0,maxChannels)) : newList;
    }

    /** Return an instance of the named ChannelTimeWindowModel class. Returns
     *  null if instance cannot be created or is not an extension of
     *  ChannelTimeWindowModel.*/
    public static ChannelTimeWindowModel getInstance(String className) {
        try {
          Object obj = Class.forName(className).newInstance();
          if (obj instanceof ChannelTimeWindowModel) {
            return (ChannelTimeWindowModel) obj;
          } else {
            return null;
          }
        } catch (Exception ex) {
          ex.printStackTrace();
          return null;
        }
    }
    /** Two ChannelTimeWindowModel's are considered equal if they have the same name.*/
    public boolean equals(Object obj) {
        if ( !(obj instanceof ChannelTimeWindowModel) ) return false;
        ChannelTimeWindowModel other = (ChannelTimeWindowModel) obj;
        return other.getModelName().equals(getModelName()) ;
    }
    /** Return a Collection of String objects based on the contents
     *  of the property string. The string is a series of fully qualified class
     *  names and has the form:<p>
     *
     *  "org.trinet.jasi.ThisIsaModel, org.trinet.otherstuff.AnotherModel"
     *
     * The actual class objects are instantiated with
     *   ChannelTimeWindowModel.getInstance(String).
     *  */
    public static AbstractCollection parsePropertyString(String string) {

        if (string == null) return null;

        ActiveList list = new ActiveList();
        String delList = ",:;" ;  // delimiter list
        StringTokenizer strTok = new StringTokenizer(string, delList);

         try {
              while (strTok.hasMoreTokens()) {
                list.add(strTok.nextToken().trim());
              }
         } catch (Exception ex) {
              ex.printStackTrace();
              System.err.println("Bad syntax in property string:" +string);
              return null;
         }

         return (AbstractCollection) list;

    }

    /** Returns ChannelTimeWindowModel's name. This is used produce a string to
     *  be displayed in a JComboBox, for instance. */
    public String toString() {
      return getModelName();
    }

    /** Return a prefix string that is prepended to every property for this class.
     * @see:  setProperties() */
    public String getPropertyPrefix() {
      return getClassname()+".";
    }
    /**
     * Set debug flag
     * @param tf
     */
    public void setDebug(boolean tf) {
        debug = tf;
        if (debug) {
            System.out.println("INFO ChannelTimeWindowModel: " + getModelName() + " set debug = " + tf);
        }
    }

    public boolean getDebug() {
        return debug;
    }
/** Setup model behavior parameters using values in a property list.
 * Assumes each property has a prefix matching the string returned by getClassname().
 * Override this to set properties specific to a model.
 * In the new method call super.setProperties(props) first to get the standard stuff.<p>
 * For example, you have a class called org.trinet.stuff.MyCoolModel,
 * a list of properties would defined using the template:<br>
 * <i> org.trinet.stuff.MyCoolModel.aModelProperty=myCustomValue </i> <br>
 */
    public void setProperties(GenericPropertyList props) {

      setDefaultProperties();
      if (props == null) return;

      String pre = getPropertyPrefix();

      if (props.isSpecified(pre+"debug") )
          setDebug(props.getBoolean(pre+"debug"));
      
      if (props.isSpecified(pre+"modelName") )
          setModelName(props.getProperty(pre+"modelName"));
      if (props.isSpecified(pre+"modelExplanation") )
          setExplanation(props.getProperty(pre+"modelExplanation"));

      double val = props.getDouble(pre+"minDistance");
      if (!Double.isNaN(val)) setMinDistance(val);
      val = props.getDouble(pre+"maxDistance");
      if (!Double.isNaN(val)) setMaxDistance(val);

      val = props.getDouble(pre+"maxChannels");
      if (!Double.isNaN(val)) setMaxChannels((int)val);

      val = props.getDouble(pre+"defaultNullMag");
      if (!Double.isNaN(val)) setDefaultNullMag(val);

      val = props.getDouble(pre+"includeAllMag");
      if (!Double.isNaN(val)) setIncludeAllMag(val);

      val = props.getDouble(pre+"postEventSize");
      if (!Double.isNaN(val)) setPostEventSize(val);
      val = props.getDouble(pre+"preEventSize");
      if (!Double.isNaN(val)) setPreEventSize(val);

      val = props.getDouble(pre+"minWindowSize");
      if (!Double.isNaN(val)) setMinWindowSize(val);
      val = props.getDouble(pre+"maxWindowSize");
      if (!Double.isNaN(val)) setMaxWindowSize(val);

      val = props.getDouble(pre+"magTauSlope");
      if (!Double.isNaN(val)) setMagTauSlope(val);
      val = props.getDouble(pre+"magTauIntercept");
      if (!Double.isNaN(val)) setMagTauIntercept(val);
      //val = props.getDouble(pre+"magDistSlope");
      //if (!Double.isNaN(val)) setMagDistSlope(val);
      //val = props.getDouble(pre+"magDepthSlope");
      //if (!Double.isNaN(val)) setMagDepthSlope(val);

      if (props.isSpecified(pre+"includeAllComponents") )
          setIncludeAllComponents(props.getBoolean(pre+"includeAllComponents"));

      if (props.isSpecified(pre+"includePhases") )
          setIncludePhases(props.getBoolean(pre+"includePhases"));
      if (props.isSpecified(pre+"includeMagAmps") )
          setIncludeMagAmps(props.getBoolean(pre+"includeMagAmps"));
      if (props.isSpecified(pre+"includePeakAmps") )
          setIncludePeakAmps(props.getBoolean(pre+"includePeakAmps"));
      if (props.isSpecified(pre+"includeCodas") )
          setIncludeMagCodas(props.getBoolean(pre+"includeCodas"));

      if (props.isSpecified(pre+"includeDataSourceWf") )
          setIncludeDataSourceWf(props.getBoolean(pre+"includeDataSourceWf"));

      if (props.isSpecified(pre+"candidateListName") )
          setCandidateListName(props.getProperty(pre+"candidateListName"));

      if (props.isSpecified(pre+"allowedStaTypes") )
          setAllowedStaTypes(props.getStringArray(pre+"allowedStaTypes"));

      if (props.isSpecified(pre+"allowedSeedChanTypes") )
          setAllowedChannelTypes(props.getStringArray(pre+"allowedSeedChanTypes"));

      if (props.isSpecified(pre+"allowedLocationTypes") ) {
          String [] mylocs = props.getStringArray(pre+"allowedLocationTypes");
          for (int i=0; i<mylocs.length; i++) {
              mylocs[i]=mylocs[i].replace('-',' ');
          }
          setAllowedLocationTypes(mylocs);
      }

      if (props.isSpecified(pre+"allowedNetTypes") )
          setAllowedNetTypes(props.getStringArray(pre+"allowedNetTypes"));

      if (props.isSpecified(pre+"rejectedStaTypes") )
          setRejectedStaTypes(props.getStringArray(pre+"rejectedStaTypes"));

      if (props.isSpecified(pre+"rejectedSeedChanTypes") )
          setRejectedChannelTypes(props.getStringArray(pre+"rejectedSeedChanTypes"));

      if (props.isSpecified(pre+"rejectedLocationTypes") ) {
          String [] mylocs = props.getStringArray(pre+"rejectedLocationTypes");
          for (int i=0; i<mylocs.length; i++) {
              mylocs[i]=mylocs[i].replace('-',' ');
          }
          setRejectedLocationTypes(mylocs);
      }

      if (props.isSpecified(pre+"rejectedNetTypes") )
          setRejectedNetTypes(props.getStringArray(pre+"rejectedNetTypes"));

      if (props.isSpecified(pre+"startTime") )
          setStartTime(props.getDouble(pre+"startTime"));

      if (props.isSpecified(pre+"synchCandidateListBySolution")) 
          synchListBySolution = props.getBoolean(pre+"synchCandidateListBySolution");
    
      if (props.isSpecified(pre+"filterWfListByChannelList"))
          setFilterWfListByChannelList(props.getBoolean(pre+"filterWfListByChannelList"));

      if (props.isSpecified(pre+"useMasterListCandidateList"))
          useMasterList = props.getBoolean(pre+"useMasterListCandidateList");
      
      if (props.isSpecified(pre+"requiresLocation") )    // DDG 1/29/10
          this.setRequiresLocation(props.getBoolean(pre+"requiresLocation"));

      // default, amp, coda, or p2s (time span from P-preEvent to S+postEvent, does not include coda/amp span seconds)
      if (props.isSpecified(pre+"windowDurationType") )
          windowDurationType = props.getProperty(pre+"windowDurationType", "default");

      if (props.isSpecified(pre+"triggerSortOrder") )
          triggerSortOrder = props.getProperty(pre+"triggerSortOrder");

      if (props.isSpecified(pre+"loadCandidateListLLZ") )
          loadCandidateListLLZ = props.getBoolean(pre+"loadCandidateListLLZ");

      if (debug) props.dumpProperties();

    }

    /** Returns a human-readable list of the current parameter settings. */
    public String getParameterDescriptionString() {
      Format df81 = new Format("%8.2f");
      StringBuffer sb = new StringBuffer(256);
      String pre = getPropertyPrefix();
      sb.append(pre).append("modelName = ").append(getModelName()).append("\n");
      sb.append(pre).append("modelExplanation = ").append(getExplanation()).append("\n");
      sb.append(pre).append("debug = ").append(getDebug()).append("\n");
      sb.append(pre).append("minDistance = ").append(df81.form(getMinDistance())).append("\n");
      sb.append(pre).append("maxDistance = ").append(df81.form(getMaxDistance())).append("\n");
      sb.append(pre).append("minWindowSize = ").append(df81.form(getMinWindowSize())).append("\n");
      sb.append(pre).append("maxWindowSize = ").append(df81.form(getMaxWindowSize())).append("\n");
      //sb.append(pre).append("magDepthSlope = ").append(df81.form(getMagDepthSlope())).append("\n");
      //sb.append(pre).append("magDistSlope = ").append(df81.form(getMagDistSlope())).append("\n");
      sb.append(pre).append("magTauIntercept = ").append(df81.form(getMagTauIntercept())).append("\n");
      sb.append(pre).append("magTauSlope = ").append(df81.form(getMagTauSlope())).append("\n");
      sb.append(pre).append("preEventSize = ").append(df81.form(getPreEventSize())).append("\n");
      sb.append(pre).append("postEventSize = ").append(df81.form(getPostEventSize())).append("\n");
      sb.append(pre).append("defaultNullMag = ").append(df81.form(getDefaultNullMag())).append("\n");
      sb.append(pre).append("includeAllMag = ").append(df81.form(getIncludeAllMag())).append("\n");
      sb.append(pre).append("includeAllComponents = ").append(getIncludeAllComponents()).append("\n");
      sb.append(pre).append("requiresLocation= ").append(getRequiresLocation()).append("\n");
      sb.append(pre).append("maxChannels = ").append(getMaxChannels()).append("\n");
      sb.append(pre).append("includePhases = ").append(getIncludePhases()).append("\n");
      sb.append(pre).append("includeMagAmps = ").append(getIncludeMagAmps()).append("\n");
      sb.append(pre).append("includePeakAmps = ").append(getIncludePeakAmps()).append("\n");
      sb.append(pre).append("includeDataSourceWf = ").append(getIncludeDataSourceWf()).append("\n");
      sb.append(pre).append("includeCodas = ").append(getIncludeMagCodas()).append("\n");
      sb.append(pre).append("candidateListName= ").append(getCandidateListName()).append("\n");
      sb.append(pre).append("loadCandidateListLLZ = ").append(loadCandidateListLLZ).append("\n"); 
      sb.append(pre).append("allowedStaTypes= ").append(GenericPropertyList.toPropertyString(staIncludeList)).append("\n");
      sb.append(pre).append("allowedSeedChanTypes= ").append(GenericPropertyList.toPropertyString(chanIncludeList)).append("\n");
      sb.append(pre).append("allowedLocationTypes= ").append(GenericPropertyList.toPropertyString(locIncludeList)).append("\n");
      sb.append(pre).append("allowedNetTypes= ").append(GenericPropertyList.toPropertyString(netIncludeList)).append("\n");
      sb.append(pre).append("rejectedStaTypes= ").append(GenericPropertyList.toPropertyString(staRejectList)).append("\n");
      sb.append(pre).append("rejectedSeedChanTypes= ").append(GenericPropertyList.toPropertyString(chanRejectList)).append("\n");
      sb.append(pre).append("rejectedLocationTypes= ").append(GenericPropertyList.toPropertyString(locRejectList)).append("\n");
      sb.append(pre).append("rejectedNetTypes= ").append(GenericPropertyList.toPropertyString(netRejectList)).append("\n");
      sb.append(pre).append("startTime = ").append(new Format("%12.2f").form(getStartTime()).trim()).append("\n");
      sb.append(pre).append("synchCandidateListBySolution = ").append(synchListBySolution).append("\n");
      sb.append(pre).append("filterWfListByChannelList= ").append(getFilterWfListByChannelList()).append("\n");
      sb.append(pre).append("useMasterListCandidateList= ").append(useMasterList).append("\n");
      sb.append(pre).append("windowDurationType= ").append(windowDurationType).append("\n");
      sb.append(pre).append("triggerSortOrder = ").append(triggerSortOrder).append("\n"); 

      return sb.toString();

    }

    public ArrayList getKnownPropertyKeys() {
      String pre = getPropertyPrefix();
      ArrayList list = new ArrayList(32);
      list.add(pre +"modelName");
      list.add(pre +"modelExplanation");
      list.add(pre +"debug");
      list.add(pre +"minDistance");
      list.add(pre +"maxDistance");
      list.add(pre +"minWindowSize");
      list.add(pre +"maxWindowSize");
      //list.add(pre +"magDepthSlope");
      //list.add(pre +"magDistSlope");
      list.add(pre +"magTauIntercept");
      list.add(pre +"magTauSlope");
      list.add(pre +"preEventSize");
      list.add(pre +"postEventSize");
      list.add(pre +"defaultNullMag");
      list.add(pre +"includeAllMag");
      list.add(pre +"includeAllComponents");
      list.add(pre +"requiresLocation");
      list.add(pre +"maxChannels");
      list.add(pre +"includePhases");
      list.add(pre +"includeMagAmps");
      list.add(pre +"includePeakAmps");
      list.add(pre +"includeDataSourceWf");
      list.add(pre +"includeCodas");
      list.add(pre +"candidateListName");
      list.add(pre +"loadCandidateListLLZ");
      list.add(pre +"allowedStaTypes");
      list.add(pre +"allowedSeedChanTypes");
      list.add(pre +"allowedLocationTypes");
      list.add(pre +"allowedNetTypes");
      list.add(pre +"rejectedStaTypes");
      list.add(pre +"rejectedSeedChanTypes");
      list.add(pre +"rejectedLocationTypes");
      list.add(pre +"rejectedNetTypes");
      list.add(pre +"startTime");
      list.add(pre +"synchCandidateListBySolution");
      list.add(pre +"useMasterListCandidateList");
      list.add(pre +"filterWfListByChannelList");
      list.add(pre +"windowDurationType");
      list.add(pre +"triggerSortOrder");
      return list;
    }

    public void setDefaultProperties() {
        setMyDefaultProperties();
    }

    private void setMyDefaultProperties() {

        debug = false;

        synchListBySolution = false;
        filterWfListByChannelList = false;
        useMasterList = false;

        //sol = null;   // don't reset preserve value previously set by application
        //oldSol = null;   // don't reset preserve value previously set by application

        candidateList = null;
        candidateListName = defaultCandidateListName;
        oldCandidateListName = candidateListName;
        requiredList = new ChannelableList();

        staIncludeList = new String [0];
        locIncludeList = new String [0];
        chanIncludeList = new String [0];
        netIncludeList = new String [0];
        staRejectList = new String [0];
        locRejectList = new String [0];
        chanRejectList = new String [0];
        netRejectList = new String [0];

        //-0.939721411 // intercept
        //2.093523334 // slope
        //0.003764123 // distParm
        //0.004630818 // depthParm
        // Mag = intercept + slope*logT + distParm*dist + depthParm*depth
        // T = 10**(mag-intercept-distParm*dist-depthParm*depth)/slope
        //-0.925377239 // intercept
        //2.103007893  // slope
        //0.003832809 // distParm
        //0.005223616 // depthParm

        magTauIntercept =  -0.95;
        magTauSlope     = 2.15;

        starttime = defStartTime;
        minWindow    =  30.0;
        maxWindow    = 600.0;
        preEvent     =  10.0;
        postEvent    =  10.0;

        minDistance = 0.0;
        maxDistance = Double.MAX_VALUE;
        maxChannels = Integer.MAX_VALUE;

        defaultNullMag = 1.5;
        includeAllMag = 10.0;

        includeAllComponents = false;

        includePhases = true;
        includeMagAmps = true;
        includePeakAmps = true;
        includeCodas = true;

        includeDataSourceWf = false;

        triggerSortOrder="DIST";

        loadCandidateListLLZ = false;

    }

    /** Given a Collection of ChannelTimeWindowModels
     *  produce a string that is a delimited concatenation of their class names
     *  of the form:<p>
     *
     *  "org.trinet.jasi.ThisIsaModel, org.trinet.otherstuff.AnotherModel"
     *
     * Note the input Collection is a Collection of ChannelTimeWindowModels/
     *  */
    public static String toModelListPropertyString(Collection list) {
        final String delim = ", ";
        StringBuffer sb = new StringBuffer(list.size() * 48);
        for (Iterator it=list.iterator(); it.hasNext(); ) {
          sb.append(((ChannelTimeWindowModel) it.next()).getClassname()).append(delim);
        }
        return sb.toString();
    }

    public Object clone() {
        ChannelTimeWindowModel newObj = null; 
        try {
          newObj = (ChannelTimeWindowModel) super.clone();
        } catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
        }
        return newObj;
    }
}
