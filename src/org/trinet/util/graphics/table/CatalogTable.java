package org.trinet.util.graphics.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.trinet.jasi.GTypeMap;
import org.trinet.jasi.Solution;
import org.trinet.jasi.SolutionList;
import org.trinet.jiggle.common.LogUtil;
import org.trinet.util.GenericPropertyList;

public class CatalogTable implements CatalogColorConst {
    // Implementation of Listener interface
    public class EditorMouseListener extends MouseAdapter {
        public void mousePressed(MouseEvent evt) {
            int maskS1 = MouseEvent.SHIFT_MASK | MouseEvent.BUTTON1_MASK;
            int maskC1 = MouseEvent.CTRL_MASK | MouseEvent.BUTTON1_MASK;
            int mods = evt.getModifiers();

            if ((mods & maskC1) == maskC1) {
                // Debug.println("CatalogTable: EML Canceling edit");
                tableView.editingCanceled(new ChangeEvent(tableView));
                tableView.repaint();
            } else if ((mods & maskS1) == maskS1) {
                // Debug.println("CatalogTable: EML Stopping edit");
                tableView.editingStopped(new ChangeEvent(tableView));
                // tableView.repaint();
            }
        }
    }

    private class MyJTable extends JTable {
        private static final long serialVersionUID = 1L;

        public MyJTable(TableModel m) {
            super(m);
        }

        private Color getCellBackgroundColor(int row, int colorByType) {
            if (colorByType <= 0)
                return CatalogUtils.getColorBck(catColors);

            String str = null;
            if (colorByType == 1)
                str = "ETYPE";
            else if (colorByType == 2)
                str = "ST";
            else if (colorByType == 3)
                str = "SRC";
            else if (colorByType == 4)
                str = "GT";
            else
                return CatalogUtils.getColorBck(catColors);

            TableColumn tc = null;
            try {
                tc = tableView.getColumn(str);
            } catch (IllegalArgumentException ex) {
            }

            if (tc == null)
                return CatalogUtils.getColorBck(catColors);

            int idx = tableView.convertColumnIndexToView(tc.getModelIndex());
            str = tableView.getValueAt(row, idx).toString();

            Color c = null;
            if (colorByType == 1) {
                c = colorMap.get("etype." + str);
            } else if (colorByType == 2) {
                c = colorMap.get("state." + str);
            } else if (colorByType == 3) {
                if (str == null || str.equals(""))
                    c = CatalogUtils.getColor(colorMap, CatalogColor.COLOR_CATALOG_SUBSRC_UNKNOWN);
                else
                    c = colorMap.get("subsrc." + str);

                if (c == null) {
                    if (str.startsWith("RT"))
                        c = CatalogUtils.getColor(colorMap, CatalogColor.COLOR_CATALOG_SUBSRC_RT);
                    else
                        c = CatalogUtils.getColor(colorMap, CatalogColor.COLOR_CATALOG_SUBSRC_OTHER);
                }
            } else if (colorByType == 4) {
                str = GTypeMap.fromDbType(str.toLowerCase());
                c = colorMap.get("gtype." + str);
            }

            if (c == null) {
                c = CatalogUtils.getColorBck(catColors);
            }

            return c;
        }

        public TableCellRenderer getCellRenderer(int row, int col) {
            TableCellRenderer tcr = super.getCellRenderer(row, col);
            return (tableView == null) ? tcr
                    : setRendererBackgroundColor(tcr, getCellBackgroundColor(row, colorByType));
        }

        private TableCellRenderer setRendererBackgroundColor(TableCellRenderer tcr, Color c) {
            if (c != null) {
                if (tableView == this) {
                    if (tcr instanceof ColorableRendererIF) {
                        ((ColorableRendererIF) tcr).setDefaultBackground(c);
                    } else if (tcr instanceof DefaultTableCellRenderer) {
                        ((DefaultTableCellRenderer) tcr).setBackground(c);
                    }
                } else if (tcr instanceof CatalogDeletedRowCellRenderer) {
                    ((CatalogDeletedRowCellRenderer) tcr)
                            .setDefaultBackground((c == CatalogUtils.getColorBck(catColors)) ? defaultColorHeadBck : c);
                }
            }
            return tcr;
        }

    }

    class TableEditListener implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent e) {
            if (tableView.isEditing()) {
                tableView.editingStopped(new ChangeEvent(tableView));
            }
        }
    }

    private final class TableHeaderRenderer extends DefaultTableCellRenderer {
        private static final long serialVersionUID = 1L;

        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
                int row, int column) {
            if (table != null) {
                JTableHeader header = table.getTableHeader();
                if (header != null) {
                    setForeground(header.getForeground());
                    setBackground(header.getBackground());
                    this.setToolTipText(getHeaderToolTipText(value));
                    setFont(header.getFont());
                }
            }

            setText((value == null) ? "" : value.toString());
//            setBorder(UIManager.getBorder("TableHeader.cellBorder"));
            setBorder(BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
            setHorizontalAlignment(JLabel.CENTER);
            return this;
        }
    }

    class TableRowHeadSelectionListener implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent e) {
            if (tableRowHead.getSelectedRowCount() <= 0)
                return;
            int[] rows = tableRowHead.getSelectedRows();
            tableView.clearSelection();
            for (int index = 0; index < rows.length; index++) {
                tableView.addRowSelectionInterval(rows[index], rows[index]);
            }
            if (!e.getValueIsAdjusting()) {
                int irow = tableRowHead.getSelectedRow();
                // Below two lines a test for aliased Map list notify ? -aww 09/13/2005
                SolutionList sl = getModel().getList();
                sl.setSelected((Solution) sl.get(getSortedRowModelIndex(irow)));
            }
        }
    }

    class TableViewSelectionListener implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent e) {
            if (tableView.getSelectedRowCount() <= 0) {
                return;
            }

            int irow = tableView.getSelectedRow();
            int irow2 = tableRowHead.getSelectedRow(); // need to check for equivalence else race ValueChanged event
                                                       // loop.
            if (irow != irow2 && irow >= 0) {
                tableRowHead.clearSelection();
                tableRowHead.setRowSelectionInterval(irow, irow);
            }
        }
    }

    static final int COLUMN_WIDTH_PADDING = 0;

    public static final String DEFAULT_HEADER_TOOLTIP = "dbl-click sort ascend; shift-dbl-click sort descend";
    private final static String[] DEFAULT_NAME_ORDER = CatalogTableConstants.columnNames;
    private static final Map<String, String> headerToolTipMap = new HashMap<>(47);
    static final int MAX_FRACTION_ELEMENTS = 16;
    static {
        headerToolTipMap.put("ID", "Event evid number");
        headerToolTipMap.put("EXT_ID", "Event external id (imported)");
        headerToolTipMap.put("DATETIME", "Preferred origin time");
        headerToolTipMap.put("LAT", "Preferred origin latitude");
        headerToolTipMap.put("LON", "Preferred origin longitude");

        headerToolTipMap.put("Z", "Preferred origin depth");
        headerToolTipMap.put("HDATUM", "Origin horizontal datum");
        headerToolTipMap.put("VDATUM", "Origin vertical datum");
        headerToolTipMap.put("OT", "Origin type");
        headerToolTipMap.put("METHOD", "Origin algorithm");

        headerToolTipMap.put("CM", "Origin crustal model id");
        headerToolTipMap.put("VM", "Origin velocity model id");
        headerToolTipMap.put("AUTH", "Origin authority");
        headerToolTipMap.put("SRC", "Origin subsource");
        headerToolTipMap.put("GAP", "Origin azimuthal gap");

        headerToolTipMap.put("DIST", "Origin (smallest epicentral) distance km");
        headerToolTipMap.put("RMS", "Origin rms travelTime residual secs");
        headerToolTipMap.put("ERR_T", "Origin datetime uncertainty secs");
        headerToolTipMap.put("ERR_H", "Origin horizontal uncertainty km");
        headerToolTipMap.put("ERR_Z", "Origin depth uncertainty km");

        headerToolTipMap.put("ERR_LAT", "Origin latitude uncertainty km");
        headerToolTipMap.put("ERR_LON", "Origin longitude uncertainty km");
        headerToolTipMap.put("OBS", "Origin associated arrivals");
        headerToolTipMap.put("USED", "Origin associated arrivals used (non-zero wts)");
        headerToolTipMap.put("AUSE", "Origin associated auto arrivals used (picker generated)");
        headerToolTipMap.put("S", "Origin associated S arrivals used");

        headerToolTipMap.put("FM", "Origin associated first motions");
        headerToolTipMap.put("Q", "Origin quality");
        headerToolTipMap.put("V", "Event select flag (1= valid event)");
        headerToolTipMap.put("ETYPE", "Event etype");
        headerToolTipMap.put("GT", "Origin gtype");
        headerToolTipMap.put("ST", "Origin processing state");

        headerToolTipMap.put("ZF", "Origin depth fixed (flag = 1)");
        headerToolTipMap.put("HF", "Origin lat,lon fixed (flag = 1)");
        headerToolTipMap.put("TF", "Origin datetime fixed (flag = 1)");
        headerToolTipMap.put("WRECS", "Event channels associated with waveforms");
        headerToolTipMap.put("MAG", "Event preferred magnitude");
        headerToolTipMap.put("MERR", "Event preferred magnitude error");
        headerToolTipMap.put("Md", "Preferred Md");
        headerToolTipMap.put("Me", "Preferred Me");
        headerToolTipMap.put("Ml", "Preferred Ml");
        headerToolTipMap.put("Mw", "Preferred Mw");
        headerToolTipMap.put("Ml-Md", "Difference of preferred Ml - Md");
        headerToolTipMap.put("Mlr", "Preferred Mlr");

        headerToolTipMap.put("PR", "Event processing priority");
        headerToolTipMap.put("MTYP", "Magnitude type");
        headerToolTipMap.put("COMMENT", "Event comment, remark text");
        headerToolTipMap.put("B", "Origin lat,lon bogus (flag = 1)");
        headerToolTipMap.put("OWHO", "Origin attribution identifier");

        headerToolTipMap.put("MWHO", "Magnitude attribution identifier");
        headerToolTipMap.put("VER", "Event version number");
        headerToolTipMap.put("MMETH", "Event preferred magnitude algorithm");
        headerToolTipMap.put("MOBS", "Event preferred magnitude channel count");
        headerToolTipMap.put("MSTA", "Event preferred magnitude station count");
        headerToolTipMap.put("PM", "Event has mechanism");
        headerToolTipMap.put("LDDATE", "Origin db row creation date");
    }

    public static String getNumberFormatPattern(int scale) {
        StringBuffer pattern = new StringBuffer("#0");
        if (scale > 0) {
            char[] frac = new char[scale];
            Arrays.fill(frac, '0');
            pattern.append('.');
            pattern.append(frac);
        }
        return pattern.toString();
    }

    private Color[] catColors = CatalogUtils.createColors();
    private int colorByType = 0;
    private Map<String, Color> colorMap = CatalogUtils.createColorMap();
    private String[] columnNameOrder = DEFAULT_NAME_ORDER;
    private Font font;
    private int fontSize = 12;
    private GenericPropertyList props = null;
    private TableSorterAbs sorter;
    private JScrollPane tableDB;
    private CatalogTableModel tableModel;
    private MyJTable tableRowHead;
    private MyJTable tableView;

    public CatalogTable(CatalogTableModel tm) {
        this.tableModel = tm;
    }

    public JScrollPane createTable() {
//        Debug.println("CatalogTable setting up MainTable JTable scrollpane ...");
        sorter = new TableSorterAbs();
        sorter.setModel(tableModel);
        tableView = new MyJTable(sorter);

        /*
         * if (props != null) { int debugGraphicsFlag =
         * props.getInt("catalogDebugGraphicsOption", -1); if (debugGraphicsFlag >= 0) {
         * if (debugGraphicsFlag == 0)
         * tableView.setDebugGraphicsOptions(DebugGraphics.NONE_OPTION); else if
         * (debugGraphicsFlag == 1)
         * tableView.setDebugGraphicsOptions(DebugGraphics.LOG_OPTION); //else if
         * (debugGraphicsFlag == 2)
         * tableView.setDebugGraphicsOptions(DebugGraphics.FLASH_OPTION); //else if
         * (debugGraphicsFlag == 3)
         * tableView.setDebugGraphicsOptions(DebugGraphics.BUFFERED_OPTION); } }
         */

        if (font == null)
            font = new Font("Monospaced", Font.PLAIN, fontSize);
        tableView.setFont(font);
        tableView.setShowGrid(true);
        tableView.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
//      tableView.setToolTipText("double-click edits data field only if SELECT FOR UPDATE query");

//        tableView.sizeColumnsToFit(tableView.AUTO_RESIZE_NEXT_COLUMN );
//        tableView.getTableHeader().setUpdateTableInRealTime(false);
        tableView.getTableHeader().setForeground(Color.red);
        tableView.getTableHeader().setBackground(grayBck);
        // tableView.getTableHeader().setToolTipText("dbl-click sort ascend;
        // shift-dbl-click sort descend");
        tableView.getTableHeader().setFont(font);

//        JScrollPane scrollpane = new JScrollPane(tableView);

// Make second table for row header
//        Debug.println("CatalogTable setting up RowHeader JTable scrollpane ...");
        TableColumnModel tcm = tableView.getColumnModel();
        TableColumnModel tcm2 = new DefaultTableColumnModel();
        TableColumn col;
        int maxCol = tableModel.getColumnCount();
        for (int i = 0; i < maxCol; i++) {
            if (tableModel.isKeyColumn(i)) {
                col = tcm.getColumn(tcm.getColumnIndex((Object) tableModel.getColumnName(i)));
                tcm.removeColumn(col);
                tcm2.addColumn(col);
            }
// test of limited column view AWW 11/99
            if (tableModel.isColumnHidden(i))
                tcm.removeColumn(tcm.getColumn(tcm.getColumnIndex((Object) tableModel.getColumnName(i))));
        }
        // try { // what is this move about below ? -aww REMOVED 2011/03/16
        // tcm.moveColumn(tcm.getColumnIndex((Object) "MAG"), 4);
        // }
        // catch (IllegalArgumentException ex) { }
//
// test to change column order here?
        TableColumnModel tcmTmp = new DefaultTableColumnModel();
        maxCol = columnNameOrder.length;
        for (int i = 0; i < maxCol; i++) {
            String colName = columnNameOrder[i];
            try {
                tcmTmp.addColumn(tableView.getColumn(colName));
            } catch (IllegalArgumentException ex) {
                System.out.println("Check column order by name - No such table column name:" + colName);
            }
        }
        tableView.setColumnModel(tcmTmp);
        tcm = tcmTmp;
// end of test
        tableRowHead = new MyJTable(sorter);

        /*
         * if (props != null) { int debugGraphicsFlag =
         * props.getInt("catalogDebugGraphicsOption", -1); if (debugGraphicsFlag >= 0) {
         * if (debugGraphicsFlag == 0)
         * tableView.setDebugGraphicsOptions(DebugGraphics.NONE_OPTION); else if
         * (debugGraphicsFlag == 1)
         * tableView.setDebugGraphicsOptions(DebugGraphics.LOG_OPTION); //else if
         * (debugGraphicsFlag == 2)
         * tableView.setDebugGraphicsOptions(DebugGraphics.FLASH_OPTION); //else if
         * (debugGraphicsFlag == 3)
         * tableView.setDebugGraphicsOptions(DebugGraphics.BUFFERED_OPTION); } }
         */

        tableRowHead.setColumnModel(tcm2);
        tableRowHead.setFont(font);
        tableRowHead.setShowGrid(true);
        tableRowHead.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

//        tableRowHead.sizeColumnsToFit(tableRowHead.AUTO_RESIZE_NEXT_COLUMN );
//        tableRowHead.getTableHeader().setUpdateTableInRealTime(false);
        tableRowHead.getTableHeader().setForeground(Color.red);
        tableRowHead.getTableHeader().setBackground(grayBck);
        tableRowHead.getTableHeader().setFont(font);
        // tableRowHead.getTableHeader().setToolTipText("dbl-click sort ascend;
        // shift-dbl-click sort descend");

// Setup format patterns and number renderers
        FormattedNumberRenderer[] numberRenderer = new FormattedNumberRenderer[MAX_FRACTION_ELEMENTS];
        FormattedNumberEditor_FW[] numberEditor = new FormattedNumberEditor_FW[MAX_FRACTION_ELEMENTS];
        String pattern; // format pattern buffer
        String pattern9 = "#0.000000"; // generic four byte float
        FormattedNumberRenderer numberRenderer9 = new FormattedNumberRenderer(pattern9);
        FormattedNumberEditor_FW numberEditor9 = new FormattedNumberEditor_FW(pattern9);
        numberEditor9.getComponent().addMouseListener(new EditorMouseListener());

// Assign editor/renderers to table columns        
// comment out for rendering test aww
        for (int i = 0; i < tableView.getColumnCount(); i++) {
            TableColumn tc = tcm.getColumn(i);
            tc.setHeaderRenderer(new TableHeaderRenderer());
            Class<?> tmpclass = tableView.getColumnClass(i);
//          Debug.println("CatalogTable tableView getColumnClass (i)" + i + " " + tmpclass.getName());
            if (tmpclass.getName().indexOf("String") >= 0) {
                tc.setCellRenderer(new TextCellRenderer(font));
                TextCellEditor textEditor = new TextCellEditor(font);
                textEditor.getComponent().addMouseListener(new EditorMouseListener());
                tc.setCellEditor(textEditor);
            } else if (tmpclass.getName().indexOf("Double") >= 0) {
                int indexViewToModel = tableView.convertColumnIndexToModel(i);
                int fractionWidth = tableModel.getColumnFractionDigits(indexViewToModel);
                if (fractionWidth >= 0) {
                    if (fractionWidth > MAX_FRACTION_ELEMENTS - 1)
                        fractionWidth = MAX_FRACTION_ELEMENTS - 1;
                    pattern = getNumberFormatPattern(fractionWidth);
                    if (numberRenderer[fractionWidth] == null)
                        numberRenderer[fractionWidth] = new FormattedNumberRenderer(pattern);
                    if (numberEditor[fractionWidth] == null) {
                        numberEditor[fractionWidth] = new FormattedNumberEditor_FW(pattern);
                        numberEditor[fractionWidth].getComponent().addMouseListener(new EditorMouseListener());
                    }
                    tc.setCellEditor(numberEditor[fractionWidth]);
                    tc.setCellRenderer(numberRenderer[fractionWidth]);
                } else if (fractionWidth < 0) {
                    tc.setCellEditor(numberEditor9);
                    tc.setCellRenderer(numberRenderer9);
                }
            } else if (tmpclass.getName().indexOf("Long") >= 0 || tmpclass.getName().indexOf("Integer") >= 0
                    || tmpclass.getName().indexOf("Number") >= 0) {
                pattern = getNumberFormatPattern(0);
                if (numberRenderer[0] == null)
                    numberRenderer[0] = new FormattedNumberRenderer(pattern);
                if (numberEditor[0] == null) {
                    numberEditor[0] = new FormattedNumberEditor_FW(pattern);
                    numberEditor[0].getComponent().addMouseListener(new EditorMouseListener());
                }
                tc.setCellEditor(numberEditor[0]);
                tc.setCellRenderer(numberRenderer[0]);
            }
        }

        CalendarDateRenderer cdRenderer = new CalendarDateRenderer();
        cdRenderer.setHorizontalAlignment(JLabel.LEFT);
        tableView.setDefaultRenderer(java.util.Date.class, cdRenderer); // used to be java.sql.Date 2008/02/07 -aww
        DateTimeEditor dtEditor = new DateTimeEditor(font);
        dtEditor.getComponent().addMouseListener(new EditorMouseListener());
        DateTimeRenderer dtRenderer = new DateTimeRenderer();
        dtRenderer.setHorizontalAlignment(JLabel.LEFT);

        for (int i = 0; i < tableView.getColumnCount(); i++) {
//        Debug.println("CatalogTable tableView getColumnName (i)" + i + " " + tableView.getColumnName(i));
            if (tableView.getColumnName(i).equalsIgnoreCase("datetime")) {
                TableColumn tc = tcm.getColumn(i);
                tc.setCellRenderer(dtRenderer);
                tc.setCellEditor(dtEditor);
            }
        }
//comment out for test of rendering
// size scrollable main table columns
//        Debug.println("CatalogTable sizing MainTable columns");        
        initColumnSizes(tableView);

// setup main table selection/color attributes
        tableView.setCellSelectionEnabled(false);
        tableView.setRowSelectionAllowed(true);
        tableView.setColumnSelectionAllowed(false);
        tableView.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        tableView.setForeground(Color.black);
        tableView.setBackground(CatalogUtils.getColorBck(catColors));
        tableView.setSelectionForeground(Color.red);
        tableView.setSelectionBackground(Color.white);

// assign rowheader renderer (string type)
//        Debug.println("CatalogTable setting up RowHeader cell renderers/editors...");

        for (int i = 0; i < tableRowHead.getColumnCount(); i++) {
            TableColumn tc = tcm2.getColumn(i);
            tc.setHeaderRenderer(new TableHeaderRenderer());

            Class<?> tmpclass = tableRowHead.getColumnClass(i);
            if (tmpclass.getName().equals("java.lang.String")) {
                tc.setCellRenderer(new TextCellRenderer(font));
                TextCellEditor textEditor = new TextCellEditor(font);
                textEditor.getComponent().addMouseListener(new EditorMouseListener());
                tc.setCellEditor(textEditor);
            } else if (tmpclass.getName().equals("java.lang.Double")) {
                int indexViewToModel = tableRowHead.convertColumnIndexToModel(i);
                int fractionWidth = tableModel.getColumnFractionDigits(indexViewToModel);
                if (fractionWidth >= 0) {
                    if (fractionWidth > MAX_FRACTION_ELEMENTS - 1)
                        fractionWidth = MAX_FRACTION_ELEMENTS - 1;
                    pattern = getNumberFormatPattern(fractionWidth);
                    if (numberRenderer[fractionWidth] == null)
                        numberRenderer[fractionWidth] = new FormattedNumberRenderer(pattern);
                    if (numberEditor[fractionWidth] == null) {
                        numberEditor[fractionWidth] = new FormattedNumberEditor_FW(pattern);
                        numberEditor[fractionWidth].getComponent().addMouseListener(new EditorMouseListener());
                    }
                    tc.setCellEditor(numberEditor[fractionWidth]);
                    tc.setCellRenderer(numberRenderer[fractionWidth]);
                } else if (fractionWidth < 0) {
                    tc.setCellEditor(numberEditor9);
                    tc.setCellRenderer(numberRenderer9);
                }
            } else if (tmpclass.getName().equals("java.lang.Integer") || tmpclass.getName().equals("java.lang.Number")
                    || tmpclass.getName().equals("java.lang.Long")) {
                pattern = getNumberFormatPattern(0);
                if (numberRenderer[0] == null)
                    numberRenderer[0] = new FormattedNumberRenderer(pattern);
                if (numberEditor[0] == null) {
                    numberEditor[0] = new FormattedNumberEditor_FW(pattern);
                    numberEditor[0].getComponent().addMouseListener(new EditorMouseListener());
                }
                tc.setCellEditor(numberEditor[0]);
                tc.setCellRenderer(numberRenderer[0]);
//                  tc.setCellEditor(numberEditor7);
//                  tc.setCellRenderer(numberRenderer7);
            }
        }

        for (int i = 0; i < tableRowHead.getColumnCount(); i++) {
            String name = tableRowHead.getColumnName(i);
            if (name.equalsIgnoreCase("datetime")) {
                TableColumn tc = tcm2.getColumn(i);
                tc.setCellRenderer(dtRenderer);
                tc.setCellEditor(dtEditor);
            }
// implemented code below as an example of renderering "deleted" solution ids differently.
            else if (name.equalsIgnoreCase("id")) {
                // below a kludge until melded with JasiListTableModelIF like Mung aww 10/03
                CatalogDeletedRowCellNumberRenderer render = new CatalogDeletedRowCellNumberRenderer(tableModel);
                render.setDefaultBackground(defaultColorHeadBck);
                tcm2.getColumn(i).setCellRenderer(render);
            }
        }
        tableRowHead.setDefaultRenderer(java.util.Date.class, cdRenderer); // used to be java.sql.Date 2008/02/07 -aww
// end of renderer removal test aww
// size rowheader columns
//        Debug.println("CatalogTable sizing RowHead columns");        
        initColumnSizes(tableRowHead);

//        Debug.println("CatalogTable finished with renderer setup; final table setup");        
// setup rowheader selection/color attributes
        tableRowHead.setCellSelectionEnabled(false);
        tableRowHead.setRowSelectionAllowed(true);
        tableRowHead.setColumnSelectionAllowed(false);
        tableRowHead.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        tableRowHead.setForeground(Color.black);
        tableRowHead.setBackground(defaultColorHeadBck);
        tableRowHead.setSelectionForeground(Color.blue);
        tableRowHead.setSelectionBackground(Color.orange);
//      tableRowHead.setToolTipText("Key fields highlighted yellow (shift-click over cell resizes rowheader width)");

// size rowheader and put in scrollpane
        JScrollPane scrollpane = null;
        tableRowHead.setPreferredScrollableViewportSize(tableRowHead.getPreferredSize());
        if (tableView.getColumnCount() <= 0) {
            System.out.println("CatalogTable: tableView has no columns");
            scrollpane = new JScrollPane(tableRowHead);
        } else
            scrollpane = new JScrollPane(tableView);
        scrollpane.setRowHeaderView(tableRowHead);
        scrollpane.setCorner(JScrollPane.UPPER_LEFT_CORNER, tableRowHead.getTableHeader());

// add main table models event listeners
        tcm2.addColumnModelListener(tableView);
//        tcm2.getSelectionModel().addListSelectionListener(new TableEditListener());
//        tcm.getSelectionModel().addListSelectionListener(new TableEditListener());
        sorter.addTableModelListener(tableView);
        sorter.addMouseListenerToHeaderInTable(tableView); // Install a mouse listener in the TableHeader as the sorter
                                                           // UI.
// sync rowheader selection with main table cell selection
        tableView.getSelectionModel().addListSelectionListener(new TableEditListener());
        tableRowHead.getSelectionModel().addListSelectionListener(new TableRowHeadSelectionListener());
        tableView.getSelectionModel().addListSelectionListener(new TableViewSelectionListener());

        tableView.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent evt) {
                // int clickCount = evt.getClickCount();
                int maskC1 = MouseEvent.CTRL_MASK | MouseEvent.BUTTON1_MASK;
                int mods = evt.getModifiers();
//                        Debug.println("TV ClickCount:"+clickCount+" TV Editing? "+tableView.isEditing()+" Button: "+mods);

                if ((mods & maskC1) == maskC1) {
                    LogUtil.info("Canceling edit");
                    tableView.editingCanceled(new ChangeEvent(tableView));
                    tableView.repaint();
                }
            }
        });
        // aww test of mouse clicks
        tableRowHead.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent evt) {
                // Debug.println("TRH ClickCount:" + evt.getClickCount() + " TRH Editing? " +
                // tableRowHead.isEditing());
                int maskA1 = MouseEvent.ALT_MASK | MouseEvent.BUTTON1_MASK;
                int mods = evt.getModifiers();
                if ((mods == maskA1) && evt.getClickCount() >= 1) {
                    Point p = evt.getPoint();
                    resetRowColumnSizes(tableRowHead, tableRowHead.rowAtPoint(p), tableRowHead.columnAtPoint(p));
                    // resetRowColumnSizes(tableRowHead, tableRowHead.getSelectedRow(),
                    // tableRowHead.getSelectedColumn());
                    resetRowHeader(tableRowHead, tableDB);
                    tableDB.revalidate();
                    tableRowHead.repaint();
                } // else System.out.println("Shift failed");
            }
        });

        tableView.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent evt) {
                // Debug.println("TV ClickCount:" + evt.getClickCount() + " TV Editing? " +
                // tableView.isEditing());
                int maskA1 = MouseEvent.ALT_MASK | MouseEvent.BUTTON1_MASK;
                int mods = evt.getModifiers();
                if ((mods == maskA1) && evt.getClickCount() >= 1) {
                    Point p = evt.getPoint();
                    // SwingUtilities.convertPoint((Component)evt.getSource(), p, tableView);
                    resetRowColumnSizes(tableView, tableView.rowAtPoint(p), tableView.columnAtPoint(p));
                    // resetRowColumnSizes(tableView, tableView.getSelectedRow(),
                    // tableView.getSelectedColumn());
                    // resetRowHeader(tableRowHead, tableDB);
                    tableView.revalidate();
                    tableDB.revalidate();
                    tableView.repaint();
                }
                // else System.out.println("Shift failed");
            }
        });
        // end of mouse click test aww

// add rowheader models event listeners
        tcm.addColumnModelListener(tableRowHead);
        sorter.addTableModelListener(tableRowHead);
        sorter.addMouseListenerToHeaderInTable(tableRowHead);

// size the table viewport
        tableView.setPreferredScrollableViewportSize(tableView.getPreferredSize());

        tableDB = scrollpane;

        return scrollpane;
    } // end of createTable() method

    public JScrollPane createTable(AbstractTableModel tableModel) {
        this.tableModel = (CatalogTableModel) tableModel;
        return createTable();
    }

    public JScrollPane createTable(Font font) {
        if (font != null)
            this.font = font;
        return createTable();
    }

    public JScrollPane createTable(int fontSize) {
        if (fontSize > 0)
            this.fontSize = fontSize;
        return createTable();
    }

    private String getHeaderToolTipText(Object value) {
        String tip = (String) headerToolTipMap.get(value);
        return (tip != null) ? tip : DEFAULT_HEADER_TOOLTIP;
    }

    public CatalogTableModel getModel() {
        return this.tableModel;
    }

    public Solution getNextSortedSolution(Solution currentSol) {
        if (sorter == null)
            return null;
        CatalogTableModel ctm = (CatalogTableModel) sorter.getModel();
        int rowid = ctm.getIndexOf(currentSol);
        if (rowid < 0)
            return null;
        rowid = modelToSortedRowIndex(rowid);
        if (rowid < 0)
            return null;
        if (++rowid >= ctm.getRowCount())
            rowid = 0;
        rowid = getSortedRowModelIndex(rowid);
        if (rowid < 0)
            return null;
        return ctm.getSolution(rowid);
    }

    public int getRenderedCellWidth(JTable table, int irow, int icol) {
        if (table == null)
            return -1;
        int rowCount = table.getRowCount();
        if (irow < 0 || irow > rowCount)
            return -1;
        int colCount = table.getColumnCount();
        if (icol < 0 || icol > colCount)
            return -1;
        TableColumn column = table.getColumnModel().getColumn(icol);
        if (column == null)
            return -1;
        Component comp = table.getCellRenderer(irow, icol).getTableCellRendererComponent(table,
                table.getValueAt(irow, icol), true, true, irow, icol);
        Insets insets = ((Container) comp).getInsets();
        return comp.getPreferredSize().width + insets.left + insets.right;
    }

    public JTable getRowHeader() {
        return tableRowHead;
    }

    public TableSorterAbs getSortedModel() {
        return sorter;
    }

    public int getSortedRowModelIndex(int selectedRow) {
        if (sorter == null)
            return -1;
        return sorter.indexes[selectedRow];
    }

    public int getSortedTableRowIndex(Solution currentSol) {
        if (sorter == null)
            return -1;
        CatalogTableModel ctm = (CatalogTableModel) sorter.getModel();
        int rowid = ctm.getIndexOf(currentSol);
        if (rowid < 0)
            return -1;
        rowid = modelToSortedRowIndex(rowid);
        if (rowid < 0)
            return -1;
        return rowid;
    }

    public JTable getTable() {
        return tableView;
    }

    public String[] getTableColumnHeaders() {
        if (tableView == null)
            return null;
        int count = tableView.getColumnCount();
        if (count < 1)
            return null;
        String[] headers = new String[count];
        TableColumnModel tcm = tableView.getColumnModel();
        for (int i = 0; i < count; i++) {
            headers[i] = (String) tcm.getColumn(i).getHeaderValue();
        }
        return headers;
    }

    public void initColumnSizes(JTable table) {
        TableColumn column = null;
        Component comp = null;
        int headerWidth = 0;
        int cellWidth = 0;

        int colCount = table.getColumnCount();
        int rowCount = table.getRowCount();
        // perhaps would be better to used subset for speed?
        int checkRows;
        if (rowCount > 300)
            checkRows = 300;
        else
            checkRows = rowCount;

        int insetPads = 0;
        for (int i = 0; i < colCount; i++) {
            column = table.getColumnModel().getColumn(i);
            comp = column.getHeaderRenderer().getTableCellRendererComponent(table, column.getHeaderValue(), true, true,
                    0, 0);
            if (i == 0) {
                Insets insets = ((Container) comp).getInsets();
                insetPads = insets.left + insets.right;
            }

            headerWidth = comp.getPreferredSize().width;

            int maxCellWidth = 0;
            // Do a bottom rows check since their ids maybe the largest
            for (int j = rowCount - checkRows; j < rowCount; j++) {
                comp = table.getCellRenderer(j, i).getTableCellRendererComponent(table, table.getValueAt(j, i), true,
                        true, j, i);
                cellWidth = comp.getPreferredSize().width;
                if (cellWidth > maxCellWidth)
                    maxCellWidth = cellWidth;
            }

            column.setPreferredWidth(Math.max(headerWidth, maxCellWidth) + insetPads);
        }
    }

    public int modelToSortedRowIndex(int modelRowIndex) {
        if (sorter == null)
            return -1;
        for (int idx = 0; idx < sorter.getRowCount(); idx++) {
            if (sorter.indexes[idx] == modelRowIndex)
                return idx;
        }
        return -1;
    }

    public boolean resetRowColumnSizes(JTable table, int irow) {
        if (table == null)
            return false;
        int rowCount = table.getRowCount();
        if (irow < 0 || irow > rowCount)
            return false;
        TableColumn column = null;
        Component comp = null;
        int headerWidth = 0;
        int cellWidth = 0;
        int colCount = table.getColumnCount();
        for (int icol = 0; icol < colCount; icol++) {
            column = table.getColumnModel().getColumn(icol);
            comp = column.getHeaderRenderer().getTableCellRendererComponent(table, column.getHeaderValue(), true, true,
                    0, 0);

            Insets insets = ((Container) comp).getInsets();
            int insetPads = insets.left + insets.right;
            headerWidth = comp.getPreferredSize().width + insetPads;

            int maxCellWidth = 0;

            comp = table.getCellRenderer(irow, icol).getTableCellRendererComponent(table, table.getValueAt(irow, icol),
                    true, true, irow, icol);
            cellWidth = comp.getPreferredSize().width + insetPads;

            if (cellWidth > maxCellWidth)
                maxCellWidth = cellWidth;

            cellWidth = Math.max(headerWidth, maxCellWidth);

            // If current width is larger let it be the same as before
            column.setPreferredWidth(Math.max(column.getPreferredWidth(), cellWidth));
        }

        return true;
    }

    public boolean resetRowColumnSizes(JTable table, int irow, int icol) {
        if (table == null)
            return false;
        int rowCount = table.getRowCount();
        if (irow < 0 || irow > rowCount)
            return false;
        TableColumn column = null;
        Component comp = null;
        int headerWidth = 0;
        int cellWidth = 0;
        column = table.getColumnModel().getColumn(icol);
        comp = column.getHeaderRenderer().getTableCellRendererComponent(table, column.getHeaderValue(), true, true, 0,
                0);

        Insets insets = ((Container) comp).getInsets();
        int insetPads = insets.left + insets.right;
        headerWidth = comp.getPreferredSize().width + insetPads;

        int maxCellWidth = 0;

        comp = table.getCellRenderer(irow, icol).getTableCellRendererComponent(table, table.getValueAt(irow, icol),
                true, true, irow, icol);
        cellWidth = comp.getPreferredSize().width + insetPads;
        if (cellWidth > maxCellWidth)
            maxCellWidth = cellWidth;

        cellWidth = Math.max(headerWidth, maxCellWidth);

        // If current width is larger let it be
        column.setPreferredWidth(Math.max(column.getPreferredWidth(), cellWidth));

        return true;
    }

    public boolean resetRowHeader(JTable table, JScrollPane scrollpane) {
        if (table == null || scrollpane == null)
            return false;
        table.setPreferredScrollableViewportSize(table.getPreferredSize());
        scrollpane.setRowHeaderView(table);
        scrollpane.setCorner(JScrollPane.UPPER_LEFT_CORNER, table.getTableHeader());
        return true;
    }

    private void setColorMap(String key, Color c) {
        CatalogUtils.setColorMap(colorMap, key, c);
    }

    public final void setColumnNameOrder(String[] names) {
        if (names != null)
            columnNameOrder = names;
    }

    public void setProperties(GenericPropertyList gpl) {
        props = gpl;

        colorByType = props.getInt("colorCatalogByType");

        Color c;
        CatalogUtils.setupColors(catColors, gpl);
        CatalogUtils.setupColorMap(colorMap, catColors, gpl);

        // Note when adding new subsrc, etype, or state remember to update DPcatalog
        // init

        // What are these loops for to extract "color.catalog props ?
        final Set<String> keys = props.stringPropertyNames();
        for (String key : keys) {
            if (!key.startsWith("color.catalog.subsrc"))
                continue;
            c = props.getColor(key); // getColor() returns null if unparseable
            if (c != null) {
                key = key.substring(14);
                setColorMap(key, c);
            }
        }

        for (String key : keys) {
            if (!key.startsWith("color.catalog.etype"))
                continue; // discriminate "catalog" color type
            c = props.getColor(key); // getColor() returns null if unparseable
            if (c != null) {
                key = key.substring(14);
                setColorMap(key, c);
            }
        }

        for (String key : keys) {
            if (!key.startsWith("color.catalog.gtype"))
                continue; // discriminate "catalog" color type
            c = props.getColor(key); // getColor() returns null if unparseable
            key = key.substring(14);
            if (c != null) {
                setColorMap(key, c);
            }
        }

        toProperties(props);
        // System.out.println(colorMap);
    }

    public void toProperties(GenericPropertyList gpl) {
        Set<Map.Entry<String, Color>> set = colorMap.entrySet();
        int size = set.size();
        if (size == 0)
            return;

        Iterator<Map.Entry<String, Color>> iter = set.iterator();
        Map.Entry<String, Color> me;
        Color c = null;
        String key = null;
        String value = null;
        while (iter.hasNext()) {
            me = iter.next();
            c = me.getValue();
            key = (String) me.getKey();
            value = (c == null) ? "" : Integer.toHexString(c.getRGB());
            if (value != "")
                gpl.setProperty("color.catalog." + key, value);
        }
    }

    public int updateDB() {
        return tableModel.updateDB();
    }
}
