package org.trinet.jiggle;

import java.awt.event.AdjustmentListener;
import java.awt.event.AdjustmentEvent;
import java.awt.*;
import javax.swing.*;
import java.text.DecimalFormat;

/**
 * Overrides JViewport's paint() method to draw channel name labels on left side of viewport 
 */

public class ChannelLabelViewport extends JViewport implements AdjustmentListener {

    private WFScroller ws = null;    

    private static final DecimalFormat df1 = new DecimalFormat ( "###0.0" );  // distance format

    private boolean drawLabels = false;

    public ChannelLabelViewport() {
        //Below means: just repaint area newly scrolled into view
        setScrollMode(JViewport.BLIT_SCROLL_MODE);
        setOpaque(true);  // aww, if false then scroller repaint not as efficient
    }
    public ChannelLabelViewport(WFScroller wfScroller) {
        this();
        this.ws = wfScroller;
        ws.getHorizontalScrollBar().addAdjustmentListener(this);
        ws.getVerticalScrollBar().addAdjustmentListener(this);
    }
    
    public void paintChildren(Graphics g) {
        super.paintChildren(g);
        if (drawLabels) drawChannelLabel(g);
    }
    
    private void drawChannelLabel(Graphics g) {
        int pHeight = ws.groupPanel.singlePanelSize.height;
        int fHeight = g.getFontMetrics().getHeight();
        if (pHeight < fHeight || pHeight <= 12) return; // dont't draw labels
        //int yoff = fHeight - 2; // shift down from top
        int yoff = pHeight - 2; // shift down from top

        Color oldColor = g.getColor();  // remember current color to reset to later
        //Font oldFont = g.getFont();  // could do font scaling ?

        int cnt = 0;
        // logic if WFPanels are "hidden", skipped over, we need to find the offset of topmost visible -aww 2009/04/02
        int topIndex = -1;
        JViewport jvp = ws.getViewport();
        if (jvp != null) {
          Component c = jvp.getView();
          if (c != null) c = c.getComponentAt(jvp.getViewPosition());
          if (c != null) {
              topIndex = ws.groupPanel.wfpList.indexOf(c);
              //System.out.println("GetComponentAt:"+((ActiveWFPanel)c).wfv.getChannelObj().toString()+" topIndex: " + topIndex);
          }
        }
        if (topIndex < 0) topIndex = ws.getTopIndex();
        // end of added logic -aww 2009/04/02

        int bottomIndex = topIndex + ws.viewsPerPage;
        // Label the trace
        int ii = topIndex;
        while (ii < bottomIndex) {
            WFPanel wp =  (WFPanel) ws.groupPanel.wfpList.get(ii);
            if ( ! wp.isVisible() || ! wp.isShowing() ) {
                continue;
            }
            ii++;

            StringBuffer sb = new StringBuffer(80); // aww
            if (wp.wfv.getChannelObj() == null) sb.append("null");
            else {
              // Allow use of static Channel.useFullName property toggle for debug -aww
              sb.append(wp.wfv.getChannelObj().toDelimitedNameString(' '));
              sb.append(" ").append(df1.format(wp.wfv.getHorizontalDistance())).append("km ");
              if (wp.getWf() != null) sb.append(wp.getWf().getFilterName());
              String str = sb.toString(); 

              //int x = 10;
              int x = getExtentSize().width - g.getFontMetrics().stringWidth(str) - 10;
              int y = cnt*pHeight + yoff;

              g.setColor(wp.textColor);
              g.drawString(str, x, y);
              cnt++;

            }
        }
        g.setColor(oldColor);
    }

    public void adjustmentValueChanged(AdjustmentEvent e) {
        if (! e.getValueIsAdjusting()) {
            repaint();
        }
    }

    public void setDrawLabels(boolean tf) {
        if (drawLabels != tf) {
          drawLabels = tf;
          repaint();
        }
    }

}
