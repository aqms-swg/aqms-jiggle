package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see Arrival
*/
public interface TableRowArrival extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "ARRIVAL";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  28;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "ARSEQ";


/**  ARRIVAL table "arid" column data object offset in collection stored by implementing class.
*/
    public static final int ARID = 0;

/**  ARRIVAL table "commid" column data object offset in collection stored by implementing class.
*/
    public static final int COMMID = 1;

/**  ARRIVAL table "datetime" column data object offset in collection stored by implementing class.
*/
    public static final int DATETIME = 2;

/**  ARRIVAL table "sta" column data object offset in collection stored by implementing class.
*/
    public static final int STA = 3;

/**  ARRIVAL table "net" column data object offset in collection stored by implementing class.
*/
    public static final int NET = 4;

/**  ARRIVAL table "auth" column data object offset in collection stored by implementing class.
*/
    public static final int AUTH = 5;

/**  ARRIVAL table "subsource" column data object offset in collection stored by implementing class.
*/
    public static final int SUBSOURCE = 6;

/**  ARRIVAL table "channel" column data object offset in collection stored by implementing class.
*/
    public static final int CHANNEL = 7;

/**  ARRIVAL table "channelsrc" column data object offset in collection stored by implementing class.
*/
    public static final int CHANNELSRC = 8;

/**  ARRIVAL table "seedchan" column data object offset in collection stored by implementing class.
*/
    public static final int SEEDCHAN = 9;

/**  ARRIVAL table "location" column data object offset in collection stored by implementing class.
*/
    public static final int LOCATION = 10;

/**  ARRIVAL table "iphase" column data object offset in collection stored by implementing class.
*/
    public static final int IPHASE = 11;

/**  ARRIVAL table "qual" column data object offset in collection stored by implementing class.
*/
    public static final int QUAL = 12;

/**  ARRIVAL table "clockqual" column data object offset in collection stored by implementing class.
*/
    public static final int CLOCKQUAL = 13;

/**  ARRIVAL table "clockcorr" column data object offset in collection stored by implementing class.
*/
    public static final int CLOCKCORR = 14;

/**  ARRIVAL table "ccset" column data object offset in collection stored by implementing class.
*/
    public static final int CCSET = 15;

/**  ARRIVAL table "fm" column data object offset in collection stored by implementing class.
*/
    public static final int FM = 16;

/**  ARRIVAL table "ema" column data object offset in collection stored by implementing class.
*/
    public static final int EMA = 17;

/**  ARRIVAL table "azimuth" column data object offset in collection stored by implementing class.
*/
    public static final int AZIMUTH = 18;

/**  ARRIVAL table "slow" column data object offset in collection stored by implementing class.
*/
    public static final int SLOW = 19;

/**  ARRIVAL table "deltim" column data object offset in collection stored by implementing class.
*/
    public static final int DELTIM = 20;

/**  ARRIVAL table "delinc" column data object offset in collection stored by implementing class.
*/
    public static final int DELINC = 21;

/**  ARRIVAL table "delaz" column data object offset in collection stored by implementing class.
*/
    public static final int DELAZ = 22;

/**  ARRIVAL table "delslo" column data object offset in collection stored by implementing class.
*/
    public static final int DELSLO = 23;

/**  ARRIVAL table "quality" column data object offset in collection stored by implementing class.
*/
    public static final int QUALITY = 24;

/**  ARRIVAL table "snr" column data object offset in collection stored by implementing class.
*/
    public static final int SNR = 25;

/**  ARRIVAL table "rflag" column data object offset in collection stored by implementing class.
*/
    public static final int RFLAG = 26;

/**  ARRIVAL table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 27;
/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =
   "ARID,COMMID,truetime.getEpoch(DATETIME,'UTC'),STA,NET,AUTH,SUBSOURCE,CHANNEL,CHANNELSRC,SEEDCHAN,LOCATION,IPHASE,QUAL,CLOCKQUAL,CLOCKCORR,CCSET,FM,EMA,AZIMUTH,SLOW,DELTIM,DELINC,DELAZ,DELSLO,QUALITY,SNR,RFLAG,LDDATE";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    "ARRIVAL.ARID,ARRIVAL.COMMID,truetime.getEpoch(ARRIVAL.DATETIME,'UTC'),ARRIVAL.STA,ARRIVAL.NET,ARRIVAL.AUTH,ARRIVAL.SUBSOURCE,ARRIVAL.CHANNEL,ARRIVAL.CHANNELSRC,ARRIVAL.SEEDCHAN,ARRIVAL.LOCATION,ARRIVAL.IPHASE,ARRIVAL.QUAL,ARRIVAL.CLOCKQUAL,ARRIVAL.CLOCKCORR,ARRIVAL.CCSET,ARRIVAL.FM,ARRIVAL.EMA,ARRIVAL.AZIMUTH,ARRIVAL.SLOW,ARRIVAL.DELTIM,ARRIVAL.DELINC,ARRIVAL.DELAZ,ARRIVAL.DELSLO,ARRIVAL.QUALITY,ARRIVAL.SNR,ARRIVAL.RFLAG,ARRIVAL.LDDATE";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
        "ARID", "COMMID", "DATETIME", "STA", "NET", 
        "AUTH", "SUBSOURCE", "CHANNEL", "CHANNELSRC", "SEEDCHAN", 
        "LOCATION", "IPHASE", "QUAL", "CLOCKQUAL", "CLOCKCORR", 
        "CCSET", "FM", "EMA", "AZIMUTH", "SLOW", 
        "DELTIM", "DELINC", "DELAZ", "DELSLO", "QUALITY", 
        "SNR", "RFLAG", "LDDATE"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
        false, true, false, false, true, 
        true, true, true, true, true, 
        true, true, true, true, true, 
        true, true, true, true, true, 
        true, true, true, true, true, 
        true, true, true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
        DATALONG, DATALONG, DATADOUBLE, DATASTRING, DATASTRING, 
        DATASTRING, DATASTRING, DATASTRING, DATASTRING, DATASTRING, 
        DATASTRING, DATASTRING, DATASTRING, DATASTRING, DATALONG, 
        DATASTRING, DATASTRING, DATADOUBLE, DATADOUBLE, DATADOUBLE, 
        DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, 
        DATADOUBLE, DATASTRING, DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
        0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 4, 
        2, 2, 2, 4, 1, 0, 0, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
        15, 15, 25, 6, 8, 15, 8, 3, 8, 3, 2, 8, 1, 1, 15, 1, 2, 5, 4, 8, 
        5, 4, 5, 8, 2, 126, 2, 7
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
        null, null, null, null, null, null, null, null, null, null, 
        null, null, null, null, null, null, null, null, null, null, 
        null, null, null, null, null, null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))"
    };
}
