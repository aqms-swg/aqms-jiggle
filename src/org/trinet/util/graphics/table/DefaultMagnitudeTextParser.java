package org.trinet.util.graphics.table;
import java.io.*;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.util.*;

public class DefaultMagnitudeTextParser extends AbstractJasiTableRowTextParser {
  protected String magType; 
  protected double magValue;
  protected double magErr;
  protected int magChnls;
  //prefMag should be set at panel editor level

  public DefaultMagnitudeTextParser() {
    super();
    parserName = "Magnitude";
    dialogTitle = "<value> <type e.g. l,h> [staCount] [error] ; preferred mag is set to last table row entered";
  }

  public void parseInputTableRows(String text) {
    hasParseErrors = false;
    if (jrList == null) jrList = new MagList();
    else jrList.clear();
    if (text == null || text.length() == 0) return;
    BufferedReader buffReader = null;
    try {
      buffReader = new BufferedReader(new StringReader(text));
      String aLine = null;
      while ((aLine = buffReader.readLine()) != null) {
        if (aLine.trim().equals("")) continue;
        StringTokenizer strToke = new StringTokenizer(aLine);
        initValues();
        if (strToke.countTokens() < 2) {
          System.err.println("Error: Input magnitude parser data has too few field tokens, needs value and type");
          System.err.println(aLine);
          hasParseErrors = true;
          continue;
        }
        try {
          magValue = Double.parseDouble(strToke.nextToken());
        }
        catch (NumberFormatException ex) {
          System.err.println("Error: Unable to parse magnitude size");
          System.err.println(aLine);
          hasParseErrors = true;
          continue;
        }
        // could put a pattern matcher here for type constraints
        magType = strToke.nextToken();
        if (magType.length() > 2) {
          System.err.println("Error: Unable to parse magnitude subtype string");
          System.err.println(aLine);
          hasParseErrors = true;
          continue;
        }
        if (strToke.hasMoreTokens()) {
          try {
            magChnls = Integer.parseInt(strToke.nextToken());
          }
          catch (NumberFormatException ex) {
            System.err.println("Error: Unable to parse magnitude station count.");
            System.err.println(aLine);
            hasParseErrors = true;
            continue;
          }
        }
        if (strToke.hasMoreTokens()) {
          try {
            magErr = Double.parseDouble(strToke.nextToken());
          }
          catch (NumberFormatException ex) {
            System.err.println("Error: Unable to parse magnitude error");
            System.err.println(aLine);
            hasParseErrors = true;
            continue;
          }
        }
        Magnitude aRow = (Magnitude) createRow();
        aRow.setNeedsCommit(false); // why false?, may want this removed
        if (aRow != null) jrList.add(aRow);
      }
    }
    catch (IOException ex) {
       ex.printStackTrace();
       hasParseErrors = true;
    }
    finally {
      try {
        if (buffReader != null) buffReader.close();
      }
      catch (IOException ex2) {
        ex2.toString();
      }
    }
  }
  protected void initValues() {
    magType = ""; 
    magValue = -1.;
    magErr = -1.;
    magChnls = -1;
  }
  public JasiCommitableIF createRow() {
      if (tableModel == null) return null;
      Magnitude aMag = (Magnitude) tableModel.initRowValues(Magnitude.create());
      aMag.value.setValue(magValue);
      if (! magType.equals("")) aMag.subScript.setValue(magType);
      if (magErr >= 0.) aMag.error.setValue(magErr);
      if (magChnls > 0) aMag.usedChnls.setValue(magChnls); // used to be stationsUsed - aww 2008/07/10
      aMag.setAuthority(defaultAuth);
      aMag.setSource(defaultSource);
      Debug.println("parsed input mag: " + aMag.toString());
      return aMag;
  }
}
