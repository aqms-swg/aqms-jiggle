package test.org.trinet.apps;

import org.junit.jupiter.api.Test;
import org.trinet.apps.StaDist;
import test.common.DatabaseTests;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Requires a channelList-test.cacheByLocation file
 * Gererated by running ChannelListCacheWriter
 *
 * TODO Make this an automated process for this test
 * https://gitlab.com/aqms-swg/aqms-jiggle/-/issues/253
 * Will disable tests until this is addressed since this can cause false negative pipeline failures
 */
public class StaDistTests extends DatabaseTests {

    /*
    @Test
    public void verify_stadist_main_function_returns_correct_value() throws Exception {
        if (!System.getProperty("java.version").startsWith("1.8")) {
            StaDist.main(new String[]{"1", "1", "1", "1", "1", getTestPropFilePath() + "channelList-test.cacheByLocation", DatabaseTests.getHost(),
                    DatabaseTests.getDbName(), DatabaseTests.getDbUsername(), DatabaseTests.getDbPassword(), DatabaseTests.getPortNumber()});
            assertEquals("CJ T0423 HNZ 01    5361.8  39.776667 30.520556 -0.000000\n" +
                    "CJ T0423 HNN 01    5361.8  39.776667 30.520556 -0.000000\n", StaDist.getResult());
        }
    }
     */
}