package test.testqueries.org.trinet.apps;

import test.org.trinet.apps.CalcMLTests;
import test.testqueries.common.TestQueries;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

public class WFviewerTestsQueries extends TestQueries {

    public WFviewerTestsQueries(String URL) throws SQLException {
        super(URL);
    }

    public int populateWaveFormDataToDatabase() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.waveform\n" +
                "(wfid, net, sta, auth, subsource, channel, channelsrc, seedchan, \"location\", archive, datetime_on, datetime_off, samprate, wavetype, fileid, foff, nbytes, traceoff, tracelen, status, wave_fmt, format_id, wordorder, recordsize, locevid, qc_level, lddate)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, 1213985321);
        statement.setString(index++, "BK");
        statement.setString(index++, "THIS");
        statement.setString(index++, "CI");
        statement.setString(index++, "SCDC");
        statement.setString(index++, "BHE");
        statement.setString(index++, "SEED");
        statement.setString(index++, "BHE");
        statement.setString(index++, "00");
        statement.setString(index++, "SCEDC");
        statement.setDouble(index++, 1730292097.15);
        statement.setDouble(index++, 1730292183.2);
        statement.setDouble(index++, 40.0);
        statement.setString(index++, "T");
        statement.setInt(index++, 2112516793);
        statement.setInt(index++, 0);
        statement.setInt(index++, 3072);
        statement.setInt(index++, 0);
        statement.setInt(index++, 3072);
        statement.setString(index++, "T");
        statement.setInt(index++, 2);
        statement.setInt(index++, 11);
        statement.setInt(index++, 1);
        statement.setInt(index++, 512);
        statement.setString(index++, "34865");
        statement.setString(index++, "D");
        statement.setString(index++, "2024-10-30 12:44:38.063");
        return insertQueryHelper(statement);
    }

    public int populateAssocWaEDataToDatabase() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.assocwae\n" +
                "(wfid, evid, datetime_on, datetime_off, lddate)\n" +
                "VALUES(?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, 1213985321);
        statement.setInt(index++, 60253301);
        statement.setDouble(index++, 1730292097.15);
        statement.setDouble(index++, 1730292183.2);
        statement.setString(index++, "2024-10-30 12:44:38.112");
        return insertQueryHelper(statement);
    }

    public int populateEventDataToDatabase() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.\"event\"\n" +
                "(evid, prefor, prefmag, prefmec, commid, auth, subsource, etype, selectflag, \"version\", lddate)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, 60253301);
        statement.setInt(index++, 251931);
        statement.setInt(index++, 490056);
        statement.setNull(index++, Types.INTEGER);
        statement.setNull(index++, Types.INTEGER);
        statement.setString(index++, "CI");
        statement.setString(index++, "RT1");
        statement.setString(index++, "eq");
        statement.setInt(index++, 1);
        statement.setInt(index++, 2);
        statement.setString(index++, "2024-10-30 12:43:00.182");
        return insertQueryHelper(statement);
    }

    public int populateFilenameDataToDatabase() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.filename\n" +
                "(fileid, dfile, datetime_on, datetime_off, nbytes, lddate, subdirid)\n" +
                "VALUES(?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'), ?);";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, 2112516793);
        statement.setString(index++, "BK.THIS.BHE.00");
        statement.setDouble(index++, 1730292097.15);
        statement.setDouble(index++, 1730292183.2);
        statement.setInt(index++, 3072);
        statement.setString(index++, "2024-10-30 12:43:00.182");
        statement.setNull(index++, Types.INTEGER);
        return insertQueryHelper(statement);
    }
}
