package org.trinet.jasi;

import org.trinet.jasi.EW.*;
import org.trinet.jasi.TN.*;

public class TestDataSource extends DataSource {

    protected TestDataSource(String URL, String driver,
                             String username, String password) {
        super(URL, driver, username, password);
    }

    protected TestDataSource(String URL, String driver,
                             String username, String password, String dbName) {
        super(URL, driver, username, password);
    }

    public static final DataSource create(String host) {
        DataSource ds = null;
        if (JasiObject.getDefault() == JasiFactory.EARTHWORM) {
            throw new NullPointerException("TestDataSourceEW class DNE! write source for it!");
        } else if (JasiObject.getDefault() == JasiFactory.TRINET) {
            ds = new TestDataSourceTN(host);
        }
        return ds;
    }

    public static final DataSource create(String host, String dbasename) {
        DataSource ds = null;
        if (JasiObject.getDefault() == JasiFactory.EARTHWORM) {
            throw new NullPointerException("TestDataSourceEW class DNE! write source for it!");
        } else if (JasiObject.getDefault() == JasiFactory.TRINET) {
            ds = new TestDataSourceTN(host, dbasename);
        }
        return ds;
    }

    public static final DataSource create(String host, String dbasename, String username, String password) {
        DataSource ds = null;
        if (JasiObject.getDefault() == JasiFactory.EARTHWORM) {
            throw new NullPointerException("TestDataSourceEW class DNE! write source for it!");
        } else if (JasiObject.getDefault() == JasiFactory.TRINET) {
            ds = new TestDataSourceTN(host, dbasename, username, password);
        }
        return ds;
    }

    public static final DataSource create(String host, String dbasename, String username, String password, String subProtocol) {
        DataSource ds = null;
        if (JasiObject.getDefault() == JasiFactory.EARTHWORM) {
            throw new NullPointerException("TestDataSourceEW class DNE! write source for it!");
        } else if (JasiObject.getDefault() == JasiFactory.TRINET) {
            ds = new TestDataSourceTN(host, dbasename, username, password, subProtocol);
        }
        return ds;
    }

    public static final DataSource create(String host, String dbasename, String username, String password, String subProtocol, String port) {
        DataSource ds = null;
        if (JasiObject.getDefault() == JasiFactory.EARTHWORM) {
            throw new NullPointerException("TestDataSourceEW class DNE! write source for it!");
        } else if (JasiObject.getDefault() == JasiFactory.TRINET) {
            ds = new TestDataSourceTN(host, dbasename, username, password, subProtocol, port);
        }
        return ds;
    }

    public static final DataSource create(String host, String dbasename, String username, String password, String subProtocol, String port, String driver) {
        DataSource ds = null;
        if (JasiObject.getDefault() == JasiFactory.EARTHWORM) {
            throw new NullPointerException("TestDataSourceEW class DNE! write source for it!");
        } else if (JasiObject.getDefault() == JasiFactory.TRINET) {
            ds = new TestDataSourceTN(host, dbasename, username, password, subProtocol, port, driver);
        }
        return ds;
    }

    public static final DataSource create() {
        DataSource ds = null;
        if (JasiObject.getDefault() == JasiFactory.EARTHWORM) {
            throw new NullPointerException("TestDataSourceEW class DNE! write source for it!");
        } else if (JasiObject.getDefault() == JasiFactory.TRINET) {
            ds = new TestDataSourceTN();
        }
        return ds;
    }
}