package org.trinet.util.graphics.table;
import java.text.*;
import java.awt.*;
import java.awt.event.*;
import java.util.EventObject;
import javax.swing.*;
import org.trinet.util.graphics.text.*;

public class FormattedNumberEditor extends AbstractCellEditor {
  protected NumberTextField numberField;
  protected DecimalFormat df;
  protected int fontSize=12;
  protected Font font;

  public FormattedNumberEditor() {
    NumberFormat nf = NumberFormat.getNumberInstance();
    if (nf instanceof DecimalFormat) {
      df = (DecimalFormat) nf;
      df.setGroupingUsed(false);
      df.setDecimalSeparatorAlwaysShown(false);
    }
    df.applyPattern("#0.");
    numberField  = new NumberTextField(0, 12, df);
    numberField.setHorizontalAlignment(NumberTextField.RIGHT);
    font = new Font("Monospaced", Font.PLAIN, fontSize);
    numberField.setFont(font);
    numberField.addActionListener(this);
  }

  public FormattedNumberEditor(String pattern) {
    NumberFormat nf = NumberFormat.getNumberInstance();
    if (nf instanceof DecimalFormat) {
      df = (DecimalFormat) nf;
      df.setGroupingUsed(false);
      df.setDecimalSeparatorAlwaysShown(false);
    }
    if (pattern == null || pattern.length() == 0) pattern = "#0.";
    df.applyPattern(pattern);
    numberField  = new NumberTextField(0, 12, df);
    numberField.setHorizontalAlignment(NumberTextField.RIGHT);
    font = new Font("Monospaced", Font.PLAIN, fontSize);
    numberField.setFont(font);
    numberField.addActionListener(this);
  }

  public FormattedNumberEditor(String pattern, int columns) {
    NumberFormat nf = NumberFormat.getNumberInstance();
    if (nf instanceof DecimalFormat) {
      df = (DecimalFormat) nf;
      df.setGroupingUsed(false);
      df.setDecimalSeparatorAlwaysShown(false);
    }
    if (pattern == null || pattern.length() == 0) pattern = "#0.";
    df.applyPattern(pattern);
    if (columns >= 0) numberField  = new NumberTextField(0, columns, df);
    else numberField  = new NumberTextField(0, 1, df);
    numberField.setHorizontalAlignment(NumberTextField.RIGHT);
    font = new Font("Monospaced", Font.PLAIN, fontSize);
    numberField.setFont(font);
    numberField.addActionListener(this);
  }


  public int getFontSize() {
    return fontSize;
  }

  public void setFontSize(int fontSize) {
    if (fontSize > 0) this.fontSize = fontSize;
    font = font.deriveFont((float) fontSize);
    numberField.setFont(font);
  }

  public void setFont(Font font) {
    if (font != null) this.font = font;
    numberField.setFont(font);
  }

//Override AbstractCellEditor methods

  public boolean startCellEditing(EventObject anEvent) {
    if(anEvent == null) numberField.requestFocus();
    else if (anEvent instanceof MouseEvent) {
      if (((MouseEvent)anEvent).getClickCount() < clickCountToStart)
        return false;
    }
    return true;
  }

  public Component getComponent() {
    return numberField;
  }

  public void setCellEditorValue(Object value) {
    if (value != null) {
//      if (value.getClass() isInstanceOf Object)
        numberField.setValue(value);
    }
    else numberField.setText("");
    this.value = value;
  }

//Override getCellEditorValue method to return a number, not a String:
  public Object getCellEditorValue() {
    switch (numberField.getDataType()) {
      case NumberTextField.INT_TYPE :
        return Integer.valueOf(numberField.getIntValue()) ;
      case NumberTextField.LONG_TYPE :
        return Long.valueOf(numberField.getLongValue()) ;
      case NumberTextField.FLOAT_TYPE :
        return Float.valueOf(numberField.getFloatValue()) ;
      case NumberTextField.DOUBLE_TYPE :
        return Double.valueOf(numberField.getDoubleValue()) ;
      default:
        return numberField.getValue();
    }
  }

  public Component getTableCellEditorComponent( JTable table, Object value, boolean isSelected, int row, int column) {
//    numberField.setText(value.toString());
    setCellEditorValue(value);
    return (Component) numberField;
  }
}
