#!/bin/bash 

# build_jiggle.sh
# This script builds the jiggle.jar executable from compiled .class files and .jar external libraries.
# This script lives in the bin/ directory
# Assumes a "manifest.txt" file lives in the bin/ directory

if [ "$#" -ne 1 ]
then
   echo "Usage: ./build_jiggle.sh <java_version>"
   echo "Possible values of <java_version>: 8, 11"
   exit 1
fi

# Java version from command line
JAVA_VER=$1
echo "JAVA_VER: 1."${JAVA_VER}

case "${JAVA_VER}" in
   8 )
      echo "8"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.8`
#      export JAVA_HOME=`/usr/libexec/java_home -v 1.8.0_202`
      ;;
   11 )
      echo "Using JDK 11"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.11`
      ;;
   17 )
      echo "Using JDK 17"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.17`
      ;;
   * )

      echo "8"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.8`
#      export JAVA_HOME=`/usr/libexec/java_home -v 1.8.0_202`
      ;;
esac

# Input directory for .jar external libraries
LIB_DIR="../lib/"

# Input directory for .class files
CLASS_DIR="../src/out_classes_java"${JAVA_VER}
echo "CLASS_DIR: "${CLASS_DIR}

# Output directory for jiggle.jar file
JAR_DIR="./"

# Output jiggle.jar file name
JAR_FILE="jiggle_p"${JAVA_VER}"g.jar"
echo "JAR_FILE: "${JAR_FILE}

# Build jiggle.jar
jar cmfv manifest${JAVA_VER}.txt ${JAR_FILE} -C ${CLASS_DIR} edu -C ${CLASS_DIR} gov -C ../src images -C ${CLASS_DIR} org -C ${CLASS_DIR} pljava.ddr

