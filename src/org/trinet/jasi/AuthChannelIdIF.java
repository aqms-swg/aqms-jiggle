package org.trinet.jasi;
/** Methods to retrieve NCDC database schema station channel description elements. */
public interface AuthChannelIdIF extends ChannelIdIF {

    public static final int AUTH        = 6;
    public static final int SUBSOURCE   = 7;

    String getAuth();
    String getSubsource();

    void setAuth(String auth);
    void setSubsource(String subsource);
}

