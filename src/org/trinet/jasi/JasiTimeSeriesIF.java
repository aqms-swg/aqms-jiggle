package org.trinet.jasi;

import java.util.*;
import org.trinet.util.*;

public interface JasiTimeSeriesIF extends JasiFactory, TimeSeriesIF, Channelable, DateLookUpIF {
    // super IF to combine other interfaces
    // put special methods below:
}

