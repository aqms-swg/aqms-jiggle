package org.trinet.jiggle.database.event;

public interface EventAssocFactory {
    /**
     * Get the event event associated.
     * 
     * @return return the event associated.
     */
    EventAssoc getEventAssoc();
}
