package org.trinet.jiggle;

import java.awt.event.*;
import java.util.*;
import java.text.*;
import javax.swing.*;
import javax.swing.event.*;
import org.trinet.util.*;

/**
 * A comboBox for selecting how many seconds are visible in the viewport of a
 * ZoomPanel. The selected value will change if the MasterView's SelectedWFViewModel
 * duration is changed. Likewise if the the selected values changes through user input,
 * the MasterView model will be updated to the new value.
 *
 * @author Doug Given
 * */
// Note upon user text enter of a new scale value this sequence:
// jcb edit-actionPerformed->model->setSelected model->firesContentsChanged 
// jcb listener contentsChanged if selection has changed from its old value: 
//   fires 2 itemStateChanged (selected, deselected) &
//   then fires "comboBoxChanged" actionEvent 
// jcb edit-actionPerformed then fires the "comoBoxEdited" actionEvent last.
//
public class ZoomScaleComboBox extends JComboBox // implements Observer
{
    private boolean viewModelSettingScaleValue = false; // signal flag used by listeners

    private String userInput = ""; // default selection value;

    private DecimalFormat df = new DecimalFormat( "##0.00" );  // time scale format

    private MyActionListener actionHandler = new MyActionListener(); // used to store only hand-edit entry

    // list of values in the combobox, reserve the 0th entry for non-list values
    private static final String defList[] = {
      "1.00",
      "2.00",
      "5.00",
      "10.00",
      "20.00",
      "30.00",
      "60.00",
      "90.00"};

    private String valList[] = defList;

      /** The master view, need for MVC interaction. */
    private MasterView mv;

    /** Constructor sets up editable combobox with a default list of scale values.*/
    public ZoomScaleComboBox() {
       this(defList);
    }

    /** Sets up an editable JComboBox whose list values are the input array of strings.
     * The String values must be parseable as either int or double. */
    public ZoomScaleComboBox(String[] list) {

      if (list != null) setList(list);
      setEditable(true);                // allow freeform user input
      //addItemListener(itemHandler);     // mouse drag updater
      addActionListener(actionHandler); // hand-edit list updater
    }

    /** Constructor sets up the default scale list then adds listener to view selected window model.*/
    public ZoomScaleComboBox(MasterView masterview) {
      this();
      mv = masterview;
      mv.masterWFWindowModel.addChangeListener(new WFWindowListener());
    }

    /** Creates a new model containing the input list of time scales. Input Strings must be parseable numbers.
     *  Sets the selected item is the first list element.
     * */
    public void setList(String[] list) {
        setList(userInput, list);
    }
    /** Creates a new model containing the input list of time scales. Adds value of typedInput to top of list.
     *  Sets the selected Item to the first list element, typedInput.
     */
    protected void setList(String typedInput, String[] list) {
      valList = list;
      DefaultComboBoxModel cbm = new DefaultComboBoxModel();
      cbm.addElement(typedInput); // sets model selection to this 1st element
      for (int i = 0; i < valList.length; i++) {
        cbm.addElement(valList[i]);
      }
      setModel(cbm);
      setMaximumRowCount(valList.length+1); // items not displayed in scrolling window
      userInput = typedInput;
      setSelectedItem(typedInput); // fires item event if box selection differ from model, fires action
    }

    /** Set the current value of the selected item equal to this value.
     * Formats it as a double value String of the form  "##0.00". */
    public void setCurrentValue(double val) {
        setCurrentValue(df.format(val));
    }
    /** Set the current value of the selected item equal to this string. */
    public void setCurrentValue(String str) {
        if (getSelectedItem().equals(str)) return; // no change no-op aww

        //System.out.println("DEBUG ZoomScaleComboBox.setCurrentValue to: " +str+ " current selected: "+getSelectedItem());
        DefaultComboBoxModel cbm = (DefaultComboBoxModel) getModel();
        int selIdx = cbm.getIndexOf(str);
        if (selIdx >= 0)  { // its in the list
          setSelectedItem(cbm.getElementAt(selIdx));  // this fires the action, 2 item events 
        } else { // not in the list
         //create a new list model with userInput in the 0 (top) menu position
         setList(str, valList);
        }
        //System.out.println("DEBUG ZoomScaleComboBox.setCurrentValue on exit current selected: "+getSelectedItem());
    }

    /** Return the currently selected item as a String. */
    public String getCurrentValue() {
      return (String) getSelectedItem(); // not necessarily the userInput aww
    }

    /** Return the currently selected item as a double. */
    public double getValue() {
        try {
            return Double.parseDouble(getCurrentValue());
        }
        catch (Exception ex) {return -1;}    // -1 if bad entry
    }


// Would be better if the model fired an event like a "propertyChangeEvent"
// to discriminate type of change to listen for, some model changes
// are not relevent to time scale updates of comboBox. -aww
/** Sets scale selection to match any change of duration in the masterview selected window model. */
    private class WFWindowListener implements ChangeListener {

        public void stateChanged(ChangeEvent changeEvent) {

            // get current model scale
            SelectedWFWindowModel windowModel = mv.masterWFWindowModel;
            double modelScale = windowModel.getTimeSpan().getDuration();
            if (Double.isNaN(modelScale) || modelScale <= 0.) return; // invalid scale

            final String modelItem = df.format(modelScale);

            //System.out.println("DEBUG ZoomScaleComboBox mv.windowModel stateChange timespan duration on entry:"+ modelItem+" current selected: "+getSelectedItem());
            //Unfortunately we don't know if model scale changed so we do this test
            if ( modelItem.equals(getSelectedItem()) ) {
                //System.out.println("DEBUG ZoomScaleComboBox stateChange to identical value, noop return"); 
                return; // no change of interest
            }

            // Invoked in event queue, not sure its ever invoked from other loading thread
            // aww 10/16/2006 removed runnable
            //SwingUtilities.invokeLater(
                //new Runnable() {
                    //public void run() {
                        // flag value change originating from model
                        viewModelSettingScaleValue = true;
                        // only set selection no list change
                        setSelectedItem(modelItem);
                        viewModelSettingScaleValue = false;
                    //}
               //}
            //);
        }
    }
    // Option to use only hand-edited insert overwrites into list box first (0) position
    private class MyActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            //System.out.println("DEBUG ZoomScaleComboBox.MyActionListener action : " + e.getActionCommand());
            if (e.getActionCommand().equals("comboBoxEdited")) {
              comboBoxEdited(e);
            }
            else {
              comboBoxChanged(e);
            }
        }
    }
    private void comboBoxEdited(ActionEvent e) {

        if (mv == null) return; // no view to update

        double secInView = getValue();
        // no-op, if user input too small for current minimum model scale
        if (mv.masterWFWindowModel == null || mv.masterWFWindowModel.getMinTime() > secInView) return;

        // redo list, update 1st list element with hand edited selection
        setList(df.format(secInView), valList); // sets selection to 1st element 

    }

    // Notifies masterview selected window model of scale selection change. 
    private void comboBoxChanged(ActionEvent e) {

        if (mv == null) return; // no view to update
        if (viewModelSettingScaleValue) return; // recursion in progress ignore

        double secsInView = getValue();
        if (secsInView <= 0.) return; // scale is invalid number

        SelectedWFWindowModel windowModel = mv.masterWFWindowModel;
        if (windowModel.getMinTime() > secsInView) return; // scale too small for model

        if (df.format(windowModel.getTimeSpan().getDuration()).equals(getSelectedItem())) {
          return; // scale is same as the current model
        }

        //System.out.println("DEBUG ZoomScaleComboBox.comboBoxChanged windowModel.setTimeSpanDuration to: " +secsInView);

        // scale differs from model, so update the view's WFWindowModel 
        windowModel.setTimeSpanDuration(secsInView);

    }

} // ZoomScaleComboBox
