package org.trinet.web;

import java.text.*;
import java.awt.*;
import java.util.*;
import java.io.*;
import java.awt.event.*;
import javax.swing.*;

import org.trinet.jiggle.MasterView;
import org.trinet.jasi.*;
import org.trinet.util.GenericPropertyList;

public class WASnapGif extends SnapGif {

    protected static boolean showIt = true;

    public static void main (String args[]) {

        if (args.length < 1) { // no args
          System.out.println ("Usage: WASnapGif <evid> [ntraces] [max_secs] [out-file] ");
          System.out.println ("           defaults:  ["+ntraces+"] ["+maxSecs+"] [<evid>.gif] ");
          System.exit(-1);
        }

        long evid = Long.parseLong(args[0]);

        // # of traces
        if (args.length > 1) {
          ntraces = Integer.parseInt(args[1]);
        }

        // max. seconds to plot
        if (args.length > 2) {
          maxSecs= Integer.parseInt(args[2]);
        }

        // destination .gif file
        String gifFile = System.getProperty("user.dir") + GenericPropertyList.FILE_SEP + evid + ".gif";  //default
        if (args.length > 3) {
            gifFile = args[3];
        }

        System.out.println ("Making connection...");
        TestDataSource.create();  // make connection

        // Makes the .gif file, returns the grapical component in case you want to display it below
        Component view = makeGif(evid, ntraces, maxSecs, gifFile);

        // make a frame
        if (view != null && showIt) {

            JFrame frame = new JFrame("SnapGif of "+evid);

            frame.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {System.exit(-3);}
            });

            frame.getContentPane().add(new JScrollPane(view)); // add scroller to frame
            frame.pack();
            frame.setVisible(true);

        } else {

            // Return codes:
            if (view == null) {
                System.err.println ("exit status = -1");
                System.exit(-1);        // failure
            } else {
                System.err.println ("exit status = 0");
                System.exit(0);        // success
            }
        }
    }

    public static Component makeGif(long id, int ntraces, int maxSecs, String outFile) {

        // must make instance because makeViewWithPhases() is not static
        SnapShot snapShot =  new SnapShot();

        // station info
        MasterChannelList.smartLoad();

        if (debug) System.out.println ("Making MasterView for evid = "+id);

        // Make the "superset" MasterView
        MasterView mv = new MasterView();
        mv.setWaveformLoadMode(MasterView.LoadAllInForeground);
        mv.setAlignmentMode(MasterView.AlignOnTime);

        // read in the data!
        mv.defineByDataSource(id);

        Component view = snapShot.makeView(mv);
        view.validate();
        //encodeGifFile(view, outFile);
        return encodeImageFile(view, outFile) ? view : null;

    }

}
