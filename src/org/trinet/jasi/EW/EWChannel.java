
// *** PACKAGE NAME *** \
package org.trinet.jasi.EW;


// *** IMPORTS *** \
import java.util.*;
import java.sql.Connection;
import org.trinet.jasi.EW.SQL.EWSQLStatement;

// individual SQL calls
import org.trinet.jasi.EW.SQL.GetStationListStatement;
import org.trinet.jasi.EW.SQL.GetComponentInfoStatement;

/**
 * <!-- Class Description>
 *
 *
 * Created:  2002/06/21
 *
 * @author   ew_oci2java Tool / DK
 * @version  0.99
 **/



public class EWChannel
{


  // *** CLASS ATTRIBUTES *** \
  public long idChan; // Description
  public long idComp; // Description
  public String sSta; // Description
  public String sComp; // Description
  public String sNet; // Description
  public String sLoc; // Description
  public float dAzm; // Description
  public float dDip; // Description
  public double dLat; // Description
  public double dLon; // Description
  public double dElev; // Description
  public int tReqTime; // Description


  public long idCompT; // Description


  // *** CLASS SQL STATEMENTS *** \
  EWSQLStatement es1;
  EWSQLStatement es2;


  // *** CONSTRUCTORS *** \
  public EWChannel()
  {
    es1 = new GetStationListStatement(org.trinet.jasi.DataSource.getConnection());
    es2 = new GetComponentInfoStatement(org.trinet.jasi.DataSource.getConnection());
  }


  // *** CLASS METHODS *** \
  public void SetConnection(Connection IN_conn)
  {
    es1.SetConnection(IN_conn);
    es2.SetConnection(IN_conn);
  }


  public Vector ReadList()
  {
    Vector ResultList = new Vector();

    es1.CallQueryStatement((Object)this, ResultList);
    return(ResultList);
  }  // end EWChannel:ReadList()


  public EWChannel Read()
  {

    if(es2.CallStatement((Object)this))
      return(this);
    else
      return(null);
  }  // end EWChannel:Read()

}  // end class EWChannel
