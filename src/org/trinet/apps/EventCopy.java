package org.trinet.apps;

import org.trinet.jiggle.*;
import java.awt.*;

import org.trinet.jasi.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.Dimension;

import org.trinet.util.WaveClient;

/**
 * EventCopy: copy an event from one dbase to another.
 *
 *  THIS IS NOT WORKING - HAS CONSTRAINT ERROR PROBLEMS
 *
 * @author Doug Given
 */

public class EventCopy {

    static boolean debug = true;
    //static boolean debug = false;

    static long evid;
    static String toHost;
    static String fromHost;

    public EventCopy() { }


    public static void main(String args[]) {

        if (args.length < 3) {
            System.out.println("Usage: EventCopy <evid> <from-dbasehost> <to-dbasehost>");
            System.exit(0);
    
        } else {
            Long val = Long.valueOf(args[0]);
            evid = (long) val.longValue();
    
            toHost   = args[1];
            fromHost = args[2];
        }
    
        System.out.println ("Making connection...");
        DataSource init = TestDataSource.create();  // make connection
        init.setWriteBackEnabled(true);
    
         //     if (debug) System.err.println ("Checking destination dbase connection: "+ toUrl);
         DataSource toDs =  TestDataSource.create(); 	// make connection
         if (toDs == null) {
            System.out.println ("Could not connect to: "+ toHost);
            System.exit(0);
         }
    
         //     if (debug) System.err.println ("Making source dbase connection: "+ fromUrl);
         DataSource fromDs =  TestDataSource.create(); // make connection
         if (fromDs == null) {
            System.out.println ("Could not connect to: "+ fromHost);
            System.exit(0);
         }
         // Make the "superset" MasterView
         if (debug) System.out.println ("Reading data for evid = "+evid);
         MasterView mv = new MasterView();
         mv.setWaveformLoadMode(MasterView.LoadNone);
    
         mv.defineByDataSource(evid);
    
         // get the first solution in the list
         //	Solution sol = (Solution) mv.solList.solList.get(0);
         Solution sol = mv.getSelectedSolution();
         if (sol != null) {
            if (debug) {
              System.out.println ("SOL: "+sol.toString());
           System.out.println ("There are " +
                                 mv.getPhaseCount() +" phases, "+
                                 mv.getAmpCount() + " amps and "+
                                 mv.getCodaCount()+ " codas.");
            }
         } else {
            System.out.println ("No dbase entry found for evid = "+evid);
            System.exit(0);
         }
    
         // Write it
         fromDs.close();
    
         toDs = TestDataSource.create(); 	// make connection
         toDs.setWriteBackEnabled(true);
    
         // force dbase rewrite
         sol.setUpdate(true);
         sol.magnitude.value.setUpdate(true);
/*
         JasiReading[] jr = sol.phaseList.getArray();
         for (int i = 0; i<jr.length; i++ ) {
           jr[i].setFromDataSource(false);
         }
         jr = sol.ampList.getArray();
         for (int i = 0; i<jr.length; i++ ) {
           jr[i].setFromDataSource(false);
         }
         jr = sol.ampList.getArray();
         for (int i = 0; i<jr.length; i++ ) {
           jr[i].setFromDataSource(false);
         }
*/
         try {
           if (sol.commit()) {
            System.out.println ("Event copied.");
           } else {
            System.out.println ("** Event copy failed. **");
           }
         } catch (JasiCommitException ex) {}
    }
}
