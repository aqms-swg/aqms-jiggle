//NOTE: for speed, only use identifier equivalence checking?  addOrReplaceAll(List)?
//NOTE: associate vs. assign, do we check list contains element or not?
package org.trinet.jasi;
import java.util.*;
public abstract class AbstractMagnitudeAssocList extends JasiReadingList implements MagnitudeAssociatedListIF {

  protected AbstractMagnitudeAssocList() { super(); }
  protected AbstractMagnitudeAssocList(int capacity) { super(capacity); }
  protected AbstractMagnitudeAssocList(Collection col) { super(col); }

  public boolean commit(Magnitude mag) {
    return (mag == null) ? true : getAssociatedWith(mag).commit();
  }

  /** Return a list containing the undeleted elements associated with input
    * Magnitude.
    * Null input returns empty list.
  */
  public MagnitudeAssociatedListIF getAssociatedWith(Magnitude mag) {
    return getAssociatedWith(mag, true);
  }

  /**
    * Return a list of elements associated with input Magnitude.
    * undeletedOnly == true, does not return the elements flagged virtually deleted.
    * Null input Magnitude returns empty list.
   */
  public MagnitudeAssociatedListIF getAssociatedWith(Magnitude mag, boolean undeletedOnly) {
      return getAssociatedWith(mag, undeletedOnly, false);
  }

  public MagnitudeAssociatedListIF getAssociatedWith(Magnitude mag, boolean undeletedOnly, boolean validTypeOnly) {
      MagnitudeAssociatedListIF newList = (MagnitudeAssociatedListIF) newInstance();
      if (mag == null || newList == null) return newList;
      int mySize = size();
      for (int i = 0; i<mySize; i++) {
          JasiMagnitudeAssociationIF jc = (JasiMagnitudeAssociationIF) get(i);
          if (jc.isAssignedTo(mag) ) {
            if ( (undeletedOnly && jc.isDeleted()) || (validTypeOnly && !mag.isTypeFor(jc)) ) continue;
            newList.add(jc);
          }
      }
      return newList;
    }
    public MagnitudeAssociatedListIF getUnassociatedWithMag() {
      return getUnassociatedWithMag(true);
    }
    public MagnitudeAssociatedListIF getUnassociatedWithMag(boolean undeletedOnly) {
        MagnitudeAssociatedListIF newList = (MagnitudeAssociatedListIF) newInstance();
        if (newList == null) return newList;
        int mySize = size();
        for (int i = 0; i<mySize; i++) {
          JasiMagnitudeAssociationIF jc = (JasiMagnitudeAssociationIF) get(i);
          if (! jc.isAssignedToMag()) {
            if (undeletedOnly && jc.isDeleted()) continue;
            else newList.add(jc);
          }
        }
        return newList;
    }

  /** Sets associated Magnitude and associates all elements in the list with it.
   * Associate implementations may append list elements to input Magnitude list.
   * Null input is a no-op.
   * */
  public void associateAllWith(Magnitude mag) {
    if (mag == null) return;
    int mySize = size();
    for (int i = 0; i<mySize; i++) {
      ((JasiMagnitudeAssociationIF)get(i)).associate(mag);
    }
  }
  /**
   * Assigns input Magnitude to all elements in the list. 
   * Unlike associate...() does not add instance references to
   * input Magnitude collections.
   * @see #unassociateAllFromMag().
   * */
  public void assignAllTo(Magnitude mag) {
    int mySize = size();
    for (int i = 0; i<mySize; i++) {
      ((JasiMagnitudeAssociationIF)get(i)).assign(mag);
    }
  }
  /**
   * Assigns input Magnitude to all elements in the list. 
   * Unlike associate...() does not add instance references to
   * input Magnitude collections.
   * Throws an exception if any element is already
   * associated with a different non-null Solution.
   * */
  public static final void assignListTo(List jcList, Magnitude mag) {
    int count = jcList.size();
    for (int i = 0; i<count; i++) {
      ((JasiMagnitudeAssociationIF)jcList.get(i)).assign(mag);
    }
  }
  /** Unassociates all list elements from their associated Magnitude, if any.
   * Elements are removed from associated Magnitude collections. 
   * */
  public void unassociateAllFromMag() {
    for (int i = size()-1; i >= 0; i--) {
      ((JasiMagnitudeAssociationIF) get(i)).unassociateMag();
    }
  }

  /** Unassociates all list elements from input Magnitude, if any.
   * Elements are removed from input Magnitude collections. 
   * */
  public void unassociateAllFrom(Magnitude mag) {
    if (mag == null) return;
    for (int i = size()-1; i >= 0; i--) {
      JasiMagnitudeAssociationIF jc = (JasiMagnitudeAssociationIF) get(i);
      if (jc.isAssignedTo(mag)) jc.unassociateMag();
    }
  }

  /**
  * Return list of undeleted elements of the same type that are
  * associated with the input Magnitude.
  * Ignores Magnitude association is input Magnitude is null.
  * Returns empty list if input type is null.
  */
  public MagnitudeAssociatedListIF getListByType(Magnitude mag, Object type) {
      MagnitudeAssociatedListIF newList = (MagnitudeAssociatedListIF) newInstance();
      if (type == null || newList == null) return newList;
      int mySize = size();
      for (int i = 0; i<mySize; i++) {
        JasiMagnitudeAssociationIF jc = (JasiMagnitudeAssociationIF) get(i);
        if ( mag == null || (jc.isAssignedTo(mag) && ! jc.isDeleted()) ) {
          if (jc.isSameType(type)) newList.add(jc);
        }
      }
      return newList;
  }
  /**
  * Mark all Magnitude associated elements in the list as deleted.
  * Returns the number of deleted elements, does not remove them from list.
  * Null input returns 0.
  */
  public int deleteAll(Magnitude mag) {
    if (mag == null) return 0;
    int knt = 0;
    int mySize = size();
    for (int i = 0; i < mySize; i++) {
      JasiMagnitudeAssociationIF jc = (JasiMagnitudeAssociationIF) get(i);
      //if (jc.isAssignedTo(mag) && jc.setDeleteFlag(true)) knt++;
      //Replaced above to fire "stateChanged" deleted event:
      if (jc.isAssignedTo(mag) &&
          setElementDeleteState(i, true)) knt++;
    }
    return knt;
  }
  public int deleteUnassociatedWithMag() {
    int knt = 0;
    int mySize = size();
    for (int i = 0; i < mySize; i++) {
      JasiMagnitudeAssociationIF jc = (JasiMagnitudeAssociationIF) get(i);
      //if (! jc.isAssignedToMag() && jc.setDeleteFlag(true)) knt++;
      //Replaced above to fire "stateChanged" deleted event:
      if (! jc.isAssignedToMag() &&
          setElementDeleteState(i, true)) knt++;
    }
    return knt;
  }
  /** Remove all elements from list associated with the input Magnitude.
   *  Null input returns 0.
   * */
  public int removeAll(Magnitude mag) {
    if (mag == null) return 0;
    int knt = 0;
    for (int i = size()-1; i >=0 ; i--) {
      JasiMagnitudeAssociationIF jc = (JasiMagnitudeAssociationIF) get(i);
      if (jc.isAssignedTo(mag)) {
       remove(i);
       knt++;
      }
    }
    return knt;
  }
  public int removeUnassociatedWithMag() {
    int knt = 0;
    for (int i = size()-1; i >=0 ; i--) {
      JasiMagnitudeAssociationIF jc = (JasiMagnitudeAssociationIF) get(i);
      if (! jc.isAssignedToMag()) {
       remove(i);
       knt++;
      }
    }
    return knt;
  }
  /** Convenience wrapper combining remove and delete of all elements
   * in list not associated with any Magnitude.  */
  public int eraseUnassociatedWithMag() {
    int knt = 0;
    for (int i = size()-1; i >=0 ; i--) {
      JasiMagnitudeAssociationIF jc = (JasiMagnitudeAssociationIF) get(i);
      if (! jc.isAssignedToMag() ) {
       remove(i); // fire notifies
       //jc.setDeleteFlag(true);
       setElementDeleteState(i, true);
       knt++;
      }
    }
    return knt;
  }

  /**
  * Virtually delete all elements whose residual >= value.
  * Regardless of association.
  * Does not remove them from list.
  */
  public int stripByResidual(double value) {
    return stripByResidual(value, (Magnitude)null);
  }
  /**
  * Virtually delete all elements associated with input Magnitude
  * whose residual >= value. If input Magnitude is null, 
  * all elements satifying condition are virtually deleted.
  * Does not remove them from list.
  */
  public int stripByResidual(double value, Magnitude mag) {
    int mySize = size();
    int knt = 0;
    for (int i = 0; i < mySize; i++) {
      //JasiMagnitudeAssociationIF jc = (JasiMagnitudeAssociationIF) get(i);
      MagnitudeAssocJasiReadingIF jc = (MagnitudeAssocJasiReadingIF) get(i);
      if (jc.isFinal()) continue; // don't remove required? -aww 2013/02/15
      if (mag == null || jc.isAssignedTo(mag)) {
        if ((Math.abs(jc.getResidual()) >= value)) {
          //jc.setDeleteFlag(true);
          setElementDeleteState(i, true);
          knt++;
        }
      }
    }
    return knt;
  }

  /**
  * Virtually delete all elements whose residual >= value.
  * Regardless of association.
  * Does not remove them from list.
  */
  public int stripByMagResidual(double value) {
    return stripByMagResidual(value, (Magnitude)null);
  }
  /**
  * Virtually delete all elements associated with input Magnitude
  * whose residual >= value. If input Magnitude is null, 
  * all elements satifying condition are virtually deleted.
  * Does not remove them from list.
  */
  public int stripByMagResidual(double value, Magnitude mag) {
    int mySize = size();
    int knt = 0;
    for (int i = 0; i < mySize; i++) {
      //JasiMagnitudeAssociationIF jc = (JasiMagnitudeAssociationIF) get(i);
      MagnitudeAssocJasiReadingIF jc = (MagnitudeAssocJasiReadingIF) get(i);
      if (jc.isFinal()) continue; // don't remove required? -aww 2013/02/15
      if (mag == null || jc.isAssignedTo(mag)) {
        if ((Math.abs(jc.getMagResidual()) >= value)) {
          //jc.setDeleteFlag(true);
          setElementDeleteState(i, true);
          knt++;
        }
      }
    }
    return knt;
  }

  /**
  * Virtually delete all elements associated with input Solution
  * whose residual >= value. If input Magnitude is null, 
  * all elements satifying condition are virtually deleted.
  * Does not remove them from list.
  */
  public int stripByMagResidual(double value, Solution sol) {
    int mySize = size();
    int knt = 0;
    for (int i = 0; i < mySize; i++) {
      //JasiMagnitudeAssociationIF jc = (JasiMagnitudeAssociationIF) get(i);
      MagnitudeAssocJasiReadingIF jc = (MagnitudeAssocJasiReadingIF) get(i);
      if (jc.isFinal()) continue; // don't remove required? -aww 2013/02/15
      if (sol == null || jc.isAssignedTo(sol)) {
        if ((Math.abs(jc.getMagResidual()) >= value)) {
          //jc.setDeleteFlag(true);
          setElementDeleteState(i, true);
          knt++;
        }
      }
    }
    return knt;
  }

  /**
  * Virtually delete all elements whose distance > value (km). 
  * Regardless of association.
  * Does not remove them from list.
  */
  public int stripByDistance(double value) {
    return stripByDistance(value, (Magnitude)null);
  }
  /**
  * Virtually delete all elements associated with input Magnitude
  * whose distance > value (km). If input Magnitude is null, 
  * all elements satifying condition are deleted.
  * Does not remove them from list.
  */
  public int stripByDistance(double value, Magnitude mag) {
    int knt = 0;
    int mySize = size();
    for (int i = 0; i < mySize; i++) {
      //JasiMagnitudeAssociationIF jc = (JasiMagnitudeAssociationIF) get(i);
      MagnitudeAssocJasiReadingIF jc = (MagnitudeAssocJasiReadingIF) get(i);
      if (jc.isFinal()) continue; // don't remove required? -aww 2013/02/15
      if (mag == null || jc.isAssignedTo(mag)) {
        //if (jc.getDistance() >= value) { // don't use slant aww 06/11/2004
        if (jc.getHorizontalDistance() >= value) { // aww 06/11/2004
          //jc.setDeleteFlag(true);
          setElementDeleteState(i, true);
          knt++;
        }
      }
    }
    return knt;
  }

  /**
  * Virtually delete all elements whose residual >= value.
  * Regardless of association.
  * Does not remove them from list.
  */
  public int rejectByResidual(double value) {
    return rejectByResidual(value, (Magnitude)null);
  }
  /**
  * Virtually delete all elements associated with input Magnitude
  * whose residual >= value. If input Magnitude is null, 
  * all elements satifying condition are virtually deleted.
  * Does not remove them from list.
  */
  public int rejectByResidual(double value, Magnitude mag) {
    int mySize = size();
    int knt = 0;
    for (int i = 0; i < mySize; i++) {
      MagnitudeAssocJasiReadingIF jc = (MagnitudeAssocJasiReadingIF) get(i);
      if (jc.isFinal()) continue; // don't remove required? -aww 2013/02/15
      if (mag == null || jc.isAssignedTo(mag)) {
        if ((Math.abs(jc.getResidual()) >= value)) {
          jc.setReject(true);
          knt++;
        }
      }
    }
    return knt;
  }

  /**
  * Virtually delete all elements whose residual >= value.
  * Regardless of association.
  * Does not remove them from list.
  */
  public int rejectByMagResidual(double value) {
    return rejectByMagResidual(value, (Magnitude)null);
  }
  /**
  * Virtually delete all elements associated with input Magnitude
  * whose residual >= value. If input Magnitude is null, 
  * all elements satifying condition are virtually deleted.
  * Does not remove them from list.
  */
  public int rejectByMagResidual(double value, Magnitude mag) {
    int mySize = size();
    int knt = 0;
    for (int i = 0; i < mySize; i++) {
      MagnitudeAssocJasiReadingIF jc = (MagnitudeAssocJasiReadingIF) get(i);
      if (jc.isFinal()) continue; // don't remove required? -aww 2013/02/15
      if (mag == null || jc.isAssignedTo(mag)) {
        if ((Math.abs(jc.getMagResidual()) >= value)) {
          jc.setReject(true);
          knt++;
        }
      }
    }
    return knt;
  }

  /**
  * Virtually delete all elements associated with input Magnitude
  * whose residual >= value. If input Magnitude is null, 
  * all elements satifying condition are virtually deleted.
  * Does not remove them from list.
  */
  public int rejectByMagResidual(double value, Solution sol) {
    int mySize = size();
    int knt = 0;
    for (int i = 0; i < mySize; i++) {
      MagnitudeAssocJasiReadingIF jc = (MagnitudeAssocJasiReadingIF) get(i);
      if (jc.isFinal()) continue; // don't remove required? -aww 2013/02/15
      if (sol == null || jc.isAssignedTo(sol)) {
        if ((Math.abs(jc.getMagResidual()) >= value)) {
          jc.setReject(true);
          knt++;
        }
      }
    }
    return knt;
  }

  /**
  * Virtually delete all elements whose distance > value (km). 
  * Regardless of association.
  * Does not remove them from list.
  */
  public int rejectByDistance(double value) {
    return rejectByDistance(value, (Magnitude)null);
  }
  /**
  * Virtually delete all elements associated with input Magnitude
  * whose distance > value (km). If input Magnitude is null, 
  * all elements satifying condition are deleted.
  * Does not remove them from list.
  */
  public int rejectByDistance(double value, Magnitude mag) {
    int knt = 0;
    int mySize = size();
    for (int i = 0; i < mySize; i++) {
      MagnitudeAssocJasiReadingIF jc = (MagnitudeAssocJasiReadingIF) get(i);
      if (jc.isFinal()) continue; // don't remove required? -aww 2013/02/15
      if (mag == null || jc.isAssignedTo(mag)) {
        if (jc.getHorizontalDistance() >= value) {
          jc.setReject(true);
          knt++;
        }
      }
    }
    return knt;
  }

} 
