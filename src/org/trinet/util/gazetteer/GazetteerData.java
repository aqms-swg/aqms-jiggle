package org.trinet.util.gazetteer;
public interface GazetteerData extends Geoidal {
    public String getState();
    public String getName();
    public String getRemark();
}
