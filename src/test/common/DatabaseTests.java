package test.common;

import org.junit.jupiter.api.BeforeAll;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.wait.strategy.LogMessageWaitStrategy;
import org.testcontainers.utility.DockerImageName;
import org.trinet.jasi.DataSource;
import org.trinet.jdbc.datasources.DbaseConnectionDescription;
import org.trinet.util.LeapSeconds;
import test.testqueries.common.DatabaseTestsQueries;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.fail;

public class DatabaseTests extends UnitTests {

    protected static PostgreSQLContainer<?> postgreSQLContainer;

    @BeforeAll
    public static void setUpDatabase() throws Exception {
        Class.forName("org.postgresql.Driver");
        DockerImageName databaseImage = DockerImageName.parse("parchdbtest-docker").withTag("latest")
                .asCompatibleSubstituteFor("postgres").withRegistry("registry.gitlab.com/aqms-swg/aqms-jiggle");
        String logFileName = "/data/postgres/log/" + "postgresql-" + getDayOfWeek() + ".log";
        postgreSQLContainer = new PostgreSQLContainer<>(databaseImage).withExposedPorts(5432)
                .withCommand("/usr/pgsql-14/bin/postgres", "-D", "/data/postgres", "-c", "config_file=/data/postgres/postgresql.conf");
        postgreSQLContainer.setWaitStrategy(new LogMessageWaitStrategy().withRegEx(".*redirecting log output to logging collector process.*"));
        postgreSQLContainer.start();
        postgreSQLContainer.execInContainer("truncate", "-s", "0", logFileName);
        boolean databaseStarted = false;
        int RETRY_COUNT = 45;
        for (int i = 1; i <= RETRY_COUNT; i++) {
            if (Pattern.compile(getCurrentDate() + ".*database system is ready to accept connections")
                    .matcher(postgreSQLContainer.execInContainer("cat", logFileName)
                            .getStdout()).find()) {
                databaseStarted = true;
                break;
            } else {
                Thread.sleep(1000);
            }
        }
        if (!databaseStarted) {
            fail("DATABASE FAILED TO START!");
        }

        postgreSQLContainer.execInContainer("mkdir", getWavearchivePathInContainer());

        DataSource ds = new DataSource();
        DbaseConnectionDescription dbaseDescr = new DbaseConnectionDescription();
        dbaseDescr.setSubprotocol(getSubProtocol());
        dbaseDescr.setDbaseName(DatabaseTests.getDbName());
        dbaseDescr.setHostName(DatabaseTests.getHost());
        dbaseDescr.setDomain(DatabaseTests.getDomain());
        dbaseDescr.setPort(DatabaseTests.getPortNumber());
        dbaseDescr.setUserName(DatabaseTests.getDbUsername());
        dbaseDescr.setPassword(DatabaseTests.getDbPassword());
        dbaseDescr.setDriver(DatabaseTests.getDatabaseDriver());

        // make connection
        ds.set(dbaseDescr);
        LeapSeconds.loadArraysFromDataSource();

        //init filepath
        addWaveServerFilePathToDatabase();
    }

    private static String getDayOfWeek() {
        LocalDate today = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("E");
        String dayOfWeek = today.format(formatter);
        return dayOfWeek;
    }

    private static String getCurrentDate() {
        LocalDate currentDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return currentDate.format(formatter);
    }

    /**
     * Local JDBC URL on Windows (Adam's machine) will be i.e jdbc:postgresql://127.0.0.1:53361/test
     * Gitlab JDBC URL will be i.e jdbc:postgresql://docker:53361/test
     *
     * Grab everything from the end of '1:' or 'r:' until '/test'
     * @return port number
     */
    public static String getPortNumber() {
        if (System.getProperty("os.name").contains("Windows")) {
            if (postgreSQLContainer.getJdbcUrl().contains("localhost")) {
                return postgreSQLContainer.getJdbcUrl().substring(postgreSQLContainer.getJdbcUrl().indexOf("host") + 5, postgreSQLContainer.getJdbcUrl().indexOf("/t"));
            } else {
                return postgreSQLContainer.getJdbcUrl().substring(postgreSQLContainer.getJdbcUrl().indexOf("1:") + 2, postgreSQLContainer.getJdbcUrl().indexOf("/t"));
            }
        } else {
            // For gitlab pipeline
            return postgreSQLContainer.getJdbcUrl().substring(postgreSQLContainer.getJdbcUrl().indexOf("r:") + 2, postgreSQLContainer.getJdbcUrl().indexOf("/t"));
        }
    }

    public static String getDbUsername() {
        return "trinetdb";
    }

    public static String getDbPassword() {
        return "trinetdb_pass";
    }

    public static String getDbName() {
        return "archdb1";
    }

    public static String getDatabaseDriver() {
        return "org.postgresql.Driver";
    }

    public static String getSubProtocol() {
        return "postgresql";
    }

    public static String getHost() {
        return postgreSQLContainer.getHost();
    }

    public static String getDomain() {
        return "";
    }

    private static void addWaveServerFilePathToDatabase() throws Exception {
        new DatabaseTestsQueries(postgreSQLContainer.getJdbcUrl()).addWaverootsFilePathToDatabase();
    }

    public static String getWavearchivePathInContainer() {
        return "/var/lib/pgsql/aqms-db-pg/create/wavearchive";
    }
}