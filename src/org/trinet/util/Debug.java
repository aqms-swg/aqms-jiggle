package org.trinet.util;
import java.io.*;

/** Convenience Wrappers around a static PrintStream to enable discrimination
* between DEBUG output messages and normal output in coding implementations.
* Output can be globally enabled by invoking enable(). Default is disabled.
*/

public class Debug {

/** PrintStream utilized by printXX(...)  methods */
    static PrintStream out = System.out;

/** Default constructor, defaults static PrintStream to System.out */
    public Debug() {}

/** Constructor sets static PrintStream to the specified input stream object. */
    public Debug(PrintStream out) {
        this.out = out;
    }

/** Global printing state */
    static boolean enabled = false;

/** Globally enable printing */
    public final static void enable() {
        enabled = true;
    }

/** Globally disable printing */
    public final static void disable() {
        enabled = false;
    }

/** True if printing is disabled */
    public final static boolean isDisabled() {
        return ! enabled ;
    }

/** True if printing is enabled */
    public final static boolean isEnabled() {
        return enabled ;
    }

/** Print message if isEnabled() || enable == true */
    public final static void print(String message, boolean enable) {
        if (enabled || enable) out.print(message);
    }

/** Print message with newline if isEnabled() || enable == true */
    public final static void println(String message, boolean enable) {
        if (enabled || enable) out.println(message);
    }

/** Print message only if isEnabled() == true */
    public final static void print(String message) {
        if (enabled) out.print(message);
    }

/** Print message with newline only if isEnabled() == true */
    public final static void println(String message) {
        if (enabled) out.println(message);
    }
}
