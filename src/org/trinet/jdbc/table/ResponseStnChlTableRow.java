package org.trinet.jdbc.table;
import java.util.Date;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.*;
import org.trinet.util.EpochTime;

public abstract class ResponseStnChlTableRow extends ResponseStnTableRow {

    public ResponseStnChlTableRow (String tableName, String sequenceName, int maxFields, int[] keyColumnIndex,
                String [] fieldNames, boolean [] fieldNulls, int [] fieldClassIds ) {
        super(tableName, sequenceName, maxFields, keyColumnIndex, fieldNames, fieldNulls, fieldClassIds);
    }

/** Overrides ResponseStnTableRow.clone()
*/
    public Object clone() {
        ResponseStnChlTableRow obj = null;
        obj = (ResponseStnChlTableRow) super.clone();
        return obj;
    }

/** Returns table row count of the active stations for the specified date, net, station, and channel.
* Input date string must be of the form 'yyyy-MM-dd HH:mm:ss'.
* If date == null the date SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) function is used for the active date.
*/
    public int getRowCountByDate(String date, String net, String sta, String seedchan) {
        return getRowCountByDate(EpochTime.stringToDate(date,"yyyy-MM-dd HH:mm:ss"), net, sta, seedchan); // DATE nominal GMT in db - aww 2008/02/12
    }

/** Returns table row count of the active stations for the specified date, net, station, and channel.
* If date == null the date SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) function is used for the active date.
*/
    public int getRowCountByDate(java.util.Date date, String net, String sta, String seedchan) { // DATE nominal GMT in db - aww 2008/02/12
        String whereString = "WHERE NET = " + StringSQL.valueOf(net) +
                        " AND STA = " + StringSQL.valueOf(sta) +
                        " AND SEEDCHAN = " + StringSQL.valueOf(seedchan); 

        String datestr = (date != null) ? StringSQL.toDATE(date) : "SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)";
        whereString += " AND (OFFDATE > " + datestr + " OR OFFDATE = NULL) AND ONDATE <= " + datestr;

        return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
    }

/** Returns table row count of the active stations for the specified net, station, channel, and location.
* Input date string must be of the form 'yyyy-MM-dd HH:mm:ss'.
* If date == null the date SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) function is used for the active date.
*/
    public int getRowCountByDate(String date, String net, String sta, String seedchan, String location) { // DATE nominal GMT in db - aww 2008/02/12
        return getRowCountByDate(EpochTime.stringToDate(date,"yyyy-MM-dd HH:mm:ss"), net, sta, seedchan, location);
    }

/** Returns table row count of the active stations for the specified net, station, channel, and location.
* If date == null the date SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) function is used for the active date.
*/
    public int getRowCountByDate(java.util.Date date, String net, String sta, String seedchan, String location) { // DATE nominal GMT in db - aww 2008/02/12
        String whereString = "WHERE NET = " + StringSQL.valueOf(net) +
                                " AND STA = " + StringSQL.valueOf(sta) +
                                " AND SEEDCHAN = " + StringSQL.valueOf(seedchan) + 
                                " AND LOCATION = " + StringSQL.valueOf(location, false); 

        String datestr = (date != null) ? StringSQL.toDATE(date) : "SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)";
        whereString += " AND (OFFDATE > " + datestr + " OR OFFDATE = NULL) AND ONDATE <= " + datestr;

        return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* WHERE the station channel was active for the specified input date, net, sta, and seedchan.
* Input date string must be of the form 'yyyy-MM-dd HH:mm:ss'.
* A return value of null indicates no data or an error condition.
*/
    public Object getRowsByDate(String date, String net, String sta, String seedchan) {
        return getRowsByDate(EpochTime.stringToDate(date,"yyyy-MM-dd HH:mm:ss"), net, sta, seedchan); // DATE nominal GMT in db - aww 2008/02/12
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* WHERE the station channel was active for the specified input date, net, sta, and seedchan.
* A return value of null indicates no data or an error condition.
*/
    public Object getRowsByDate(java.util.Date date, String net, String sta, String seedchan) { // DATE nominal GMT in db - aww 2008/02/12
        String whereString = "WHERE NET = " + StringSQL.valueOf(net) +
                                " AND STA = " + StringSQL.valueOf(sta) +
                                " AND SEEDCHAN = " + StringSQL.valueOf(seedchan) ; 

        String datestr = (date != null) ? StringSQL.toDATE(date) : "SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)";
        whereString += " AND (OFFDATE > " + datestr + " OR OFFDATE = NULL) AND ONDATE <= " + datestr;

        return getRowsEquals(whereString);
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* WHERE the station channel was active for the specified input date, net, sta, seedchan, and location.
* Input date string must be of the form 'yyyy-MM-dd HH:mm:ss'.
* A return value of null indicates no data or an error condition.
*/
    public Object getRowsByDate(String date, String net, String sta, String seedchan, String location) {
        return getRowsByDate(EpochTime.stringToDate(date,"yyyy-MM-dd HH:mm:ss"), net, sta, seedchan, location); // DATE nominal GMT in db - aww 2008/02/12
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* WHERE the station channel was active for the specified input date, net, sta, seedchan, and location.
* A return value of null indicates no data or an error condition.
*/
    public Object getRowsByDate(java.util.Date date, String net, String sta, String seedchan, String location) { // DATE nominal GMT in db - aww 2008/02/12
        String whereString = "WHERE NET = " + StringSQL.valueOf(net) +
                                " AND STA = " + StringSQL.valueOf(sta) +
                                " AND SEEDCHAN = " + StringSQL.valueOf(seedchan) + 
                                " AND LOCATION = " + StringSQL.valueOf(location, false); 
        String datestr = (date != null) ? StringSQL.toDATE(date) : "SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)";
        whereString += " AND (OFFDATE > " + datestr + " OR OFFDATE = NULL) AND ONDATE <= " + datestr;

        return getRowsEquals(whereString);
    }

// DataStnChl methods
/** Returns a DataStnChl object derived from the station channel data stored in this object instance, else returns null.
*/
    public DataStnChl getDataStnChl() {
        return new DataStnChl(
                (DataString) fields.get(findFieldIndex("STA")),
                (DataString) fields.get(findFieldIndex("NET")),
                (DataString) fields.get(findFieldIndex("")),
                (DataString) fields.get(findFieldIndex("")),
                (DataString) fields.get(findFieldIndex("CHANNEL")),
                (DataString) fields.get(findFieldIndex("CHANNELSRC")),
                (DataString) fields.get(findFieldIndex("SEEDCHAN")),
                (DataString) fields.get(findFieldIndex("LOCATION")));
    }

/** Sets station channel data for this object instance to the DataObjects members of the input DataStnChl object;
* a null input results in a no-op.
*/
    public void setDataStnChl(DataStnChl obj) {
        if (obj == null) return;
        fields.set(findFieldIndex("STA"), ((DataString) obj.getDataObject(DataStnChl.STA)).clone());
        fields.set(findFieldIndex("NET"), ((DataString) obj.getDataObject(DataStnChl.NET)).clone());
        fields.set(findFieldIndex("CHANNEL"), ((DataString) obj.getDataObject(DataStnChl.CHANNEL)).clone());
        fields.set(findFieldIndex("CHANNELSRC"), ((DataString) obj.getDataObject(DataStnChl.CHANNELSRC)).clone());
        fields.set(findFieldIndex("SEEDCHAN"), ((DataString) obj.getDataObject(DataStnChl.SEEDCHAN)).clone());
        fields.set(findFieldIndex("LOCATION"), ((DataString) obj.getDataObject(DataStnChl.LOCATION)).clone());
    }

}

