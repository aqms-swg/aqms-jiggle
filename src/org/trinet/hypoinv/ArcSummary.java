package org.trinet.hypoinv;
import org.trinet.util.*;
import java.lang.Math;

public class ArcSummary {
  public int year;
  public int month;
  public int day;
  public int hr;
  public int mn;
  public float sec;
  public float latd;
  public char ns;
  public float latm;
  public float lond;
  public char ew;
  public float lonm;
  public float depth;
  public float s_amp_mag;
  public int npsfw;
  public int gap;
  public float dmin;
  public float rms;
  public float az_errs;
  public float dip_errs;
  public float km_errs;
  public float az_erri;
  public float dip_erri;
  public float km_erri;
  public float coda_mag;
  public String locrmk;
  public float km_errm;
  public String auxrmk;
  public int nsw;
  public float km_errh;
  public float km_errv;
  public int nfirst;
  public float smagw;
  public float durmagw;
  public float mad_smag;
  public float mad_durmag;
  public String cdm;
  public char lauth;
  public char mcps_dsc;
  public char mcdur_dsc;
  public char mcamp_dsc;
  public char cdmtc;
  public int nps;
  public char smtc;
  public char xmtc;
  public float exmag;
  public float exmagw;
  public char aamtc;
  public float aamag;
  public float aamagw;
  public long evid;
  public char pmtc;
  public float prefmag;
  public float prefmagw;
  public char acdmtc;
  public float acdmag;
  public float acdmagw;
  public char state;
  public char hreview;
  public String cmodId;
  public String vmodId;

  public static int MAX_ARC_SUMMARY_CHARS=164; // should be 168 
  public boolean parseStatus;
  public static String titleStr = "   EVID    DATE       TIME         LAT       LON        DEPTH   RMS   ERH   ERZ   DMIN GAP  NPS  NWR  NFM MOD AUTH VMODID";

  public ArcSummary () {
  }

  public ArcSummary(String hdr) {
    parseArcSummary(hdr);
  }

  public boolean parseArcSummary(String hdr) {
    parseStatus = false;
    if (hdr.length() < MAX_ARC_SUMMARY_CHARS) {  // 168 OK
      System.out.println("parseArcSummary: Arc summary record length is "+ hdr.length()+
                         " - not equal to MAX_ARC_SUMMARY_CHARS." );
      System.out.println("\"" + hdr + "\"");
      return parseStatus;
    }
    year = ParseString.getInt(hdr, 0, 4);
    month = ParseString.getInt(hdr, 4, 6);
    day = ParseString.getInt(hdr, 6, 8);
    hr = ParseString.getInt(hdr, 8, 10);
    mn = ParseString.getInt(hdr, 10, 12);
    sec = ParseString.getIntToFloat(hdr, 12, 16, 2);
    latd = ParseString.getIntToFloat(hdr, 16, 18, 0);
    ns = ParseString.getChar(hdr, 18);
    latm = ParseString.getIntToFloat(hdr, 19, 23, 2);
    lond = ParseString.getIntToFloat(hdr, 23, 26, 0);
    ew = ParseString.getChar(hdr, 26);
    lonm = ParseString.getIntToFloat(hdr, 27, 31, 2);
    depth = ParseString.getIntToFloat(hdr, 31, 36, 2);
    s_amp_mag = ParseString.getIntToFloat(hdr, 36, 39, 2);
    npsfw = ParseString.getInt(hdr, 39, 42);
    gap = ParseString.getInt(hdr, 42, 45);
    dmin = ParseString.getIntToFloat(hdr, 45, 48, 0);
    rms = ParseString.getIntToFloat(hdr, 48, 52, 2);
    az_errs = ParseString.getIntToFloat(hdr, 52, 55, 0);
    dip_errs = ParseString.getIntToFloat(hdr, 55, 57, 0);
    km_errs = ParseString.getIntToFloat(hdr, 57, 61, 2);
    az_erri = ParseString.getIntToFloat(hdr, 61, 64, 0);
    dip_erri = ParseString.getIntToFloat(hdr, 64, 66, 0);
    km_erri = ParseString.getIntToFloat(hdr, 66, 70, 2);
    coda_mag = ParseString.getIntToFloat(hdr, 70, 73, 2);
    locrmk = ParseString.getString(hdr, 73, 76);
    km_errm = ParseString.getIntToFloat(hdr, 76, 80, 2);
    auxrmk = ParseString.getString(hdr, 80, 82);
    nsw = ParseString.getInt(hdr, 82, 85);
    km_errh = ParseString.getIntToFloat(hdr, 85, 89, 2);
    km_errv = ParseString.getIntToFloat(hdr, 89, 93, 2);
    nfirst = ParseString.getInt(hdr, 93, 96);
    smagw = ParseString.getIntToFloat(hdr, 96, 100, 1);
    durmagw = ParseString.getIntToFloat(hdr, 100, 104, 1);
    mad_smag = ParseString.getIntToFloat(hdr, 104, 107, 2);
    mad_durmag = ParseString.getIntToFloat(hdr, 107, 110, 2);
    cdm = ParseString.getString(hdr, 110, 113);
    lauth = ParseString.getChar(hdr, 113);
    mcps_dsc = ParseString.getChar(hdr, 114);
    mcdur_dsc = ParseString.getChar(hdr, 115);
    mcamp_dsc = ParseString.getChar(hdr, 116);
    cdmtc = ParseString.getChar(hdr, 117);
    nps = ParseString.getInt(hdr, 118, 121);
    smtc = ParseString.getChar(hdr, 121);
    xmtc = ParseString.getChar(hdr, 122);
    exmag = ParseString.getIntToFloat(hdr, 123, 126, 2);
    exmagw = ParseString.getIntToFloat(hdr, 126, 129, 1);
    aamtc = ParseString.getChar(hdr, 129);
    aamag = ParseString.getIntToFloat(hdr, 130, 133, 2);
    aamagw = ParseString.getIntToFloat(hdr, 133, 136, 1);
    evid = ParseString.getLong(hdr, 136, 146);
    pmtc = ParseString.getChar(hdr, 146);
    prefmag = ParseString.getIntToFloat(hdr, 147, 150, 2);
    prefmagw = ParseString.getIntToFloat(hdr, 150, 154, 1);
    acdmtc = ParseString.getChar(hdr, 154);
    acdmag = ParseString.getIntToFloat(hdr, 155, 158, 2);
    acdmagw = ParseString.getIntToFloat(hdr, 158, 162, 1);
    state = ParseString.getChar(hdr,162);

    hreview = ParseString.getChar(hdr,163);

    // Some records exactly 164 chars
    if (hdr.length() > 164) //aww 11/09/2006 - revised 12/21/2007 to check for last
      cmodId = ParseString.getString(hdr,164,166);
    if (hdr.length() > 166) //aww 11/09/2006 - revised 12/21/2007 to check for last
      vmodId = ParseString.getString(hdr,166,168);

    parseStatus = true;
    return parseStatus;

  }

  public String toArcSummaryString() {
    StringBuffer sb = new StringBuffer(164);
    //Concat cc = new Concat();
    sb = Concatenate.format(sb, year, 4, 4);
    sb = Concatenate.format(sb, (long) month, 2, 2);
    sb = Concatenate.format(sb, (long) day, 2, 2);
    sb = Concatenate.format(sb, (long) hr, 2, 2);
    sb = Concatenate.format(sb, (long) mn, 2, 2);
    sb = Concatenate.format(sb, (long) Math.round(sec*100.), 4, 4);
    sb = Concatenate.format(sb, (long) latd, 2);
    sb = sb.append(ns);
    sb = Concatenate.format(sb, (long) Math.round(latm*100.), 4);
    sb = Concatenate.format(sb, (long) lond, 3);
    sb = sb.append(ew);
    sb = Concatenate.format(sb, (long) Math.round(lonm*100.), 4);
    sb = Concatenate.format(sb, (long) Math.round(depth*100.), 5);
    sb = Concatenate.format(sb, (long) Math.round(s_amp_mag*100.), 3);
    sb = Concatenate.format(sb, (long) npsfw, 3);
    sb = Concatenate.format(sb, (long) gap, 3);
    sb = Concatenate.format(sb, (long) dmin, 3);
    sb = Concatenate.format(sb, (long) Math.round(rms*100.), 4);
    sb = Concatenate.format(sb, (long) az_errs, 3);
    sb = Concatenate.format(sb, (long) dip_errs, 2);
    sb = Concatenate.format(sb, (long) Math.round(km_errs*100.), 4);
    sb = Concatenate.format(sb, (long) az_erri, 3);
    sb = Concatenate.format(sb, (long) dip_erri, 2);
    sb = Concatenate.format(sb, (long) Math.round(km_erri*100.), 4);
    sb = Concatenate.format(sb, (long) Math.round(coda_mag*100.), 3);
    try {
      sb = sb.append(locrmk.substring(0,3));
    }
    catch  (StringIndexOutOfBoundsException e) {
      System.out.println ("toArcSummaryString: String out of bounds error for locrmk:\"" + locrmk + "\"");
    }
    sb = Concatenate.format(sb, (long) Math.round(km_errm*100.), 4);
    try {
      sb = sb.append(auxrmk.substring(0,2));
    }
    catch  (StringIndexOutOfBoundsException e) {
      System.out.println ("toArcSummaryString: String out of bounds error for auxrmk:\"" + auxrmk + "\"");
    }
    sb = Concatenate.format(sb, (long) nsw, 3);
    sb = Concatenate.format(sb, (long) Math.round(km_errh*100.), 4);
    sb = Concatenate.format(sb, (long) Math.round(km_errv*100.), 4);
    sb = Concatenate.format(sb, (long) nfirst, 3);
    sb = Concatenate.format(sb, (long) Math.round(smagw*10.), 4);
    sb = Concatenate.format(sb, (long) Math.round(durmagw*10.), 4);
    sb = Concatenate.format(sb, (long) Math.round(mad_smag*100.), 3);
    sb = Concatenate.format(sb, (long) Math.round(mad_durmag*100.), 3);
    try {
      sb = sb.append(cdm.substring(0,3));
    }
    catch  (StringIndexOutOfBoundsException e) {
      System.out.println ("toArcSummaryString: String out of bounds error for cdm:\"" + cdm + "\"");
    }
    sb = sb.append(lauth);
    sb = sb.append(mcps_dsc);
    sb = sb.append(mcdur_dsc);
    sb = sb.append(mcamp_dsc);
    sb = sb.append(cdmtc);
    sb = Concatenate.format(sb, (long) nps, 3);
    sb = sb.append(smtc);
    sb = sb.append(xmtc);
    sb = Concatenate.format(sb, (long) Math.round(exmag*100.), 3);
    sb = Concatenate.format(sb, (long) Math.round(exmagw*10.), 3);
    sb = sb.append(aamtc);
    sb = Concatenate.format(sb, (long) Math.round(aamag*100.), 3);
    sb = Concatenate.format(sb, (long) Math.round(aamagw*10.), 3);
    sb = Concatenate.format(sb, (long) evid, 10);
    sb = sb.append(pmtc);
    sb = Concatenate.format(sb, (long) Math.round(prefmag*100.), 3);
    sb = Concatenate.format(sb, (long) Math.round(prefmagw*10.), 4);
    sb = sb.append(acdmtc);
    sb = Concatenate.format(sb, (long) Math.round(acdmag*100.), 3);
    sb = Concatenate.format(sb, (long) Math.round(acdmagw*10.), 4);
    sb = sb.append(state);
    sb = sb.append(hreview);
    try {
      sb = sb.append(cmodId.substring(0,2));
    }
    catch  (StringIndexOutOfBoundsException e) {
      System.out.println ("toArcSummaryString: String out of bounds error for cmodId:\"" + cmodId + "\"");
    }
    try {
      sb = sb.append(vmodId.substring(0,2));
    }
    catch  (StringIndexOutOfBoundsException e) {
      System.out.println ("toArcSummaryString: String out of bounds error for vmodId:\"" + vmodId + "\"");
    }

    return sb.toString();

  }

  public void printArcSummaryString() {
    System.out.println(toArcSummaryString());
  }

  public static String getTitle() {
    return titleStr;
  }

  public static void printTitle() {
// 1234567890 2000 23:23 12.34 +32 12.34 -118 12.34 99.99 99.99 99.99 99.99 123 1234 1234 1234 abc
    System.out.println(titleStr);
  }

  public String getFormattedErrorEllipseString() {
    if (! parseStatus) return "";
    StringBuffer sb = new StringBuffer(132);
    //Concat cc = new Concat();
    sb = sb.append("ERROR ELLIPSE: < " );
    sb = Concatenate.format(sb, (double) km_errs, 2, 2) ; 
    sb = sb.append(" ") ;
    sb = Concatenate.format(sb, (double) az_errs, 3, 0) ; 
    sb = sb.append(" ") ;
    sb = Concatenate.format(sb, (double) dip_errs, 2, 0) ; 
    sb = sb.append(" >-< ") ;
    sb = Concatenate.format(sb, (double) km_erri, 2, 2) ; 
    sb = sb.append(" ") ;
    sb = Concatenate.format(sb, (double) az_erri, 3, 0) ; 
    sb = sb.append(" ") ;
    sb = Concatenate.format(sb, (double) dip_erri, 2, 0) ; 
    sb = sb.append(" >-< ") ;
    sb = Concatenate.format(sb, (double) km_errm, 2, 2) ; 
    sb = sb.append(" >") ;
    return sb.toString();
  }

  public void printFormattedErrorEllipse() {
    System.out.println(getFormattedErrorEllipseString());
  }

  public String getFormattedOriginString() {
    if (! parseStatus) return "";
    StringBuffer sb = new StringBuffer(132);
    //Concat cc = new Concat();

    // EVID    DATE ORIGIN TIME LAT       LON  DEPTH RMS   ERH   ERZ   DMIN  GAP NPS  NWR  NFM  MOD AUTH VMODID
    sb = Concatenate.format(sb, (long) evid, 10);
    sb = sb.append(" ");
    sb = Concatenate.format(sb, (long) year, 4, 4); 
    sb = sb.append("-");
    sb = Concatenate.format(sb, (long) month, 2, 2); 
    sb = sb.append("-");
    sb = Concatenate.format(sb, (long) day, 2, 2); 
    sb = sb.append(" ");
    sb = Concatenate.format(sb, (long) hr, 2, 2); 
    sb = sb.append(":");
    sb = Concatenate.format(sb, (long) mn, 2, 2); 
    sb = sb.append(":");
    sb = Concatenate.format(sb, (double) sec, 2, 2, 2);
    sb = Concatenate.format(sb, (long) latd, 4);
    sb = Concatenate.format(sb, (double) latm, 3, 2);
    sb = Concatenate.format(sb, (long) lond, 5);
    sb = Concatenate.format(sb, (double) lonm, 3, 2);
    sb = Concatenate.format(sb, (double) depth, 4, 2);
    sb = Concatenate.format(sb, (double) rms, 3, 2);
    sb = Concatenate.format(sb, (double) km_errh, 3, 2);
    sb = Concatenate.format(sb, (double) km_errv, 3, 2);
    sb = Concatenate.format(sb, (double) dmin, 4, 2);
    sb = Concatenate.format(sb, (long) gap, 4);
    sb = Concatenate.format(sb, (long) nps, 5);
    sb = Concatenate.format(sb, (long) npsfw, 5);
    sb = Concatenate.format(sb, (long) nfirst, 5);
    sb = sb.append(" ");
    sb = sb.append(cdm);
    sb = sb.append("   ");
    sb = sb.append(cmodId);
    sb = sb.append("   ");
    sb = sb.append(vmodId);
    return sb.toString();
  }

  public void printFormattedOrigin() {
    System.out.println(getFormattedOriginString());
  }

  public void printFormattedOrigin(String hdr) {
    parseArcSummary(hdr);
    printFormattedOrigin();
  }

  public void printFormattedSummary() {
    printFormattedErrorEllipse();
    printFormattedSummary();
  }

  public void printFormattedSummary(String hdr) {
    parseArcSummary(hdr);
    printFormattedSummary();
  }

}
