package test.org.trinet.jiggle.database.event;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.trinet.jiggle.common.type.AssocEvent;
import org.trinet.jiggle.database.event.EventAssoc;
import org.trinet.jiggle.database.event.EventAssocDb;
import org.trinet.jiggle.database.event.EventAssocDbCache;
import test.common.DatabaseTests;
import test.testqueries.org.trinet.jiggle.database.events.EventAssocDbCacheTestsQueries;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.junit.jupiter.api.Assertions.*;

public class EventAssocDbCacheTests extends DatabaseTests {

    // Delete event ids
    public static final long deleteEventId = 60205810L;
    public static final long deleteEventId2 = 105309101L;

    // Get all event ids
    public static final long getAllEventId = 60205811L;
    public static final long getAllEventId2 = 105309311L;

    // Get all event ids
    public static final long getAssociatedEventId = 60205812L;
    public static final long getAssociatedEventId2 = 105309312L;

    // Get all event ids
    public static final long getCommentEventId = 60205813L;
    public static final long getCommentEventId2 = 105309313L;
    public static final long getCommentCommId = 1073L;

    // Insert Event ids
    public static final long insertEventId = 60205814L;
    public static final long insertEventId2 = 105309314L;

    // Is Exist Event ids
    public static final long isExistEventId = 60205815L;
    public static final long isExistEventId2 = 105309315L;

    private static EventAssoc eventAssocDbCache;

    @BeforeAll
    public static void initTests() throws Exception {
        eventAssocDbCache = EventAssocDbCache.getEventAssoc();
        EventAssocDbCacheTestsQueries eventAssocDbCacheTestsQueries = new EventAssocDbCacheTestsQueries(postgreSQLContainer.getJdbcUrl());

        // Delete test set up
        int rows = eventAssocDbCacheTestsQueries.populateEventTableForDeleteTest();
        assertTrue(rows > 0);
        rows = eventAssocDbCacheTestsQueries.populateEvent2TableForDeleteTest();
        assertTrue(rows > 0);
        rows = eventAssocDbCacheTestsQueries.populateAssocEventTableForDeleteTest();
        assertTrue(rows > 0);

        // Get all test set up
        rows = eventAssocDbCacheTestsQueries.populateEventTableForGetAllTest();
        assertTrue(rows > 0);
        rows = eventAssocDbCacheTestsQueries.populateEvent2TableForGetAllTest();
        assertTrue(rows > 0);
        rows = eventAssocDbCacheTestsQueries.populateAssocEventTableForGetAllTest();
        assertTrue(rows > 0);

        // Get associated test set up
        rows = eventAssocDbCacheTestsQueries.populateEventTableForGetAssociatedTest();
        assertTrue(rows > 0);
        rows = eventAssocDbCacheTestsQueries.populateEvent2TableForGetAssociatedTest();
        assertTrue(rows > 0);
        rows = eventAssocDbCacheTestsQueries.populateAssocEventTableForGetAssociatedTest();
        assertTrue(rows > 0);

        // Get comment test set up
        rows = eventAssocDbCacheTestsQueries.populateEventTableForGetCommentTest();
        assertTrue(rows > 0);
        rows = eventAssocDbCacheTestsQueries.populateEvent2TableForGetCommentTest();
        assertTrue(rows > 0);
        rows = eventAssocDbCacheTestsQueries.populateAssocEventTableForGetCommentTest();
        assertTrue(rows > 0);

        // Insert event set up
        rows = eventAssocDbCacheTestsQueries.populateEventTableForInsertTest();
        assertTrue(rows > 0);
        rows = eventAssocDbCacheTestsQueries.populateEvent2TableForInsertTest();
        assertTrue(rows > 0);

        // Is Exist test set up
        rows = eventAssocDbCacheTestsQueries.populateEventTableForIsExistTest();
        assertTrue(rows > 0);
        rows = eventAssocDbCacheTestsQueries.populateEvent2TableForIsExistTest();
        assertTrue(rows > 0);
        rows = eventAssocDbCacheTestsQueries.populateAssocEventTableForIsExistTest();
        assertTrue(rows > 0);
    }

    @Test
    public void verify_delete_associated_events_works_as_intended() {
        assertTrue(eventAssocDbCache.deleteAssociatedEvent(deleteEventId));
    }

    @Test
    public void verify_get_all_events_works_as_intended() {
        assertTrue(eventAssocDbCache.getAllEventId(getAllEventId).contains(getAllEventId));
    }

    @Test
    public void verify_get_associated_events_works_as_intended() {
        assertEquals(getAssociatedEventId, eventAssocDbCache.getAssociatedEvents(getAssociatedEventId).get(0).getMasterEventId());
    }

    @Test
    public void verify_get_comment_works_as_intended() {
        assertEquals(getCommentCommId, eventAssocDbCache.getComment(getCommentEventId).getCommentId());
    }

    @Test
    public void verify_insert_associated_event_works_as_intended() {
        AssocEvent test = new AssocEvent();
        test.setMasterEventId(insertEventId);
        test.setAssocEventId(insertEventId2);
        test.setCommentId(getCommentEventId);
        assertTrue(eventAssocDbCache.insertAssociatedEvent(test));
    }

    @Test
    public void verify_is_exist_works_as_intended() {
        assertNotNull(eventAssocDbCache.isExist(isExistEventId, isExistEventId2));
    }

}
