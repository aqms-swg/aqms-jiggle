package org.trinet.jasi.engines;

public class HypoinvWeightedMedian extends Object {
/**
Computes the weighted median of N values (like magnitudes).
All weights must be positve, but need not be normalized.
Input array elements are rearranged in sorted order by method.<br>
      n   The number values in v and wt arrays.<br>
      v   Array of values whose median is to be found.<br>
      wt  Array of weights corresponding to value indices.<br>
*/

public static double calcMedian(int n, double [] v, double [] wt) {

      if (n < 2) return v[0];

//Get halfWts value, the sum total of positive weights then divide by 2
      double halfWts = wt[0];
      for (int i=1; i < n; i++) {
        halfWts += wt[i];
      }
      halfWts = halfWts*.5;

//First sort the values of k in ascending order
      //int [] sIdx = IndexSort.getSortedIndexes(v); 
      sort2(n, v, wt);

//Find the index i of the weight at or below the halfway point
      double closestToHalfWts =wt[0];
      int idx = 0;
      for (int i=1; i < n; i++) {
        if (closestToHalfWts +.5*wt[i] > halfWts) break;
        closestToHalfWts += wt[i];
        idx = i;
      }

//Half the total weight x is now at idx or between idx & idx+1
//The cumulative weight at idx is closestToHalfWts-.5*wt(idx)
//The cumulative weight at i+1 is closestToHalfWts+.5*wt(idx+1)
//Use linear interpolation between idx and idx+1 to estimate median value
//System.out.println( "value: "+v[idx]+"diff: "+(v[idx+1]-v[idx])+" ratio: "+(2.*(halfWts-closestToHalfWts)+wt[idx])/(wt[idx+1]+wt[idx]));
      return v[idx]+(v[idx+1]-v[idx])*(2.*(halfWts-closestToHalfWts)+wt[idx])/(wt[idx+1]+wt[idx]);
  }

//CONVERTED FROM FRED KLINE'S HYP2000 FORTRAN SOURCE CODE IN MEDWT.FOR
//WHICH WAS ADAPTED FROM A NUMERICAL RECIPES BOOK FOR A HEAPSORT.
//BELOW KA ARRAY IS REARRANGED IN ASCENDING ORDER, KB IS PUT IN THE SAME INDEX ORDER AS KA.
  private static void sort2(int N, double [] KA, double [] KB) {
      double KKA = 0.;
      double KKB = 0.;
      int L=N/2+1; // middle element, plus one 
      int IR=N; // at bottom of sort elements

      while (true) { // loop until break returns
          if (L>1) { // still have elements to test for move
              L=L-1;
              KKA=KA[L-1]; // save
              KKB=KB[L-1];
          } else { // swap 1st with current bottom
              KKA=KA[IR-1];
              KKB=KB[IR-1];
              KA[IR-1]=KA[0];
              KB[IR-1]=KB[0];
              IR=IR-1;  // next up 1 towards array beginning
              if (IR==1) { // done
                KA[0]=KKA;
                KB[0]=KKB;
                break; // exit loop here
              }
          }
          int I=L;
          int J=L+L; // double I
          while (J<=IR) {
              if (J<IR) {
                if (KA[J-1] < KA[J]) J=J+1;
              }
              if (KKA < KA[J-1]) {
                KA[I-1]=KA[J-1];
                KB[I-1]=KB[J-1];
                I=J;
                J=J+J; // double I
              } else {
                J=IR+1; // break; // ? why increment in fortran ?
              }
         }
         KA[I-1]=KKA;
         KB[I-1]=KKB;
      } // end of infinite loop
      return;
    }
/*
  public static final class Tester {
    public static final void main(String [] args) {
        double equalWts [] =   {.5,.5,.5,.5,.5,.5,.5,.5,.5, .5, .5, .5, .5,.5,.5};
        double biasedHiWts [] =  {1.,.1,1.,.1,1.,.1,.5,.2,.4, .3, .1, .1, .1,.2,.8};
        double biasedLoWts [] =  {.01,.1,.01,.08,.01,.06,.01,.02,.01, .02, .03, .1, .1,.08,.02};
        double xtest [] = {13.,2.,12.,4.,11.,6.,10.,8.,9.,7.,5.,3.,1.,0.,14.};
        double median = calcMedian(xtest.length, xtest, equalWts);
        System.out.println("equal wts median: " + median);
        xtest = new double [] {13.,2.,12.,4.,11.,6.,10.,8.,9.,7.,5.,3.,1.,0.,14.};
        median = calcMedian(xtest.length, xtest, biasedHiWts);
        System.out.println("biased high wts median: " + median);
        xtest = new double [] {13.,2.,12.,4.,11.,6.,10.,8.,9.,7.,5.,3.,1.,0.,14.};
        median = calcMedian(xtest.length, xtest, biasedLoWts);
        System.out.println("biased low wts median: " + median);
        System.out.println("Low wts sorted order:\n");
        StringBuffer sb = new StringBuffer(256);
        for (int j=0;j<xtest.length;j++) {
          sb.append(xtest[j]).append(":").append(biasedLoWts[j]).append(" ");
        }
        System.out.println(sb.toString());
        xtest = new double [] {13.,2.,12.,4.,11.,6.,10.,8.,9.,7.,5.,3.,1.,0.,14.};
        biasedLoWts = new double [] {2.5,25.,2.5,20.,2.5,15.,2.5,5.0,2.5,5.0,7.5,25.,25.,20.,5.};
        median = calcMedian(xtest.length, xtest, biasedLoWts);
        System.out.println("biased low factor wts median: " + median);
        System.out.println("Low wts sorted order:\n");
        sb = new StringBuffer(256);
        for (int j=0;j<xtest.length;j++) {
          sb.append(xtest[j]).append(":").append(biasedLoWts[j]).append(" ");
        }
    }
  }
*/
}
