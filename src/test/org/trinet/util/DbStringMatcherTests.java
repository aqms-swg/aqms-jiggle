package test.org.trinet.util;

import jregex.Pattern;
import jregex.Replacer;
import org.junit.jupiter.api.Test;
import org.trinet.util.DbStringMatcher;
import test.common.UnitTests;

import static org.junit.jupiter.api.Assertions.*;

public class DbStringMatcherTests extends UnitTests {

    DbStringMatcher sm = new DbStringMatcher(new String [] {"HHZ","HLZ","HLE"});
    DbStringMatcher sm2 = new DbStringMatcher(new String [] {"H_Z","BLZ","ABC"});
    DbStringMatcher sm3 = new DbStringMatcher(new String [] {"DEF","BLZ","H%"});
    DbStringMatcher sm4 = new DbStringMatcher(new String [] {"DEF","BLZ","H%"}, Pattern.IGNORE_CASE);

    @Test
    public void HHZ_sm_matches_returns_true() {
        assertTrue(sm.matches("HHZ"));
    }

    @Test
    public void HLZ_sm_matches_returns_true() {
        assertTrue(sm.matches("HLZ"));
    }

    @Test
    public void HLE_sm_matches_returns_true() {
        assertTrue(sm.matches("HLE"));
    }

    @Test
    public void ABC_sm_matches_returns_false() {
        assertFalse(sm.matches("ABC"));
    }

    @Test
    public void H_Z_sm2_matches_HHZ_returns_true() {
        assertTrue(sm2.matches("HHZ"));
    }

    @Test
    public void H_Z_sm2_matches_HLZ_returns_true() {
        assertTrue(sm2.matches("HLZ"));
    }

    @Test
    public void H_Z_sm2_matches_HLE_returns_false() {
        assertFalse(sm2.matches("HLE"));
    }

    @Test
    public void H_percent_sm3_matches_HHZ_returns_true() {
        assertTrue(sm3.matches("HHZ"));
    }

    @Test
    public void H_percent_sm3_matches_HLZ_returns_true() {
        assertTrue(sm3.matches("HLZ"));
    }

    @Test
    public void H_percent_sm3_matches_HLE_returns_true() {
        assertTrue(sm3.matches("HLE"));
    }

    @Test
    public void H_percent_sm3_matches_ABC_returns_true() {
        assertFalse(sm3.matches("ABC"));
    }

    @Test
    public void H_percent_sm4_matches_hhz_returns_true() {
        assertTrue(sm4.matches("hhz"));
    }

    @Test
    public void H_percent_sm4_matches_hlz_returns_true() {
        assertTrue(sm4.matches("hlz"));
    }

    @Test
    public void H_percent_sm4_matches_hle_returns_true() {
        assertTrue(sm4.matches("hle"));
    }

    @Test
    public void H_percent_sm4_matches_abc_returns_true() {
        assertFalse(sm4.matches("abc"));
    }

    @Test
    public void db_wildcards_to_regex_triple_underscore_equals_triple_period() {
        assertEquals("...", sm.dbWildCardsToRegex("___"));
    }

    @Test
    public void db_wildcards_to_regex_percent_HD_percent_equals_period_asterisk_HD_period_asterisk() {
        assertEquals(".*HD.*", sm.dbWildCardsToRegex("%HD%"));
    }

    @Test
    public void db_wildcards_to_regex_underscore_percent_equals_underscore_underscore_asterisk() {
        assertEquals("..*", sm.dbWildCardsToRegex("_%"));
    }

    @Test
    public void regex_to_db_wildcards_H_period_asterisk_D_period_asterisk_equals_H_percent_D_percent() {
        assertEquals("H%D%", sm.regexToDbWildCards("H.*D.*"));
    }
}
