package org.trinet.jiggle.ui.common;

import org.trinet.jiggle.common.type.SolutionEvent;

import javax.swing.event.EventListenerList;
import java.util.EventObject;

/**
 * Modified from http://www.java2s.com/Code/Java/Event/CreatingaCustomEvent.htm
 */
public class SolutionChanged {

    protected EventListenerList listenerList = new EventListenerList();

    public void addSolutionChangedListener(SolutionListener listener) {
        this.listenerList.add(SolutionListener.class, listener);
    }

    public void removeSolutionChangedListener(SolutionListener listener) {
        this.listenerList.remove(SolutionListener.class, listener);
    }

    public void fireSolutionChanged(SolutionEvent evt) {
        Object[] listeners = listenerList.getListenerList();
        for (int i = 0; i < listeners.length; i = i+2) {
            if (listeners[i] == SolutionListener.class) {
                ((SolutionListener) listeners[i+1]).solutionChanged(evt);
            }
        }
    }
}
