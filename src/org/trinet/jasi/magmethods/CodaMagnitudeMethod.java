package org.trinet.jasi.magmethods;
import java.util.*;
import org.trinet.filters.ButterworthFilterSMC;
import org.trinet.filters.FilterTypes;
import org.trinet.jasi.*;
import org.trinet.jasi.coda.*;
import org.trinet.jdbc.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;
import org.trinet.util.velocitymodel.*;

public abstract class CodaMagnitudeMethod extends AbstractMagnitudeMethod 
    implements CodaMagnitudeMethodIF, Runnable {

    protected static final double DEFAULT_CODA_START_TIME_OFFSET           = 1.;
    protected double codaStartOffsetSecs = DEFAULT_CODA_START_TIME_OFFSET;
    protected static final double DEFAULT_PRE_P_TIME_OFFSET                = 2.;
    protected double codaPrePTimeBiasOffsetSecs = DEFAULT_PRE_P_TIME_OFFSET;

    protected static final int DEFAULT_MIN_READING_COUNT = 3;
    {
      defaultMinValidReadings = DEFAULT_MIN_READING_COUNT;
      minValidReadings = defaultMinValidReadings;
    }

    protected String tauTypeFilter = null;

    protected boolean logCodaCalc = false;
    protected boolean waveformFilterOn = true;

    public static boolean SET_TAU_FROM_FIT = false;

    // default scan only picked traces 
    protected boolean useZeroQuality = true;

    protected CodaGeneratorIF codaGenerator;
    protected FilterIF codaWaveFilter;
    protected SmPTravelTimeSource tts = null;

    // Move all waveform load caching stuff into variant of this class?
    protected Thread loadingThread;
    protected static final long   DEFAULT_WAVEFORM_LOAD_WAIT_MILLIS        = 2000l;
    protected long waveformLoadWaitMillis= DEFAULT_WAVEFORM_LOAD_WAIT_MILLIS;
    protected static final int    DEFAULT_MAX_LOADING_WAIT_ITERATIONS      = 15;
    protected int  maxWaveformLoadingWaits = DEFAULT_MAX_LOADING_WAIT_ITERATIONS;
    protected static final int    DEFAULT_WAVEFORM_CACHE_SIZE              = 100;
    protected int  maxWaveformCacheSize = DEFAULT_WAVEFORM_CACHE_SIZE;
    protected boolean waveformCacheLoadingEnabled = true;
    protected List   solMagReadingWaveAssocBinList;
    protected int    solWaveformLoadedIndex    = -1;
    protected int    solWaveformToProcessIndex = -1;
    protected String progressMessageChannelName;
    // end of waveform cache data elements

    double analogPeakAmpClipRatio = AbstractCodaMagCalibration.DEFAULT_CODA_CLIP_PEAK_AMP_RATIO;

    // Method does not use horiz (maybe different sometime in future?)
    {
      usesHoriz = false; // ml does but not md currently
    }

    protected CodaMagnitudeMethod() {
      super(org.trinet.jasi.Coda.class);
    }

    public void setDefaultProperties() {
      super.setDefaultProperties();
      debug = false;
      verbose = false;
      logCodaCalc = false;
      scanAllWaveformsForMag = false;
      useZeroQuality = true;
      waveformFilterOn = true;
      codaStartOffsetSecs = DEFAULT_CODA_START_TIME_OFFSET;
      codaPrePTimeBiasOffsetSecs = DEFAULT_PRE_P_TIME_OFFSET;

      waveformLoadWaitMillis= DEFAULT_WAVEFORM_LOAD_WAIT_MILLIS;
      maxWaveformLoadingWaits = DEFAULT_MAX_LOADING_WAIT_ITERATIONS;
      maxWaveformCacheSize = DEFAULT_WAVEFORM_CACHE_SIZE;

      analogPeakAmpClipRatio = AbstractCodaMagCalibration.DEFAULT_CODA_CLIP_PEAK_AMP_RATIO;

      SET_TAU_FROM_FIT = false;

    }

    public double distanceCorrection(double dist) { return 0.;}

  // MagnitudeMethodIF 

    public boolean trimBySummaryStatistics(Magnitude mag) {
        if (! sumMagStatTrim) return false;
        List aList = getDataUsedBySummaryMagCalc(mag);
        //if (aList.size() <= 10) return rejectChannelsByQTest(aList);
        //return rejectChannelsByStdDevTest(mag, aList);
        return chauvenetTrim(aList);
    }

    /* Not invoked, so use parent super class definition - aww 04/25/2005
    public double getInputWeight(MagnitudeAssocJasiReadingIF jr) {
        return jr.getQuality();
    }
    */

    protected double getCodaTau(Coda coda) {

        double xtau = coda.getTau();

        if ( ! SET_TAU_FROM_FIT ) return xtau;

        Channel chan = coda.getChannelObj();
        if (debug) {
          System.out.println("\n" + methodName + " getCodaTau fit for " + getStnChlNameString(chan) + " algorithm:" + coda.algorithm
                + " processing state: " + coda.getProcessingStateString());
        }

        // Don't change HAND picked coda tau created by PhasePopup menu option in Picking panel
        if ( coda.algorithm.equals("HAND") &&
                coda.getProcessingStateString().equals(JasiProcessingConstants.STATE_FINAL_TAG)) return xtau;

        // try to get from method map or channel map
        AbstractCodaMagCalibration calibrData =
            (AbstractCodaMagCalibration) getCalibrationFor(chan, LeapSeconds.trueToDate(coda.datetime.doubleValue()));

        // if none, try for default
        if (calibrData == null) {
            if (verbose || debug) System.out.println(methodName + " getCodaTau " + getStnChlNameString(chan) +
            " has no calibration, no coda cutoff amp, SKIPPING coda tau extrapolation, returning existing tau");
            return xtau;
        }

        double codaCutoffAmp = calibrData.getCodaCutoffAmpValue();
        if (codaCutoffAmp <= 0. )  {
            if (verbose || debug) System.out.println(methodName + " getCodaTau " + getStnChlNameString(chan) +
            " calibration (cutoff) amp values must be > 0., SKIPPING coda tau extrapolation, returning existing tau");
            return xtau;
        }

        if (debug) {
          System.out.println(methodName + " getCodaTau fit for " + getStnChlNameString(chan) + " codaCutoffAmp:" + codaCutoffAmp);
        }

        int goodWindowCount = coda.windowCount.intValue();
        double rms = coda.residual.doubleValue();
        double aFree = coda.aFree.doubleValue();
        double qFix = coda.qFix.doubleValue();
        double qFree = coda.qFree.doubleValue();
        double aFix = coda.aFix.doubleValue();
        StringBuffer sb;

        int dlen = coda.getPhaseDescription().length();

        // If the free l1fit was good, use that to extrapolate the coda */
        if( goodWindowCount >= 3 && rms < 0.3  && aFree > 0.0  && qFree > 1.0  && qFree < 5.0 ) {
          xtau = Math.pow(10., (aFree - MathTN.log10(codaCutoffAmp))/qFree) + 0.5;

          sb = new StringBuffer(coda.getPhaseDescription());
          if (dlen >= 2) sb.setCharAt(2, AbstractCodaGenerator.TAU_EXTRAPOLATED_FREE);  // qFree extrapolation
          else if (dlen == 2) sb.append(AbstractCodaGenerator.TAU_EXTRAPOLATED_FREE);
          else sb.append(" S").append(AbstractCodaGenerator.TAU_EXTRAPOLATED_FREE);
          coda.setPhaseDescription(sb.toString());
          coda.setSource(EnvironmentInfo.getApplicationName());
        }
        // if at least one on-scale coda amp, use fixed l1fit information to extrapolate tau
        else if ( goodWindowCount >= 1 && aFix > 0.0 && qFix > 1.0 ) {
          xtau =  Math.pow(10., (aFix - MathTN.log10(codaCutoffAmp))/qFix) + 0.5;

          sb = new StringBuffer(coda.getPhaseDescription());
          if (dlen >= 2) sb.setCharAt(2, AbstractCodaGenerator.TAU_EXTRAPOLATED_FIXED); // qFix  extrapolation
          else if (dlen == 2) sb.append(AbstractCodaGenerator.TAU_EXTRAPOLATED_FIXED);
          else sb.append(" S").append(AbstractCodaGenerator.TAU_EXTRAPOLATED_FIXED);
          coda.setPhaseDescription(sb.toString());
          coda.setSource(EnvironmentInfo.getApplicationName());
        }
        // Otherwise, can't extrapolate the coda properly; keep the original tau and set descriptor flag
        else {
          sb = new StringBuffer(coda.getPhaseDescription());
          if (dlen >= 2) sb.setCharAt(2, AbstractCodaGenerator.CODA_CALC_INVALID_TYPE); // unknown, extrapolation failure, reject
          else if (dlen == 2) sb.append(AbstractCodaGenerator.CODA_CALC_INVALID_TYPE);
          else sb.append(" S").append(AbstractCodaGenerator.CODA_CALC_INVALID_TYPE);
          coda.setPhaseDescription(sb.toString());
          coda.setSource(EnvironmentInfo.getApplicationName());
        }

        // Reset coda value
        coda.tau.setValue(xtau);

        if (debug) {
          System.out.println(methodName + " getCodaTau fit for " + getStnChlNameString(chan) + " new tau:" + xtau);
        }
        return xtau;

    }

    abstract public double calcChannelMagValue(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException;

    /* Override kludge here forced clear of calibrList - old implementation
    public List getDataForSummaryMagCalc(List readingList) {
        List aList = super.getDataForSummaryMagCalc(readingList);
        if ( aList.isEmpty()) { // No reading data may mean stale channel corr info
          // clear used to force calibration load upon next event processed
          // but calibrList load code commented out see createAssocMagDataFromWaveform
          if (calibrList != null) calibrList.clear();
        }
        return aList;
    }
    */
    public List getDataForSummaryMagCalc(List dataList) {

      List dataList2 = super.getDataForSummaryMagCalc(dataList);
      if (tauTypeFilter == null || tauTypeFilter.length() == 0) return dataList2;
      if (debug) System.out.println("DEBUG getDataForSummaryMagCalc tauTypeFilter: " + tauTypeFilter); 
      int cnt = dataList2.size();
      List aList = new ArrayList(cnt);
      Coda coda = null;
      String desc = null;
      for (int idx=0; idx<cnt; idx++) {
        coda = (Coda) dataList2.get(idx);
        desc = coda.getPhaseDescription();
        if (desc != null && (desc.length() > 2) && tauTypeFilter.indexOf(String.valueOf(desc.charAt(2))) >= 0) aList.add(coda);
        else {
            if (debug) System.out.println("DEBUG getDataForSummaryMagCalc rejecting coda, unacceptable tau type: "+ desc + " " + getStnChlNameString(coda));
            rejectReading(coda);
        }

      }
      if ( aList.size() == 0) {
         System.out.println("FYI : getDataForSummaryMagCalc: No data acceptable for summary mag"); 
      }

      return aList;

    }

    public boolean isValid(MagnitudeAssocJasiReadingIF jr) {
        if (! ( jr instanceof Coda)) return false;
        // To discriminate "ca" from "cd" Coda subtype calculation add check:
        //  && isIncludedReadingType(jr);
        Coda coda = (Coda) jr;
        boolean valid = (!coda.windowCount.isNull() && (coda.windowCount.longValue() > 0))
            || (coda.algorithm.toString().equals("HAND") && coda.getTau()>0.);
        if (debug && ! valid) {
          reportMessage(" !isValid(coda) no windowCount or not hand-picked: ", jr);
        }
        return valid;
    }


    // Next two IF methods relate to setting maximum, total coda duration windows ? 
    public void setScanWaveformTimeWindow(boolean tf) {  } // MagnitudeMethodIF ?
    public boolean getScanWaveformTimeWindow() { return true; }

    // TODO: need user override of "bogus" Md caculations derived from automatic waveform scan.
    // i.e. implement Jiggle GUI accessible variant to allow extraction of scan TimeSpan
    // specified by user input in GUI window, e.g. as when waveform has noise before or
    // after the "coda energy" of interest. The scan TimeSpan bounds must be contained within
    // the TimeSpan of the timeseries data for the input waveform of interest. -aww
    public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveform(SolutionList solList, Waveform wf) {
        return createAssocMagDataFromWaveform(solList.getSolution(0), wf, null);
    }
    public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveform(SolutionList solList, Waveform wf, TimeSpan scanSpan) {
        return createAssocMagDataFromWaveform(solList.getSolution(0), wf, scanSpan);
    }

    public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveform(Solution aSol, Waveform wf) {
        return createAssocMagDataFromWaveform(aSol, wf, null);
    }
    public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveform(Solution aSol, Waveform wf, TimeSpan scanSpan) {

        currentSol = aSol; // for tracking
        if (aSol == null || wf == null) return null;

        if (codaGenerator == null) {
          System.err.println("Error: createAssocMagDataFromWaveform, codaGenerator D.N.E., state invalid");
          return null;
        }

        if (wf.getAmpUnits() != Units.COUNTS) {
          System.err.println("Error: createAssocMagDataFromWaveform, Wrong amp units for Coda, expected COUNTS, found:" +
                 Units.getString(wf.getAmpUnits()) );
          return null;
        }

        /*
        // AWW get solution region here, check set region name and then re-initialize the parameters ??
        String oldEventRegionName = eventRegionName;
        boolean insideRegion = false;
        for (int idx = 0; idx < parmsRegionNames.length; idx++) {
            insideRegion = aSol.isInsideRegion(parmsRegionNames[idx]);
            //if (debug) System.out.println("DEBUG checking parms region "+ parmsRegionNames[idx] + " is event in region? " + insideRegion);
            if (insideRegion) { // change default velocity model to new region
                eventRegionName = parmsRegionNames[idx];
                break;
            }
            eventRegionName = "DEFAULT";
        }
        if (debug) System.out.println("DEBUG current eventRegionName for parms is: " + eventRegionName);

        if (!oldEventRegionName.equals(eventRegionName)) {
            if (debug) System.out.println("DEBUG initializeMethod() called for eventRegionName = " + eventRegionName);
            initializeMethod();
        }
        */

        Waveform filteredWf = wf;
        if (hasWaveformFilter()) { // global conditioning filter applied to "ALL" traces
          filteredWf = wfFilter.filter(wf);
          if (filteredWf == null) return null; // waveform filter failed
        }
        // could force calibrList reload here by aSol datetime 
        // if (calibrList == null || calibrList.isEmpty())
        //     loadCalibrations(aSol.getDateTime()); 
        // 
        MagReadingWaveAssocBin wd = createMagReadingWaveAssocBin(aSol, filteredWf);
        if (wd == null) return null; // 06/06/05 -aww added since method above can return null
        wd.loadTimeSeries();
        //if (! generateFromMagReadingWaveAssocBin(wd)) return null; // aww 11/22/2006 replaced by below
        if (! generateFromMagReadingWaveAssocBin(wd, scanSpan)) return null; // scanSpan input as from userGUI
        return getUpdatedReadingWithGeneratorResults(wd);
    }


// Begin waveform processing helper methods
    private Channel getValidWaveformChannel(Solution aSol, Waveform wf) {
        Channel chan = wf.getChannelObj();
        if (chan == null) {
           System.out.println(methodName + " WARNING getWaveformChannel(aSol, wf) null wavform channel object!");
           return null;
        }
        if (! chan.hasLatLonZ()) {
            System.out.println(methodName + " getValidWaveformChannel(aSol, wf) channel has !hasLatLonZ: "
                               + getStnChlNameString(wf.getChannelObj()) );
            return null;
        }
        if (! acceptChannelType(chan)) {
            if (debug) {
                System.out.println(methodName +
                    " getValidWaveformChannel(aSol, wf) not accepting channel : " + getStnChlNameString(chan));
            }
            return null;
        }
        //alway recalculate, removes doubt
        // we need horizontal range for traveltime generator
        chan.calcDistance(aSol); // aww 06/11/2004
        return chan;
    }

    /** Implementation ported from EarthWorm routine (asnwgt() cnvwt2()). */
    public void assignWeight(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException {
      // should we throw WrongDataTypeException?
      if (! isIncludedReadingType(jr))
        throw new WrongCodaTypeException("Error assignWeight input reading not subtype for MagnitudeMethod " +
                          methodName);
      Coda coda = (Coda) jr;
      int tmpWt = 9;  // highest quality 
      int winCountL1Fit = coda.windowCount.intValue();
      // Magic scalar comparisons from AGLindh and others; how were they derived?
      // The actual coda decay line fit slope is "negative" but
      // stored coda data may follows CUSP convention of using
      // "positive" q values so set appropriate sign of value
      // for test:
      double testQFree = (codaGenerator.getQFix() < 0) ? //   >0 => CUSP
          coda.qFree.doubleValue() : -coda.qFree.doubleValue();
      if (testQFree >= 0 ) tmpWt = 0;  // don't allow energy building q slope

      if ( winCountL1Fit >= 2 ) {
        double junk = (0.7*coda.getChannelMagValue()+0.5+testQFree);
        junk = Math.abs(junk);
        if (junk > 0.6  ) tmpWt--;
        if (junk > 1.2  ) tmpWt--;
        if (junk > 1.8  ) tmpWt--;
        if (junk > 2.4  ) tmpWt--;
        if (junk > 3.0  ) tmpWt--;
        
        double rmsL1Fit = coda.getResidual();
        if (rmsL1Fit  > 0.08 ) tmpWt--;
        if (rmsL1Fit  > 0.16 ) tmpWt--;
        if (rmsL1Fit  > 0.24 ) tmpWt--;
        if (rmsL1Fit  > 0.32 ) tmpWt--;
        if (rmsL1Fit  > 0.40 ) tmpWt--;
      }
      else if (winCountL1Fit == 1) {
        tmpWt = 3;
      }
      else if (winCountL1Fit == 0) {
        tmpWt = 0;
      }
      else { // no fit data
        tmpWt = 0;
      }

      if ( tmpWt < 0  ||  tmpWt > 9 ) { tmpWt = 0; }

      // convert tmpWt to HYP2000 weight scheme:
      if ( tmpWt >= 7 ) tmpWt = 0;  // best
      else if ( tmpWt >= 5 ) tmpWt = 1;
      else if ( tmpWt >= 3 ) tmpWt = 2;
      else if ( tmpWt >= 1 ) tmpWt = 3;
      else tmpWt = 4;              // worst

      if (debug) System.out.println("   Coda quality b4: " +coda.getQuality()+ " after assignWt : " + PhaseDescription.toQuality(tmpWt) + " tmpWt:" + tmpWt);

      coda.setQuality(PhaseDescription.toQuality(tmpWt));
      //coda.setInWgt(coda.getQuality()); // Not here, done by calcChannelMag... ! aww 03/04 
      //coda.setWeightUsed(coda.getInWgt()); // Not here, done by calcSummaryMag... ! aww 03/04 
    }

//
// begin bulk waveform timeseries loading methods for thread processing
// should waveform cache loading be removed or moved up into super class? -aww
//
    public final void setWaveformCacheLoading(boolean value) {
        waveformCacheLoadingEnabled = value;
    }

    public final boolean isWaveformCacheLoadingEnabled() {
        return waveformCacheLoadingEnabled;
    }

    private void initializeWaveformListProcessingIndices() {
        solWaveformToProcessIndex = -1;
        solWaveformLoadedIndex = -1;
    }
    // if caching enabled, runs in separate thread
    private MagReadingWaveAssocBin loadTimeSeries(int index, List aList) {
        solWaveformToProcessIndex = index; // currentProgressValue
        MagReadingWaveAssocBin bin  = (MagReadingWaveAssocBin) aList.get(index);
        progressMessageChannelName = getStnChlNameString(bin.wave.getChannelObj());
        if (waveformCacheLoadingEnabled) {
            if (debug) 
                System.out.println("DEBUG " +methodName+ " loadTimeSeries(idx,list) waveformCacheLoadingEnabled:" + waveformCacheLoadingEnabled);
            if (index <= solWaveformLoadedIndex) return bin;
            if (loadingThread == null) {
                loadingThread  = new Thread(this);
                loadingThread.setName("CodaMagWFLoadingThread");
                loadingThread.setDaemon(true);
                loadingThread.start();
            }
            try {
                for (int count = 0; count < maxWaveformLoadingWaits; count++) {
                    if ( ! loadingThread.isAlive() || loadingThread.isInterrupted()) break;
                    Thread.currentThread().sleep(waveformLoadWaitMillis);
                    if (index <= solWaveformLoadedIndex) return bin;
                }
            }
            catch (InterruptedException ex) {
               ex.printStackTrace();
            }
            resultsString = methodName + " loadedTimeSeries unable to load time series data";
            System.out.println(resultsString);
            return null;
        }
        else {
            //if (debug) System.out.println("DEBUG " +methodName+ " loadTimeSeries(idx,list)");
            bin.loadTimeSeries(); // single thread load here
            solWaveformLoadedIndex = index;
            return bin;
        }
    }
    public final void run() {
        if (debug) System.out.println("*** " + methodName + " Loading thread started *** ");
        loadWaveformTimeSeriesData(solMagReadingWaveAssocBinList);
        loadingThread  = null;
        if (debug) System.out.println("*** " + methodName + " Loading thread ended *** ");
    }
    //invoked by loading thread:
    protected final void loadWaveformTimeSeriesData(List aList) {
        int size = (aList == null) ? 0 : aList.size();
        if (size < 1) return;
        for (int idx = solWaveformLoadedIndex+1; idx < size; idx++) {
            if (solWaveformLoadedIndex - solWaveformToProcessIndex > maxWaveformCacheSize) {
                if (debug) System.out.println(methodName +
                           "   Max waveforms cached: " +maxWaveformCacheSize+ " loading thread stopping.");
                break;
            }
            ((TimeSeriesContainerIF) aList.get(idx)).loadTimeSeries();
            solWaveformLoadedIndex = idx;
            Thread.currentThread().yield();
        }
    }
    //
    // For jiggle, override super createAssocMagDataFromWaveformList(Solution aSol, List wfList) here
    // move filtering to bulk cache process method below
    public List createAssocMagDataFromWaveformList(Solution aSol, List wfList) {
        if ( ! waveformCacheLoadingEnabled ) {
            return super.createAssocMagDataFromWaveformList(aSol,wfList);
        }
        if (debug) 
            System.out.println("DEBUG " +methodName+ " createAssocMagDataFromWaveformList(sol,list) waveformCacheLoadingEnabled:" +
                waveformCacheLoadingEnabled);

        if (codaGenerator == null) {
          System.err.println("Error: createAssocMagDataFromWaveformList, codaGenerator D.N.E., state invalid");
          return new ArrayList(0);
        }

        calcCount = 0;
        currentSol = aSol; // for tracking purposes

        solMagReadingWaveAssocBinList = createMagReadingWaveAssocBinList(aSol, wfList);
        initializeWaveformListProcessingIndices();
        int size = (solMagReadingWaveAssocBinList == null) ? 0 : solMagReadingWaveAssocBinList.size();
        List jrList = new ArrayList(size);
        if (size < 1) return jrList;

        if (debug) System.out.println("    ***" + methodName +
                        " processWaveformDataByCache input list size: " + size);
        validCount = 0;
        if (logCodaCalc) logCodaOutputHeader();
        for (int idx = 0; idx < size; idx++) {
            MagReadingWaveAssocBin bin = loadTimeSeries(idx, solMagReadingWaveAssocBinList);
            if (bin == null) {
                System.out.println(methodName +
                    " WARNING - timeout loading time series, skipped further loading of coda waveform data");
                System.out.println("    processWaveformDataByCache indices current,loaded: " +
                    idx +" "+ solWaveformLoadedIndex);
                break; // aborts all further processing alternative is just to continue to next index ????
            }
            if (debug) {
                System.out.println(methodName +
                    " processWaveformDataByCache indices current,loaded: " +idx+ " " + solWaveformLoadedIndex);
                // System.out.println(bin.toString());
            }

            if (generateFromMagReadingWaveAssocBin(bin) ) { 
                MagnitudeAssocJasiReadingIF jr = getUpdatedReadingWithGeneratorResults(bin);
                if (isValid(jr)) {    // check of windowcount, other values ?
                    resultsString = "Calculated valid magnitude reading, associated with solution";
                    jrList.add(jr);
                    validCount++;
                }
            }
            if (debug) System.out.println("===================================================================================");
            Thread.currentThread().yield();
        }
        return jrList;
    }
    private List createMagReadingWaveAssocBinList(Solution aSol, List wfList) {
        int size = (wfList == null) ? 0 : wfList.size();
        if (size < 1) {
            resultsString = methodName + " createMagReadingWaveAssocBinList found empty waveform Collection";
            return new ArrayList(0);
        }
        List binList = new ArrayList(size);
        if (debug) {
            System.out.print(methodName + " createMagReadingWaveAssocBinList input list size: " + size);
            System.out.println(" Origintime:" + aSol.getDateTime().toString());
        }
        for (int idx = 0; idx < size; idx++) {
            MagReadingWaveAssocBin jrWfBin = createMagReadingWaveAssocBin(aSol, (Waveform)wfList.get(idx));
            if (jrWfBin != null) binList.add(jrWfBin);
        }
        //System.out.println("DEBUG " +methodName+ " createMagReadingWaveAssocBinList waveforms out:" + binList.size());
        this.solMagReadingWaveAssocBinList = binList;
        return binList;
    }
//
// end of bulk waveform timeseries loading methods for thread
//

    //protected boolean hasSPhaseTimed = false; // use only if observed S phase time is known to be good.
    protected final double getCodaScanStartOffsetSecs(double smpTime) {
       //if (hasSPhaseTimed) return codaStartOffsetSecs;
       return (codaStartOffsetSecs > 0.) ? codaStartOffsetSecs : smpTime * 0.1; // offset inside S arrival
    }

    protected final MagReadingWaveAssocBin createMagReadingWaveAssocBin(Solution aSol, Waveform wf) {

        currentSol = aSol; // for tracking purposes
        Channel chan = getValidWaveformChannel(aSol, wf);
        if (chan == null) return null;

        if (debug) System.out.println(methodName + "    createMagReadingWaveAssocBin processing " +
             getStnChlNameString(chan)); 

        // disable/enable logic moved to SmPTravelTimeSource.setSource(Solution). - aww 05/04/2005
        //aSol.listenToPhaseListChange(false); // disables "stale" side-effect of phaseList add
        tts.setSource(aSol); // loads aSol.phaselist to get arrival times.
        //aSol.listenToPhaseListChange(true); // enables "stale" side-effect of phaseList add

        double pTT = tts.getPTravelTime(chan);
        if (pTT <= 0.) {
          if (debug && !scanAllWaveformsForMag) {
            System.out.println(methodName + "    createMagReadingWaveAssocBin rejecting channel: " +
                getStnChlNameString(chan) + " has no picked P or S phase time.");
          }
          return null; // reject
        }

        double originTime = tts.getOriginTime();
        double pTime = originTime + pTT;
        double sTT = tts.getSTravelTime(chan, pTT);
        double sCodaStartTime = originTime + sTT + getCodaScanStartOffsetSecs(sTT-pTT); // offset inside S arrival
        double waveStartTime = wf.getEpochStart();
        double waveEndTime  = wf.getEpochEnd();
        if (debug) System.out.println(methodName + "    createMagReadingWaveAssocBin " +
             getStnChlNameString(chan) + " pTime:" + LeapSeconds.trueToString(pTime) + // -aww 2008/02/11
             " pTT:" + pTT + " SmPTime:" + (sTT - pTT) + " hDistKm: " + chan.getHorizontalDistance() +
             // " sTime:" + LeapSeconds.trueToString(originTime+sTT) + // -aww 2008/02/11
             " codaStartTime:" + LeapSeconds.trueToString(sCodaStartTime)); // -aww 2008/02/11
        if (pTime > waveEndTime || sCodaStartTime > waveEndTime || sCodaStartTime < waveStartTime) {
            if (debug) System.out.println(methodName+
                    "    createMagReadingWaveAssocBin wave time out of bounds for id:" + aSol.getId().longValue());
            return null;
        }

        MagnitudeAssocJasiReadingIF jr = createDefaultReadingType();
        jr.setTime(pTime); // coda.datetime (observed P-arrival time else calculated traveltime) 
        jr.assign(aSol); // note not associate here
        return new MagReadingWaveAssocBin(wf, jr, sCodaStartTime);
    }

    abstract protected MagnitudeAssocJasiReadingIF createDefaultReadingType();

    protected MagnitudeAssocJasiReadingIF getUpdatedReadingWithGeneratorResults(MagReadingWaveAssocBin bin) {
        MagnitudeAssocJasiReadingIF jr = bin.jr;
        jr.setSource(EnvironmentInfo.getApplicationName());
        codaGenerator.setCodaResults((Coda)jr); // set values: ngood, AFix, AFree, QFree, tau, and windowAmp
        return jr;
    }
    /*
    Does not load the timeseries if Channel data out of range,
    sets start preP to recover timeseries needed to calculate pre-event noise LTA and S wave coda.
    Waveform objects must contain valid Channel data objects
    */
    protected boolean generateFromMagReadingWaveAssocBin(MagReadingWaveAssocBin jrWfBin) {
        return generateFromMagReadingWaveAssocBin(jrWfBin, null);
    }
    protected boolean generateFromMagReadingWaveAssocBin(MagReadingWaveAssocBin jrWfBin, TimeSpan scanSpan) {

        if (jrWfBin.rejected) return false;

        Waveform wave = jrWfBin.wave;
        boolean selfLoadedWaveTimeSeries = jrWfBin.selfLoadedWaveTimeSeries;

        double waveStartTime =  wave.getEpochStart();
        double waveEndTime   = wave.getEpochEnd();

        // restore patch here temporary for .001 jitter at end of data after collapse 04/12/04 -aww
        // patch was formerly removed on aww 12/7/00
        //waveSegment = (WFSegment) ((List) wave.getSegmentList()).get(wave.getSegmentList().size()-1);
        // The waveform should already be "collapsed" to minimal segment, without time tears only one WFSegment,
        // even so, check for contiguity to get last contiguous WFSegment ending time
        WFSegment [] segList = wave.getArray();
        if (segList == null || segList.length == 0) {
            if (debug) System.out.println("DEBUG ...  generateFromMagReadingWaveAssocBin segList null or empty");
            return false;  // no timeseries, weird -added to avoid null ptr below -aww 2010/06/24
        }

        // If input span is specified use it
        if (scanSpan != null) { // set sCoda scan window start time to input scanSpan start time value 
            jrWfBin.tsScanStartTime = scanSpan.getStart();
            if (debug) { 
                System.out.println("DEBUG ... sCoda scan start time reset to start of input timespan: " + scanSpan.toString()); 
            }
        }
        else if (debug) System.out.println("DEBUG ... sCoda scan start time: " + LeapSeconds.trueToString(jrWfBin.tsScanStartTime)); 

        WFSegment waveSegment = segList[0]; // first element, no more unless there are time tears
        if (debug) System.out.println("DEBUG generateFromMagReadingWaveAssocBin nsegs = "+segList.length);
        if (segList.length > 1) { // have time tears, unless it's not collapsed.
          //? time-tear loop logic below here like that in the getWaveformSegment() call in window() method called later in this method ?
          TimeSpan ts = null;
          boolean hasScodaTime = false;
          for (int idx = 0; idx < segList.length; idx++) {
              ts = segList[idx].getTimeSpan();
              if (debug) System.out.println(idx + " " + ts.toString());
              hasScodaTime = ts.contains(jrWfBin.tsScanStartTime); 
              if (ts.contains(jrWfBin.jr.getTime()) && hasScodaTime) {
                  // segment has the P-pick and it has the sCoda scan start time
                  if (debug) System.out.println("DEBUG ... generateFromMagReadingWaveAssocBin segment HAS P-time AND sCoda scan start time!");
                  waveSegment = segList[idx];
                  break;
              }
              else if (hasScodaTime) {
                  // segment has Scoda scan start time of 2-sec windowing
                  if (debug) System.out.println("DEBUG ... generateFromMagReadingWaveAssocBin segment without P-time BUT contains sCoda scan start time!");
                  waveSegment = segList[idx];
                  break;
              }
              else if (ts.isAfter(jrWfBin.tsScanStartTime)) {
                  // if specified, use the smaller of ending times
                  if (scanSpan != null) waveEndTime = Math.min(waveEndTime, scanSpan.getEnd());
                  if (!ts.isAfter(waveEndTime)) {
                      // segment is first past the Scoda scan start time of 2-sec windowing before end of coda
                      if (debug) {
                          System.out.println("DEBUG ... generateFromMagReadingWaveAssocBin segment without P-time AND default sCoda scan start time!");
                          System.out.println("          sCoda scan start reset to segment start: " + LeapSeconds.trueToString(ts.getStart()));
                      }
                      jrWfBin.tsScanStartTime = ts.getStart();
                      waveSegment = segList[idx];
                      break;
                  }
              }
          }
        }

        // reset scan times, for case of coda span segment between time-tears
        waveEndTime = waveSegment.getEpochEnd();
        waveStartTime = waveSegment.getEpochStart();

        double preEventStartTime =
            //jrWfBin.jr.getTime() - codaGenerator.getNoiseBiasSamples()/wave.getSampleRate() - codaPrePTimeBiasOffsetSecs;
            jrWfBin.jr.getTime() - codaGenerator.getBiasLTASecs() - codaPrePTimeBiasOffsetSecs; // -aww 2008/03/4

        if (debug) {
            System.out.println(methodName +
                    " generateFromMagReadingWaveAssocBin " + getStnChlNameString(wave.getChannelObj()) +
                    "prePStart:" + LeapSeconds.trueToString(preEventStartTime) + // -aww 2008/02/11
                    " wfsStart:" + LeapSeconds.trueToString(waveStartTime) + // -aww 2008/02/11
                    " wfsEnd:" + LeapSeconds.trueToString(waveEndTime)); // -aww 2008/02/11
        }

        if (preEventStartTime < waveStartTime) preEventStartTime = waveStartTime;

        if (scanSpan != null) { // set coda scan window to scanSpan values ? -aww 11/22/2006
            waveEndTime = Math.min(waveEndTime, scanSpan.getEnd()); // use the smaller of ending times
            //if (debug)
                System.out.println("    sCoda scan end time set to: " + LeapSeconds.trueToString(waveEndTime));
        }
        else if (! codaGenerator.isResetOnClipping()) {
            double desiredWaveEndTime = jrWfBin.tsScanStartTime + codaGenerator.getMaxCodaDurationSecs();
            if (debug) System.out.println("    desired waveEndTime:" + LeapSeconds.trueToString(desiredWaveEndTime)); // -aww 2008/02/11
            waveEndTime = Math.min(waveEndTime, desiredWaveEndTime);
        }

        // Require at least 1-sec time span:
        if ((waveEndTime - preEventStartTime) < 1.0) {
            if (debug) {
                System.out.println("    wave number of segments: " + wave.getSegmentList().size());
                System.out.println("    input waveform preEventStartTime: " + LeapSeconds.trueToString(preEventStartTime) );
                System.out.println("    input waveform     waveStartTime: " + LeapSeconds.trueToString(waveStartTime) );
                System.out.println("    input waveform       waveEndTime: " + LeapSeconds.trueToString(waveEndTime) );
            }

            System.out.println(methodName + " generateFromMagReadingWaveAssocBin waveEndTime-waveStartTime < 1 second skipping "
                + getStnChlNameString(wave.getChannelObj()));
            if (selfLoadedWaveTimeSeries) wave.unloadTimeSeries();
            return false;
        }

        // note would use non-null scanSpan values here for waveEndTime of timespan -aww
        TimeSpan codaTimeSpan = new TimeSpan(preEventStartTime, waveEndTime);
        if (debug) {
            System.out.print("  TimeSpan for coda scan: " + codaTimeSpan.toString());
            if (wave.hasTimeTears()) System.out.print(",  input waveform has time tears");
            System.out.println();
        }

        // Note time tears not accepted for calculations and call to window
        // method below "copies" the waveform time series into a new segment,
        // thus coda generator Butterworth filter doesn't effect the original wf timeseries
        
        WFSegment wfs = window(jrWfBin, codaTimeSpan); // aka WFSegment.collapse(Vector)
        if (wfs == null) {
            if (debug)
                System.out.println(methodName + " generateFromMagReadingWaveAssocBin requested Swave coda segment out of bounds: " +
                                    getStnChlNameString(wave.getChannelObj()));
            if (selfLoadedWaveTimeSeries) wave.unloadTimeSeries();
            return false;
        }

        // scan coda from startingIdx to end.
        double wfSampleInterval = wfs.getSampleInterval();
        int startingIdx = (int) Math.round((jrWfBin.tsScanStartTime - wfs.getEpochStart())/wfSampleInterval);
        int wfSamplesExpected = wfs.samplesExpected;
        if (startingIdx < 0 || startingIdx >= wfSamplesExpected) {
            if (debug) System.out.println(methodName + " generateFromMagReadingWaveAssocBin requested Swave coda startindex out of bounds");
            if (selfLoadedWaveTimeSeries) wave.unloadTimeSeries();
            return false;
        }

        // calculate the coda fit parameters for station channel
        double smpSecsToAdd = jrWfBin.tsScanStartTime - jrWfBin.jr.getTime(); // S-P time to add to S-coda to get P-coda
        Channel chan = jrWfBin.getChannelObj();
        boolean status = generateMagDataFromTimeSeries(preEventStartTime, wfSampleInterval, smpSecsToAdd,
                                                       wfs.getTimeSeries(), startingIdx, wfSamplesExpected,
                                                       chan, 0.);
        if (status && logCodaCalc) logCodaResult(chan);
        if (selfLoadedWaveTimeSeries) {
            //if (debug) System.out.println(methodName + " generateFromMagReadingWaveAssocBin LOCAL LOAD unloadTimeSeries()...");
            wave.unloadTimeSeries();
        }    
        calcCount++;
        return status;
    }

//
// end of waveform data processing helper methods
//

    protected final WFSegment getWaveformSegment(MagReadingWaveAssocBin jrWfBin, TimeSpan codaTimeSpan) {

        List waveformSegments = (List) jrWfBin.wave.getSegmentList();
        if (waveformSegments == null)  {
            System.out.println(methodName + " ERROR - waveform segments list NULL");
            return null;
        }
        int size = waveformSegments.size(); // should be 1 if contiguous
        if (debug) System.out.println(methodName + "    getWaveformSegment total waveform segments: " + size);
        WFSegment wfs = null;
        TimeSpan ts = null;
        try {
          for (int idx = 0; idx < size; idx++) {
            wfs = (WFSegment) waveformSegments.get(idx);
            ts = new TimeSpan(wfs.getEpochStart(), wfs.getEpochEnd());
            if (debug) {
                System.out.println(methodName + "    getWaveformSegment segment timespan: " + ts.toString());
            }
            //NOTE: codaTimeSpan = new TimeSpan(preEventStartTime, waveEndTime) and SCoda start is jrWfBin.tsScanStartTime
            if (ts.contains(codaTimeSpan)) { // segment has the whole requested coda span without any time-tear
                return new WFSegment(wfs);
            }
            else if (ts.contains(jrWfBin.tsScanStartTime)) { // segment at least has sCoda scan start time
                if (debug) {
                   System.out.println(methodName + "    getWaveformSegment (time-tear) returned segment contains predicted sCoda start: " +
                           LeapSeconds.trueToString(jrWfBin.tsScanStartTime));
                }
                //Time tear or wfs timespan is too-short for codaTimeSpan, so try using a segment if it contains "start" of the 2-sec windowing
                return new WFSegment(wfs);
            }
            else if (ts.isAfter(jrWfBin.tsScanStartTime) && !ts.isAfter(codaTimeSpan.getEnd())) {
                // segment is first past the Scoda scan start time of 2-sec windowing before end of coda
                if (debug) {
                   System.out.println(methodName + "    getWaveformSegment (time-tear) returned segment starts after predicted sCoda start: " + 
                           LeapSeconds.trueToString(jrWfBin.tsScanStartTime));
                   System.out.println("    sCoda scan start reset to segment start: " + LeapSeconds.trueToString(ts.getStart()));
                }
                jrWfBin.tsScanStartTime = ts.getStart(); // reset sCoda start time
                return new WFSegment(wfs);
            }
          }
        }
        catch (IllegalArgumentException ex) {
            ex.printStackTrace();
            System.err.println(wfs.toString()); // dump bad segment info reversed time ?
            System.err.println(jrWfBin.wave.toString());
        }
        // resultsString = "time tear in requested waveform window";
        return null;
    }

    /** Put time series to be used for the S coda scan into a new waveform segment*/
    protected final WFSegment window(MagReadingWaveAssocBin jrWfBin, TimeSpan codaTimeSpan) {
        double windowStartTime = codaTimeSpan.getStart(); // UTC -aww 2008/02/11
        double windowEndTime =   codaTimeSpan.getEnd(); // UTC -aww 2008/02/11
        if (windowStartTime >= windowEndTime) return null;

        WFSegment wfs = getWaveformSegment(jrWfBin, codaTimeSpan);
        if (wfs == null ) {
            if (debug) System.out.println(methodName + "    window coda time not contiguous; requested span extends beyond wave segment boundary");
            return null;
        }
        int wfSamplesExpected = wfs.samplesExpected;
        int wfTimeSeriesSize = wfs.size();
        double wfEndTime = wfs.getEpochEnd();
        double wfStartTime = wfs.getEpochStart();
        if (debug) {
            System.out.println("    wfSamplesExpected: " + wfSamplesExpected + " size: " + wfTimeSeriesSize);
            System.out.println("    wfStartTime: " + LeapSeconds.trueToString(wfStartTime) // -aww 2008/02/11
                                + " wfEndTime: " + LeapSeconds.trueToString(wfEndTime)); // -aww 2008/02/11
            System.out.println("    windowStartTime: " + LeapSeconds.trueToString(windowStartTime) // -aww 2008/02/11
                                + " windowEndTime: " + LeapSeconds.trueToString(windowEndTime)); // -aww 2008/02/11
        }
        if (wfTimeSeriesSize < 1 || windowStartTime > wfEndTime  || windowEndTime < wfStartTime) return null;
        if (windowStartTime < wfStartTime) windowStartTime = wfStartTime;
        if (windowEndTime > wfEndTime) windowEndTime = wfEndTime;
        double wfSampleInterval = wfs.getSampleInterval();
        int sampleStartOffset = (int) Math.round((windowStartTime - wfStartTime )/wfSampleInterval);
        windowStartTime = wfStartTime + sampleStartOffset*wfSampleInterval;
        int sampleCount = (int) Math.round((windowEndTime - windowStartTime)/wfSampleInterval) + 1; // include end of range sample
        if ((sampleCount + sampleStartOffset) > wfTimeSeriesSize) {
            sampleCount = wfTimeSeriesSize - sampleStartOffset;
        }
        windowEndTime = windowStartTime + (sampleCount-1)*wfSampleInterval; // -1 is for the starting sample
        // Reset the segment start and endtimes to match the sample offsets scaled by sampling interval
        if (debug) System.out.println("    window new wfs sampleCount: " + sampleCount +
                                      " windowStartTime: " + LeapSeconds.trueToString(windowStartTime) +
                                      " windowEndTime: " + LeapSeconds.trueToString(windowEndTime));

        float[] sample = new float[sampleCount];
        float[] wfsTimeSeries = wfs.getTimeSeries();
        for (int idx = 0; idx < sampleCount; idx++) {
            sample[idx] = wfsTimeSeries[idx + sampleStartOffset];
        }

        wfs.setStart(windowStartTime);
        wfs.setEnd(windowEndTime);
        wfs.samplesExpected = sampleCount;
        wfs.setTimeSeries(sample, false);
        //D.N.E. removed wfs.length      = wfs.bytesPerSample*wfs.sampleCount;
        //D.N.E. removed wfs.lenSecs     = wfs.getEpochEnd() - wfs.getEpochStart();
        return wfs;
    }
    
    protected boolean generateMagDataFromTimeSeries(double startTime, double secsPerSample, double smpSecsToAdd,
                    float [] timeSeries, int startingIdx, int sampleCount, Channel chan, double lta) {

        // try to get from method map or channel map
        AbstractCodaMagCalibration calibrData =
            (AbstractCodaMagCalibration) getCalibrationFor(chan, LeapSeconds.trueToDate(startTime)); // -aww 2008/02/11

        // if none, try for default
        if (calibrData == null) {
            // REMOVED default set below, return false instead -aww 2010/05/20
            //calibrData = (AbstractCodaMagCalibration) defaultCalibrationFactory.createDefaultCalibration(chan);
            if (verbose || debug) System.out.println(methodName + " generateMagDataFromTimeSeries " + getStnChlNameString(chan) +
            " has no calibration coda clip,cutoff amp values, SKIPPING waveform scan");
            return false;
        }

        double codaCutoffAmp = 0.;
        double codaClipAmp = 0.;

        String seedchan = chan.getSeedchan();

        if (calibrData != null) {
            // These return "default" values for "E" and "H" types when undefined by query, should they return 0?  -aww
            codaCutoffAmp = calibrData.getCodaCutoffAmpValue();
            codaClipAmp = calibrData.getCodaClipAmpValue();
            if (calibrData.getCodaClipAmpUnits() == CalibrUnits.COUNTS) { // vs. AVG_ABS_COUNTS
              // Convert from peak counts to avgAbsAmp value ratio fraction
              //codaClipAmp *= AbstractCodaMagCalibration.DEFAULT_CODA_CLIP_PEAK_AMP_RATIO;
              if (codaClipAmp <= 4096 && (seedchan.equalsIgnoreCase("EHZ") || seedchan.equalsIgnoreCase("SHZ")) ) { // assume FM analog EW -aww 04/05/2011
                  codaClipAmp *= analogPeakAmpClipRatio; 
              }
              else {
                  codaClipAmp *= AbstractCodaMagCalibration.DEFAULT_CODA_CLIP_PEAK_AMP_RATIO;
              }
            }
        }

        if (codaCutoffAmp == 0. || codaClipAmp == 0.) {
            if (verbose || debug) System.out.println(methodName + " generateMagDataFromTimeSeries " + getStnChlNameString(chan) +
            " calibration (clip,cutoff) amp values must be > 0. (" + codaClipAmp + "," + codaCutoffAmp + "), SKIPPING waveform scan" );
            return false;
        }

        if (codaGenerator.hasFilter()) {
            if (filterChannelType(seedchan)) {
                codaGenerator.enableFilter();
                //codaGenerator.setFilter(seedchan); // must get samprate from Channel - aww 
                if (debug) 
                  System.out.println(methodName + " generateMagDataFromTimeSeries " + getStnChlNameString(chan) + " applying " + codaGenerator.describeFilter());
                codaGenerator.setFilter(chan);
            }
            else codaGenerator.disableFilter();
        }

        if (debug)
            System.out.println(methodName + " generateMagDataFromTimeSeries " + getStnChlNameString(chan) +
            " fit codaCutoffAmp:" + codaCutoffAmp + " codaClipAmp:" + codaClipAmp);

        if (codaGenerator.getWindowSize() < secsPerSample) {
            System.err.println(methodName + " generateMagDataFromTimeSeries for " + getStnChlNameString(chan) +
            " codaGenerator.getWindowSize() < secsPerSample : " + codaGenerator.getWindowSize() + " < "
            + secsPerSample); 
            return false;
        }

        boolean status = codaGenerator.calcCoda(secsPerSample, smpSecsToAdd,
                             timeSeries, startingIdx, sampleCount,
                             codaCutoffAmp, codaClipAmp, lta);

        if (debug && ! status)
            System.out.println(methodName + " generateMagDataFromTimeSeries bad coda:" +
                            " * exit:" + codaGenerator.getExitStatusString());

        return status;
    }


    // based on properties instantiate subtype instances specific to method algorithm
    abstract protected CodaGeneratorIF createCodaGenerator(GenericPropertyList props);
    abstract protected CodaGeneratorParms createCodaGeneratorParms(GenericPropertyList props);
    //abstract protected FilterIF getCodaWaveFilter(GenericPropertyList props);
    protected FilterIF getCodaWaveFilter(GenericPropertyList props) {
        // return FloatButterworthHighPassFilter(true); // static, reverse filtering enabled
        // return GeneralButterworthFilter(FilterTypes.HIGHPASS, true); // 1Hz 4 pole reversed
        // String fileClassname = props.getProperty("filterClassName", "org.trinet.filters.ButterworthFilterSMC");
        int rate = 100; // default at 100 sps
        String bwfType = props.getProperty("filterType", "HIGHPASS");
        double bwfLoCut = props.getDouble("filterCornerLowFreq", 1.);
        double bwfHiCut = props.getDouble("filterCornerHighFreq", 20.);
        int bwfOrder = props.getInt("filterOrder", 4);
        boolean bwfReverse = props.getBoolean("filterReversed", true);
        ButterworthFilterSMC bwf =  (bwfType.equalsIgnoreCase("BANDPASS")) ?
            ButterworthFilterSMC.createBandpass(rate, bwfLoCut, bwfHiCut, bwfOrder, bwfReverse) :
            ButterworthFilterSMC.createHighpass(rate, bwfLoCut, bwfOrder, bwfReverse);
        if (verbose || debug) System.out.println(methodName + " ButterworthFilter:\n "+bwf.toString());
        return bwf;
    }

    public void setDebug(boolean tf) {
        super.setDebug(tf);
        ((AbstractCodaGenerator) codaGenerator).setDebug(tf); //class static DEBUG - TEMPORARY - AWW
        tts.setDebug(tf);
        if (debug) verbose = true;
    }

    //implement property specifics in class subtype 
    public void initializeMethod() {
        super.initializeMethod();
        if (props == null) return;
        if (props.isSpecified("analogPeakAmpClipRatio")) analogPeakAmpClipRatio = props.getDouble("analogPeakAmpClipRatio"); 
        if (props.isSpecified("tauTypeFilter")) tauTypeFilter = props.getProperty("tauTypeFilter"); 
        if (props.isSpecified("alwaysRecalcTauFromFit")) SET_TAU_FROM_FIT = props.getBoolean("alwaysRecalcTauFromFit"); 

        // below needed to determine coda from waveform scans
        initTravelTimeSource(props); // now does init assuming SolutionWfEditorProperty list setup -aww 2008/03/26
        initTimeScanProperties(props);
        initWaveCacheProperties(props);
        stateIsValid = initCodaGeneratorProperties(props);
    }
    protected void initTimeScanProperties(GenericPropertyList props) {
        codaStartOffsetSecs =
             Double.parseDouble(props.getProperty("codaStartOffsetSecs", String.valueOf(DEFAULT_CODA_START_TIME_OFFSET)));
        codaPrePTimeBiasOffsetSecs =
             Double.parseDouble(props.getProperty("codaPrePTimeBiasOffsetSecs", String.valueOf(DEFAULT_PRE_P_TIME_OFFSET)));
    }

    protected void initOutputProperties(GenericPropertyList props) {
        super.initOutputProperties(props);
        if (debug) logCodaCalc = true;
        else logCodaCalc   = Boolean.valueOf(props.getProperty("logCodaCalc", "false" )).booleanValue();
    }

    protected void initWaveCacheProperties(GenericPropertyList props) {
        waveformLoadWaitMillis =
             Long.parseLong(props.getProperty("waveformLoadWaitMillis", String.valueOf(DEFAULT_WAVEFORM_LOAD_WAIT_MILLIS)));
        maxWaveformLoadingWaits =
             Integer.parseInt(props.getProperty("maxLoadingWaits", String.valueOf(DEFAULT_MAX_LOADING_WAIT_ITERATIONS)));
        maxWaveformCacheSize =
             Integer.parseInt(props.getProperty("waveformCacheSize", String.valueOf(DEFAULT_WAVEFORM_CACHE_SIZE)));
        if (maxWaveformCacheSize < 2) setWaveformCacheLoading(false);
    }

    protected boolean initCodaGeneratorProperties(GenericPropertyList props) { 

        codaGenerator = createCodaGenerator(props);
        if (codaGenerator == null) {
          System.err.println(methodName +" initialization failed to create valid coda generator!");
          return false;
        }

        //if (debug) ((AbstractCodaGenerator) codaGenerator).setDebug(debug); //class static DEBUG - TEMPORARY - AWW

        CodaGeneratorParms codaGeneratorParms = createCodaGeneratorParms(props);

        // Set up generator according to specific parms configuration
        String tmp = codaGeneratorParms.getIdString();
        if (tmp == null) tmp = "?";
        codaGenerator.setAlgorithm(codaGenerator.getAlgorithm()+"."+tmp);

        waveformFilterOn  = Boolean.valueOf(props.getProperty("filter", "true")).booleanValue();
        codaWaveFilter = (waveformFilterOn) ? getCodaWaveFilter(props) : null; // to establish wave filter
        if (codaWaveFilter != null) {
          if (verbose) System.out.println(methodName + " waveform filtering ENABLED");
          codaGenerator.setFilter(codaWaveFilter);
          codaGenerator.enableFilter();
        }
        else {
          if (verbose) System.out.println(methodName + " waveform filtering DISABLED");
          codaGenerator.setFilter((org.trinet.util.FilterIF) null);
          codaGenerator.disableFilter();
        }
        //if (verbose) System.out.println(methodName + " CodaGeneratorParms: " + codaGeneratorParms.toString());
        codaGenerator.setResetOnClipping(codaGeneratorParms.resetOnClipping);
        codaGenerator.setMinGoodWindows(codaGeneratorParms.minGoodWindowsToTerminateCoda);
        codaGenerator.setMaxGoodWindows(codaGeneratorParms.maxGoodWindowsToTerminateCoda);
        codaGenerator.setCodaStartSignalToNoiseRatio(codaGeneratorParms.minSNRatioForCodaStart);
        codaGenerator.setCodaCutoffSignalToNoiseRatio(codaGeneratorParms.minSNRatioCodaCutoff);
        codaGenerator.setPassThruNoiseDecayAmpRatio(codaGeneratorParms.passThruNSRatio);
        codaGenerator.setWindowSize(codaGeneratorParms.windowSize);
        codaGenerator.setQFix(codaGeneratorParms.qFix);
        codaGenerator.setBiasLTASecs(codaGeneratorParms.biasLTASecs);
        //
        // override default generatorParms properties here or in the createCodaGeneratorParms method
        //
        tmp =  props.getProperty("resetOnClipping");
        if (tmp != null) codaGenerator.setResetOnClipping(props.getBoolean("resetOnClipping"));
        tmp =  props.getProperty("minGoodWindowsToTerminateCoda");
        if (tmp != null) codaGenerator.setMinGoodWindows(props.getInt("minGoodWindowsToTerminateCoda"));
        tmp =  props.getProperty("maxGoodWindowsToTerminateCoda");
        if (tmp != null) codaGenerator.setMaxGoodWindows(props.getInt("maxGoodWindowsToTerminateCoda"));
        tmp =  props.getProperty("minSNRatioForCodaStart");
        if (tmp != null) codaGenerator.setCodaStartSignalToNoiseRatio(props.getDouble("minSNRatioForCodaStart"));
        tmp =  props.getProperty("minSNRatioCodaCutoff");
        if (tmp != null) codaGenerator.setCodaCutoffSignalToNoiseRatio(props.getDouble("minSNRatioCodaCutoff"));
        tmp =  props.getProperty("passThruNSRatio");
        if (tmp != null) codaGenerator.setPassThruNoiseDecayAmpRatio(props.getDouble("passThruNSRatio"));
        tmp =  props.getProperty("codaBiasLTASecs");
        if (tmp != null) codaGenerator.setBiasLTASecs(props.getDouble("codaBiasLTASecs"));
        tmp =  props.getProperty("codaWindowSize");
        if (tmp != null) codaGenerator.setWindowSize(props.getDouble("codaWindowSize"));
        // TEST properties below for overlapping averaged amp windows at start of S-coda scan -aww 2008/03/07
        tmp =  props.getProperty("overlapStart");
        if (tmp != null) ((AbstractCodaGenerator)codaGenerator).setOverlapStartingWindows(props.getBoolean("overlapStart"));
        tmp =  props.getProperty("overlapWindowMax");
        if (tmp != null) ((AbstractCodaGenerator)codaGenerator).setOverlapWindowMax(props.getInt("overlapWindowMax"));

        if (verbose) System.out.println(methodName + " CodaGenerator properties reset " + codaGenerator.inputToString());

        return true;
    }

    protected void initTravelTimeSource(GenericPropertyList props) {

        // Do waveform scans for only picked traces or all traces? default is false, do only the picked
        if ( props.isSpecified("scanAllWaveformsForMag") )
            scanAllWaveformsForMag = props.getBoolean("scanAllWaveformsForMag"); 

        if ( props.isSpecified("useZeroQualityPick") )
            useZeroQuality = props.getBoolean("useZeroQualityPick"); 

        UniformFlatLayerVelocityModelIF flatLayerModel = null;
        /* First check for velocity model class name
        String velModelName = props.getProperty("velocityModelClassName");
        if (velModelName != null) {
          try {
            flatLayerModel = (UniformFlatLayerVelocityModelIF) Class.forName(velModelName).newInstance();
          } catch (Exception ex) {
            System.err.println("ERROR - Unable to create instance of named velocity model class: " + velModelName);
            ex.printStackTrace();
          }
        }
        else { // check for explicit velocity model properties - new option added for setting velocity model -aww 2008/03/18 
          flatLayerModel = UniformFlatLayerVelocityModel.getDefaultModelFromProps(props);
          if (flatLayerModel == null) flatLayerModel = TravelTime.getInstance().getModel(); 
          if (flatLayerModel == null)
              System.err.println("ERROR - Unable to create instance of default velocity model from properties or TravelTime instance.");
        }
        */
        // now assumes SolutionWfEditorPropertyList setup has initialized the default model -aww 2008/03/25
        flatLayerModel = TravelTime.getInstance().getModel(); 

        tts = new SmPTravelTimeSource(flatLayerModel);
        tts.setOnlyPickedTravelTimes(!scanAllWaveformsForMag);
        tts.setUseZeroQuality(useZeroQuality);

        if (debug) {
          System.out.println("DEBUG CodaMagnitudeMethod TravelTimeSource " + tts.toString());
          tts.setDebug(true);
        }
    }

    public GenericPropertyList getProperties() {
        GenericPropertyList props = super.getProperties();
        props.setProperty("filter", String.valueOf(waveformFilterOn));
        props.setProperty("logCodaCalc", String.valueOf(logCodaCalc));
        props.setProperty("codaStartOffsetSecs", String.valueOf(codaStartOffsetSecs));
        props.setProperty("codaPrePTimeBiasOffsetSecs", String.valueOf(codaPrePTimeBiasOffsetSecs));
        props.setProperty("waveformLoadWaitMillis", String.valueOf(waveformLoadWaitMillis));
        props.setProperty("maxLoadingWaits", String.valueOf(maxWaveformLoadingWaits));
        props.setProperty("waveformCacheSize", String.valueOf(maxWaveformCacheSize));
        props.setProperty("scanAllWaveformsForMag", String.valueOf(scanAllWaveformsForMag));

        if (codaGenerator == null) return props;

        props.setProperty("codaWindowSize", codaGenerator.getWindowSize());
        props.setProperty("codaBiasLTASecs", codaGenerator.getBiasLTASecs());
        props.setProperty("passThruNSRatio", codaGenerator.getPassThruNoiseDecayAmpRatio());
        props.setProperty("resetOnClipping", codaGenerator.isResetOnClipping());
        props.setProperty("minGoodWindowsToTerminateCoda", codaGenerator.getMinGoodWindows());
        props.setProperty("maxGoodWindowsToTerminateCoda", codaGenerator.getMaxGoodWindows());
        props.setProperty("overlapWindowMax", codaGenerator.getOverlapWindowMax());
        props.setProperty("overlapStart", codaGenerator.getOverlapStartingWindows());
        props.setProperty("minSNRatioForCodaStart", codaGenerator.getCodaStartSignalToNoiseRatio());
        props.setProperty("minSNRatioCodaCutoff", codaGenerator.getCodaCutoffSignalToNoiseRatio());
        props.setProperty("minSNRatioCodaCutoff", codaGenerator.getCodaCutoffSignalToNoiseRatio());
        props.setProperty("alwaysRecalcTauFromFit", SET_TAU_FROM_FIT);

        return props;
    }


    protected void logCodaOutputHeader() {
        System.out.println(
            "                    range  phase   qual   tau startAmp    endAmp good bad 1st   rms  QFix  QFree AFree AFix Filter Status"
        );
    }
    protected void logCodaResult(Channel chan) {
        if (debug) logCodaOutputHeader();
        StringBuffer sb = new StringBuffer(160);
        Concatenate.leftJustify(sb,AbstractMagnitudeMethod.getStnChlNameString(chan),19); // -aww added 2 more chars to len 08/15/2006
        //Concatenate.format(sb, chan.getDistance(), 4, 1); // don't use slant distance - aww 06/11/2004
        Concatenate.format(sb, chan.getHorizontalDistance(), 4, 1); // use horizontal distance - aww 06/11/2004
        sb.append(" ");
        sb.append(codaGenerator.outputToString());
        System.out.println(sb.toString());
    }
    public void logHeader() {
      System.out.println("    Channel                 Dist" + ChannelMag.getNeatStringHeader() + "  Segments");
    }

    public void logChannelResidual(MagnitudeAssocJasiReadingIF jr) {
          Coda coda = (Coda) jr;
          StringBuffer sb = new StringBuffer(132);
          sb.append("  ");
          Concatenate.leftJustify(sb, AbstractMagnitudeMethod.getStnChlNameString(coda), 24);
          Concatenate.format(sb, coda.getHorizontalDistance(), 4,1).append(" ");
          sb.append(coda.getChannelMag().toNeatString());
          Concatenate.format(sb, coda.windowCount.longValue(),4);
          System.out.println(sb.toString());
    }

    // inner class a container for managing Waveform and dependent MagnitudeAssocJasiReading data
    protected class MagReadingWaveAssocBin implements TimeSeriesContainerIF {
    //, Channelable
        Waveform wave;
        MagnitudeAssocJasiReadingIF jr;
        double tsScanStartTime;
        boolean selfLoadedWaveTimeSeries;
        boolean rejected;

        protected MagReadingWaveAssocBin(Waveform wave, MagnitudeAssocJasiReadingIF jr, double tsScanStartTime) {
            this.wave = wave;
            this.jr = jr;
            this.jr.setChannelObj(wave.getChannelObj());
            this.tsScanStartTime = tsScanStartTime;
        }

        public String toString() {
            StringBuffer sb = new StringBuffer(512);
            sb.append(wave.toString());
            sb.append(" readingTime: ").append(jr.getTime());
            sb.append(" tsScanStartTime:").append(tsScanStartTime);
            sb.append(" selfLoad: ").append(selfLoadedWaveTimeSeries);
            sb.append(" reject: ").append(rejected);
            return sb.toString();
        }
        public Channel getChannelObj() { return wave.getChannelObj();}
        public void setChannelObj(Channel chan) { wave.setChannelObj(chan); }
        public boolean equalsChannelId(Channelable chan) {
          return wave.equalsChannelId(chan);
        }
        public boolean sameStationAs(Channelable chan) {
          return wave.sameStationAs(chan);
        }
        public double calcDistance(GeoidalLatLonZ latLonZ) { return wave.calcDistance(latLonZ); } // aww 06/11/2004
        public double getAzimuth() { return wave.getAzimuth(); }
        public double getDistance() { return wave.getDistance(); }
        public double getHorizontalDistance() { return wave.getHorizontalDistance(); }
        public double getVerticalDistance() { return wave.getVerticalDistance(); }
        public void setAzimuth(double az) { wave.setAzimuth(az); }
        public void setDistance(double dist) { wave.setDistance(dist); }
        public void setHorizontalDistance(double dist) { wave.setHorizontalDistance(dist); }
        public boolean hasTimeSeries() { return wave.hasTimeSeries(); }
        public final boolean loadTimeSeries() {
            selfLoadedWaveTimeSeries = false;
            if (! hasTimeSeries()) { // load only if not preloaded, but could define a settable property to force override 
                //if (debug)
                //    System.out.println(methodName + " DEBUG MagReadingWaveAssocBin LOCAL LOAD of timeseries for: "+
                //                              wave.getChannelObj().toDelimitedSeedNameString(" "));
                selfLoadedWaveTimeSeries = wave.loadTimeSeries();
                if (! selfLoadedWaveTimeSeries) {
                    if (debug)
                        System.out.println(methodName + " DEBUG MagReadingWaveAssocBin loadTimeSeries NOT LOADED for:" +
                                                  wave.getChannelObj().toDelimitedSeedNameString(" "));
                    rejected = true;
                }
                return selfLoadedWaveTimeSeries;
            }
            //if (debug)
            //    System.out.println(methodName + " DEBUG MagReadingWaveAssocBin timeseries ALREADY loaded for: "+
            //                                  wave.getChannelObj().toDelimitedSeedNameString(" "));
            return true;
        }

        public boolean loadTimeSeries(double start, double end) { return wave.loadTimeSeries(start,end); }
        public boolean unloadTimeSeries() { return wave.unloadTimeSeries(); }
    } // end of MagReadingWaveAssocBin inner class

} // end of CodaMagnitudeMethod class

