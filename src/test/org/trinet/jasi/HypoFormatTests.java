package test.org.trinet.jasi;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.trinet.jasi.HypoFormat;

import java.util.Objects;

public class HypoFormatTests {

    @Test
    public void verify_parse_arc_to_phase_returns_correct_phase_object_DTP() {
        // Ignore SQL Exception errors
        assertEquals("Deleted=false Reject=false CI DTP EHZ  S My id: NULL evid: NULL orid: 0 .S.3 2002-09-12 20:43:45.820 50.4 -0.02 0.00.54 148.0 90.0 1.0 ",
                Objects.requireNonNull(HypoFormat.parseArcToPhase("DTP  CI  EHZ    0200209122043    0   0  0 4582 S 3  -2      0 0 54   0   0 504 9000  0    0148  0  0   0 141")).toString());
    }

    @Test
    public void verify_parse_arc_to_phase_returns_correct_phase_object_CLC() {
        assertEquals("Deleted=false Reject=false CI CLC HHZ  P My id: NULL evid: NULL orid: 0 .Pc1 2002-09-12 20:43:39.850 51.5 0.02 0.01.63 69.0 90.0 1.0 ",
                Objects.requireNonNull(HypoFormat.parseArcToPhase("CLC  CI  HHZ  PU1200209122043 3985   2163    0   0   0      0 0  0   0   0 515 9000  0    0 69  0  0 362   0")).toString());
    }

    @Test
    public void verify_parse_arc_to_phase_returns_correct_phase_object_WWP() {
        assertEquals("Deleted=false Reject=false CI WWP EHZ  P My id: NULL evid: NULL orid: 0 .Pd2 2002-09-12 20:43:33.080 99999.0 -0.01 0.00.0 NaN NaN 1.0 ",
                Objects.requireNonNull(HypoFormat.parseArcToPhase("WWP  CI VEHZ  PD2200209122043 3308  -1  0                                  106")).toString());
    }

    @Test
    public void verify_parse_arc_to_phase_returns_correct_phase_object_WOR() {
        assertEquals("Deleted=false Reject=false CI WOR EHZ  P My id: NULL evid: NULL orid: 0 .Pc1 2002-09-12 20:43:33.300 99999.0 -0.01 0.00.0 NaN NaN 1.0 ",
                Objects.requireNonNull(HypoFormat.parseArcToPhase("WOR  CI VEHZ  PU1200209122043 3330  -1  0                                  118")).toString());
    }
}
