package org.trinet.storedprocs.eventmatch;
/**
 * Score.java
 *
 *
 * Created: Mon Feb  7 16:52:51 2000
 *
 * @author Doug Given
 * @version
 */

public class Score  {

    public Score() {

    }
  public static void main (String args[]) {
	long evid1=0;
	long evid2=0;

	if (args.length < 2)	// no args
	{
	  System.out.println ("Usage: score [evid1] [evid2])");

	  System.exit(0);

	} else {

	  Integer val = Integer.valueOf(args[0]);    // convert arg String to 'double'
	  evid1 = (int) val.intValue();
	  val = Integer.valueOf(args[1]);    // convert arg String to 'double'
	  evid2 = (int) val.intValue();

	}

    // another test
    System.out.println ("Score of "+evid1+" and "+evid2+" = "+
			EventMatch.getScore(evid1, evid2) );
    System.out.println (" passingScore = "+ EventMatch.passingScore);
  }

} // Score
