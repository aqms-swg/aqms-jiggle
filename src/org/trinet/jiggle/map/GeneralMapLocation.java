//GeneralMapLocation.java - A generalized map location class, modified from "URLRasterLocation" and "BasicLocation".
// Adopted from original source in package com.isti.quakewatch.omutil;
package org.trinet.jiggle.map;

import java.io.*;
import java.net.URL;
import java.awt.Point;
import java.awt.Rectangle;
import javax.swing.ImageIcon;
import com.bbn.openmap.omGraphics.OMRect;
import com.bbn.openmap.omGraphics.OMCircle;
import com.bbn.openmap.omGraphics.OMRaster;
import com.bbn.openmap.omGraphics.OMGraphic;
import com.bbn.openmap.omGraphics.OMText;
import com.bbn.openmap.layer.location.Location;
import com.bbn.openmap.layer.DeclutterMatrix;
import com.bbn.openmap.proj.Projection;
import com.bbn.openmap.omGraphics.OMRasterObject;
import com.bbn.openmap.util.Debug;

/**
 * Class GeneralMapLocation is generalized map location class, modified from
 * "URLRasterLocation" and "BasicLocation".  Various 'OMGraphic'  classes
 * may be used for the location-marker, including 'OMCircle', 'OMRect'
 * and 'OMRaster' (using a URL that references an image file (gif, jpeg)
 * will create an ImageIcon object).
 */
public class GeneralMapLocation extends Location
{
  //vertical alignment values:
  public static final int VALIGN_TOP = 0;        //marker above label text
  public static final int VALIGN_MIDDLE = 1;     //marker even with label
  public static final int VALIGN_BOTTOM = 2;     //marker below label text
  public static final int VALIGN_UNDERLINE = 3;  //marker below (tighter)

  /** The spacing between the label and the outside of the image. */
  protected static final int IMAGE_SPACING = 0;
  //horizontal spacing between label and location marker:
  protected static final int LABEL_X_SPACING = DEFAULT_SPACING;
  //vertical spacing between label and location marker:
  protected static final int LABEL_Y_SPACING = 10; //(when not 'VALIGN_MIDDLE')

  protected int extraLabelXOffs = 0;     //extra X-offset for label
  protected int extraLabelYOffs = 0;     //extra Y-offset for label
  protected int symbolNumber = 0;        //symbol number (from ".SMB" file)


  /**
   * Creates a location at a latitude/longitude.  If the
   * locationMarker is null, a small square (dot) will be created
   * to mark the location.
   * @param latitude the latitude, in decimal degrees, of the location.
   * @param longitude the longitude, in decimal degrees, of the location.
   * @param name the name of the location, also used in the label.
   * @param locationMarker the OMGraphic to use for the location mark.
   * @param justVal the justification value for the label (one of the
   * "OMText.JUSTIFY_..." values).
   * @param vAlignVal vertical alignment value
   */
  public GeneralMapLocation(double latitude, double longitude, String name,
                         OMGraphic locationMarker, int justVal, int vAlignVal)
  {
    super(latitude, longitude, name, locationMarker);
    fixupLabel(justVal,vAlignVal);     //fix up label text
  }

  /**
   * Creates a location at a map location.  If the
   * locationMarker is null, a small square (dot) will be created
   * to mark the location.
   * @param x the pixel location of the object from the let of the map.
   * @param y the pixel location of the object from the top of the map
   * @param name the name of the location, also used in the label.
   * @param locationMarker the OMGraphic to use for the location mark.
   * @param justVal the justification value for the label (one of the
   * "OMText.JUSTIFY_..." values).
   * @param vAlignVal vertical alignment value
   */
  public GeneralMapLocation(int x, int y, String name,OMGraphic locationMarker,
                         int justVal, int vAlignVal)
  {
    super(x, y, name, locationMarker);
    fixupLabel(justVal,vAlignVal);     //fix up label text
  }

  /**
   * Creates a location at a pixel offset from a latitude/longitude.
   * If the locationMarker is null, a small square (dot) will be
   * created to mark the location.
   * @param latitude the latitude, in decimal degrees, of the location.
   * @param longitude the longitude, in decimal degrees, of the location.
   * @param xOffset the pixel location of the object from the longitude.
   * @param yOffset the pixel location of the object from the latitude.
   * @param name the name of the location, also used in the label.
   * @param locationMarker the OMGraphic to use for the location mark.
   * @param justVal the justification value for the label (one of the
   * "OMText.JUSTIFY_..." values).
   * @param vAlignVal vertical alignment value
   */
  public GeneralMapLocation(double latitude, double longitude,
                         int xOffset, int yOffset, String name,
                         OMGraphic locationMarker, int justVal, int vAlignVal)
  {
    super(latitude, longitude, xOffset, yOffset, name, locationMarker);
    fixupLabel(justVal,vAlignVal);     //fix up label text
  }

  /**
   * Creates a location at a latitude/longitude.  If the
   * locationMarker is null, a small square (dot) will be created
   * to mark the location.
   * @param latitude the latitude, in decimal degrees, of the location.
   * @param longitude the longitude, in decimal degrees, of the location.
   * @param name the name of the location, also used in the label.
   * @param locationMarker the OMGraphic to use for the location mark.
   */
  public GeneralMapLocation(double latitude, double longitude, String name,
                         OMGraphic locationMarker)
  {
    this(latitude, longitude, name, locationMarker,OMText.JUSTIFY_LEFT,
         VALIGN_MIDDLE);
  }

  /**
   * Creates a location at a map location.  If the
   * locationMarker is null, a small square (dot) will be created
   * to mark the location.
   * @param x the pixel location of the object from the let of the map.
   * @param y the pixel location of the object from the top of the map
   * @param name the name of the location, also used in the label.
   * @param locationMarker the OMGraphic to use for the location mark.
   */
  public GeneralMapLocation(int x, int y, String name,OMGraphic locationMarker)
  {
    this(x, y, name, locationMarker,OMText.JUSTIFY_LEFT,VALIGN_MIDDLE);
  }

  /**
   * Creates a location at a pixel offset from a latitude/longitude.
   * If the locationMarker is null, a small square (dot) will be
   * created to mark the location.
   * @param latitude the latitude, in decimal degrees, of the location.
   * @param longitude the longitude, in decimal degrees, of the location.
   * @param xOffset the pixel location of the object from the longitude.
   * @param yOffset the pixel location of the object from the latitude.
   * @param name the name of the location, also used in the label.
   * @param locationMarker the OMGraphic to use for the location mark.
   */
  public GeneralMapLocation(double latitude, double longitude,
                         int xOffset, int yOffset, String name,
                         OMGraphic locationMarker)
  {
    this(latitude, longitude, xOffset, yOffset, name, locationMarker,
         OMText.JUSTIFY_LEFT,VALIGN_MIDDLE);
  }

  /**
   * Creates a OMRaster-location at a latitude/longtude location.
   * If the iconURL is null, a small square (dot) will be
   * created to mark the location.
   * @param latitude latitide in decimal degrees.
   * @param longitude longitude in decimal degrees.
   * @param name the label for the location.
   * @param iconURL a string to a URL for an image.
   * @param justVal the justification value for the label (one of the
   * "OMText.JUSTIFY_..." values).
   * @param vAlignVal vertical alignment value
   */
  public GeneralMapLocation(double latitude, double longitude,
                         String name, String iconURL, int justVal, int vAlignVal)
  {
    super(latitude, longitude, name,
          getIconRaster(latitude, longitude, iconURL));
    if (location instanceof OMRaster)
    {
      setHorizontalLabelBuffer(
          (((OMRaster)location).getWidth()/2) + IMAGE_SPACING);
    }
    fixupLabel(justVal,vAlignVal);     //fix up label text
  }

  /**
   * Creates a OMRaster-location at a latitude/longtude location.
   * If the iconURL is null, a small square (dot) will be
   * created to mark the location.
   * @param latitude latitide in decimal degrees.
   * @param longitude longitude in decimal degrees.
   * @param name the label for the location.
   * @param iconURL a URL for an image.
   * @param justVal the justification value for the label (one of the
   * "OMText.JUSTIFY_..." values).
   * @param vAlignVal vertical alignment value
   */
  public GeneralMapLocation(double latitude, double longitude,
                         String name, URL iconURL, int justVal, int vAlignVal)
  {
    super(latitude, longitude, name,
          getIconRaster(latitude, longitude, iconURL));
    if (location instanceof OMRaster)
    {
      setHorizontalLabelBuffer(
          (((OMRaster)location).getWidth()/2) + IMAGE_SPACING);
    }
    fixupLabel(justVal,vAlignVal);     //fix up label text
  }

  /**
   * Creates a OMRaster-location at a screen x/y position.
   * If the iconURL is null, a small square (dot) will be
   * created to mark the location.
   * @param x horizontal pixel screen location from the the left
   * side of the map.
   * @param y vertical pixel screen location, from the top of the
   * map.
   * @param name the label for the location.
   * @param iconURL a String for a URL for an image.
   * @param justVal the justification value for the label (one of the
   * "OMText.JUSTIFY_..." values).
   * @param vAlignVal vertical alignment value
   */
  public GeneralMapLocation(int x, int y, String name,String iconURL,
                         int justVal, int vAlignVal)
  {
    super(x, y, name, getIconRaster(x, y, iconURL));
    if (location instanceof OMRaster)
    {
      setHorizontalLabelBuffer(
          (((OMRaster)location).getWidth()/2) + IMAGE_SPACING);
    }
    fixupLabel(justVal,vAlignVal);     //fix up label text
  }

  /**
   * Creates a OMRaster-location at a screen x/y position.
   * If the iconURL is null, a small square (dot) will be
   * created to mark the location.
   * @param x horizontal pixel screen location from the the left
   * side of the map.
   * @param y vertical pixel screen location, from the top of the
   * map.
   * @param name the label for the location.
   * @param iconURL a URL for an image.
   * @param justVal the justification value for the label (one of the
   * "OMText.JUSTIFY_..." values).
   * @param vAlignVal vertical alignment value
   */
  public GeneralMapLocation(int x, int y, String name,URL iconURL,
                         int justVal, int vAlignVal)
  {
    super(x, y, name, getIconRaster(x, y, iconURL));
    if (location instanceof OMRaster)
    {
      setHorizontalLabelBuffer(
          (((OMRaster)location).getWidth()/2) + IMAGE_SPACING);
    }
    fixupLabel(justVal,vAlignVal);     //fix up label text
  }

  /**
   * Creates a OMRaster-location at a screen x/y position.
   * If the iconURL is null, a small square (dot) will be
   * created to mark the location.
   * @param latitude latitide in decimal degrees.
   * @param longitude longitude in decimal degrees.
   * @param xOffset horizontal pixel screen location from the
   * longitude map point.
   * @param yOffset vertical pixel screen location, from the
   * latitide map point.
   * @param name the label for the location.
   * @param iconURL a String for a URL for an image.
   * @param justVal the justification value for the label (one of the
   * "OMText.JUSTIFY_..." values).
   * @param vAlignVal vertical alignment value
   */
  public GeneralMapLocation(double latitude, double longitude,
                         int xOffset, int yOffset, String name,String iconURL,
                         int justVal, int vAlignVal)
  {
    super(latitude, longitude, xOffset, yOffset, name,
          getIconRaster(latitude, longitude, xOffset, yOffset, iconURL));
    if (location instanceof OMRaster)
    {
      setHorizontalLabelBuffer(
          (((OMRaster)location).getWidth()/2) + IMAGE_SPACING);
    }
    fixupLabel(justVal,vAlignVal);     //fix up label text
  }

  /**
   * Creates a OMRaster-location at a screen x/y position.
   * If the iconURL is null, a small square (dot) will be
   * created to mark the location.
   * @param latitude latitide in decimal degrees.
   * @param longitude longitude in decimal degrees.
   * @param xOffset horizontal pixel screen location from the
   * longitude map point.
   * @param yOffset vertical pixel screen location, from the
   * latitide map point.
   * @param name the label for the location.
   * @param iconURL a URL for an image.
   * @param justVal the justification value for the label (one of the
   * "OMText.JUSTIFY_..." values).
   * @param vAlignVal vertical alignment value
   */
  public GeneralMapLocation(double latitude, double longitude,
                         int xOffset, int yOffset, String name,URL iconURL,
                         int justVal, int vAlignVal)
  {
    super(latitude, longitude, xOffset, yOffset, name,
          getIconRaster(latitude, longitude, xOffset, yOffset, iconURL));
    if (location instanceof OMRaster)
    {
      setHorizontalLabelBuffer(
          (((OMRaster)location).getWidth()/2) + IMAGE_SPACING);
    }
    fixupLabel(justVal,vAlignVal);     //fix up label text
  }

  /**
   * Creates a OMRaster-location at a latitude/longtude location.
   * If the iconURL is null, a small square (dot) will be
   * created to mark the location.
   * @param latitude latitide in decimal degrees.
   * @param longitude longitude in decimal degrees.
   * @param name the label for the location.
   * @param iconURL a string to a URL for an image.
   */
  public GeneralMapLocation(double latitude, double longitude,
                         String name, String iconURL)
  {
    this(latitude, longitude, name, iconURL,OMText.JUSTIFY_LEFT,
         VALIGN_MIDDLE);
  }

  /**
   * Creates a OMRaster-location at a latitude/longtude location.
   * If the iconURL is null, a small square (dot) will be
   * created to mark the location.
   * @param latitude latitide in decimal degrees.
   * @param longitude longitude in decimal degrees.
   * @param name the label for the location.
   * @param iconURL a URL for an image.
   */
  public GeneralMapLocation(double latitude, double longitude,
                         String name, URL iconURL)
  {
    this(latitude, longitude, name, iconURL, OMText.JUSTIFY_LEFT,
         VALIGN_MIDDLE);
  }

  /**
   * Creates a OMRaster-location at a screen x/y position.
   * If the iconURL is null, a small square (dot) will be
   * created to mark the location.
   * @param x horizontal pixel screen location from the the left
   * side of the map.
   * @param y vertical pixel screen location, from the top of the
   * map.
   * @param name the label for the location.
   * @param iconURL a String for a URL for an image.
   */
  public GeneralMapLocation(int x, int y, String name,String iconURL)
  {
    this(x, y, name, iconURL, OMText.JUSTIFY_LEFT, VALIGN_MIDDLE);
  }

  /**
   * Creates a OMRaster-location at a screen x/y position.
   * If the iconURL is null, a small square (dot) will be
   * created to mark the location.
   * @param x horizontal pixel screen location from the the left
   * side of the map.
   * @param y vertical pixel screen location, from the top of the
   * map.
   * @param name the label for the location.
   * @param iconURL a URL for an image.
   */
  public GeneralMapLocation(int x, int y, String name,URL iconURL)
  {
    this(x, y, name, iconURL, OMText.JUSTIFY_LEFT, VALIGN_MIDDLE);
  }

  /**
   * Creates a OMRaster-location at a screen x/y position.
   * If the iconURL is null, a small square (dot) will be
   * created to mark the location.
   * @param latitude latitide in decimal degrees.
   * @param longitude longitude in decimal degrees.
   * @param xOffset horizontal pixel screen location from the
   * longitude map point.
   * @param yOffset vertical pixel screen location, from the
   * latitide map point.
   * @param name the label for the location.
   * @param iconURL a String for a URL for an image.
   */
  public GeneralMapLocation(double latitude, double longitude,
                         int xOffset, int yOffset, String name,String iconURL)
  {
    this(latitude, longitude, xOffset, yOffset, name, iconURL,
         OMText.JUSTIFY_LEFT, VALIGN_MIDDLE);
  }

  /**
   * Creates a OMRaster-location at a screen x/y position.
   * If the iconURL is null, a small square (dot) will be
   * created to mark the location.
   * @param latitude latitide in decimal degrees.
   * @param longitude longitude in decimal degrees.
   * @param xOffset horizontal pixel screen location from the
   * longitude map point.
   * @param yOffset vertical pixel screen location, from the
   * latitide map point.
   * @param name the label for the location.
   * @param iconURL a URL for an image.
   */
  public GeneralMapLocation(double latitude, double longitude,
                         int xOffset, int yOffset, String name,URL iconURL)
  {
    this(latitude, longitude, xOffset, yOffset, name, iconURL,
         OMText.JUSTIFY_LEFT, VALIGN_MIDDLE);
  }

  /**
   * Enters extra offsets values for the symbol's label text
   * @param xOffs extra label X offset
   * @param yOffs extra label y offset
   */
  public void setLabelOffsets(int xOffs,int yOffs)
  {
    origXLabelOffset -= extraLabelXOffs;    //take out current label offsets
    origYLabelOffset -= extraLabelYOffs;
    origXLabelOffset += (extraLabelXOffs=xOffs); //put in new label offsets
    origYLabelOffset += (extraLabelYOffs=yOffs);
  }

  /**
   * Fixes up justification and offsets for label text.
   * @param justVal the justification value for the label (one of the
   * "OMText.JUSTIFY_..." values).
   * @param vAlignVal vertical alignment value
   */
  protected void fixupLabel(int justVal, int vAlignVal)
  {
    final OMText labelObj;        //if different justify then change it:
    if((labelObj=getLabel()) != null && labelObj.getJustify() != justVal)
      labelObj.setJustify(justVal);
    origXLabelOffset += extraLabelXOffs;    //put in "extra" label offsets
    origYLabelOffset += extraLabelYOffs;
    //setup spacing value based on label-text justification:
    if(justVal == OMText.JUSTIFY_LEFT)
      origXLabelOffset += LABEL_X_SPACING;
    else if(justVal == OMText.JUSTIFY_RIGHT)
      origXLabelOffset -= LABEL_X_SPACING;
    //setup vertical spacing offset:
    switch(vAlignVal)
    {
      case VALIGN_TOP:            //marker above label text
        origYLabelOffset += LABEL_Y_SPACING;
        break;
      case VALIGN_BOTTOM:         //marker below label text
        origYLabelOffset -= LABEL_Y_SPACING;
        break;
      case VALIGN_UNDERLINE:      //marker below label (tighter)
        origYLabelOffset -= LABEL_Y_SPACING/2;
        break;
    }
  }

  /**
   * Returns a new OMRaster at a latitude/longitude, from a image URL.
   * @param lat latitide in decimal degrees.
   * @param lon longitude in decimal degrees.
   * @param iconURL a URL for an image.
   * @return OMRaster
   */
  public static OMRaster getIconRaster(double lat, double lon, String iconURL)
  {
    URL url = getIconRasterURL(iconURL);
    if (url == null)
      return null;
    return getIconRaster(lat, lon, url);
  }

  /**
   * Returns a new OMRaster at a latitude/longitude, from a image URL.
   * @param lat latitide in decimal degrees.
   * @param lon longitude in decimal degrees.
   * @param iconURL a URL for an image.
   * @return OMRaster
   */
  public static OMRaster getIconRaster(double lat, double lon, URL iconURL)
  {
    ImageIcon icon = new ImageIcon(iconURL);
    int offX = icon.getIconWidth() / 2;
    int offY = icon.getIconHeight() / 2;
    return new OMRaster(lat, lon, -offX, -offY, icon);
  }

  /**
   * Returns a new OMRaster at a latitude/longitude, from a image URL.
   * @param x horizontal pixel screen location from the the left
   * side of the map.
   * @param y vertical pixel screen location, from the top of the
   * map.
   * @param iconURL a String for a URL for an image.
   * @return OMRaster
   */
  public static OMRaster getIconRaster(int x, int y, String iconURL)
  {
    URL url = getIconRasterURL(iconURL);
    if (url == null)
      return null;
    return getIconRaster(x, y, url);
  }

  /**
   * Returns a new OMRaster at a latitude/longitude, from a image URL.
   * @param x horizontal pixel screen location from the the left
   * side of the map.
   * @param y vertical pixel screen location, from the top of the
   * map.
   * @param iconURL a URL for an image.
   * @return OMRaster
   */
  public static OMRaster getIconRaster(int x, int y, URL iconURL)
  {
    ImageIcon icon = new ImageIcon(iconURL);
    int offX = icon.getIconWidth() / 2;
    int offY = icon.getIconHeight() / 2;
    return new OMRaster(x-offX, y-offY, icon);
  }

  /**
   * Returns a new OMRaster at a latitude/longitude, from a image URL.
   * @param lat latitide in decimal degrees.
   * @param lon longitude in decimal degrees.
   * @param x horizontal pixel screen location from the
   * longitude map point.
   * @param y vertical pixel screen location, from the
   * latitide map point.
   * @param iconURL a String for URL for an image.
   * @return OMRaster
   */
  public static OMRaster getIconRaster(double lat, double lon,
                                       int x, int y, String iconURL)
  {
    URL url = getIconRasterURL(iconURL);
    if (url == null)
      return null;
    return getIconRaster(lat, lon, x, y, url);
  }

  /**
   * Returns a new OMRaster at a latitude/longitude, from a image URL.
   * @param lat latitide in decimal degrees.
   * @param lon longitude in decimal degrees.
   * @param x horizontal pixel screen location from the
   * longitude map point.
   * @param y vertical pixel screen location, from the
   * latitide map point.
   * @param iconURL a URL for an image.
   * @return OMRaster
   */
  public static OMRaster getIconRaster(double lat, double lon,
                                       int x, int y, URL iconURL)
  {
    ImageIcon icon = new ImageIcon(iconURL);
    int offX = icon.getIconWidth() / 2;
    int offY = icon.getIconHeight() / 2;
    return new OMRaster(lat, lon, x-offX, y-offY, icon);
  }

  /**
   * Returns a new ImageIcon from a String to an image URL.
   * @param iconURL a string to a URL for an image.
   * @return new URL
   */
  public static URL getIconRasterURL(String iconURL)
  {
    try
    {
      return new URL(iconURL);
    }
    catch (java.net.MalformedURLException mue)
    {
      new com.bbn.openmap.util.HandleError(mue);
    }
    //Java7 warning as unreachable exception catch
    //catch (java.io.IOException ioe)
    //{
    //  new com.bbn.openmap.util.HandleError(ioe);
    //}
    return null;
  }

  /**
   * Sets the placement of the location.  Overidden version that does not
   * add "DEFAULT_SPACING" to the X-position of the label-text (now taken
   * care of in constructor, with correct offsets depending on label-text
   * justification).
   * @param latitude the latitude, in decimal degrees, of the location.
   * @param longitude the longitude, in decimal degrees, of the location.
   */
  @Override
  public void setLocation(double latitude, double longitude)
  {
    lat = latitude;
    lon = longitude;

    origXLabelOffset = origYLabelOffset = 0;

    setRenderType(RENDERTYPE_LATLON);
    if (location != null && label != null)
      setGraphicLocations(latitude, longitude);
  }

  /**
   * Sets the placement of the location.  Overidden version that does not
   * add "DEFAULT_SPACING" to the X-position of the label-text (now taken
   * care of in constructor, with correct offsets depending on label-text
   * justification).
   * @param x location x-position
   * @param y location y-position
   */
  @Override
  public void setLocation(int x, int y)
  {
    this.x = x;
    this.y = y;

    origXLabelOffset = x;
    origYLabelOffset = y;

    setRenderType(RENDERTYPE_XY);
    if (location != null && label != null)
      setGraphicLocations(x, y);
  }

  /**
   * Sets the placement of the location.  Overidden version that does not
   * add "DEFAULT_SPACING" to the X-position of the label-text (now taken
   * care of in constructor, with correct offsets depending on label-text
   * justification).
   * @param latitude the latitude, in decimal degrees, of the location.
   * @param longitude the longitude, in decimal degrees, of the location.
   * @param xOffset the pixel location of the object from the longitude.
   * @param yOffset the pixel location of the object from the latitude.
   */
  @Override
  public void setLocation(double latitude, double longitude,
                          int xOffset, int yOffset)
  {
    lat = latitude;
    lon = longitude;
    this.xOffset = xOffset;
    this.yOffset = yOffset;

    origXLabelOffset = xOffset;
    origYLabelOffset = yOffset;

    setRenderType(RENDERTYPE_OFFSET);
    if (location != null && label != null)
      setGraphicLocations(latitude, longitude, xOffset, yOffset);
  }

  /**
   * Given a new latitude/longitude, reposition the graphic and
   * label.
   * @param latitude the latitude, in decimal degrees, of the location.
   * @param longitude the longitude, in decimal degrees, of the location.
   */
  @Override
  public void setGraphicLocations(double latitude, double longitude)
  {
    if(location instanceof OMCircle)
    {
      final OMCircle c = (OMCircle)location;
      c.setLatLon(latitude, longitude);
    }
    else if(location instanceof OMRect)
    {
      final OMRect r = (OMRect)location;
      r.setLocation(latitude, longitude, r.getLeft(), r.getTop(),
                    r.getRight(), r.getBottom());
    }
    else if(location instanceof OMRaster)
    {
      final OMRaster ras = (OMRaster) location;
      ras.setLat(latitude);
      ras.setLon(longitude);
      setHorizontalLabelBuffer(
          (((OMRaster)location).getWidth()/2) + IMAGE_SPACING);
    }
    label.setLat(latitude);
    label.setLon(longitude);
  }

  /**
   * Given a new x/y screen location, reposition the graphic and
   * label.
   * @param x location x-position
   * @param y location y-position
   */
  @Override
  public void setGraphicLocations(int x, int y)
  {
    if(location instanceof OMCircle)
    {
      final OMCircle c = (OMCircle)location;
      c.setX(x);
      c.setY(y);
    }
    else if(location instanceof OMRect)
    {
      final OMRect r = (OMRect)location;
      final int dx = x - (r.getLeft() + r.getRight())/2;
      final int dy = y - (r.getTop() + r.getBottom())/2;
      r.setLocation(r.getLeft()+dx, r.getTop()+dy, r.getRight()+dx,
                    r.getBottom()+dy);
    }
    else if(location instanceof OMRaster)
    {
      final OMRaster ras = (OMRaster) location;
      ras.setX(x);
      ras.setY(y);
      setHorizontalLabelBuffer(
          (((OMRaster)location).getWidth()/2) + IMAGE_SPACING);
    }
    label.setX(x + extraLabelXOffs);
    label.setY(y + extraLabelYOffs);
  }

  /**
   * Given a new latitude/longitude with x/y offset points,
   * reposition the graphic and label.
   * @param latitude the latitude, in decimal degrees, of the location.
   * @param longitude the longitude, in decimal degrees, of the location.
   * @param xOffset the pixel location of the object from the longitude.
   * @param yOffset the pixel location of the object from the latitude.
   */
  @Override
  public void setGraphicLocations(double latitude, double longitude,
                                  int xOffset, int yOffset)
  {
    if(location instanceof OMCircle)
    {
      final OMCircle c = (OMCircle)location;
      c.setLatLon(latitude, longitude);
      c.setOffX(xOffset);
      c.setOffY(yOffset);
    }
    else if(location instanceof OMRect)
    {
      final OMRect r = (OMRect)location;
      r.setLocation(latitude, longitude,xOffset-r.getLeft(),
                    yOffset-r.getTop(),xOffset+r.getRight(), yOffset+r.getBottom());
    }
    else if(location instanceof OMRaster)
    {
      final OMRaster ras = (OMRaster) location;
      ras.setLat(latitude);
      ras.setLon(longitude);
      ras.setX(xOffset);
      ras.setY(yOffset);
      setHorizontalLabelBuffer(
          (((OMRaster)location).getWidth()/2) + IMAGE_SPACING);
    }
    label.setLat(latitude);
    label.setLon(longitude);
    label.setX(xOffset + extraLabelXOffs);
    label.setY(yOffset + extraLabelYOffs);
  }

  /**
   * Set the fill java.awt.Paint for the marker graphic.
   * @param locationPaint location paint
   */
  public void setLocationFillPaint(java.awt.Paint locationPaint)
  {
    if (location != null)
      location.setFillPaint(locationPaint);
  }

  /**
   * Sets the symbol number (value from QPager/CUBE symbol ".SMB" file).
   * @param val symbol number value
   */
  public void setSymbolNumber(int val)
  {
    symbolNumber = val;
  }

  /**
   * Returns the symbol number (value from QPager/CUBE symbol ".SMB"
   * file).
   * @return symbol number
   */
  public int getSymbolNumber()
  {
    return symbolNumber;
  }

  /**
   * If the application-object held by this location has been set then
   * the results of the '.toString()' method for that object is returned;
   * otherwise a String representing the latitude, longitude and name
   * of this location is returned.
   * @return string representing the location
   */
  @Override
  public String toString()
  {
    final Object obj;
    if((obj=getAppObject()) != null)
      return obj.toString();
    return lat + " " + lon + " " + name;
  }
}
