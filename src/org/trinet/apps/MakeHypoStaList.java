package org.trinet.apps;

import org.trinet.jasi.*;

/**
 * Make a Hypoinverse #2 format station list for the currently active channels
 * in the default dbase. Only returns "HH_", "EH_", "HL_" channels.
 */
public class MakeHypoStaList {

    private static final StringBuilder result = new StringBuilder();

    public MakeHypoStaList() {
    }

    public static void main(String args[]) {
        //   System.out.println ("Making connection... ");
        if (args.length > 0 ) {
             TestDataSource.create(args[0], args[1], args[2], args[3], args[4], args[5]);
        } else {
            TestDataSource.create();
        }
        //
        //   System.out.println (dbase.toString());
        //ChannelList list = ChannelList.readCurrentList();
        //
        String compList[] = {"HH_", "EH_", "HL_"};
        ChannelList list = ChannelList.getByComponent(compList);
        Channel ch[] = list.getArray();

        for (int i = 0; i < ch.length; i++) {
            result.append(HypoFormat.toH2StationString(ch[i])).append("\n");
            //System.out.println(HypoFormat.toH2StationString(ch[i]));
        }
        System.out.println(result.toString());
    }
    // end of Main

    public static String getResult() {
        return result.toString();
    }
} 
