package test.org.trinet.apps;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.trinet.apps.CalcML;
import test.common.DatabaseTests;
import test.testqueries.common.TestQueries;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CalcMLTests extends DatabaseTests {

    public static final int CalcMLTestEventId = 39520735;
    public static final int CalcMLTestAmpId = 296400735;
    public static final int CalcMLTestMagId = 10619439;

    @BeforeAll
    public static void populateEventDataForCubeEventLineTest() throws Exception {
        TestQueries testQueries = new TestQueries(postgreSQLContainer.getJdbcUrl());
        int rows = testQueries.populateEventDataForCalcMlTest();
        assertTrue(rows > 0);
        rows = testQueries.populateAssocammDataForCalcMLTest();
        assertTrue(rows > 0);
        rows = testQueries.populateNetMagDataForCalcMLTest();
        assertTrue(rows > 0);
        rows = testQueries.populateAmpDataForCalcMLTest();
        assertTrue(rows > 0);
        rows = testQueries.populateEventPrefMagDataForCalcMLTest();
        assertTrue(rows > 0);
    }

    @Test
    public void verify_CalcML_main_function_outputs_correct_associated_magnitude() throws Exception {
        CalcML.main(new String[]{String.valueOf(CalcMLTestEventId), "CalcML-test.props"});
        assertTrue(CalcML.getAssociatedMagnitudes().get(0)
                .contains("BC UABX  HHE -- 99999.00   0 08:25:00.355    0.0389 WAS    cm      T      0.00    NaN  NaN      RT1  T  1.35  0.02 -0.34  1.00  1.00 A"));
    }
}
