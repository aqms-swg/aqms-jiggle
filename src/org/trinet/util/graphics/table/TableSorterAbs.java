/**
 * A sorter for TableModels. The sorter has a model (conforming to TableModel) 
 * and itself implements TableModel. TableSorter does not store or copy 
 * the data in the TableModel, instead it maintains an array of 
 * integers which it keeps the same size as the number of rows in its 
 * model. When the model changes it notifies the sorter that something 
 * has changed eg. "rowsAdded" so that its internal array of integers 
 * can be reallocated. As requests are made of the sorter (like 
 * getValueAt(row, col) it redirects them to its model via the mapping 
 * array. That way the TableSorter appears to hold another copy of the table 
 * with the rows in a different order. The sorting algorthm used is stable 
 * which means that it does not move around rows when its comparison 
 * function returns 0 to denote that they are equivalent. 
 *
 * @version 1.5 12/17/97 orginal
 * @author Philip Milne modified by AWW 
 */

package org.trinet.util.graphics.table;

import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import org.trinet.jdbc.datatypes.*;

// Imports for picking up mouse events from the JTable. 

public class TableSorterAbs extends TableMap {
    protected int         indexes[];
    protected ArrayList   sortingColumns = new ArrayList(128); // for multi column key
    protected boolean     isDefaultSortAscending = true; // used to be false -aww 2009/03/25
    protected boolean     ascending = true;
    protected int         compares = 0;
    protected int         lastColumnSorted = -1;  // added 10/24/2002 aww assumes single column sort
    protected TableModel  lastSetModel;  // added 10/24/2002 aww

    public TableSorterAbs() {
        indexes = new int [0]; // For consistency.        
    }

    public TableSorterAbs(TableModel model) {
        setModel(model);
    }
    public boolean isDefaultSortAscending() {
        return isDefaultSortAscending;
    }
    public void setDefaultSortAscending() {
        isDefaultSortAscending = true;
    }
    public void setDefaultSortDescending() {
        isDefaultSortAscending = false;
    }
    public void setModel(TableModel model) {
        super.setModel(model); 
        lastSetModel = model;  // added 10/24/2002 aww
        reallocateIndexes(); 
    }

    private boolean useAbsValue(int column) {
        String colName = model.getColumnName(column).toUpperCase();
        switch (colName) {
        case "MAG":
            return false;
        case "Z":
            return false;
        default:
            if (colName.indexOf("DATE") != -1 || colName.indexOf("TIME") != -1) {
                return false;
            }
            return true;
        }
    }
 
    private double doubleValue(double d, boolean useAbsValue) {
        if (useAbsValue) {
            d = Math.abs(d);
        }
        return d;
    }

    public int compareRowsByColumn(int row1, int row2, int column)
    {
        Class type = model.getColumnClass(column);
        boolean useAbsValue = useAbsValue(column);
        Class superClass = type.getSuperclass();

        TableModel data = model;

        Object o1 = data.getValueAt(row1, column);
        Object o2 = data.getValueAt(row2, column); 

        //System.out.println("Superclass: " + superClass.getName() + " class:" +o1.getClass().getName());
        DataObject dataObject1 = null;
        DataObject dataObject2 = null;

        // Check for nulls
        if (superClass == DataObject.class) {
            dataObject1 = (DataObject) o1;
            dataObject2 = (DataObject) o2;
        
            if (dataObject1.isNull() && dataObject2.isNull()) { // If both values are null return 0
                return 0;
            }
            else if (dataObject1.isNull()) { // Define null greater than everything (nulls last?). 
                return (ascending) ? +1 : -1;
            } 
            else if (dataObject2.isNull()) { 
                return (ascending) ? -1 : +1; 
            }
        }
        else {
            if (o1 == null && o2 == null) {
                return 0; 
            }
            else if (o1 == null) { 
                return (ascending) ? +1 : -1;
            } 
            else if (o2 == null) { 
                return (ascending) ? -1 : +1; 
            }
        }

        if (superClass == java.lang.Number.class) {
// aww added absolute value variation 6/4/99 to original sorter; magnitude not sign is issue.
                Number n1 = (Number) o1;
                double d1 = doubleValue(n1.doubleValue(), useAbsValue);
                Number n2 = (Number) o2;
                double d2 = doubleValue(n2.doubleValue(), useAbsValue);
                
                if (Double.isNaN(d1) && Double.isNaN(d2) )
                    return 0;
                if (Double.isNaN(d1) && !Double.isNaN(d2) )
                    return (ascending) ? +1 : -1;
                if (!Double.isNaN(d1) && Double.isNaN(d2) )
                    return (ascending) ? -1 : +1; 

                if (d1 < d2)
                    return -1;
                else if (d1 > d2)
                    return 1;
                else
                    return 0;
        }

        else if (superClass == DataObject.class) {
          if (DataNumeric.class.isInstance(o1)) {
                double d1 = doubleValue(dataObject1.doubleValue(), useAbsValue);
                double d2 = doubleValue(dataObject2.doubleValue(), useAbsValue);

                if (Double.isNaN(d1) && Double.isNaN(d2) )
                    return 0;
                if (Double.isNaN(d1) && !Double.isNaN(d2) )
                    return (ascending) ? +1 : -1;
                if (!Double.isNaN(d1) && Double.isNaN(d2) )
                    return (ascending) ? -1 : +1; 
                
                if (d1 < d2)
                    return -1;
                else if (d1 > d2)
                    return 1;
                else
                    return 0;
          }
          else {
                int result = dataObject1.compareTo(dataObject2);

                if (result < 0)
                    return -1;
                else if (result > 0)
                    return 1;
                else return 0;
          }
        }
        else if (type == java.util.Date.class || type.getSuperclass() == java.util.Date.class) {
                Date d1 = (Date) o1;
                Date d2 = (Date) o2;
                /* removed 2008/02/07 for general subclass case
                long n1 = d1.getTime();
                long n2 = d2.getTime();
                if (n1 < n2)
                    return -1;
                else if (n1 > n2)
                    return 1;
                else return 0;
                */
                return d1.compareTo(d2); // use override methods for general subclass case -aww 2008/02/07 
        }
        else if (type == String.class) {
                String s1 = (String) o1;
                String s2    = (String) o2;
                int result = s1.compareTo(s2);

                if (result < 0)
                    return -1;
                else if (result > 0)
                    return 1;
                else return 0;
        }
        else if (type == Boolean.class) {
                Boolean bool1 = (Boolean) o1;
                boolean b1 = bool1.booleanValue();
                Boolean bool2 = (Boolean) o2;
                boolean b2 = bool2.booleanValue();
                if (b1 == b2)
                    return 0;
                else if (b1) // Define false < true
                    return 1;
                else
                    return -1;
        }
        else { // convert object to string and compare
                String s1 = o1.toString();
                String s2 = o2.toString();
                int result = s1.compareTo(s2);
                if (result < 0)
                    return -1;
                else if (result > 0)
                    return 1;
                else return 0;
        }
    }

    public int compare(int row1, int row2)
    {
        compares++;
        int size = sortingColumns.size();
        for(int level = 0; level < size; level++)
            {
                Integer column = (Integer)sortingColumns.get(level);
                int result = compareRowsByColumn(row1, row2, column.intValue());
                if (result != 0)
                    return ascending ? result : -result;
            }
        return 0;
    }
    public void resetSort() { // added 10/24/2002 aww
       lastColumnSorted = -1;
       ascending = isDefaultSortAscending;
       sortingColumns.clear();
       reallocateIndexes(); //aww 03/18/03
       super.tableChanged(new TableModelEvent(this)); // all rows changed, aww 03/18/03
    }
    public void  reallocateIndexes()
    {
        int rowCount = model.getRowCount();

        // Set up a new array of indexes with the right number of elements
        // for the new data model.
        indexes = new int[rowCount];

        // Initialise with the identity mapping.
        for(int row = 0; row < rowCount; row++)
            indexes[row] = row;
        // note resorts unless resetSort was done prior to reallocate
        if (lastColumnSorted != -1) sortByColumn(lastColumnSorted, ascending); // to handle edits test aww 10/24/2002
    }

    public void tableChanged(TableModelEvent e)
    {
        //System.out.println("Sorter: tableChange is update only: " + (e.getType() == TableModelEvent.UPDATE)); 
        //reallocateIndexes(); // removed 10/24/2002 aww
        if (indexes.length != model.getRowCount() || e.getType() != TableModelEvent.UPDATE) {
          if (lastColumnSorted == -1) {
            int cnt = getColumnCount();
            String colName = null;
            for (int i=0; i<cnt; i++) {
               colName = getColumnName(i);
               if (colName.indexOf("DATE") >= 0) {
                   lastColumnSorted = i;
                   break;
               }
            }
           }
           //System.out.println("Sorter: tableChange update lastColumnSorted: " + getColumnName(lastColumnSorted) + " ascending: " + ascending);
           reallocateIndexes(); // aww 10/24/2002 test for edits
        }
        super.tableChanged(e);
    }

    public void checkModel()
    {
        if (indexes.length != model.getRowCount() || lastSetModel != model) {
            System.err.println("TableSorterAbs: Row sorter not informed of a change in model: " + ((Object) model).toString());
        }
    }

    public void  sort(Object sender)
    {
        checkModel();

        compares = 0;
        // n2sort();
        // qsort(0, indexes.length-1);
        shuttlesort((int[])indexes.clone(), indexes, 0, indexes.length);
//        Debug.println("Compares: "+compares);
    }

    public void n2sort() {
        for(int i = 0; i < getRowCount(); i++) {
            for(int j = i+1; j < getRowCount(); j++) {
                if (compare(indexes[i], indexes[j]) == -1) {
                    swap(i, j);
                }
            }
        }
    }

    // This is a home-grown implementation which we have not had time
    // to research - it may perform poorly in some circumstances. It
    // requires twice the space of an in-place algorithm and makes
    // NlogN assigments shuttling the values between the two
    // arrays. The number of compares appears to vary between N-1 and
    // NlogN depending on the initial order but the main reason for
    // using it here is that, unlike qsort, it is stable.
    public void shuttlesort(int from[], int to[], int low, int high) {
        if (high - low < 2) {
            return;
        }
        int middle = (low + high)/2;
        shuttlesort(to, from, low, middle);
        shuttlesort(to, from, middle, high);

        int p = low;
        int q = middle;

        /* This is an optional short-cut; at each recursive call,
        check to see if the elements in this subset are already
        ordered.  If so, no further comparisons are needed; the
        sub-array can just be copied.  The array must be copied rather
        than assigned otherwise sister calls in the recursion might
        get out of sinc.  When the number of elements is three they
        are partitioned so that the first set, [low, mid), has one
        element and and the second, [mid, high), has two. We skip the
        optimisation when the number of elements is three or less as
        the first compare in the normal merge will produce the same
        sequence of steps. This optimisation seems to be worthwhile
        for partially ordered lists but some analysis is needed to
        find out how the performance drops to Nlog(N) as the initial
        order diminishes - it may drop very quickly.  */

        if (high - low >= 4 && compare(from[middle-1], from[middle]) <= 0) {
            for (int i = low; i < high; i++) {
                to[i] = from[i];
            }
            return;
        }

        // A normal merge. 

        for(int i = low; i < high; i++) {
            if (q >= high || (p < middle && compare(from[p], from[q]) <= 0)) {
                to[i] = from[p++];
            }
            else {
                to[i] = from[q++];
            }
        }
    }

    public void swap(int i, int j) {
        int tmp = indexes[i];
        indexes[i] = indexes[j];
        indexes[j] = tmp;
    }

    // The mapping only affects the contents of the data rows.
    // Pass all requests to these rows through the mapping array: "indexes".

    public Object getValueAt(int aRow, int aColumn)
    {
        checkModel();
        return model.getValueAt(indexes[aRow], aColumn);
    }

    public void setValueAt(Object aValue, int aRow, int aColumn)
    {
        checkModel();
        //System.out.println("TableSorterAbs.setValueAt row: "+aRow+" col: "+aColumn+" index: "+indexes[aRow]);
        model.setValueAt(aValue, indexes[aRow], aColumn);
    }

    public void sortByColumn(int column) {
        sortByColumn(column, isDefaultSortAscending);
    }

    public void sortByColumn(int column, boolean ascending) {
        this.ascending = ascending;
        this.lastColumnSorted = column; // added 10/24/2002 aww
        sortingColumns.clear();
        sortingColumns.add(Integer.valueOf(column));
        sort(this);
        super.tableChanged(new TableModelEvent(this)); 
    }

    // Add a mouse listener to the Table to trigger a table sort 
    // when a column heading is clicked in the JTable. 
    public void addMouseListenerToHeaderInTable(JTable table) { 
        final TableSorterAbs sorter = this; 
        final JTable tableView = table; 
        tableView.setColumnSelectionAllowed(false);  // ? why this here
        MouseAdapter listMouseListener = new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                TableColumnModel columnModel = tableView.getColumnModel();
                int viewColumn = columnModel.getColumnIndexAtX(e.getX()); 
                int column = tableView.convertColumnIndexToModel(viewColumn); 
                if(e.getClickCount() == 2 && column != -1) {
                    int shiftPressed = e.getModifiers()&InputEvent.SHIFT_MASK; 
                    boolean ascending = (shiftPressed == 0); 
                    System.out.println("TableSorterAbs: Please wait ... Sorting table column "
                         + (String) columnModel.getColumn(viewColumn).getHeaderValue() + (ascending ? " asc" : " desc")); 
                    sorter.sortByColumn(column, ascending); 
                    System.out.println("Sort finished.");
                }
             }
         };
        JTableHeader th = tableView.getTableHeader(); 
        th.addMouseListener(listMouseListener); 
    }
}
