package org.trinet.jiggle;

import java.util.*;

import org.trinet.jasi.EventType;
import org.trinet.jasi.EventTypeMap3;

public class DPeventType extends PreferencesPanelChooser {
    private static final long serialVersionUID = 1L;
    private static final String[] names;
    private static final Map<String, String> toolTipMap;
    static {
        EventType eventType;
        final EventType[] EVENT_TYPES = EventTypeMap3.getEventTypes();
        int size = EVENT_TYPES.length;
        names = new String[size];
        toolTipMap = new HashMap<>(size);
        for (int idx = 0; idx < size; idx++) {
            eventType = EVENT_TYPES[idx];
            names[idx] = eventType.name;
            toolTipMap.put(names[idx], eventType.desc);
        }
        Arrays.sort(names);
    }

    /**
     * Create the Event Type Menu preferences.
     * 
     * @param props the properties.
     */
    public DPeventType(JiggleProperties props) {
        super(props, names, toolTipMap, "eventTypeChoices", "Event type selection menu items", "Menu choices");
    }

    @Override
    protected String getName(String propValue) {
        return EventTypeMap3.fromDbType(propValue);
    }

    @Override
    protected String getPropValue(String name) {
        return EventTypeMap3.toDbType(name);
    }
}
