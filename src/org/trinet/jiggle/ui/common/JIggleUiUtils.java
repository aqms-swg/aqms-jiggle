package org.trinet.jiggle.ui.common;

import java.awt.Desktop;
import java.net.URL;

import javax.swing.JEditorPane;
import javax.swing.JTextPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.JTextComponent;

import org.trinet.jiggle.common.JiggleExceptionHandler;

public class JIggleUiUtils {
    /** HTML text content */
    public static final String HTML_TEXT = "text/html";
    /** Plain text content */
    public static final String PLAIN_TEXT = "text/plain";
 
    /**
     * Add a hyperlink listener.
     * 
     * @param editorPane the editor pane.
     * @return true if added, false otherwise.
     */
    public static boolean addHyperlinkListener(JEditorPane editorPane) {
        final HyperlinkListener listener = createHyperlinkListener();
        if (listener != null) {
            editorPane.addHyperlinkListener(listener);
        }
        return false;
    }

    /**
     * Add a hyperlink listener.
     * 
     * @param textPane the text pane.
     * @return true if added, false otherwise.
     */
    public static boolean addHyperlinkListener(JTextPane textPane) {
        final HyperlinkListener listener = createHyperlinkListener();
        if (listener != null) {
            textPane.addHyperlinkListener(listener);
        }
        return false;
    }

    /**
     * Create a hyperlink listener.
     * 
     * @return the hyperlink listener or null if not supported.
     */
    public static HyperlinkListener createHyperlinkListener() {
        final Desktop desktop;
        if (!Desktop.isDesktopSupported() || !(desktop = Desktop.getDesktop()).isSupported(Desktop.Action.BROWSE)) {
            return null;
        }
        return new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(HyperlinkEvent e) {
                final URL url = e.getURL();
                try {
                    if (e.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED)) {
                        desktop.browse(url.toURI());
                    }
                } catch (Exception ex) {
                    JiggleExceptionHandler.handleException(ex, "Error opening link", "Could not open link: " + url);
                }
            }
        };
    }

    /**
     * Get a message text component for a dialog that the user may copy text from.
     * 
     * @param text the text.
     * @return the message text component.
     */
    public static JTextComponent getMessage(String text) {
        return getMessage(text, PLAIN_TEXT, false);
    }

    /**
     * Get a message text component for a dialog that the user may copy text from.
     * 
     * @param text the text.
     * @param contentType the type of content.
     * @param hyperlinkListener
     * @return the message text component.
     */
    public static JTextComponent getMessage(String text, String contentType, boolean hyperlinkListener) {
        JTextPane message = new JTextPane();
        message.setContentType(contentType);
        message.setEditable(false);
        message.setBackground(null);
        message.setBorder(null);
        message.setText(text);
        if (hyperlinkListener) {
            addHyperlinkListener(message);
        }
        return message;
    }
}
