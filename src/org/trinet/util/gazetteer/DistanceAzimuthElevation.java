package org.trinet.util.gazetteer;
import java.io.Serializable;

/**
 * Object that contains distance, azimuth and elevation between two points.
 *
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: USGS</p>
 * @author Allan Walter
 * @version 1.0
 */
// Java1.2 has toDegrees() and toRadians in java.lang.math
public class DistanceAzimuthElevation implements java.io.Serializable {
    protected double hdist;
    protected double az;
    protected double elev;
    protected GeoidalUnits elevUnitsType;

    private static final org.trinet.util.Format f61 = new org.trinet.util.Format("%6.1f");

// Default constructor initializes distance, azimuth, and elevation to Double.NaN.
//  Elevation units type is degrees.
    protected DistanceAzimuthElevation() {
        hdist = Double.NaN;
        az = Double.NaN;
        elev = Double.NaN;
        elevUnitsType = GeoidalUnits.DEGREES;
    }

    public static DistanceAzimuthElevation create(GeoidalUnits gu) {
    if (! (gu == GeoidalUnits.KILOMETERS  || gu == GeoidalUnits.DEGREES) )
        throw new IllegalArgumentException("DistanceAzimuthElevation:create(GeoidalUnits) type must be km or degrees.");
        DistanceAzimuthElevation dae = new DistanceAzimuthElevation();
        dae.elevUnitsType = gu;
        return dae;
    }

/** Returns horizontal distance in specified type units. Uses default distance units if input parameter is null */
    public double getDistance(GeoidalUnits distanceUnits) {
        if (distanceUnits == null) return hdist;
        if (distanceUnits == GeoidalUnits.KILOMETERS) return getDistanceKm();
        else if (distanceUnits == GeoidalUnits.MILES) return getDistanceMiles();
        else throw new IllegalArgumentException("DistanceAzimuthElevation:getDistance(GeoidalUnits) must be km or miles.");
    }

/** Returns line-of-sight distance (this includes the added range due to the elevation difference).
 * Uses default distance units if input parameter is null.
 * Returns Double.NaN if any lat,lon,z values are Double.NaN.
 * */
    public double getSlantDistance(GeoidalUnits distanceUnits) {
        double hKm = getDistanceKm();
        if (Double.isNaN(hKm) || Double.isNaN(elev)) return hKm;
        double zKm = (elevUnitsType == GeoidalUnits.DEGREES) ? hKm*Math.tan(Math.toRadians(elev)) : elev;
        double slantDistance = Math.sqrt( Math.pow(hKm, 2) + Math.pow(zKm, 2) );
        if (distanceUnits == null || distanceUnits == GeoidalUnits.KILOMETERS) return slantDistance;
        else if (distanceUnits == GeoidalUnits.MILES) return slantDistance*GeoidalConvert.MILES_PER_KM;
        else throw new IllegalArgumentException("DistanceAzimuthElevation:getSlantDistance(GeoidalUnits) must be km or miles.");
    }


/** Returns horizontal distance separation in kilometers. */
    public double getDistanceKm() {
        return hdist;
    }

/** Returns horizontal distance separation in miles. */
    public double getDistanceMiles() {
        double hKm =  getDistanceKm();
        return (Double.isNaN(hKm)) ? hKm : hKm*GeoidalConvert.MILES_PER_KM;
    }

/** Returns azimuth stored as degrees. */
    public double getAzimuth() {
        return az;
    }
/** Returns elevation in units of created type, degrees or kilometers.
 * @see #getElevUnitsType()
 * */
    public double getElevation() {
        return elev;
    }

    public GeoidalUnits getElevUnitsType() {
        return elevUnitsType;
    }

    /** Set the horizontal distance, azimuth, and elevation, assumes elevation is in appropiate units for type.
     * @see #getElevUnitsType()
     * @see #create(GeoidalUnits)
     * */
    public void setDistanceAzimuthElevation(double distance, double azimuth, double elevation) {
        this.hdist = distance;
        this.az = azimuth;
        this.elev = elevation;
    }

/** Given two points calculate and save the distance, azimuth, and elevation from reference point to endpoint.
* (lat1, lon1, z1) are coordinates of the starting reference point, (lat2, lon2, z2) are those of the target endpoint.
* z (height/depth) input parameter is assumed to be in km units, with depth positive downward (i.e. negative for elevation).
*/
    public void setDistanceAzimuthElevation(double lat1, double lon1, double z1, double lat2, double lon2, double z2) {
        hdist = GeoidalConvert.horizontalDistanceKmBetween(lat1, lon1, lat2, lon2);
        az = GeoidalConvert.azimuthDegreesTo(lat1, lon1, lat2, lon2);
        if (elevUnitsType == GeoidalUnits.DEGREES)
            elev = GeoidalConvert.elevationDegreesTo(lat1, lon1, z1, lat2, lon2, z2);
        else
            elev = (z1-z2);
    }

/** Resets values to those calculated using the specified location for the source and target references.
 */
    public void setDistanceAzimuthElevation(Geoidal g1, Geoidal g2) {
        if (g1 == null || g2 == null)
            throw new IllegalArgumentException("DistanceAzimuthElevation:setDistanceAzimuthElevation input parameter null");
        hdist = GeoidalConvert.horizontalDistanceKmBetween(g1,g2);
        az = GeoidalConvert.azimuthDegreesTo(g1,g2);
        if (elevUnitsType == GeoidalUnits.DEGREES)
            elev = GeoidalConvert.elevationDegreesTo(g1, g2);
        else
            elev = (g1.getZ()-g2.getZ());
    }

/** Returns pretty (labelled) String with kilometers as the distance unit. */
    public String toLabeledStringWithKm() {
        return toLabeledStringWithUnits(GeoidalUnits.KILOMETERS);
    }

/** Returns pretty (labelled) String with miles as the distance unit. */
    public String toLabeledStringWithMiles() {
        return toLabeledStringWithUnits(GeoidalUnits.MILES);
    }

/** Returns pretty (labelled) String with the specified units as the distance unit. */
    public String toLabeledStringWithUnits(GeoidalUnits distanceUnits) {
        return toLabeledStringWithUnits(distanceUnits, false);
    }

    public String toLabeledStringWithUnits(GeoidalUnits distanceUnits, boolean includeElev) {
    StringBuffer sb = new StringBuffer(32);
        if (distanceUnits != null) {
            sb.append(f61.form(getDistance(distanceUnits)));
            sb.append(distanceUnits.getLabel()).append(" ");
        }
        else {
            sb.append(f61.form(getDistance(GeoidalUnits.KILOMETERS)));
            sb.append(GeoidalUnits.KILOMETERS.getLabel()).append(" (");
            sb.append(f61.form(getDistance(GeoidalUnits.MILES)));
            sb.append(GeoidalUnits.MILES.getLabel()).append(") ");
        }
        //
        sb.append(GeoidalConvert.directionString(az));
        sb.append(" (");
        sb.append(f61.form((double)Math.round(az)).substring(0,5));
        sb.append(" azimuth");
        if (includeElev) {
            sb.append(", ");
            sb.append(f61.form(elev));
            sb.append(elevUnitsType.getLabel());
            sb.append(" elev");
        }
        sb.append(")");
        return sb.toString();
    }

/** Returns distance,azimuth,elevation formatted as 6.2f character fields 
* using the default storage units.
*/
    public String toString() {
        return toString(null);
    }

/** Returns distance,azimuth,elevation formatted as 6.3f character fields in 
* the specified storage units.
*/
    public String toString(GeoidalUnits distanceUnits) {
        StringBuffer sb = new StringBuffer(32);
        org.trinet.util.Format f63 = new org.trinet.util.Format("%6.3f");
        if (Double.isNaN(hdist)) sb.append("NaN      ");
        sb.append(f63.form(getDistance(distanceUnits))).append(" ");
        sb.append(f63.form(az)).append(" ");
        sb.append(f63.form(elev));
        return sb.toString();
    }
/*
    public static final void main(String args []) {
        DistanceAzimuthElevation dae = new DistanceAzimuthElevation();
        dae.setDistanceAzimuthElevation(9999.99,45.,10.);
        System.out.println(dae.toString());
        System.out.println(dae.toLabeledStringWithUnits(null, false));
        System.out.println(dae.toLabeledStringWithUnits(null, true));
        System.out.println(dae.toLabeledStringWithUnits(GeoidalUnits.KILOMETERS, true));
        System.out.println(dae.toLabeledStringWithUnits(GeoidalUnits.KILOMETERS, false));
        System.out.println(dae.toLabeledStringWithUnits(GeoidalUnits.MILES, true));
        System.out.println(dae.toLabeledStringWithUnits(GeoidalUnits.MILES, false));

    }
*/
} // End of DistanceAzimuthElevation class
