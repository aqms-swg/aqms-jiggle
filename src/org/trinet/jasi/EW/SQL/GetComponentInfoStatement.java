// *** PACKAGE NAME *** \                                     
package org.trinet.jasi.EW.SQL;                                
                                                               
                                                               
// *** IMPORTS *** \                                          
import java.util.*;                                            
import java.sql.Connection;                                    
import java.sql.SQLException;                                  
import org.trinet.jasi.EW.EWChannel;                                  
                                                               
                                                               
/**                                    
 * <!-- Class Description>             
 *                                     
 *                                     
 * Created:  2002/06/21                        
 *                                     
 * @author   ew_oci2java Tool / DK                        
 * @version  0.99                        
 **/                                   
                                       
public class GetComponentInfoStatement extends EWSQLStatement 
{                                      
                                       
  // *** CLASS ATTRIBUTES *** \       
                                       
  // *** CONSTRUCTORS *** \           
  public GetComponentInfoStatement()                          
  {                                    
    sSQLStatement = "Begin Get_Comp_Params(OUT_idCompT => :OUT_idCompT, IN_idChan => :IN_idChan,  IN_tTime => :IN_tTime, OUT_idComp => :OUT_idComp,  OUT_dLat => :OUT_dLat, OUT_dLon => :OUT_dLon,  OUT_dElev => :OUT_dElev,  OUT_dAzm => :OUT_dAzm, OUT_dDip => :OUT_dDip,  OUT_sSta => :OUT_sSta, OUT_sComp => :OUT_sComp,  OUT_sNet => :OUT_sNet, OUT_sLoc => :OUT_sLoc); End;";
    bIsQuery = false;                
    init();                            
  }                                    
                                       
                                       
  public GetComponentInfoStatement(Connection IN_conn)        
  {                                    
    this();                            
    SetConnection(IN_conn);            
  }                                    
                                       
                                       
  // *** CLASS METHODS *** \          
                                       
  protected boolean SetInputParams(Object obj)    
  {                                  
                                     
    EWChannel objEWChannel = (EWChannel)obj;              
                                     
    try                              
    {                                
      cs.setLong(2, objEWChannel.idChan);    
      cs.setInt(3, objEWChannel.tReqTime);    // was tTime
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetComponentInfoStatement:SetInputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end GetComponentInfoStatement::SetInputParams()    
                                     
                                     
  protected boolean RegisterOutputParams()    
  {                                  
    try                              
    {                                
      cs.registerOutParameter(1, java.sql.Types.BIGINT); 
      cs.registerOutParameter(4, java.sql.Types.BIGINT); 
      cs.registerOutParameter(5, java.sql.Types.FLOAT); 
      cs.registerOutParameter(6, java.sql.Types.FLOAT); 
      cs.registerOutParameter(7, java.sql.Types.FLOAT); 
      cs.registerOutParameter(8, java.sql.Types.FLOAT); 
      cs.registerOutParameter(9, java.sql.Types.FLOAT); 
      cs.registerOutParameter(10, java.sql.Types.VARCHAR); 
      cs.registerOutParameter(11, java.sql.Types.VARCHAR); 
      cs.registerOutParameter(12, java.sql.Types.VARCHAR); 
      cs.registerOutParameter(13, java.sql.Types.VARCHAR); 
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetComponentInfoStatement:RegisterOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
    return(true);                    
  }  // end GetComponentInfoStatement:RegisterOutputParams()
                                     
                                     
  protected boolean RetrieveOutputParams(Object obj)
  {                                  
    EWChannel objEWChannel = (EWChannel)obj;             
                                     
    try                              
    {                                
      objEWChannel.idCompT=cs.getLong(1);     
      objEWChannel.idComp=cs.getLong(4);     
      objEWChannel.dLat=cs.getFloat(5);     
      objEWChannel.dLon=cs.getFloat(6);     
      objEWChannel.dElev=cs.getFloat(7);     
      objEWChannel.dAzm=cs.getFloat(8);     
      objEWChannel.dDip=cs.getFloat(9);     
      objEWChannel.sSta=cs.getString(10);     
      objEWChannel.sComp=cs.getString(11);     
      objEWChannel.sNet=cs.getString(12);     
      objEWChannel.sLoc=cs.getString(13);     
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetComponentInfoStatement:RetrieveOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end GetComponentInfoStatement::RetrieveOutputParams()
                                     
                                     
} // end class GetComponentInfoStatement                      
                                       
                                       
//             <EOF>                   
