package org.trinet.util.graphics.table;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class NamedCheckBoxCellEditor extends AbstractCellEditor {
  JCheckBox component;
  private String colName;

  public NamedCheckBoxCellEditor(String colName, Object value) { 
    super();
    this.colName = colName;
    component = new JCheckBox();
    component.setHorizontalAlignment(SwingConstants.CENTER);
    component.setSelected(Boolean.valueOf(value.toString()).booleanValue());
    component.addItemListener(this);
    //component.addActionListener(this);
  }
  
// Methods to implement DefaultCellEditor 
  public boolean startCellEditing(EventObject anEvent) {
    if(anEvent == null) component.requestFocus();
    else if (anEvent instanceof MouseEvent) {
      if (((MouseEvent)anEvent).getClickCount() < clickCountToStart)
        return false;
    }
    return true;
  }
  public Component getComponent() {
    return this.component;
  }

  public Object getCellEditorValue() {
    return component.isSelected() ? Boolean.TRUE : Boolean.FALSE;
  }

  public void setCellEditorValue(Object value) {
    component.setSelected(Boolean.valueOf(value.toString()).booleanValue());
  }

  public Component getTableCellEditorComponent( JTable table, Object value,
        boolean isSelected,int row,int column) {
    if (table.getColumnName(column).equals(colName)) {
      setCellEditorValue(value);
      return component;
    }
    else return super.getTableCellEditorComponent(table,value,isSelected,row,column); 
  }
}
