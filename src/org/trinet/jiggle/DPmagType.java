package org.trinet.jiggle;

import java.util.*;

import org.trinet.jasi.MagTypeIdIF;

public class DPmagType extends PreferencesPanelChooser {
    private static final long serialVersionUID = 1L;
    private static final String[] names;
    private static final Map<String, String> toolTipMap;
    static {
        final String[] MAG_TYPES = MagTypeIdIF.TYPES;
        final String[] TYPES_DESC = MagTypeIdIF.TYPES_DESC;
        int size = MAG_TYPES.length;
        names = new String[size];
        toolTipMap = new HashMap<>(size);
        for (int idx = 0; idx < size; idx++) {
            names[idx] = MAG_TYPES[idx];
            toolTipMap.put(names[idx], TYPES_DESC[idx]);
        }
        Arrays.sort(names);
    }

    protected boolean typeChanged = false;

    /**
     * Create the Mag Type Menu preferences.
     * 
     * @param props the properties.
     */
    public DPmagType(JiggleProperties props) {
        super(props, names, toolTipMap, "EventEdit.magTypeList", "Magnitude type selection menu items", "Menu choices");
    }

    @Override
    protected String getDefaultPropertyValue() {
        return "h n";
    }
}
