package org.trinet.jasi.coda;

import java.sql.*;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

/**
* Abstract type for coda calibrations origininating from TriNet using NCDC schema.
*/
public abstract class MdCodaMagCalibration extends AbstractCodaMagCalibration {

    protected MdCodaMagCalibration() {
        this(null, null);
    }

    protected MdCodaMagCalibration(ChannelIdIF id) {
        this(id, null);
    }

    protected MdCodaMagCalibration(ChannelIdIF id, DateRange dateRange) {
        super(id, dateRange);
        corrType.setValue(CorrTypeIdIF.MD);
    }
    protected MdCodaMagCalibration(ChannelIdIF id, DateRange dateRange, double value) {
        this(id, dateRange, value, null, null);
    }
    protected MdCodaMagCalibration(ChannelIdIF id, DateRange dateRange, double value,
                    String corrFlag) {
        this(id, dateRange, value, corrFlag, null);
    }
    protected MdCodaMagCalibration(ChannelIdIF id, DateRange dateRange, double value,
                    String corrFlag, String authority) {
        super(id, dateRange, value, CorrTypeIdIF.MD, corrFlag, authority);
    }

    public boolean hasMagCorr(MagnitudeAssocJasiReadingIF jr) {
      return (super.hasMagCorr(jr)) ?
          ((org.trinet.jasi.Coda) jr).isMdType() : false; 
    }

    public static MdCodaMagCalibration create() {
        return create(JasiObject.DEFAULT);
    }
    public static final MdCodaMagCalibration create(int schemaType) {
        return create(JasiFactory.suffix[schemaType]);
    }
    public static final MdCodaMagCalibration create(String suffix) {
        return (MdCodaMagCalibration) JasiObject.newInstance("org.trinet.jasi.coda.MdStaCorrCodaMagCalibrView", suffix);
    }

// super.copy?
    public AbstractChannelCalibration createDefaultCalibration(ChannelIdIF chan) {
        MdCodaMagCalibration calibr = MdCodaMagCalibration.create();
        calibr.channelId = (ChannelIdIF) chan.clone(); // try test for cloneable?
        calibr.corr.setValue(getDefaultCorrection());
        calibr.maxAmp.setValue(getDefaultMaxAmp());
        calibr.clipAmp.setValue(getDefaultClipAmp());
        calibr.gainCorr.setValue(getDefaultGainCorr());
        calibr.codaClipAmp.setValue(getDefaultCodaClipAmp());
        calibr.codaCutoffAmp.setValue(getDefaultCodaCutoffAmp());
        return calibr;
    }
} // end of class MdCodaMagCalibration
