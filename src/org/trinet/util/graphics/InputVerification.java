package org.trinet.util.graphics;
import javax.swing.*;

/** Temporary interface to implement in jdk1.2 for jdk1.3 JComponent verification. */
public interface InputVerification {
    InputVerifier getInputVerifier() ;
    void setInputVerifier(InputVerifier inputVerifier ) ;

    /** This is a class that is added to JDK1.3 and is put here for compatiblity
     *  with v1.2.2*/
    public class InputVerifier {

        public InputVerifier() {}

        public boolean verify(JComponent input) {
            return true;
        }

        public boolean shouldYieldFocus(JComponent input) {
            return verify(input);
        }
    }
}
