package org.trinet.util.gazetteer;
public interface HorizontalDistanceBearingIF extends DistanceBearingIF {
    public double getHorizontalDistance();
    public void   setHorizontalDistance(double dist);
    public double getVerticalDistance();
}
