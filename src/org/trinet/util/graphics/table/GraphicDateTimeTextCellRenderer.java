package org.trinet.util.graphics.table;
import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import org.trinet.util.LeapSeconds;

public class GraphicDateTimeTextCellRenderer extends GraphicTextCellRenderer {

    static final int LENGTH_OF_EPOCHTIME_STRING = 24;

    public GraphicDateTimeTextCellRenderer(FontMetrics metrics, Color defaultBackgroundColor, Color selectedBackgroundColor) {
        super(metrics, defaultBackgroundColor, selectedBackgroundColor);
    }

    public void setValue(Object value) {
        if (value == null) {
                super.setValue("");
        }
        else {
            try { //for UTC time - aww 2008/02/07 
                super.setValue(LeapSeconds.trueToString(((Number) value).doubleValue()).substring(0, LENGTH_OF_EPOCHTIME_STRING)); // aww 2008/02/07 
            }
        catch (Exception ex) {
                super.setValue(value);
            }
        }
    }
}

