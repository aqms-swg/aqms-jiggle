package test.org.trinet.jdbc;

import org.junit.jupiter.api.Test;
import org.trinet.jdbc.StringSQL;
import test.common.UnitTests;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StringSQLTests extends UnitTests {

    private final Date testDate = new java.util.Date(1727734293472L);

   @Test
    public void verify_to_date_function_returns_correct_value_with_valid_date() {
       assertEquals("TO_DATE('2024-09-30 22:11:33', 'YYYY-MM-DD HH24:MI:SS')", StringSQL.toDATE(testDate));
    }

    @Test
    public void verify_value_of_function_returns_correct_value_with_valid_null_integer() {
        assertEquals("NULL", StringSQL.valueOf(Integer.MAX_VALUE));
    }

    @Test
    public void verify_value_of_function_returns_correct_value_with_valid_null_long() {
        assertEquals("NULL", StringSQL.valueOf(Long.MAX_VALUE));
    }

    @Test
    public void verify_value_of_function_returns_correct_value_with_valid_long() {
        assertEquals("1", StringSQL.valueOf(1L));
    }

    @Test
    public void verify_value_of_function_returns_correct_value_with_valid_null_float() {
        assertEquals("NULL", StringSQL.valueOf(Float.NaN));
    }

    @Test
    public void verify_value_of_function_returns_correct_value_with_valid_float() {
        assertEquals("1.0", StringSQL.valueOf(1f));
    }

    @Test
    public void verify_value_of_function_returns_correct_value_with_valid_null_time() {
        assertEquals("NULL", StringSQL.valueOf(new Time(0L)));
    }

    @Test
    public void verify_value_of_function_returns_correct_value_with_valid_null_date() {
        assertEquals("NULL", StringSQL.valueOf(new java.sql.Date(0L)));
    }

    @Test
    public void verify_value_of_function_returns_correct_value_with_valid_null_timestamp() {
        assertEquals("NULL", StringSQL.valueOf(new Timestamp(0L)));
    }

    @Test
    public void verify_value_of_function_returns_correct_value_with_valid_null_double() {
        assertEquals("NULL", StringSQL.valueOf(Double.NaN));
    }

    @Test
    public void verify_value_of_function_returns_correct_value_with_valid_double() {
        assertEquals("1.0", StringSQL.valueOf(1d));
    }

    @Test
    public void verify_to_date_function_returns_correct_value_with_valid_null_string() {
        assertEquals("NULL", StringSQL.valueOf("NULL"));
    }

    @Test
    public void verify_value_of_function_returns_correct_value_with_valid_date() {
       assertEquals("'2024-09-30 22:11:33'", StringSQL.valueOf(testDate));
    }
}
