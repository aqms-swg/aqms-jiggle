package org.trinet.jdbc.datasources;

import java.sql.*;
import org.trinet.jdbc.ConnectionFactory;
import org.trinet.jdbc.JDBConn;

/**
 * Used by DataSource class, the default data source for all JASI classes.
 * This data source will be used unless DataSource.DEFAULT_DATASOURCE_STRING.
 * defines a different class.
 * */
public class JDBCDataSource extends AbstractSQLDataSource {

    public JDBCDataSource() { }

    /** Set/reset static connection.
     *  Lookup parameter data about a connection. Does not connect, assumes
     *  the connection is valid. */
    public boolean set(Connection connection) {
        if (connection != getConnection()) {
          source = DBASE;
          conn = connection;
         // parse connection info
          try {
            DatabaseMetaData md = conn.getMetaData() ;
            desc.setURL(md.getURL());
            // NOTE: the info in a Connection object is NOT sufficient to create a
            //desc.setDriver(md.getDriverName()); // this returns bull shit "Oracle JDBC driver"
            desc.setUserName(md.getUserName());
          } catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
            return false;
          }
        }
        return true;  // OK or no change
    }

    /** Set/reset the static connection.  Closes any existing instance connection.
     * Uses the input description metadata to create a new connection. */
    public void set(String url, String driver, String username, String passwd) {
        source = DBASE;
        desc.setURL(url);
        desc.setDriver(driver);
        desc.setUserName(username);
        desc.setPassword(passwd);
        //System.out.println("DEBUG JDBCDataSource set(url, driver, user, pass) getNewConnect() ...");
        if (! isClosed() ) close(); // close existing static connection - aww 01/22/2007
        conn = getNewConnect();
    }

    /** Get a new connection to the default data source. This is useful when
     *  you need multiple, simultaneous dbase connections.
     *  Does not close or set the current static connection.
     */
     public Connection getNewConnect() {
         //if (onServer) return createInternalDbServerConnection(); //

         Connection tempConn = JDBConn.createConnection(
                    desc.getURL(),
                    desc.getDriver(),
                    desc.getUserName(),
                    desc.getPassword()
                );

         // Database specific object to separate databased dependent code
         this.jiggleConnection = new ConnectionFactory().getJiggleConnection(tempConn);
         return tempConn;
    }

    /** Get a server connection on the host machine. This is for server-side stored objects.
     *  Does not set the static connection.
     */
    public Connection createInternalDbServerConnection() {
        return JDBConn.createInternalDbServerConnection();
    }

    /** Returns the status of last connection created. */
    public String getConnectionStatus() {
       return JDBConn.getStatus();
    }

    public boolean onServer() {
        return JDBConn.onServer();
    }

    /** Return true if you cannot write results back to the data source */
    public boolean isReadOnly() {
        try {
         if (conn == null) return true;
         return conn.isReadOnly();
        } catch (SQLException ex)
        {
            System.err.println(ex);
            ex.printStackTrace();
        }
        // default (?)
        return true;
    }

    /**
     * Actually commit any transactions on this connect to the dbase.
     * If this is not called changes will not take effect.  */
    public void commit(Connection connection) {
        try {
            connection.commit();
        } catch (SQLException ex)
        {
            System.err.println(ex);
            ex.printStackTrace();
        }
    }

    /**
     * Rollback to the dbase. If this is not called changes will not take
     * effect.  */
    public void rollback() {
        try {
            conn.rollback();
        } catch (SQLException ex)
        {
            System.err.println(ex);
            ex.printStackTrace();
        }
    }

    /** Closes the static db connection if open, creates a new static db connection object. */
    public boolean connect() {
        disconnect();
        conn = getNewConnect();
        return (conn != null);
    }
    public boolean disconnect() {
        if (isClosed()) return true;
        close();
        return isClosed();
    }
    /** Close the connection */
    public void close() {
        try {
            conn.close();
        }
        catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }
    }
    public boolean isClosed() {
        boolean retVal = true;
        try {
            retVal =  (conn == null || conn.isClosed());
        }
        catch (SQLException ex) {
            System.err.println(ex);
            //ex.printStackTrace();
        }
        return retVal;
    }

    /** Set Login time to wait for connection login. */
    public void setServerTimeoutMillis(int millis) {
            DriverManager.setLoginTimeout(millis/1000);
    }

    /** Get Login time to wait for connection login. */
    public int getServerTimeoutMillis() {
        return DriverManager.getLoginTimeout()*1000;
    }

}  // end of JDBCDataSource

