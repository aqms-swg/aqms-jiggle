package org.trinet.apps;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import org.trinet.jiggle.*;
import org.trinet.jasi.*;


/**
 * GUI for interactive viewing of waveforms and parameters for a single event.
 * @author Doug Given
 */

public class WFviewer {

  protected static boolean debug = true;
  //protected static boolean useWaveserver = false;

  public WFviewer() { }

  public static void main(String args[]) {
    // event 13307456 is a good test because it has no WF in 1st panel
    // 3113276 is an event from 1993
    // event 13692644 has PASA with Steim2 compression

    long wfid = 0l;

    if (args.length <= 0) { // no args
      System.out.println("Usage: WFviewer <wfid> (optional) [host] [dbasename] [username] [password] [subprotocol] [port] [driver]");
    }

    if (args.length > 0) {
      wfid = Long.parseLong(args[0]);
    }

    System.out.println("Making connection...");
    if (args.length > 2) {
      // Make connection to test database
      TestDataSource.create(args[1], args[2], args[3], args[4], args[5], args[6], args[7]);      // make connection
    } else {
      TestDataSource.create("serverk2", "databasek2");  // make connection
    }


    if (debug) System.out.println("Making view for wfid = "+wfid);

    // load the Waveform object
    Wavelet wf = Wavelet.create();
    wf = wf.getByWaveformId(wfid);
    if (wf == null) {
      System.out.println("No waveform in dbase.");
      System.exit(0);
    }
    System.err.println(wf.toString());

    if (wf.loadTimeSeries()) {
      System.out.println("Got waveform: nsegs =  "+ wf.getSegmentList().size() );
      //System.out.println ("Expected samps = "+ wfx.sampleCount.toString() );
      System.out.println("Actual samps   = "+ wf.samplesInMemory() );
      System.out.println("max = "+ wf.getMaxAmp());
      System.out.println("min = "+ wf.getMinAmp());
      System.out.println("bias= "+ wf.getBias());

    } else {
      System.out.println("Waveform retreival failed.");
    }

    // Make the MasterView
    MasterView mv = new MasterView();
    mv.setAlignmentMode(MasterView.AlignOnTime);
    mv.addWFView(new WFView(wf));

// Make graphics components

    if (debug) System.out.println("Creating GUI...");

    int height = 900;
    int width  = 640;

    //VirtScroller wfScroller = new VirtScroller(mv, channelsPerPage, true);
    final WFScroller wfScroller = new WFScroller(mv, true) ;    // make scroller

// test this
    //wfScroller.getWFGroupPanel().setShowSegments(true);

    // make an empty Zoomable panel
 //   ZoomPanel zpanel = new ZoomPanel(mv);
    PickingPanel zpanel = new PickingPanel(mv);

    // enable filtering, default will be Butterworth. See: ZoomPanel
    // Use zpanel.zwfp.setFilter(FilterIF) to change the filter
    zpanel.setFilterEnabled(true);

    // Retain previously selected WFPanel if there is one,
    // if none default to the first WFPanel in the list
    WFView wfvSel = mv.masterWFViewModel.get();
    // none selected, use the 1st one in the scroller
    if (wfvSel == null && mv.getWFViewCount() > 0) {
      wfvSel = (WFView) mv.wfvList.get(0);

      //////
     /// wfvSel.wf = null;     // crash
      //((WFView) mv.wfvList.get(3)).wf = null;  // no crash
      //////
    }

    // Must reset selected WFPanel because PickingPanel and WFScroller are
    // new and must be notified (via listeners) of the selected WFPanel.  It might
    // be null if no data is loaded.
    if (wfvSel != null ) {
      ((ActiveWFPanel)wfScroller.groupPanel.getWFPanel(wfvSel)).setSelected(true);
      mv.masterWFViewModel.set(wfvSel);
      mv.masterWFWindowModel.setFullView(wfvSel.getWaveform());
    }

    zpanel.setMinimumSize(new Dimension(300, 100) );
    wfScroller.setMinimumSize(new Dimension(300, 100) );

    ///////////    TEST //////////////////
    wfScroller.setSecondsInViewport(60.0);

    // make a split pane with the zpanel and wfScroller
    JSplitPane split =
        new JSplitPane(JSplitPane.VERTICAL_SPLIT,
        false,       // don't repaint until resizing is done
        zpanel,      // top component
        wfScroller); // bottom component

/*  Couldn't get this to work!
    RepaintManager repaintManager = RepaintManager.currentManager(split);
    repaintManager.setDoubleBufferingEnabled(false);
    //split.setDebugGraphicsOptions(DebugGraphics.LOG_OPTION );
    split.setDebugGraphicsOptions(DebugGraphics.FLASH_OPTION );
    split.setDebugGraphicsOptions(DebugGraphics.BUFFERED_OPTION );
    //        DebugGraphics.setFlashTime(5);
*/
        /*
            debugOptions - determines how the component should display the information; one of the following options:

        * DebugGraphics.LOG_OPTION - causes a text message to be printed.
        * DebugGraphics.FLASH_OPTION - causes the drawing to flash several times.
        * DebugGraphics.BUFFERED_OPTION - creates an ExternalWindow that displays the operations performed on the View's offscreen buffer.
        * DebugGraphics.NONE_OPTION disables debugging.
  * A value of 0 causes no changes to the debugging options.

debugOptions is bitwise OR'd into the current value
        */

// make a main frame
    //        JFrame frame = new JFrame("SnapShot of "+evid);
    JFrame frame = new JFrame("Waveform number "+wfid);

//    String fileName = "JiggleLogo.gif";
//    IconImage.setDebug(true);
//    Image image = IconImage.getImage(fileName);
//    frame.setIconImage(image);

    frame.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {System.exit(0);}
    });
    frame.getContentPane().add(split, BorderLayout.CENTER);    // add splitPane to frame

// Add the channelname finder
    JPanel finderPanel = new JPanel();
    finderPanel.add(new JLabel("Find: "));
    finderPanel.add(new ChannelFinderTextField(mv));
    frame.getContentPane().add(finderPanel, BorderLayout.SOUTH);

//        SelectablePhaseList list = new SelectablePhaseList(mv);
//        frame.getContentPane().add(list, BorderLayout.SOUTH);

// ///////////////////////////////

    frame.pack();
    frame.setVisible(true);
    frame.setSize(width, height); // must be done AFTER setVisible

    // put divider at 25/75% position
    split.setOneTouchExpandable(true);
    split.setDividerLocation(0.25);

    //debug
    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++");
    System.out.println("WFView count        = "+ mv.getWFViewCount());
    System.out.println("masterWFPanelModel  = "+ mv.masterWFViewModel.countListeners());
    System.out.println("masterWFWindowModel = "+ mv.masterWFWindowModel.countListeners());
    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++");

  }

} // EventViewer
