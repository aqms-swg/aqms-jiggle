package org.trinet.jdbc.table;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import java.sql.Connection;

/** Constructor uses static data members defined by the TableRowArrival interface to initialize the base class with 
* the parameters necessary to describe the schema table named by the interface String parameter DB_TABLE_NAME. 
* The class implements several convenience methods to provides class specific static arguments to the argument
* list of the DataTableRow base class methods. Base class methods are used to set or modify the values and states
* of the contained DataObjects. Because the base class uses a JDBC connection class to access the database containing
* the table described by this object, a connection object must be instantiated before using any of the database enabled
* methods of this class.
* Object states refering to data update, nullness, and mutability are inhereited from the DataTableRow base class
* implementation of the DataState interface. These state conditions are used to control the methods access to the
* object and its contained DataObjects, which also implement the DataState interface.
* The default constructor sets the default states to: setUpdate(false), setNull(true), setMutable(true), and
* setProcessing(NONE).
*/

public class Arrival extends StnChlTableRow implements TableRowArrival {
    public Arrival () {
	super(DB_TABLE_NAME, SEQUENCE_NAME, MAX_FIELDS, KEY_COLUMNS, FIELD_NAMES, FIELD_NULLS, FIELD_CLASS_IDS);
	initialize();
    }
    
/** Copy constructor invokes the default constructor, then clones all the DataObject classes of the input argument.
* The newly instantiated object state values are set to those of the input object.
*/ 
    public Arrival(Arrival object) {
	this();
	for (int index = 0; index < MAX_FIELDS; index++) {
	    fields.set(index, ((DataObject) object.fields.get(index)).clone());
	}
	this.valueUpdate = object.valueUpdate;
	this.valueNull = object.valueNull;
	this.valueMutable = object.valueMutable;
    }

/** Constructor invokes default constructor, then sets the default connection object to the handle of input Connection argument.
* The newly instantiated object state values are those of the default constructor.
*/
    public Arrival(Connection conn) {
	this();
        setConnection(conn);
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Arrival(long arid) {
	this();
	fields.set(ARID, new DataLong(arid));
	valueUpdate = true;
	valueNull = false;
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Arrival(long arid, double datetime, DataStnChl sc) {
	this(arid);
	setDataStnChl(sc);
	fields.set(DATETIME, new DataDouble(datetime));
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Arrival(long arid, double datetime, DataStnChl sc, String iphase, String qual, String fm) {
	this(arid, datetime, sc);
	fields.set(IPHASE, new DataString(iphase));
	fields.set(QUAL, new DataString(qual));
	fields.set(FM, new DataString(fm));
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Arrival(long arid, double datetime, DataStnChl sc, String iphase, String qual, String fm, double quality) {
	this(arid, datetime, sc, iphase, qual, fm);
	fields.set(QUALITY, new DataDouble(quality));
    }
    
/** Initialize data string members used in base class method argument lists
*/
    private void initialize () {
	keyColumn = FIELD_NAMES[KEY_COLUMNS[0]];
	assocTableOrigin = "ASSOCARO";
	assocTableNetmag = null;
    }

}
