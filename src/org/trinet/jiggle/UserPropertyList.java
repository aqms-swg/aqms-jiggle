package org.trinet.jiggle;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import java.util.Properties;

public class UserPropertyList extends JiggleProperties {
    private static final long serialVersionUID = 1L;
    /** User properties key */
    public static final String USER_PROPS_KEY = "userProps";

    /**
     * Create the Jiggle properties.
     * 
     * @param fileName the user properties file name or null for the default.
     * @return the Jiggle properties.
     */
    public static JiggleProperties createJiggleProperties(String fileName) {
        if (fileName == null) {
            fileName = System.getProperty(USER_PROPS_KEY);
        }
        JiggleProperties jp;
        if (fileName != null) {
            System.out.printf("Jiggle property %s=%s\n", USER_PROPS_KEY, fileName);
            jp = new UserPropertyList(fileName);
        } else {
            jp = new JiggleProperties();
        }
        return jp;
    }

    private static void setDefaultUserProperties(Properties props) {
        // default to save properties on exit
        setSavePropsOnExit(props, Boolean.TRUE);
    }

    private static void setSavePropsOnExit(Properties props, Boolean b) {
        props.setProperty("savePropsOnExit", b.toString());
    }

    private volatile boolean storeUserProperties;
    private Properties userProperties = new Properties();
    private final String userPropertiesFileName;

    /**
     * Create the user properties list.
     * 
     * @param fileName the properties file name.
     */
    private UserPropertyList(String fileName) {
        userPropertiesFileName = fileName;
        setDefaultUserProperties(userProperties);
        readPropertiesFile(fileName, userProperties, true);
    }

    @Override
    public synchronized void clear() {
        userProperties.clear();
        super.clear();
        setDefaultUserProperties(userProperties);
    }

    /**
     * Clear the user properties.
     */
    public synchronized void clearUserProperties() {
        userProperties.clear();
        setDefaultUserProperties(userProperties);
    }

    @Override
    public synchronized Object clone() {
        UserPropertyList c = (UserPropertyList) super.clone();
        c.userProperties = (Properties) userProperties.clone();
        return c;
    }

    @Override
    public Object get(Object key) {
        Object value = userProperties.get(key);
        if (value != null) {
            return value;
        }
        return super.get(key);
    }

    @Override
    public String getProperty(String key) {
        String value = userProperties.getProperty(key);
        if (value != null) {
            return value;
        }
        return super.getProperty(key);
    }

    /**
     * Determines if storing to user properties.
     * 
     * @return true if storing to user properties, false otherwise.
     */
    public boolean isStoreUserProperties() {
        return storeUserProperties;
    }

    @Override
    public synchronized Object put(Object key, Object value) {
        if (isStoreUserProperties()) {
            final Object userValue = userProperties.get(key);
            // if value is the same as the user value
            if (value.equals(userValue)) {
                return userValue; // no change to user value
            }
            Object oldValue = super.get(key);
            // if value is the same as the old value
            if (value.equals(oldValue)) {
                // if user value did exist
                if (userValue != null) {
                    // old value was previous user value
                    oldValue = userValue;
                    userProperties.remove(key); // remove from user properties
                }
                return oldValue; // done, value already exists
            }
            // save new value
            return userProperties.put(key, value);
        } else {
            return super.put(key, value);
        }
    }

    @Override
    public synchronized void putAll(Map<?, ?> t) {
        if (isStoreUserProperties()) {
            for (java.util.Map.Entry<?, ?> e : t.entrySet()) {
                put(e.getKey(), e.getValue());
            }
        } else {
            super.putAll(t);
        }
    }

    @Override
    public synchronized Object putIfAbsent(Object key, Object value) {
        Object oldValue = get(key);
        if (oldValue != null) {
            return oldValue;
        }
        return put(key, value);
    }

    @Override
    public synchronized Object remove(Object key) {
        if (isStoreUserProperties()) {
            userProperties.remove(key);
        }
        return super.remove(key);
    }

    @Override
    public synchronized Object replace(Object key, Object value) {
        if (isStoreUserProperties()) {
            Object oldValue = get(key);
            // if old value exists and value is not the same as the old value
            if (oldValue != null && !value.equals(oldValue)) {
                return oldValue; // done, value already exists
            }
            put(key, value);
            return oldValue;
        }
        return super.replace(key, value);
    }

    @Override
    public synchronized boolean replace(Object key, Object oldValue, Object newValue) {
        if (isStoreUserProperties()) {
            // if new value is not the same as the old value
            if (!newValue.equals(oldValue)) {
                put(key, newValue);
                return true;
            }
            return false;
        }
        return super.replace(key, oldValue, newValue);
    }

    @Override
    protected String getDefaultSavePropertiesFilename() {
        return userPropertiesFileName;
    }

    @Override
    public void store(OutputStream out, String comments) throws IOException {
        if (isStoreUserProperties()) {
            userProperties.store(out, comments);
        } else {
            super.store(out, comments);
        }
    }

    /**
     * Store to user properties.
     */
    public void storeUserProperties() {
        synchProperties();
        final File file = new File(getDefaultSavePropertiesFilename());
        storeUserProperties = true;
        if (!file.exists()) { // if file does not exist
            if (!saveProperties()) { // if could not save properties
                // default to no auto save
                setSavePropsOnExit(userProperties, Boolean.FALSE);
            }
        } else if (!file.canRead()) { // cannot read properties
            // default to no auto save
            setSavePropsOnExit(userProperties, Boolean.FALSE);
        }
    }
}
