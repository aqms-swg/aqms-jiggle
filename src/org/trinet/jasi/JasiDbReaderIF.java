package org.trinet.jasi;
import java.sql.*;
import java.util.*;
import org.trinet.jdbc.*;
public interface JasiDbReaderIF extends JasiDataReaderIF {
    public JasiObject parseResultSetDb(ResultSetDb rsdb);
    public Collection getBySQL(String sql);
    public Collection getBySQL(Connection conn, String sql);
}
