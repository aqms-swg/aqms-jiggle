package org.trinet.jasi;
public interface ChannelIdentifiable {
    public ChannelIdIF getChannelId();
    public void setChannelId(ChannelIdIF id);
    public boolean equalsChannelId(ChannelIdIF id); // net,sta,seedchan, (may include location)
    public boolean sameNetworkAs(ChannelIdIF id);   // net,sta
    public boolean sameStationAs(ChannelIdIF id);   // net,sta
    public boolean sameSeedChanAs(ChannelIdIF id);  // net,sta,seedchan
    public boolean sameTriaxialAs(ChannelIdIF id);  // net,sta,seedchan.substring(0,2)
    public String toDelimitedSeedNameString(String delimiter); // as in ChannelIdIF  - aww
    public String toDelimitedNameString(String delimiter); // as in ChannelIdIF  - aww
}
