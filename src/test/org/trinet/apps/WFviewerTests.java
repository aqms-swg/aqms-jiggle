package test.org.trinet.apps;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.testcontainers.images.builder.Transferable;
import org.testcontainers.utility.MountableFile;
import org.trinet.apps.WFviewer;
import test.common.DatabaseTests;
import test.common.UnitTests;
import test.testqueries.org.trinet.apps.WFviewerTestsQueries;

import java.nio.file.FileSystems;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class WFviewerTests extends DatabaseTests {

    public static final int WFviewerEventId = 60253301;

    @BeforeAll()
    public static void addDataToDatabase() throws Exception {
        WFviewerTestsQueries testQueries = new WFviewerTestsQueries(postgreSQLContainer.getJdbcUrl());

        int rows = testQueries.populateWaveFormDataToDatabase();
        assertTrue(rows > 0);
        rows = testQueries.populateAssocWaEDataToDatabase();
        assertTrue(rows > 0);
        rows = testQueries.populateEventDataToDatabase();
        assertTrue(rows > 0);
        rows = testQueries.populateFilenameDataToDatabase();

        // Add file to container
        postgreSQLContainer.execInContainer("mkdir", getWavearchivePathInContainer() + "/" + WFviewerEventId);
        postgreSQLContainer.copyFileToContainer(MountableFile.forHostPath(getTestDataFilePath() + "wavearchive"
                + FileSystems.getDefault().getSeparator() + WFviewerEventId + FileSystems.getDefault().getSeparator() + "BK.THIS.BHE.00"),
                getWavearchivePathInContainer() + "/" + WFviewerEventId + "/BK.THIS.BHE.00");
    }

    //TODO Will address in issue 285

//    @Test
//    public void verify_main_function_works() {
//        try {
//            WFviewer.main(new String[]{"1213985321", postgreSQLContainer.getHost(), getDbName(),
//                    getDbUsername(), getDbPassword(), getSubProtocol(), getPortNumber(), getDatabaseDriver()});
//        } catch (Exception e) {
//
//        }
//    }
}
