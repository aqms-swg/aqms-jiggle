package org.trinet.util.graphics.table;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.util.*;
import java.text.*;

import org.trinet.jdbc.NullValueDb;

public class TextCellRenderer extends DefaultTableCellRenderer {
  protected int fontSize=12;
  //protected Font font;

  public TextCellRenderer() { 
    this(-1, null);
  }
  public TextCellRenderer(int fontSize) { 
    this(fontSize, null);
  }
  public TextCellRenderer(Font font) { 
     this(-1, font);
  }
  protected TextCellRenderer(int fontSize, Font font) { 
    super();
    if (fontSize > 0) this.fontSize = fontSize;
    if (font == null) setFont(new Font("Monospaced", Font.PLAIN, this.fontSize));
    else {
      if (this.fontSize != font.getSize())
          setFont(font.deriveFont((float)this.fontSize));
      else setFont(font);
    }
    setHorizontalAlignment(JLabel.LEFT);
  }
  public int getFontSize() {
    return getFont().getSize();
  }
  public void setFontSize(int fontSize) {
    if (fontSize > 0 && this.fontSize != fontSize) {
      this.fontSize = fontSize;
      setFont(getFont().deriveFont((float) this.fontSize));
    }
  }
  protected void setValue(Object value) {
      if (NullValueDb.isNull(value)) super.setValue("  ");
      else super.setValue(value);
  }

}
