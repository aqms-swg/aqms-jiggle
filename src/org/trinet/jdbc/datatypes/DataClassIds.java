package org.trinet.jdbc.datatypes;

/** Constant integer indexes used to refer to offset in DataClasses interface DATAES array
* as implemented by DataTableRow class and its extensions.
* Class objects for these types are stored at offsets defined by the constants in this interface.
* Classes extending DataTableRow map DataObject classes to the respective column data types.
* @see DataClasses
* @see DataString
* @see DataLong
* @see DataDouble
* @see DataInteger
* @see DataFloat
* @see DataDate
* @see DataTimestamp
*/
public interface DataClassIds {
    public static final int DATASTRING = 0;
/** Schema column type CHAR and VARCHAR2(x) are mapped to the DataString.class.*/
    public static final int DATALONG = 1;
/** Schema column type NUMBER(x) is mapped to the DataLong.class. */
    public static final int DATADOUBLE = 2;
/** Schema column type NUMBER(x,y) where y is non-zero is mapped to the DataDouble.class. */
    public static final int DATAINT = 3;
    public static final int DATAFLOAT = 4;
    public static final int DATADATE = 5;
/** Schema column type DATE is mapped to the DataTimestamp.class. */
    public static final int DATATIMESTAMP = 6;
}
