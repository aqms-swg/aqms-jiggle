package test.org.trinet.apps;

import org.junit.jupiter.api.Test;
import org.trinet.apps.CheckSum;
import test.common.UnitTests;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CheckSumTests extends UnitTests {

    @Test
    public void verify_check_sum_test_case_1_works() {
        String testString = "E 09082344CI21999040217051050339860-1169945017316000014001800120009004332C0002hP";
        CheckSum.main(new String[]{testString});
        assertEquals(testString + "d", CheckSum.getCheckSum());
    }

    @Test
    public void verify_check_sum_test_case_2_works() {
        String testString = "E 09082344CI21999040217051050339860-1169945017316000014001800120009004332C0002h";
        CheckSum.main(new String[]{testString});
        assertEquals(testString + "P", CheckSum.getCheckSum());
    }

    @Test
    public void verify_check_sum_test_case_3_works() {
        String testString = "E meav    US3199904021838195-201884 1681247 33054 19 192283 062 387  00  B 8   ";
        CheckSum.main(new String[]{testString});
        assertEquals(testString + "v", CheckSum.getCheckSum());
    }

    @Test
    public void verify_check_sum_test_case_4_works() {
        String testString = "E 09872117CIL2002122115593100360201-1178858006211***015001000060006000649L0102h";
        CheckSum.main(new String[]{testString});
        assertEquals(testString + "~", CheckSum.getCheckSum());
    }

    @Test
    public void verify_check_sum_test_case_5_works() {
        String testString = "E 09872021CIL2002122108522400348103-1162616006310***016007000110007000854L0101h";
        CheckSum.main(new String[]{testString});
        assertEquals(testString + "^", CheckSum.getCheckSum());
    }

    @Test
    public void verify_check_sum_test_case_6_works() {
        String testString = "E 09872985CIL2002122313233600339693-1171616016918***035009000210003001113L1602h";
        CheckSum.main(new String[]{testString});
        assertEquals(testString + "E", CheckSum.getCheckSum());
    }
}
