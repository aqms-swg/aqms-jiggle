package org.trinet.jasi.TN;

import java.sql.*;
import java.util.*;

import org.trinet.jasi.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

/**
* Abstract type for generic channel corrections originating from StaCorrections database table.
* need to implement a subclass to define the specific correction type.
*/
public class ChannelCorrTN extends ChannelCorr implements DbReadableJasiChannelObjectIF {

    protected boolean fromDbase = false; 

    protected final static String TN_TABLE_NAME = StaCorrections.DB_TABLE_NAME;

    protected final static String SQL_SELECT_PREFIX =
      "SELECT " +StaCorrections.QUALIFIED_COLUMN_NAMES+ " FROM " +TN_TABLE_NAME;

    static protected JasiChannelDbReader jasiDataReader =  new JasiChannelDbReader();
    { 
      jasiDataReader.setFactoryClassName(getClass().getName());
      jasiDataReader.setDebug(JasiDatabasePropertyList.debugSQL);
    }

    public ChannelCorrTN() {}

    public ChannelCorrTN(ChannelIdIF id) {
        super(id);
    }

    public ChannelCorrTN(ChannelIdIF id, DateRange dateRange) {
        super(id, dateRange);
    }
    public ChannelCorrTN(ChannelIdIF id, DateRange dateRange, double value, String corrType) {
        this(id, dateRange, value, corrType, null, null);
    }
    public ChannelCorrTN(ChannelIdIF id, DateRange dateRange, double value, String corrType, String corrFlag) {
        this(id, dateRange, value, corrType, corrFlag, null);
    }
    public ChannelCorrTN(ChannelIdIF id, DateRange dateRange, double value, String corrType,
                    String corrFlag, String authority) {
        super(id, dateRange, value, corrType, corrFlag, authority);
    }

    public String toChannelSQLSelectPrefix() {
        return SQL_SELECT_PREFIX +
          " WHERE " + TN_TABLE_NAME + ".CORR_TYPE = " + corrType.toStringSQL() ;
    }
    public String toChannelSQLSelectPrefix(java.util.Date date) {
        return SQL_SELECT_PREFIX +
          " WHERE " + TN_TABLE_NAME + ".CORR_TYPE = " + corrType.toStringSQL() +
          " AND " + TN_TABLE_NAME + ".OFFDATE > SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)";
          //" AND " + DataTableRow.toDateConstraintSQLWhereClause(TN_TABLE_NAME, date) ; // always want currently active
    }
    public String toChannelSQLSelectLikePrefix(String typeExpr, java.util.Date date) {
        return SQL_SELECT_PREFIX +
          " WHERE " + TN_TABLE_NAME + ".CORR_TYPE LIKE '"+typeExpr+"'" +
          " AND " + TN_TABLE_NAME + ".OFFDATE > SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)";
          //" AND " + DataTableRow.toDateConstraintSQLWhereClause(TN_TABLE_NAME, date) ; // always want currently active
    }

    /** Return count of all channels in data source at specified time. */
    public int getCount(java.util.Date date) {
      return jasiDataReader.getCountBySQL(DataSource.getConnection(), toChannelSQLSelectPrefix(date));
    }

    /**
     * Returns an instance valid for the specified input channel id and date.
     * If input date is null, returns most recently available data for the input channel identifier.
     * Returns null if no data are found satisfying input criteria.
    */
    public ChannelDataIF getByChannelId(ChannelIdIF id, java.util.Date date) {
        return (ChannelDataIF) jasiDataReader.getByChannelId(TN_TABLE_NAME, id, date);
    }
    public Collection getByChannelId(ChannelIdIF id, DateRange dr) {
        return jasiDataReader.getByChannelId(TN_TABLE_NAME, id, dr);
    }

    public Collection getAllTypesByChannelId(ChannelIdIF id, java.util.Date date) {
      return getLikeTypesByChannelId(id, date, "%");
    }
    public Collection getLikeTypesByChannelId(ChannelIdIF id, java.util.Date date, String typeExpr) {
        return jasiDataReader.getBySQL(DataSource.getConnection(), toChannelSQLSelectLikePrefix(typeExpr, date));
    }

    /**
    * Returns an instance whose data members values are parsed from the input ResultSetDb object.
    */
    private static ChannelCorrTN parseResultSet(ResultSetDb rsdb) {
        int offset = 0;
        StaCorrections newRow = (StaCorrections) new StaCorrections().parseOneRow(rsdb, offset); // from package org.trinet.jdbc.table
        if (newRow == null) {
           System.err.println("ChannelCorrTN parseResultSet cannot parse row from resultSet");
           return null;
        }
        // if (verbose) System.out.println("ChannelCorrTN parseResultSet: " + newRow.toString());

        if (DataSource.isWriteBackEnabled()) {
            newRow.setMutable(true); // allow changes with aliases to fields of this DataTableRow.
        }
        ChannelCorrTN corr = new ChannelCorrTN();
        corr.parseStaCorrectionsRow(newRow);
        offset += StaCorrections.MAX_FIELDS;
        corr.fromDbase = true; // flag as db acquired
        return corr;
    }

    public boolean isFromDataSource() { return fromDbase; }

    private void parseStaCorrectionsRow(StaCorrections row) {
        if (channelId == null) channelId = new ChannelName();
        channelId = DataTableRowUtil.parseChannelSrcFromRow(row, channelId);

        DataDate on = (DataDate) row.getDataObject(StaCorrections.ONDATE);
        java.util.Date onDate =  ( on.isNull()) ? null : on.dateValue();
        DataDate off = (DataDate) row.getDataObject(StaCorrections.OFFDATE);
        java.util.Date offDate = (off.isNull()) ? null : off.dateValue();
        if (dateRange == null) {
          dateRange = new DateRange(onDate, offDate);
        }
        else {
          dateRange.setMin(onDate);
          dateRange.setMax(offDate);
        }

        DataObject tmp = row.getDataObject(StaCorrections.CORR);
        if (! tmp.isNull()) corr.setValue(tmp);

        tmp  = row.getDataObject(StaCorrections.CORR_TYPE);
        if (! tmp.isNull()) corrType.setValue(tmp.toString());

        tmp = row.getDataObject(StaCorrections.CORR_FLAG);
        if (! tmp.isNull()) corrFlag.setValue(tmp.toString());

        tmp = row.getDataObject(StaCorrections.AUTH);
        if (! tmp.isNull()) authority.setValue(tmp.toString());
    }

    private StaCorrections toStaCorrectionsRow() {
        StaCorrections newRow = new StaCorrections();
        newRow.setUpdate(true); // set flag to enable processing
        // not null, or error results on table insert
        DataTableRowUtil.setRowChannelId(newRow,channelId) ;
        if (! corr.isNull())
                newRow.setValue(StaCorrections.CORR, corr);

        if (! corrType.equalsValue(UNKNOWN_STRING_TYPE))
                newRow.setValue(StaCorrections.CORR_TYPE, corrType);

        java.util.Date minDate = dateRange.getMinDate();
        if (minDate != null) 
                newRow.setValue(StaCorrections.ONDATE, minDate);

        // nullable table columns fields
        if (! corrFlag.equalsValue(UNKNOWN_STRING_TYPE))
                newRow.setValue(StaCorrections.CORR_FLAG, corrFlag);

        if (! authority.equalsValue(UNKNOWN_STRING_TYPE))
                newRow.setValue(StaCorrections.CORR_FLAG, corrFlag);

        if (dateRange.hasMaxLimit())
                newRow.setValue(StaCorrections.OFFDATE, dateRange.getMaxDate());

        return newRow;
    }

    protected boolean dbaseInsert () {                      // aka JasiReading
        boolean status = true;
        StaCorrections row = toStaCorrectionsRow();
        //System.out.println("ChannelCorrTN dbaseInsert row.toString(): " + row.toString());
        if (fromDbase) {
            row.setProcessing(DataTableRowStates.UPDATE);
            status = (row.updateRow(DataSource.getConnection()) > 0);
        }
        else {
            row.setProcessing(DataTableRowStates.INSERT);
            status = (row.insertRow(DataSource.getConnection()) > 0);
        }
        if (status) {
            fromDbase = true; // now its "from" the dbase
        }
        return status;
    }

    /**
     * Returns a Collection of objects from the default DataSource regardless of date.
     * There may be multiple entries for each channel that represent changes through time.
     * Uses the default DataSource Connection.
    */
    public  Collection loadAll() {
        return loadAll((java.util.Date) null);
    }

    /*
     * Return Collection of objects that were valid on the input date.
     * Data is retrieved from the default DataSource Connection.
    */
    public Collection loadAll(java.util.Date date) {
        return loadAll(DataSource.getConnection(), date);
    }

    public Collection loadAll(java.util.Date date, String[] prefSeedchan) {
        return loadAll(date, null, prefSeedchan, null);
    }

    public Collection loadAll(java.util.Date date, String [] prefNet,
                    String [] prefSeedchan, String [] prefChannel) {
        return loadAll(date, prefNet, prefSeedchan, prefChannel, null);
    }
    public Collection loadAll(java.util.Date date, String [] prefNet,
                    String [] prefSeedchan, String [] prefChannel,
                    String [] prefLocations) {
        return jasiDataReader.loadAll(TN_TABLE_NAME, DataSource.getConnection(), date,
                        prefNet, prefSeedchan, prefChannel, prefLocations);
    }

    /**
    * Returns a Collection of objects created from data obtained from the specified connection
    * that are valid for the specified input date.
    * Note - individual lookups may be more time efficient, if one shot less then several hundred channels.
    */
    static public Collection loadAll(Connection conn, java.util.Date date) {
        return jasiDataReader.loadAll(conn, date);
    }

    /** Writes the data values of this instance to the default DataSource archive.*/
    public boolean commit() {
        if (!DataSource.isWriteBackEnabled()) return false;
        return dbaseInsert();
    }

    public JasiObject parseData(Object rsdb) {
      return parseResultSetDb((ResultSetDb) rsdb);
    }
    public JasiObject parseResultSetDb(ResultSetDb rsdb) {
      return parseResultSet((ResultSetDb) rsdb);
    }
    /**Access to the JasiDbReader to allow generic SQL queries. */
    public JasiDataReaderIF getDataReader() {
        return jasiDataReader;
    }

    public boolean hasChanged() {
        return corr.isUpdate() || corrFlag.isUpdate() || corrType.isUpdate();
    }

} // end of class ChannelCorrTN

