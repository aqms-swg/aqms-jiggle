package org.trinet.jiggle.database.event;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.trinet.jiggle.common.type.AssocEvent;

public class EventAssocNone implements EventAssoc {
    /**
     * Get the event associated.
     * 
     * @return the event associated.
     */
    public static EventAssoc getEventAssoc() {
        return new EventAssocNone();
    }

    private EventAssocNone() {
    }

    @Override
    public void close() {
    }

    @Override
    public void commit() {
    }

    @Override
    public boolean deleteAssociatedEvent(long eventId) {
        return false;
    }

    @Override
    public Set<Long> getAllEventId(long eventId) {
        return Collections.emptySet();
    }

    @Override
    public List<AssocEvent> getAssociatedEvents(long eventId) {
        return Collections.emptyList();
    }

    @Override
    public AssocEvent getComment(long eventId) {
        return null;
    }

    @Override
    public boolean insertAssociatedEvent(AssocEvent event) {
        return false;
    }

    @Override
    public AssocEvent isExist(long masterEventId, long assocEventId) {
        return null;
    }

    @Override
    public void rollback() {
    }
}
