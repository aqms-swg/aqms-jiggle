package org.trinet.jasi.EW;

import org.trinet.jasi.*;
import java.util.Collection;
import java.util.Vector;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class SolutionLockEW extends SolutionLock {


  EWSolutionLock ewSolutionLock;
  long idSource;
  long tCreation;

  static Vector SolutionLockList = new Vector();

  public SolutionLockEW()
  {
    this.lockingWorks = true;
    this.tCreation = System.currentTimeMillis();
  }

  public boolean isLocked()
  {
    if(this.object instanceof SolutionEW)
    {
      if(this.requestorDateTime > 0.0)  // our lock value
        return(true);
      else
        return(false);
    }
    else
    {
      return(false);
    }
  }

  public boolean lock()
  {
    this.requestorDateTime = System.currentTimeMillis()/1000;
    toEWSolutionLock();
    // boolean bRetCode = (ewSolutionLock.Write()!= null);
    ewSolutionLock.Write();
    EWSolutionLock_2_SolutionLockEW();
    if(ewSolutionLock.iRetCode != 0)
    {
      this.requestorDateTime = 0;
      System.out.println("Error: (" + ewSolutionLock.iRetCode + ") while trying to Lock solution " + this.id);
    }

    if(this.requestorDateTime > 0.0)
    {
      SolutionLockList.add((Object)this);
      return(true);
    }
    else
    {
      return(false);
    }
  }

  protected SolutionLockEW EWSolutionLock_2_SolutionLockEW()
  {
    this.id = ewSolutionLock.idEvent;
    this.username = ewSolutionLock.sSource;
    this.requestorUsername = ewSolutionLock.sSource;
    this.idSource = ewSolutionLock.idSource;
    if(ewSolutionLock.iRetCode == 0)
      this.requestorDateTime = ewSolutionLock.iLockTime;
    return(this);
  }


  protected EWSolutionLock toEWSolutionLock()
  {
    ewSolutionLock = new EWSolutionLock();
    ewSolutionLock.idEvent = this.id;
    ewSolutionLock.sSource = this.requestorUsername;
    ewSolutionLock.iLockTime = (int)this.requestorDateTime;
    ewSolutionLock.sMachineName = this.requestorHost;
    ewSolutionLock.sNote = this.application;
    return(ewSolutionLock);
  }

  public Collection getAllMyLocks()
  {
    return(SolutionLockList);
  }

  // return all locks in DB by current author
  public Collection getAllOfMyLocks()
  {
    toEWSolutionLock();
    Vector vEWSolutionLockList = ewSolutionLock.ReadList();
    if(vEWSolutionLockList == null)
      return(null);
    else
    {
      Vector vSolutionLockEWList = new Vector(vEWSolutionLockList.size());
      for(int i=0; i < vEWSolutionLockList.size(); i++)
      {
        SolutionLockEW lock = new SolutionLockEW();
        lock.ewSolutionLock = (EWSolutionLock)vEWSolutionLockList.get(i);
        lock.EWSolutionLock_2_SolutionLockEW();
        vSolutionLockEWList.add(lock);
      }
      return(vSolutionLockEWList);
    }
  }

  public Collection getLocksByApplication(String application) {
    return(null);
    /**@todo: implement this org.trinet.jasi.SolutionLock abstract method*/
  }
  public boolean checkLockingWorks()
  {
    return(true);
    /**@todo: implement this org.trinet.jasi.SolutionLock abstract method*/
  }
  public Collection getAllLocks()
  {
    toEWSolutionLock();
    return(ewSolutionLock.ReadFullList());
  }

  public boolean unlock()
  {
    toEWSolutionLock();
    ewSolutionLock.Delete();
    EWSolutionLock_2_SolutionLockEW();
    if(this.requestorDateTime == 0.0)
    {
      SolutionLockEW slEW;
      for(int i=0; i < SolutionLockList.size(); i++)
      {
        slEW = (SolutionLockEW)SolutionLockList.get(i);
        if(slEW.id == this.id)
          SolutionLockList.remove(i);
      }
      return(true);
    }
    else
    {
      return(false);
    }
  }

  public boolean unlockAll() {
    toEWSolutionLock();
    return(ewSolutionLock.DeleteAll() != null);
  }

  public Collection getLocksByUser(String usename)
  {
    toEWSolutionLock();
    ewSolutionLock.sSource = usename;
    return(ewSolutionLock.ReadList());
  }

  public Collection getLocksByHost(String host) {
    /**@todo: implement this org.trinet.jasi.SolutionLock abstract method*/
    return(null);
  }

  public boolean lockIsMine()
  {
    if(this.object == null  || this.object instanceof SolutionEW)
    {
      SolutionLockEW slEW;
      for(int i=0; i < SolutionLockList.size(); i++)
      {
        slEW = (SolutionLockEW)SolutionLockList.get(i);
        if(slEW.id == this.id)
          return(true);
      }
    }
    return(false);
  }

  public boolean unlockAllMyLocks()
  {
    Collection list = getAllMyLocks();
    if(list == null)
      return(false);
    else
    {
      boolean bRetCode = true;
      SolutionLockEW[] solList = (SolutionLockEW[])(list.toArray(new SolutionLockEW[0]));
      for(int i=0; i < solList.length; i++)
      {
        bRetCode = bRetCode && solList[i].unlock();
      }
      return(bRetCode);
    }
  }

    public void setSolution(Solution sol)
    {
	this.object = (JasiObject)sol;
        this.id = sol.id.longValue();
    }

    // DK CLEANUP this function is a problem,
    // because we want a link to the object per ResourceLock.object

    public void setId(long id)
    {

	this.id = id;
    }



    /**@todo: implement this org.trinet.jasi.SolutionLock abstract method*/
}
