package org.trinet.jasi.picker; 
import java.util.List;
import javax.swing.JComponent;
import org.trinet.jasi.Channel;
import org.trinet.jasi.Channelable;
import org.trinet.jasi.ChannelableList;
import org.trinet.jasi.PhaseList;
import org.trinet.jasi.Solution;
import org.trinet.jasi.Waveform;
import org.trinet.util.GenericPropertyList;
import org.trinet.util.TimeSpan;

public interface PhasePickerIF {

    public static final int NONE    = 0;;
    public static final int P_ONLY  = 1;
    public static final int S_ONLY  = 2;
    public static final int P_AND_S = 3;
    public static final int DEFAULT = 4;

    public void setProperties(GenericPropertyList gpl);
    public GenericPropertyList getProperties(GenericPropertyList gpl);
    public JComponent getPropertyEditor(GenericPropertyList gpl);

    public String getName();

    public boolean isDebug();
    public void setDebug(boolean tf);
    public boolean isVerbose();
    public void setVerbose(boolean tf);

    public boolean reject(Channelable ch);
    public boolean withinPickerRange(Channel ch, Solution sol, String pickType); 

    public String getCandidateListName();
    public void setCandidateListName(String newName);

    public ChannelableList getCandidateList();
    public void setCandidateList(ChannelableList candidateListIn);

    // Uses Solution depth and preferred magnitude to screen channel for picking
    public PhaseList pick(Waveform wf, int phasesToPickFlag, boolean gainUnitsFilter);
    public PhaseList pick(Waveform wf, Solution sol, int phasesToPickFlag, boolean gainUnitsFilter, boolean distanceFilter);
    public PhaseList pick(Waveform wf, int phasesToPickFlag, PickerParmIF snclMappedParm, TimeSpan scanSpan, TimeSpan pSpan, TimeSpan sSpan);
    public PhaseList pick(ChannelableList wfList, Solution sol, int phasesToPickFlag, boolean gainUnitsFilter, boolean distanceFilter);

    public double getMaxDistance();
    public void setMaxDistance(double km);

    public double getMaxDistance(double magnitude);
    public double sMaxDistScalar();

    public boolean velocityOnly();
    public void setVelocityOnly(boolean tf);

    public boolean pOnH();
    public void setPonH(boolean tf);

    public boolean sOnV();
    public void setSonV(boolean tf);

    public PickerParmIF getDefaultParm();
    public GenericPropertyList getDefaultParmValues(GenericPropertyList gpl);
    public void setDefaultParmValues(GenericPropertyList gpl);
    public void setDefaultParmValues(PickerParmIF parm);

    public boolean loadChannelParms();
    public boolean isAutoLoadChannelParms();
    public void setAutoLoadChannelParms(boolean tf);

    public boolean hasChannelParms();
    public boolean hasChannelParm(Channelable ch);

    public void printChannelParms();
    public void printChannelParm(Channelable ch);
    public void printChannelParm(String sncl);

    public PickerParmIF getChannelParm(Channelable ch);
    public PickerParmIF getChannelParm(String sncl);
    public java.util.List getChannelParms();

    public PickerParmIF addChannelParm(String sncl, PickerParmIF parm);
    public PickerParmIF addChannelParm(Channelable ch, PickerParmIF parm);
    public PickerParmIF addChannelParm(PickerParmIF parm);
    public void addChannelParms(List parmList);

    public void clearChannelParms();
    public PickerParmIF removeChannelParm(String sncl);
    public PickerParmIF removeChannelParm(Channelable ch);
    public PickerParmIF removeChannelParm(PickerParmIF parm);

    public JComponent getChannelParmEditor(Channelable ch);
    public JComponent getChannelParmEditor(String sncl);

    public PickerParmIF parseChannelParm(String parmStr);
    public String toParseableString(PickerParmIF parm);

    //PhasePickerIF clone();

    /*
    public void setAllowedNetTypes(String[] netList);
    public void setAllowedStaTypes(String[] staList);
    public void setAllowedChannelTypes(String[] chanList);

    public void setRejectedNetTypes(String[] netList);
    public void setRejectedStaTypes(String[] staList);
    public void setRejectedChannelTypes(String[] chanList);
    */

}
