package test.org.trinet.jiggle.database.event;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.trinet.jiggle.common.type.AssocEvent;
import org.trinet.jiggle.database.event.EventAssoc;
import org.trinet.jiggle.database.event.EventAssocDb;
import test.common.DatabaseTests;
import test.testqueries.org.trinet.jiggle.database.events.EventAssocDbTestsQueries;

import static org.junit.jupiter.api.Assertions.*;

public class EventAssocDbTests extends DatabaseTests {

    // Delete event ids
    public static final long deleteEventId = 60205886L;
    public static final long deleteEventId2 = 105309321L;

    // Get all event ids
    public static final long getAllEventId = 60205887L;
    public static final long getAllEventId2 = 105309322L;

    // Get all event ids
    public static final long getAssociatedEventId = 60205888L;
    public static final long getAssociatedEventId2 = 105309323L;

    // Get all event ids
    public static final long getCommentEventId = 60205889L;
    public static final long getCommentEventId2 = 105309324L;
    public static final long getCommentCommId = 1073L;

    // Insert Event ids
    public static final long insertEventId = 60205881L;
    public static final long insertEventId2 = 105309325L;

    // Is Exist Event ids
    public static final long isExistEventId = 60205882L;
    public static final long isExistEventId2 = 105309326L;

    private static EventAssoc eventAssocDb;

    @BeforeAll
    public static void initTests() throws Exception {
        eventAssocDb = EventAssocDb.getEventAssoc();
        EventAssocDbTestsQueries eventAssocDbTestsQueries = new EventAssocDbTestsQueries(postgreSQLContainer.getJdbcUrl());

        // Delete test set up
        int rows = eventAssocDbTestsQueries.populateEventTableForDeleteTest();
        assertTrue(rows > 0);
        rows = eventAssocDbTestsQueries.populateEvent2TableForDeleteTest();
        assertTrue(rows > 0);
        rows = eventAssocDbTestsQueries.populateAssocEventTableForDeleteTest();
        assertTrue(rows > 0);

        // Get all test set up
        rows = eventAssocDbTestsQueries.populateEventTableForGetAllTest();
        assertTrue(rows > 0);
        rows = eventAssocDbTestsQueries.populateEvent2TableForGetAllTest();
        assertTrue(rows > 0);
        rows = eventAssocDbTestsQueries.populateAssocEventTableForGetAllTest();
        assertTrue(rows > 0);

        // Get associated test set up
        rows = eventAssocDbTestsQueries.populateEventTableForGetAssociatedTest();
        assertTrue(rows > 0);
        rows = eventAssocDbTestsQueries.populateEvent2TableForGetAssociatedTest();
        assertTrue(rows > 0);
        rows = eventAssocDbTestsQueries.populateAssocEventTableForGetAssociatedTest();
        assertTrue(rows > 0);

        // Get comment test set up
        rows = eventAssocDbTestsQueries.populateEventTableForGetCommentTest();
        assertTrue(rows > 0);
        rows = eventAssocDbTestsQueries.populateEvent2TableForGetCommentTest();
        assertTrue(rows > 0);
        rows = eventAssocDbTestsQueries.populateAssocEventTableForGetCommentTest();
        assertTrue(rows > 0);

        // Insert event set up
        rows = eventAssocDbTestsQueries.populateEventTableForInsertTest();
        assertTrue(rows > 0);
        rows = eventAssocDbTestsQueries.populateEvent2TableForInsertTest();
        assertTrue(rows > 0);

        // Is Exist test set up
        rows = eventAssocDbTestsQueries.populateEventTableForIsExistTest();
        assertTrue(rows > 0);
        rows = eventAssocDbTestsQueries.populateEvent2TableForIsExistTest();
        assertTrue(rows > 0);
        rows = eventAssocDbTestsQueries.populateAssocEventTableForIsExistTest();
        assertTrue(rows > 0);
    }

    @Test
    public void verify_delete_associated_events_works_as_intended() {
        assertTrue(eventAssocDb.deleteAssociatedEvent(deleteEventId));
    }

    @Test
    public void verify_get_all_events_works_as_intended() {
        assertTrue(eventAssocDb.getAllEventId(getAllEventId).contains(getAllEventId));
    }

    @Test
    public void verify_get_associated_events_works_as_intended() {
        assertEquals(getAssociatedEventId, eventAssocDb.getAssociatedEvents(getAssociatedEventId).get(0).getMasterEventId());
    }

    @Test
    public void verify_get_comment_works_as_intended() {
        assertEquals(getCommentCommId, eventAssocDb.getComment(getCommentEventId).getCommentId());
    }

    @Test
    public void verify_insert_associated_event_works_as_intended() {
        AssocEvent test = new AssocEvent();
        test.setMasterEventId(insertEventId);
        test.setAssocEventId(insertEventId2);
        test.setCommentId(getCommentEventId);
        assertTrue(eventAssocDb.insertAssociatedEvent(test));
    }

    @Test
    public void verify_is_exist_works_as_intended() {
        assertNotNull(eventAssocDb.isExist(isExistEventId, isExistEventId2));
    }
}
