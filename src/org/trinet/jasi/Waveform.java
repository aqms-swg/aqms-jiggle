package org.trinet.jasi;

import java.util.*;
import javax.swing.event.ChangeListener;

import org.trinet.util.*;

public interface Waveform extends JasiTimeSeriesIF {

    //public long getWordOrder(); // if wordOrder added to waveform query
    //public long setWordOrder(long wordOrder); // if wordOrder added to waveform query

    public long getFormat();
    public void setFormat(long format);
    public long getEncoding();
    public void setEncoding(long code);

    //public String getDataMode();
    //public void setDataMode(String mode);

    public String getWaveType();
    public void setWaveType(String type);
    public String getWaveArchive();
    public void setWaveArchive(String archive);
    public String getWaveStatus();
    public void setWaveStatus(String status);

    public String getSavingEvent();
    public void setSavingEvent(String evid);

    public boolean setWaveSource(Object source);
    public Object getWaveSource();
    public String getWaveSourceName();
    public int getWaveSourceType();

    public Waveform createWaveform();
    public Waveform copy();
    public void copyHeader(Waveform wf);
    public void copyTimeSeries(Waveform wf);
    public int getWcopy();

    public Collection getByChannel(Channelable ch, long evid);
    public Collection getByChannel(Channelable ch, Solution sol);

    public Collection getBySolution(long id);
    public Collection getBySolution(Solution sol);
    public int getCountBySolution(long evid);
    public int getCountBySolution(Solution sol);

    public boolean loadTimeSeries();
    public boolean loadTimeSeries(double startTime, double endTime);
    public boolean unloadTimeSeries();
  
    public void collapse();
    public boolean hasTimeSeries();
    public WFSegment[] getArray();
    public Collection getSubSegments(double startTime, double endTime);
  
    public int getAmpUnits();
    public String getAmpUnitsTag();
    public void setAmpUnits(int units);
    public void setAmpUnits(String units);
    public double getSampleInterval();
    public double closestSampleInterval(double dtime);
  
    public void scanForBias();
    public void demean();
    public void scaleAmp(float value);

    public Waveform filter(FilterIF filter);
    public String getFilterName();
    public void setFilterName(String str);
    public void setIsFiltered(boolean tf);

    public float scanForNoiseLevel();
    public float scanForNoiseLevel(TimeSpan ts);
    public float scanForNoiseLevel(double startTime, double endTime);
  
    public boolean hasTimeTears();
    public boolean hasTimeTears(TimeSpan ts);
    public float getBias();

    public Amplitude getPeakAmplitude(double startTime, double endTime);
    public Amplitude getPeakAmplitude();
    public Amplitude getPeakAmplitude(TimeSpan timeSpan);
    public Amplitude getPeakAmplitude(TimeSpan timeSpan, int peakType);

    public int samplesInMemory();
    public Sample closestSample(double dtime);
    public Sample getFirstSample();
    public Sample getLastSample();

    public Sample scanForPeak();
    public Sample scanForPeak(TimeSpan timeSpan);
    public Sample scanForPeak(double startTime, double endTime);

    //public Sample scanForPeak2Peak();
    //public Sample scanForPeak2Peak(TimeSpan timeSpan);
    //public Sample scanForPeak2Peak(double startTime, double endTime);

    public void scanForAmps();
    public boolean isClipped();
    public void setClipped(boolean tf);
    public Sample getMaxSample();
    public double getMaxAmp();
    public Sample getMinSample();
    public double getMinAmp();
    public double rangeAmp();
    public Sample getPeakSample();
  
    public TravelTime getTravelTimeModel();
    //public void setTravelTimeModel(TravelTime tt);
    public TimeSpan getEnergyTimeSpan(double originTime, double horizDistance, double depth);
    public TimeSpan getSWaveEnergyTimeSpan(double originTime, double horizDistance, double depth);
    public TimeSpan getPreEnergyTimeSpan(double originTime, double horizDistance, double depth);

    // If read from disk file in lieu of database access then need these:
    public boolean filesAreLocal();
    public String getPathFilename();
    public void setFilePath(String path);
    public String getFilePath();
    public void setFilename(String filename);
    public String getFilename();
    public void setFileOffset(int fileOffset);
    public int getFileOffset();
    public void setFileByteCount(int count);
    public int getFileByteCount();

    public void addChangeListener(ChangeListener l);
    public void removeChangeListener(ChangeListener l);

    public String toBlockString();
    public String getDescription();

}
