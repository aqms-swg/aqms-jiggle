package org.trinet.jasi;

import java.util.*;
import java.io.*;

import org.trinet.util.*;

/**
 * This class discribes a single contiguous waveform. An int array
 * containing the samples is created to the proper length and is always FULL.
 */

public class WFSegment extends TimeSeriesSegment {

    public static final int AVG = 0;
    public static final int RMS = 1;
    public static final int AAA = 2;

    public static int scanNoiseType = AVG;

    /** recording device */
    public String rec = "";

    /** data recording device */
    public String source = "";

    /** gram length in bytes */
//    public int length;
    /** Count of samples the waveform SHOULD have according to the data source.
    * This may not be how many are actually in memory. */
    public int samplesExpected;
//    public int secsExpected;
    public int bytesExpected;

    /** data format (see SCEC-DC manual) */
    public int fmt;

/** Type of SEED encoding (Steim 1, Steim 2, etc. ) See: org.trinet.SeedEncodingFormat()*/
    public int encoding;

    /** bias contained in the minfo file */
    public int readbias;

    public int bytesPerSample;

    /** byte offset of the data in the file */
    public int index;

    /** waveform data file name */
    public String filename = "";

    public int status;

    static final int NULL = (int) Math.pow(2.0, 31.0);

    Sample maxSample;
    Sample minSample;

    float biasVal = NULL;

    /** Rectified background noise level with bias removed. It may be derived
    * from some outside source (say a longterm average from the external datasource.)
    * If calculated by this class it is
    * the median of the peaks between zero-crossings for a rectified time-series
    * with the bias removed. The time window for which it is calculated is set by
    * a call to scanForNoiseLevel(start, end). */
    float noiseLevel = Float.NaN;
    int npeak = 1;

/** Data and clock quality bits from Seed Header flags */

    public boolean clockLocked;
    public boolean ampSaturated;        // clipped?
    public boolean digClipped;
    public boolean spikes;
    public boolean glitches;
    public boolean badTimeTag;

 /* data format codes used by SCEC-DC
       The starred formats are currently supported:

          0:  Unknown, or Not Applicable
         *1:  Standard-order 2-byte integers (i.e. SUNsparc short; big endian)
          2:  Reverse-order 2-byte integers (i.e. DECvax short; little endian)
         *3:  Standard-order 4-byte integers (i.e. SUNsparc integer)
          4:  Reverse-order 4-byte integers (i.e. DECvax integer)
          5:  IEEE 4-byte floating-point (i.e. SUNsparc float)
          6:  DEC 4-byte floating-point (i.e. DECvax float)
          7:  SEED format
         *8:  Steim Compressed
          9:  SAC binary
*/

   /**
    * Null constructor
    */
    public WFSegment() {
    }

    /**
     * Constructor without allocation of 'rawSamples' array. This allows creation of
     * segments without using up lots of memory.
    */
    public WFSegment(Channel ch) {
        setChannelObj(ch);
    }

    /**
     * Constructor allocated space for the time series.
     */
    public WFSegment(Channel ch, int sampleCount) {
        this(ch);
        allocateTimeSeriesArray(sampleCount);
    }

    /**
     * Make segment with this time series.
    */
    public WFSegment(Channel ch, float timeseries[] ) {
        this(ch);
        setTimeSeries(timeseries);
    }

    public WFSegment(String net, String sta, String channel, int sampleCount) {
        this(Channel.create().setChannelName(net, sta, channel, ""));
        allocateTimeSeriesArray(sampleCount);
    }

    /**
     * Copy constructor
    */
    public WFSegment(WFSegment wf) {
        copy(wf);
    }

    /**
     * Make a "deep" copy of a WFSegment header only. Time series and
     * timeseries dependent values (bias, max, min) are not copied.
     */
    public void copyHeader(WFSegment wf) {

        chan = wf.chan;               // note: this copies the reference
        rec = wf.rec ;
        source = wf.source ;

        setStart(wf.getStart()) ;
        setEnd(wf.getEnd()) ;
        setSampleInterval(wf.dt) ;

//        length = wf.length ;
//        sampleCount = wf.sampleCount ;
//        lenSecs = wf.lenSecs ;

        fmt = wf.fmt ;
        readbias = wf.readbias ;

        bytesPerSample = wf.bytesPerSample ;
        index = wf.index ;
        filename = wf.filename ;
        status = wf.status ;

        // Added instance data member attributes below -aww 10/26/2004 
        encoding = wf.encoding;
        samplesExpected = wf.samplesExpected;
        bytesExpected = wf.bytesExpected;
        // end of 10/26/2004 addition -aww

        // Data and clock quality bits from Seed Header flags 
        copyTimeFlags(this, wf);

        setTimeQuality(wf.getTimeQuality());

    }

    private static void copyTimeFlags(WFSegment toWFSeg, WFSegment fromWFSeg) {
        toWFSeg.clockLocked = fromWFSeg.clockLocked;
        toWFSeg.ampSaturated = fromWFSeg.ampSaturated;
        toWFSeg.digClipped = fromWFSeg.digClipped;
        toWFSeg.spikes = fromWFSeg.spikes;
        toWFSeg.glitches = fromWFSeg.glitches;
        toWFSeg.badTimeTag = fromWFSeg.badTimeTag;
    }

    /**
     * Make a "deep" copy of a WFSegment, including the time series.
     */
    public void copy(WFSegment wfs) {

      copyHeader(wfs);

      if (wfs.maxSample != null)  maxSample =  wfs.maxSample.copy();
      if (wfs.minSample != null)  minSample =  wfs.minSample.copy();

      biasVal    = wfs.biasVal ;

      // copy the time series
      ts = wfs.getTimeSeriesCopy() ;

      noiseLevel = wfs.noiseLevel; // added since your copied the ts 10/26/2004 -aww
    }

    /** Return the first Sample in the segment. */
    public Sample getFirstSample() {
        return sampleAtIndex(0);
    }

    /** Return the last sample in the segment */
    public Sample getLastSample() {
        return sampleAtIndex(size() - 1);
    }

  /**
   * Return the Sample closest to the given time. If the time is way outside the time
   * span of the segment it will still return the closest; usually the first or last.
  */
  public Sample closestSample(double dtime) {
    if (!hasTimeSeries())  return null;         // no time series

    if (dtime < getEpochStart()) return getFirstSample();      // before time range
    if (dtime > getEpochEnd())   return getLastSample();           // after time range

    // must be in the range
    int sampNo =  sampleIndexAtTime(dtime);    // sample #

    return sampleAtIndex(sampNo);
 }

  /** return the Sample at index of this time series. If index is out of bounds
  * null is returned.
  */
  public Sample sampleAtIndex(int index) {
    if (!hasTimeSeries()) return null;
    Sample samp = null;
    if (index >= 0 && index < size()) { // bug? changed from || to && in test condition 2008/11/20 -aww
        samp = new Sample();
        samp.setValue(ts[index]);
        samp.setDatetime(timeAtIndex(index));
    }
    return samp;
  }

  public SampleWithPeriod sampleWithPeriodAtIndex(int index) {
    if (!hasTimeSeries()) return null;
    SampleWithPeriod samp = null;
    if (index >= 0 && index < size()) { // bug? changed from || to && in test condition 2008/11/20 -aww
        samp = new SampleWithPeriod();
        samp.setValue(ts[index]);
        samp.setDatetime(timeAtIndex(index));
        samp.setPeriod(getPeriodAt(index));
    }
    return samp;
  }

   /** Return the epoch time at this sample number. Returns -1 if index is out
   of range. */
   public double timeAtIndex(int index) {
     if (index < 0 || index >= size()) return -1;
     return getEpochStart() + (index * getSampleInterval());
   }

    /** Return index in the 'sample[]' array of the sample closest to the time given.
    * Returns -1 if time is not within this waveform segment. */
    public int sampleIndexAtTime(double time) {
           int idx = (int) Math.round((time - getEpochStart())/getSampleInterval());   // round
           if (idx < 0 || idx >= size()) idx = -1; // bug, used to return size, index must be < size - aww 2008/11/20
           return idx;
    }

  /** Estimated period between zero-crossings with the bias removed at the time. */
  public float getPeriodAt(double time) {
      if (!hasTimeSeries() || !getTimeSpan().contains(time)) return Float.NaN; // no time series
      return getPeriodAt(sampleIndexAtTime(time));
  }

  /** Estimated period between zero-crossings with the bias removed at the time. */
  private float getPeriodAt(int peakIdx) {

     if (peakIdx < 0) return Float.NaN;

     float bias = getBias();

     float samp = 0.f;

     float peakSamp = ts[peakIdx] - bias;

     int plusIdx = peakIdx;
     for (int i = peakIdx+1; i < size(); i++) {
           samp = ts[i]-bias; // remove bias
           if ( (peakSamp>0.0f) != (samp>0.0f) ) { // sign changed :. a zero crossing!
               plusIdx = i;
               break;
           }
     }

     int minusIdx = peakIdx;
     for (int i = peakIdx-1; i >= 0; i--) {
           samp = ts[i]-bias; // remove bias
           if ( (peakSamp>0.0f) != (samp>0.0f) ) { // sign changed :. a zero crossing!
               minusIdx = i;
               break;
           }
     }

     if ((plusIdx - minusIdx) < 2) return Float.NaN; // best guess minimum

     return (float) ((plusIdx - minusIdx - 1) * getSampleInterval() * 2.);

  }

    /**
     * Given a Collection of WFSegments, collapse them into the minimum number
     * of segments that preserves time-gaps. A Collection of contiguous segs
     * should return a single WFSegment. Returns a Collection containing the new
     * concatenated WFSegments.  */
    public static Collection collapse(Collection oldList) {

        ArrayList newSegList = new ArrayList();

        WFSegment seg[] = WFSegment.getArray(oldList);

        int i = 0;
        WFSegment mergedSeg = null;
            /*
            double qclk = seg[0].getTimeQuality().doubleValue();
            double lstclk = 0.;
            int zcnt = 0;
            int ocnt = 0;
            int mcnt = 0;
            double minval = Math.min(1., qclk);
            double maxval = Math.max(0., qclk);
            */
        while (i < seg.length) {
            WFSegment newseg = new WFSegment(seg[i]);   // 1st seg in group
            i++;

            // This loop works as long as segs are contiguous
            while (i < seg.length && WFSegment.areContiguous(newseg, seg[i])) {
                mergedSeg = WFSegment.concatenate(newseg, seg[i]);
                  /*
                  lstclk = seg[i].getTimeQuality().doubleValue();
                  minval = Math.min(minval, lstclk);
                  maxval = Math.max(maxval, lstclk);
                  if ((lstclk-qclk) == 0) {
                    zcnt++;
                  }
                  else if (Math.abs(lstclk-qclk) <= .0101) {
                    ocnt++;
                  }
                  else {
                    mcnt++;
                  }
                  if (Math.abs(lstclk-qclk) >= .01) { 
                    System.out.printf("                          %04d segment(s) time quality difference at segment %03d =%6.3f, %6.3f->%6.3f%n",
                            seg.length, i, (lstclk-qclk), qclk, lstclk); 
                    qclk = lstclk;
                  }
                  */

                if (mergedSeg == null) break;  // sample rate changed or it's not contiguous
                newseg = mergedSeg;
                i++;
            }
            // set bias, max, min, etc.
            newseg.scanTimeSeries();
            newSegList.add(newseg);
        }
        /*
        if (seg.length > 1) {
         System.out.printf("FYI WFSegment concatenated to %d segments, original quality cnts with diff of zero=%d, one=%d, many=%d (%6.3f, %6.3f) for %s%n%n",
               newSegList.size(), zcnt, ocnt, mcnt, minval, maxval, seg[0].getChannelObj().toDelimitedSeedNameString());
        }
        */

        return newSegList;
    }

/*
    public static WFSegment concatenate(WFSegment seg1, WFSegment seg2)
    {
    // Check contiguousness
    if (!WFSegment.areContiguous(seg1, seg2)) return null;

        int newSize = seg1.size() + seg2.size();
        int oldSeg1Size = seg1.size();

    // join time series
    float ns[] = new float[newSize];    // new array big enough for both
    try {
      //                  src,  pos, dest, pos, length
        System.arraycopy(seg1.ts, 0, ns, 0, seg1.size());
        System.arraycopy(seg2.ts, 0, ns, seg1.size(), seg2.size());

    } catch (Exception ex) {
      System.err.println ("WFSegment.concatenate error: " + ex.toString());
    }

    // Update header fields
    seg1.setEnd(seg2.getEnd());
//  seg1.length   += seg2.length;         // length in bytes
//      seg1.sampleCount  += seg1.sample.length +  seg2.sample.length;
//     seg1.sampleCount  = newSize ;   // length in samples

     // is this right? or does it skip a sample interval?
     //  Should be??? seg1.lenSecs = seg.tend = seg.tstart;
//  seg1.lenSecs      += seg2.lenSecs;

    // averaging the bias is an expedient. Should recalc if you really care.
    //  seg1.biasVal      = (seg1.biasVal + seg2.biasVal) /2.0;

    // These will be scanned for when done
    //seg1.biasVal      = ( (seg1.biasVal * oldSeg1Size) + (seg2.biasVal * seg2.size())) / newSize;
    //seg1.maxSample    = Sample.max(seg1.maxSample, seg2.maxSample);
    //seg1.minSample    = Sample.min(seg1.minSample, seg2.minSample);

    seg1.setTimeSeries(ns , false); // use new time series array

    // combine (OR) quality bits
    seg1.clockLocked    |= seg2.clockLocked;
    seg1.ampSaturated   |= seg2.ampSaturated;       // clipped?
    seg1.digClipped     |= seg2.digClipped;
    seg1.spikes     |= seg2.spikes;
    seg1.glitches       |= seg2.glitches;
    seg1.badTimeTag     |= seg2.badTimeTag;

// precision problems     if (seg1.lenSecs != seg1.sampleCount*seg1.dt)

    return seg1;
    }

*/
    /**
     * Concatinate two WFSegments. Set all header stuff. Returns 'null'
     * if segments are not contiguous. Does NOT check anything else (like are
     * they the same channel, format, etc.). For speed, does NOT scan the
     * resulting segment for bias, max, min, etc. You must do that when you're
     * done concatenating.
     */
    public static WFSegment concatenate(WFSegment s1, WFSegment s2) {

        if (s2 == null) return s1; // return the input, nothing to add

        float [] ts2 = s2.getTimeSeries();
        int cnt2 = ts2.length;
        if (cnt2 == 0) return s1;  // return the input, nothing to add

        // Get metadata and check for time tear
        double s2StartT = s2.getEpochStart(); 
        double s1EndT = s1.getEpochEnd(); 
        double sampInt1 = s1.getSampleInterval();
        double delta = sampInt1*getTimeTearTolerance();
        if ( (s2StartT - s1EndT) > delta ) return null; // can't combine series, gap exists
        //System.out.println("FYI WFSegment concatenate " + s1.getChannelObj().toDelimitedSeedNameString() + " slew: " + (s2StartT-s1EndT-sampInt1)); 


        // NOTE: No checks here for sample intervals, channel identity etc.
        // Check for time series data to combine
        float [] ts1 = s1.getTimeSeries();
        int cnt1 = ts1.length;
        if (cnt1 == 0) { // no data in first, override with metadata from second
            float [] tsNew = new float [cnt2]; // new time series array
            System.arraycopy(ts2, 0, tsNew, 0, cnt2);
            // reset 1st segment's metadata to match 2nd's
            s1.setStart(s2StartT);
            s1.setEnd(s2.getEpochEnd());
            s1.setSampleInterval(s2.getSampleInterval());
            s1.setTimeSeries(tsNew , false);
            copyTimeFlags(s1, s2);
        }
        else { // data in both segments either contiguous or an overlap (total or partial) 
            double sampInt2 = s2.getSampleInterval();
            if (Math.abs(sampInt2 - sampInt1) > .00001) { 
                System.out.println("FYI WFSegment concatenate skipped " + s1.getChannelObj().toDelimitedSeedNameString() +
                    " sample interval changed from 1: " + sampInt1 + " to 2: " + sampInt2 +
                    " at time: " + LeapSeconds.trueToString(s2StartT)); 
                return null; // preserve rate change do not concatenate segments when sampling rate change
            }

            /* Do we want to check the time quality flags s1.getTimeQuality(), s1.clockLocked, or s1.badTimeTag too ?
            if (s1.clockLocked != s2.clockLocked)
                    System.out.println("FYI WFSegment concatenate clock locked difference " + s1.getChannelObj().toString() +
                    " 1: " + s1.clockLocked + "  2: " + s2.clockLocked);
            
            if (s1.badTimeTag != s2.badTimeTag)
                    System.out.println("FYI WFSegment concatenate bad time tag difference " + s1.getChannelObj().toString() +
                    " 1: " + s1.badTimeTag + "  2: " + s2.badTimeTag);
            
            if (Math.abs(s1.getTimeQuality().doubleValue()-s2.getTimeQuality().doubleValue()) > 0.10) 
                    System.out.println("FYI WFSegment concatenate time quality difference " + s1.getChannelObj().toString() +
                    " 1: " + s1.getTimeQuality() + "  2: " + s2.getTimeQuality());
            */
            
            int i2start = 0;
            double nextStartT = s1EndT + 0.5*sampInt1; // only add 1/2 sample forward in time (allow for jitter)
            if (s2StartT < nextStartT) {
                double t2 = s2StartT; 
                while (i2start < cnt2) {
                    if (t2 > nextStartT) break;
                    t2 += sampInt2; // time better tied to 2nd rather than 1st interval
                    i2start++;
                } 
            }
            int nSamp2 = cnt2 - i2start;
            if (nSamp2 > 0) { // 2nd goes beyond end of 1st, so combine series
               float [] tsNew = new float [cnt1 + nSamp2];
               System.arraycopy(ts1, 0, tsNew, 0, cnt1);
               System.arraycopy(ts2, i2start, tsNew, cnt1, nSamp2);
               s1.setEnd(s2.getEpochEnd());
               orTimeFlags(s1, s2);
               s1.setTimeSeries(tsNew , false); // new time series array
            }
        }
        return s1;
    }

    private static void orTimeFlags(WFSegment s1, WFSegment s2) {
        // combine (OR) quality bits
        s1.clockLocked    |= s2.clockLocked;
        s1.ampSaturated   |= s2.ampSaturated;       // clipped?
        s1.digClipped     |= s2.digClipped;
        s1.spikes         |= s2.spikes;
        s1.glitches       |= s2.glitches;
        s1.badTimeTag     |= s2.badTimeTag;
    }

    /** Return a sub-segment of this segment as defined by the start/stop times.
    * If either the start or end time is beyond what is available in the WFSegment
    * the returned WFSegment will be limited by the available data. If the
    * time window does not overlap the time of this WFSegment an empty WFSegment
    * is returned, one with zero-length timeseries.
    */
     public WFSegment getSubSegment(double startTime, double endTime) {
         WFSegment newWFseg = new WFSegment(this);
         if (newWFseg.trim(startTime, endTime)) return newWFseg; // valid input and/or data series -aww 2010/09/24

         newWFseg.setTimeSeries(new float[0]); // reset segment timeseries -aww 2010/09/24

         return newWFseg;
     }

    /** Trim the time in this WFSegment to this range of samples.
    * If the lastSample is beyond what is available in the WFSegment
    * the trim will be limited by the available data.
    */
     public boolean trim(int firstSample, int lastSample) {

         if (size() == 0) return true;

         if (lastSample < firstSample) return false; // bad input, throw exception ?

         if (firstSample < 0) firstSample = 0; 

         int lastData = size()-1;
         if (lastSample > lastData) lastSample = lastData;

         if (firstSample == 0 && lastSample == lastData) return true; // can't trim, done

         int arrayLen = (lastSample - firstSample) + 1;
         float newSample[] = new float[arrayLen] ;
         System.arraycopy(ts, firstSample, newSample, 0, arrayLen);

         ts = newSample;

         scanTimeSeries();

         return true;
     }

    /** Trim the time in this WFSegment to this time window.
    * If either the start or end time is beyond what is available in the WFSegment
    * the trim will be limited by the available data. If the
    * time window does not overlap the time of this WFSegment the WFSegment is
    * left unchanged and 'false' is returned.
    */
    public boolean trim(double startTime, double endTime) {

        // no overlap
        if (startTime > getEpochEnd() || endTime < getEpochStart()) return false;

        int firstSamp = sampleIndexAtTime(startTime);
        int lastSamp  = sampleIndexAtTime(endTime);

        if (firstSamp < 0) {
            firstSamp = 0;
        } else {
            //setStart(startTime);
            setStart(getEpochStart() + firstSamp*getSampleInterval());
        }

        if (lastSamp < 0 || lastSamp >= size()) {
            lastSamp = size()-1; // one less then length -aww 2008/11/20
        } else {
            //setEnd(endTime);
            setEnd(getEpochStart() + (lastSamp-firstSamp)*getSampleInterval());
        }

        return trim(firstSamp, lastSamp);
     }

    /**
     * Convert a Collection of WFSegments to an Array.
    */
    public static WFSegment[] getArray(Collection segList) {
      if (segList.size() == 0) return new WFSegment[0]; // changed from null - aww 2008/08/29

      WFSegment segArray[] = new WFSegment[segList.size()];
      return (WFSegment[]) segList.toArray(segArray);
    }

  /**
   * Return the Sample with the maximum amplitude in the waveform segment.
   * "Maximum" value could be the largest excursion downward.
   * Bias is NOT removed because segments may be short.
   * Because of large biases even "positive" peak would be a negative number.
   * Returns 'null' if no time-series.
   */
   public Sample getPeakSample() {
       return getPeakSample(getEpochStart(), getEpochEnd());
   }

  /**
   * Return the Sample with the maximum amplitude in the waveform segment.
   * "Maximum" value could be the largest excursion downward.
   * Bias is NOT removed because segments may be short.
   * Because of large biases even "positive" peak would be a negative number.
   * Returns 'null' if no time-series, time period is
   * not in this segment or the time bounds are inverted (start > end).
  */
  public Sample getPeakSample(double startTime, double endTime) {
     if (startTime > endTime ||       // inverted time
         !hasTimeSeries() ||          // no time series
         !getTimeSpan().overlaps(startTime, endTime)) return null;

    // determine start & end sample indexes with sanity checks
     int startSample = sampleIndexAtTime(startTime);
     if (startSample < 0) startSample = 0;
     int endSample   = sampleIndexAtTime(endTime);
     if (endSample < 0 || endSample >= size()) endSample = size()-1;

     if (startSample == endSample) return sampleAtIndex(startSample);

     float comp = Math.abs(ts[startSample]);
     int sampNo = startSample;

     for (int i = startSample + 1; i <= endSample; i++) {

        if ( Math.abs(ts[i]) > comp) {
           sampNo = i;
           comp = Math.abs(ts[i]);
        }
      }
      return sampleAtIndex(sampNo);

  }

  public SampleWithPeriod getPeakSampleWithPeriod(double startTime, double endTime) {
     if (startTime > endTime ||       // inverted time
         !hasTimeSeries() ||          // no time series
         !getTimeSpan().overlaps(startTime, endTime)) return null;

    // determine start & end sample indexes with sanity checks
     int startSample = sampleIndexAtTime(startTime);
     if (startSample < 0) startSample = 0;
     int endSample   = sampleIndexAtTime(endTime);
     if (endSample < 0 || endSample >= size()) endSample = size()-1;

     if (startSample == endSample) return sampleWithPeriodAtIndex(startSample);

     float comp = Math.abs(ts[startSample]);
     int sampNo = startSample;

     for (int i = startSample + 1; i <= endSample; i++) {

        if ( Math.abs(ts[i]) > comp) {
           sampNo = i;
           comp = Math.abs(ts[i]);
        }
      }
      return sampleWithPeriodAtIndex(sampNo);

  }

  /**
   * Return the Sample with the largest positive amplitude in the waveform segment.
   * Because of large biases even "positive" peak could be a negative number.
   * Returns 'null' if no time-series.
  */
  public Sample getMaxSample() {

    if (!hasTimeSeries()) return (null);        // no time series
    if (maxSample == null) {                   // only calculate on 1st request

      int sampNo = 0;
      double comp = ts[0];

      for (int i = 1; i < size(); i++) {
        if ( ts[i] > comp) {
           sampNo = i;
           comp = ts[i];
        }
      }
//      maxSample = new Sample (getEpochStart() + (sampNo * dt), ts[sampNo]);
      maxSample = sampleAtIndex(sampNo);
    }

    return maxSample;

  } // end of getMaxSample

  public Sample getMaxSample(double startTime, double endTime) {

      if (!hasTimeSeries()) return null;  // no time series

      int sampNo = 0;
      double comp = ts[0];

      int start = sampleIndexAtTime(startTime);
      if (start < 0) start = 0;
      int end   = sampleIndexAtTime(endTime);
      if (end < 0 || end >= size()) end = size()-1;

      for (int i = start; i <= end; i++) {
          if ( ts[i] > comp) {
              sampNo = i;
              comp = ts[i];
          }
      }
      return sampleAtIndex(sampNo);

  } // end of getMaxSample

 /**
  * Return the value with the largest positive amplitude in the waveform segment.
  * Because of large biases even "positive" peak could be a negative number.
  * Returns 'null' if no time-series.
  */
  public float getMaxValue(double startTime, double endTime) {

      if (!hasTimeSeries()) return Float.NaN;  // no time series

      float maxVal = (float) -Math.pow(2.,32.);
      int start = sampleIndexAtTime(startTime);
      if (start < 0) start = 0;
      int end   = sampleIndexAtTime(endTime);
      if (end < 0 || end >= size()) end = size()-1;

      for (int i=start; i<=end; i++) {
        if ( ts[i] > maxVal) {
           maxVal = ts[i];
        }
      }
      return maxVal;
  }

  /**
   * Return the Sample with the smallest, the most negative, amplitude value the waveform segment.
   * Because of large biases even "negative" peak could be a positive number.
  */
  public Sample getMinSample() {

    if (!hasTimeSeries()) return null;  // no time series
    if (minSample == null) {                // only calculate on 1st request

      int sampNo = 0;
      double comp = ts[0];

      for (int i = 1; i < size(); i++) {
        if ( ts[i] < comp) {
           sampNo = i;
           comp = ts[i];
        }
      }
//      minSample = new Sample (tstart + (sampNo * dt), ts[sampNo]);
      minSample = sampleAtIndex(sampNo);
    }

    return minSample;

 }

  public Sample getMinSample(double startTime, double endTime) {

      if (!hasTimeSeries()) return null;  // no time series

      int sampNo = 0;
      double comp = ts[0];

      int start = sampleIndexAtTime(startTime);
      if (start < 0) start = 0;
      int end   = sampleIndexAtTime(endTime);
      if (end < 0 || end >= size()) end = size()-1;

      for (int i = start; i <= end; i++) {
          if (ts[i] < comp) {
              sampNo = i;
              comp = ts[i];
          }
      }

      return sampleAtIndex(sampNo);
 }

  /**
   * Return the value with the smallest, the most negative, amplitude value the waveform segment.
   * Because of large biases even "negative" peak could be a positive number.
  */
  public float getMinValue(double startTime, double endTime) {

      if (!hasTimeSeries()) return Float.NaN;  // no time series

      float minVal = (float)Math.pow(2.,32.);
      int start = sampleIndexAtTime(startTime);
      if (start < 0) start = 0;
      int end   = sampleIndexAtTime(endTime);
      if (end < 0 || end >= size()) end = size()-1;

      for (int i=start; i<=end; i++) {
        if ( ts[i] < minVal) {
           minVal = ts[i];
        }
      }
      return minVal;
  }

  /**
  * Calculate the background noise level. It is
  * the mean of the peaks between zero-crossings for a rectified time-series
  * with the bias removed for the whole time-series.
  * Returns Float.NaN if it cannot be calculated.
  */
  public float scanForNoiseLevel() {
     return scanForNoiseLevel(getEpochStart(), getEpochEnd());
  }

  /**
  * Calculate the background noise level. Sets value of 'noiseLevel'. It is
  * the mean of the peaks between zero-crossings for a rectified time-series
  * with the bias for the time window given removed.
  * Returns Float.NaN if it cannot be calculated.
  */
  public float scanForNoiseLevel(double startTime, double endTime) {
      if (scanNoiseType == RMS) return scanForRMSAmpLevel(startTime,endTime);
      else if (scanNoiseType == AVG) return scanForMeanPeakAmpLevel(startTime,endTime); // Absolute Avg Peak Amp
      else return scanForMeanAbsAmpLevel(startTime,endTime); // AbsoluteAverageAmp value as like coda window
  }

  /** with the bias removed for the time window given. */
  public float scanForMeanAbsAmpLevel(double startTime, double endTime) { // averaged abs value like 2-second coda window

     if (!hasTimeSeries() ||    // no time series
         !getTimeSpan().overlaps(startTime, endTime)) return Float.NaN;

     int startSample = sampleIndexAtTime(startTime);
     if (startSample < 0)  startSample = 0;
     int endSample = sampleIndexAtTime(endTime);
     if (endSample < 0 || endSample >= size()) endSample = size()-1;

     float bias = getBias(startSample,endSample); // -aww use the starting and ending sample span for bias

     double sumSamp = 0.;
     for (int i = startSample; i <= endSample; i++) {
           sumSamp += Math.abs(ts[i]-bias); // remove bias
     }
     noiseLevel = (float) (sumSamp/(endSample-startSample+1));

     return noiseLevel;

  }

  /** with the bias removed for the time window given. */
  public float scanForMeanPeakAmpLevel(double startTime, double endTime) {

     if (!hasTimeSeries() ||    // no time series
         !getTimeSpan().overlaps(startTime, endTime)) return Float.NaN;

     int startSample = sampleIndexAtTime(startTime);
     if (startSample < 0)  startSample = 0;
     int endSample = sampleIndexAtTime(endTime);
     if (endSample < 0 || endSample >= size()) endSample = size()-1;

     float bias = getBias(startSample,endSample); // -aww use the starting and ending sample span for bias

     float samp = 0.f;
     float lastSamp = ts[startSample]-bias;
     float peak = Math.abs(lastSamp);

     npeak = 1;
     double sumPeak = peak;

     for (int i = startSample+1; i <= endSample; i++) {

           // remove bias
           samp = ts[i]-bias;

           // same sign :. not a zero crossing, search for peak
           if ( (lastSamp>0.0f) == (samp>0.0f) ) {
              peak = Math.max(peak,  Math.abs(samp));
           // sign changed :. a zero crossing!
           } else {
              sumPeak += peak;
              npeak++;
              peak = Math.abs(samp);
           }
           lastSamp = samp;
     }


     noiseLevel = (float) (sumPeak/npeak);

    // System.out.println ( "WFSegment scanForNoise noiseLevel: "+ noiseLevel +" npeak: "+npeak);

     return noiseLevel;

  }

  /** with the bias removed for the time window given. */
  public float scanForRMSAmpLevel(double startTime, double endTime) {

     if (!hasTimeSeries() ||    // no time series
         !getTimeSpan().overlaps(startTime, endTime)) return Float.NaN;

     int startSample = sampleIndexAtTime(startTime);
     if (startSample < 0) startSample = 0;
     int endSample   = sampleIndexAtTime(endTime);
     if (endSample < 0 || endSample >= size()) endSample = size()-1;

     float bias = getBias(startSample,endSample); // -aww use the starting and ending sample span for bias

     double sumSamp = Math.pow((double)(ts[startSample]-bias), 2.);

     for (int i = startSample+1; i <= endSample; i++) {
           sumSamp +=  Math.pow((double)(ts[i]-bias), 2.);
     }

     noiseLevel = (float) Math.sqrt(sumSamp/(endSample-startSample+1));

    // System.out.println ( "scanned RMS amp: " + noiseLevel );

     return noiseLevel;

  }

  /** Filter this time series in place.  */
  public void filter(FilterIF filter) {
      if (!hasTimeSeries()) return;

      filter.filter(ts);  // pass float[] of timeseries

      scanTimeSeries();   // recalc max, min, bias

  }

   /** Scan for bias, max and min. This should be done after any operation
   * that modifies the timeseries. */
   public void scanTimeSeries() {
     biasVal = NULL;
     maxSample = null;
     minSample = null;

     getBias();
     getMaxSample();
     getMinSample();

     //Do you want to always scanForNoiseLevel() too? -aww
     noiseLevel = Float.NaN; // reset aww 10/26/2004

   }

  /** Return the bias of the waveform segment including the input sample span */
  public float getBias(int start, int end) {

    if (end < start) throw new IllegalArgumentException("getBias input sample end index < start index");

    if (!hasTimeSeries()) {
        return 0;
    }

    if (start < 0) start = 0;
    if (end >= size()) end = size()-1;

    double sum = 0.0;
    for (int i=start; i<=end; i++) {
        sum += ts[i];
    }

    return (float) (sum/(end-start+1));

  }

  /** Return the bias of the waveform segment */
  public float getBias() {

    if (!hasTimeSeries()) {
        return 0;
    }

    if (biasVal == NULL) { // only calculate on 1st request
        double sum = 0.0;
        for (int i = 0; i < size(); i++) {
            sum += ts[i];
        }
        biasVal = (float) (sum / size());
    }

    return biasVal;
  }

    /**
    * Dump some WFSegment info for debugging
    */
    public void dump() {
        System.out.println (
            " rec = " + rec + " " +
            " source = " + source + " " +
            " nsamps = " + size() + " " +
            " secs = " + getDuration() + " " +
            " fmt = " + fmt );
        System.out.println ( " filename = " + filename);
        System.out.println ( " timespan = " + getTimeSpan().toString());
    }

    /**
   * Dump some WFSegment info for debugging
   */
    public String dumpToString() {
           return toString();
    }

    /**
    * Dump some WFSegment info for debugging
    */
    public String toString() {
        String str =  "chan = "+ getChannelObj().toString() +
          " rec = " + rec + " " +
          " source = " + source + " " +
//        " sampleCount = " + sampleCount + " " +
          " size = " + size() + " " +
          " lenSecs = " + getDuration() + " " +
          " dt = " + getSampleInterval() +
          " fmt = " + fmt + "\n";
        str += " filename = " + filename +
             " bytesPerSample = "+ bytesPerSample+"\n";

        if (maxSample != null) str += " maxAmp = " + maxSample.value;
        if (minSample != null) str += " minAmp = " + minSample.value;

        str +=  " clockLocked = "+ clockLocked +
          " ampSaturated = " + ampSaturated +
          " digClipped = " + digClipped +
          " spikes = " + spikes +
          " glitches = " + glitches +
          " badTimeTag = " + badTimeTag +
          " timeQual = "+ getTimeQuality()+"\n";
        str +=  " timespan = " + getTimeSpan().toString() + "\n";

        return str;
    }
}   // end of WFSegment class

