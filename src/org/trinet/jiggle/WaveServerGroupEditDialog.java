package org.trinet.jiggle;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

import java.util.Collection;
import org.trinet.jasi.ActiveList;

/**
* This is a dialog that allows adding, deleting and editing WaveServerGroups.
*/

public class WaveServerGroupEditDialog extends JDialog implements Observer {

    private JiggleProperties props;

    /** List of WaveServerGroups */
    private ActiveList wsgList = new ActiveList(); // the WaveServerGroup List
    private JList wsgNameList = null; // Swing graphic containing list of the names of the above list groups 

    private WaveServerGroupDialogPanel wsgDialog = null; // editor for selected WaveServerGroup

    private JPanel wsgEditPanel = null; // contains the wsgDialog 

    public WaveServerGroupEditDialog(JiggleProperties props) {

        setModal(true);

        setProperties(props);

        try  {
            jbInit();
            pack();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }

        centerDialog();
        setVisible(true);
    }

    public void setProperties(JiggleProperties props) {
        this.props = props;
        //this.props.setupWaveServerGroup();
    }

    public JiggleProperties getProperties() {
        return props;
    }

    /** Set the WaveServerGroup that is shown in the right (edit) panel. */
    public void setGroup(WaveServerGroup grp) {

        if (wsgEditPanel != null) wsgEditPanel.removeAll();

        if (grp != null) {
            wsgDialog = new WaveServerGroupDialogPanel(grp);

            // pass the list handle so dialog can add things to it with its "UPDATE" button
            wsgDialog.set(wsgList);

            if (wsgEditPanel != null) wsgEditPanel.add(wsgDialog);
        }
        if (wsgEditPanel != null) wsgEditPanel.revalidate();
    }

    /** Returns the list of WaveServerGroups. */
    public Collection getList() {
        return wsgList;
    }

    /** Forces refresh of all graphics with the data of the current WaveServerGroup list. */
    public void resetList() {
       setList(wsgList);
    }

    /** Set the list that shows in the left panel. */
    public void setList(ActiveList list) {

        // stop listening to old list
        if (wsgList != null) wsgList.deleteObserver(this);

        wsgList = list;
        wsgList.addObserver(this);

        if (wsgDialog == null) wsgDialog = new WaveServerGroupDialogPanel();
        wsgDialog.set(wsgList);

        if (list.isEmpty()) return; // moved from top of method down to here -aww 2008/08/22

        DefaultListModel model = new DefaultListModel();

        // put names in list
        for (int i = 0; i<wsgList.size(); i++) {
           String name = ((WaveServerGroup)wsgList.get(i)).getName();
           model.addElement(name);
        }

        wsgNameList.setModel(model);

        WaveServerGroup sel = WaveServerGroup.getSelected(wsgList);
        if (sel == null) {
          wsgNameList.setSelectedIndex(0);
        } else {
          wsgNameList.setSelectedValue(sel.getName(), true);
        }

        wsgNameList.revalidate();
    }

    /** Returns the WaveServerGroup currently selected. */
    public WaveServerGroup getSelected() {
        //return WaveServerGroup.getSelected(props.getWaveServerGroupList());
        return WaveServerGroup.getByName(wsgList,(String)wsgNameList.getSelectedValue());
    }

    private JPanel createButtonPanel() {
        JButton doneButton = new JButton("DONE");
        doneButton.setToolTipText("Update JiggleProperties.");
        doneButton.addActionListener(new ActionListener() {
             public void actionPerformed(ActionEvent e) {
                  doneButton_actionPerformed(e);
             }
        });

        JButton newButton = new JButton("CREATE");
        newButton.setToolTipText("Create new WaveServerGroup");
        newButton.addActionListener(new ActionListener() {
             public void actionPerformed(ActionEvent e) {
                  newButton_actionPerformed(e);
             }
        });

        JButton deleteButton = new JButton("DELETE");
        deleteButton.setToolTipText("Delete selected WaveServerGroup");
        deleteButton.addActionListener(new ActionListener() {
             public void actionPerformed(ActionEvent e) {
                  deleteButton_actionPerformed(e);
               }
        });

        JButton cancelButton = new JButton("CANCEL");
        cancelButton.setToolTipText("Close without updating JiggleProperties");
        cancelButton.addActionListener(new ActionListener() {
             public void actionPerformed(ActionEvent e) {
                  WaveServerGroupEditDialog.this.setVisible(false);    // dismiss dialog
             }
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(doneButton);
        buttonPanel.add(newButton);
        buttonPanel.add(deleteButton);
        buttonPanel.add(cancelButton);
        return buttonPanel;
    }

    private JPanel createListPanel() {
        // copy of the list in case we cancel
        wsgNameList = new JList();
        wsgNameList.setBorder(BorderFactory.createLineBorder(Color.black));
        wsgNameList.addListSelectionListener(new ListListener());
        setList(new ActiveList(props.getWaveServerGroupList())); // uses above list
        JPanel wsgListPanel  = new JPanel();
        wsgListPanel.setBorder(new TitledBorder("Editable WaveServer Group Profiles"));
        wsgListPanel.setLayout(new BorderLayout());
        wsgListPanel.add(new JScrollPane(wsgNameList), BorderLayout.CENTER);
        wsgListPanel.add(createButtonPanel(), BorderLayout.SOUTH);
        wsgListPanel.setPreferredSize(new Dimension(400, 400));
        return wsgListPanel;
    }

    private JPanel createEditPanel() {
        wsgEditPanel = new JPanel();
        wsgEditPanel.add(wsgDialog);
        wsgEditPanel.setBorder(new TitledBorder("Edit WaveServer Group"));
        return wsgEditPanel;
    }

    private void jbInit() throws Exception {

        setTitle("WaveServer Group Editing");

        Box horizBox = Box.createHorizontalBox();
        horizBox.add(Box.createGlue());
        horizBox.add(createListPanel());
        wsgEditPanel = createEditPanel();
        horizBox.add(wsgEditPanel);
        horizBox.add(Box.createGlue());

        wsgNameList.setBackground(wsgEditPanel.getBackground());
        wsgNameList.getCellRenderer().getListCellRendererComponent(wsgNameList, "", 0, false, false).setBackground(wsgNameList.getBackground());

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(horizBox, BorderLayout.CENTER);
        getContentPane().add(mainPanel);
    }

    /**
     * Center the dialog on the screen
     */
    private void centerDialog() {
        Dimension screenSize = getToolkit().getScreenSize();
        Dimension size = getSize();
        screenSize.height = screenSize.height/2;
        screenSize.width = screenSize.width/2;
        size.height = size.height/2;
        size.width = size.width/2;
        int y = screenSize.height - size.height;
        int x = screenSize.width - size.width;
        setLocation(x,y);
    }

    /** Observer Interface method.
     * <i>obs</i> is instance of ActiveList.MyModel, <i>arg </i> is instance of WaveServerGroup */
    public void update(Observable obs, Object arg) {
        // WaveServerGroup list changed
        if (arg instanceof WaveServerGroup) resetList();
    }

    /** Delete a WaveServerGroup */
    private void deleteButton_actionPerformed(ActionEvent e) {
        wsgList.delete(getSelected());
        //delete forces observer update (resetList()) as does any add done by wsgDialogPanel
    }

    /** Make a new WaveServerGroup */
    private void newButton_actionPerformed(ActionEvent e) {
       WaveServerGroup newWsg = new WaveServerGroup("<new>");
       newWsg.getWaveClient().setWaveformVerify(false);
       setGroup(newWsg);
    }

    /** All done, commit to changes */
    private void doneButton_actionPerformed(ActionEvent e) {

          // replace the "original list with the modified one
         props.setWaveServerGroupList((ActiveList) wsgList);

         //System.out.println("*** WSGED selected group = "+ WaveServerGroup.getSelected(props.getWaveServerGroupList()).getName());
         setVisible(false); // dismiss dialog
    }

    /** Handle selections from the list of WaveServer groups. */
    class ListListener implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent evt) {

            JList src = (JList) evt.getSource();

            int index = src.getSelectedIndex();
            //System.out.println("WaveServerGroupEditDialog ListListener selected index = " +index);

            // if there's no selection or the list is empty getSelectedIndex returns -1
            if (index > -1) { // get the wsg thats selected in the JList
               WaveServerGroup wsg = (WaveServerGroup) wsgList.get(index);
               WaveServerGroup.setSelected(wsg, wsgList);
               setGroup(wsg); // show it in the right panel
            } else {
               setGroup(null);
            }

        }
    }

}

