package org.trinet.jasi;

import org.trinet.util.Bits;

/**
 * A group of boolean variables describing the quality of a time series.
 * This is based on the information available in a SEED record.
 * All values are initiallized to 'false'.
 * See SEED manuel v2.3, pg. 93
 */

public class TimeSeriesQuality {

/** Data and clock quality bits from Seed Header flags */

// Activity info
    public boolean calPresent = false;
    public boolean timeCorrected = false;
    public boolean eventStart = false;
    public boolean eventEnd = false;
    public boolean leapSecondPresent = false;
    public boolean eventInProgress = false;

// I/O flag info
    public boolean parityErrorPresent = false;
    public boolean longRecordRead = false;
    public boolean shortRecordRead = false;
    public boolean timeSeriesStart = false;
    public boolean timeSeriesEnd = false;
    public boolean clockLocked = false;

// Quality flag info
    public boolean ampSaturated = false;
    public boolean digClipped = false;
    public boolean spikes = false;
    public boolean glitches = false;
    public boolean missingData = false;
    public boolean synchError = false;
    public boolean filterChanging = false;
    public boolean badTimeTag = false;

  public TimeSeriesQuality() {
  }

  /** Parse the byte in which the "Activity" information is encoded. */
  public void setActivityFlags (byte ubyte) {
      calPresent        = Bits.bitSet( ubyte, 0);
      timeCorrected     = Bits.bitSet( ubyte, 1);
      eventStart        = Bits.bitSet( ubyte, 2);
      eventEnd          = Bits.bitSet( ubyte, 3);
      leapSecondPresent = Bits.bitSet( ubyte, 4);
      eventInProgress   = Bits.bitSet( ubyte, 5);
  }

  /** Parse the byte in which the "I/O" information is encoded. */
  public void setIOFlags (byte ubyte) {
      parityErrorPresent = Bits.bitSet( ubyte, 0);
      longRecordRead   = Bits.bitSet( ubyte, 1);
      shortRecordRead  = Bits.bitSet( ubyte, 2);
      timeSeriesStart  = Bits.bitSet( ubyte, 3);
      timeSeriesEnd    = Bits.bitSet( ubyte, 4);
      clockLocked      = Bits.bitSet( ubyte, 5);
  }

  /** Parse the byte in which the "quality" information is encoded. */
  public void setQualityFlags (byte ubyte) {
      ampSaturated    = Bits.bitSet( ubyte, 0);
      digClipped      = Bits.bitSet( ubyte, 1);
      spikes	      = Bits.bitSet( ubyte, 2);
      glitches	      = Bits.bitSet( ubyte, 3);
      missingData     = Bits.bitSet( ubyte, 4);
      synchError      = Bits.bitSet( ubyte, 5);
      filterChanging  = Bits.bitSet( ubyte, 6);
      badTimeTag      = Bits.bitSet( ubyte, 7);
  }

  public String toString() {
    StringBuffer sb = new StringBuffer(128);
    sb.append((calPresent) ? "T":"F");
    sb.append((timeCorrected) ? "T":"F");
    sb.append((eventStart) ? "T":"F");
    sb.append((eventEnd) ? "T":"F");
    sb.append((leapSecondPresent) ? "T":"F");
    sb.append((eventInProgress) ? "T":"F");
    sb.append((parityErrorPresent) ? "T":"F");
    sb.append((longRecordRead) ? "T":"F");
    sb.append((shortRecordRead) ? "T":"F");
    sb.append((timeSeriesStart) ? "T":"F");
    sb.append((timeSeriesEnd) ? "T":"F");
    sb.append((clockLocked) ? "T":"F");
    sb.append((ampSaturated) ? "T":"F");
    sb.append((digClipped) ? "T":"F");
    sb.append((spikes) ? "T":"F");
    sb.append((glitches) ? "T":"F");
    sb.append((missingData) ? "T":"F");
    sb.append((synchError) ? "T":"F");
    sb.append((filterChanging) ? "T":"F");
    sb.append((badTimeTag) ? "T":"F");
    return sb.toString();
  }

}