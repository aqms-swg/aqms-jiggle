package test.testqueries.common;

import test.common.DatabaseTests;
import test.org.trinet.apps.CalcMLTests;
import test.org.trinet.apps.CubeEventLineTests;
import test.org.trinet.apps.MakeGmpTests;
import test.org.trinet.jasi.ChannelGrouperTests;

import java.sql.*;

public class TestQueries {

    protected Connection connection;

    public TestQueries(String URL) throws SQLException {
        connection = DriverManager.getConnection(URL.replace("test", DatabaseTests.getDbName()), DatabaseTests.getDbUsername(), DatabaseTests.getDbPassword());
    }

    public int populateDataForArcImportTest() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.\"event\"\n" +
                "(evid, prefor, prefmag, prefmec, commid, auth, subsource, etype, selectflag, \"version\", lddate)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, 38816154);
        statement.setInt(index++, 5935970);
        statement.setInt(index++, 9245426);
        statement.setNull(index++, Types.INTEGER);
        statement.setNull(index++, Types.INTEGER);
        statement.setString(index++, "CI");
        statement.setString(index++, "RT3");
        statement.setString(index++, "eq");
        statement.setInt(index++, 0);
        statement.setInt(index++, 2);
        statement.setString(index++, "2021-01-15 08:26:31.000");
        return insertQueryHelper(statement);
    }

    public int populateJasiChannelViewDataForAmpGenPpTest() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.jasi_channel_view\n" +
                "(net, sta, seedchan, channel, channelsrc, \"location\", lat, lon, elev, edepth, azimuth, dip, samprate, ondate, offdate)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(index++, "AZ");
        statement.setString(index++, "BZN");
        statement.setString(index++, "HHE");
        statement.setString(index++, "HHE");
        statement.setString(index++, "SEED");
        statement.setString(index++, "  ");
        statement.setDouble(index++, 33.4915);
        statement.setDouble(index++, -116.667);
        statement.setDouble(index++, 1301.0);
        statement.setDouble(index++, 0.0);
        statement.setDouble(index++, 90.0);
        statement.setDouble(index++, 0.0);
        statement.setDouble(index++, 100.0);
        statement.setString(index++, "2013-10-22 19:30:00.000");
        statement.setString(index++, "3000-01-01 00:00:00.000");
        return insertQueryHelper(statement);
    }

    /**
     * Must be run before app channels insert query
     */
    public int populateApplicationsDataForAmpGenPpTest() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.applications\n" +
                "(progid, \"name\", lddate)\n" +
                "VALUES(?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, 1);
        statement.setString(index++, "AmpGen");
        statement.setString(index++, "2008-12-09 11:07:19.000");
        return insertQueryHelper(statement);
    }

    public int populateAppChannelsDataForAmpGenPpTest() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.appchannels\n" +
                "(progid, net, sta, seedchan, \"location\", config, ondate, offdate, lddate)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, 1);
        statement.setString(index++, "AZ");
        statement.setString(index++, "BZN");
        statement.setString(index++, "HHE");
        statement.setString(index++, "  ");
        statement.setString(index++, "");
        statement.setString(index++, "2004-08-25 23:15:00.000");
        statement.setString(index++, "3000-01-01 00:00:00.000");
        statement.setString(index++, "2008-12-09 11:07:19.000");
        return insertQueryHelper(statement);
    }

    public int populateGazetteerLineDataForWhereIsEngineTest() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.gazetteerline\n" +
                "(gazid, \"type\", \"name\", line, format, points, lddate)\n" +
                "VALUES(?, ?, ?, ?, ?, decode(?, 'hex'), TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, 54);
        statement.setInt(index++, 601);
        statement.setString(index++, "San Jacinto Fault");
        statement.setInt(index++, 1);
        statement.setString(index++, "double");
        statement.setString(index++, "404127B7E90FF972C05D67CB923A29C74041263886594AF5C05D66F1A9FBE76D40412538EF34D6A1C05D65CC63F14120404123C9EECBFB16C05D6427525460AA4041200000000000C05D62AB367A0F914041200000000000C05D61BDA5119CE040411F6FD21FF2E5C05D605D6388659540411DC28F5C28F6C05D5EBB98C7E28240411B3333333333C05D5D0CB295E9E240411A6E978D4FDFC05D5C2A9930BE0E4041193DD97F62B7C05D5B404EA4A8C1404118068DB8BAC7C05D5A6E978D4FDF404115E00D1B7176C05D5923A29C779A40411353F7CED917C05D57E28240B780404111D495182A99C05D574D6A161E4F4041106594AF4F0EC05D568C154C985F40410EC8B4395810C05D55BC01A36E2F40410CBFB15B573FC05D54ADAB9F559B40410A95E9E1B08AC05D538A0902DE014041088CE703AFB8C05D528A71DE69AD404106978D4FDF3BC05D516A161E4F76404105119CE075F7C05D508E8A71DE6A404103C01A36E2EBC05D4FE5C91D14E4404102161E4F7660C05D4F06F69446744041000000000000C05D4DED288CE7044040FEFD21FF2E49C05D4CFC504816F04040FD2BD3C36113C05D4BDBF487FCB94040FB851EB851ECC05D4AE978D4FDF44040FA1CAC083127C05D4A075F6FD2204040F8D4FDF3B646C05D494AF4F0D8454040F780346DC5D6C05D486F694467384040F645A1CAC083C05D47DF3B645A1D4040F521FF2E48E9C05D477CED9168734040F374BC6A7EFAC05D46E2EB1C432D4040F22339C0EBEEC05D464C2F837B4A4040F0D1B71758E2C05D45A9FBE76C8B4040EF5F6FD21FF3C05D4513404EA4A94040ED21FF2E48E9C05D440EBEDFA4404040EB81D7DBF488C05D4310CB295E9E4040E9E4F765FD8BC05D42353F7CED914040E7E90FF97247C05D4116872B020C4040E6809D495183C05D4049BA5E353F4040E495182A9931C05D3F2617C1BDA54040E2C3C9EECBFBC05D3D916872B0214040E10624DD2F1BC05D3C51EB851EB84040DEE978D4FDF4C05D3AB367A0F9094040DD1B71758E22C05D396F0068DB8C4040DB4D6A161E4FC05D37EC56D5CFAB4040D9B089A02752C05D3657A786C2274040D7EC56D5CFABC05D34BE0DED288D4040D5B573EAB368C05D330F27BB2FEC4040D4154C985F07C05D31C0EBEDFA444040D23D70A3D70AC05D304D013A92A34040CF212D773190C05D2DC0EBEDFA444040CC226809D495C05D2BD21FF2E48F4040CB05532617C2C05D2AD288CE703B4040C9EB851EB852C05D29FF2E48E8A74040C92D77318FC5C05D2966CF41F2134040C85BC01A36E3C05D28710CB295EA4040C6E631F8A090C05D26BD3C3611344040C57A786C2268C05D25BC01A36E2F4040C35DCC63F141C05D24816F0068DC4040C16BB98C7E28C05D23318FC504814040C00D1B71758EC05D2228240B78034040BE0DED288CE7C05D20B780346DC64040BCD6A161E4F7C05D1F4A2339C0EC4040BB71758E2196C05D1DDE69AD42C44040B90CB295E9E2C05D1C538EF34D6A4040B7765FD8ADACC05D1AC226809D494040B5D97F62B6AEC05D191B71758E224040B491D14E3BCDC05D17AACD9E83E44040B271DE69AD43C05D15A6B50B0F284040B0C49BA5E354C05D140D1B71758E4040AF93DD97F62BC05D132B020C49BA4040AE4F765FD8AEC05D126CF41F212D4040ACC63F141206C05D111D14E3BCD34040ABB645A1CAC1C05D0FC6A7EF9DB24040AA6809D49518C05D0E9AD42C3C9F4040A90624DD2F1BC05D0D5B573EAB364040A7BB2FEC56D6C05D0C395810624E4040A65C91D14E3CC05D0B46DC5D63884040A4C63F141206C05D09D495182A994040A3B645A1CAC1C05D090CB295E9E24040A2027525460BC05D0801A36E2EB2");
        statement.setString(index++, "2024-07-19 13:06:07.338");
        return insertQueryHelper(statement);
    }

    public int populateEventDataForCubeEventLineTest() throws Exception {
        int index = 1;
        String sql = "INSERT INTO TRINETDB.EVENT\n" +
                "(EVID, PREFOR, PREFMAG, PREFMEC, COMMID, AUTH, SUBSOURCE, ETYPE, SELECTFLAG, VERSION, LDDATE)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, CubeEventLineTests.CubeEventLineTestEventId);
        statement.setInt(index++, 629663);
        statement.setInt(index++, 1312527);
        statement.setNull(index++, Types.INTEGER);
        statement.setNull(index++, Types.INTEGER);
        statement.setString(index++, "CI");
        statement.setString(index++, "RT7");
        statement.setString(index++, "eq");
        statement.setInt(index++, 0);
        statement.setInt(index++, 2);
        statement.setString(index++, "2014-06-26 19:34:56.000000");
        return insertQueryHelper(statement);
    }

    public int populateOriginDataForMakeGmpTest() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.origin\n" +
                "(orid, evid, prefmag, prefmec, commid, bogusflag, datetime, lat, lon, \"depth\", mdepth, \"type\", algorithm, algo_assoc, auth, subsource, datumhor, datumver, gap, distance, wrms, stime, erhor, sdep, erlat, erlon, totalarr, totalamp, ndef, nbs, nbfm, locevid, quality, fdepth, fepi, ftime, vmodelid, cmodelid, crust_type, crust_model, gtype, lddate, rflag)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'), ?);";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, MakeGmpTests.MakeGMPTestOrId);
        statement.setInt(index++, MakeGmpTests.MakeGMPTestEventId);
        statement.setInt(index++, MakeGmpTests.MakeGMPTestPreMag);
        statement.setInt(index++, MakeGmpTests.MakeGMPTestPreMec);
        statement.setNull(index++, Types.INTEGER);
        statement.setInt(index++, 0);
        statement.setDouble(index++, 1718191386.58);
        statement.setDouble(index++, 33.78783416748047);
        statement.setDouble(index++, -116.12000274658203);
        statement.setDouble(index++, 4.380000114440918);
        statement.setDouble(index++, 5.070000171661377);
        statement.setString(index++, "H");
        statement.setString(index++, "BINDER");
        statement.setNull(index++, Types.VARCHAR);
        statement.setString(index++, "CI");
        statement.setString(index++, "RT6");
        statement.setNull(index++, Types.DOUBLE);
        statement.setNull(index++, Types.DOUBLE);
        statement.setDouble(index++, 37.0 );
        statement.setDouble(index++, 16.0);
        statement.setDouble(index++, 0.170000002);
        statement.setNull(index++, Types.DOUBLE);
        statement.setDouble(index++, 0.170000002);
        statement.setDouble(index++, 0.829999983);
        statement.setNull(index++, Types.INTEGER);
        statement.setNull(index++, Types.INTEGER);
        statement.setInt(index++, 152);
        statement.setNull(index++, Types.INTEGER);
        statement.setInt(index++, 131);
        statement.setInt(index++, 26);
        statement.setInt(index++, 117);
        statement.setString(index++, "27460");
        statement.setDouble(index++, 0.0);
        statement.setString(index++, "n");
        statement.setNull(index++, Types.VARCHAR);
        statement.setNull(index++, Types.VARCHAR);
        statement.setString(index++, "02");
        statement.setString(index++, "CI");
        statement.setString(index++, "H");
        statement.setString(index++, "03E");
        statement.setString(index++, "l");
        statement.setString(index++, "2024-06-12 11:24:16.601");
        statement.setString(index++, "A");
        return insertQueryHelper(statement);
    }

    public int populateEventDataForMakeGmpTest() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.\"event\"\n" +
                "(evid, prefor, prefmag, prefmec, commid, auth, subsource, etype, selectflag, \"version\", lddate)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, MakeGmpTests.MakeGMPTestEventId);
        statement.setInt(index++, MakeGmpTests.MakeGMPTestOrId);
        statement.setInt(index++, MakeGmpTests.MakeGMPTestPreMag);
        statement.setInt(index++, MakeGmpTests.MakeGMPTestPreMec);
        statement.setNull(index++, Types.INTEGER);
        statement.setString(index++, "CI");
        statement.setString(index++, "RT6");
        statement.setString(index++, "eq");
        statement.setInt(index++, 1);
        statement.setInt(index++, 5);
        statement.setString(index++, "2024-06-12 11:24:16.601");
        return insertQueryHelper(statement);
    }

    public int populateAmpDataForMakeGmpTest() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.amp\n" +
                "(ampid, commid, datetime, sta, net, auth, subsource, channel, channelsrc, seedchan, \"location\", iphase, amplitude, amptype, units, ampmeas, eramp, flagamp, per, snr, tau, quality, rflag, cflag, wstart, duration, lddate)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, MakeGmpTests.MakeGMPTestAmpId);
        statement.setNull(index++, Types.INTEGER);
        statement.setDouble(index++, 1718191413.1783);
        statement.setString(index++, "BZN");
        statement.setString(index++, "AZ");
        statement.setString(index++, "CI");
        statement.setString(index++, "RT6");
        statement.setString(index++, "HHE");
        statement.setString(index++, "SEED");
        statement.setString(index++, "HHE");
        statement.setString(index++, "  ");
        statement.setNull(index++, Types.VARCHAR);
        statement.setDouble(index++, 0.0014845997793599963);
        statement.setString(index++, "SP3.0");
        statement.setString(index++, "cmss");
        statement.setNull(index++, Types.VARCHAR);
        statement.setNull(index++, Types.DOUBLE);
        statement.setNull(index++, Types.VARCHAR);
        statement.setNull(index++, Types.DOUBLE);
        statement.setDouble(index++, 241.48167419433594);
        statement.setNull(index++, Types.DOUBLE);
        statement.setDouble(index++, 1.0);
        statement.setString(index++, "A");
        statement.setString(index++, "os");
        statement.setDouble(index++, 1718191391.764317);
        statement.setDouble(index++, 32.465605);
        statement.setString(index++, "2024-06-12 11:25:08.369");
        return insertQueryHelper(statement);
    }

    public int populateAssoCamoDataForMakeGmpTest() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.assocamo\n" +
                "(orid, ampid, commid, auth, subsource, delta, seaz, rflag, lddate)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, MakeGmpTests.MakeGMPTestOrId);
        statement.setInt(index++, MakeGmpTests.MakeGMPTestAmpId);
        statement.setNull(index++, Types.INTEGER);
        statement.setString(index++, "CI");
        statement.setString(index++, "RT6");
        statement.setDouble(index++, 60.4630775);
        statement.setDouble(index++, 56.9184914);
        statement.setString(index++, "A");
        statement.setString(index++, "2024-06-12 11:25:08.369");
        return insertQueryHelper(statement);
    }

    public int populateJasChannelViewForMakeHypoStaListTest() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.jasi_channel_view\n" +
                "(net, sta, seedchan, channel, channelsrc, \"location\", lat, lon, elev, edepth, azimuth, dip, samprate, ondate, offdate)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(index++, "PB");
        statement.setString(index++, "B918");
        statement.setString(index++, "EH1");
        statement.setString(index++, "EH1");
        statement.setString(index++, "SEED");
        statement.setString(index++, "  ");
        statement.setDouble(index++, 35.9357);
        statement.setDouble(index++, -117.6017);
        statement.setDouble(index++, 1042.6);
        statement.setDouble(index++, 189.9);
        statement.setDouble(index++, 344.5);
        statement.setDouble(index++, 0.0);
        statement.setDouble(index++, 100.0);
        statement.setString(index++, "2008-06-23 00:00:00.000");
        statement.setString(index++, "3000-01-01 00:00:00.000");
        return insertQueryHelper(statement);
    }

    public int populateEventDataForCalcMlTest() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.\"event\"\n" +
                "(evid, prefor, prefmag, prefmec, commid, auth, subsource, etype, selectflag, \"version\", lddate)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, CalcMLTests.CalcMLTestEventId);
        statement.setInt(index++, 6638399);
        statement.setInt(index++, 10619439);
        statement.setNull(index++, Types.INTEGER);
        statement.setNull(index++, Types.INTEGER);
        statement.setString(index++, "CI");
        statement.setString(index++, "RT1");
        statement.setString(index++, "eq");
        statement.setInt(index++, 0);
        statement.setInt(index++, 2);
        statement.setString(index++, "2021-01-15 08:26:31.000");
        return insertQueryHelper(statement);
    }

    public int populateAssocammDataForCalcMLTest() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.assocamm\n" +
                "(magid, ampid, commid, auth, subsource, weight, in_wgt, mag, magres, magcorr, importance, rflag, lddate)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, CalcMLTests.CalcMLTestMagId);
        statement.setInt(index++, CalcMLTests.CalcMLTestAmpId);
        statement.setNull(index++, Types.INTEGER);
        statement.setString(index++, "CI");
        statement.setString(index++, "RT1");
        statement.setDouble(index++, 1.0);
        statement.setDouble(index++, 1.0);
        statement.setDouble(index++, 1.35);
        statement.setDouble(index++, 0.02);
        statement.setDouble(index++, -0.34);
        statement.setNull(index++, Types.DOUBLE);
        statement.setString(index++, "A");
        statement.setString(index++, "2021-01-15 08:27:22.000");
        return insertQueryHelper(statement);
    }

    public int populateNetMagDataForCalcMLTest() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.netmag\n" +
                "(magid, orid, commid, magnitude, magtype, auth, subsource, magalgo, nsta, uncertainty, gap, distance, quality, rflag, lddate, nobs)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'), ?);";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, CalcMLTests.CalcMLTestMagId);
        statement.setInt(index++, 6638399);
        statement.setNull(index++, Types.INTEGER);
        statement.setDouble(index++, 1.33);
        statement.setString(index++, "l");
        statement.setString(index++, "CI");
        statement.setString(index++, "RT1");
        statement.setString(index++, "trimag-SP");
        statement.setInt(index++, 7);
        statement.setDouble(index++, 0.19);
        statement.setDouble(index++, 98.0);
        statement.setDouble(index++, 9.601);
        statement.setNull(index++, Types.DOUBLE);
        statement.setString(index++, "A");
        statement.setString(index++, "2021-01-15 08:27:22.000");
        statement.setInt(index++, 14);
        return insertQueryHelper(statement);
    }

    public int populateAmpDataForCalcMLTest() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.amp\n" +
                "(ampid, commid, datetime, sta, net, auth, subsource, channel, channelsrc, seedchan, \"location\", iphase, amplitude, amptype, units, ampmeas, eramp, flagamp, per, snr, tau, quality, rflag, cflag, wstart, duration, lddate)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, CalcMLTests.CalcMLTestAmpId);
        statement.setNull(index++, Types.INTEGER);
        statement.setDouble(index++, 1610699127.355);
        statement.setString(index++, "UABX");
        statement.setString(index++, "BC");
        statement.setString(index++, "CI");
        statement.setString(index++, "RT1");
        statement.setString(index++, "HHE");
        statement.setString(index++, "SEED");
        statement.setString(index++, "HHE");
        statement.setString(index++, "  ");
        statement.setNull(index++, Types.VARCHAR);
        statement.setDouble(index++, 0.03891371935606);
        statement.setString(index++, "WAS");
        statement.setString(index++, "cm");
        statement.setNull(index++, Types.DOUBLE);
        statement.setNull(index++, Types.DOUBLE);
        statement.setNull(index++, Types.VARCHAR);
        statement.setNull(index++, Types.DOUBLE);
        statement.setDouble(index++, 0.0);
        statement.setNull(index++, Types.DOUBLE);
        statement.setNull(index++, Types.DOUBLE);
        statement.setString(index++, "A");
        statement.setString(index++, "OS");
        statement.setDouble(index++, 1610699123.70993);
        statement.setDouble(index++, 7.140704);
        statement.setString(index++, "2021-01-15 08:27:22.000");
        return insertQueryHelper(statement);
    }

    public int populateEventPrefMagDataForCalcMLTest() throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.eventprefmag\n" +
                "(evid, magtype, magid, lddate)\n" +
                "VALUES(?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(index++, CalcMLTests.CalcMLTestEventId);
        statement.setString(index++, "l");
        statement.setInt(index++, CalcMLTests.CalcMLTestMagId);
        statement.setString(index++, "2021-01-15 08:27:22.000");
        return insertQueryHelper(statement);
    }

    protected int insertQueryHelper(PreparedStatement statement) throws Exception {
        int rowsAffected = statement.executeUpdate();
        if (rowsAffected > 0) {
            return rowsAffected;
        } else {
            throw new Exception("Test Data was not created");
        }
    }
}