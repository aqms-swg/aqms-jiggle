// *** PACKAGE NAME *** \                                     
package org.trinet.jasi.EW.SQL;                                
                                                               
                                                               
// *** IMPORTS *** \                                          
import java.util.*;                                            
import java.sql.Connection;                                    
import java.sql.SQLException;                                  
import org.trinet.jasi.EW.EWPhase;                                  
                                                               
                                                               
/**                                    
 * <!-- Class Description>             
 *                                     
 *                                     
 * Created:  2002/08/28                        
 *                                     
 * @author   ew_oci2java Tool / DK                        
 * @version  0.99                        
 **/                                   
                                       
public class CreatePickStatement extends EWSQLStatement 
{                                      
                                       
  // *** CLASS ATTRIBUTES *** \       
                                       
  // *** CONSTRUCTORS *** \           
  public CreatePickStatement()                          
  {                                    
    sSQLStatement = "Begin Create_Pick(OUT_idPick => :OUT_idPick,IN_idChan => :IN_idChan,IN_sSource => :IN_sSource,IN_sSourcePickID => :IN_sSourcePickID,IN_sPhase => :IN_sPhase,IN_tPhase => :IN_tPhase,IN_cMotion => :IN_cMotion,IN_cOnset => :IN_cOnset,IN_dSigma => :IN_dSigma); End;";
    bIsQuery = false;                
    init();                            
  }                                    
                                       
                                       
  public CreatePickStatement(Connection IN_conn)        
  {                                    
    this();                            
    SetConnection(IN_conn);            
  }                                    
                                       
                                       
  // *** CLASS METHODS *** \          
                                       
  protected boolean SetInputParams(Object obj)    
  {                                  
                                     
    EWPhase objEWPhase = (EWPhase)obj;              
                                     
    try                              
    {                                
      cs.setLong(2, objEWPhase.idChan);    
      cs.setString(3, objEWPhase.sSource);    
      cs.setString(4, objEWPhase.sSourcePickID);    
      cs.setString(5, objEWPhase.sPhase);    
      cs.setDouble(6, objEWPhase.tPhase);    
      cs.setString(7, objEWPhase.cMotion);    
      cs.setString(8, objEWPhase.cOnset);    
      cs.setDouble(9, objEWPhase.dSigma);    
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in CreatePickStatement:SetInputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end CreatePickStatement::SetInputParams()    
                                     
                                     
  protected boolean RegisterOutputParams()    
  {                                  
    try                              
    {                                
      cs.registerOutParameter(1, java.sql.Types.BIGINT); 
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in CreatePickStatement:RegisterOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
    return(true);                    
  }  // end CreatePickStatement:RegisterOutputParams()
                                     
                                     
  protected boolean RetrieveOutputParams(Object obj)
  {                                  
    EWPhase objEWPhase = (EWPhase)obj;             
                                     
    try                              
    {                                
      objEWPhase.idPick=cs.getLong(1);     
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in CreatePickStatement:RetrieveOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end CreatePickStatement::RetrieveOutputParams()
                                     
                                     
} // end class CreatePickStatement                      
                                       
                                       
//             <EOF>                   
