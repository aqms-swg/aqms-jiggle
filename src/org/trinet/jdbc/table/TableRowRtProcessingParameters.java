package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see RtProcessingParameters
*/
public interface TableRowRtProcessingParameters extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "RT_PROCESSING_PARAMETERS";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  20;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "";


/**  RT_PROCESSING_PARAMETERS table "net" column data object offset in collection stored by implementing class.
*/
    public static final int NET = 0;

/**  RT_PROCESSING_PARAMETERS table "sta" column data object offset in collection stored by implementing class.
*/
    public static final int STA = 1;

/**  RT_PROCESSING_PARAMETERS table "seedchan" column data object offset in collection stored by implementing class.
*/
    public static final int SEEDCHAN = 2;

/**  RT_PROCESSING_PARAMETERS table "location" column data object offset in collection stored by implementing class.
*/
    public static final int LOCATION = 3;

/**  RT_PROCESSING_PARAMETERS table "corner_frequency" column data object offset in collection stored by implementing class.
*/
    public static final int CORNER_FREQUENCY = 4;

/**  RT_PROCESSING_PARAMETERS table "damping_constant" column data object offset in collection stored by implementing class.
*/
    public static final int DAMPING_CONSTANT = 5;

/**  RT_PROCESSING_PARAMETERS table "gain" column data object offset in collection stored by implementing class.
*/
    public static final int GAIN = 6;

/**  RT_PROCESSING_PARAMETERS table "gain_units" column data object offset in collection stored by implementing class.
*/
    public static final int GAIN_UNITS = 7;

/**  RT_PROCESSING_PARAMETERS table "ml_corr" column data object offset in collection stored by implementing class.
*/
    public static final int ML_CORR = 8;

/**  RT_PROCESSING_PARAMETERS table "ml_flag" column data object offset in collection stored by implementing class.
*/
    public static final int ML_FLAG = 9;

/**  RT_PROCESSING_PARAMETERS table "me_corr" column data object offset in collection stored by implementing class.
*/
    public static final int ME_CORR = 10;

/**  RT_PROCESSING_PARAMETERS table "me_flag" column data object offset in collection stored by implementing class.
*/
    public static final int ME_FLAG = 11;

/**  RT_PROCESSING_PARAMETERS table "vel_corr" column data object offset in collection stored by implementing class.
*/
    public static final int VEL_CORR = 12;

/**  RT_PROCESSING_PARAMETERS table "vel_flag" column data object offset in collection stored by implementing class.
*/
    public static final int VEL_FLAG = 13;

/**  RT_PROCESSING_PARAMETERS table "acc_corr" column data object offset in collection stored by implementing class.
*/
    public static final int ACC_CORR = 14;

/**  RT_PROCESSING_PARAMETERS table "acc_flag" column data object offset in collection stored by implementing class.
*/
    public static final int ACC_FLAG = 15;

/**  RT_PROCESSING_PARAMETERS table "noise_flag" column data object offset in collection stored by implementing class.
*/
    public static final int NOISE_FLAG = 16;

/**  RT_PROCESSING_PARAMETERS table "ondate" column data object offset in collection stored by implementing class.
*/
    public static final int ONDATE = 17;

/**  RT_PROCESSING_PARAMETERS table "offdate" column data object offset in collection stored by implementing class.
*/
    public static final int OFFDATE = 18;

/**  RT_PROCESSING_PARAMETERS table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 19;
/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =
   "NET,STA,SEEDCHAN,LOCATION,CORNER_FREQUENCY,DAMPING_CONSTANT,GAIN,GAIN_UNITS,ML_CORR,ML_FLAG,ME_CORR,ME_FLAG,VEL_CORR,VEL_FLAG,ACC_CORR,ACC_FLAG,NOISE_FLAG,ONDATE,OFFDATE,LDDATE";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    "RT_PROCESSING_PARAMETERS.NET,RT_PROCESSING_PARAMETERS.STA,RT_PROCESSING_PARAMETERS.SEEDCHAN,RT_PROCESSING_PARAMETERS.LOCATION,RT_PROCESSING_PARAMETERS.CORNER_FREQUENCY,RT_PROCESSING_PARAMETERS.DAMPING_CONSTANT,RT_PROCESSING_PARAMETERS.GAIN,RT_PROCESSING_PARAMETERS.GAIN_UNITS,RT_PROCESSING_PARAMETERS.ML_CORR,RT_PROCESSING_PARAMETERS.ML_FLAG,RT_PROCESSING_PARAMETERS.ME_CORR,RT_PROCESSING_PARAMETERS.ME_FLAG,RT_PROCESSING_PARAMETERS.VEL_CORR,RT_PROCESSING_PARAMETERS.VEL_FLAG,RT_PROCESSING_PARAMETERS.ACC_CORR,RT_PROCESSING_PARAMETERS.ACC_FLAG,RT_PROCESSING_PARAMETERS.NOISE_FLAG,RT_PROCESSING_PARAMETERS.ONDATE,RT_PROCESSING_PARAMETERS.OFFDATE,RT_PROCESSING_PARAMETERS.LDDATE";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"NET", "STA", "SEEDCHAN", "LOCATION", "CORNER_FREQUENCY", 
	"DAMPING_CONSTANT", "GAIN", "GAIN_UNITS", "ML_CORR", "ML_FLAG", 
	"ME_CORR", "ME_FLAG", "VEL_CORR", "VEL_FLAG", "ACC_CORR", 
	"ACC_FLAG", "NOISE_FLAG", "ONDATE", "OFFDATE", "LDDATE"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, false, false, false, true, 
	true, true, true, true, true, 
	true, true, true, true, true, 
	true, true, false, true, true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATASTRING, DATASTRING, DATASTRING, DATASTRING, DATADOUBLE, 
	DATADOUBLE, DATADOUBLE, DATASTRING, DATADOUBLE, DATASTRING, 
	DATADOUBLE, DATASTRING, DATADOUBLE, DATASTRING, DATADOUBLE, 
	DATASTRING, DATASTRING, DATADATE, DATADATE, DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {2, 3, 1, 17, 0};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 0, 0, 0, 7, 3, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 0, 0, 0, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	8, 6, 3, 2, 10, 4, 12, 20, 4, 1, 4, 1, 4, 1, 4, 1, 1, 7, 7, 7
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, null, null, null, null, null, null, null, 
	null, null, null, null, null, null, null, null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))"
    };
}
