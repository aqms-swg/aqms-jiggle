package org.trinet.jiggle;

import java.awt.*;
import java.io.File;
import java.util.StringTokenizer;
import javax.swing.*;
import javax.swing.border.*;

import java.awt.event.*;

import org.trinet.jasi.engines.AbstractHypoMagEngineDelegate;
import org.trinet.jasi.engines.MagnitudeEngineIF;
import org.trinet.jasi.magmethods.MagnitudeMethodIF;
import org.trinet.jasi.SolutionWfEditorPropertyList;
import org.trinet.util.graphics.text.JTextClipboardPopupMenu;

/** A Dialog panel for reviewing/editing the magnitude method properties.
 * The magnitude method properties filenames are contained withing the JiggleProperties class.*/

public class DPMagMethod extends JPanel implements ActionListener {

    protected JiggleSolutionSolverDelegate delegate;
    private JiggleProperties newProps;

    /** The currently selected magnitude method */
    private String selectedMagMethodType = null;
    private MagnitudeMethodIF selectedMagMethod = null;
    private JTextArea methodTextArea = null;
    private JComboBox magMethodBox = null;
    private JCheckBox autoRecalcCheckBox = null;
    private JCheckBox mdRecalcCheckBox = null;

    private JTextField mmField = null;
    /*
     private JTextField methodClassNameField = new JTextField(48);
        methodClassNameField.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                newProps.setProperty(((String)magMethodBox.getSelectedItem())+"MagMethod", methodClassNameField.getText()); 
            }
     });
    */

    private boolean hasChanged = false;

    /** Allow selection of waveform reading mode. */
    public DPMagMethod(Jiggle jiggle, JiggleProperties newProps, JiggleSolutionSolverDelegate delegate) {

        this.newProps = newProps;
        this.delegate = delegate;
           // new JiggleSolutionSolverDelegate(jiggle.solSolverDelegate, newProps);

        try {
            initGraphics();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    /** Build the GUI. */
    private void initGraphics() throws Exception {

        setLayout(new BorderLayout());

        String [] types = new String [] { "un" };
        if (delegate != null)  {
          types = delegate.getMagMethodTypes();
          if (types == null || types.length == 0) types = new String [] { "un" };
        }
        magMethodBox = new JComboBox(types);

        int idx = ((DefaultComboBoxModel) magMethodBox.getModel()).getIndexOf(newProps.getProperty("defaultPreferencesMagMethodType", "ml").toLowerCase());
        if (idx >= 0) magMethodBox.setSelectedIndex(idx);
        else magMethodBox.setSelectedIndex(0);
        selectedMagMethodType = (String) magMethodBox.getSelectedItem();

        JPanel selectPanel = new JPanel(new BorderLayout());
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        JLabel jlbl = new JLabel("Magnitude Method ");
        p.add(jlbl);
        p.add(magMethodBox);
        p.setAlignmentX(Component.LEFT_ALIGNMENT);
        selectPanel.add(p,BorderLayout.NORTH);

        Box hbox = Box.createHorizontalBox();
        mmField = new JTextField(48);
        new JTextClipboardPopupMenu(mmField);
        mmField.setToolTipText("Classname specified for method type by Jiggle property");
        mmField.setText( newProps.getProperty(selectedMagMethodType+"MagMethod", "") );
        mmField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                newProps.setProperty(selectedMagMethodType +"MagMethod", mmField.getText());
                if (delegate != null) selectedMagMethod = delegate.initMagMethod(null, selectedMagMethodType, false);
                hasChanged = true;
            }
        });
        JButton mmButton = new JButton("Class name ");
        mmButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                newProps.setProperty(selectedMagMethodType +"MagMethod", mmField.getText());
                System.out.println("INFO: Jiggle property reset " + selectedMagMethodType +"MagMethod=" + mmField.getText());
                if (delegate != null) selectedMagMethod = delegate.initMagMethod(null, selectedMagMethodType, false);
                //magMethodBox.setSelectedItem(selectedMagMethodType);
                doSetMethodProperties();
                hasChanged = true;
            }
        });
        mmButton.setToolTipText("Press to set selected method property to the classname specified in text field");
        hbox.add(mmButton);
        hbox.add(mmField);
        hbox.add(Box.createHorizontalGlue());
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        selectPanel.add(hbox, BorderLayout.CENTER);

        //autoRecalc Mag 
        Box cBox = Box.createVerticalBox();
        autoRecalcCheckBox = new JCheckBox("Auto recalculate magnitude after relocation");
        autoRecalcCheckBox.setSelected(newProps.getBoolean("autoRecalc"+selectedMagMethodType.toUpperCase()));
        autoRecalcCheckBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        autoRecalcCheckBox.addItemListener( new ItemListener() {
          public void itemStateChanged(ItemEvent evt) {
            newProps.setProperty("autoRecalc"+selectedMagMethodType.toUpperCase(), (evt.getStateChange() == ItemEvent.SELECTED));
          }
        });
        cBox.add(autoRecalcCheckBox);

        mdRecalcCheckBox = new JCheckBox("Md recalc choice of resetting coda tau values to decay parameter extrapolations");
        mdRecalcCheckBox.setSelected(newProps.getBoolean("mdTauOptionEnabled"));
        mdRecalcCheckBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        mdRecalcCheckBox.addItemListener( new ItemListener() {
          public void itemStateChanged(ItemEvent evt) {
            newProps.setProperty("mdTauOptionEnabled", (evt.getStateChange() == ItemEvent.SELECTED));
          }
        });
        cBox.add(mdRecalcCheckBox);
        mdRecalcCheckBox.setVisible(selectedMagMethodType.endsWith("d"));

        selectPanel.add(cBox, BorderLayout.SOUTH);

        methodTextArea = new JTextArea();
        methodTextArea.setToolTipText("Edit text to add a property or change its value, then press \"Set\"");
        methodTextArea.setEditable(true); // until we come up with property parser for text -aww 09/20/2007
        methodTextArea.setBackground(Color.white);
        methodTextArea.setRows(24);
        methodTextArea.setColumns(60);
        //methodTextArea.setPreferredSize(new Dimension(600,500));
        new JTextClipboardPopupMenu(methodTextArea);
        setMethodTextArea(selectedMagMethodType);


         // method properties action buttons
        ActionListener actionListener =  new SetSaveLoadMethodActionHandler();
        JButton setPropsButton = new JButton("Set");
        setPropsButton.addActionListener(actionListener);
        setPropsButton.setToolTipText("Set the method's properties to current text area values.");

        JButton loadPropsButton = new JButton("Load");
        loadPropsButton.addActionListener(actionListener); 
        loadPropsButton.setToolTipText("Read and set the selected method's properties from a specified file.");

        JButton savePropsButton = new JButton("Save");
        savePropsButton.addActionListener(actionListener); 
        savePropsButton.setToolTipText("Set, then write the methods's properties as shown in text area to a specified file.");

        JButton removePropsButton = new JButton("Remove");
        removePropsButton.addActionListener(actionListener); 
        removePropsButton.setToolTipText("Remove selected/input property key (e.g. misspelled)");

        Box methodButtonBox = Box.createHorizontalBox();
        methodButtonBox.add(Box.createHorizontalGlue());
        methodButtonBox.add(setPropsButton);
        methodButtonBox.add(savePropsButton);
        methodButtonBox.add(loadPropsButton);
        methodButtonBox.add(removePropsButton);
        methodButtonBox.add(Box.createHorizontalGlue());

        TitledBorder titledBorder =
            new TitledBorder(BorderFactory.createEtchedBorder(Color.white, 
                        new Color(148, 145, 140)),"Magnitude Method Properties");
        this.setBorder(titledBorder);
        this.add(selectPanel, BorderLayout.NORTH);
        JScrollPane jsp = new JScrollPane(methodTextArea);
        jsp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        this.add(jsp, BorderLayout.CENTER);
        this.add(methodButtonBox, BorderLayout.SOUTH);

        // add AFTER box's magMethod is populated else adding/selecting items fires events
        magMethodBox.addActionListener(DPMagMethod.this);
    }

    protected void addMagTypeChangeListener(ActionListener l) {
        magMethodBox.addActionListener(l);
    }

    public void actionPerformed(ActionEvent e) {
        JComboBox src = (JComboBox) e.getSource();
        String magType = (String) src.getSelectedItem();

        if (magType != null && !magType.equals(selectedMagMethodType) )  {

            if (src != magMethodBox) {
                magMethodBox.setSelectedItem(magType);
                return;
            }

            selectedMagMethodType = magType;
            if (autoRecalcCheckBox != null) {
                autoRecalcCheckBox.setSelected(newProps.getBoolean("autoRecalc"+selectedMagMethodType.toUpperCase()));
            }

            if (mdRecalcCheckBox != null) {
                mdRecalcCheckBox.setVisible(selectedMagMethodType.endsWith("d"));
                mdRecalcCheckBox.setSelected(newProps.getBoolean("mdTauOptionEnabled"));
            }

            setMethodTextArea(selectedMagMethodType);

            if (mmField != null) {
                mmField.setText(newProps.getProperty(magType+"MagMethod", ""));
            }
        }
    }

    private void setMethodTextArea(String magType) {
        if (delegate != null) {
            selectedMagMethod =  delegate.initMagMethod(magType);
        }
        if (selectedMagMethod != null) {
            setMagMethodTextArea(selectedMagMethod);
        }
        else {
            String msg = (delegate == null) ?
               "Magnitude Engine delegate null, not yet initialized by Jiggle startup." :
               "No magnitude method is defined for type : " + magType;
            methodTextArea.setText(msg);
            methodTextArea.selectAll();
        }
    }
 
    private boolean doSetProperties() {
        return doSetMethodProperties();
    }

    private boolean doSetMethodProperties() {
        boolean status = true;
        if (selectedMagMethod != null) {
            SolutionWfEditorPropertyList gpl = new SolutionWfEditorPropertyList(); // changed list type -aww 2008/03/26
            System.out.println("INFO: Properties dialog setting method properties for: " + selectedMagMethod.getMethodName());
            status = gpl.setProperties( methodTextArea.getText() ); // parse properties from text area string
            selectedMagMethod.setProperties(gpl);
            hasChanged = true;
        }
        return status;
    }

    protected boolean hasChanged() {
        return hasChanged;
    }

    /** Return the type of the selected magnitude method. */
    public MagnitudeMethodIF getSelectedMagMethod() {
        return selectedMagMethod;
    }

    /** Return the type of the selected magnitude method. */
    public void setSelectedMagMethodType(String type) {
        magMethodBox.setSelectedItem(type);
    }

    /** Return the type of the selected magnitude method. */
    public String getSelectedMagMethodType() {
        return selectedMagMethodType;
    }

    /** Populate the text area with magnitude method properties.  */
    protected void setMagMethodTextArea(MagnitudeMethodIF magMethod) {
        if (methodTextArea == null) return;
        StringBuffer sb = new StringBuffer(1024);
        if (magMethod != null) {
          sb.append("# -- MagMethod Class: ").append(magMethod.getClass().getName()).append("\n");
          sb.append("# -- ").append(magMethod.getMethodName()).append(" MagMethod Properties --\n");
          sb.append(magMethod.getProperties().listToString()).append("\n");
        }
        methodTextArea.setText(sb.toString());
        methodTextArea.setCaretPosition(0);
    }

    private class SetSaveLoadMethodActionHandler implements ActionListener {

      public void actionPerformed(ActionEvent evt) {

        if (selectedMagMethod == null) return;

        String cmd = evt.getActionCommand();

        try {
          boolean success = true;

          if (cmd == "Set") {
            success = doSetMethodProperties();
          }
          else {

            JFileChooser jfc = new JFileChooser(newProps.getUserFilePath());
            //jfc.setFileFilter(new MyFileFilter());
            //jfc.setFileSelectionMode(JFileChooser.FILES_ONLY); // forces save to be inside jiggle user dir -aww 2008/06/24
            String filename = newProps.getUserFileNameFromProperty(selectedMagMethodType+"MagMethodProps");
            String oldName = filename;
            File file = null;
            if (filename != null) {
                jfc.setSelectedFile(new File(filename));
                oldName = jfc.getSelectedFile().getAbsolutePath();
            }

            if (cmd == "Save") {
              if (jfc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
                  file = jfc.getSelectedFile();
                  filename = file.getAbsolutePath();
                  doSetMethodProperties(); // assume user wants to use those saved as the current properties
                  System.out.println("INFO: Saving user edited properties for: " +
                      selectedMagMethod.getMethodName() + " to file: " + filename);
                  /*
                  if (file.exists()) {
                      file.renameTo(file.getAbsolutePath()+EpochTime.epochToString(System.currentTimeMillis()/1000.,"yyyy-MM-dd_HHmmss"));
                  }
                  */
                  selectedMagMethod.getProperties().setUserPropertiesFileName(filename);
                  success = selectedMagMethod.getProperties().saveProperties("Saved by Jiggle GUI"); 
              }
            }
            else if (cmd == "Load") {
              if (jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                  file = jfc.getSelectedFile();
                  filename = file.getAbsolutePath();
                  if (! file.exists()) {
                      JOptionPane.showMessageDialog(getTopLevelAncestor(),
                           filename + " file D.N.E. !", 
                           "Load MagMethod Properties", JOptionPane.ERROR_MESSAGE);
                     return;
                  }

                  System.out.println("INFO: Loading properties for: " + selectedMagMethod.getMethodName() +
                      " from file: " + filename);
                  success = selectedMagMethod.setProperties(filename);

                  if (success) hasChanged=true;
              }
            }
            else if (cmd == "Remove") {
                StringTokenizer toke =  new StringTokenizer(methodTextArea.getSelectedText(), "=:");
                String key = null;
                if (toke.countTokens() > 0) {
                    key = toke.nextToken().trim();
                }
                key = JOptionPane.showInputDialog(getTopLevelAncestor(), "Enter a property name (selected text key) to remove from list", key);
                if (key != null && key.length() > 0) {
                    selectedMagMethod.getProperties().remove(key);
                    setMagMethodTextArea(selectedMagMethod);
                }
                else return;  //no-op
            }

            // Change delegate property for property filename of this magmethod type ? 
            if (success) {
              selectedMagMethod.reset();

              if (oldName == null || ! oldName.equals(filename)) { // prompt to change jiggle.props delegate property
                if ( JOptionPane.showConfirmDialog(null,
                    "REQUIRED properties not specified in input file were set to DEFAULT values\n" +
                    "Set new filename as new default for this magnitude method in jiggle.props?",
                    "Update Jiggle properties",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)  {
                  //System.out.println(jfc.getCurrentDirectory().getAbsolutePath() + " : " + newProps.getUserFilePath());
                  if (jfc.getCurrentDirectory().getAbsolutePath().equals(newProps.getUserFilePath()) ) {
                      filename = jfc.getSelectedFile().getName();
                  }
                  newProps.setProperty(selectedMagMethodType + "MagMethodProps",  filename);
                  hasChanged = true;
                }
              }
              else {
                // Popup below reminds user that the currently selected method class sets its required properties
                // to default values when they are not defined in the loaded input file
                JOptionPane.showMessageDialog(getTopLevelAncestor(),
                    "REQUIRED properties not specified in input file were set to DEFAULT values", 
                    "Load MagMethod Properties", JOptionPane.PLAIN_MESSAGE);
              }
            }
          }
          if (! success) notifyFailure(cmd, "Method");

          // setSelectedItem fires actionEvent and this forces listening magMethodBox to refresh everything 
          //magMethodBox.setSelectedItem(selectedMagMethodType);
          setMagMethodTextArea(selectedMagMethod);
          //if (success && (cmd == "Load")) doSetMethodProperties(); // test 2012/02/13 -aww

        }
        catch(Exception ex) {
          ex.printStackTrace();
          notifyFailure(cmd, "Method");
        }
      }

    }

    private void notifyFailure(String cmd, String type) {
        JOptionPane.showMessageDialog(null,
            cmd + " properties failed , check text and messages!",
            "Magnitude " + type + " Properties", JOptionPane.PLAIN_MESSAGE);
    }

    private class MyFileFilter extends javax.swing.filechooser.FileFilter {

        public String getDescription() {
            return ".props";
        }

        public boolean accept(File f) {
            if (f.isDirectory()) {
                return true;
            }
            String str = f.getName();
            int idx = str.lastIndexOf('.');
            String ext = null;
            if (idx > 0 &&  idx < str.length() - 1) {
                ext = str.substring(idx+1).toLowerCase();
            }
            if (ext != null) {
                if (ext.equalsIgnoreCase("props")) return true;
            }
            return false;
        }
    }

}
