package org.trinet.jasi.coda;
import java.util.*;
import org.trinet.jasi.*;

/** To calculate time series amplitude coda decay data and get results in a Coda object. */
public class DefaultMcCodaGenerator extends AbstractCodaGenerator {

    public static final String  DEFAULT_ALGORITHM              = "Mc";


    public static final int     DEFAULT_MIN_GOOD_WINDOWS       = 2;
    public static final int     DEFAULT_MAX_GOOD_WINDOWS       = 20;

    public static final double  DEFAULT_QFIX                   = 1.8;

    public static final double  DEFAULT_BIAS_LTA_SECS          = 10.;
    public static final double  DEFAULT_CODA_START_SNR         = 3.0;
    public static final double  DEFAULT_CODA_CUTOFF_SNR        = 1.5;
    public static final double  DEFAULT_PASS_THRU_NSR          = 1.8; 

    public static final boolean DEFAULT_RESET_ON_CLIP          = false;

    public DefaultMcCodaGenerator() {
        initBuffers(DEFAULT_MAX_GOOD_WINDOWS);
        this.algorithm                         = DEFAULT_ALGORITHM;
        this.biasLTASecs                       = DEFAULT_BIAS_LTA_SECS;       // optionally set by method input
        this.minGoodWindowsToEndCalcOnClipping = DEFAULT_MIN_GOOD_WINDOWS;    // optionally set by method input
        this.codaStartSignalToNoiseRatio       = DEFAULT_CODA_START_SNR;      // optionally set by method input
        this.codaCutoffSignalToNoiseRatio      = DEFAULT_CODA_CUTOFF_SNR;     // optionally set by method input
        this.passThruNoiseDecayAmpRatio        = DEFAULT_PASS_THRU_NSR;       // optionally set by method input
        this.qFix                              = DEFAULT_QFIX;                // optionally set by method input
        this.resetOnClipping                   = DEFAULT_RESET_ON_CLIP;       // optionally set by method input
    }

    /*
    protected Coda timeAmpsToResults(Coda resultsCoda) {
        return super.timeAmpsToResults(resultsCoda, 0); // save values from start
    }
    */

    protected double getWindowWeight(double tauCurrentWindow, double averagedAbsAmp) {
        double weight = 1.0;
        if (averagedAbsAmp < minCodaStartingAmp) {
             weight = (averagedAbsAmp - maxCodaNoiseCutoffAmp)/(minCodaStartingAmp - maxCodaNoiseCutoffAmp);
        }
        return (100.*weight)/(tauCurrentWindow*tauCurrentWindow);
    }

}
