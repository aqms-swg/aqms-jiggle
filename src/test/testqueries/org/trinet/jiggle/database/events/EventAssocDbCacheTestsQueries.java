package test.testqueries.org.trinet.jiggle.database.events;

import org.junit.jupiter.api.Test;
import org.trinet.jiggle.common.type.AssocEvent;
import org.trinet.jiggle.database.event.EventAssocDbCache;
import test.org.trinet.jiggle.database.event.EventAssocDbCacheTests;
import test.testqueries.common.TestQueries;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import static org.junit.jupiter.api.Assertions.*;

public class EventAssocDbCacheTestsQueries extends TestQueries {


    public EventAssocDbCacheTestsQueries(String URL) throws SQLException {
        super(URL);
    }

    public int populateEventTableForDeleteTest() throws Exception {
        return populateEventTableHelper(EventAssocDbCacheTests.deleteEventId);
    }

    public int populateEvent2TableForDeleteTest() throws Exception {
        return populateEventTableHelper(EventAssocDbCacheTests.deleteEventId2);
    }

    public int populateAssocEventTableForDeleteTest() throws Exception {
        return populateAssocEventTableForGetAllTest(EventAssocDbCacheTests.deleteEventId, EventAssocDbCacheTests.deleteEventId2);
    }

    public int populateEventTableForGetAllTest() throws Exception {
        return populateEventTableHelper(EventAssocDbCacheTests.getAllEventId);
    }

    public int populateEvent2TableForGetAllTest() throws Exception {
        return populateEventTableHelper(EventAssocDbCacheTests.getAllEventId2);
    }

    public int populateAssocEventTableForGetAllTest() throws Exception {
        return populateAssocEventTableForGetAllTest(EventAssocDbCacheTests.getAllEventId, EventAssocDbCacheTests.getAllEventId2);
    }

    public int populateEventTableForGetAssociatedTest() throws Exception {
        return populateEventTableHelper(EventAssocDbCacheTests.getAssociatedEventId);
    }

    public int populateEvent2TableForGetAssociatedTest() throws Exception {
        return populateEventTableHelper(EventAssocDbCacheTests.getAssociatedEventId2);
    }

    public int populateAssocEventTableForGetAssociatedTest() throws Exception {
        return populateAssocEventTableForGetAllTest(EventAssocDbCacheTests.getAssociatedEventId, EventAssocDbCacheTests.getAssociatedEventId2);
    }

    public int populateEventTableForGetCommentTest() throws Exception {
        return populateEventTableHelper(EventAssocDbCacheTests.getCommentEventId);
    }

    public int populateEvent2TableForGetCommentTest() throws Exception {
        return populateEventTableHelper(EventAssocDbCacheTests.getCommentEventId2);
    }

    public int populateAssocEventTableForGetCommentTest() throws Exception {
        return populateAssocEventTableForGetAllTest(EventAssocDbCacheTests.getCommentEventId, EventAssocDbCacheTests.getCommentEventId2);
    }

    public int populateEventTableForInsertTest() throws Exception {
        return populateEventTableHelper(EventAssocDbCacheTests.insertEventId);
    }

    public int populateEvent2TableForInsertTest() throws Exception {
        return populateEventTableHelper(EventAssocDbCacheTests.insertEventId2);
    }

    public int populateEventTableForIsExistTest() throws Exception {
        return populateEventTableHelper(EventAssocDbCacheTests.isExistEventId);
    }

    public int populateEvent2TableForIsExistTest() throws Exception {
        return populateEventTableHelper(EventAssocDbCacheTests.isExistEventId2);
    }

    public int populateAssocEventTableForIsExistTest() throws Exception {
        return populateAssocEventTableForGetAllTest(EventAssocDbCacheTests.isExistEventId, EventAssocDbCacheTests.isExistEventId2);
    }

    private int populateEventTableHelper(long eventId) throws Exception {
        int index = 1;
        String sql = "INSERT INTO TRINETDB.EVENT\n" +
                "(EVID, PREFOR, PREFMAG, PREFMEC, COMMID, AUTH, SUBSOURCE, ETYPE, SELECTFLAG, VERSION, LDDATE)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setLong(index++, eventId);
        statement.setInt(index++, 629663);
        statement.setInt(index++, 1312527);
        statement.setNull(index++, Types.INTEGER);
        statement.setNull(index++, Types.INTEGER);
        statement.setString(index++, "CI");
        statement.setString(index++, "RT7");
        statement.setString(index++, "eq");
        statement.setInt(index++, 0);
        statement.setInt(index++, 2);
        statement.setString(index++, "2014-06-26 19:34:56.000000");
        return insertQueryHelper(statement);
    }

    public int populateAssocEventTableForGetAllTest(long eventId, long eventId2) throws Exception {
        int index = 1;
        String sql = "INSERT INTO trinetdb.assocevents\n" +
                "(evid, evidassoc, commid, lddate)\n" +
                "VALUES(?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS'));";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setLong(index++, eventId);
        statement.setLong(index++, eventId2);
        statement.setLong(index++, EventAssocDbCacheTests.getCommentCommId);
        statement.setString(index++, "2024-09-23 20:21:31.846");
        return insertQueryHelper(statement);
    }
}

