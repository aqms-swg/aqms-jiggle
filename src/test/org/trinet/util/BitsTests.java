package test.org.trinet.util;

import org.junit.jupiter.api.Test;
import org.trinet.util.Bits;
import test.common.UnitTests;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class BitsTests extends UnitTests {

    @Test
    public void bit_set_returns_true_with_number_4_and_index_of_bit_1() {
        assertTrue(Bits.bitSet((byte) 4, 1));
    }

    @Test
    public void bit_set_returns_false_with_number_4_and_index_of_bit_0() {
        assertFalse(Bits.bitSet((byte) 4, 0));
    }

    @Test
    public void bytes_to_string_returns_correct_value() {
        assertEquals("D!#$", Bits.bytesToString(new byte[]{68, 33, 35, 36}));
    }

    @Test
    public void bytes_to_int_2_big_endian_true_returns_correct_value() {
        assertEquals(17441, Bits.byteToUInt2(new byte[]{68, 33, 35, 36}, 0, true));
    }

    @Test
    public void bytes_to_int_2_big_endian_false_returns_correct_value() {
        assertEquals(8516, Bits.byteToUInt2(new byte[]{68, 33, 35, 36}, 0, false));
    }
}