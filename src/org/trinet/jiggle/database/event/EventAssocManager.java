package org.trinet.jiggle.database.event;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import org.trinet.jasi.DataSource;
import org.trinet.jiggle.common.AlertUtil;
import org.trinet.jiggle.common.JiggleExceptionHandler;
import org.trinet.jiggle.database.JiggleDb;

public class EventAssocManager {
    /** Jiggle release notes link */
    public static final String JIGGLE_RELEASE_NOTES_LINK = "<a href=\"https://gitlab.com/aqms-swg/aqms-jiggle/-/releases\">Jiggle Release Notes</a>";
    private static final String ASSOCEVENTS_TABLE_NAME = "ASSOCEVENTS";
    private static EventAssocFactory eventAssocFactory;
    private static final String MESSAGE = "<html>Your AQMS database doesn't have an ASSOCEVENTS table<br>"
            + "Please update your AQMS schema by adding the ASSOCEVENTS table.<br>" + "See also:<br>"
            + "<a href=\"https://gitlab.com/aqms-swg/aqms-db-pg/-/blob/master/create/tables/parametric_schema/create_ASSOCEVENTS.sql\">create_ASSOCEVENTS.sql</a><br>"
            + JIGGLE_RELEASE_NOTES_LINK + "<br>"
            + "<br>Please restart the application after adding the ASSOCEVENTS table.</html>";
    private static final String TITLE = "Database Error";

    private static synchronized EventAssocFactory createEventAssocFactory() {
        if (eventAssocFactory == null) {
            final EventAssocFactory factory;
            if (!doesAssoceventsTableExists()) {
                AlertUtil.displayMessage(null, TITLE, MESSAGE, JOptionPane.ERROR_MESSAGE);
                final EventAssoc eventAssocNone = EventAssocNone.getEventAssoc();
                factory = new EventAssocFactory() {
                    @Override
                    public EventAssoc getEventAssoc() {
                        return eventAssocNone;
                    }
                };
            } else if (EventAssocCacheStore.getMaxAge() > 0) {
                // if event cache is enabled
                factory = new EventAssocFactory() {
                    @Override
                    public EventAssoc getEventAssoc() {
                        return EventAssocDbCache.getEventAssoc();
                    }
                };
            } else {
                // if event cache is disabled
                factory = new EventAssocFactory() {
                    @Override
                    public EventAssoc getEventAssoc() {
                        return EventAssocDb.getEventAssoc();
                    }
                };
            }
            eventAssocFactory = factory;
        }
        return eventAssocFactory;
    }

    /**
     * Determines if the ASSOCEVENTS table exists.
     * 
     * @return true if the ASSOCEVENTS table exists, false otherwise.
     */
    private static boolean doesAssoceventsTableExists() {
        try (JiggleDb jiggleDb = new JiggleDb(DataSource.getNewConnect())) {
            String name;
            DatabaseMetaData databaseMetaData = jiggleDb.getConnection().getMetaData();
            ResultSet resultSet = databaseMetaData.getTables(null, null, null, new String[] { "TABLE" });
            while (resultSet.next()) {
                name = resultSet.getString("TABLE_NAME");
                if (ASSOCEVENTS_TABLE_NAME.equalsIgnoreCase(name)) {
                    return true;
                }
            }
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(null, ex);
        }
        return false;
    }

    /**
     * Get the event associated.
     * 
     * @return the event associated.
     */
    public static EventAssoc getEventAssoc() {
        EventAssocFactory factory = eventAssocFactory;
        if (factory == null) {
            factory = createEventAssocFactory();
        }
        return factory.getEventAssoc();
    }
}
