package org.trinet.util.graphics.text;
public class InvalidInputException extends IllegalArgumentException {
    public InvalidInputException() {
        super();
    }

    public InvalidInputException(String message) {
        super(message);
    }
}
