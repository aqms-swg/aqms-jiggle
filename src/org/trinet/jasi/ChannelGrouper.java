package org.trinet.jasi;

/**
 * Similar to a StringTokenizer for Channelable lists. Given a Channelable list,
 * it will return smaller lists that contain only Channelable objects for a single
 * Channel.
 * This will sort the ChannelableList by NET.STA.COMP .
 * @see: ChannelNameSorter
 * <p>Company: USGS</p>
 * @author Doug Given
 * @version 1.0
 */

public class ChannelGrouper extends StationGrouper {

  public ChannelGrouper() {
  }

  /** Set the total groups present .*/
  protected void setGroupCount() {
    groupcount = list.getChannelCount();
  }

  /** */
  public ChannelGrouper(ChannelableListIF list) {
    super(list);
  }

  /** Return true if the two channelabel object belong in the same group. */
  public boolean inSameGroup (Channelable ch1, Channelable ch2) {
    return ch1.getChannelObj().equalsChannelId((ChannelIdIF)ch2.getChannelObj());
  }
}
