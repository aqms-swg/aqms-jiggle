package org.trinet.jasi.engines;
import java.sql.Connection;

public interface DbCommitEngineIF extends CommitEngineIF { // implement this or extension of
    Connection getConnection() ;
    void setConnection(Connection connection) ;
}

