package test.org.trinet.apps;

import org.junit.jupiter.api.Test;
import org.trinet.apps.CalcML;
import org.trinet.apps.Locate;
import test.common.DatabaseTests;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class LocateTests extends DatabaseTests {

    @Test
    public void verify_locate_main_function_outputs_correct_associated_magnitude() {
        if (isOnLocalMachine()) {
            Locate.main(new String[]{"39520735"});
        }
    }
}
